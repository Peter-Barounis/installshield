Microsoft SQL Server 2005 Command Line Query Utility

The SQLCMD utility allows users to connect, send Transact-SQL batches, and output rowset information from SQL Server 7.0, SQL Server 2000, and SQL Server 2005 instances. SQLCMD is a replacement for ISQL and OSQL, but can coexist with installations that have ISQL or OSQL installed.

Note: Microsoft SQL Server 2005 Command Line Query Utility requires Microsoft SQL Server Native Client.
