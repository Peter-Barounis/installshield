@echo off
pushd %Windir%\inf
if not exist digimps md digimps
if exist dgasync.inf move /Y dgasync.inf digimps & echo dgasync.inf
if exist dgasync.pnf move /Y dgasync.pnf digimps & echo dgasync.pnf
if exist digiasyn.inf move /Y digiasyn.inf digimps & echo digiasyn.inf
if exist digiasyn.pnf move /Y digiasyn.pnf digimps & echo digiasyn.pnf
if exist digiisdn.inf move /Y digiisdn.inf digimps & echo digiisdn.inf
if exist digiisdn.pnf move /Y digiisdn.pnf digimps & echo digiisdn.pnf
if exist digimps.inf  move /Y digimps.inf  digimps & echo digimps.inf
if exist digimps.pnf  move /Y digimps.pnf  digimps & echo digimps.pnf
if exist digippp.inf  move /Y digippp.inf  digimps & echo digippp.inf
if exist digippp.pnf  move /Y digippp.pnf  digimps & echo digippp.pnf
if exist digipvc.inf  move /Y digipvc.inf  digimps & echo digipvc.inf
if exist digipvc.pnf  move /Y digipvc.pnf  digimps & echo digipvc.pnf
if exist digisna.inf  move /Y digisna.inf  digimps & echo digisna.inf
if exist digisna.pnf  move /Y digisna.pnf  digimps & echo digisna.pnf
if exist digisvc.inf  move /Y digisvc.inf  digimps & echo digisvc.inf
if exist digisvc.pnf  move /Y digisvc.pnf  digimps & echo digisvc.pnf
if exist mdmdgden.inf move /Y mdmdgden.inf digimps & echo mdmdgden.inf
if exist mdmdgden.pnf move /Y mdmdgden.pnf digimps & echo mdmdgden.pnf
if exist netdgdxb.inf move /Y netdgdxb.inf digimps & echo netdgdxb.inf
if exist netdgdxb.pnf move /Y netdgdxb.pnf digimps & echo netdgdxb.pnf
if exist netmap.inf   move /Y netmap.inf   digimps & echo netmap.inf
if exist netmap.pnf   move /Y netmap.pnf   digimps & echo netmap.pnf
if exist dgaxasyn.inf move /Y dgaxasyn.inf digimps & echo dgaxasyn.inf
if exist dgaxasyn.pnf move /Y dgaxasyn.pnf digimps & echo dgaxasyn.pnf
if exist dgaxmps.inf  move /Y dgaxmps.inf  digimps & echo dgaxmps.inf
if exist dgaxmps.pnf  move /Y dgaxmps.pnf  digimps & echo dgaxmps.pnf
if exist mdmdgax.inf  move /Y mdmdgax.inf  digimps & echo mdmdgax.inf
if exist mdmdgax.pnf  move /Y mdmdgax.pnf  digimps & echo mdmdgax.pnf

if exist classbrd.inf move /Y classbrd.inf digimps & echo classbrd.inf
if exist classbrd.pnf move /Y classbrd.pnf digimps & echo classbrd.pnf
if exist clasport.inf move /Y clasport.inf digimps & echo clasport.inf
if exist clasport.pnf move /Y clasport.pnf digimps & echo clasport.pnf

for %%i in (oem*.inf) do call :MoveOems %%~ni
for %%i in (oem*.inf) do call :MoveWanlinksOems %%~ni
for %%i in (oem*.inf) do call :MoveSplitDriverOems %%~ni
for %%i in (oem*.inf) do call :MoveBoardOems %%~ni
for %%i in (oem*.inf) do call :MovePortOems %%~ni
for %%i in (oem*.inf) do call :MoveOldNewOems %%~ni
for %%i in (oem*.inf) do call :MoveNewOems %%~ni
for %%i in (oem*.inf) do call :MoveNewOemsExtended %%~ni
echo Finished
popd
goto :EOF

:MoveOems
findstr /M CatalogFile=digifep5.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
if (%ERRORLEVEL%) == (0) goto :EndMoveOems
findstr /M CatalogFile=digimpsa.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
if (%ERRORLEVEL%) == (0) goto :EndMoveOems
findstr /M CatalogFile.nt=dgaport.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
if (%ERRORLEVEL%) == (0) goto :EndMoveOems
findstr /M CatalogFile.nt=dgasync.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
if (%ERRORLEVEL%) == (0) goto :EndMoveOems
findstr /M CatalogFile.nt=mdmdigi.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
if (%ERRORLEVEL%) == (0) goto :EndMoveOems
findstr /M /c:"Digi AccelePort" %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
if (%ERRORLEVEL%) == (0) goto :EndMoveOems
:EndMoveOems
goto :EOF

:MoveWanlinksOems
findstr /M CatalogFile=netdgdxb.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps & echo %1.inf
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

:MoveSplitDriverOems
findstr /M CatalogFile=dgaxmps.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps & echo %1.inf
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

:MoveBoardOems
findstr /M CatalogFile=classbrd.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

:MovePortOems
findstr /M CatalogFile=clasport.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

:MoveOldNewOems
findstr /M CatalogFile=digiclsb.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

:MoveNewOems
findstr /M CatalogFile.NT=digiclsb.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
findstr /M CatalogFile.NT=digimpsa.cat %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

:MoveNewOemsExtended
findstr /M /c:"CatalogFile.NT = digiclsb.cat" %1.inf
if (%ERRORLEVEL%) == (0) move /Y %1.inf digimps
if (%ERRORLEVEL%) == (0) if exist %1.pnf move /Y %1.pnf digimps & echo %1.pnf
goto :EOF

