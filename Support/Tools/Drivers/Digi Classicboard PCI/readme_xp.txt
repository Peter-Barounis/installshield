                        Release Notes  

               Digi AccelePort Xp Adapter Driver
                 Version 7.0.15.0, 07/27/2006

                      Microsoft Windows XP
                  Microsoft Windows Server 2003

                 Software Package PN 40002544_A


  CONTENTS

  Section      Description
  -------------------------------
  1            Introduction
  2            Supported Operating Systems
  3            Supported Products
  4            Known Limitations
  5            Update Driver
  6            Additional Notes
  6.1            Digi PortAssist Manager (PAM)
  7            History

  1. INTRODUCTION

     This document describes the device driver that supports 
     the Digi devices listed below.  The driver is for use
     with Microsoft Windows XP or Microsoft Windows Server 2003.

     Refer to the following number when searching the Digi
     International Inc. Web site (www.digi.com) or the FTP
     site (ftp.digi.com) for the latest software package:
     40002544_<highest revision letter>

  2. SUPPORTED OPERATING SYSTEMS

     Microsoft Windows XP
     Microsoft Windows XP x64
     Microsoft Windows Server 2003
     Microsoft Windows Server 2003 x64

  3. SUPPORTED PRODUCTS

     Digi AccelePort 2p Adapter
     Digi AccelePort 4p Adapter
     Digi AccelePort 8p Adapter
     Digi AccelePort 16p Adapter

  4. KNOWN LIMITATIONS

     Digi Port Assist Manager (PAM) is not supported on x64 systems.

     To use the adapter with serial printers, reboot the system after
     device installation or after renaming ports. This is a known
     limitation of the Print Spooler which does not detect changes
     in the list of available ports automatically.

  5. UPDATE DRIVER

     You need administrative privileges to update device drivers. 
     Please be sure you are logged into Windows as Administrator 
     or as a user that is a member of the Administrators group.

     To update an existing AccelePort Xp driver, first perform these 
     steps:

     1. Unzip the driver installation package to a folder of your 
        choice.
     2. Run Digiclean.cmd to remove previous Inf file versions.

     Next, locate and follow the instructions for your specific
     operating system:

     WINDOWS XP
     WINDOWS SERVER 2003

     a. First, navigate to the Device Manager and locate the device
        whose device driver is to be updated:

          o Open the Start menu
          o Right-click "My Computer"
          o Select "Manage"
          o In the left-hand side of the Computer Management
            window, select "Device Manager"

        Locate the adapter to update:

          o In the right-hand side of the window, expand the
            device category entitled: "Multi-port serial adapters"
          o Right-click on the device to update
          o Choose "Properties"

        Begin the update process:

          o Click the "Driver" tab
          o Click the "Update Driver..." button
          o Click "Next >"
          o Verify that the default option is selected, "Search 
            for a suitable driver for my device (recommended)"
          o Click "Next >"
          o Deselect all options
          o Select the option, "Specify a location"
          o Click "Next >"
          o Enter the path to the new driver files and click "OK"
          o Click "Next >"

     b. Windows will now install the new device driver for the
        adapter.

     c. Follow any other instructions to complete the installation.

     d. Click "Finish" to dismiss the Upgrade Device Driver Wizard.

     e. Repeat steps a. through d. for each device you want to update.

  6. ADDITIONAL NOTES

     On-line help is available when you install the product.

     Installation documentation can be found at Digi's Web site,
     http://www.digi.com, or at Digi's FTP server, ftp://ftp.digi.com.

     6.1 Digi PortAssist Manager (PAM)
     ---------------------------------

       PAM is not supported x64 systems. 

       This driver's PAM communication agent uses UDP Port 2364. 
       PortAssist Manager users will need to reconfigure the 
       PAM Server to use this port number instead of the default. 
       See the Users Guide at http://www.portassist.com for further 
       instructions.

  7. HISTORY

     Version 7.0.15.0, 07/27/2006 (Rev. A)
     ----------------------------------------
     o Added x64 driver support.
     o Added Skip Enumerations support.
     o Fixed port hang using HyperTerminal when port is configured for
       "Complete When Sent" and no cable is connected to the port.
     o IBM X Series 236 IBM Server wouldn't shutdown Windows Server 2003
       when an Xp adapter is installed.
     o Fixed receive stall after 1 byte when application requests a zero
       length receive buffer size.
     o Fixed receive hang after 3-4 days of continuous data transfer.
     o The port's FriendlyName was getting changed to "Digi ASYN" when
       a port settings was reconfigured.

     Version 6.0.04.0 (Released)
     ---------------------------
     o Combined all multiport serial adapters into a single driver
       package with a new part number.
     o Fixed a bugcheck that occurred at the end of the WHQL Device 
       Path Exerciser test with an AccelePort 16p in Windows Server
       2003.

     Version 5.0.315.0 (N Release)
     -----------------------------
     o Microsoft WHQL certification.
     o Fixed ports hung after recovery from stand-by when low 
       latency is enabled.
     o Fixed bugcheck on shutdown after adapter is uninstalled 
       from hot-plug slot.

     Version 5.0.313.0 (M Release)
     -----------------------------
     o Fixed IOCTL_SERIAL_IMMEDIATE_CHAR (TransmitCommChar),
       which was not waiting for previous request to complete.
     o Fixed modem signals so that DTR and RTS default to high 
       on first open.
     o Fixed throughput problems introduced in the M1P release.
     o Fixed extremely slow throughput when the "Write Complete 
       When Sent" option is enabled.
     o Updated AccelePort Xp firmware to C release. Detailed 
       information about firmware enhancements and bugfixes can 
       be found in the AccelePort Xp firmware Release Notes.
     o Implemented RTS toggle support.
     o Added support for Microsoft Windows Server 2003.
     o Removed support for AccelePort RAS, DataFire RAS and
       DataFire SYNC adapters.
     o Enhanced co-installer to support the "Update Driver..." 
       functionality available through the Device Manager.
     o Assigned new part number and released driver as new product.
     o Fixed write timeout failure, in which timed-out writes were 
       being inadvertently completed with STATUS_CANCELLED.
     o Fixed write timeout failure caused by enabling the "Write 
       Complete When Sent" option.

     Version 4.0.229.0 (L Release)
     -----------------------------
     o Fixed: DataFire Sync configuration dialog for X.25 does not 
       show up.
     o Fixed: WanLinks driver does not remove keys from SERIALCOMM 
       when uninstalled.
     o Fixed: The DigiAsyn.sys driver is hanging in an endless loop.
     o Fixed: Multiple issues with E1/R2 parameter configuration.
     o Fixed: The DigiAsyn.sys driver does not handle zero length read
       requests correctly.
     o Fixed: Some timed-out writes are being inadvertently completed 
       with STATUS_CANCELLED.
     o Fixed: Write timeout does not work when Write Complete When 
       Sent option is enabled.
     o Fixed: Dgclean.cmd does not delete DIGIMPS.inf and MDMDGDEN.inf
       files.
     o Fixed: DataFire Sync driver may crash during driver update or 
       uninstall.
     o Updated DataFire RAS firmware to M release. Detailed 
       information about firmware enhancements and bugfixes can be 
       found in the DataFire RAS firmware Release Notes.
     o Fixed: DataFire Sync bugchecks under HIS 2000 SNA 
       configuration (digisoh.sys).
     o Fixed: AccelePort Xp bugchecks when returning from Standby 
       (random).
     o Fixed: Microsoft FAX does not work with AccelePort Xp.
     o Fixed: Unable to print with AccelePort Xp.
     o Fixed: Parity error detection failure with AccelePort Xp.

     Version 4.0.217.0 (L3P)
     -----------------------
     o Fixed: Caller DID option not working.
     o Fixed: E1/R2 GroupB and backward signalling range incorrect.
     o Fixed: Add Channel Mapping option for DataFire RAS24/48/30/60 
       CT1 and PRI.
     o Fixed: Driver does not start when installing L2P from scratch.
     o Fixed: DgUpdate.exe fails with error unable to find specified 
       file.
     o Fixed: DeviceIoctls test application causes bugcheck.

     Version 4.0.214.0 (L2P)
     -----------------------
     o Fixed problem establishing fax connection.
     o Changed Sync2000 PLX9080 setup to initialize in PCI 2.1 mode.
	 o Fixed problem with modem country setting for E1/T1 cards.
	 o Fixed: Port Assist Viewer does not show ISDN layer 3 status.
	 o Fixed: Port Assist Viewer faults when clearing statistics.
	 o Fixed: Port Assist Viewer does not display RAS30/60 boards in 
       E1R2 mode.
     o Fixed dgupdate issues.
     o Fixed Sync2000 issues in preparation for WHQL submission.
     o Corrected Inf issues in preparation for WHQL submission.
     o Fixed 15 second delay on ring-back for dense modems.
	 o Fixed: DM_BB diagnostic not working on Win2K.
	 o Fixed: RasAudio diagnostic not working on Win2K.
	 o Fixed: Debug information displayed in free build.
     o Fixed serial printing errors.

     Version 4.0.210.0 (L1P)
     -----------------------
     o This version was not released to the public.
