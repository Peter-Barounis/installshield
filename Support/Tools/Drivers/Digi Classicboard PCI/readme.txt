                  Release Notes PN 93000605_A

                     Device Driver Package
                 for Digi's Async PCI Adapters
                  Version 7.0.15.0, 07/27/2006

                      Microsoft Windows XP
                  Microsoft Windows Server 2003

                 Software Package PN 40002544_A
 


  CONTENTS

     1. Introduction
     2. Supported Operating Systems
     3. Supported Products
     4. Additional Notes
     5. History


  1. INTRODUCTION

     This document describes the device drivers that support
     the Digi adaptesr listed below.  The drivers are for use
     with Microsoft Windows XP or Microsoft Windows Server 2003.

     Refer to the following number when searching the Digi
     International Inc. Web site (www.digi.com) or the FTP
     site (ftp.digi.com) for the latest software package:

     40002544_<highest revision letter>


  2. SUPPORTED OPERATING SYSTEMS

     Microsoft Windows XP
     Microsoft Windows XP x64
     Microsoft Windows Server 2003
     Microsoft Windows Server 2003 x64


  3. SUPPORTED PRODUCTS

     Digi AccelePort 2p Adapter
     Digi AccelePort 4p Adapter
     Digi AccelePort 8p Adapter
     Digi AccelePort 16p Adapter
     Digi AccelePort C/X PCI
     Digi AccelePort EPC/X PCI
     Digi AccelePort Xem PCI
     Digi AccelePort 4r EIA-422 PCI
     Digi AccelePort 8r EIA-422 PCI
     Digi AccelePort 2r 920 PCI
     Digi AccelePort 4r 920 PCI
     Digi AccelePort 8r 920 PCI
     Digi Neo 2
     Digi Neo 4
     Digi Neo 8
     ClassicBoard PCI 4
     ClassicBoard EIA-422 PCI 4
     ClassicBoard PCI 8
     ClassicBoard EIA-422 PCI 8
     Digi Neo 1

     Note: The Digi Neo 1 will install two COM ports in 
      your system. If you connect a standard cable to the 
      DB25 connector on the adapter, only the first COM 
      port will be usable. You may upgrade the Digi Neo 1 
      to a two port adapter by ordering a cable upgrade 


  4. ADDITIONAL NOTES

     On-line help is available when you install the product.

     See also, the following readme files for notes specific
     to each adapter family:

         readme_xp.txt:   AccelePort 2p, 4p, 8p, and 16p.
         readme_fep5.txt: AccelePort C/X, EPC/X, Xem, and Xr.
         readme_neo.txt:  Neo and ClassicBoards.

     Installation documentation can be found at Digi's Web 
     site, http://www.digi.com, or at Digi's FTP server, 
     ftp://ftp.digi.com.

     When installing the beta driver, run the program Digiclean.cmd 
     to move the existing INF files for the adapter. This ensures 
     that the new driver files will be installed instead of the old 
     driver files. Digiclean.cmd is part of the driver software package 
     and can be found in the same directory as the new driver files.

  5. HISTORY

     Version 7.0.15.0, 07/27/2006 (Rev. A)
     -------------------------------------
     o This driver release has been signed by Microsoft's Windows Hardware Quality 
       Labs (WHQL).
     o This driver package does not support Windows 2000. Windows 2000 is supported
       by separate driver - see part number 40002448.

     Neo and ClassicBoards adapters:
     o Added support for x64.
     o Fixed that system wouldn't shutdown with Neo or ClassicBoard installed.
     o Fixed transmit hang in dual processor systems.
     o Fixed program not receiving data due to a special case of
       read timeouts. The behavior is now the same as other drivers. 
     o Added Skip Enumerations support

     AccelePort 2p, 4p, 8p, and 16p adapters 
     o Added support for x64.
     o Fixed port hang using HyperTerminal when port is configured for
       "Complete When Sent" and no cable is connected to the port.
     o IBM X Series 236 IBM Server wouldn't shutdown Windows Server 2003
       when adapter is installed.
     o Added Skip Enumerations support
     o Fixed receive stall after 1 byte when application requests a zero
       length receive buffer size.
     o Fixed receive hang after 3-4 days of continuous data transfer.
     o The port's FriendlyName was getting changed to "Digi ASYN" when
       a port settings was reconfigured.

     AccelePort C/X, EPC/X, Xem, and Xr adapters:
     o Added support for x64.
     o Removed DTR and RTS signale states from modem status response, to
       match the behavior documented in Windows API.
     o IBM X Series 236 IBM Server won't shutdown Windows Server 2003
       when adapter is installed.
     o Fixed Defect "Ports are failing to read data when 
       application reopen ports quickly"
     o Added Skip Enumerations support

     Version 6.0.04.0, 12/19/2004 (Rev. A)
     -------------------------------------
     o Combined the multiport serial adapters into a single driver
       package with a new part number: 40002448. The combined package
       has been signed by Microsoft's Windows Hardware Quality Labs
       (WHQL).

     AccelePort 2p, 4p, 8p, and 16p adapters:
     o Fixed a bugcheck that occurred at the end of the WHQL Device 
       Path Exerciser test with an AccelePort 16p in Windows Server
       2003.

     AccelePort C/X, EPC/X, Xem, and Xr adapters:
     o Fixed a bugcheck on shutdown when data remains in the 
       transmit queue.
     o Fixed cancel handling of writes.
     o Fixed a bugcheck screen during cleanup while running RTS 
       toggle.
     o Fix to honor former flow control settings on port open.
     o Fix to allow FEP time to handle DTR signal change before 
       the next request.

     Neo and ClassicBoards adapters:
     o Fixed a bugcheck on resume from hibernate.
  

     History for previous driver releases
     ------------------------------------
     See the following readme files for the history specific to each
     adapter family:

         readme_xp.txt:   AccelePort 2p, 4p, 8p, and 16p.
         readme_fep5.txt: AccelePort C/X, EPC/X, Xem, and Xr.
         readme_neo.txt:  Neo and ClassicBoards.

