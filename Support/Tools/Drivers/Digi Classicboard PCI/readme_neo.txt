
          Digi Neo and ClassicBoard PCI Device Driver
                  Version 7.0.15.0, 07/27/2006

                      Microsoft Windows XP
                  Microsoft Windows Server 2003

                 Software Package PN 40002544_A
 

  CONTENTS

  Section      Description
  -------------------------------
  1            Introduction
  2            Supported Operating Systems
  3            Supported Products
  4            Known Limitations
  5            Update Driver
  6            Additional Notes
  7            History

  1. INTRODUCTION

     This document describes the Digi Neo and ClassicBoard 
     PCI Device Driver for Microsoft Windows XP and Server 2003.

     Refer to the following number when searching the Digi
     International Inc. Web site (www.digi.com) or the FTP
     site (ftp.digi.com) for the latest software package:
     40002544_<highest revision letter>

  2. SUPPORTED OPERATING SYSTEMS

     Microsoft Windows XP
     Microsoft Windows XP x64
     Microsoft Windows Server 2003
     Microsoft Windows Server 2003 x64

  3. SUPPORTED PRODUCTS

     Model                  Number of Ports
     --------------------------------------
     Digi Neo                      2
     Digi Neo                      4
     Digi Neo                      8
     ClassicBoard PCI              4
     ClassicBoard EIA-422 PCI      4
     ClassicBoard PCI              8
     ClassicBoard EIA-422 PCI      8
     Digi Neo                      1

     Note: The Digi Neo 1 will install two COM ports in 
      your system. If you connect a standard cable to the 
      DB25 connector on the adapter, only the first COM 
      port will be usable. You may upgrade the Digi Neo 1 
      to a two port adapter by ordering a cable upgrade 
      kit (part number 76000713).

  4. KNOWN LIMITATIONS
   
     The property pages won't display and update properly 
     if the current user does not have local administrative 
     privileges.

     The pop-up help displays a blank window on systems
     with Internet Explorer 6 installed. 

     On Windows Embedded XP, the property pages will use
     a font size that is twice as large as expected. A 
     work-around is to install the MS Sans Serif font. 

     The driver does not support upgrading from the
     earliest version of the driver, Rev. A. If you
     are unable to install the latest driver, first
     uninstall any previously installed Neo and
     ClassicBoard driver.

     The driver supports ClassicBoard PCI adapters only.
     The ClassicBoard ISA adapter is not supported.

     AltPin and DTR/DSR flow control are not compatible.

     If you are planning to use the Digi Neo or
     ClassicBoard PCI with Serial Printers, you have to
     reboot the system after device installation or
     after renaming ports. This is a known limitation of
     the Print Spooler, which does not detect changes in
     the list of available ports automatically.

  5. UPDATE DRIVER

     You need administrative privileges to update device drivers. 
     Please be sure you are logged into Windows as Administrator 
     or as a user that is a member of the Administrators group.

     To update an existing Neo and ClassicBoard PCI driver, first 
     unzip the driver installation package to a folder of your 
     choice.

     Note that the Update Driver functionality available via the 
     Device Manager is not supported for this product, so please 
     follow the instructions carefully.

     a. First, uninstall each adapter that needs to be updated.
        Begin by opening the Device Manager:

          o Open the Start menu
          o Right-click "My Computer"
          o Select "Manage"
          o In the left-hand side of the Computer Management
            window, select "Device Manager"

        Next, uninstall the adapter:

          o Expand the "Multi-port serial adapters" category
          o Right-click on the adapter to uninstall
          o Choose "Uninstall"
          o Click "OK"

        Be sure to uninstall each adapter that needs to be
        upgraded.

     b. Tell Windows to scan for new hardware. Find the "Add
        Hardware Wizard", located in the Control Panel
        directory of the Start Menu.

          o Click "Next >"
          o Wait while Windows searches for new hardware
          o Windows will display the "Found New Hardware Wizard"

        Alternatively, one may reboot Windows to conjure the
        "Found New Hardware Wizard".

     c. Point the "Found New Hardware Wizard" to the directory
        where you unzipped the driver installation package:

          o Select "Install from a list or specific location"
          o Click "Next >"
          o Select "Search for the best driver in these
            locations"
          o Select "Include this location in the search:"
          o Enter the path to the new driver files or click
            "Browse" to browse for the folder.
          o Click "Next >"

     d. Follow any other instructions to complete the installation.

     e. Note that the "Found New Hardware Wizard" may reappear
        throughout the installation process depending on the
        number and type of adapters and ports being installed.

  6. ADDITIONAL NOTES

     Online help is available when you install the product.

     Installation documentation can be found at Digi's Web site,
     http://www.digi.com, or at Digi's FTP server, ftp://ftp.digi.com.

     AltPin
     ------

     10-pin RJ-45 plugs may be difficult to obtain in the retail
     market; therefore, the device driver software incorporates
     a feature called AltPin, which swaps the logical functions
     of DSR (Data Set Ready) with DCD (Data Carrier Detect). When
     the AltPin box is checked, DCD becomes available on pin 1
     of an 8-pin RJ-45 connector (equivalent to pin 2 of a 10-pin
     connector).

     Force Baud Rate
     ---------------

     With the Force Baud Rate feature, the device driver software
     forces the baud rate of a COM port to a fixed value. This
     enables older serial communication applications to use baud
     rates above the applications configuration limit (e.g. 115200).
     If Force Baud Rate is enabled, the driver replaces the
     applications baud rate with the forced baud rate value.

     Drain UART Before Completing Write Requests
     -------------------------------------------

     When enabled, the Drain UART feature forces the driver to wait 
     for the UART to go idle before completing any outstanding write
     requests. The default behavior (disabled) emulates the behavior
     of the standard serial driver, which is to complete write 
     requests as soon all the transmit data has been written to the 
     FIFO (or transmit holding register, if FIFOs are disabled).

  7. HISTORY

     Version 7.0.15.0, 07/27/2006 (P/N 40002544 Rev. A)
     --------------------------------------------------
     o Added x64 driver support.
     o Added Skip Enumerations support
     o System won't shutdown with Neo or ClassicBoard is 
       installed.
     o Fixed transmit hang in dual processor systems.
     o Fixed program not receiving data due to a special case of
       read timeouts. The behavior is now the same as other drivers. 

     Version 6.0.04.0, 12/19/2004 (P/N 40002448 Rev. A)
     --------------------------------------------------
     o Combined all multiport serial adapters into a single driver
       package with a new part number: 40002448.
     o Fixed a bugcheck on resume from hibernate.
  
     Version 2.6.38.0, 03/23/2004 (Rev. G)
     --------------------------------------------
     o Passed Windows Logo testing.
     o Added Port Settings property page.
     o Added 9600 to the drop down list for configuring Forced
       Baud rate.
     o Fixed bluescreen when Neo resumes after system standby.
     o Fixed Embedded XP hang during install of second Neo port.
     o Added support for ClassicBoard 4 EIA-422 adapter.
     o Improved processing of Read Interval timeouts.
     o Enhanced FIFO trigger selection to allow users to select
       trigger values between 0 and 64, inclusive, for Neo.
     o Fixed port names not available after disabled adapter is 
       uninstalled.
     o Fixed flow control issues with multiple adapters installed.
    
     Version 2.5.35.0, 06/24/2003 (Rev. F)
     --------------------------------------------
     o Added workaround for two Exar UART bugs when reading the TX
       and RX FIFO counters
     o Added support for Microsoft Windows Server 2003
     o Added support for ClassicBoard EIA-422 PCI and Digi Neo 2
     o Implemented feature, "Drain UART before completing write 
       requests," to support application-level RTS toggle
     o Refitted driver to flush UART buffers when PurgeComm is
       called and device is flow controlled
     o Modified baud rate divisor calculation to better support
       non-standard baud rates
     o Fixed bluescreen when adapter properties are viewed just 
       after disabling one of the adapter's ports
     o Fixed UI showing disabled ports with port name of COM0
	 
     Version 2.4.26.0, 08/08/2002 (Rev. E)
     --------------------------------------------
     o Added support for MSCOMM32-based Visual Basic applications
     o Fixed port close so outstanding read requests are completed
       prior to closing the port
     o Added full PCI IDs to INF for ClassicBoard 4/8 PCI Rev. A
     o Added full PCI IDs to INF for unattented installation
     o Fixed software flow control character echo failure
     o Added support for Windows XP

     Version 2.0.16.1, 01/30/2002 (Rev. C)
     --------------------------------------------
     o Augmented driver to accept arbitrary baud rates
     o Resolved COM port renumbering issue
     o Enabled force baud feature
     o Implemented on-board hardware flow control
     o Reinforced input buffer flow control processing
     o Remedied Isr / DpcForIsr race condition
     o Upgraded to Html help system

     Version 2.0.0.8, 06/12/2001 (Rev. B)
     --------------------------------------------
     o Added AltPin support for alternative RJ-45 connectors
     o Added support for user definable baud rates
     o Added support for force baud rate

     Version 2.0.0.0, 05/14/2001 (Rev. B1P)
     --------------------------------------------
     o Added support for Digi Neo adapters

     Version 1.1.0.0, 07/27/2000 (Rev. A)
     --------------------------------------------
     o Miscellaneous fixes

     Version 1.0.0.0, 06/28/2000 (Rev. 4P)
     --------------------------------------------
     o Fixed version numbers

     Version 5.01 (5.0.2195.1), 06/20/2000 (Rev. 3P)
     --------------------------------------------
     o Miscellaneous fixes

     Version 5.00 (5.0.2195.1), 05/19/2000 (Rev. 1P)
     --------------------------------------------
     o Pilot release
