                          Release Notes  

              Digi C/X + EPC/X + Xem + Xr + Xr 920
                          Device Driver
                  Version 7.0.15.0, 07/27/2006

                      Microsoft Windows XP
                  Microsoft Windows Server 2003

                        Software Package 
 

  CONTENTS

  Section      Description
  -------------------------------
  1            Introduction
  2            Supported Operating Systems
  3            Supported Products
  4            Additional Notes
  5            History

  1. INTRODUCTION
    
     Refer to the following number when searching the Digi
     International Inc. Web site (www.digi.com) or the FTP
     site (ftp.digi.com) for the latest software package:
     40002544_<highest revision letter>

  2. SUPPORTED OPERATING SYSTEMS

     Microsoft Windows XP
     Microsoft Windows XP x64
     Microsoft Windows Server 2003
     Microsoft Windows Server 2003 x64

  3. SUPPORTED PRODUCTS

     CONTROLLERS

     Model                  
     -------------------------
     Digi AccelePort C/X PCI
     Digi AccelePort EPC/X PCI
     Digi AccelePort Xem PCI

     MULTI-PORT SERIAL CARDS

     Model                              Number of Ports
     --------------------------------------------------
     Digi AccelePort Xr EIA-422 PCI            4
     Digi AccelePort Xr EIA-422 PCI            8
     Digi AccelePort Xr 920 PCI                2
     Digi AccelePort Xr 920 PCI                4
     Digi AccelePort Xr 920 PCI                8

  4. ADDITIONAL NOTES

     On-line help is available when you install the product.

     Installation documentation can be found at Digi's Web 
     site, http://www.digi.com, or at Digi's FTP server, 
     ftp://ftp.digi.com.

     DRIVER INSTALLATION NOTES FOR PCI ADAPTERS
     ------------------------------------------

     After installing one of the above adapters and booting the
     machine, Windows will automatically install an older 
     version of the device driver. This version of the driver is 
     included as part of Windows, and it can also be found on 
     the Windows CD.

     The Update Driver functionality available via the Device
     Manager is not supported, however. Instead, to upgrade the
     existing device driver for any of the above adapters, please 
     follow these steps:

     a. First, uninstall the previously installed device driver.
        From the Device Manager, expand the catagory "Multi-port
        serial adapters". Right-click on the Digi adapter(s) you 
        would like to uninstall from the system and select 
        "Uninstall".

        Be sure to uninstall each adapter that needs to be 
        upgraded.

     b. Run the program DigiClean.cmd to move the existing INF files
        for the adapter. This ensures that the new driver files 
        will be installed instead of the old driver files. 
        DigiClean.cmd is part of the driver software package and can
        be found in the same directory as the new driver files.

     c. Tell Windows to scan for new hardware. From the Device 
        Manager, right click and select "Scan for hardware changes".
        The "Found New Hardware Wizard" should appear.

        Alternatively, one may reboot Windows to conjure the 
        "Found New Hardware Wizard".

     d. Follow the instructions for the "Found New Hardware 
        Wizard". From the "Locate Driver Files" screen, be sure 
        to select "Specify a location" to point the wizard to the 
        location of the new driver files.

     e. Windows will display a "Digital Signature Not Found" 
        message, explaining that the driver is not signed.  Click 
        "Yes" to continue the installation. One may see several of
        these warnings -- simply click "Yes" for each message to 
        proceed with installation.
   
     f. Follow any other instructions to complete the installation.
   
     g. The "Found New Hardware Wizard" may reappear a number of
        times depending on the number and type of adapters you are 
        installing.

  5. HISTORY

     Version 7.0.15.0, 07/27/2006 (PN 40002544 Rev. A)
     -------------------------------------------------
     o Added support for x64.
     o Removed DTR and RTS signal states from modem status response, 
       to match the behavior documented in Windows API.
     o IBM X Series 236 IBM Server won't shutdown Windows Server 2003
       when an FEP5 adapter is installed.
     o Fixed Defect "Ports are failing to read data when 
       application reopen ports quickly"
     o Added Skip Enumerations support

     Version 6.0.04.0 (PN 40002448 Rev. A - December 19, 2004)
     ---------------------------------------------
     o Combined all multiport serial adapters into a single driver
       package with a new part number: 40002448.
     o Fixed bugcheck on shutdown when data remains in the 
       transmit queue.
     o Fixed cancel handling of writes.
     o Fix to prevent blue screen during cleanup while running RTS 
       toggle.
     o Fix to honor former flow control settings on port open.
     o Fix to allow FEP time to handle DTR signal change
       before next request.
       
     Version 4.3.48.0 (PN 40002409 Rev. A, May 19, 2004)
     -----------------------------------------
     o This driver package has a new Digi part number:
         This release:       PN 40002409 (Rev A)
         Previous release:   PN 40002207 (Rev C)
     o The following adapters are no longer supported by this 
       driver:
         Digi AccelePort C/X ISA
         Digi AccelePort Xem ISA
         Digi AccelePort Xe EIA-232 ISA         
         Digi AccelePort Xe EIA-232/422 ISA     
         Digi AccelePort Xe EIA-232/422 ISA      
         IBM 8-Port Async EIA-232/RS-422A PCI      
         IBM 128-Port Async PCI           
     o Added Port Settings property page.
     o Fixed blue screen in Windows Server 2003 when the Xem
       adapter is installed without any modules attached.
     o Fixed missing event viewer descriptions for several 
       message ids.
     o Removed install wizard page for Xr 920 adapters. 
     o Fixed TransmitCommChar() handling - driver was 
       allowing multiple simultaneous requests.
     o C/X adapter was accepting a baudrate it couldn't 
       handle - 921600 baud.

     Version 4.2.45.0 (PN 40002207 Rev. C, December 21, 2003)
     ---------------------------------------------------
     o Fixed problem with ClearCommError misinterpreting 
       breaks and reporting too many bytes in cbInQueue.
     o Fixed a multiprocessor system lock-up when making 
       an inbound RAS connection.
     o Fixed TransmitCommChar() handling.
     o Fixed bugcheck on shutdown after adapter is 
       uninstalled from hot-plug slot.
     o Fixed a race condition in the WaitCommEvent handling.
     o Fixed forced baud rate.
     o Fixed multi-threaded contention problem driving CPU 
       utilization to extremes.
     o Fixed race condition on Xr and Xem adapters which 
       resulted in intermittent adapter deadlock when setting
       the baud rate.

     Version 4.1.38.0 (Rev. B, May 05, 2003)
     ---------------------------------------
     o Added support for Microsoft Windows Server 2003
     o The following adapters are no longer supported by this driver:
         Digi AccelePort 4r 920 ISA
         Digi AccelePort 8r 920 ISA
         Digi AccelePort 4r EIA-232 ISA/PCI
         Digi AccelePort 8r EIA-232 ISA/PCI
         Digi AccelePort EPC/X ISA
         Digi PC/4e ISA
         Digi PC/8e ISA
         Digi PC/16e ISA
         Digi PC/8i ISA
         Digi PC/16i ISA
     o Fixed EV_RXFLAG handling when EV_ERR is also requested.
     o Modified parity, framing, and break error detection such
       that events are now reported before the client issues a
       read request.
     o Fixed intermittent problem with too many characters being 
       reported in the receive queue.
     o Fixed two ring events being generated instead of just one.
     o Fixed an inaccurate ring buffer head pointer value used to
       copy received data from the adapter to the driver.  This
       gave the appearance that the data in the ring buffer had
       wrapped around, giving us a unusually high received byte
       count and bogus data.
     o Fixed a race on multi processor systems: one CPU inaccurately
       reported a count of characters in the receive buffer which 
       included characters that the other CPU was simultaneously 
       removing.
     o Added RTS toggle support for Xem, Xr, and Xr 920 adapters.
     o Fixed RTS toggle to not slew character after dropping signal.
     o Improved compatibility with standard serial driver.
     o Fixed initial port settings to default to 1200,7,e,1 for
       proper Plug & Play modem detection.
     o Changed "Complete writes when transmit complete" feature 
       to default to on.
     o Modified port open and set baud rate procedures to prevent
       race condition when multiple ports are opened simultaneously.
     o Fixed problem upgrading AccelePort Xr EIA-232 adapter
       driver from Windows 2000 to Windows XP or Windows .NET.

     Version 4.0.21.0 (Rev. A, February 13, 2002)
     --------------------------------------------
     o Updated firmware images to latest firmware release, Rev. L 
     o Fixed port number assignment with multiple modules on Xem
     o Fixed display of ports in list of print spooler ports
     o Fixed uninstall problem caused by renaming ports
     o Implemented deallocation of ports when board is physically removed
     o Removed limitation when renaming ports greater than 256
     o Fixed bugcheck when uninstalling second of two CX 128 adapters
     o Included dgclean to aid in the upgrade process
     o Corrected recognition of firmware that supports FepFivePlus

     Version 3.7.3.0 (April 14, 2000)
     --------------------------------
     o Windows XP Professional Installation CD release

     Version 3.7.1.10 (July 8, 1999)
     -------------------------------
     o Windows 2000 Installation CD release
