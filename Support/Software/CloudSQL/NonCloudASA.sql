//////////////////////////////////////////////////////////////////////
// Script to set Cloud setting in new install to No
//// DATABUILD-156 - RBB - 5/30/17
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
//// Make sure configurationsData table exists
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='configurationsData' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.configurationsData (
	  id int not null identity,
	  configurationID smallint not null,
	  dataName varchar(100) not null,
	  dataValue varchar(100) not null,
	  isActive bit not null,
	  comments varchar(250) null,
      constraint PK_CONFIGURATIONSDATA primary key (id),
      constraint AK_CONFIGURATIONSDATA unique (configurationID, dataName),
      constraint FK_CONFIGURATIONSDATA_CONFIGURATIONS foreign key (configurationID)
         references DBA.configurations (id)
	);

   comment on table DBA.configurationsData is 'configurations data';
   comment on column DBA.configurationsData.id is 'Configuration data unique ID';
   comment on column DBA.configurationsData.configurationID is 'Reference to configurations table';
   comment on column DBA.configurationsData.dataName is 'Configuration data name';
   comment on column DBA.configurationsData.dataValue is 'Configuration data value';
   comment on column DBA.configurationsData.isActive is '1 - active, 0 - inactive';
   comment on column DBA.configurationsData.comments is 'Configuration data comments';
end if;
go

grant SELECT on dba.configurationsData to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
//// Add Cloud Setting to configurationsData
/////////////////////////////////////////////////////////////////////////
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'Cloud') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
		values(0, 'Cloud', 'No', 1, 'Cloud Setting');
else
	update configurationsData
		set dataValue = 'No'
		where configurationID = 0 and dataName = 'Cloud';
end if;


