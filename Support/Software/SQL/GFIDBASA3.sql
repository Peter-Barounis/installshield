////////////////////////////////////////////////////////////////////////
// * * * S T O R E D   P R O C E D U R E   D E F I N I T I O N S * * *
////////////////////////////////////////////////////////////////////////

// $Author: rbecker $
// $Revision: 1.66 $
// $Date: 2017-04-04 22:35:50 $


if exists (select * from sysobjects where name = 'AllTransData' and type = 'V') then
	drop view AllTransData;
end if;
go

create view dba.AllTransData
as
/*
select top 10 * from AllTransData;

select top 10 * from AllTransData
where tbl_name = 'svd';
*/
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'ppd' as tbl_name, p.id, p.loc_n, p.tr_seq, grp, des, p.seq,
			p.fs, p.ttp, p.aid, p.mid, p.flags, null as trk2, 0 as amt, null as org_dir, null as keys, 
			(case when x.card_pid IS null then (case when p.seq != 0 then CAST(p.seq as varchar(25)) else null end) else x.card_pid end) as card_id, 
			tpbc, sc, null as remval, null as impval, null as f_use_f, null as restored_f
	 from ppd as p
	 left join gfi_epay_card_inv x 
			on p.card_eid = x.card_eid	
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'svd' as tbl_name, p.id, p.loc_n, p.tr_seq, grp, des, p.seq,
			p.fs, p.ttp, p.aid, p.mid, p.flags, null as trk2, deduction as amt, null as org_dir, null as keys, 
			(case when x.card_pid IS null then (case when p.seq != 0 then CAST(p.seq as varchar(25)) else null end) else x.card_pid end) as card_id, 
			null as tpbc, null as sc, remval, impval, f_use_f, restored_f
	 from svd as p
	 left join gfi_epay_card_inv x 
			on p.card_eid = x.card_eid	
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'srd' as tbl_name,p.id, p.loc_n, p.tr_seq, grp, des, p.seq,
			p.fs, p.ttp, p.aid, 0 as mid, p.flags, null as trk2, 0 as amt, null as org_dir, null as keys, 
			(case when x.card_pid IS null then (case when p.seq != 0 then CAST(p.seq as varchar(25)) else null end) else x.card_pid end) as card_id, 
			null as tpbc, null as sc, remval, impval, f_use_f, restored_f
	 from srd as p
	 left join gfi_epay_card_inv x 
			on p.card_eid = x.card_eid	
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'scd' as tbl_name,p.id, p.loc_n, p.tr_seq, grp, des, 0 as seq,
			p.fs, p.ttp, /* gfisf_epay_aid */ 0 as aid, 0 as mid, 0 as flags, p.trk2, 0 as amt, null as org_dir, null as keys, 
			null as card_id, null as tpbc, null as sc, null as remval, null as impval, null as f_use_f, null as restored_f
	 from scd as p
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'trd' as tbl_name, p.id, p.loc_n, p.tr_seq, grp, des, p.seq,
			p.fs ,p.ttp ,p.aid ,p.mid ,p.flags, null as trk2, 0 as amt, org_dir, null as keys, 
			(case when x.card_pid IS null then (case when p.seq != 0 then CAST(p.seq as varchar(25)) else null end) else x.card_pid end) as card_id, 
			null as tpbc, null as sc, null as remval, impval, null as f_use_f, restored_f
	 from trd as p
	 left join gfi_epay_card_inv x 
			on p.card_eid = x.card_eid	
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'ccd' as tbl_name, p.id, p.loc_n, p.tr_seq, grp, des, 0 as seq, 
			p.fs, p.ttp, /* gfisf_epay_aid */ 0 as aid, 0 as mid, 0 as flags, p.trk2, 0 as amt, null as org_dir, null as keys, x.card_pid as card_id, 
			null as tpbc, null as sc, null as remval, null as impval, null as f_use_f, null as restored_f
	 from ccd as p
	 left join gfi_epay_card_inv x 
			on p.card_eid = x.card_eid	
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'trmisc' as tbl_name, p.id, p.loc_n, p.tr_seq, 0 as grp, 0 as des, 0 as seq, 
			0 as fs, p.ttp, /* gfisf_epay_aid */ 0 as aid, 0 as mid, 0 as flags, null as trk2, p.amt, null as org_dir, keys, 
			                          null as card_id, null as tpbc, null as sc, null as remval, null as impval, null as f_use_f, null as restored_f
	 from trmisc as p
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
			
union			
			
select tr.loc_n
	  ,tr.id
	  ,tr.tr_seq
	  ,tr.ts
	  ,tr.type
	  ,tr.bus
	  ,tr.route
	  ,tr.run
	  ,tr.trip
	  ,tr.dir
	  ,tr.n
	  ,tr.longitude
	  ,tr.latitude
	  ,tr.stop_name
	  ,tr.fs
	  ,tr.drv
	  ,u.tbl_name
	  ,u.grp
	  ,u.des
	  ,u.ttp
	  ,u.aid
	  ,u.mid
	  ,u.flags
	  ,u.trk2
	  ,u.amt
	  ,et.text
	  ,u.org_dir
	  ,media.description
	  ,media.m_ndx
	  ,m.tday
	  ,u.keys
	  ,u.seq
	  ,u.card_id
	  ,u.tpbc
	  ,u.sc
	  ,u.remval
	  ,u.impval
	  ,u.f_use_f
	  ,u.restored_f
	  ,m.fbx_n 
from tr
inner join et
        on et.type = tr.type
inner join ml as m
        on m.id = tr.id
	   and m.loc_n = tr.loc_n
left join
(	 select 'mbl' as tbl_name, p.id, p.loc_n, p.tr_seq, p.grp, p.des, 0 as seq, 
			p.fs_id as fs, p.ttp, p.aid, p.mid, 0 as flags, null as trk2, p.deduction as amt, null as org_dir, null as keys, 
			p.barcode as card_id, null as tpbc, null as sc, null as remval, null as impval, null as f_use_f, null as restored_f
	 from gfi_epay_mobile_tickets as p	-- GD-2561
) as u on tr.id = u.id 
	   and tr.loc_n = u.loc_n 
	   and tr.tr_seq = u.tr_seq
join cnf
	on tr.loc_n = cnf.loc_n
left join media
       on media.loc_n = tr.loc_n
       and media.fs_id = cnf.fs_id
	   and ( ( media.grp = u.grp
			  and media.des = u.des
			  and u.grp >0
				)
				or
			( media.m_ndx = u.ttp
			  and u.grp = 0
				)
			)
;
go


if exists(select 1 from sys.sysprocedure where proc_name='updtnotify') then
   drop proc updtnotify;                          // obsolete
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='mklocview') then
   drop proc mklocview;                           // obsolete
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='sp_getstatus') then
   drop proc sp_getstatus;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_getstatus') then
   drop proc gfisp_getstatus;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='sp_linkcrev') then
   drop proc sp_linkcrev;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_linkcrev') then
   drop proc gfisp_linkcrev;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='sp_updtsum') then
   drop proc sp_updtsum;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_updtsum') then
   drop proc gfisp_updtsum;
end if;
go

if exists(select 1 from sys.systrigger where lower(trigger_name)='tdb_vnd_list') then
   drop trigger tdb_vnd_list;
end if;
go

if exists(select 1 from sys.systrigger where lower(trigger_name)='tub_vnd_list') then
   drop trigger tub_vnd_list;
end if;
go

if exists(select 1 from sys.systrigger where lower(trigger_name)='tib_vnd_list') then
   drop trigger tib_vnd_list;
end if;
go

////////////////////////////////////////////////////////////////////////
// fssync - synchronize fare structures across locations by transit authority
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fssync') then
   drop proc fssync;
end if;
go

create procedure dba.fssync()
begin
   declare ls_tab varchar(32);
   declare ls_aut varchar(128);
   declare ls_sql varchar(2000);
   declare ls     varchar(512);
   declare li     smallint;
   declare li_aut smallint;
   declare li_loc smallint;

   select list(aut_n order by aut_n)||',' into ls_aut from aut;
   while ls_aut<>'' loop                          // loop through transit authorities
      set li=locate(ls_aut,',');                  // find comma separator
      set li_aut=cast(left(ls_aut,li - 1) as smallint);
      set ls_aut=substr(ls_aut,li+1);

      select count(*) into li from aut_cnf where aut_n=li_aut;
      if li>1 then                                // if transit authority has more than one locations, perform synchronization from the first location to the rest within the transit authority
         set li_loc=0;
         select min(loc_n) into li_loc from fsc where loc_n in (select loc_n from aut_cnf where aut_n=li_aut);
         if li_loc>0 then                         // delete fare structure settings except for the first location
            delete from farecell     where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);
            delete from media        where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);
            delete from fareset      where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);
            delete from transfers    where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);
            delete from holidays     where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);
            delete from fsc          where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);
            delete from media_track2 where loc_n in (select loc_n from aut_cnf where aut_n=li_aut and loc_n<>li_loc);

            set ls='fsc,holidays,transfers,fareset,media,farecell,media_track2,';
            while ls<>'' loop                     // loop through fare structure tables and copy settings from first location to the rest
               set li=locate(ls,',');
               set ls_tab=left(ls,li - 1);
               set ls=substr(ls,li+1);

               select list(cname order by colno) into ls_sql from sys.syscolumns where tname=ls_tab;
               set ls_sql='insert into '||ls_tab||' ('||ls_sql||') '||
                          'select '||replace(ls_sql,'loc_n,','c.loc_n,')||
                         ' from '||ls_tab||' t, aut_cnf c where t.loc_n='||li_loc||' and c.aut_n='||li_aut||' and c.loc_n<>'||li_loc;
               execute immediate ls_sql;
            end loop;

            commit;
         end if;
      end if;
   end loop;

   delete from rdr;                               // regenerate rdr table
   insert into rdr (loc_n, rdr_id, farecell_n, description, farecell_id, included_f)
      select loc_n, fs_id, farecell_n, description, farecell_id, included_f from farecell where fareset_n=1;

   commit;
end;
go

grant execute on fssync to public;
go

////////////////////////////////////////////////////////////////////////
// updtgs - calculate selected totals in general summary (gs) table
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='updtgs') then
   drop proc updtgs;
end if;
go

create procedure dba.updtgs(in ad1 date,
                            in ad2 date,
                            in loc smallint default 0,
                            in typ smallint default 0,
                            in fst smallint default 2,
                            in as1 varchar(20) default null,
                            in as2 varchar(20) default null)
begin
   // to test : call updtgs('2015-01-01', '2015-02-01');
   //
   // LG-1005 - updtrsf generates error for large fare structure
   // ad1, ad2 - transit date range
   // loc - location number; null or 0 for all locations (default)
   // fst - 0: fareset 0; 1: fareset 1-10; anything else: 0-10 (default)
   // typ, as1, as2 - not used; left for backward compatibility
   declare ls_loc varchar(128);
   declare li     smallint;
   declare li_loc smallint;
   declare ls     varchar(8000);

	// GD-2370 - Add option settings to run updtgs and updtrsf stored procedures
	if exists	(	select null from configurationsData
					where	configurationID = 0
						and	dataName = 'RUN_UPDTGS'
						and	dataValue = 'No'
						and	isActive = 1
				)	then
		return;
	end if;
   
   set ad1=isnull(ad1,current date);              // use today for null date arguments
   set ad2=isnull(ad2,current date);
   if ad2<ad1 then                                // if d2 is earlier than d1, set d2 to d1
      set ad2=ad1;
   end if;

   set fst=isnull(fst,2);
   if fst<>0 and fst<>1 then
      set fst=2;                                  // default to calculate all faresets
   end if;

   select list(loc_n)||',' into ls_loc from cnf where loc_n=(if isnull(loc,0)=0 then loc_n else loc endif);
   while ls_loc<>'' loop                          // all locations or the input location
      set li=locate(ls_loc,',');
      set li_loc=cast(left(ls_loc,li - 1) as smallint);
      set ls_loc=substr(ls_loc,li+1);

	  // LG-1005 - used fnttpstr2 instead of fnttpstr
	  //           added use of ml_extd
      set ls=fnttpstr2('',3,li_loc);               // ml.pass_c is not a pass count formula based count; use the formula to calculate
      set ls='update gs set '||
             'bus_c=isnull((select count(distinct bus) from ml where loc_n=gs.loc_n and tday=gs.tday),0),'||
             'token_t=isnull((if fs=0 then (select sum(token_c) from ml where loc_n=gs.loc_n and tday=gs.tday) '||
                             'else (select sum(ev.token_c) from ml key join ev where ml.loc_n=gs.loc_n and ml.tday=gs.tday and ev.fs=gs.fs) endif),0),'||
             'ticket_t=isnull((if fs=0 then (select sum(ticket_c) from ml where loc_n=gs.loc_n and tday=gs.tday) '||
                              'else (select sum(ev.ticket_c) from ml key join ev where ml.loc_n=gs.loc_n and ml.tday=gs.tday and ev.fs=gs.fs) endif),0),'||
             
             'pass_t=isnull((if fs=0 then (select sum('||ls||') from ml left join ml_extd as mlx on mlx.loc_n = ml.loc_n and mlx.id = ml.id where ml.loc_n=gs.loc_n and ml.tday=gs.tday) '||
             
                            'else (select sum(ev.pass_c) from ml key join ev where ml.loc_n=gs.loc_n and ml.tday=gs.tday and ev.fs=gs.fs) endif),0),'||
             'rdr_c=isnull((if fs=0 then (select sum(rdr_c) from ml where loc_n=gs.loc_n and tday=gs.tday) '||
                           'else (select sum(ev.rdr_c) from ml key join ev where ml.loc_n=gs.loc_n and ml.tday=gs.tday and ev.fs=gs.fs) endif),0) '||
             'where loc_n=li_loc and tday between ad1 and ad2 '||
               'and fs=(case fst when 0 then 0 when 2 then fs else (if fs=0 then -1 else fs endif) end)';

      execute immediate ls;
      commit;
   end loop;
end;
go

grant execute on updtgs to public;
go

////////////////////////////////////////////////////////////////////////
// updtrsf - generate daily and monthly route summary records
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='updtrsf') then
   drop proc updtrsf;
end if;
go

create procedure dba.updtrsf(in ad1 date,
                             in ad2 date,
                             in loc smallint default 0,
                             in an1 smallint default null,
                             in an2 smallint default null,
                             in as1 varchar(20) default null,
                             in as2 varchar(20) default null)
////////////////////////////////////////////////////////////////////////
//
// Procedure updtrsf(ad1,ad2,loc,an1,an2,as1,as2)
//
// Description: Generate route summary data for rtesum and mrtesum tables
//
// Argument: ad1 - start date
//           ad2 - end date
//           loc - location number; NULL or 0 for all locations (default)
//           an1, an2, as1, as2 - unused; for backward compatibility only
//
// Revision:	07/07/1999 Tony Hart - initial version
//				07/07/1999 Maryann Jordan - add fare counts for masterlist totals (-1) and location
//				07/13/2000 Cathy Chu - add year check for monthly report
//				10/25/2000 Cathy Chu - add location check for 'OTHERS'
//				02/09/2005 Mason Liu - rewrite for simplicity and performance (ASA9 and up)
//				04/21/2008 Mason Liu - replace ml.pass_c with pass formula and expose negative unknowns
//				LG-1005 - updtrsf generates error for large fare structure
//				10/26/2015 Igor Mikhalyev - LG-928 mux - deadlock detected
//
////////////////////////////////////////////////////////////////////////
// Copyright(c)2008 GFI Genfare. All rights reserved.
////////////////////////////////////////////////////////////////////////
begin
   // to test : call updtrsf('2015-01-01', '2015-02-01');
   declare ls_loc varchar(128);
   declare li     smallint;
   declare li_loc smallint;
   declare ls     varchar(8000);

	// GD-2370 - Add option settings to run updtgs and updtrsf stored procedures
	if exists	(	select null from configurationsData
					where	configurationID = 0
						and	dataName = 'RUN_UPDTRSF'
						and	dataValue = 'No'
						and	isActive = 1
				)	then
		return;
	end if;

   set loc=isnull(loc,0);

   set ad1=isnull(ad1,current date);              // use today for null date arguments
   set ad2=isnull(ad2,current date);
   if ad2<ad1 then                                // if d2 is earlier than d1, set d2 to d1
      set ad2=ad1;
   end if;

   // purge existing daily route summary data
   delete from rtesum      where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2;
   delete from rtesum_extd where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2;

   // generate route summary records from ev table (special handling fare1_c - fare10_c columns)
   select list('sum('||(if left(cname,4)='fare' and length(cname)>6 then '(if fs='||substr(replace(cname,'_c',''),5)||
               ' then ev.fare_c else 0 endif)' else 'ev.'||cname endif)||')' order by colno)
   into ls from sys.syscolumns where tname='rtesum' and colno>3;

   execute immediate 'insert into rtesum select ml.loc_n,tday,route,'||ls||
                    ' from ml key join ev where ml.loc_n=(if loc=0 then ml.loc_n else loc endif) and tday between ad1 and ad2'||
                    ' and route>=0 and route<1000000'||
					' group by ml.loc_n, tday, route';
                    
   select list('isnull(sum('||(if left(cname,4)='fare' and length(cname)>6 then '(if fs='||substr(replace(cname,'_c',''),5)||
               ' then ev.fare_c else 0 endif)' else 'evx.'||cname endif)||'),0)' order by colno)
   into ls from sys.syscolumns where tname='rtesum_extd' and colno>3;

   execute immediate 'insert into rtesum_extd select ml.loc_n,tday,route,'||ls||
                     ' from ml key join ev'||
                     ' left join ev_extd as evx on evx.loc_n = ev.loc_n and evx.id = ev.id and evx.seq = ev.seq' ||
                     ' where ml.loc_n=(if loc=0 then ml.loc_n else loc endif) and tday between ad1 and ad2'||
                     ' and route>=0 and route<1000000 group by ml.loc_n, tday, route';

   // generate OTHER (route= -2) records for undefined routes from rtesum table
   select list('sum('||cname||')' order by colno) into ls from sys.syscolumns where tname='rtesum' and colno>3;

   execute immediate 'insert into rtesum select loc_n,tday, -2,'||ls||
                    ' from rtesum where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2'||
                    ' and route not in (select route from rtelst where loc_n=rtesum.loc_n)'||
                    ' group by loc_n, tday';

   select list('isnull(sum('||cname||'),0)' order by colno) into ls from sys.syscolumns where tname='rtesum_extd' and colno>3;

   execute immediate 'insert into rtesum_extd select loc_n,tday, -2,'||ls||
                    ' from rtesum_extd where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2'||
                    ' and route not in (select route from rtelst where loc_n=rtesum_extd.loc_n)'||
                    ' group by loc_n, tday';

   delete from rtesum where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2
      and route not in (select route from rtelst where loc_n=rtesum.loc_n) and route<> -2;

   delete from rtesum_extd where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2
      and route not in (select route from rtelst where loc_n=rtesum_extd.loc_n) and route<> -2;

   // generate TOTAL (route=-1) records from ml table (set negative value for UNKNOWN calculation, will convert to positive)
   select list(loc_n)||',' into ls_loc from cnf where loc_n=(if loc=0 then loc_n else loc endif);
   while ls_loc<>'' loop                          // one location at a time
      set li=locate(ls_loc,',');
      set li_loc=cast(left(ls_loc,li - 1) as smallint);
      set ls_loc=substr(ls_loc,li+1);

      // get fare1_c - fare10_c from rtesum table itself
      select list((case when left(cname,4)='fare' and length(cname)>6 then
                           '(select -isnull(sum('||cname||'),0) from rtesum where loc_n=ml.loc_n and tday=ml.tday and route between 0 and 1000000)'
                        when cname='pass_c' then '-sum('||fnttpstr2('',3,li_loc)||')'
                        else '-sum('||cname||')' end) order by colno)
      into ls from sys.syscolumns where tname='rtesum' and colno>3;

      execute immediate 'insert into rtesum select ml.loc_n,ml.tday, -1,'||ls||
						' from ml'||
						' left join ml_extd as mlx on mlx.loc_n = ml.loc_n and mlx.id = ml.id' +
						' where ml.loc_n=li_loc and ml.tday between ad1 and ad2 group by ml.loc_n, ml.tday';
                       
      select list('isnull(-sum('||cname||'),0)')
      into ls from sys.syscolumns where tname='rtesum_extd' and colno>3;

      execute immediate 'insert into rtesum_extd select ml.loc_n,ml.tday, -1,'||ls||
                        ' from ml' ||
                        ' left join ml_extd as mlx on mlx.loc_n = ml.loc_n and mlx.id = ml.id' ||
                        ' where ml.loc_n=li_loc and tday between ad1 and ad2 group by ml.loc_n, ml.tday';
  
   end loop;

   // generate UNKNOWN (route= -3) records from rtesum table (ML - EV)
   select list('-sum('||cname||')' order by colno) into ls from sys.syscolumns where tname='rtesum' and colno>3;

   execute immediate 'insert into rtesum select loc_n,tday, -3,'||ls||
                    ' from rtesum where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2 group by loc_n, tday';

   select list('isnull(-sum('||cname||'),0)' order by colno) into ls from sys.syscolumns where tname='rtesum_extd' and colno>3;

   execute immediate 'insert into rtesum_extd select loc_n,tday, -3,'||ls||
                    ' from rtesum_extd where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2 group by loc_n, tday';

   // change negative values in TOTAL records to positive
   select list(cname||'=abs('||cname||')') into ls from sys.syscolumns where tname='rtesum' and colno>3;


   execute immediate 'update rtesum set '||ls||' where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2 and (route= -1 or route= -3) ';

   select list(cname||'=isnull(abs('||cname||'),0)') into ls from sys.syscolumns where tname='rtesum_extd' and colno>3;

   execute immediate 'update rtesum_extd set '||ls||' where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2 and (route= -1 or route= -3) ';

   // purge existing monthly route summary data
   set ad1=ymd(year(ad1),month(ad1),1);
   set ad2=ymd(year(ad2),month(ad2),1)+32;
   set ad2=ymd(year(ad2),month(ad2),1) - 1;
   
	// LG-928
	if loc = 0 then
		select list(loc_n)||',' into ls_loc from cnf;
		
		while ls_loc <> '' loop
			set li = locate(ls_loc, ',');
			set li_loc = cast(left(ls_loc, li - 1) as smallint);
			set ls_loc = substr(ls_loc, li + 1);

			delete from mrtesum where loc_n = li_loc and mday between ad1 and ad2;
			delete from mrtesum_extd where loc_n = li_loc and mday between ad1 and ad2;
		end loop;
	else
		delete from mrtesum where loc_n = loc and mday between ad1 and ad2;
		delete from mrtesum_extd where loc_n = loc and mday between ad1 and ad2;
	end if;

   // generate mrtesum records from rtesum table
   select list('sum('||cname||')' order by colno) into ls from sys.syscolumns where tname='rtesum' and colno>3;

   execute immediate 'insert into mrtesum select loc_n,ymd(year(tday),month(tday),1) td,route,'||ls||
                    ' from rtesum where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2'||
                    ' group by loc_n, td, route';

   select list('isnull(sum('||cname||'),0)' order by colno) into ls from sys.syscolumns where tname='mrtesum_extd' and colno>3;

   execute immediate 'insert into mrtesum_extd select loc_n,ymd(year(tday),month(tday),1) td,route,'||ls||
                    ' from rtesum_extd where loc_n=(if loc=0 then loc_n else loc endif) and tday between ad1 and ad2'||
                    ' group by loc_n, td, route';

   commit;
end;
go

grant execute on updtrsf to public;
go

// convention change: special route numbers (1000000 and over) are replaced by negative numbers
update mrtesum set route= -2 where route=1000000 and (not exists(select 0 from mrtesum where route= -2));
update mrtesum set route= -3 where route=1000001 and (not exists(select 0 from mrtesum where route= -3));
update rtesum set route= -2 where route=1000000 and (not exists(select 0 from rtesum where route= -2));
update rtesum set route= -3 where route=1000001 and (not exists(select 0 from rtesum where route= -3));
commit;
go

////////////////////////////////////////////////////////////////////////
// gfisp_addtype - add a list type to gfi_lst table
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_addtype') then
   drop proc gfisp_addtype;
end if;
go

create procedure dba.gfisp_addtype(in list_type varchar(16), in list_desc varchar(4000))
begin
   set list_type=upper(trim(list_type));
   set list_desc=trim(list_desc);

   if list_type<>'' and list_desc<>'' then        // if type or description is empty, ignore the request
      if list_type<>'TYPE' then                   // make sure the master type record is created; use dynamic SQL because compiler does not support recursive call
         execute('gfisp_addtype ''TYPE'',''List type definitions (class: 0; code: ID; name: list type code (upper case); value: description)''');
      end if;

      if exists(select 0 from gfi_lst where type='TYPE' and name=list_type) then
         // if found, make sure the description is up to date
         if not exists(select 0 from gfi_lst where type='TYPE' and name=list_type and value=list_desc) then
            update gfi_lst set value=list_desc where type='TYPE' and name=list_type;
            commit;
         end if;
      else                                        // if not found, add it
         insert into gfi_lst (type,class,code,name,value)
         select 'TYPE',0,isnull(max(code),0)+1,list_type,list_desc from gfi_lst where type='TYPE';
         commit;
      end if;
   end if;
end;
go

grant execute on gfisp_addtype to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_deltype - delete a list type from gfi_lst table
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_deltype') then
   drop proc gfisp_deltype;
end if;
go

create procedure dba.gfisp_deltype(in list_type varchar(16))
begin
   set list_type=upper(trim(list_type));

   if exists(select 0 from gfi_lst where type=list_type or (type='TYPE' and name=list_type)) then
      delete from gfi_lst where type=list_type or (type='TYPE' and name=list_type);
      commit;
   end if;
end;
go

grant execute on gfisp_deltype to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_additem - add a list item to gfi_lst table
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_additem') then
   drop proc gfisp_additem;
end if;
go

create procedure dba.gfisp_additem(in list_type  varchar(16),
                                   in list_name  varchar(256),
                                   in list_class smallint default 0,
                                   in list_code  integer default null,
                                   in list_value varchar(4000) default null)
begin
   declare li_over_name  bit;
   declare li_over_value bit;

   set list_type=upper(trim(list_type));

   if list_name like '|%' then                    // if name starts with |, turn on override flag
      set li_over_name=1;
      set list_name=substr(list_name,2,256);
   end if;

   if list_type<>'' and trim(list_name)<>'' then  // if type or name is empty, ignore the request
      set list_class=isnull(list_class,0);

      if list_value like '|%' then                // if value starts with |, turn on override flag
         set li_over_value=1;
         set list_value=substr(list_value,2,4000);
      end if;

      if list_code is null then                   // code is not provided, search by name
         if exists(select 0 from gfi_lst where type=list_type and class=list_class and name=list_name) then
            if li_over_value=1 and (not exists(select 0 from gfi_lst where type=list_type and class=list_class and name=list_name and isnull(value,'')=list_value)) then
               update gfi_lst set value=list_value where type=list_type and class=list_class and name=list_name;
               commit;                            // update value column if value mismatches
            end if;
         else                                     // if not found, add new
            insert into gfi_lst (type,class,code,name,value)
            select list_type,list_class,isnull(max(code),0)+1,list_name,list_value from gfi_lst where type=list_type and class=list_class;
            commit;
         end if;
      elseif exists(select 0 from gfi_lst where type=list_type and class=list_class and code=list_code) then
         if li_over_name=1 and (not exists(select 0 from gfi_lst where type=list_type and class=list_class and code=list_code and name=list_name)) then
            update gfi_lst set name=list_name where type=list_type and class=list_class and code=list_code;
            commit;                               // update name column if name mismatches
         end if;

         if li_over_value=1 and (not exists(select 0 from gfi_lst where type=list_type and class=list_class and code=list_code and isnull(value,'')=list_value)) then
            update gfi_lst set value=list_value where type=list_type and class=list_class and code=list_code;
            commit;                               // update value column if value mismatches
         end if;
      else                                        // if not found, add new
         insert into gfi_lst (type,class,code,name,value) values(list_type,list_class,list_code,list_name,list_value);
         commit;
      end if;
   end if;
end;
go

grant execute on gfisp_additem to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_delitem - delete one or more list items from gfi_lst table
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_delitem') then
   drop proc gfisp_delitem;
end if;
go

create procedure dba.gfisp_delitem(in list_type  varchar(16),
                                   in list_name  varchar(256) default null,
                                   in list_class smallint default null,
                                   in list_code  integer default null)
begin
   set list_type=upper(trim(list_type));

   if list_type<>'' then                          // if type is empty, ignore the request
      if trim(list_name)<>'' then
         if list_class is null then
            if list_code is null then
               if exists(select 0 from gfi_lst where type=list_type and name=list_name) then
                  delete from gfi_lst where type=list_type and name=list_name;
                  commit;                         // delete on name
               end if;
            elseif exists(select 0 from gfi_lst where type=list_type and code=list_code and name=list_name) then
               delete from gfi_lst where type=list_type and code=list_code and name=list_name;
               commit;                            // delete on code and name
            end if;
         else
            if list_code is null then
               if exists(select 0 from gfi_lst where type=list_type and class=list_class and name=list_name) then
                  delete from gfi_lst where type=list_type and class=list_class and name=list_name;
                  commit;                         // delete on class and name
               end if;
            elseif exists(select 0 from gfi_lst where type=list_type and class=list_class and code=list_code and name=list_name) then
               delete from gfi_lst where type=list_type and class=list_class and code=list_code and name=list_name;
               commit;                            // delete on class, code, and name
            end if;
         end if;
      else
         if list_class is null then
            if list_code is null then             // if name, class, and code are nulls, delete the type; use dynamic SQL because compiler does not support recursive call
               call gfisp_deltype(list_type);
            elseif exists(select 0 from gfi_lst where type=list_type and code=list_code) then
               delete from gfi_lst where type=list_type and code=list_code;
               commit;                            // delete on code
            end if;
         else
            if list_code is null then
               if exists(select 0 from gfi_lst where type=list_type and class=list_class) then
                  delete from gfi_lst where type=list_type and class=list_class;
                  commit;                         // delete on class
               end if;
            elseif exists(select 0 from gfi_lst where type=list_type and class=list_class and code=list_code) then
               delete from gfi_lst where type=list_type and class=list_class and code=list_code;
               commit;                            // delete on class and code
            end if;
         end if;
      end if;
   end if;
end;
go

grant execute on gfisp_delitem to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_addevt - add an event/transaction type to gfi_lst table
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_addevt') then
   drop proc gfisp_addevt;
end if;
go

create procedure dba.gfisp_addevt(in list_class smallint,
                                  in list_code  integer,
                                  in list_name  varchar(256),
                                  in list_value varchar(4000) default null)
begin
   if trim(list_name)<>'' and list_code>=0 then   // name and code are required
      if not exists(select 0 from gfi_lst where type='EV TYPE' and code=list_code) then
         insert into gfi_lst (type,class,code,name,value) values('EV TYPE',isnull(list_class,0),list_code,list_name,list_value);
         commit;
      end if;
   end if;
end;
go

grant execute on gfisp_addevt to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_delfs - delete farestructure
//               loc - farestructure location number (delete applies to all other locations in the same transit authority)
//               fsid - farestructure ID; null or 0 to delete all
//               b_commit - control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_delfs') then
   drop proc gfisp_delfs;
end if;
go

create procedure dba.gfisp_delfs(in loc smallint, in fsid tinyint default null, in b_commit bit default 1)
begin
   if loc>0 then
      if fsid>0 then
         delete from rdr          where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and rdr_id=fsid;
         delete from farecell     where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;
         delete from media_track2 where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;
         delete from media        where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;
         delete from fareset      where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;
         delete from transfers    where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;
         delete from holidays     where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;
         delete from fsc          where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and fs_id=fsid;

         if b_commit>0 then
            commit;
         end if;
      else
         raiserror 20010 'The value of the farestructure ID argument is invalid';
      end if;
   else
      raiserror 20012 'The value of the location number argument is invalid';
   end if;
end;
go

grant EXECUTE on gfisp_delfs to PUBLIC;
go

////////////////////////////////////////////////////////////////////////
// gfisp_addfs - add a new farestructure
//               loc/fsid - farestructure location number and ID
//               loc_fr/fsid_fr - copy from farestructure location number and ID
//               b_commit - control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
//	
//              LG-839  - DS v.59 Error while opening Daily Summary Report after connecting it to the cloud NM 
//				LG-1507 - Merge the SQL code for cloud and non cloud branches
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_addfs') then
   drop proc gfisp_addfs;
end if;
go

create procedure dba.gfisp_addfs(in loc smallint, in fsid tinyint, in loc_fr smallint default null, in fsid_fr tinyint default null, in b_commit bit default 1)
begin
   declare li     smallint;
   declare li_fc  smallint;
   declare ls     varchar(128);
   declare ls_sql varchar(2000);
   declare ls_tab varchar(32);
   declare ls_col varchar(1024);
   declare ls_loc varchar(256);
   declare li_fs_number int;
   declare li_ttp_number int;

   set li_fs_number = gfisf_GetFaresetNumber();
   set li_ttp_number = gfisf_TTP_Number();

   if loc>0 and loc<=676 then
      if fsid>0 and fsid<=255 then
         if exists(select 0 from fsc where loc_n=loc and fs_id=fsid) then
            return;                               // if farestructure already exists, exit
         end if;
      else
         raiserror 20012 'The value of the farestructure ID argument is invalid';
      end if;
   else
      raiserror 20014 'The value of the location number argument is invalid';
   end if;

   if not exists(select 0 from aut) then          // if no transit authority has been defined, add one
      insert into aut (aut_n, aut_name) values(1, 'Transit authority 1');
   end if;
                                                  // if location does not exists, add one
   if not exists(select 0 from cnf where loc_n=loc) then
      insert into cnf (loc_n, loc_name) values(loc, 'Location '||loc);
      insert into aut_cnf select min(aut_n), loc from aut;
   end if;
                                                  // copy fare structure
   if exists(select 0 from fsc where loc_n=loc_fr and fs_id=fsid_fr) then
      if fsid=fsid_fr then                        // empty the farestructure slot for all locations belonging to the same transit authorities of loc
         delete from rdr          where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and rdr_id=fsid;
         delete from farecell     where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;
         delete from media_track2 where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;
         delete from media        where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;
         delete from fareset      where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;
         delete from transfers    where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;
         delete from holidays     where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;
         delete from fsc          where (loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc))) and loc_n<>loc_fr and fs_id=fsid;

         set ls_loc='(cnf.loc_n='||loc||' or cnf.loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n='||loc||'))) and cnf.loc_n<>'||loc_fr;
      else
         call gfisp_delfs(loc, fsid, 0);
         set ls_loc='(cnf.loc_n='||loc||' or cnf.loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n='||loc||')))';
      end if;

      set ls='fsc,fareset,holidays,media,media_track2,transfers,farecell,';
      while ls<>'' loop
         set li=locate(ls,',');
         set ls_tab=substr(ls,1,li - 1);
         set ls=substr(ls,li+1);

         if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
            select list(cname order by colno) into ls_col from sys.syscolumns where tname=ls_tab;
            set ls_sql=replace(replace(replace('fs.'||ls_col,',',',fs.'),'fs.loc_n,','cnf.loc_n,'),'fs.fs_id,',fsid||',');
            set ls_sql='insert into '||ls_tab||' ('||ls_col||') select '||ls_sql||' from '||ls_tab||' fs, cnf where fs.loc_n='||loc_fr||' and fs.fs_id='||fsid_fr||' and '||ls_loc;
            execute immediate ls_sql;
         end if;
      end loop;
   else                                           // create a new fare structure from scratch
      call gfisp_delfs(loc, fsid, 0);             // empty the farestructure slot for all locations belonging to the same transit authorities of loc

      insert into fsc (loc_n, fs_id, description, lockcode, peak1on,peak1off,peak2on,peak2off, modified_userid, created_userid, text)
      select loc_n,fsid,'Farestructure '||fsid,' ','00:00:00','00:00:00','00:00:00','00:00:00','gfi','gfi','FS '||fsid
      from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));

      set li=1;
      while li <= li_fs_number loop
         insert into fareset (loc_n, fs_id, fareset_n, description, enabled_f) select loc_n,fsid,li,'Fareset '||li,'N'
         from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));
         set li=li+1;
      end loop;

      insert into media (loc_n, fs_id, m_ndx, grp, des, description, text, enabled_f) select loc_n,fsid,0,0,0,'Undefined','Undef','N'
      from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));
      set li=1;
      while li <= li_ttp_Number loop
         insert into media (loc_n, fs_id, m_ndx, grp, des, description, text, enabled_f) select loc_n,fsid,li,0,0,' ',' ','N'
         from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));
         set li=li+1;
      end loop;

      insert into transfers select loc_n,fsid,0,0,0,0,0,0,'Undefined','Undef','N','N','N','N','N','N','N','N'
      from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));
      set li=1;                                   // 15 transfers
      while li<=15 loop
         insert into transfers select loc_n,fsid,li,0,0,0,0,0,' ',' ','N','N','N','N','N','N','N','N'
         from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));
         set li=li+1;
      end loop;

      set li=1;
      while li <= li_fs_number loop
         set li_fc=0;
         while li_fc <= li_ttp_number + 14 loop
            set ls=(case when li_fc=0 then 'Preset' when li_fc<10 then 'Key '||li_fc when li_fc=10 then 'Key *' when li_fc=11 then 'Key A'
                         when li_fc=12 then 'Key B' when li_fc=13 then 'Key C' when li_fc=14 then 'Key D' else 'TTP '||(li_fc - 14) end);
            insert into farecell (loc_n,fs_id,fareset_n,farecell_n,value,description,farecell_id,enabled_f,included_f)
            select loc_n,fsid,li,li_fc,0,ls,ls,'N','N'
            from cnf where loc_n=loc or loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc));
            set li_fc=li_fc+1;
         end loop;
         set li=li+1;
      end loop;
   end if;

   insert into rdr (loc_n, rdr_id, farecell_n, description, farecell_id, included_f)
      select cnf.loc_n, fsid, farecell_n, description, farecell_id, included_f
      from farecell, cnf where farecell.loc_n=loc and farecell.fs_id=fsid and fareset_n=1
      and (cnf.loc_n=loc or cnf.loc_n in (select loc_n from aut_cnf where aut_n in (select aut_n from aut_cnf where loc_n=loc)));

   if b_commit>0 then
      commit;
   end if;
end;
go

grant execute on gfisp_addfs to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_delloc - delete a location
//                use b_commit argument to control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_delloc') then
   drop proc gfisp_delloc;
end if;
go

create procedure dba.gfisp_delloc(in loc smallint, in b_commit bit default 1)
begin
   declare li     smallint;
   declare ls     varchar(2000);
   declare ls_tab varchar(64);

   if exists(select 0 from cnf where loc_n=loc) then
      set ls='ACT_LOG,ACT_LOG2,ACT_SAM,ACT_EF,ACT_EC,ACT_SALE,ACT_VAL,BUSLST_DAILY,'||
             'CC_CLR,RTR,MTD,MTFD,'||
             'VLT,VS,GFI_VLT_PRB_TOTAL,PRB_LOG,TRIM_DIAG,EQLST,GFI_ML,EV,FIRMWARE_INFO,TRMISC,SCD,CCD,SVD,SRD,PPD,TRD,GFI_TR_SCARD,TR,ML,'||
             'MRBILL,MRCOIN,MRMEMO,MRSUM,RDR,FARECELL,TRANSFERS,MEDIA_TRACK2,MEDIA,HOLIDAYS,FARESET,FSC,'||
             'GS,RTESUM,MRTESUM,GFI_PV_RECON,'||
             'GFI_RANGE,DRVLST,BUSLST,RTELST,RUNLST,TRPLST,RRTLST,AUT_CNF,CNF,';
      while ls<>'' loop
         set li=locate(ls,',');
         set ls_tab=lower(substr(ls,1,li - 1));
         set ls=substr(ls,li+1);

         if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
            execute immediate 'delete from '||ls_tab||' where loc_n='||loc;
         end if;
      end loop;

      delete from gfi_lst where type='CS STATUS' and code=loc;

      if exists(select 1 from sys.systable where table_name='security_app' and upper(table_type)='BASE') then
         execute immediate 'delete from security_setting where application=''location'' and item='''||loc||'''';
         execute immediate 'delete from security_def where application=''location'' and item='''||loc||'''';
      end if;

      if b_commit>0 then
         commit;
      end if;
   end if;
end;
go

grant EXECUTE on gfisp_delloc to PUBLIC;
go

////////////////////////////////////////////////////////////////////////
// gfisp_addloc - add a new location; copy from loc_fr if provided or create from scratch
//                use b_commit argument to control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_addloc') then
   drop proc gfisp_addloc;
end if;
go

create procedure dba.gfisp_addloc(in loc smallint, in loc_fr smallint default null, in b_commit bit default 1)
begin
   declare li     smallint;
   declare li_fs  smallint;
   declare ls     varchar(64);
   declare ls_sql varchar(1024);
   declare ls_tab varchar(64);

   if loc>0 and loc<=676 then                     // if location already exists, exit
      if exists(select 0 from cnf where loc_n=loc) then
         return;
      end if;
   else
      raiserror 20010 'The value of the location number argument is invalid';
   end if;

   if exists(select 0 from gfi_range where loc_n=loc) then
      delete from gfi_range where loc_n=loc;
   end if;

   if not exists(select 0 from aut) then          // if no transit authority has been defined, add one
      insert into aut (aut_n, aut_name) values(1, 'Transit authority 1');
   end if;

   set ls_tab='Location '||loc;

   if exists(select 0 from cnf where loc_n=loc_fr) then // "copy from" location exists
      select list(cname order by colno) into ls_sql from sys.syscolumns where tname='cnf' and colno>2;
      execute immediate 'insert into cnf (loc_n,loc_name,'||ls_sql||') select '||loc||', '''||ls_tab||''', '||ls_sql||' from cnf where loc_n='||loc_fr;
                                                  // assign the new location to a transit authority
      if exists(select 0 from aut_cnf where loc_n=loc_fr) then
         insert into aut_cnf (aut_n, loc_n) select aut_n, loc from aut_cnf where loc_n=loc_fr;
      else
         insert into aut_cnf (aut_n, loc_n) select min(aut_n), loc from aut;
      end if;
                                                  // copy list items to new location
      set ls='gfi_range,drvlst,buslst,rtelst,runlst,trplst,buslst_daily,';
      while ls<>'' loop
         set li=locate(ls,',');
         set ls_tab=substr(ls,1,li - 1);
         set ls=substr(ls,li+1);

         if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
            select list(cname order by colno) into ls_sql from sys.syscolumns where tname=ls_tab;
            set ls_sql='insert into '||ls_tab||' ('||ls_sql||') select '||replace(ls_sql,'loc_n,',loc||',')||' from '||ls_tab||' where loc_n='||loc_fr;
            execute immediate ls_sql;
         end if;
      end loop;
   else                                           // create a new location from scratch
      insert into cnf (loc_n, loc_name) values(loc, ls_tab);
      insert into aut_cnf select min(aut_n), loc from aut;
   end if;
                                                  // copy fare structure to new location
   if exists(select 0 from fsc where loc_n=loc_fr) then
      select list(fs_id)||',' into ls from fsc where loc_n=loc_fr;
      while ls<>'' loop
         set li=locate(ls,',');
         set li_fs=cast(substr(ls,1,li - 1) as smallint);
         set ls=substr(ls,li+1);

         call gfisp_addfs(loc, li_fs, loc_fr, li_fs, 0);
      end loop;
   else                                           // create a new fare structure from scratch
      call gfisp_addfs(loc, 1, null, null, 0);
   end if;

   if b_commit>0 then
      commit;
   end if;
end;
go

grant EXECUTE on gfisp_addloc to PUBLIC;
go

////////////////////////////////////////////////////////////////////////
// gfisp_purge_mlid - delete data on location and ml.id
//                    loc - location number
//                    mlid1, mlid2 - ml.id range to delete; if both null, error; if mlid2 is null, delete mlid1 - current; if mlid1 is null, delete from earliest to mlid2
//                    b_commit - control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
//
//		GD-2507 - Delete ridership data when purging ML record
//		2017/3/30     RK - Commented the section where records are deleted when one of ml ids is not supplied, as it deletes valid records.
//                    @commit - control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_purge_mlid') then
   drop proc gfisp_purge_mlid;
end if;
go

create procedure dba.gfisp_purge_mlid(in loc smallint, mlid1 integer default null, mlid2 integer default null, in b_commit bit default 1)
begin

   declare li     smallint;
   declare ls     varchar(256);
   declare ls_tab varchar(32);
   declare del_date  date; 

   if exists(select 0 from cnf where loc_n=loc) then
		if mlid1 is null and mlid2 is null then
			raiserror 20000 'The ml.id range to delete is missing.';
		else
			delete from vlt where loc_n=loc and id between mlid1 and mlid2 and type<4;
		end if;

         set ls='prb_log,ridership,ev_extd,ev,act_log,act_sam,act_ef,act_ec,act_sale,act_val,cc_clr,trim_diag,mtd,mtfd,trmisc,scd,ccd,svd,srd,ppd,trd,rtr,gfi_tr_scard,tr,gfi_ml,ml_extd,ml,';
         while ls<>'' loop
            set li=locate(ls,',');
            set ls_tab=substr(ls,1,li - 1);
            set ls=substr(ls,li+1);

            if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
                execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and id between '||mlid1||' and '||mlid2;
            end if;
         end loop;

--DataBuild - 101 added to update the gs table after deleting form ML table.
select  cast (ts as date) into del_date from ml where id = mlid1 and loc_n = loc;

call updtgs (del_date,del_date);

call updtrsf (del_date,del_date);

		 if b_commit>0 then
			commit;
		 end if;
      
   end if;
end;
go

grant execute on gfisp_purge_mlid to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_purge_tday - delete data on location and ml.tday
//                    loc - location number
//                    tday1, tday2 - transit day range to delete; if both null, error; if tday2 is null, delete tday1 - current; if tday1 is null, delete from earliest to tday2
//                    b_commit - control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
//
//		GD-2507 - Delete ridership data when purging ML record
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_purge_tday') then
   drop proc gfisp_purge_tday;
end if;
go

create procedure dba.gfisp_purge_tday(in loc smallint, tday1 date default null, tday2 date default null, in b_commit bit default 1)
begin
// call gfisp_purge_tday (1, '1900-01-01', '1900-01-01', 1);
   declare li     smallint;
   declare ls     varchar(256);
   declare ls_tab varchar(32);

   if exists(select 0 from cnf where loc_n=loc) then
      if tday1 is null and tday2 is null then
         raiserror 20000 'The transit day range to delete is missing.';
      else
         set ls='prb_log,vlt,ridership,ev_extd,ev,act_log,act_sam,act_ef,act_ec,act_sale,act_val,cc_clr,trim_diag,mtd,mtfd,trmisc,scd,ccd,svd,srd,ppd,trd,rtr,gfi_tr_scard,tr,gfi_ml,ml_extd,';
         while ls<>'' loop
            set li=locate(ls,',');
            set ls_tab=substr(ls,1,li - 1);
            set ls=substr(ls,li+1);

            if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
               if tday1 is null then
                  execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and id in (select id from ml where loc_n='||loc||' and tday<='''||tday2||''')';
               elseif tday2 is null then
                  execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and id in (select id from ml where loc_n='||loc||' and tday>='''||tday1||''')';
               else
                  execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and id in (select id from ml where loc_n='||loc||' and tday between '''||tday1||''' and '''||tday2||''')';
               end if;
            end if;
         end loop;

         set ls='mrbill,mrcoin,mrmemo,vlt,ml,';
         while ls<>'' loop
            set li=locate(ls,',');
            set ls_tab=substr(ls,1,li - 1);
            set ls=substr(ls,li+1);

            if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
               if tday1 is null then
                  execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and tday<='''||tday2||'''';
               elseif tday2 is null then
                  execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and tday>='''||tday1||'''';
               else
                  execute immediate 'delete from '||ls_tab||' where loc_n='||loc||' and tday between '''||tday1||''' and '''||tday2||'''';
               end if;
            end if;
         end loop;

         if b_commit>0 then
            commit;
         end if;
      end if;
   end if;
end;
go

grant EXECUTE on gfisp_purge_tday to PUBLIC;
go

////////////////////////////////////////////////////////////////////////
// gfisp_move - move data to a new ml.id block
//                   loc_fr, loc_to - from and to location numbers
//                   id_fr1, id_fr2 - ml.id range to move; if both are nulls, move all records for the location; if id_fr1 is null, move <=id_fr2; if id_fr2 is null, move >=id_fr1
//                   id_to1 - destination starting ml.id; if null, ml.ids stay the same
//                   b_commit - control whether the procedure should COMMIT at the end (>0 - yes; 0 - no)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_move') then
   drop proc gfisp_move;
end if;
go

create procedure dba.gfisp_move(loc_fr smallint, loc_to smallint, id_fr1 integer default null, id_fr2 integer default null, id_to1 integer default null, in b_commit bit default 1)
begin
   declare li     smallint;
   declare ll1    integer;
   declare ll2    integer;
   declare ll3    integer;
   declare ll     integer;
   declare ll_id  integer;                        // starting ml.id to move to
   declare ls     varchar(4000);
   declare ls_tab varchar(64);

   if loc_fr>0 then                               // from location number must be positive
      if not exists(select 0 from ml where loc_n=loc_fr) then
         return;
      end if;

      if id_fr1 is null and id_fr2 is null then   // if ml.id range is not specified (move the whole location), retrieve the ml.id range from database
         select min(id), max(id) into id_fr1, id_fr2 from ml where loc_n=loc_fr;
      elseif id_fr1 is null then
         if exists(select 0 from ml where loc_n=loc_fr and id<=id_fr2) then
            select min(id) into id_fr1 from ml where loc_n=loc_fr and id<=id_fr2;
         else
            return;
         end if;
      elseif id_fr2 is null then
         if exists(select 0 from ml where loc_n=loc_fr and id>=id_fr1) then
            select max(id) into id_fr2 from ml where loc_n=loc_fr and id>=id_fr1;
         else
            return;
         end if;
      else
         if exists(select 0 from ml where loc_n=loc_fr and id between id_fr1 and id_fr2) then
            select min(id), max(id) into id_fr1, id_fr2 from ml where loc_n=loc_fr and id between id_fr1 and id_fr2;
         else
            return;
         end if;
      end if;
   else
      raiserror 20010 'The value of the from location number argument is invalid';
   end if;

   if loc_to>0 then                               // to location number must be positive
      if id_to1 is null then                      // store destination starting ml.id to variable ll_id
         set ll_id=id_fr1;
      else
         set ll_id=id_to1;
      end if;

      if loc_fr=loc_to and ll_id=id_fr1 then
         raiserror 20012 'No new location number and new starting ml.id are specified (no destination to move)';
      end if;

      set ll=2147483647;                          // if the new ml.id block exceeds the ml.id upper limit (signed long), exit
      if ll_id>0 and id_fr2 - id_fr1>ll - ll_id then
         raiserror 20014 'This move will cause ml.id to overflow (maximum ml.id='||ll||')';
      end if;
   else
      raiserror 20016 'The value of the to location number argument is invalid';
   end if;

   set ll=(id_fr2 - id_fr1)+ll_id;                // destination ending ml.id
   if loc_fr=loc_to then                          // handle same location overlapping situations
      if ll_id>id_fr1 and ll_id<=id_fr2 then      // move up with overlap
         if exists(select 0 from ml where loc_n=loc_to and id between (id_fr2+1) and ll) then
                                                  // if destination is not empty, move destination up
            select min(id), max(id), ll+1 into ll1, ll2, ll3 from ml where loc_n=loc_to and id>id_fr2;
            execute('gfisp_move '||loc_to||','||loc_to||','||ll1||','||ll2||','||ll3||',0');
         end if;

         set ll3=ll - id_fr2;                     // move overlapped section, ll3 records at a time
         while id_fr2>=ll_id loop                 // use a loop to move segment by segment instead of a deep recursive call which may cause stack overflow when there are too many segments to move
            set ll1=id_fr2 - ll3+1;
            set ll2=id_fr2+1;
            execute('gfisp_move '||loc_to||','||loc_to||','||ll1||','||id_fr2||','||ll2||',0');
            set id_fr2=ll1 - 1;
         end loop;
         execute('gfisp_move '||loc_to||','||loc_to||','||id_fr1||','||id_fr2||','||ll_id||',0');

         if b_commit>0 then
            commit;
         end if;
         return;

      elseif ll_id<id_fr1 and id_fr1<=ll then     // move down with overlap
         if exists(select 0 from ml where loc_n=loc_to and id between ll_id and (id_fr1 - 1)) then
                                                  // if destination is not empty, move destination down
            select min(id), max(id) into ll1, ll2 from ml where loc_n=loc_to and id<id_fr1;
            if ll_id>=(ll2 - ll1) - 2147483647 then
               set ll3=ll_id - 1 - (ll2 - ll1);
               execute('gfisp_move '||loc_to||','||loc_to||','||ll1||','||ll2||','||ll3||',0');
            else
               raiserror 20026 'This move will cause ml.id to overflow (minimum ml.id= -2147483648)';
            end if;
         end if;

         set ll3=id_fr1 - ll_id;                  // move overlapped section, ll3 records at a time
         while id_fr1<=ll loop                    // use a loop to move segment by segment instead of a deep recursive call which may cause stack overflow when there are too many segments to move
            set ll1=id_fr1+ll3 - 1;
            execute('gfisp_move '||loc_to||','||loc_to||','||id_fr1||','||ll1||','||ll_id||',0');
            set ll_id=id_fr1;
            set id_fr1=ll1+1;
         end loop;
         execute('gfisp_move '||loc_to||','||loc_to||','||id_fr1||','||id_fr2||','||ll_id||',0');

         if b_commit>0 then
            commit;
         end if;
         return;

      else
         if exists(select 0 from ml where loc_n=loc_to and id between ll_id and ll) then
            select min(id), max(id), ll+1 into ll1, ll2, ll3 from ml where loc_n=loc_to and id>=ll_id;
            execute('gfisp_move '||loc_to||','||loc_to||','||ll1||','||ll2||','||ll3||',0');
         end if;

      end if;
   else                                           // handle different location overlapping situation
      call gfisp_addloc(loc_to,loc_fr,0);         // make sure the destination location exists
                                                  // check if destination is empty; if not, move destination block to make room
      if exists(select 0 from ml where loc_n=loc_to and id between ll_id and ll) then
         select min(id), max(id), ll+1 into ll1, ll2, ll3 from ml where loc_n=loc_to and id>=ll_id;
         execute('gfisp_move '||loc_to||','||loc_to||','||ll1||','||ll2||','||ll3||',0');
      end if;
                                                  // check if vlt destination is empty; if not, move vlt destination block to make room
      if exists(select 0 from vlt where loc_n=loc_to and seq in ( select seq from vlt where loc_n=loc_fr and type<4 and
        (id in (select id from ml where loc_n=loc_fr and id between id_fr1 and id_fr2) or (id>0 and id between id_fr1 and id_fr2)) )) then
         select min(seq), max(seq) into ll1, ll2 from vlt where loc_n=loc_fr and type<4 and
               (id in (select id from ml where loc_n=loc_fr and id between id_fr1 and id_fr2) or (id>0 and id between id_fr1 and id_fr2));
         set ll3=ll2 - ll1+1;
         update vlt set seq=seq+ll3 where loc_n=loc_to and seq>=ll1;
      end if;

   end if;

   set ll=ll_id - id_fr1;                         // get ml.id increment/decrement value
                                                  // copy ml data to destination
   select list(cname order by colno) into ls from sys.syscolumns where tname='ml' and colno>2;
   set ls='insert into ml (loc_n,id,'||ls||') select '||loc_to||', id+('||ll||'), '||ls||' from ml where loc_n='||loc_fr||' and id between '||id_fr1||' and '||id_fr2;
   execute immediate ls;
                                                  // copy tr data to destination
   select list(cname order by colno) into ls from sys.syscolumns where tname='tr' and colno>2;
   set ls='insert into tr (loc_n,id,'||ls||') select '||loc_to||', id+('||ll||'), '||ls||' from tr where loc_n='||loc_fr||' and id between '||id_fr1||' and '||id_fr2;
   execute immediate ls;

   update vlt set loc_n=loc_to, id=id+(ll) where loc_n=loc_fr and type<4 and
   (id in (select id from ml where loc_n=loc_fr and id between id_fr1 and id_fr2) or (id>0 and id between id_fr1 and id_fr2));

   set ls='gfi_ml,ev,rtr,trd,ppd,srd,svd,ccd,scd,trmisc,trim_diag,cc_clr,act_val,act_sale,mtd,mtfd,act_ec,act_ef,act_sam,act_log,gfi_tr_scard,prb_log,gfi_epay_act_bus,';
   while ls<>'' loop
      set li=locate(ls,',');
      set ls_tab=substr(ls,1,li - 1);
      set ls=substr(ls,li+1);

      if exists(select 1 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
         execute immediate 'update '||ls_tab||' set loc_n='||loc_to||', id=id+('||ll||') where loc_n='||loc_fr||' and id between '||id_fr1||' and '||id_fr2;
      end if;
   end loop;

   if exists(select 1 from sys.systable where table_name='gfi_epay_autoload_tr' and upper(table_type)='BASE') then
      execute immediate 'update gfi_epay_autoload_tr set loc_n='||loc_to||', id=id+('||ll||') where loc_n='||loc_fr||' and id between '||id_fr1||' and '||id_fr2||' and tr_seq>0 and mid=13';
   end if;

   if exists(select 1 from sys.systable where table_name='gfi_epay_non_autoload_tr' and upper(table_type)='BASE') then
      execute immediate 'update gfi_epay_non_autoload_tr set loc_n='||loc_to||', id=id+('||ll||') where loc_n='||loc_fr||' and id between '||id_fr1||' and '||id_fr2||' and tr_seq>0 and mid=13';
   end if;

   delete from tr where loc_n=loc_fr and id between id_fr1 and id_fr2;
   delete from ml where loc_n=loc_fr and id between id_fr1 and id_fr2;

   if b_commit>0 then
      commit;
   end if;
end;
go

grant EXECUTE on gfisp_move to PUBLIC;
go

////////////////////////////////////////////////////////////////////////
// gfisp_seed - create dummy ml and/or vlt records
//              loc - location numbers
//              mlid - ml.id to create
//              vltseq - vlt.seq to create
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_seed') then
   drop proc gfisp_seed;
end if;
go

create procedure dba.gfisp_seed(in loc smallint, mlid integer default null, vltseq integer default null)
begin
   if loc>0 then                               // from location number must be positive
      if not exists(select 0 from cnf where loc_n=loc) then
         raiserror 20010 'Location '||loc||' does not exist';
      elseif mlid is null and vltseq is null then  // if both seeds are null, error
         raiserror 20012 'Neither ml.id seed nor vlt.seq seed is specified';
      else
         if mlid>=0 or mlid<0 then
            if exists(select 0 from ml where loc_n=loc and id=mlid) then
               raiserror 20014 'The ml.id '||mlid||' already exists';
            else
               insert into ml (loc_n,id,tday) values(loc,mlid,CURRENT DATE);
            end if;
         end if;

         if vltseq>=0 or vltseq<0 then
            if exists(select 0 from vlt where loc_n=loc and seq=vltseq) then
               raiserror 20016,'The vlt.seq '||vltseq||' already exists';
            else
               insert into vlt (loc_n,seq,probed,inserted,removed,bus,revenue,cbxid,cbxsn,binid,binsn,tday,type,probe_n,vault_n,
                                dime,penny,nickel,quarter,half,sba,one,two,five,ten,twenty,token,ticket,outalm,inalm,probe_f,vault_f,remove_f,null_f,
                                birec,brrec,abrec,verec,startup,logoff,nobin,memclr,cbxalm,bypalm,dooralm,timedsc,pdu,evntovr,fschange,
                                revdsc,coinovr,billovr,passovr,coinerrovr,passerrovr,coldovr,vererr,cbxvlt,notprb)
               values(loc,vltseq,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0,0,0,0,0,0,CURRENT DATE,
                      1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            end if;
         end if;

         commit;
      end if;
   else
      raiserror 20018 'The value of the location number argument is invalid';
   end if;
end;
go

grant EXECUTE on gfisp_seed to PUBLIC;
go

////////////////////////////////////////////////////////////////////////
// gfisp_getlargefare - New report for large fare structure  LG-826
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_getlargefare') then
   drop proc gfisp_getlargefare;
end if;
go

create procedure dba.gfisp_getlargefare
	(	in Loc smallint,
		in FareStructure tinyint,
		in Fareset tinyint default NULL )
begin

// Find effective date of current fare structure

declare CurrentEffectiveDate datetime;

set CurrentEffectiveDate = (	select MAX(effective_ts)
								from fsc
								where loc_n = Loc
									and fs_id = FareStructure
									and effective_ts < GETDATE()
							);
							
// Query for Keys:

	select
		fc.farecell_n Num,
		f.description Fare_Structure_Description,
		f.effective_ts Effective_Date,
		f.alt_key Alt_Key,
		fs.description Fareset_Description,
		fs.enabled_f Fareset_Enabled,
		case
			when fc.farecell_n = 0 then 'Preset'
			when fc.farecell_n between 1 and 9 then 'Key ' + CAST(farecell_n as varchar(1))
			when fc.farecell_n = 10 then 'Key *'
			when fc.farecell_n = 11 then 'Key A'
			when fc.farecell_n = 12 then 'Key B'
			when fc.farecell_n = 13 then 'Key C'
			when fc.farecell_n = 14 then 'Key D'
		end Key_TTP,
		fc.farecell_id Key_TTP_Text, 
		fc.description Key_TTP_Description,
		fc.Value,
		fc.enabled_f Key_TTP_Enabled,
		cast(fc.attr as varchar(4)) + ' - ' + isnull(a.text,'') +' ' + isnull(a.description,'')  Attribute,
		fc.t_enabled_f Transfer_Enabled,
		cast(isnull(fc.t_ndx,0) as varchar(4)) + ' - ' + isnull(ltrim(rtrim(t.text)),'') +' ' + isnull(t.description,'') Transfer,
		fc.m_enabled_f Media_Enabled,
		'' Media_Group,
		0 Designation,
		'N' Peak,
		'N' Off_Peak,
		'N' WeekDays,
		'N' Saturday,
		'N' Sunday,
		'N' Holidays,
		'N' Friendly,
		case 
			when f.effective_ts < CurrentEffectiveDate then 'Past'
			when f.effective_ts = CurrentEffectiveDate then 'Current'
			when f.effective_ts > CurrentEffectiveDate then 'Future'
		end Status,
		fs.fareset_n Fareset_Number,
		cnf.loc_name Location_Name,
		case isnull(fc.led1, 0)
			when 0 then '0 - No LED'
			when 1 then '1 - Red'
			when 2 then '2 - Yellow'
			when 4 then '4 - Green'
			else CAST(fc.led1 as varchar(4)) + ' - Unknown'
		end LED1,	
		case isnull(fc.led2, 0)
			when 0 then '0 - No LED'
			when 1 then '1 - Red'
			when 2 then '2 - Yellow'
			when 4 then '4 - Green'
			else CAST(fc.led2 as varchar(4)) + ' - Unknown'
		end LED2	
	from fsc f
	join fareset fs
		on f.loc_n = fs.loc_n
		and f.fs_id = fs.fs_id
	join farecell fc
		on f.loc_n = fc.loc_n
		and f.fs_id = fc.fs_id
		and fs.fareset_n = fc.fareset_n
		and fc.farecell_n between 0 and 14
	join cnf	
		on f.loc_n = cnf.loc_n
	left join attr a
		on fc.attr = a.attr
	left join transfers t
		on f.loc_n = t.loc_n
		and f.fs_id = t.fs_id
		and fc.t_ndx = t.t_ndx
	where fs.loc_n = Loc
		and fs.fs_id = FareStructure
		and (	fs.fareset_n = isnull(Fareset, fs.fareset_n)
				or Fareset = 0 )

	union all

	// Query for TTPs:

	select 
		m.m_ndx + 14 Num,
		f.description Fare_Structure_Description,
		f.effective_ts Effective_Date,
		f.alt_key Alt_Key,
		fs.description Fareset_Description,
		fs.enabled_f Fareset_Enabled,
		'TTP ' + cast(m.m_ndx as varchar(4)) Key_TTP,
		isnull(fc.farecell_id, m.text) Key_TTP_Text, 
		isnull(fc.description, m.text) Key_TTP_Description,
		isnull(fc.value, 0.00) Value,
		isnull(fc.enabled_f, 'N') Key_TTP_Enabled,
		cast(isnull(fc.attr,0) as varchar(4)) + ' - ' + isnull(a.text,'') +' ' + isnull(a.description,'') Attribute,
		isnull(fc.t_enabled_f, 'N') Transfer_Enabled,
		cast(isnull(fc.t_ndx,0) as varchar(4)) + ' - ' + isnull(ltrim(rtrim(t.text)),'') +' ' + isnull(t.description,'') Transfer,
		m.enabled_f Media_Enabled,
		case m.grp 
			when 0 then '0 - Transfer'
			when 1 then '1 - Period Pass'
			when 2 then '2 - Stored Ride'
			when 3 then '3 - Stored Value'
			when 4 then '4 - Credit Card/Special Card'
			when 5 then '5 - Token/Ticket'
			when 6 then '6 - Alt Key'
			when 7 then '7 - Maintenance/Employee Card'
			else CAST(m.grp as varchar(4))
		end Media_Group,
		m.des Designation,
		m.peak_f Peak,
		m.offpeak_f Off_Peak,
		m.wkd_f WeekDays,
		m.sat_f Saturday,
		m.sun_f Sunday,
		m.hol_f Holidays,
		m.friendly_f Friendly,
		case 
			when f.effective_ts < CurrentEffectiveDate then 'Past'
			when f.effective_ts = CurrentEffectiveDate then 'Current'
			when f.effective_ts > CurrentEffectiveDate then 'Future'
		end Status,
		fs.fareset_n Fareset_Number,
		cnf.loc_name Location_Name,		
		case isnull(fc.led1, 0)
			when 0 then '0 - No LED'
			when 1 then '1 - Red'
			when 2 then '2 - Yellow'
			when 4 then '4 - Green'
			else CAST(fc.led1 as varchar(4)) + ' - Unknown'
		end LED1,	
		case isnull(fc.led2, 0)
			when 0 then '0 - No LED'
			when 1 then '1 - Red'
			when 2 then '2 - Yellow'
			when 4 then '4 - Green'
			else CAST(fc.led2 as varchar(4)) + ' - Unknown'
		end LED2	
	from fsc f
	join fareset fs
		on f.loc_n = fs.loc_n
		and f.fs_id = fs.fs_id
	join media m
		on f.loc_n = m.loc_n
		and f.fs_id = m.fs_id
		and m.m_ndx <> 0
	join cnf	
		on f.loc_n = cnf.loc_n
	left join farecell fc
		on f.loc_n = fc.loc_n
		and f.fs_id = fc.fs_id
		and fs.fareset_n = fc.fareset_n
		and fc.farecell_n - 14 = m.m_ndx 
	left join attr a
		on isnull(fc.attr,0) = a.attr
	left join transfers t
		on f.loc_n = t.loc_n
		and f.fs_id = t.fs_id
		and isnull(fc.t_ndx,0) = t.t_ndx
	where fs.loc_n = Loc
		and fs.fs_id = FareStructure
		and (	fs.fareset_n = isnull(Fareset, fs.fareset_n)
				or Fareset = 0 );
end;
go

grant execute on gfisp_getlargefare to public;
go


// LG-1478
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_rpt_transaction_detail') then
   drop proc gfisp_rpt_transaction_detail;
end if;
go

create procedure dba.gfisp_rpt_transaction_detail
(	in_TransitDay		VARCHAR(1) default '1',
	in_FromDate			DATETIME,
	in_ToDate			DATETIME,
	in_PassType			VARCHAR(8000) default '0',
	in_TransType		VARCHAR(8000) default '0',
	in_Bus				VARCHAR(8000) default '0',
	in_Route			VARCHAR(8000) default '0',
	in_Run				VARCHAR(8000) default '0',
	in_Trip				VARCHAR(8000) default '0',
	in_CardId			VARCHAR(8000) default '',
	in_Trk2				VARCHAR(8000) default '',
	in_FromLongitude	VARCHAR(8000) default NULL,
	in_ToLongitude 		VARCHAR(8000) default NULL,
	in_FromLatitude 	VARCHAR(8000) default NULL,
	in_ToLatitude 		VARCHAR(8000) default NULL,
	in_FromStops		VARCHAR(8000) default NULL,
	in_ToStops 			VARCHAR(8000) default NULL
)
BEGIN
-- =============================================
-- Author:		Richard B. Becker
-- Create date: 08/01/2016
-- Description:	gfisp_rpt_transaction_detail
-- JIRA		  : GD-2267, GD-2268, GD-2269, GD-2270, GD-2276
-- =============================================
-- call gfisp_rpt_transaction_detail('1', '2016-10-03', '2016-10-03');
--DATARUN-343/Sruthi M/Modified the where clause - increased varchar size to 3

	declare stringvar varchar(20);
	declare ind int;

	set in_PassType			= isnull(in_PassType, '0');
	set in_TransType		= isnull(in_TransType, '0');
	set in_Bus				= isnull(in_Bus, '0');
	set in_Route			= isnull(in_Route, '0');
	set in_Run				= isnull(in_Run, '0');
	set in_Trip				= isnull(in_Trip, '0');
	set in_CardId			= isnull(in_CardId, '');
	set in_Trk2				= isnull(in_Trk2, '');
	
	create table #PassType (PassType int);

	if(in_PassType <> '0')
	then
		set ind = charindex(',',in_PassType);
		while ind > 0
		loop
			  set stringvar = substring(in_PassType,1,ind-1);
			  set in_PassType = substring(in_PassType,ind+1,length(in_PassType)-ind);
			  insert into #PassType values (stringvar);
			  set ind = charindex(',',in_PassType);
		end loop;
		set stringvar = in_PassType;
		insert into #PassType values (stringvar);
	end if;	

	create table #TransType (TransType int);

	if (in_TransType <> '0')
	then
		set ind = charindex(',',in_TransType);
		while ind > 0
		loop
			  set stringvar = substring(in_TransType,1,ind-1);
			  set in_TransType = substring(in_TransType,ind+1,length(in_TransType)-ind);
			  insert into #TransType values (stringvar);
			  set ind = charindex(',',in_TransType);
		end loop;
		set stringvar = in_TransType;
		insert into #TransType values (stringvar);
	end if;

	create table #Bus (Bus int);

	if(in_Bus <> '0')
	then
		set ind = charindex(',',in_Bus);
		while ind > 0
		loop
			  set stringvar = substring(in_Bus,1,ind-1);
			  set in_Bus = substring(in_Bus,ind+1,length(in_Bus)-ind);
			  insert into #Bus values (stringvar);
			  set ind = charindex(',',in_Bus);
		end loop;
		set stringvar = in_Bus;
		insert into #Bus values (stringvar);
	end if;	
	
	create table #Route (Route int);

	if(in_Route <> '0')
	then
		set ind = charindex(',',in_Route);
		while ind > 0
		loop
			  set stringvar = substring(in_Route,1,ind-1);
			  set in_Route = substring(in_Route,ind+1,length(in_Route)-ind);
			  insert into #Route values (stringvar);
			  set ind = charindex(',',in_Route);
		end loop;
		set stringvar = in_Route;
		insert into #Route values (stringvar);
	end if;	

	create table #Run (Run int);

	if(in_Run <> '0')
	then
		set ind = charindex(',',in_Run);
		while ind > 0
		loop
			  set stringvar = substring(in_Run,1,ind-1);
			  set in_Run = substring(in_Run,ind+1,length(in_Run)-ind);
			  insert into #Run values (stringvar);
			  set ind = charindex(',',in_Run);
		end loop;
		set stringvar = in_Run;
		insert into #Run values (stringvar);
	end if;	

	create table #Trip (Trip int);

	if(in_Trip <> '0')
	then
		set ind = charindex(',',in_Trip);
		while ind > 0
		loop
			  set stringvar = substring(in_Trip,1,ind-1);
			  set in_Trip = substring(in_Trip,ind+1,length(in_Trip)-ind);
			  insert into #Trip values (stringvar);
			  set ind = charindex(',',in_Trip);
		end loop;
		set stringvar = in_Trip;
		insert into #Trip values (stringvar);
	end if;	

	create table #CardId (CardId int);

	if(in_CardId <> '')
	then
		set ind = charindex(',',in_CardId);
		while ind > 0
		loop
			  set stringvar = substring(in_CardId,1,ind-1);
			  set in_CardId = substring(in_CardId,ind+1,length(in_CardId)-ind);
			  insert into #CardId values (stringvar);
			  set ind = charindex(',',in_CardId);
		end loop;
		set stringvar = in_CardId;
		insert into #CardId values (stringvar);
	end if;	

	create table #Trk2 (Trk2 int);

	if(in_Trk2 <> '')
	then
		set ind = charindex(',',in_Trk2);
		while ind > 0
		loop
			  set stringvar = substring(in_Trk2,1,ind-1);
			  set in_Trk2 = substring(in_Trk2,ind+1,length(in_Trk2)-ind);
			  insert into #Trk2 values (stringvar);
			  set ind = charindex(',',in_Trk2);
		end loop;
		set stringvar = in_Trk2;
		insert into #Trk2 values (stringvar);
	end if;	

---------------------------------------------------------New code 8/25/2017
--select id,tday into #id from  ml where tday between in_FromDate and in_ToDate ;
create table #id(id bigint,tday date,loc_n int,FBX_N int );

		insert into #id
		select id,tday,loc_n,FBX_N  from  ml where tday between in_FromDate and in_ToDate; 

		CREATE CLUSTERED INDEX IX_TestTable_TestCol1   
		ON #id (id) WITH FILLFACTOR = 100;

select
		'ppd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		p.mid,
		p.flags,
		null as trk2,
		0 as amt,
		null as org_dir,
		null as keys,
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id, 
		tpbc,
		sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f,null as orig_bus INTO #subquery
	from dba.ppd as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate  
	left join dba.gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
		
			
	union all
	 
	select
		'svd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		p.mid,
		p.flags,
		null as trk2,
		deduction as amt,
		null as org_dir,
		null as keys,
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id, 
		null as tpbc,
		null as sc,
		remval,
		impval,
		f_use_f,
		restored_f,null as orig_bus
	from dba.svd as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate 
	left join dba.gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'srd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		0 as mid,
		p.flags,
		null as trk2,
		0 as amt,
		null as org_dir,
		null as keys, 
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id,
		null as tpbc,
		null as sc,
		remval,
		impval,
		f_use_f,
		restored_f,null as orig_bus
	from dba.srd as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate 
	left join dba.gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'scd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		0 as seq,
		p.fs,
		p.ttp,
		'' as aid,
		0 as mid,
		0 as flags,
		p.trk2,
		0 as amt,
		null as prg_dir,
		null as keys,
		null as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f,null as orig_bus
	from dba.scd as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate  
	 
	union all
	 
	select
		'trd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		p.mid,
		p.flags,
		null as trk2,
		0 as amt,
		org_dir,
		null as keys,
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		impval,
		null as f_use_f,
		restored_f,trim_n as orig_bus
	from dba.trd as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate 
	left join dba.gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'ccd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		0 as seq,
		p.fs,
		p.ttp,
		--dba.gfisf_epay_aid() ,
		''as aid,
		0 as mid,
		0 as flags,
		p.trk2,
		0 as amt,
		null as org_dir,
		null as keys,
		x.card_pid as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f,null as orig_bus
	from dba.ccd as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate 
	left join dba.gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'trmisc' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		0 as grp,
		0 as des,
		0 as seq,
		0 as fs,
		p.ttp,
		--dba.gfisf_epay_aid() ,
		''as aid,
		0 as mid,
		0 as flags,
		null as trk2,
		p.amt,
		null as org_dir,
		keys, 
		cast(p.card_id as varchar(25)) as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f,null as orig_bus
	from dba.trmisc as p join #id m on m.id=p.id --and tday between @FromDate and @ToDate 
	 
	union all
	 
	select
		'mbl' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		0 as seq,
		p.fs_id as fs,
		p.ttp,
		p.aid,
		p.mid,
		0 as flags,
		null as trk2,
		p.deduction as amt,
		null as org_dir,
		null as keys, 
		p.barcode as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f,null as orig_bus
	from dba.gfi_epay_mobile_tickets as p  join #id m on m.id=p.id;	-- GD-2561
	
	--CREATE CLUSTERED INDEX IX_subquery_TestCol1   
 --   ON tempsubquery (id,LOC_N,TR_SEQ) WITH FILLFACTOR = 100;
select
	tr.loc_n,
	tr.id,
	tr.tr_seq,
	tr.ts,
	tr.type,
	tr.bus,
	tr.route,
	tr.run,
	tr.trip,
	tr.dir,
	tr.n,
	tr.longitude,
	tr.latitude,
	tr.stop_name,
	tr.fs,
	tr.drv,
	u.tbl_name,
	u.grp,
	u.des,
	u.ttp,
	u.aid,
	u.mid,
	u.flags,
	u.trk2,
	u.amt,
	et.text,
	u.org_dir,
	media.description,
	media.m_ndx,
	m.tday,
	u.keys,
	u.seq,
	u.card_id,
	u.tpbc,
	u.sc,
	u.remval,
	u.impval,
	u.f_use_f,
	u.restored_f,
	m.fbx_n,u.orig_bus,
	fsc.description as fsc_desc,
	media.text as media_text,
	media.fs_id ,cast((RIGHT('0' + CAST(isnull(u.grp,0) as varchar(1)), 1) + RIGHT('00' + CAST(isnull(u.des,0) as varchar(2)), 2)) as int) Passtype1 ,
	cast(u.grp as varchar(1)) + cast(u.des as varchar(2)) passtype2--,farecell_id
	into #Alltransdata
--SELECT *
from 
--(select id,loc_n,fbx_n,tday  from ml where tday between @FromDate and @ToDate ) 
#id as m
inner join dba.tr
on	m.id = tr.id
	and	m.loc_n = tr.loc_n --and m.tday between '4/1/2017' and '4/2/2017'
	inner join dba.et
	on	et.type = tr.type 
inner join dba.cnf
	on	tr.loc_n = cnf.loc_n
left join
(select * from #subquery 
	) as u	on	tr.id = u.id 
		and tr.loc_n = u.loc_n 
		and tr.tr_seq = u.tr_seq
left join dba.fsc
	on	fsc.loc_n = cnf.loc_n
	and fsc.fs_id = cnf.fs_id
left join dba.media
	on	media.loc_n = cnf.loc_n
	and media.fs_id = cnf.fs_id
	and(
			media.grp = u.grp and media.des = u.des and (u.grp <> 0 or u.des <>0 )
		or
			media.m_ndx = u.ttp and m_ndx <>0
		);
		
CREATE NONCLUSTERED INDEX IX_TestTable_TestCol1   
ON #Alltransdata (passtype1) WITH FILLFACTOR = 100;	

CREATE NONCLUSTERED INDEX IX_TestTable_TestColpt 
ON #Alltransdata (passtype2) WITH FILLFACTOR = 100;	
    
	SELECT DISTINCT cast(media.grp as varchar(1)) + cast(media.des as varchar(2)) mediatype  into #medialist from media
	WHERE
	--( media.loc_n = 1 ) AND 
	( media.fs_id = ( SELECT Max(fs_id) 
	FROM media 
	)) and (media.grp <> 0 OR 
		media.des <> 0 or media.m_ndx <>0) ;
							
							--and cast(media.grp as varchar(1)) + cast(media.des as varchar(2))
		
		SELECT
				atd.loc_n as Location,
				atd.id as Transaction_Id,
				atd.tr_seq as Transaction_Sequence,
				atd.ts as Timestamp,
				atd.type as	Transaction_Type,
				atd.Bus,
				atd.Route,
				atd.Run,
				atd.Trip,
				atd.dir as Direction,
				atd.n,
				atd.longitude,
				atd.latitude,
				atd.stop_name,
				atd.fs as Fareset,
				atd.drv as Driver,
				atd.tbl_name as Tablename,
				atd.Grp,
				atd.des as Designator,
				--atd.ttp,					-- GD-2276
				case atd.type
					when 142 then '0'
					when 143 then 'ttp' + cast(isnull(atd.n,'0') AS varchar(100)) // Raj - New case included for Datarun 40
					// else atd.ttp    -- Raj - Commented this and included for Datarun 40 to convert all the value into VARCHAR
					else cast(atd.ttp AS varchar(100))			
				end as TTP,
				atd.aid,
				atd.mid,
				atd.Flags,
				atd.trk2 as Track2,
				atd.amt as Amount,
				atd.text,
				atd.org_dir,
				case when atd.keys = 'DMP' then 'DUMP' 
						else (case 	when rdr.farecell_id is null and atd.m_ndx <> 0 then atd.description
							else rdr.farecell_id
							end)
					end as Description,
				case atd.m_ndx 
						when 0 then NULL
						else atd.m_ndx
					end as MNDX,
				atd.tday,
				atd.keys,
				--atd.card_id,
                    case when SUBSTRING (dba.CharReversal(dba.[DecimalToBinary](atd.flags)),10,1)<>'1' and atd.seq is not null then 'Seq #: '+cast(atd.seq as varchar(20)) 
					when SUBSTRING (dba.CharReversal(dba.[DecimalToBinary](atd.flags)),10,1)='1' 
					then (select 'Card ID #: ' + cast(ppd.card_eid as varchar(20))   from ppd where ppd.id = 6271 and ppd.tr_seq = atd.tr_seq 
					union
					select 'Card ID #: ' + cast(card_eid as varchar(20))   from svd where svd.id = atd.id and svd.tr_seq = atd.tr_seq 
					union
					select 'Card ID #: ' + cast(card_eid as varchar(20))   from srd where srd.id = atd.id and srd.tr_seq = atd.tr_seq )  end card_id,
				atd.tpbc,
				atd.sc,
				atd.remval,
				atd.impval,
				atd.f_use_f,
				atd.restored_f,
				atd.fbx_n,atd.orig_bus,
				atd.fsc_desc,
				atd.description as media_desc,
				atd.fs_id FROM #Alltransdata atd
LEFT JOIN rdr		
			ON rdr.loc_n = atd.loc_n 
				and rdr.farecell_n = (select case atd.keys
											when 'PRE' then 0
											when '1' then 1
											when '2' then 2
											when '3' then 3
											when '4' then 4
											when '5' then 5
											when '6' then 6
											when '7' then 7
											when '8' then 8
											when '9' then 9
											when '*' then 10
											when 'A' then 11
											when 'B' then 12
											when 'C' then 13
											when 'D' then 14
											else NULL
											end)
				and rdr.rdr_id = (select max(rdr_id) 
									from rdr 
									where rdr.loc_n = atd.loc_n 
									and rdr.farecell_n = (select case atd.keys
															when 'PRE' then 0
															when '1' then 1
															when '2' then 2
															when '3' then 3
															when '4' then 4
															when '5' then 5
															when '6' then 6
															when '7' then 7
															when '8' then 8
															when '9' then 9
															when '*' then 10
															when 'A' then 11
															when 'B' then 12
															when 'C' then 13
															when 'D' then 14
															else NULL
															end) )
		where
       	atd.ts between in_fromdate and in_todate and(in_Bus = '0' or atd.bus in (select * from #Bus))
			and (in_Route = '0' or atd.route = 0 or atd.route in (select * from #Route))
			and (in_Run = '0' or atd.run = 0 or atd.run in (select * from #Run))
			and (in_Trip = '0' or atd.trip = 0 or atd.trip in (select * from #Trip))
			and (in_TransType = '0' or atd.type in (select * from #TransType))
		and ( ISNULL(in_PassType,'0') <> '0' and passtype1
		in (select * from #PassType)
		or (ISNULL(in_PassType,'0') = '0' 
		and --cast(TR.grp as varchar(1)) + cast(TR.des as varchar(2)) in
		--passtype2 in
		exists
		(select * from #medialist where mediatype  = passtype2  
		)
		))
		and ( in_CardId = '' or atd.card_id  in (select * from #CardId))
			and ( in_Trk2 = '' or atd.trk2 in (select * from #Trk2))
			and (atd.longitude between in_FromLongitude and in_ToLongitude or ISNULL(in_FromLongitude,'') = '' or ISNULL(in_ToLongitude,'') = '' )
			and (atd.latitude between in_FromLatitude and in_ToLatitude or ISNULL(in_FromLatitude,'') = '' or ISNULL(in_ToLatitude,'') = '' )
			and (atd.stop_name between in_FromStops and in_ToStops or ISNULL(in_FromStops,'') = '' or ISNULL(in_ToStops,'') = '' )
		;
END;
go

grant execute on gfisp_rpt_transaction_detail to public;
go

////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_daily_summary_report - LG-841
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_daily_summary_report') then
   drop proc gfisp_get_data_for_daily_summary_report;
end if;
go

create procedure dba.gfisp_get_data_for_daily_summary_report (
	in locations varchar(4000),
	in day datetime )
begin
/*
call gfisp_get_data_for_daily_summary_report('1,2', '2015-09-09');
*/
	declare li_loc_n smallint;
	declare li_index int;

    create table #locations(
        loc_n smallint
    );

	set locations = ltrim(rtrim(isnull(locations, '')));

	while locations <> '' loop
		set li_index = charindex(',', locations);

		if li_index = 0 then
			set li_loc_n = cast(locations as smallint);
			set locations = '';
		else
			set li_loc_n = cast(rtrim(substring(locations, 1, li_index - 1)) as smallint);
			set locations = ltrim(substring(locations, li_index + 1, length(locations)));
		end if;

		insert into #locations values (li_loc_n);
	end loop;

	select gs.fs,
		sum(isnull(gs.fare_c, 0)) as v0,
		sum(isnull(gs.key1, 0))   as v1,
		sum(isnull(gs.key2, 0))   as v2,
		sum(isnull(gs.key3, 0))   as v3,
		sum(isnull(gs.key4, 0))   as v4,
		sum(isnull(gs.key5, 0))   as v5,
		sum(isnull(gs.key6, 0))   as v6,
		sum(isnull(gs.key7, 0))   as v7,
		sum(isnull(gs.key8, 0))   as v8,
		sum(isnull(gs.key9, 0))   as v9,
		sum(isnull(gs.keyast, 0)) as v10,
		sum(isnull(gs.keya, 0))   as v11,
		sum(isnull(gs.keyb, 0))   as v12,
		sum(isnull(gs.keyc, 0))   as v13,
		sum(isnull(gs.keyd, 0))   as v14,
		sum(isnull(gs.ttp1, 0))   as v15,
		sum(isnull(gs.ttp2, 0))   as v16,
		sum(isnull(gs.ttp3, 0))   as v17,
		sum(isnull(gs.ttp4, 0))   as v18,
		sum(isnull(gs.ttp5, 0))   as v19,
		sum(isnull(gs.ttp6, 0))   as v20,
		sum(isnull(gs.ttp7, 0))   as v21,
		sum(isnull(gs.ttp8, 0))   as v22,
		sum(isnull(gs.ttp9, 0))   as v23,
		sum(isnull(gs.ttp10, 0))  as v24,
		sum(isnull(gs.ttp11, 0))  as v25,
		sum(isnull(gs.ttp12, 0))  as v26,
		sum(isnull(gs.ttp13, 0))  as v27,
		sum(isnull(gs.ttp14, 0))  as v28,
		sum(isnull(gs.ttp15, 0))  as v29,
		sum(isnull(gs.ttp16, 0))  as v30,
		sum(isnull(gs.ttp17, 0))  as v31,
		sum(isnull(gs.ttp18, 0))  as v32,
		sum(isnull(gs.ttp19, 0))  as v33,
		sum(isnull(gs.ttp20, 0))  as v34,
		sum(isnull(gs.ttp21, 0))  as v35,
		sum(isnull(gs.ttp22, 0))  as v36,
		sum(isnull(gs.ttp23, 0))  as v37,
		sum(isnull(gs.ttp24, 0))  as v38,
		sum(isnull(gs.ttp25, 0))  as v39,
		sum(isnull(gs.ttp26, 0))  as v40,
		sum(isnull(gs.ttp27, 0))  as v41,
		sum(isnull(gs.ttp28, 0))  as v42,
		sum(isnull(gs.ttp29, 0))  as v43,
		sum(isnull(gs.ttp30, 0))  as v44,
		sum(isnull(gs.ttp31, 0))  as v45,
		sum(isnull(gs.ttp32, 0))  as v46,
		sum(isnull(gs.ttp33, 0))  as v47,
		sum(isnull(gs.ttp34, 0))  as v48,
		sum(isnull(gs.ttp35, 0))  as v49,
		sum(isnull(gs.ttp36, 0))  as v50,
		sum(isnull(gs.ttp37, 0))  as v51,
		sum(isnull(gs.ttp38, 0))  as v52,
		sum(isnull(gs.ttp39, 0))  as v53,
		sum(isnull(gs.ttp40, 0))  as v54,
		sum(isnull(gs.ttp41, 0))  as v55,
		sum(isnull(gs.ttp42, 0))  as v56,
		sum(isnull(gs.ttp43, 0))  as v57,
		sum(isnull(gs.ttp44, 0))  as v58,
		sum(isnull(gs.ttp45, 0))  as v59,
		sum(isnull(gs.ttp46, 0))  as v60,
		sum(isnull(gs.ttp47, 0))  as v61,
		sum(isnull(gs.ttp48, 0))  as v62,
		sum(isnull(x.ttp49, 0))   as v63,
		sum(isnull(x.ttp50, 0))   as v64,
		sum(isnull(x.ttp51, 0))   as v65,
		sum(isnull(x.ttp52, 0))   as v66,
		sum(isnull(x.ttp53, 0))   as v67,
		sum(isnull(x.ttp54, 0))   as v68,
		sum(isnull(x.ttp55, 0))   as v69,
		sum(isnull(x.ttp56, 0))   as v70,
		sum(isnull(x.ttp57, 0))   as v71,
		sum(isnull(x.ttp58, 0))   as v72,
		sum(isnull(x.ttp59, 0))   as v73,
		sum(isnull(x.ttp60, 0))   as v74,
		sum(isnull(x.ttp61, 0))   as v75,
		sum(isnull(x.ttp62, 0))   as v76,
		sum(isnull(x.ttp63, 0))   as v77,
		sum(isnull(x.ttp64, 0))   as v78,
		sum(isnull(x.ttp65, 0))   as v79,
		sum(isnull(x.ttp66, 0))   as v80,
		sum(isnull(x.ttp67, 0))   as v81,
		sum(isnull(x.ttp68, 0))   as v82,
		sum(isnull(x.ttp69, 0))   as v83,
		sum(isnull(x.ttp70, 0))   as v84,
		sum(isnull(x.ttp71, 0))   as v85,
		sum(isnull(x.ttp72, 0))   as v86,
		sum(isnull(x.ttp73, 0))   as v87,
		sum(isnull(x.ttp74, 0))   as v88,
		sum(isnull(x.ttp75, 0))   as v89,
		sum(isnull(x.ttp76, 0))   as v90,
		sum(isnull(x.ttp77, 0))   as v91,
		sum(isnull(x.ttp78, 0))   as v92,
		sum(isnull(x.ttp79, 0))   as v93,
		sum(isnull(x.ttp80, 0))   as v94,
		sum(isnull(x.ttp81, 0))   as v95,
		sum(isnull(x.ttp82, 0))   as v96,
		sum(isnull(x.ttp83, 0))   as v97,
		sum(isnull(x.ttp84, 0))   as v98,
		sum(isnull(x.ttp85, 0))   as v99,
		sum(isnull(x.ttp86, 0))   as v100,
		sum(isnull(x.ttp87, 0))   as v101,
		sum(isnull(x.ttp88, 0))   as v102,
		sum(isnull(x.ttp89, 0))   as v103,
		sum(isnull(x.ttp90, 0))   as v104,
		sum(isnull(x.ttp91, 0))   as v105,
		sum(isnull(x.ttp92, 0))   as v106,
		sum(isnull(x.ttp93, 0))   as v107,
		sum(isnull(x.ttp94, 0))   as v108,
		sum(isnull(x.ttp95, 0))   as v109,
		sum(isnull(x.ttp96, 0))   as v110,
		sum(isnull(x.ttp97, 0))   as v111,
		sum(isnull(x.ttp98, 0))   as v112,
		sum(isnull(x.ttp99, 0))   as v113,
		sum(isnull(x.ttp100, 0))  as v114,
		sum(isnull(x.ttp101, 0))  as v115,
		sum(isnull(x.ttp102, 0))  as v116,
		sum(isnull(x.ttp103, 0))  as v117,
		sum(isnull(x.ttp104, 0))  as v118,
		sum(isnull(x.ttp105, 0))  as v119,
		sum(isnull(x.ttp106, 0))  as v120,
		sum(isnull(x.ttp107, 0))  as v121,
		sum(isnull(x.ttp108, 0))  as v122,
		sum(isnull(x.ttp109, 0))  as v123,
		sum(isnull(x.ttp110, 0))  as v124,
		sum(isnull(x.ttp111, 0))  as v125,
		sum(isnull(x.ttp112, 0))  as v126,
		sum(isnull(x.ttp113, 0))  as v127,
		sum(isnull(x.ttp114, 0))  as v128,
		sum(isnull(x.ttp115, 0))  as v129,
		sum(isnull(x.ttp116, 0))  as v130,
		sum(isnull(x.ttp117, 0))  as v131,
		sum(isnull(x.ttp118, 0))  as v132,
		sum(isnull(x.ttp119, 0))  as v133,
		sum(isnull(x.ttp120, 0))  as v134,
		sum(isnull(x.ttp121, 0))  as v135,
		sum(isnull(x.ttp122, 0))  as v136,
		sum(isnull(x.ttp123, 0))  as v137,
		sum(isnull(x.ttp124, 0))  as v138,
		sum(isnull(x.ttp125, 0))  as v139,
		sum(isnull(x.ttp126, 0))  as v140,
		sum(isnull(x.ttp127, 0))  as v141,
		sum(isnull(x.ttp128, 0))  as v142,
		sum(isnull(x.ttp129, 0))  as v143,
		sum(isnull(x.ttp130, 0))  as v144,
		sum(isnull(x.ttp131, 0))  as v145,
		sum(isnull(x.ttp132, 0))  as v146,
		sum(isnull(x.ttp133, 0))  as v147,
		sum(isnull(x.ttp134, 0))  as v148,
		sum(isnull(x.ttp135, 0))  as v149,
		sum(isnull(x.ttp136, 0))  as v150,
		sum(isnull(x.ttp137, 0))  as v151,
		sum(isnull(x.ttp138, 0))  as v152,
		sum(isnull(x.ttp139, 0))  as v153,
		sum(isnull(x.ttp140, 0))  as v154,
		sum(isnull(x.ttp141, 0))  as v155,
		sum(isnull(x.ttp142, 0))  as v156,
		sum(isnull(x.ttp143, 0))  as v157,
		sum(isnull(x.ttp144, 0))  as v158,
		sum(isnull(x.ttp145, 0))  as v159,
		sum(isnull(x.ttp146, 0))  as v160,
		sum(isnull(x.ttp147, 0))  as v161,
		sum(isnull(x.ttp148, 0))  as v162,
		sum(isnull(x.ttp149, 0))  as v163,
		sum(isnull(x.ttp150, 0))  as v164,
		sum(isnull(x.ttp151, 0))  as v165,
		sum(isnull(x.ttp152, 0))  as v166,
		sum(isnull(x.ttp153, 0))  as v167,
		sum(isnull(x.ttp154, 0))  as v168,
		sum(isnull(x.ttp155, 0))  as v169,
		sum(isnull(x.ttp156, 0))  as v170,
		sum(isnull(x.ttp157, 0))  as v171,
		sum(isnull(x.ttp158, 0))  as v172,
		sum(isnull(x.ttp159, 0))  as v173,
		sum(isnull(x.ttp160, 0))  as v174,
		sum(isnull(x.ttp161, 0))  as v175,
		sum(isnull(x.ttp162, 0))  as v176,
		sum(isnull(x.ttp163, 0))  as v177,
		sum(isnull(x.ttp164, 0))  as v178,
		sum(isnull(x.ttp165, 0))  as v179,
		sum(isnull(x.ttp166, 0))  as v180,
		sum(isnull(x.ttp167, 0))  as v181,
		sum(isnull(x.ttp168, 0))  as v182,
		sum(isnull(x.ttp169, 0))  as v183,
		sum(isnull(x.ttp170, 0))  as v184,
		sum(isnull(x.ttp171, 0))  as v185,
		sum(isnull(x.ttp172, 0))  as v186,
		sum(isnull(x.ttp173, 0))  as v187,
		sum(isnull(x.ttp174, 0))  as v188,
		sum(isnull(x.ttp175, 0))  as v189,
		sum(isnull(x.ttp176, 0))  as v190,
		sum(isnull(x.ttp177, 0))  as v191,
		sum(isnull(x.ttp178, 0))  as v192,
		sum(isnull(x.ttp179, 0))  as v193,
		sum(isnull(x.ttp180, 0))  as v194,
		sum(isnull(x.ttp181, 0))  as v195,
		sum(isnull(x.ttp182, 0))  as v196,
		sum(isnull(x.ttp183, 0))  as v197,
		sum(isnull(x.ttp184, 0))  as v198,
		sum(isnull(x.ttp185, 0))  as v199,
		sum(isnull(x.ttp186, 0))  as v200,
		sum(isnull(x.ttp187, 0))  as v201,
		sum(isnull(x.ttp188, 0))  as v202,
		sum(isnull(x.ttp189, 0))  as v203,
		sum(isnull(x.ttp190, 0))  as v204,
		sum(isnull(x.ttp191, 0))  as v205,
		sum(isnull(x.ttp192, 0))  as v206,
		sum(isnull(x.ttp193, 0))  as v207,
		sum(isnull(x.ttp194, 0))  as v208,
		sum(isnull(x.ttp195, 0))  as v209,
		sum(isnull(x.ttp196, 0))  as v210,
		sum(isnull(x.ttp197, 0))  as v211,
		sum(isnull(x.ttp198, 0))  as v212,
		sum(isnull(x.ttp199, 0))  as v213,
		sum(isnull(x.ttp200, 0))  as v214,
		sum(isnull(x.ttp201, 0))  as v215,
		sum(isnull(x.ttp202, 0))  as v216,
		sum(isnull(x.ttp203, 0))  as v217,
		sum(isnull(x.ttp204, 0))  as v218,
		sum(isnull(x.ttp205, 0))  as v219,
		sum(isnull(x.ttp206, 0))  as v220,
		sum(isnull(x.ttp207, 0))  as v221,
		sum(isnull(x.ttp208, 0))  as v222,
		sum(isnull(x.ttp209, 0))  as v223,
		sum(isnull(x.ttp210, 0))  as v224,
		sum(isnull(x.ttp211, 0))  as v225,
		sum(isnull(x.ttp212, 0))  as v226,
		sum(isnull(x.ttp213, 0))  as v227,
		sum(isnull(x.ttp214, 0))  as v228,
		sum(isnull(x.ttp215, 0))  as v229,
		sum(isnull(x.ttp216, 0))  as v230,
		sum(isnull(x.ttp217, 0))  as v231,
		sum(isnull(x.ttp218, 0))  as v232,
		sum(isnull(x.ttp219, 0))  as v233,
		sum(isnull(x.ttp220, 0))  as v234,
		sum(isnull(x.ttp221, 0))  as v235,
		sum(isnull(x.ttp222, 0))  as v236,
		sum(isnull(x.ttp223, 0))  as v237,
		sum(isnull(x.ttp224, 0))  as v238,
		sum(isnull(x.ttp225, 0))  as v239,
		sum(isnull(x.ttp226, 0))  as v240,
		sum(isnull(x.ttp227, 0))  as v241,
		sum(isnull(x.ttp228, 0))  as v242,
		sum(isnull(x.ttp229, 0))  as v243,
		sum(isnull(x.ttp230, 0))  as v244,
		sum(isnull(x.ttp231, 0))  as v245,
		sum(isnull(x.ttp232, 0))  as v246,
		sum(isnull(x.ttp233, 0))  as v247,
		sum(isnull(x.ttp234, 0))  as v248,
		sum(isnull(x.ttp235, 0))  as v249,
		sum(isnull(x.ttp236, 0))  as v250,
		sum(isnull(x.ttp237, 0))  as v251,
		sum(isnull(x.ttp238, 0))  as v252,
		sum(isnull(x.ttp239, 0))  as v253,
		sum(isnull(x.ttp240, 0))  as v254,
		sum(isnull(x.ttp241, 0))  as v255
	from gs
	left join gs_extd as x
		   on x.loc_n = gs.loc_n
		  and x.tday = gs.tday
		  and x.fs = gs.fs
	where gs.loc_n in (select loc_n from #locations)
	  and gs.tday = day
	group by gs.fs;
end;
go

grant execute on gfisp_get_data_for_daily_summary_report to public;
go


////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_monthly_routesum_preset_keys - LG-1242
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_monthly_routesum_preset_keys') then
   drop proc gfisp_get_data_for_monthly_routesum_preset_keys;
end if;
go

create procedure dba.gfisp_get_data_for_monthly_routesum_preset_keys (
	in locations varchar(8000),
	in monthDay datetime )
begin
/*
call gfisp_get_data_for_monthly_routesum_preset_keys ('1,2,3', '2015-10-01');
*/
	declare sqlCommand varchar(4000);
	
	select fc.loc_n, fc.farecell_n, fc.farecell_id
	into #pkn
	from farecell as fc
	inner join cnf as c
		on	c.loc_n = fc.loc_n
	where	fc.fareset_n = 1
		and	fc.farecell_n between 1 and 14
		and	fc.fs_id = c.fs_id;

	select
		m.loc_n,
		c.loc_name,
		m.route,
		sum(m.fare_c) as fare_c,
		sum(m.key1) as key1,
		sum(m.key2) as key2,
		sum(m.key3) as key3,
		sum(m.key4) as key4,
		sum(m.key5) as key5,
		sum(m.key6) as key6,
		sum(m.key7) as key7,
		sum(m.key8) as key8,
		sum(m.key9) as key9,
		sum(m.keyast) as keyast,
		sum(m.keya) as keya,
		sum(m.keyb) as keyb,
		sum(m.keyc) as keyc,
		sum(m.keyd) as keyd,
		cast('' as varchar(8)) as key1_name,
		cast('' as varchar(8)) as key2_name,
		cast('' as varchar(8)) as key3_name,
		cast('' as varchar(8)) as key4_name,
		cast('' as varchar(8)) as key5_name,
		cast('' as varchar(8)) as key6_name,
		cast('' as varchar(8)) as key7_name,
		cast('' as varchar(8)) as key8_name,
		cast('' as varchar(8)) as key9_name,
		cast('' as varchar(8)) as keyast_name,
		cast('' as varchar(8)) as keya_name,
		cast('' as varchar(8)) as keyb_name,
		cast('' as varchar(8)) as keyc_name,
		cast('' as varchar(8)) as keyd_name
	into #pk
	from mrtesum as m
	inner join cnf as c
		on	c.loc_n = m.loc_n
	where	m.mday = monthDay
	group by
		m.loc_n,
		c.loc_name,
		m.route;

	set sqlCommand = '
		delete from #pk
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;
	
	update #pk set
		key1_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 1),
		key2_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 2),
		key3_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 3),
		key4_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 4),
		key5_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 5),
		key6_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 6),
		key7_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 7),
		key8_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 8),
		key9_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 9),
		keyast_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 10),
		keya_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 11),
		keyb_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 12),
		keyc_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 13),
		keyd_name = (select ltrim(min(pkn.farecell_id)) from #pkn as pkn where pkn.loc_n = pk.loc_n and pkn.farecell_n = 14)
	from #pk as pk;
	
	select * from #pk;
end
go

grant execute on gfisp_get_data_for_monthly_routesum_preset_keys to public
go


////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_monthly_routesum_revenue_ridership - LG-1242
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_monthly_routesum_revenue_ridership') then
   drop proc gfisp_get_data_for_monthly_routesum_revenue_ridership;
end if;
go

create procedure dba.gfisp_get_data_for_monthly_routesum_revenue_ridership (
	in locations varchar(8000),
	in monthDay datetime )
begin
/*
call gfisp_get_data_for_monthly_routesum_revenue_ridership ('1,2,3', '2015-10-01');
*/
	declare sqlCommand varchar(4000);

	select
		m.loc_n,
		c.loc_name,
		m.route,
		sum(m.curr_r) as curr_r,
		sum(m.rdr_c) as rdr_c,
		sum(m.token_c) as token_c,
		sum(m.ticket_c) as ticket_c,
		sum(m.pass_c) as pass_c,
		sum(m.bill_c) as bill_c,
		sum(m.uncl_r) as uncl_r,
		sum(m.dump_c) as dump_c
	into #rr
	from mrtesum as m
	inner join cnf as c
		on	c.loc_n = m.loc_n
	where	m.mday = monthDay
	group by
		m.loc_n,
		c.loc_name,
		m.route;

	set sqlCommand = '
		delete from #rr
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;
	
	select * from #rr;
end;
go

grant execute on gfisp_get_data_for_monthly_routesum_revenue_ridership to public;
go


////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_monthly_routesum_ttp - LG-1242
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_monthly_routesum_ttp') then
   drop proc gfisp_get_data_for_monthly_routesum_ttp;
end if;
go

create procedure dba.gfisp_get_data_for_monthly_routesum_ttp (
	in locations varchar(8000),
	in monthDay datetime,
	in ttps int )
begin
/*
call gfisp_get_data_for_monthly_routesum_ttp ('1,2,3', '2015-10-01', 1);
call gfisp_get_data_for_monthly_routesum_ttp ('1,2,3', '2015-10-01', 0);
*/
	declare sqlCommand varchar(4000);
	declare ttp_index int;
	declare ttpMax int;

	create table #result(
		loc_n int not null,
		loc_name varchar(80) not null,
		fs_id tinyint not null,
		route int not null,
		ttp_n int not null,
		ttp varchar(32) not null,
		ttp_c int null
	);
	
	set ttpMax = gfisf_TTP_Number();

	select *
	into #mrtesum
	from mrtesum
	where	mday = monthDay;

	set sqlCommand = '
		delete from #mrtesum
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;
	
	select *
	into #mrtesum_extd
	from mrtesum_extd
	where	mday = monthDay;

	set sqlCommand = '
		delete from #mrtesum_extd
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;
	
	select
		loc_n,
		fs_id,
		m_ndx,
		coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))) as text
	into #media
	from media
	where	m_ndx > 0
		and	nullif(coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))), '') is not null	
	;
		
	set sqlCommand = '
		delete from #media
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;

	set ttp_index = 1;
	while ttp_index <= ttpMax loop
		-- we have ttp_index
		if ttp_index <= 48 then
			set sqlCommand = '
				insert into #result
				select
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route,
					' + ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(m.ttp' + ltrim(str(ttp_index)) + '), 0) -- ttp_c
				from #mrtesum as m
				inner join cnf as c
					on	c.loc_n = m.loc_n
				group by
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route';
			
			if ttps <> 0 then
				set sqlCommand = sqlCommand +	'
					having sum(m.ttp' + ltrim(str(ttp_index)) + ') <> 0
					';
			end if;
		else
			set sqlCommand = '
				insert into #result
				select
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route,
					' + ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(m.ttp' + ltrim(str(ttp_index)) + '), 0) -- ttp_c
				from #mrtesum_extd as m
				inner join cnf as c
					on	c.loc_n = m.loc_n
				group by
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route';
			
			if ttps <> 0 then
				set sqlCommand = sqlCommand +	'
					having sum(m.ttp' + ltrim(str(ttp_index)) + ') <> 0
					';
			end if;
		end if;

		execute immediate sqlCommand;
	
		set ttp_index = ttp_index + 1;
	end loop;
	
	update #result set
		ttp = m.text
	from #result as r
	inner join #media as m
		on	m.loc_n = r.loc_n
		and	m.fs_id = r.fs_id
		and	m.m_ndx = r.ttp_n
	;
	
	select
		loc_n,
		loc_name,
		route,
		ttp_n,
		ttp,
		ttp_c
	from #result
	order by
		loc_n,
		route,
		ttp_n
	;
end;
go

grant execute on gfisp_get_data_for_monthly_routesum_ttp to public;
go


////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_monthly_summary_report - LG-1242
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_monthly_summary_report') then
   drop proc gfisp_get_data_for_monthly_summary_report;
end if;
go

create procedure dba.gfisp_get_data_for_monthly_summary_report (
	in locations varchar(4000),
	in dayBegin datetime,
	in dayEnd datetime,
	in ttps int	)
begin
/*
call gfisp_get_data_for_monthly_summary_report('1,2,3', '2015-10-01', '2015-10-31', 1);
call gfisp_get_data_for_monthly_summary_report('1,2,3', '2015-10-01', '2015-10-31', 0);
*/
	declare sqlCommand varchar(4000);
	declare ttp_index int;
	declare ttpMax int;
	declare v_fs_id tinyint;
	declare v_loc_n smallint;
	
    create table #result(
		ttp_n int not null,
		ttp varchar(32) not null,
		dom1 int null,
		dom2 int null,
		dom3 int null,
		dom4 int null,
		dom5 int null,
		dom6 int null,
		dom7 int null,
		dom8 int null,
		dom9 int null,
		dom10 int null,
		dom11 int null,
		dom12 int null,
		dom13 int null,
		dom14 int null,
		dom15 int null,
		dom16 int null,
		dom17 int null,
		dom18 int null,
		dom19 int null,
		dom20 int null,
		dom21 int null,
		dom22 int null,
		dom23 int null,
		dom24 int null,
		dom25 int null,
		dom26 int null,
		dom27 int null,
		dom28 int null,
		dom29 int null,
		dom30 int null,
		dom31 int null,
		total_n int not null
    );
	
	set ttpMax = gfisf_TTP_Number();
	
	select
		min(loc_n) into v_loc_n	-- what else we could do?
	from cnf;
	
	select
		fs_id into v_fs_id		-- what else we could do?
	from cnf
	where	loc_n = v_loc_n;
	
	select *
	into #gs
	from gs
	where	tday between dayBegin and dayEnd
		and fs = 0
	;
		
	set sqlCommand = '
		delete from #gs
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;
	
	select *
	into #gs_extd
	from gs_extd
	where	tday between dayBegin and dayEnd
		and fs = 0
	;
		
	set sqlCommand = '
		delete from #gs_extd
		where	loc_n not in (' + locations + ')';
	execute immediate sqlCommand;
	
	select
		m_ndx,
		coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))) as text
	into #media
	from media
	where	m_ndx > 0
		and	nullif(coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))), '') is not null	
		and	loc_n = v_loc_n
		and fs_id = v_fs_id
	;
	
	set ttp_index = 1;
	while ttp_index <= ttpMax loop
		-- we have ttp_index
		if ttp_index <= 48 then
			set sqlCommand = '
				insert into #result
				select ' +
					ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(case datepart(day, tday) when  1 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom1
					isnull(sum(case datepart(day, tday) when  2 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom2
					isnull(sum(case datepart(day, tday) when  3 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom3
					isnull(sum(case datepart(day, tday) when  4 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom4
					isnull(sum(case datepart(day, tday) when  5 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom5
					isnull(sum(case datepart(day, tday) when  6 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom6
					isnull(sum(case datepart(day, tday) when  7 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom7
					isnull(sum(case datepart(day, tday) when  8 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom8
					isnull(sum(case datepart(day, tday) when  9 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom9
					isnull(sum(case datepart(day, tday) when 10 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom10
					isnull(sum(case datepart(day, tday) when 11 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom11
					isnull(sum(case datepart(day, tday) when 12 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom12
					isnull(sum(case datepart(day, tday) when 13 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom13
					isnull(sum(case datepart(day, tday) when 14 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom14
					isnull(sum(case datepart(day, tday) when 15 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom15
					isnull(sum(case datepart(day, tday) when 16 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom16
					isnull(sum(case datepart(day, tday) when 17 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom17
					isnull(sum(case datepart(day, tday) when 18 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom18
					isnull(sum(case datepart(day, tday) when 19 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom19
					isnull(sum(case datepart(day, tday) when 20 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom20
					isnull(sum(case datepart(day, tday) when 21 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom21
					isnull(sum(case datepart(day, tday) when 22 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom22
					isnull(sum(case datepart(day, tday) when 23 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom23
					isnull(sum(case datepart(day, tday) when 24 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom24
					isnull(sum(case datepart(day, tday) when 25 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom25
					isnull(sum(case datepart(day, tday) when 26 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom26
					isnull(sum(case datepart(day, tday) when 27 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom27
					isnull(sum(case datepart(day, tday) when 28 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom28
					isnull(sum(case datepart(day, tday) when 29 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom29
					isnull(sum(case datepart(day, tday) when 30 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom30
					isnull(sum(case datepart(day, tday) when 31 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom31
					0 -- total_n
				from #gs';
		else
			set sqlCommand = '
				insert into #result
				select ' +
					ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(case datepart(day, tday) when  1 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom1
					isnull(sum(case datepart(day, tday) when  2 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom2
					isnull(sum(case datepart(day, tday) when  3 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom3
					isnull(sum(case datepart(day, tday) when  4 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom4
					isnull(sum(case datepart(day, tday) when  5 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom5
					isnull(sum(case datepart(day, tday) when  6 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom6
					isnull(sum(case datepart(day, tday) when  7 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom7
					isnull(sum(case datepart(day, tday) when  8 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom8
					isnull(sum(case datepart(day, tday) when  9 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom9
					isnull(sum(case datepart(day, tday) when 10 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom10
					isnull(sum(case datepart(day, tday) when 11 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom11
					isnull(sum(case datepart(day, tday) when 12 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom12
					isnull(sum(case datepart(day, tday) when 13 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom13
					isnull(sum(case datepart(day, tday) when 14 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom14
					isnull(sum(case datepart(day, tday) when 15 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom15
					isnull(sum(case datepart(day, tday) when 16 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom16
					isnull(sum(case datepart(day, tday) when 17 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom17
					isnull(sum(case datepart(day, tday) when 18 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom18
					isnull(sum(case datepart(day, tday) when 19 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom19
					isnull(sum(case datepart(day, tday) when 20 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom20
					isnull(sum(case datepart(day, tday) when 21 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom21
					isnull(sum(case datepart(day, tday) when 22 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom22
					isnull(sum(case datepart(day, tday) when 23 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom23
					isnull(sum(case datepart(day, tday) when 24 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom24
					isnull(sum(case datepart(day, tday) when 25 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom25
					isnull(sum(case datepart(day, tday) when 26 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom26
					isnull(sum(case datepart(day, tday) when 27 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom27
					isnull(sum(case datepart(day, tday) when 28 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom28
					isnull(sum(case datepart(day, tday) when 29 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom29
					isnull(sum(case datepart(day, tday) when 30 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom30
					isnull(sum(case datepart(day, tday) when 31 then ttp' + ltrim(str(ttp_index)) + ' else 0 end), 0), -- dom31
					0 -- total_n
				from #gs_extd';
		end if;
			
		execute immediate sqlCommand;
	
		set ttp_index = ttp_index + 1;
	end loop;

	update #result set
		ttp = m.text
	from #result as r
	inner join #media as m
		on	m.m_ndx = r.ttp_n
	;

	update #result set
		total_n =
			dom1  +
			dom2  +
			dom3  +
			dom4  +
			dom5  +
			dom6  +
			dom7  +
			dom8  +
			dom9  +
			dom10 +
			dom11 +
			dom12 +
			dom13 +
			dom14 +
			dom15 +
			dom16 +
			dom17 +
			dom18 +
			dom19 +
			dom20 +
			dom21 +
			dom22 +
			dom23 +
			dom24 +
			dom25 +
			dom26 +
			dom27 +
			dom28 +
			dom29 +
			dom30 +
			dom31
	;
	
	if ttps <> 0 then
		delete from #result
		where total_n = 0;
	end if;

	select * from #result
	order by ttp_n;
end
go

grant execute on gfisp_get_data_for_monthly_summary_report to public;
go


////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_daily_route_summary_report - LG-1453
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_daily_route_summary_report') then
   drop proc gfisp_get_data_for_daily_route_summary_report;
end if;
go

create procedure dba.gfisp_get_data_for_daily_route_summary_report (
	in in_locations varchar(8000),
	in in_day datetime,
	in in_ttps int )
begin
/*
call gfisp_get_data_for_daily_route_summary_report ('1,2,3', '2016-01-15', 1);
call gfisp_get_data_for_daily_route_summary_report ('1,2,3', '2016-01-15', 0);
*/
	declare sqlCommand varchar(4000);
	declare ttp_index int;
	declare ttpMax int;

	create table #result(
		loc_n int not null,
		loc_name varchar(80) not null,
		fs_id tinyint not null,
		route int not null,
		ttp_n int not null,
		ttp varchar(32) not null,
		ttp_c int null
	);
	
	set ttpMax = gfisf_TTP_Number();

	select *
	into #rtesum
	from rtesum
	where	tday = in_day
		and	route <> -1
	;

	set sqlCommand = '
		delete from #rtesum
		where	loc_n not in (' + in_locations + ')';
	execute immediate sqlCommand;

	select *
	into #rtesum_extd
	from rtesum_extd
	where	tday = in_day
		and	route <> -1
	;

	set sqlCommand = '
		delete from #rtesum_extd
		where	loc_n not in (' + in_locations + ')';
	execute immediate sqlCommand;
	
	select
		loc_n,
		fs_id,
		m_ndx,
		coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))) as text
	into #media
	from media
	where	m_ndx > 0
		and	nullif(coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))), '') is not null	
	;
		
	set sqlCommand = '
		delete from #media
		where	loc_n not in (' + in_locations + ')';
	execute immediate sqlCommand;

	set ttp_index = 1;
	while ttp_index <= ttpMax loop
		-- we have ttp_index
		if ttp_index <= 48 then
			set sqlCommand = '
				insert into #result
				select
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route,
					' + ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(m.ttp' + ltrim(str(ttp_index)) + '), 0) -- ttp_c
				from #rtesum as m
				inner join cnf as c
					on	c.loc_n = m.loc_n
				group by
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route';
			
			if in_ttps <> 0 then
				set sqlCommand = sqlCommand +	'
					having sum(m.ttp' + ltrim(str(ttp_index)) + ') <> 0
					';
			end if;
		else
			set sqlCommand = '
				insert into #result
				select
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route,
					' + ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(m.ttp' + ltrim(str(ttp_index)) + '), 0) -- ttp_c
				from #rtesum_extd as m
				inner join cnf as c
					on	c.loc_n = m.loc_n
				group by
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route';
			
			if in_ttps <> 0 then
				set sqlCommand = sqlCommand +	'
					having sum(m.ttp' + ltrim(str(ttp_index)) + ') <> 0
					';
			end if;
		end if;

		execute immediate sqlCommand;
	
		set ttp_index = ttp_index + 1;
	end loop;

	update #result set
		ttp = m.text
	from #result as r
	inner join #media as m
		on	m.loc_n = r.loc_n
		and	m.fs_id = r.fs_id
		and	m.m_ndx = r.ttp_n
	;

	select
		loc_n,
		loc_name,
		route,
		ttp_n,
		ttp,
		ttp_c
	from #result
	order by
		loc_n,
		route,
		ttp_n
	;
end;
go

grant execute on gfisp_get_data_for_daily_route_summary_report to public;
go


////////////////////////////////////////////////////////////////////////
// gfisp_get_data_for_routesum_report - LG-1513
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_data_for_routesum_report') then
   drop proc gfisp_get_data_for_routesum_report;
end if;
go

create procedure dba.gfisp_get_data_for_routesum_report (
	in locations varchar(8000),
	in daytimefrom datetime,
	in daytimeto datetime,
	in route varchar(8000),
	in period varchar(8000),
	in weekday varchar(8000),
	in WantTTPsWithRides int,
	in ttps int,
	in ttpSelection varchar(8000) )	-- LG-1902
begin
/*
call gfisp_get_data_for_routesum_report ('1,2,3', '2016-01-01 00:00:00.000', '2016-02-12 00:00:00.000', '1,2,3,4,5,0','1,2,3,4,5','1,2,3,4,5',1,1,'1,67');
call gfisp_get_data_for_routesum_report ('1,2,3', '2016-01-01 00:00:00.000', '2016-02-12 00:00:00.000', 'ALL','1,2,3,4,5','1,2,3,4,5',1,1,'ALL');
*/
 
	declare sqlCommand varchar(8000);
	declare ttp_index int;
	declare ttpMax int;

	create table #result (
		loc_n int not null,
		loc_name varchar(80) not null,
		fs_id tinyint not null,
		route int not null,
		ttp_n int not null,
		ttp varchar(32) not null,
		ttp_c int null
	);
 
	set ttps = 0; -- Currently default input ttp to 0. this will pull all ttps - TODO
	set ttpMax= gfisf_TTP_Number();

	select *
	into #ev
	from ev 	 
	where	ts between daytimefrom and daytimeto
		and	route <> -1
	;

	if locations <> 'ALL' then
		set sqlCommand = 'delete from #ev	where loc_n not in (' + locations + ')';
		execute immediate (sqlCommand);
	end if;	
	 		
	if route <> 'ALL' then
		set sqlCommand = 'delete from #ev	where route not in (' + route + ')';
		execute immediate (sqlCommand);
	end if;	
	
	if weekday <> 'ALL' then
		set sqlCommand = 'delete from #ev	where datepart(dw,ts) not in (' + weekday + ')';
		execute immediate (sqlCommand);
	end if;	
	
	if period <> 'ALL' then
		set sqlCommand = 'delete from #ev	where
			(
				select
					case
						when cast(#ev.ts as time) <  cast(fsc.peak1on  as time)
						then 1
						when cast(#ev.ts as time) >= cast(fsc.peak1on  as time) and cast(#ev.ts as time) < cast(fsc.peak1off as time)
						then 2
						when cast(#ev.ts as time) >= cast(fsc.peak1off as time) and cast(#ev.ts as time) < cast(fsc.peak2on  as time)
						then 3
						when cast(#ev.ts as time) >= cast(fsc.peak2on  as time) and cast(#ev.ts as time) < cast(fsc.peak2off as time)
						then 4
						when cast(#ev.ts as time) >  cast(fsc.peak2off as time)
						then 5
					end
				from fsc
				join cnf
					on	cnf.loc_n = fsc.loc_n
					and	cnf.fs_id = fsc.fs_id
				where #ev.loc_n = fsc.loc_n
			) not in (' + period + ')';
		execute immediate (sqlCommand);
	end if;	
	
	select x.*
	into #ev_extd
	from #ev as ev
	inner join ev_extd as x
		on	x.loc_n = ev.loc_n
		and	x.id = ev.id
		and	x.seq = ev.seq
	;
		
	select
		loc_n,
		fs_id,
		m_ndx,
		coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))) as text
    into #media
	from media
	where	m_ndx > 0
		and	nullif(coalesce(nullif(ltrim(rtrim(text)), ''), ltrim(rtrim(description))), '') is not null	 
	;
		
	set sqlCommand = '
		delete from #media
		where	loc_n not in (' + locations + ')';
	execute immediate (sqlCommand);

	set ttp_index = 1;
	while ttp_index <= ttpMax loop
	 	if ttp_index <= 48 then
			set sqlCommand = '
				insert into #result
				select
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route,
					' + ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(m.ttp' + ltrim(str(ttp_index)) + '), 0) -- ttp_c
				from #ev as m
				inner join cnf as c
					on	c.loc_n = m.loc_n
				group by
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route';	
		else
			set sqlCommand = '
				insert into #result
				select
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route,
					' + ltrim(str(ttp_index)) + ', -- ttp_n
					'''', -- ttp
					isnull(sum(s.ttp' + ltrim(str(ttp_index)) + '), 0) -- ttp_c
				from #ev m
				left join #ev_extd s on m.loc_n= s.loc_n and m.id=  s.id and m.seq=s.seq  
				left join cnf as c
					on	c.loc_n = m.loc_n
				group by
					c.loc_n,
					c.loc_name,
					c.fs_id,
					m.route';	
		end if;

		execute immediate sqlCommand;
		
		set ttp_index = ttp_index + 1;
	end loop;

	-- LG-1902
	set ttpSelection = nullif(ltrim(rtrim(ttpSelection)), '');
	set ttpSelection = isnull(ttpSelection, 'ALL');
	if ttpSelection <> 'ALL' then
		delete from #result
		where	charindex(',' + ltrim(str(ttp_n)) + ',', ',' + ttpSelection + ',') = 0;
	end if;

 	update #result set
		ttp = m.text
	from #result as r
	inner join #media as m
		on	m.loc_n = r.loc_n
		and	m.fs_id = r.fs_id
		and	m.m_ndx = ttp_n
	;  
	
	if WantTTPsWithRides = 1 then
		select
			loc_n,
			loc_name,
			route,
			ttp_n,
			ttp,
			ttp_c
		from #result
		where	ttp <> 'Empty TTP'
			and	ttp_c <> 0
		order by
			loc_n,
			route,
			ttp_n
		;
	else
		select
			loc_n,
			loc_name,
			route,
			ttp_n,
			ttp,
			ttp_c
		from #result
		where	ttp <> 'Empty TTP'
		order by
			loc_n,
			route,
			ttp_n
		;
	end if;
end
go

grant execute on gfisp_get_data_for_routesum_report to public
go


////////////////////////////////////////////////////////////////////////
// gfisp_vaulting_reconciliation - LG-1684
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisp_vaulting_reconciliation') then
   drop proc gfisp_vaulting_reconciliation;
end if;
go

create procedure dba.gfisp_vaulting_reconciliation (
	in in_location smallint,
	in in_begin datetime,
	in in_end datetime,
	in in_type int,
	in in_update int )
begin
/*
call gfisp_vaulting_reconciliation (1, '2000-01-01', '2016-12-31', 3, 0);
call gfisp_vaulting_reconciliation (1, '2000-01-01', '2016-12-31', 3, 1);
call gfisp_vaulting_reconciliation (1, '2000-01-01', '2016-12-31', 5, 0);
call gfisp_vaulting_reconciliation (1, '2000-01-01', '2016-12-31', 5, 1);
*/
	if in_type = 3 then
		create table #ml_revenue(
			loc_n smallint not null,
			binid smallint not null,
			seq int not null,
			id int not null,
			dime int not null,		-- GD-1684
			penny int not null,		-- GD-1684
			nickel int not null,	-- GD-1684
			quarter int not null,	-- GD-1684
			sba int not null,		-- GD-1684
			half int not null,		-- GD-1684
			one int not null,		-- GD-1684
			two int not null,		-- GD-1684
			five int not null,		-- GD-1684
			ten int not null,		-- GD-1684
			twenty int not null,	-- GD-1684
			revenue numeric(14, 2) not null,
			ml_revenue numeric(14, 2) not null,
			revenue_diff numeric(14, 2) not null,
			primary key nonclustered (loc_n, seq)
		);
		
		insert into #ml_revenue
		select
			ml.loc_n,
			vlt.binid,
			vlt.seq,
			ml.id,
			ml.dime,		-- GD-1684
			ml.penny,		-- GD-1684
			ml.nickel,		-- GD-1684
			ml.quarter,		-- GD-1684
			ml.sba,			-- GD-1684
			ml.half,		-- GD-1684
			ml.one,			-- GD-1684
			ml.two,			-- GD-1684
			ml.five,		-- GD-1684
			ml.ten,			-- GD-1684
			ml.twenty,		-- GD-1684
			vlt.revenue,
			ml.curr_r,
			vlt.revenue - ml.curr_r
		from ml
		inner join vlt
			on	vlt.loc_n = ml.loc_n
			and	vlt.id = ml.id
			and	vlt.type = 3
		where	ml.tday between in_begin and in_end
			and	vlt.revenue <> ml.curr_r;
		
		if in_update = 0 then
			select * from #ml_revenue
			order by
				loc_n,
				seq
			;
		else
			update vlt set
				revenue = r.ml_revenue,
				dime = r.dime,			-- GD-1684
				penny= r.penny,			-- GD-1684
				nickel=r.nickel,		-- GD-1684
				quarter = r.quarter,	-- GD-1684
				sba = r.sba,			-- GD-1684
				half = r.half,			-- GD-1684
				one = r.one,			-- GD-1684
				two = r.two,			-- GD-1684
				five = r.five,			-- GD-1684
				ten = r.ten,			-- GD-1684
				twenty = r.twenty		-- GD-1684
			from vlt
			inner join #ml_revenue as r
				on	r.loc_n = vlt.loc_n
				and	r.seq = vlt.seq
			;
			
			commit;
		end if;
	elseif in_type = 5 then
		create table #revenue(
			loc_n smallint not null,
			binid smallint not null,
			seq_begin int not null,
			seq_end int not null,
			revenue numeric(14, 2) not null,
			revenue_sum numeric(14, 2) not null,
			revenue_diff numeric(14, 2) not null,
			dime int not null,		-- GD-1684
			penny int not null,		-- GD-1684
			nickel int not null,	-- GD-1684
			quarter int not null,	-- GD-1684
			sba int not null, 		-- GD-1684
			half int not null,		-- GD-1684
			one int not null,		-- GD-1684
			two int not null,		-- GD-1684
			five int not null,		-- GD-1684
			ten int not null,		-- GD-1684
			twenty int not null,	-- GD-1684
			primary key nonclustered (loc_n, seq_end)
		);

		insert into #revenue
		select
			type45.loc_n,
			type45.binid,
			type45.seq_begin,
			type45.seq_end,
			type45.revenue,
			isnull(sum(type3.revenue), 0) as revenue_sum,
			type45.revenue - isnull(sum(type3.revenue), 0) as revenue_diff,
			isnull(sum(type3.dime), 0),		-- GD-1684
			isnull(sum(type3.penny), 0),	-- GD-1684
			isnull(sum(type3.nickel), 0),	-- GD-1684
			isnull(sum(type3.quarter), 0),	-- GD-1684
			isnull(sum(type3.sba), 0),		-- GD-1684
			isnull(sum(type3.half), 0),		-- GD-1684
			isnull(sum(type3.one), 0),		-- GD-1684
			isnull(sum(type3.two), 0),		-- GD-1684
			isnull(sum(type3.five), 0),		-- GD-1684
			isnull(sum(type3.ten), 0),		-- GD-1684
			isnull(sum(type3.twenty), 0)	-- GD-1684
		from
		(
			select
				type5.loc_n,
				type5.binid,
				max(type4.seq) as seq_begin,
				type5.seq as seq_end,
				type5.revenue
			from vlt as type5
			inner join
			(
				select
					loc_n,
					binid,
					seq,
					revenue
				from vlt
				where	loc_n = in_location
					and	tday between in_begin and in_end
					and	type = 4
			) as type4	on	type4.loc_n = type5.loc_n
						and	type4.binid = type5.binid
						and	type4.seq < type5.seq
			where	type5.loc_n = in_location
				and	type5.tday between in_begin and in_end
				and	type5.type = 5
			group by 
				type5.loc_n,
				type5.binid,
				type5.seq,
				type5.revenue
		) as type45
		left join
		(
			select
				loc_n,
				binid,
				seq,
				dime,		-- GD-1684
				penny,		-- GD-1684
				nickel,		-- GD-1684
				quarter,	-- GD-1684
				sba,		-- GD-1684
				half,		-- GD-1684
				one,		-- GD-1684
				two,		-- GD-1684
				five,		-- GD-1684
				ten,		-- GD-1684
				twenty,		-- GD-1684
				revenue
			from vlt
			where	loc_n = in_location
				and	tday between in_begin and in_end
				and	type = 3
		) as type3	on	type3.loc_n = type45.loc_n
					and	type3.binid = type45.binid
					and type3.seq between type45.seq_begin and type45.seq_end
		group by
			type45.loc_n,
			type45.binid,
			type45.seq_begin,
			type45.seq_end,
			type45.revenue
		having type45.revenue <> isnull(sum(type3.revenue), 0);
		
		if in_update = 0 then
			select * from #revenue
			order by
				loc_n,
				seq_end
			;
		else
			update vlt set
				revenue = r.revenue_sum,
				dime = r.dime,			-- GD-1684
				penny= r.penny,			-- GD-1684
				nickel=r.nickel,		-- GD-1684
				quarter = r.quarter,	-- GD-1684
				sba = r.sba,			-- GD-1684
				half = r.half,			-- GD-1684
				one = r.one,			-- GD-1684
				two = r.two,			-- GD-1684
				five = r.five,			-- GD-1684
				ten = r.ten,			-- GD-1684
				twenty = r.twenty		-- GD-1684
		from vlt
			inner join #revenue as r
				on	r.loc_n = vlt.loc_n
				and	r.seq_end = vlt.seq
			;
			
			commit;
		end if;
	end if;
end
go

grant execute on gfisp_vaulting_reconciliation to public
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_trans_detail_by_receipt') then
   drop proc gfisp_trans_detail_by_receipt;
end if;
go

create procedure dba.gfisp_trans_detail_by_receipt
	(
	in_receipt 		int			default NULL,
	in_StartDT 		datetime,
	in_EndDT 		datetime,
	in_TransID 		int			default NULL,
	in_Station_name varchar(100)	default NULL,
	in_TVM_Number 	varchar(100)	default NULL,
	in_Card_type 	varchar(100)	default NULL
	)
begin
/*
call gfisp_trans_detail_by_receipt (2669, '2012-01-16 00:00:00', '2012-01-25 00:00:00');
*/
	select   
		case t.eq_seq		
			when 0 then ISNULL(t2.eq_seq, 0)
			else t.eq_seq
		end	as ReceiptID,
		t.ts			as Transaction_Date_Time,
		isnull(in_Station_name, isnull(l.name, cast(t.loc_n as varchar(10)))) as Station_Name,
		e.eq_desc		as TVM,
		t.tr_id			as TransactionID, 
		(CASE WHEN t.type=302 AND t.grp=3 AND t.price=0 AND t.remval>0 
				THEN t.remval 
				ELSE t.price 
				END) as Sale_Price,
		t.bill_amt + t.coin_amt as Total_Cash_Inserted,
		t.bill_amt		as Bills, 
		t.coin_amt		as Coins, 
		t.chg_amt		as Change, 
		t.chg_amt_err	as Change_Error,
		case t.flags & 2 
			when 2 then 'Yes'
			else 'No'
		end as Receipt_Given,
		case t.flags & 4
			when 4 then 'Yes'
			else 'No'
		end				as Bad_List,						
		d.pass_desc		as CardType,
		t.t1_in as t1_in,
		t.t2_in as t2_in,
		e.eq_n as tvm_nbr
	from vnd_tr t
	join gfi_eq e
		on t.eq_n = e.eq_n
	left join vnd_def d
		on t.pass_type = d.pass_type
		and t.pass_category = d.pass_category
	left join gfi_lst l
		on t.loc_n = l.code
		and l.type='EQ LOC' 
		and l.code>0
	left join vnd_tr t2
		on	t.loc_n = t2.loc_n
		and	t.tr_seq = t2.tr_id
	where (t.eq_seq =in_receipt  or in_receipt is null)
		and t.ts >= in_StartDT
		and t.ts < in_EndDT  
		and (d.pass_desc=in_Card_type or in_Card_type is null)
		and (d.pass_desc=in_Card_type or in_Card_type is null)
		and (e.eq_n =in_TVM_Number  or in_TVM_Number is null)
		and (t.tr_id=in_TransID  or in_TransID is null)
		and (l.name=in_Station_name   or in_Station_name is null)
	order by e.eq_desc;
end;
go

grant execute on gfisp_trans_detail_by_receipt to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_Smart_Card_History_Report') then
   drop proc gfisp_Smart_Card_History_Report;
end if;
go

create Procedure dba.gfisp_Smart_Card_History_Report
(	FromDate			datetime		default NULL,
	ToDate				datetime		default NULL,
	TransType			varchar(8000)	default NULL,
	CardIDInclude		varchar(15)		default NULL,
	CardIDExclude		varchar(15)		default NULL,
	Bus					varchar(8000)	default NULL		// LG-1313
)
Begin
	declare stringvar varchar(20);
	declare ind int;
	create table #TransType (TransType int);

	if (TransType is not null)
	then
		set ind = charindex(',',TransType);
		while ind > 0
		loop
			  set stringvar = substring(TransType,1,ind-1);
			  set TransType = substring(TransType,ind+1,length(TransType)-ind);
			  insert into #TransType values (stringvar);
			  set ind = charindex(',',TransType);
		end loop;
		set stringvar = TransType;
		insert into #TransType values (stringvar);
	end if;

	create table #Bus (TransType int);

	if(Bus is not null)
	then
		set ind = charindex(',',Bus);
		while ind > 0
		loop
			  set stringvar = substring(Bus,1,ind-1);
			  set Bus = substring(Bus,ind+1,length(Bus)-ind);
			  insert into #Bus values (stringvar);
			  set ind = charindex(',',Bus);
		end loop;
		set stringvar = Bus;
		insert into #Bus values (stringvar);
	end if;
	
	select 
		rec_type, 
		t.card_id, 
		t.card_type, 
		t.card_pid, 
		t.loc_n, 
		t.id, 
		t.tr_seq, 
		t.type, 
		t.ts, 
		t.aid, 
		t.mid, 
		t.eq_type, 
		t.eq_n, 
		t.route,
		t.prod_id, 
		t.prod_type, 
		t.price, 
		t.change, 
		(	case 
				when t.prod_type in (1,7) and t.value=0 
					then (	case 
								when p.value>0 then p.value 
								when f.value>0 then f.value 
								else 0 
							end
						) 
				else t.value 
			end
		) as value,
		t.pending, 
		t.pay_type, 
		t.pay_cc_type, 
		t.pay_card_n, 
		t.create_ts, 
		t.exp_ts, 
		t.user_profile, 
		(select top 1 des from gfi_epay_non_autoload_tr  tsub where tsub.card_id = t.card_id and tsub.prod_type = t.prod_type
		and ts between start_ts and exp_ts   order by exp_ts desc)des, 
		rc, 
		t.flags
	from 
	(
		select 
			1 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			t.loc_n, 
			t.id, 
			t.tr_seq, 
			t.type, 
			t.ts, 
			c.aid, 
			13 as mid, 
			(case when ml.prb_n>100 then 1 else 0 end) as eq_type, 
			t.bus as eq_n, 
			t.route,
			t.prod_id, 
			t.prod_type, 
			t.price, 
			t.change, 
			t.value, 
			t.pending, 
			t.pay_type, 
			t.pay_cc_type, 
			t.pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile, 
			0 as rc, 
			' ' as flags,
			null as des
		from gfi_epay_act_bus t 
		inner join gfi_epay_card c
			on t.card_id = c.card_id 
		left outer join ml 
			on t.loc_n = ml.loc_n 
			and t.id = ml.id
		where t.card_id > 0 
			and t.ts >= FromDate
			and t.ts < ToDate  
			and t.type in (select * from #TransType)
			and ( Bus is null or t.bus in (select * from #Bus) )	// LG-1313
			
		union all

		select 2 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			null as loc_n, 
			t.tr_id as id, 
			null as tr_seq , 
			t.type, 
			t.ts, 
			c.aid, 
			5 as mid, 
			t.eq_type, 
			t.eq_n, 
			null as route,
			t.prod_id, 
			t.prod_type, 
			t.price, 
			t.change, 
			t.value, 
			t.pending, 
			t.pay_type, 
			t.pay_cc_type, 
			t.pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile, 
			0 as rc,
				case when vnd.flags & 1 = 1 then '' else '' end +
				case when vnd.flags & 2 = 2 then '' else '' end +
				case when vnd.flags & 4 = 4 then ' Badlist ' else '' end +
				case when vnd.flags & 8 = 8 then ' Passpack ' else '' end +
				case when vnd.flags & 16 = 16 then '' else '' end +
				case when vnd.flags & 32 = 32 then '' else '' end +
				case when vnd.flags & 64 = 64 then ' Lost Credit ' else '' end +
				case when vnd.flags & 128 = 128 then ' Over Payment ' else '' end +
				case when vnd.flags & 256 = 256 then ' Replace - Bad ' else '' end +
				case when vnd.flags & 512 = 512 then ' Stripe Changed ' else '' end +
				case when vnd.flags & 1024 = 1024 then ' Tray Sensor - No Detect ' else '' end +
				case when vnd.flags & 2048 = 2048 then ' Issue Failed ' else '' end 
			as flags,
			na.des
		from gfi_epay_act_eq t
		join gfi_epay_non_autoload_tr na 
			on t.tr_id=na.tr_id
		join vnd_tr vnd 
			on vnd.tr_id = na.id
		join gfi_epay_card c
			on t.card_id = c.card_id 
		where t.card_id > 0 
			and t.ts >= FromDate
			and t.ts < ToDate 
			and t.type in (select * from #TransType)
			and Bus is null															// LG-1313

		union all

		select 
			3 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			null as loc_n, 
			t.tr_id as id, 
			null as tr_seq ,
			t.tr_type as type , 
			t.ts, 
			c.aid, 
			t.mid, 
			t.eq_type, 
			t.eq_n, 
			null as route,
			t.prod_id, 
			t.prod_type, 
			t.price, 
			t.change, 
			t.value, 
			t.pending, 
			null as pay_type, 
			null as pay_cc_type, 
			null as pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile,
			(case when t.tr_type=0 and c.ord_id>0 and c.item_n>0 then 1 else 0 end) as rc, 
			' ' as flags, 
			t.des
		from gfi_epay_non_autoload_tr t
		join gfi_epay_card c
			on t.card_id = c.card_id 
		where t.card_id > 0 
			and t.ts >= FromDate
			and t.ts < ToDate 
			and t.tr_type in (select * from #TransType)
			and t.mid=9
			and Bus is null															// LG-1313
		
		union all
		
		select 
			4 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			t.ord_id as loc_n, 
			t.item_n as id, 
			t.rec_id as tr_seq, 
			t.type, 
			t.ts, 
			c.aid, 
			5 as mid, 
			3 as eq_type , 
			null as eq_n, 
			null as route,
			t.prod_id, 
			t.prod_type, 
			t.price, 
			t.change, 
			0 as value, 
			0 as pending, 
			t.pay_type, 
			t.pay_cc_type, 
			t.pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile, 
			i.type as rc, 
			' ' as flags,
			null as des
		from gfi_epay_act_order t
		join gfi_epay_card c
			on t.card_id = c.card_id 
		join gfi_epay_order_item i
			on t.ord_id = i.ord_id 
			and t.item_n = i.item_n
		where 
			((t.type=0 and t.ord_id=c.ord_id and t.item_n=c.item_n) or 
			(t.type>0 and t.card_id>0 and t.card_id=c.card_id))
			and i.type <> 6
			and t.ts >= FromDate
			and t.ts < ToDate 
			and t.type in (select * from #TransType)
			and ((t.fulfill_ts is not null) OR (t.pending=0)) // Raj - Commented above and included this for Datarun 11
			--and i.fulfill_ts is not null	// Sruthi 6/2/2017
			and Bus is null															// LG-1313
		
		union all
		
		select 
			4 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			t.ord_id as loc_n, 
			t.item_n as id, 
			t.rec_id as tr_seq , 
			t.type, 
			t.ts, 
			c.aid, 
			5 as mid, 
			3 as eq_type, 
			null as eq_n, 
			null as route,
			t.prod_id, 
			t.prod_type, 
			t.price, 
			t.change, 
			0 as value, 
			0 as pending, 
			t.pay_type, 
			t.pay_cc_type, 
			t.pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile, 
			i.type as rc, 
			' ' as flags,
			null as des
		from gfi_epay_act_order t
		join gfi_epay_card c
			on c.card_id = t.card_id 
		join gfi_epay_order_item i
			on t.ord_id = i.ord_id 
			and t.item_n = i.item_n
		join gfi_epay_order_autoload_range r
			on t.ord_id = r.ord_id 
			and t.item_n = r.item_n 
			and t.rec_id = r.rec_id
		where t.type > 0 
			and i.type = 6 
			and t.ts >= FromDate
			and t.ts < ToDate 
			and t.type in (select * from #TransType)
			and ((t.fulfill_ts is not null) OR (t.pending=0)) // Raj - Datarun 11
			and i.fulfill_ts is not null
			and Bus is null															// LG-1313
		
		union all
		
		select 
			5 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			t.ord_id as loc_n, 
			t.item_n as id, 
			t.rec_id as tr_seq , 
			t.type, 
			t.fulfill_ts as ts, 
			c.aid, 
			t.fulfill_mid, 
			t.fulfill_eq_type, 
			t.fulfill_eq_n, 
			null as route, 
			t.prod_id, 
			t.prod_type, 
			null as price, 
			t.change, 
			t.value, 
			t.pending, 
			null as pay_type, 
			null as pay_cc_type, 
			null as pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile, 
			t.rc, 
			' ' as flags, 
			null as des
		from gfi_epay_act_order t
		join gfi_epay_card c
			on t.card_id = c.card_id 
		join gfi_epay_order_item i
			on t.ord_id = i.ord_id 
			and t.item_n = i.item_n 
		where t.type > 0 
			and t.card_id > 0 
			and i.type <> 6
			and t.fulfill_ts >= FromDate
			and t.fulfill_ts < ToDate 
			and t.type in (select * from #TransType)
			and ((t.fulfill_ts is not null) OR (t.pending=0)) // Raj - Datarun 11
			and i.fulfill_ts is not null
			and Bus is null															// LG-1313
		
		union all
		
		select 
			5 as rec_type, 
			c.card_id, 
			c.card_type, 
			c.card_pid, 
			r.ord_id as loc_n, 
			r.item_n as id, 
			r.rec_id as tr_seq , 
			t.load_type as type , 
			t.ts, 
			c.aid, 
			t.mid, 
			t.eq_type, 
			t.eq_n, 
			tr.route,
			t.prod_id_card as prod_id, 
			r2.prod_type, 
			f.price, 
			r.value as change, 
			t.value, 
			t.pending, 
			null as pay_type, 
			null as pay_cc_type, 
			null as pay_card_n, 
			c.create_ts, 
			c.exp_ts, 
			c.user_profile, 
			t.rc, 
			' ' as flags, 
			null as des
		from gfi_epay_autoload_tr t 
		join gfi_epay_card c
			on c.card_id = t.card_id 
		join gfi_epay_autoload_range r 
			on r.load_seq = t.load_seq
		join gfi_epay_autoload_pkg p
			on p.pkg_id = t.pkg_id 
			and r.id = p.auto_id_range 
		join gfi_epay_order_autoload_range r2
			on r.ord_id = r2.ord_id 
			and r.item_n = r2.item_n 
			and r.rec_id = r2.rec_id
		left outer join tr 
			on t.loc_n = tr.loc_n 
			and t.id = tr.id 
			and t.tr_seq = tr.tr_seq
		left outer join gfi_epay_fare f 
			on r.fare_id = f.fare_id 
			and f.fare_id > 0
		where t.range = 1 
			and t.ts >= FromDate
			and t.ts < ToDate 
			and t.load_type in (select * from #TransType)
			and ( Bus is null or tr.bus in (select * from #Bus) )		// LG-1313
	) t
	left outer join gfi_epay_card_dtl p 
		on t.card_id = p.card_id 
		and t.prod_id = p.prod_id 
		and p.des = isnull(t.des, p.des)
	left outer join gfi_epay_fare f 
		on p.fare_id = f.fare_id
	where t.card_pid like '%' + CardIDInclude + '%'
		and t.card_pid not like CardIDExclude
	order by 
		t.card_pid, 
		t.ts, 
		(case when rec_type=4 then 1 when rec_type=5 then 2 else 3 end);
end;
go

grant execute on gfisp_Smart_Card_History_Report to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_recovery_money_report') then
   drop proc gfisp_recovery_money_report;
end if;
go

create procedure dba.gfisp_recovery_money_report
     ( in_StartDT         datetime
      ,in_EndDT            datetime
      ,in_TVM_Number       varchar(8000)
      ,in_UserID           varchar(8000)
	)
begin

DECLARE in_Sql varchar(8000);

Create table #rmr(
                eq_n                          smallint,
                audit                         int,
                userid                        int,
                ts                            datetime,
                type                          varchar(20),
                amt                           numeric,
                Cents5                        smallint,
                Cent10                        smallint,
                Cent25                        smallint,
                cent100                       smallint,
                oneDollarBill                 smallint,
                TwoDollarBill                 smallint,
                FiveDollarBill                smallint,
                TenDollarBill                 smallint,
                TwentyDollarBill              smallint,
                FiftyDollarBill               smallint,
                HundredDollarBill             smallint
                );             
                
SET in_Sql = 
	'INSERT INTO #rmr
	SELECT   
		e.eq_n ,
		e.audit,
		e.userid,
		e.ts,
		( Case  when e.type = 717 then                 ''Coin Tekpak Cash''
				when e.type = 517 then ''Cashbox Cash''
				when e.type = 675 then ''Bill Tekpak''
				else ''unknown''
				end ) as type  ,
		c.amt ,
		c.c5 as Cents5,
		c.c10 Cent10,
		c.c25 Cent25,
		c.c100 cent100,
		c.b1 oneDollarBill,
		c.b2 TwoDollarBill,
		c.b5 FiveDollarBill,
		c.b10 TenDollarBill,
		c.b20 TwentyDollarBill,
		c.b50 FiftyDollarBill,
		c.b100 HundredDollarBill
	FROM vnd_ev e 
	JOIN vnd_evd_cash c ON c.ev_id=e.ev_id  
	where e.ts       between  '''+cast(in_StartDT as varchar(50))+''' and  '''+cast(in_EndDT as varchar(50))+'''
	and e.type in (717, 517, 675)
	';
	//and (e.eq_n = in_TVM_Number or in_TVM_Number is null)
	if in_TVM_Number = 'ALL' Then
		set in_sql = in_sql + 'and e.eq_n      = e.eq_n
	'
	else
		set in_sql = in_sql + 'and e.eq_n      IN  ('+in_TVM_Number+')
	'
	end if;              
	//and (E.USERID=in_USERID or in_UserID is null)
	if in_UserID = 'ALL' Then
		set in_sql = in_sql + 'and E.USERID    = E.USERID
	'
	else
		set in_sql = in_sql + 'and E.USERID    IN  ('+in_UserID+')
	'
	end if;              
	//ORDER by e.ts desc
	set in_sql = in_sql + 'ORDER by e.ts desc;';

	execute immediate in_Sql;

	select * from #rmr
	order by ts desc;

	drop table #rmr;
end;
go

grant execute on gfisp_recovery_money_report to public;
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_Cash_Transaction_Summary_report') then
   drop proc gfisp_Cash_Transaction_Summary_report;
end if;


Create Procedure dba.gfisp_Cash_Transaction_Summary_report
(	in FromDate datetime default null,
	in ToDate datetime default null,
	in locations varchar(4000) default null,
	in DateType smallint default null
)
Begin
////////////////////////////////////////////////////////////////////////
// Author:		Richard B. Becker
// Create date: 02/16/2016
// Description:	Cash Transaction Summary Report
// Based on previous version in Power Builder
//
//				GD-1607 - Cash Transaction Summary Report
////////////////////////////////////////////////////////////////////////

// call gfisp_Cash_Transaction_Summary_report ('2016-01-01', '2016-01-31', '1,2,3', 1);
// call gfisp_Cash_Transaction_Summary_report ('2016-01-01', '2016-01-31', '1,2,3', 2);
// call gfisp_Cash_Transaction_Summary_report ('2016-01-01', '2016-01-31', '1,2,3', 3);

	declare li_loc_n smallint;
	declare li_index int;

    create table #locations(
        loc_n smallint
    );

	set locations = ltrim(rtrim(isnull(locations, '')));

	while locations <> '' loop
		set li_index = charindex(',', locations);

		if li_index = 0 then
			set li_loc_n = cast(locations as smallint);
			set locations = '';
		else
			set li_loc_n = cast(rtrim(substring(locations, 1, li_index - 1)) as smallint);
			set locations = ltrim(substring(locations, li_index + 1, length(locations)));
		end if;

		insert into #locations values (li_loc_n);
	end loop;
	
	// Date Type = 1 (Transaction Time)

	SELECT 
		cnf.loc_n, 
		cnf.loc_name,
		YMD(YEAR(tr.ts),MONTH(tr.ts),1) mth, 
		fs, 
		n, 
		COUNT(*) cnt, 
		SUM(amt) amt 
	FROM tr
	inner join cnf
		on	cnf.loc_n = tr.loc_n
	JOIN trmisc 
		ON tr.loc_n = trmisc.loc_n 
		AND tr.id = trmisc.id 
		AND tr.tr_seq = trmisc.tr_seq 
	WHERE tr.loc_n IN (select loc_n from #locations)
		AND tr.type = 119 
		AND trmisc.amt > 0 
		AND tr.ts >= FromDate
		AND tr.ts <= ToDate
		AND DateType = 1
	GROUP BY 
		cnf.loc_n, 
		cnf.loc_name,
		YMD(YEAR(tr.ts),MONTH(tr.ts),1), 
		fs, 
		n

	UNION

	// Date Type = 2 (Probe Time)

	SELECT 
		cnf.loc_n, 
		cnf.loc_name,
		YMD(YEAR(tr.ts),MONTH(tr.ts),1) mth, 
		fs, 
		n, 
		COUNT(*) cnt, 
		SUM(amt) amt 
	FROM ml
	inner join cnf
		on	cnf.loc_n = ml.loc_n
	JOIN tr 
		ON ml.loc_n = tr.loc_n 
		AND ml.id = tr.id 
	JOIN trmisc 
		ON tr.loc_n = trmisc.loc_n 
		AND tr.id = trmisc.id 
		AND tr.tr_seq = trmisc.tr_seq 
	WHERE tr.loc_n IN (select loc_n from #locations)
		AND tr.type = 119 
		AND trmisc.amt > 0 
		AND ml.ts >= FromDate
		AND ml.ts <= ToDate
		AND DateType = 2
	GROUP BY 
		cnf.loc_n, 
		cnf.loc_name,
		YMD(YEAR(tr.ts),MONTH(tr.ts),1), 
		fs, 
		n

	UNION

	// Date Type = 3 (Transit Day)
	
	SELECT 
		cnf.loc_n, 
		cnf.loc_name,
		YMD(YEAR(tr.ts),MONTH(tr.ts),1) mth, 
		fs, 
		n, 
		COUNT(*) cnt, 
		SUM(amt) amt 
	FROM ml
	inner join cnf
		on	cnf.loc_n = ml.loc_n
	JOIN tr 
		ON ml.loc_n = tr.loc_n 
		AND ml.id = tr.id 
	JOIN trmisc 
		ON tr.loc_n = trmisc.loc_n 
		AND tr.id = trmisc.id 
		AND tr.tr_seq = trmisc.tr_seq 
	WHERE tr.loc_n IN (select loc_n from #locations)
		AND tr.type = 119 
		AND trmisc.amt > 0 
		AND ml.tday >= cast(FromDate as DATE)
		AND ml.tday < cast(ToDate as DATE)
		AND DateType = 3
	GROUP BY 
		cnf.loc_n, 
		cnf.loc_name,
		YMD(YEAR(tr.ts),MONTH(tr.ts),1), 
		fs, 
		n
	;
End
go

grant execute on dba.gfisp_Cash_Transaction_Summary_report to public;
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_TVM_Cash_On_Hand_Report') then
   drop proc gfisp_TVM_Cash_On_Hand_Report;
end if;
go

Create Procedure dba.gfisp_TVM_Cash_On_Hand_Report
as
Begin

select 
	LOC.name as Location,
	TVM.eq_desc as TVM,
	case TRIM0.name when 'Ride Ticket' then trim0_ct else 0 end +
	case TRIM1.name when 'Ride Ticket' then trim1_ct else 0 end +
	case TRIM2.name when 'Ride Ticket' then trim2_ct else 0 end +
	case TRIM3.name when 'Ride Ticket' then trim3_ct else 0 end		as Ride_Ticket_Count,
	case TRIM0.name when 'Metro Money' then trim0_ct else 0 end +
	case TRIM1.name when 'Metro Money' then trim1_ct else 0 end +
	case TRIM2.name when 'Metro Money' then trim2_ct else 0 end +
	case TRIM3.name when 'Metro Money' then trim3_ct else 0 end		as Metro_Money_Count,
	case TRIM0.name when 'Q-Card' then trim0_ct else 0 end +
	case TRIM1.name when 'Q-Card' then trim1_ct else 0 end +
	case TRIM2.name when 'Q-Card' then trim2_ct else 0 end +
	case TRIM3.name when 'Q-Card' then trim3_ct else 0 end		as Q_Card_Count,
	ST.cbx_c5 as Coinbox_05_Count,
	ST.cbx_c10 as Coinbox_10_Count,
	ST.cbx_c25 as Coinbox_25_Count,
	ST.cbx_c100 as Coinbox_$_Count,
	cast( (ST.cbx_c5 * 5) + (ST.cbx_c10 * 10) + (ST.cbx_c25 * 25) + (ST.cbx_c100 * 100) as money) / 100 as [Coinbox_Total_$s],
	ST.stkr_b1 as Bill_Stacker_$_Count,
	ST.stkr_b2 as Bill_Stacker_$2_Count,
	ST.stkr_b5 as Bill_Stacker_$5_Count,
	ST.stkr_b10 as Bill_Stacker_$10_Count,
	ST.stkr_b20 as Bill_Stacker_$20_Count,
	ST.stkr_b50 as Bill_Stacker_$50_Count,
	ST.stkr_b100 as Bill_Stacker_$100_Count,
	cast( ST.stkr_b1 + (ST.stkr_b2 * 2) + (ST.stkr_b5 * 5) + (ST.stkr_b10 * 10) + (ST.stkr_b20 * 20) + (ST.stkr_b50 * 50) + (ST.stkr_b100 * 100) as money) as [Bill_Stacker_Total_$s],
	ST.cmt_c5 as Coin_Tube_05_Count,
	ST.cmt_c10 as Coin_Tube_10_Count,
	ST.cmt_c25 as Coin_Tube_25_Count,
	ST.cmt_c50 as Coin_Tube_50_Count,
	ST.cmt_c100 as Coin_Tube_$_Count,
	cast( (ST.cmt_c5 * 5) + (ST.cmt_c10 * 10) + (ST.cmt_c25 * 25) + (ST.cmt_c50 * 50) + (ST.cmt_c100 * 100) as money) / 100 as [Coin_Tube_Total_$s],
	case HOP1.name
		when 'SBA' then 'Dollar'
		else HOP1.name
	end as Hopper_1_Coin_Type,
	ST.hpr1_ct as Hopper_1_Count,
	case HOP2.name
		when 'SBA' then 'Dollar'
		else HOP2.name
	end as Hopper_2_Coin_Type,
	ST.hpr2_ct as Hopper_2_Count,
	cast(	ST.hpr1_ct * case HOP1.name
								when 'Nickel' then 5
								when 'Dime' then 10
								when 'Quarter' then 25
								when 'SBA' then 100
								else 0
							end
			as money ) / 100 +
	cast(	ST.hpr2_ct * case HOP2.name
								when 'Nickel' then 5
								when 'Dime' then 10
								when 'Quarter' then 25
								when 'SBA' then 100
								else 0
							end
			as money ) / 100 as [Hopper_Total_$s],
	(	(	cast( (ST.cbx_c5 * 5) + (ST.cbx_c10 * 10) + (ST.cbx_c25 * 25) + (ST.cbx_c100 * 100) as money) / 100 )
		+ (	cast( ST.stkr_b1 + (ST.stkr_b2 * 2) + (ST.stkr_b5 * 5) + (ST.stkr_b10 * 10) + (ST.stkr_b20 * 20) + (ST.stkr_b50 * 50) + (ST.stkr_b100 * 100) as money) )
		+ (	cast( (ST.cmt_c5 * 5) + (ST.cmt_c10 * 10) + (ST.cmt_c25 * 25) + (ST.cmt_c50 * 50) + (ST.cmt_c100 * 100) as money) / 100 )
		+ (	cast(	ST.hpr1_ct * case HOP1.name
										when 'Nickel' then 5
										when 'Dime' then 10
										when 'Quarter' then 25
										when 'SBA' then 100
										else 0
									end
					as money ) / 100 )
		+ (		cast(	ST.hpr2_ct * case HOP2.name
										when 'Nickel' then 5
										when 'Dime' then 10
										when 'Quarter' then 25
										when 'SBA' then 100
										else 0
									end
					as money ) / 100 )
	)	as Total_Cash_On_Hand
from gfi_lst LOC 
join gfi_eq TVM 
	on LOC.code = TVM.loc_n
join gfi_eq_status ST
	on TVM.loc_n = ST.loc_n
	and TVM.eq_n = ST.eq_n
left join gfi_lst HOP1
	on HOP1.type = 'STOCK TYPE'
	and ST.hpr1_stock = HOP1.code
left join gfi_lst HOP2
	on HOP2.type = 'STOCK TYPE'
	and ST.hpr2_stock = HOP2.code
left join gfi_lst TRIM0
	on TRIM0.type = 'STOCK TYPE'
	and ST.trim0_stock = TRIM0.code
	and ST.trim0_ct > 0
left join gfi_lst TRIM1
	on TRIM1.type = 'STOCK TYPE'
	and ST.trim1_stock = TRIM1.code
	and ST.trim1_ct > 0
left join gfi_lst TRIM2
	on TRIM2.type = 'STOCK TYPE'
	and ST.trim2_stock = TRIM2.code
	and ST.trim2_ct > 0
left join gfi_lst TRIM3
	on TRIM3.type = 'STOCK TYPE'
	and ST.trim3_stock = TRIM3.code
	and ST.trim3_ct > 0
where LOC.type='EQ LOC' 
	AND LOC.code>0
order by 
	LOC.name,
	TVM.eq_desc

End;
go

grant execute on gfisp_TVM_Cash_On_Hand_Report to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_ride_count_value_report') then
   drop proc gfisp_ride_count_value_report;
end if;
go

create procedure dba.gfisp_ride_count_value_report
(	in_FromDate		date			default NULL,	
	in_ToDate		date			default NULL,	
	in_Bus			varchar(8000)	default NULL,	
	in_TranType		varchar(8000)	default NULL,	
	in_SerialNum	varchar(25)		default NULL,
	in_PayType		varchar(8)		default NULL
)	
begin
	declare stringvar varchar(20);
	declare ind int;
      DECLARE in_dump INT ;

  
  SELECT   CASE 
                       WHEN  gfisf_GetCloudSetting() = 'No' THEN 64 
                       ELSE 255 
                     END into in_dump;
create table #TransType (TransType int);
	if (in_TranType is not null)
	then
		set ind = charindex(',',in_TranType);
		while ind > 0
		loop
			  set stringvar = substring(in_TranType,1,ind-1);
			  set in_TranType = substring(in_TranType,ind+1,length(in_TranType)-ind);
			  insert into #TransType values (stringvar);
			  set ind = charindex(',',in_TranType);
		end loop;
		set stringvar = in_TranType;
		insert into #TransType values (stringvar);
	end if;

	create table #Bus (Bus int);

	if(in_Bus is not null)
	then
		set ind = charindex(',',in_Bus);
		while ind > 0
		loop
			  set stringvar = substring(in_Bus,1,ind-1);
			  set in_Bus = substring(in_Bus,ind+1,length(in_Bus)-ind);
			  insert into #Bus values (stringvar);
			  set ind = charindex(',',in_Bus);
		end loop;
		set stringvar = in_Bus;
		insert into #Bus values (stringvar);
	end if;

	  SELECT   DISTINCT  ATD.description                          AS [Key Type], 
          convert(datetime, (convert(varchar,ATD.ts,112))) as Ride_Date,
           convert(char(8), ATD.ts, 8) as Ride_Time,
             ATD.bus, 
             ATD.drv                                  AS Driver, 
             ATD.[route], 
             ATD.run, 
             ATD.trip, 
             ATD.dir                                  AS Direction, 
             ATD.fs, 
             CASE 
               WHEN ( farecell.farecell_n BETWEEN 1 AND 4 ) 
                    AND et.type = 119 THEN Cast(farecell.farecell_n AS VARCHAR) 
               ELSE 'NA' 
             END                                      AS [Key], 
             CASE 
               WHEN svd.ttp IS NOT NULL THEN Cast(svd.ttp AS VARCHAR) 
               WHEN farecell.m_ndx > 0 THEN Cast(farecell.m_ndx AS VARCHAR) 
               ELSE 'NA' 
             END                                      AS TTP, 
             CASE 
               WHEN et.type = 193 THEN 'CARD SV' 
               WHEN et.type = 119 
                    AND farecell.farecell_n IN ( 3, 4 ) THEN 'TALLY' 
               WHEN et.type = 119 
                    AND farecell.farecell_n IN ( 1, 2, 45, 46, 
                                                 47, 48, 49, 50 ) THEN 'CASH' 
               ELSE 'UNKNOWN' 
             END                                      AS [Pay Type], 
             Isnull(trk2, 'NA')                       AS [Serial Number], 
             Isnull(0 - svd.deduction, 0)             AS Deduction, 
             Isnull(svd.remval, 0)                    AS Remaining, 
--if a token is used then exclude transaction from revenue calculation
           case when exists (select id from AllTransData a join et e on e.type = a.type  where a.id=atd.id and e.text = 'Token or ticket' )
           then 0
           else atd.amt end AS Cash,
             CASE --exclude dump transactions
               WHEN( n = in_dump OR  ATD.TEXT = 'Token or ticket') THEN 0 
               ELSE 1
             END                                      AS [Ride Count], 
             CASE 
               WHEN farecell.farecell_n IN ( 3, 4 ) THEN 1 
               ELSE 0 
             END                                      AS [Tally Count] ,ATD.tr_seq
      FROM   alltransdata ATD 
             JOIN et
               ON ATD.type = et.type 
             JOIN cnf 
               ON ATD.loc_n = cnf.loc_n 
               JOIN ridership R ON R.id = ATD.id AND R.loc_n = ATD.loc_n AND R.route <>''
             LEFT JOIN svd
                    ON ATD.loc_n = svd.loc_n 
                       AND ATD.id = svd.id 
                       AND ATD.tr_seq = svd.tr_seq 
             LEFT JOIN fareset 
                    ON ATD.loc_n = fareset.loc_n 
                       AND ATD.fs = fareset.fareset_n 
                       AND fareset.enabled_f = 'Y' 
                       AND cnf.fs_id = fareset.fs_id 
             LEFT JOIN farecell 
                    ON ATD.loc_n = farecell.loc_n 
                       AND ATD.fs = farecell.fareset_n 
                       AND farecell.enabled_f = 'Y' 
                       AND ATD.n = farecell.farecell_n 
                       AND cnf.fs_id = farecell.fs_id 
      WHERE  et.ridership = 'Y' AND (ATD.TEXT = 'Token or ticket' OR ATD.tbl_name IS NOT NULL) and included_f ='Y'
      --Below statement excludes probes that used tokens
    --  and not exists(select id from AllTransData a join et e on e.type = a.type  where a.id=atd.id and e.text = 'Token or ticket' and a.n <>@dump)
            and (in_FromDate is NULL or (ATD.ts >= in_FromDate and ATD.ts < in_ToDate ))
		and (in_bus is NULL or ATD.bus in (select * from #Bus))
		and (in_TranType is NULL or et.type in (select * from #TransType))
		and (in_SerialNum is NULL or in_SerialNum = isnull(ATD.card_id, 'NA'))
		and (in_PayType is NULL or in_PayType = case
												when et.type = 193 then 'CARD SV'
												when et.type = 119 and farecell.farecell_n in (3,4) then 'TALLY'
												when et.type = 119 and farecell.farecell_n in (1,2,45,46,47,48,49,50) then 'CASH'
												else 'UNKNOWN'
											end )
     -- ORDER  BY atd.ts 
end;
go

grant execute on gfisp_ride_count_value_report to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_ridership_data') then
	drop proc gfisp_get_ridership_data;
end if;
go

create procedure dba.gfisp_get_ridership_data (
	in in_locations varchar(8000),
	in in_dateBegin date,
	in in_dateEnd date,
	in in_onlyDiff char(1) default 'N',
	in in_routes varchar(8000) default '',
	in in_farecells varchar(8000) default '' )
begin
/*
call gfisp_get_ridership_data ('1,2,3', '2016-06-01', '2016-06-30');
call gfisp_get_ridership_data ('1,2,3', '2016-06-01', '2016-06-30', 'N', '1,2,3,5', '37,88');
call gfisp_get_ridership_data ('1,2,3', '2016-06-01', '2016-06-30', 'Y', '1,2,3,5', '37,88');
*/
	declare v_int int;
	declare v_cmd varchar(8000);

	set in_onlyDiff = isnull(upper(in_onlyDiff), 'N');
	set in_routes = isnull(in_routes, '');
	set in_farecells = isnull(in_farecells, '');
	
	create table #ridership_data(
		loc_n smallint,
		loc_name varchar(80),
		id int,
		tday datetime,
		ts datetime,
		bus int,
		cbx_n smallint,
		prb_n tinyint,
		[route] int,
		[unknown] varchar(1),
		farecell_number tinyint,
		farecell_name varchar(48),
		[value] int,
		ev_value int,
		[match] varchar(1)
	);
	
	create table #loc_ns (
		loc_n smallint,
		loc_name varchar(80)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	set in_dateBegin = isnull(in_dateBegin, '1900-01-01');
	set in_dateEnd = isnull(in_dateEnd, '2999-12-31');
	
	insert into #ridership_data
	select
		l.loc_n,
		l.loc_name,
		ml.id,
		ml.tday,
		ml.ts,
		ml.bus,
		ml.cbx_n,
		ml.prb_n,
		cast(r.[route] as int) as [route],
		case
			when rtelst.[route] is null then '*'
			else ' '
		end as [unknown],
		r.farecell_number,
		case
			when r.farecell_number > 14 then 'TTP' +  ltrim(str(r.farecell_number - 14)) + ' - '
			else ''
		end +
		case
			when r.farecell_name = '' then ltrim(str(r.farecell_number))
			else r.farecell_name
		end as farecell_name,
		cast(0 as int) as [value],
		sum(r.[value]) as ev_value,
		'Y' as [match]
	from #loc_ns as l
	inner join ml
		on	ml.loc_n = l.loc_n
	inner join ridership as r
		on	r.loc_n = ml.loc_n
		and	r.id = ml.id
	left join rtelst
		on	rtelst.loc_n = ml.loc_n
		and	rtelst.[route] = cast(r.[route] as int)
	where	ml.tday between in_dateBegin and in_dateEnd
		and	r.[route] <> ''
	group by
		l.loc_n,
		l.loc_name,
		ml.id,
		ml.tday,
		ml.ts,
		ml.bus,
		ml.cbx_n,
		ml.prb_n,
		r.[route],
		rtelst.[route],
		r.farecell_number,
		r.farecell_name
	;
	
	update #ridership_data set
		[value] =	(
					select isnull(sum(r.[value]), 0)
					from ridership as r
					where	r.loc_n = rd.loc_n
						and	r.id = rd.id
						and r.[route] = ''
				)
	from #ridership_data as rd;
	
	update #ridership_data set
		[match] =	case	
						when [value] = (
								select isnull(sum(r.[value]), 0)
								from ridership as r
								where	r.loc_n = rd.loc_n
									and	r.id = rd.id
									and r.[route] <> ''
							) then 'Y'
						else 'N'
					end
	from #ridership_data as rd;
	
	if in_onlyDiff = 'Y' then
		delete from #ridership_data
		where	[match] = 'Y';
	end if;

	if	in_routes <> ''
	and	in_routes <> 'All' then
		set v_cmd = 'delete from #ridership_data where [route] not in ('	+ in_routes + ')';
		execute immediate v_cmd;
	end if;
	if	in_farecells <> ''
	and in_farecells <> 'All' then
		set v_cmd = 'delete from #ridership_data where farecell_number not in ('	+ in_farecells + ')';
		execute immediate v_cmd;
	end if;
	
	select
		loc_n,
		loc_name,
		[route],
		farecell_number,
		farecell_name,
		[value],
		ts,
		tday,
		[unknown],
		bus,
		cbx_n,
		prb_n,
		ev_value,
		[match]
	from #ridership_data
	order by
		loc_n,
		[route],
		tday,
		ts,
		farecell_number
	;
end;
go

grant execute on dba.gfisp_get_ridership_data to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_ridership_farecells') then
	drop proc gfisp_get_ridership_farecells;
end if;
go

create procedure dba.gfisp_get_ridership_farecells (
	in in_locations varchar(8000) )
begin
/*
call gfisp_get_ridership_farecells ('1,2,3');
*/
	declare v_int int;
	
	create table #loc_ns (
		loc_n smallint,
		loc_name varchar(80)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	select
		id,
		case
			when id > 14 then 'TTP' +  ltrim(str(id - 14)) + ' - '
			else ''
		end
	+	case
			when name = '' then ltrim(str(id))
			else name
		end as name
	from
	(
		select
			coalesce(a.id, b.id) as id,
			coalesce(a.name, b.name) as name
		from
		(
			select
				r.farecell_n as id,
				ltrim(max(coalesce(nullif(r.farecell_id, ''), nullif(r.description, ''), ''))) as name
			from #loc_ns as l
			inner join cnf with (nolock)
				on	cnf.loc_n = l.loc_n
			inner join rdr as r with (nolock)
				on	r.loc_n = l.loc_n
				and	r.rdr_id = cnf.fs_id
			where	r.included_f = 'Y'
			group by r.farecell_n
		) as a
		full join
		(
			select
				cast(r.farecell_number as int) as id,
				ltrim(max(r.farecell_name)) + ' *' as name
			from #loc_ns as l
			inner join ridership as r with (nolock)
				on	r.loc_n = l.loc_n
			group by r.farecell_number
		) as b	on	b.id = a.id
	) as c
	order by
		id
	;
end;
go

grant execute on dba.gfisp_get_ridership_farecells to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_ridership_routes') then
	drop proc gfisp_get_ridership_routes;
end if;
go

create procedure dba.gfisp_get_ridership_routes (
	in in_locations varchar(8000) )
begin
/*
call gfisp_get_ridership_routes ('1,2,3');
*/
	declare v_int int;
	
	create table #loc_ns (
		loc_n smallint,
		loc_name varchar(80)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	select
		coalesce(a.id, b.id) as id,
		coalesce(a.name, b.name) as name
	from
	(
		select distinct
			r.[route] as id,
			ltrim(str(r.[route])) as name
		from #loc_ns as l
		inner join rtelst as r
			on	r.loc_n = l.loc_n
	) as a
	full join
	(
		select distinct
			cast(r.[route] as int) as id,
			r.[route] + ' *' as name
		from #loc_ns as l
		inner join ridership as r
			on	r.loc_n = l.loc_n
		where	r.[route] <> ''
	) as b	on	b.id = a.id
	order by
		id
	;
end;
go

grant execute on dba.gfisp_get_ridership_routes to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_ridership_summary') then
	drop proc gfisp_get_ridership_summary;
end if;
go

create procedure dba.gfisp_get_ridership_summary (
	in in_locations varchar(8000),
	in in_dateBegin date,
	in in_dateEnd date,
	in in_onlyDiff char(1) default 'N' )
begin
/*
call gfisp_get_ridership_summary ('1,2,3', '2016-06-01', '2016-06-30');
call gfisp_get_ridership_summary ('1,2,3', '2016-06-01', '2016-06-30', 'N');
call gfisp_get_ridership_summary ('1,2,3', '2016-06-01', '2016-06-30', 'Y');
*/
	declare v_int int;

	set in_onlyDiff = isnull(upper(in_onlyDiff), 'N');
	
	create table #loc_ns (
		loc_n smallint,
		loc_name varchar(80)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	set in_dateBegin = isnull(in_dateBegin, '1900-01-01');
	set in_dateEnd = isnull(in_dateEnd, '2999-12-31');
	
	select
		loc_n,
		loc_name,
		tday,
		[value],
		nullif(ev_value, [value]) as ev_value,
		case
			when [value] = ev_value then 'Y'
			else 'N'
		end as [match]
	from
	(
		select
			coalesce(ml.loc_n, ev.loc_n) as loc_n,
			coalesce(ml.loc_name, ev.loc_name) as loc_name,
			coalesce(ml.tday, ev.tday) as tday,
			isnull(ml.[value], 0) as [value],
			isnull(ev.[value], 0) as ev_value
		from
		(
			select
				l.loc_n,
				l.loc_name,
				ml.tday,
				sum(r.[value]) as [value]
			from #loc_ns as l
			inner join ml
				on	ml.loc_n = l.loc_n
			inner join ridership as r
				on	r.loc_n = ml.loc_n
				and	r.id = ml.id
				and	r.[route] = ''
			where	ml.tday between in_dateBegin and in_dateEnd
			group by
				l.loc_n,
				l.loc_name,
				ml.tday
		) as ml
		full join
		(
			select
				l.loc_n,
				l.loc_name,
				ml.tday,
				sum(r.[value]) as [value]
			from #loc_ns as l
			inner join ml
				on	ml.loc_n = l.loc_n
			inner join ridership as r
				on	r.loc_n = ml.loc_n
				and	r.id = ml.id
				and	r.[route] <> ''
			where	ml.tday between in_dateBegin and in_dateEnd
			group by
				l.loc_n,
				l.loc_name,
				ml.tday
		) as ev	on	ev.loc_n = ml.loc_n
				and	ev.tday = ml.tday
	) as a
	where	(
				in_onlyDiff = 'N'
			or	[value] <> ev_value
			)
	order by
		loc_n,
		tday
	;
end;
go

grant execute on dba.gfisp_get_ridership_summary to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_get_ridership_summary_probing') then
	drop proc gfisp_get_ridership_summary_probing;
end if;
go

create procedure dba.gfisp_get_ridership_summary_probing (
	in in_locations varchar(8000),
	in in_dateBegin date,
	in in_dateEnd date,
	in in_onlyDiff char(1) default 'N' )
begin
/*
call gfisp_get_ridership_summary_probing ('1,2,3', '2016-06-01', '2016-06-30');
call gfisp_get_ridership_summary_probing ('1,2,3', '2016-06-01', '2016-06-30', 'N');
call gfisp_get_ridership_summary_probing ('1,2,3', '2016-06-01', '2016-06-30', 'Y');
*/
	declare v_int int;

	set in_onlyDiff = isnull(upper(in_onlyDiff), 'N');
	
	create table #loc_ns (
		loc_n smallint,
		loc_name varchar(80)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	set in_dateBegin = isnull(in_dateBegin, '1900-01-01');
	set in_dateEnd = isnull(in_dateEnd, '2999-12-31');
	
	select
		loc_n,
		loc_name,
		tday,
		ts,
		bus,
		cbx_n,
		prb_n,
		[value],
		nullif(ev_value, [value]) as ev_value,
		case
			when value = ev_value then 'Y'
			else 'N'
		end as [match]
	from
	(
		select
			coalesce(ml.loc_n, ev.loc_n) as loc_n,
			coalesce(ml.loc_name, ev.loc_name) as loc_name,
			coalesce(ml.tday, ev.tday) as tday,
			coalesce(ml.ts, ev.ts) as ts,
			coalesce(ml.bus, ev.bus) as bus,
			coalesce(ml.cbx_n, ev.cbx_n) as cbx_n,
			coalesce(ml.prb_n, ev.prb_n) as prb_n,
			isnull(ml.[value], 0) as [value],
			isnull(ev.[value], 0) as ev_value
		from
		(
			select
				l.loc_n,
				l.loc_name,
				ml.tday,
				ml.ts,
				ml.bus,
				ml.cbx_n,
				ml.prb_n,
				sum(r.[value]) as [value]
			from #loc_ns as l
			inner join ml
				on	ml.loc_n = l.loc_n
			inner join ridership as r
				on	r.loc_n = ml.loc_n
				and	r.id = ml.id
				and	r.[route] = ''
			where	ml.tday between in_dateBegin and in_dateEnd
			group by
				l.loc_n,
				l.loc_name,
				ml.tday,
				ml.ts,
				ml.bus,
				ml.cbx_n,
				ml.prb_n
		) as ml
		full join
		(
			select
				l.loc_n,
				l.loc_name,
				ml.tday,
				ml.ts,
				ml.bus,
				ml.cbx_n,
				ml.prb_n,
				sum(r.[value]) as [value]
			from #loc_ns as l
			inner join ml
				on	ml.loc_n = l.loc_n
			inner join ridership as r
				on	r.loc_n = ml.loc_n
				and	r.id = ml.id
				and	r.[route] <> ''
			where	ml.tday between in_dateBegin and in_dateEnd
			group by
				l.loc_n,
				l.loc_name,
				ml.tday,
				ml.ts,
				ml.bus,
				ml.cbx_n,
				ml.prb_n
		) as ev	on	ev.loc_n = ml.loc_n
				and	ev.tday = ml.tday
				and	ev.ts = ml.ts
	) as a
	where	(
				in_onlyDiff = 'N'
			or	[value] <> ev_value
			)
	order by
		loc_n,
		tday,
		ts
	;
end;
go

grant execute on dba.gfisp_get_ridership_summary_probing to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_populate_ridership_data') then
	drop proc gfisp_populate_ridership_data;
end if;
go

create procedure dba.gfisp_populate_ridership_data (
	in in_loc_n smallint,
	in in_id int,
	in in_override char(1) default 'N' )
begin
/*
call gfisp_populate_ridership_data (1, 123);
call gfisp_populate_ridership_data (1, 123, 'N');
call gfisp_populate_ridership_data (1, 123, 'Y');
*/
	declare v_cmd varchar(8000);
	declare v_column varchar(250);
	declare v_int int;
	declare v_max int;
	declare v_value int;

	set v_int = 0;
	set v_max = gfisf_TTP_Number() + 14;

	set in_override = isnull(upper(in_override), 'N');
	
	if in_override = 'N' then
		if exists	(
						select null from ridership
						where	loc_n = in_loc_n
							and	id = in_id
					)
		then
			return;
		end if;
	else
		delete from ridership
		where	loc_n = in_loc_n
			and	id = in_id
		;
	end if;
	
	create table #ridership (
		loc_n smallint,
		id int,
		[route] varchar(10),
		value_0 int,
		value_1 int,
		value_2 int,
		value_3 int,
		value_4 int,
		value_5 int,
		value_6 int,
		value_7 int,
		value_8 int,
		value_9 int,
		value_10 int,
		value_11 int,
		value_12 int,
		value_13 int,
		value_14 int,
		value_15 int,
		value_16 int,
		value_17 int,
		value_18 int,
		value_19 int,
		value_20 int,
		value_21 int,
		value_22 int,
		value_23 int,
		value_24 int,
		value_25 int,
		value_26 int,
		value_27 int,
		value_28 int,
		value_29 int,
		value_30 int,
		value_31 int,
		value_32 int,
		value_33 int,
		value_34 int,
		value_35 int,
		value_36 int,
		value_37 int,
		value_38 int,
		value_39 int,
		value_40 int,
		value_41 int,
		value_42 int,
		value_43 int,
		value_44 int,
		value_45 int,
		value_46 int,
		value_47 int,
		value_48 int,
		value_49 int,
		value_50 int,
		value_51 int,
		value_52 int,
		value_53 int,
		value_54 int,
		value_55 int,
		value_56 int,
		value_57 int,
		value_58 int,
		value_59 int,
		value_60 int,
		value_61 int,
		value_62 int,
		value_63 int,
		value_64 int,
		value_65 int,
		value_66 int,
		value_67 int,
		value_68 int,
		value_69 int,
		value_70 int,
		value_71 int,
		value_72 int,
		value_73 int,
		value_74 int,
		value_75 int,
		value_76 int,
		value_77 int,
		value_78 int,
		value_79 int,
		value_80 int,
		value_81 int,
		value_82 int,
		value_83 int,
		value_84 int,
		value_85 int,
		value_86 int,
		value_87 int,
		value_88 int,
		value_89 int,
		value_90 int,
		value_91 int,
		value_92 int,
		value_93 int,
		value_94 int,
		value_95 int,
		value_96 int,
		value_97 int,
		value_98 int,
		value_99 int,
		value_100 int,
		value_101 int,
		value_102 int,
		value_103 int,
		value_104 int,
		value_105 int,
		value_106 int,
		value_107 int,
		value_108 int,
		value_109 int,
		value_110 int,
		value_111 int,
		value_112 int,
		value_113 int,
		value_114 int,
		value_115 int,
		value_116 int,
		value_117 int,
		value_118 int,
		value_119 int,
		value_120 int,
		value_121 int,
		value_122 int,
		value_123 int,
		value_124 int,
		value_125 int,
		value_126 int,
		value_127 int,
		value_128 int,
		value_129 int,
		value_130 int,
		value_131 int,
		value_132 int,
		value_133 int,
		value_134 int,
		value_135 int,
		value_136 int,
		value_137 int,
		value_138 int,
		value_139 int,
		value_140 int,
		value_141 int,
		value_142 int,
		value_143 int,
		value_144 int,
		value_145 int,
		value_146 int,
		value_147 int,
		value_148 int,
		value_149 int,
		value_150 int,
		value_151 int,
		value_152 int,
		value_153 int,
		value_154 int,
		value_155 int,
		value_156 int,
		value_157 int,
		value_158 int,
		value_159 int,
		value_160 int,
		value_161 int,
		value_162 int,
		value_163 int,
		value_164 int,
		value_165 int,
		value_166 int,
		value_167 int,
		value_168 int,
		value_169 int,
		value_170 int,
		value_171 int,
		value_172 int,
		value_173 int,
		value_174 int,
		value_175 int,
		value_176 int,
		value_177 int,
		value_178 int,
		value_179 int,
		value_180 int,
		value_181 int,
		value_182 int,
		value_183 int,
		value_184 int,
		value_185 int,
		value_186 int,
		value_187 int,
		value_188 int,
		value_189 int,
		value_190 int,
		value_191 int,
		value_192 int,
		value_193 int,
		value_194 int,
		value_195 int,
		value_196 int,
		value_197 int,
		value_198 int,
		value_199 int,
		value_200 int,
		value_201 int,
		value_202 int,
		value_203 int,
		value_204 int,
		value_205 int,
		value_206 int,
		value_207 int,
		value_208 int,
		value_209 int,
		value_210 int,
		value_211 int,
		value_212 int,
		value_213 int,
		value_214 int,
		value_215 int,
		value_216 int,
		value_217 int,
		value_218 int,
		value_219 int,
		value_220 int,
		value_221 int,
		value_222 int,
		value_223 int,
		value_224 int,
		value_225 int,
		value_226 int,
		value_227 int,
		value_228 int,
		value_229 int,
		value_230 int,
		value_231 int,
		value_232 int,
		value_233 int,
		value_234 int,
		value_235 int,
		value_236 int,
		value_237 int,
		value_238 int,
		value_239 int,
		value_240 int,
		value_241 int,
		value_242 int,
		value_243 int,
		value_244 int,
		value_245 int,
		value_246 int,
		value_247 int,
		value_248 int,
		value_249 int,
		value_250 int,
		value_251 int,
		value_252 int,
		value_253 int,
		value_254 int,
		value_255 int
	);
	
	insert into #ridership
	select
		ml.loc_n,
		ml.id,
		'' as [route],
		ml.fare_c,
		ml.key1,
		ml.key2,
		ml.key3,
		ml.key4,
		ml.key5,
		ml.key6,
		ml.key7,
		ml.key8,
		ml.key9,
		ml.keyast,
		ml.keya,
		ml.keyb,
		ml.keyc,
		ml.keyd,
		ml.ttp1,
		ml.ttp2,
		ml.ttp3,
		ml.ttp4,
		ml.ttp5,
		ml.ttp6,
		ml.ttp7,
		ml.ttp8,
		ml.ttp9,
		ml.ttp10,
		ml.ttp11,
		ml.ttp12,
		ml.ttp13,
		ml.ttp14,
		ml.ttp15,
		ml.ttp16,
		ml.ttp17,
		ml.ttp18,
		ml.ttp19,
		ml.ttp20,
		ml.ttp21,
		ml.ttp22,
		ml.ttp23,
		ml.ttp24,
		ml.ttp25,
		ml.ttp26,
		ml.ttp27,
		ml.ttp28,
		ml.ttp29,
		ml.ttp30,
		ml.ttp31,
		ml.ttp32,
		ml.ttp33,
		ml.ttp34,
		ml.ttp35,
		ml.ttp36,
		ml.ttp37,
		ml.ttp38,
		ml.ttp39,
		ml.ttp40,
		ml.ttp41,
		ml.ttp42,
		ml.ttp43,
		ml.ttp44,
		ml.ttp45,
		ml.ttp46,
		ml.ttp47,
		ml.ttp48,
		isnull(x.ttp49, 0),
		isnull(x.ttp50, 0),
		isnull(x.ttp51, 0),
		isnull(x.ttp52, 0),
		isnull(x.ttp53, 0),
		isnull(x.ttp54, 0),
		isnull(x.ttp55, 0),
		isnull(x.ttp56, 0),
		isnull(x.ttp57, 0),
		isnull(x.ttp58, 0),
		isnull(x.ttp59, 0),
		isnull(x.ttp50, 0),
		isnull(x.ttp61, 0),
		isnull(x.ttp62, 0),
		isnull(x.ttp63, 0),
		isnull(x.ttp64, 0),
		isnull(x.ttp65, 0),
		isnull(x.ttp66, 0),
		isnull(x.ttp67, 0),
		isnull(x.ttp68, 0),
		isnull(x.ttp69, 0),
		isnull(x.ttp70, 0),
		isnull(x.ttp71, 0),
		isnull(x.ttp72, 0),
		isnull(x.ttp73, 0),
		isnull(x.ttp74, 0),
		isnull(x.ttp75, 0),
		isnull(x.ttp76, 0),
		isnull(x.ttp77, 0),
		isnull(x.ttp78, 0),
		isnull(x.ttp79, 0),
		isnull(x.ttp80, 0),
		isnull(x.ttp81, 0),
		isnull(x.ttp82, 0),
		isnull(x.ttp83, 0),
		isnull(x.ttp84, 0),
		isnull(x.ttp85, 0),
		isnull(x.ttp86, 0),
		isnull(x.ttp87, 0),
		isnull(x.ttp88, 0),
		isnull(x.ttp89, 0),
		isnull(x.ttp90, 0),
		isnull(x.ttp91, 0),
		isnull(x.ttp92, 0),
		isnull(x.ttp93, 0),
		isnull(x.ttp94, 0),
		isnull(x.ttp95, 0),
		isnull(x.ttp96, 0),
		isnull(x.ttp97, 0),
		isnull(x.ttp98, 0),
		isnull(x.ttp99, 0),
		isnull(x.ttp100, 0),
		isnull(x.ttp101, 0),
		isnull(x.ttp102, 0),
		isnull(x.ttp103, 0),
		isnull(x.ttp104, 0),
		isnull(x.ttp105, 0),
		isnull(x.ttp106, 0),
		isnull(x.ttp107, 0),
		isnull(x.ttp108, 0),
		isnull(x.ttp109, 0),
		isnull(x.ttp110, 0),
		isnull(x.ttp111, 0),
		isnull(x.ttp112, 0),
		isnull(x.ttp113, 0),
		isnull(x.ttp114, 0),
		isnull(x.ttp115, 0),
		isnull(x.ttp116, 0),
		isnull(x.ttp117, 0),
		isnull(x.ttp118, 0),
		isnull(x.ttp119, 0),
		isnull(x.ttp120, 0),
		isnull(x.ttp121, 0),
		isnull(x.ttp122, 0),
		isnull(x.ttp123, 0),
		isnull(x.ttp124, 0),
		isnull(x.ttp125, 0),
		isnull(x.ttp126, 0),
		isnull(x.ttp127, 0),
		isnull(x.ttp128, 0),
		isnull(x.ttp129, 0),
		isnull(x.ttp130, 0),
		isnull(x.ttp131, 0),
		isnull(x.ttp132, 0),
		isnull(x.ttp133, 0),
		isnull(x.ttp134, 0),
		isnull(x.ttp135, 0),
		isnull(x.ttp136, 0),
		isnull(x.ttp137, 0),
		isnull(x.ttp138, 0),
		isnull(x.ttp139, 0),
		isnull(x.ttp140, 0),
		isnull(x.ttp141, 0),
		isnull(x.ttp142, 0),
		isnull(x.ttp143, 0),
		isnull(x.ttp144, 0),
		isnull(x.ttp145, 0),
		isnull(x.ttp146, 0),
		isnull(x.ttp147, 0),
		isnull(x.ttp148, 0),
		isnull(x.ttp149, 0),
		isnull(x.ttp150, 0),
		isnull(x.ttp151, 0),
		isnull(x.ttp152, 0),
		isnull(x.ttp153, 0),
		isnull(x.ttp154, 0),
		isnull(x.ttp155, 0),
		isnull(x.ttp156, 0),
		isnull(x.ttp157, 0),
		isnull(x.ttp158, 0),
		isnull(x.ttp159, 0),
		isnull(x.ttp150, 0),
		isnull(x.ttp161, 0),
		isnull(x.ttp162, 0),
		isnull(x.ttp163, 0),
		isnull(x.ttp164, 0),
		isnull(x.ttp165, 0),
		isnull(x.ttp166, 0),
		isnull(x.ttp167, 0),
		isnull(x.ttp168, 0),
		isnull(x.ttp169, 0),
		isnull(x.ttp170, 0),
		isnull(x.ttp171, 0),
		isnull(x.ttp172, 0),
		isnull(x.ttp173, 0),
		isnull(x.ttp174, 0),
		isnull(x.ttp175, 0),
		isnull(x.ttp176, 0),
		isnull(x.ttp177, 0),
		isnull(x.ttp178, 0),
		isnull(x.ttp179, 0),
		isnull(x.ttp180, 0),
		isnull(x.ttp181, 0),
		isnull(x.ttp182, 0),
		isnull(x.ttp183, 0),
		isnull(x.ttp184, 0),
		isnull(x.ttp185, 0),
		isnull(x.ttp186, 0),
		isnull(x.ttp187, 0),
		isnull(x.ttp188, 0),
		isnull(x.ttp189, 0),
		isnull(x.ttp190, 0),
		isnull(x.ttp191, 0),
		isnull(x.ttp192, 0),
		isnull(x.ttp193, 0),
		isnull(x.ttp194, 0),
		isnull(x.ttp195, 0),
		isnull(x.ttp196, 0),
		isnull(x.ttp197, 0),
		isnull(x.ttp198, 0),
		isnull(x.ttp199, 0),
		isnull(x.ttp200, 0),
		isnull(x.ttp201, 0),
		isnull(x.ttp202, 0),
		isnull(x.ttp203, 0),
		isnull(x.ttp204, 0),
		isnull(x.ttp205, 0),
		isnull(x.ttp206, 0),
		isnull(x.ttp207, 0),
		isnull(x.ttp208, 0),
		isnull(x.ttp209, 0),
		isnull(x.ttp210, 0),
		isnull(x.ttp211, 0),
		isnull(x.ttp212, 0),
		isnull(x.ttp213, 0),
		isnull(x.ttp214, 0),
		isnull(x.ttp215, 0),
		isnull(x.ttp216, 0),
		isnull(x.ttp217, 0),
		isnull(x.ttp218, 0),
		isnull(x.ttp219, 0),
		isnull(x.ttp220, 0),
		isnull(x.ttp221, 0),
		isnull(x.ttp222, 0),
		isnull(x.ttp223, 0),
		isnull(x.ttp224, 0),
		isnull(x.ttp225, 0),
		isnull(x.ttp226, 0),
		isnull(x.ttp227, 0),
		isnull(x.ttp228, 0),
		isnull(x.ttp229, 0),
		isnull(x.ttp230, 0),
		isnull(x.ttp231, 0),
		isnull(x.ttp232, 0),
		isnull(x.ttp233, 0),
		isnull(x.ttp234, 0),
		isnull(x.ttp235, 0),
		isnull(x.ttp236, 0),
		isnull(x.ttp237, 0),
		isnull(x.ttp238, 0),
		isnull(x.ttp239, 0),
		isnull(x.ttp240, 0),
		isnull(x.ttp241, 0)
	from ml
	left join ml_extd as x
		on	x.loc_n = ml.loc_n
		and	x.id = ml.id
	where	ml.loc_n = in_loc_n
		and	ml.id = in_id
	;
	
	insert into #ridership
	select
		ev.loc_n,
		ev.id,
		ltrim(str(ev.[route])),
		sum(ev.fare_c),
		sum(ev.key1),
		sum(ev.key2),
		sum(ev.key3),
		sum(ev.key4),
		sum(ev.key5),
		sum(ev.key6),
		sum(ev.key7),
		sum(ev.key8),
		sum(ev.key9),
		sum(ev.keyast),
		sum(ev.keya),
		sum(ev.keyb),
		sum(ev.keyc),
		sum(ev.keyd),
		sum(ev.ttp1),
		sum(ev.ttp2),
		sum(ev.ttp3),
		sum(ev.ttp4),
		sum(ev.ttp5),
		sum(ev.ttp6),
		sum(ev.ttp7),
		sum(ev.ttp8),
		sum(ev.ttp9),
		sum(ev.ttp10),
		sum(ev.ttp11),
		sum(ev.ttp12),
		sum(ev.ttp13),
		sum(ev.ttp14),
		sum(ev.ttp15),
		sum(ev.ttp16),
		sum(ev.ttp17),
		sum(ev.ttp18),
		sum(ev.ttp19),
		sum(ev.ttp20),
		sum(ev.ttp21),
		sum(ev.ttp22),
		sum(ev.ttp23),
		sum(ev.ttp24),
		sum(ev.ttp25),
		sum(ev.ttp26),
		sum(ev.ttp27),
		sum(ev.ttp28),
		sum(ev.ttp29),
		sum(ev.ttp30),
		sum(ev.ttp31),
		sum(ev.ttp32),
		sum(ev.ttp33),
		sum(ev.ttp34),
		sum(ev.ttp35),
		sum(ev.ttp36),
		sum(ev.ttp37),
		sum(ev.ttp38),
		sum(ev.ttp39),
		sum(ev.ttp40),
		sum(ev.ttp41),
		sum(ev.ttp42),
		sum(ev.ttp43),
		sum(ev.ttp44),
		sum(ev.ttp45),
		sum(ev.ttp46),
		sum(ev.ttp47),
		sum(ev.ttp48),
		sum(isnull(x.ttp49, 0)),
		sum(isnull(x.ttp50, 0)),
		sum(isnull(x.ttp51, 0)),
		sum(isnull(x.ttp52, 0)),
		sum(isnull(x.ttp53, 0)),
		sum(isnull(x.ttp54, 0)),
		sum(isnull(x.ttp55, 0)),
		sum(isnull(x.ttp56, 0)),
		sum(isnull(x.ttp57, 0)),
		sum(isnull(x.ttp58, 0)),
		sum(isnull(x.ttp59, 0)),
		sum(isnull(x.ttp50, 0)),
		sum(isnull(x.ttp61, 0)),
		sum(isnull(x.ttp62, 0)),
		sum(isnull(x.ttp63, 0)),
		sum(isnull(x.ttp64, 0)),
		sum(isnull(x.ttp65, 0)),
		sum(isnull(x.ttp66, 0)),
		sum(isnull(x.ttp67, 0)),
		sum(isnull(x.ttp68, 0)),
		sum(isnull(x.ttp69, 0)),
		sum(isnull(x.ttp70, 0)),
		sum(isnull(x.ttp71, 0)),
		sum(isnull(x.ttp72, 0)),
		sum(isnull(x.ttp73, 0)),
		sum(isnull(x.ttp74, 0)),
		sum(isnull(x.ttp75, 0)),
		sum(isnull(x.ttp76, 0)),
		sum(isnull(x.ttp77, 0)),
		sum(isnull(x.ttp78, 0)),
		sum(isnull(x.ttp79, 0)),
		sum(isnull(x.ttp80, 0)),
		sum(isnull(x.ttp81, 0)),
		sum(isnull(x.ttp82, 0)),
		sum(isnull(x.ttp83, 0)),
		sum(isnull(x.ttp84, 0)),
		sum(isnull(x.ttp85, 0)),
		sum(isnull(x.ttp86, 0)),
		sum(isnull(x.ttp87, 0)),
		sum(isnull(x.ttp88, 0)),
		sum(isnull(x.ttp89, 0)),
		sum(isnull(x.ttp90, 0)),
		sum(isnull(x.ttp91, 0)),
		sum(isnull(x.ttp92, 0)),
		sum(isnull(x.ttp93, 0)),
		sum(isnull(x.ttp94, 0)),
		sum(isnull(x.ttp95, 0)),
		sum(isnull(x.ttp96, 0)),
		sum(isnull(x.ttp97, 0)),
		sum(isnull(x.ttp98, 0)),
		sum(isnull(x.ttp99, 0)),
		sum(isnull(x.ttp100, 0)),
		sum(isnull(x.ttp101, 0)),
		sum(isnull(x.ttp102, 0)),
		sum(isnull(x.ttp103, 0)),
		sum(isnull(x.ttp104, 0)),
		sum(isnull(x.ttp105, 0)),
		sum(isnull(x.ttp106, 0)),
		sum(isnull(x.ttp107, 0)),
		sum(isnull(x.ttp108, 0)),
		sum(isnull(x.ttp109, 0)),
		sum(isnull(x.ttp110, 0)),
		sum(isnull(x.ttp111, 0)),
		sum(isnull(x.ttp112, 0)),
		sum(isnull(x.ttp113, 0)),
		sum(isnull(x.ttp114, 0)),
		sum(isnull(x.ttp115, 0)),
		sum(isnull(x.ttp116, 0)),
		sum(isnull(x.ttp117, 0)),
		sum(isnull(x.ttp118, 0)),
		sum(isnull(x.ttp119, 0)),
		sum(isnull(x.ttp120, 0)),
		sum(isnull(x.ttp121, 0)),
		sum(isnull(x.ttp122, 0)),
		sum(isnull(x.ttp123, 0)),
		sum(isnull(x.ttp124, 0)),
		sum(isnull(x.ttp125, 0)),
		sum(isnull(x.ttp126, 0)),
		sum(isnull(x.ttp127, 0)),
		sum(isnull(x.ttp128, 0)),
		sum(isnull(x.ttp129, 0)),
		sum(isnull(x.ttp130, 0)),
		sum(isnull(x.ttp131, 0)),
		sum(isnull(x.ttp132, 0)),
		sum(isnull(x.ttp133, 0)),
		sum(isnull(x.ttp134, 0)),
		sum(isnull(x.ttp135, 0)),
		sum(isnull(x.ttp136, 0)),
		sum(isnull(x.ttp137, 0)),
		sum(isnull(x.ttp138, 0)),
		sum(isnull(x.ttp139, 0)),
		sum(isnull(x.ttp140, 0)),
		sum(isnull(x.ttp141, 0)),
		sum(isnull(x.ttp142, 0)),
		sum(isnull(x.ttp143, 0)),
		sum(isnull(x.ttp144, 0)),
		sum(isnull(x.ttp145, 0)),
		sum(isnull(x.ttp146, 0)),
		sum(isnull(x.ttp147, 0)),
		sum(isnull(x.ttp148, 0)),
		sum(isnull(x.ttp149, 0)),
		sum(isnull(x.ttp150, 0)),
		sum(isnull(x.ttp151, 0)),
		sum(isnull(x.ttp152, 0)),
		sum(isnull(x.ttp153, 0)),
		sum(isnull(x.ttp154, 0)),
		sum(isnull(x.ttp155, 0)),
		sum(isnull(x.ttp156, 0)),
		sum(isnull(x.ttp157, 0)),
		sum(isnull(x.ttp158, 0)),
		sum(isnull(x.ttp159, 0)),
		sum(isnull(x.ttp150, 0)),
		sum(isnull(x.ttp161, 0)),
		sum(isnull(x.ttp162, 0)),
		sum(isnull(x.ttp163, 0)),
		sum(isnull(x.ttp164, 0)),
		sum(isnull(x.ttp165, 0)),
		sum(isnull(x.ttp166, 0)),
		sum(isnull(x.ttp167, 0)),
		sum(isnull(x.ttp168, 0)),
		sum(isnull(x.ttp169, 0)),
		sum(isnull(x.ttp170, 0)),
		sum(isnull(x.ttp171, 0)),
		sum(isnull(x.ttp172, 0)),
		sum(isnull(x.ttp173, 0)),
		sum(isnull(x.ttp174, 0)),
		sum(isnull(x.ttp175, 0)),
		sum(isnull(x.ttp176, 0)),
		sum(isnull(x.ttp177, 0)),
		sum(isnull(x.ttp178, 0)),
		sum(isnull(x.ttp179, 0)),
		sum(isnull(x.ttp180, 0)),
		sum(isnull(x.ttp181, 0)),
		sum(isnull(x.ttp182, 0)),
		sum(isnull(x.ttp183, 0)),
		sum(isnull(x.ttp184, 0)),
		sum(isnull(x.ttp185, 0)),
		sum(isnull(x.ttp186, 0)),
		sum(isnull(x.ttp187, 0)),
		sum(isnull(x.ttp188, 0)),
		sum(isnull(x.ttp189, 0)),
		sum(isnull(x.ttp190, 0)),
		sum(isnull(x.ttp191, 0)),
		sum(isnull(x.ttp192, 0)),
		sum(isnull(x.ttp193, 0)),
		sum(isnull(x.ttp194, 0)),
		sum(isnull(x.ttp195, 0)),
		sum(isnull(x.ttp196, 0)),
		sum(isnull(x.ttp197, 0)),
		sum(isnull(x.ttp198, 0)),
		sum(isnull(x.ttp199, 0)),
		sum(isnull(x.ttp200, 0)),
		sum(isnull(x.ttp201, 0)),
		sum(isnull(x.ttp202, 0)),
		sum(isnull(x.ttp203, 0)),
		sum(isnull(x.ttp204, 0)),
		sum(isnull(x.ttp205, 0)),
		sum(isnull(x.ttp206, 0)),
		sum(isnull(x.ttp207, 0)),
		sum(isnull(x.ttp208, 0)),
		sum(isnull(x.ttp209, 0)),
		sum(isnull(x.ttp210, 0)),
		sum(isnull(x.ttp211, 0)),
		sum(isnull(x.ttp212, 0)),
		sum(isnull(x.ttp213, 0)),
		sum(isnull(x.ttp214, 0)),
		sum(isnull(x.ttp215, 0)),
		sum(isnull(x.ttp216, 0)),
		sum(isnull(x.ttp217, 0)),
		sum(isnull(x.ttp218, 0)),
		sum(isnull(x.ttp219, 0)),
		sum(isnull(x.ttp220, 0)),
		sum(isnull(x.ttp221, 0)),
		sum(isnull(x.ttp222, 0)),
		sum(isnull(x.ttp223, 0)),
		sum(isnull(x.ttp224, 0)),
		sum(isnull(x.ttp225, 0)),
		sum(isnull(x.ttp226, 0)),
		sum(isnull(x.ttp227, 0)),
		sum(isnull(x.ttp228, 0)),
		sum(isnull(x.ttp229, 0)),
		sum(isnull(x.ttp230, 0)),
		sum(isnull(x.ttp231, 0)),
		sum(isnull(x.ttp232, 0)),
		sum(isnull(x.ttp233, 0)),
		sum(isnull(x.ttp234, 0)),
		sum(isnull(x.ttp235, 0)),
		sum(isnull(x.ttp236, 0)),
		sum(isnull(x.ttp237, 0)),
		sum(isnull(x.ttp238, 0)),
		sum(isnull(x.ttp239, 0)),
		sum(isnull(x.ttp240, 0)),
		sum(isnull(x.ttp241, 0))
	from ev
	left join ev_extd as x
		on	x.loc_n = ev.loc_n
		and	x.id = ev.id
		and	x.seq = ev.seq
	where	ev.loc_n = in_loc_n
		and	ev.id = in_id
	group by
		ev.loc_n,
		ev.id,
		ev.[route]
	;
	
	while v_int <= v_max loop
		set v_column = 'r.value_' + ltrim(str(v_int));
	
		set v_cmd = '
			insert into ridership
			select
				r.loc_n,
				r.id,
				r.[route],
				rdr.farecell_n,
				coalesce(nullif(ltrim(rdr.farecell_id), ''''), nullif(ltrim(rdr.description), ''''), ''''),
				' + v_column + '
			from cnf with (nolock)
			inner join #ridership as r with (nolock)
				on	r.loc_n = cnf.loc_n
				and	r.id = ' + ltrim(str(in_id)) + '
			inner join rdr with (nolock)
				on	rdr.loc_n = r.loc_n
				and	rdr.rdr_id = cnf.fs_id
				and rdr.included_f = ''Y''
				and rdr.farecell_n = ' + ltrim(str(v_int)) + '
			where	cnf.loc_n = ' + ltrim(str(in_loc_n)) + '
				and	' + v_column + ' <> 0
			;
		';
		
		execute immediate v_cmd;
	
		set v_int = v_int + 1;
	end loop;
	
	commit;
end;
go

grant execute on dba.gfisp_populate_ridership_data to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_populate_ridership_data_batch') then
	drop proc gfisp_populate_ridership_data_batch;
end if;
go

create procedure dba.gfisp_populate_ridership_data_batch (
	in in_dateBegin date,
	in in_dateEnd date,
	in in_override char(1) default 'N' )
begin
/*
call gfisp_populate_ridership_data_batch ('2016-05-01', '2016-05-31');
call gfisp_populate_ridership_data_batch ('2016-05-01', '2016-05-31', 'N');
call gfisp_populate_ridership_data_batch ('2016-05-01', '2016-06-30', 'Y');
*/
	declare v_loc_n smallint;
	declare v_id int;
	declare v_cur_id int;
	declare v_value int;

	set v_loc_n = 1;
	set v_cur_id = 0;
	
	set in_override = isnull(upper(in_override), 'N');
	
	set v_id = null;
	
	select
		min(ml.id) into v_id
    from ml
    where	ml.tday between in_dateBegin and in_dateEnd
		and	ml.id > v_cur_id 
	;
	
	while (v_id is not null) loop
		set v_cur_id = v_id;
		set v_value = 0;
		
		select
			sum(
					ml.fare_c +
					ml.key1 +
					ml.key2 +
					ml.key3 +
					ml.key4 +
					ml.key5 +
					ml.key6 +
					ml.key7 +
					ml.key8 +
					ml.key9 +
					ml.keyast +
					ml.keya +
					ml.keyb +
					ml.keyc +
					ml.keyd +
					ml.ttp1 +
					ml.ttp2 +
					ml.ttp3 +
					ml.ttp4 +
					ml.ttp5 +
					ml.ttp6 +
					ml.ttp7 +
					ml.ttp8 +
					ml.ttp9 +
					ml.ttp10 +
					ml.ttp11 +
					ml.ttp12 +
					ml.ttp13 +
					ml.ttp14 +
					ml.ttp15 +
					ml.ttp16 +
					ml.ttp17 +
					ml.ttp18 +
					ml.ttp19 +
					ml.ttp20 +
					ml.ttp21 +
					ml.ttp22 +
					ml.ttp23 +
					ml.ttp24 +
					ml.ttp25 +
					ml.ttp26 +
					ml.ttp27 +
					ml.ttp28 +
					ml.ttp29 +
					ml.ttp30 +
					ml.ttp31 +
					ml.ttp32 +
					ml.ttp33 +
					ml.ttp34 +
					ml.ttp35 +
					ml.ttp36 +
					ml.ttp37 +
					ml.ttp38 +
					ml.ttp39 +
					ml.ttp40 +
					ml.ttp41 +
					ml.ttp42 +
					ml.ttp43 +
					ml.ttp44 +
					ml.ttp45 +
					ml.ttp46 +
					ml.ttp47 +
					ml.ttp48 +
					isnull(mlx.ttp49, 0) +
					isnull(mlx.ttp50, 0) +
					isnull(mlx.ttp51, 0) +
					isnull(mlx.ttp52, 0) +
					isnull(mlx.ttp53, 0) +
					isnull(mlx.ttp54, 0) +
					isnull(mlx.ttp55, 0) +
					isnull(mlx.ttp56, 0) +
					isnull(mlx.ttp57, 0) +
					isnull(mlx.ttp58, 0) +
					isnull(mlx.ttp59, 0) +
					isnull(mlx.ttp50, 0) +
					isnull(mlx.ttp61, 0) +
					isnull(mlx.ttp62, 0) +
					isnull(mlx.ttp63, 0) +
					isnull(mlx.ttp64, 0) +
					isnull(mlx.ttp65, 0) +
					isnull(mlx.ttp66, 0) +
					isnull(mlx.ttp67, 0) +
					isnull(mlx.ttp68, 0) +
					isnull(mlx.ttp69, 0) +
					isnull(mlx.ttp70, 0) +
					isnull(mlx.ttp71, 0) +
					isnull(mlx.ttp72, 0) +
					isnull(mlx.ttp73, 0) +
					isnull(mlx.ttp74, 0) +
					isnull(mlx.ttp75, 0) +
					isnull(mlx.ttp76, 0) +
					isnull(mlx.ttp77, 0) +
					isnull(mlx.ttp78, 0) +
					isnull(mlx.ttp79, 0) +
					isnull(mlx.ttp80, 0) +
					isnull(mlx.ttp81, 0) +
					isnull(mlx.ttp82, 0) +
					isnull(mlx.ttp83, 0) +
					isnull(mlx.ttp84, 0) +
					isnull(mlx.ttp85, 0) +
					isnull(mlx.ttp86, 0) +
					isnull(mlx.ttp87, 0) +
					isnull(mlx.ttp88, 0) +
					isnull(mlx.ttp89, 0) +
					isnull(mlx.ttp90, 0) +
					isnull(mlx.ttp91, 0) +
					isnull(mlx.ttp92, 0) +
					isnull(mlx.ttp93, 0) +
					isnull(mlx.ttp94, 0) +
					isnull(mlx.ttp95, 0) +
					isnull(mlx.ttp96, 0) +
					isnull(mlx.ttp97, 0) +
					isnull(mlx.ttp98, 0) +
					isnull(mlx.ttp99, 0) +
					isnull(mlx.ttp100, 0) +
					isnull(mlx.ttp101, 0) +
					isnull(mlx.ttp102, 0) +
					isnull(mlx.ttp103, 0) +
					isnull(mlx.ttp104, 0) +
					isnull(mlx.ttp105, 0) +
					isnull(mlx.ttp106, 0) +
					isnull(mlx.ttp107, 0) +
					isnull(mlx.ttp108, 0) +
					isnull(mlx.ttp109, 0) +
					isnull(mlx.ttp110, 0) +
					isnull(mlx.ttp111, 0) +
					isnull(mlx.ttp112, 0) +
					isnull(mlx.ttp113, 0) +
					isnull(mlx.ttp114, 0) +
					isnull(mlx.ttp115, 0) +
					isnull(mlx.ttp116, 0) +
					isnull(mlx.ttp117, 0) +
					isnull(mlx.ttp118, 0) +
					isnull(mlx.ttp119, 0) +
					isnull(mlx.ttp120, 0) +
					isnull(mlx.ttp121, 0) +
					isnull(mlx.ttp122, 0) +
					isnull(mlx.ttp123, 0) +
					isnull(mlx.ttp124, 0) +
					isnull(mlx.ttp125, 0) +
					isnull(mlx.ttp126, 0) +
					isnull(mlx.ttp127, 0) +
					isnull(mlx.ttp128, 0) +
					isnull(mlx.ttp129, 0) +
					isnull(mlx.ttp130, 0) +
					isnull(mlx.ttp131, 0) +
					isnull(mlx.ttp132, 0) +
					isnull(mlx.ttp133, 0) +
					isnull(mlx.ttp134, 0) +
					isnull(mlx.ttp135, 0) +
					isnull(mlx.ttp136, 0) +
					isnull(mlx.ttp137, 0) +
					isnull(mlx.ttp138, 0) +
					isnull(mlx.ttp139, 0) +
					isnull(mlx.ttp140, 0) +
					isnull(mlx.ttp141, 0) +
					isnull(mlx.ttp142, 0) +
					isnull(mlx.ttp143, 0) +
					isnull(mlx.ttp144, 0) +
					isnull(mlx.ttp145, 0) +
					isnull(mlx.ttp146, 0) +
					isnull(mlx.ttp147, 0) +
					isnull(mlx.ttp148, 0) +
					isnull(mlx.ttp149, 0) +
					isnull(mlx.ttp150, 0) +
					isnull(mlx.ttp151, 0) +
					isnull(mlx.ttp152, 0) +
					isnull(mlx.ttp153, 0) +
					isnull(mlx.ttp154, 0) +
					isnull(mlx.ttp155, 0) +
					isnull(mlx.ttp156, 0) +
					isnull(mlx.ttp157, 0) +
					isnull(mlx.ttp158, 0) +
					isnull(mlx.ttp159, 0) +
					isnull(mlx.ttp150, 0) +
					isnull(mlx.ttp161, 0) +
					isnull(mlx.ttp162, 0) +
					isnull(mlx.ttp163, 0) +
					isnull(mlx.ttp164, 0) +
					isnull(mlx.ttp165, 0) +
					isnull(mlx.ttp166, 0) +
					isnull(mlx.ttp167, 0) +
					isnull(mlx.ttp168, 0) +
					isnull(mlx.ttp169, 0) +
					isnull(mlx.ttp170, 0) +
					isnull(mlx.ttp171, 0) +
					isnull(mlx.ttp172, 0) +
					isnull(mlx.ttp173, 0) +
					isnull(mlx.ttp174, 0) +
					isnull(mlx.ttp175, 0) +
					isnull(mlx.ttp176, 0) +
					isnull(mlx.ttp177, 0) +
					isnull(mlx.ttp178, 0) +
					isnull(mlx.ttp179, 0) +
					isnull(mlx.ttp180, 0) +
					isnull(mlx.ttp181, 0) +
					isnull(mlx.ttp182, 0) +
					isnull(mlx.ttp183, 0) +
					isnull(mlx.ttp184, 0) +
					isnull(mlx.ttp185, 0) +
					isnull(mlx.ttp186, 0) +
					isnull(mlx.ttp187, 0) +
					isnull(mlx.ttp188, 0) +
					isnull(mlx.ttp189, 0) +
					isnull(mlx.ttp190, 0) +
					isnull(mlx.ttp191, 0) +
					isnull(mlx.ttp192, 0) +
					isnull(mlx.ttp193, 0) +
					isnull(mlx.ttp194, 0) +
					isnull(mlx.ttp195, 0) +
					isnull(mlx.ttp196, 0) +
					isnull(mlx.ttp197, 0) +
					isnull(mlx.ttp198, 0) +
					isnull(mlx.ttp199, 0) +
					isnull(mlx.ttp200, 0) +
					isnull(mlx.ttp201, 0) +
					isnull(mlx.ttp202, 0) +
					isnull(mlx.ttp203, 0) +
					isnull(mlx.ttp204, 0) +
					isnull(mlx.ttp205, 0) +
					isnull(mlx.ttp206, 0) +
					isnull(mlx.ttp207, 0) +
					isnull(mlx.ttp208, 0) +
					isnull(mlx.ttp209, 0) +
					isnull(mlx.ttp210, 0) +
					isnull(mlx.ttp211, 0) +
					isnull(mlx.ttp212, 0) +
					isnull(mlx.ttp213, 0) +
					isnull(mlx.ttp214, 0) +
					isnull(mlx.ttp215, 0) +
					isnull(mlx.ttp216, 0) +
					isnull(mlx.ttp217, 0) +
					isnull(mlx.ttp218, 0) +
					isnull(mlx.ttp219, 0) +
					isnull(mlx.ttp220, 0) +
					isnull(mlx.ttp221, 0) +
					isnull(mlx.ttp222, 0) +
					isnull(mlx.ttp223, 0) +
					isnull(mlx.ttp224, 0) +
					isnull(mlx.ttp225, 0) +
					isnull(mlx.ttp226, 0) +
					isnull(mlx.ttp227, 0) +
					isnull(mlx.ttp228, 0) +
					isnull(mlx.ttp229, 0) +
					isnull(mlx.ttp230, 0) +
					isnull(mlx.ttp231, 0) +
					isnull(mlx.ttp232, 0) +
					isnull(mlx.ttp233, 0) +
					isnull(mlx.ttp234, 0) +
					isnull(mlx.ttp235, 0) +
					isnull(mlx.ttp236, 0) +
					isnull(mlx.ttp237, 0) +
					isnull(mlx.ttp238, 0) +
					isnull(mlx.ttp239, 0) +
					isnull(mlx.ttp240, 0) +
					isnull(mlx.ttp241, 0) +
					isnull(ev.fare_c, 0) +
					isnull(ev.key1, 0) +
					isnull(ev.key2, 0) +
					isnull(ev.key3, 0) +
					isnull(ev.key4, 0) +
					isnull(ev.key5, 0) +
					isnull(ev.key6, 0) +
					isnull(ev.key7, 0) +
					isnull(ev.key8, 0) +
					isnull(ev.key9, 0) +
					isnull(ev.keyast, 0) +
					isnull(ev.keya, 0) +
					isnull(ev.keyb, 0) +
					isnull(ev.keyc, 0) +
					isnull(ev.keyd, 0) +
					isnull(ev.ttp1, 0) +
					isnull(ev.ttp2, 0) +
					isnull(ev.ttp3, 0) +
					isnull(ev.ttp4, 0) +
					isnull(ev.ttp5, 0) +
					isnull(ev.ttp6, 0) +
					isnull(ev.ttp7, 0) +
					isnull(ev.ttp8, 0) +
					isnull(ev.ttp9, 0) +
					isnull(ev.ttp10, 0) +
					isnull(ev.ttp11, 0) +
					isnull(ev.ttp12, 0) +
					isnull(ev.ttp13, 0) +
					isnull(ev.ttp14, 0) +
					isnull(ev.ttp15, 0) +
					isnull(ev.ttp16, 0) +
					isnull(ev.ttp17, 0) +
					isnull(ev.ttp18, 0) +
					isnull(ev.ttp19, 0) +
					isnull(ev.ttp20, 0) +
					isnull(ev.ttp21, 0) +
					isnull(ev.ttp22, 0) +
					isnull(ev.ttp23, 0) +
					isnull(ev.ttp24, 0) +
					isnull(ev.ttp25, 0) +
					isnull(ev.ttp26, 0) +
					isnull(ev.ttp27, 0) +
					isnull(ev.ttp28, 0) +
					isnull(ev.ttp29, 0) +
					isnull(ev.ttp30, 0) +
					isnull(ev.ttp31, 0) +
					isnull(ev.ttp32, 0) +
					isnull(ev.ttp33, 0) +
					isnull(ev.ttp34, 0) +
					isnull(ev.ttp35, 0) +
					isnull(ev.ttp36, 0) +
					isnull(ev.ttp37, 0) +
					isnull(ev.ttp38, 0) +
					isnull(ev.ttp39, 0) +
					isnull(ev.ttp40, 0) +
					isnull(ev.ttp41, 0) +
					isnull(ev.ttp42, 0) +
					isnull(ev.ttp43, 0) +
					isnull(ev.ttp44, 0) +
					isnull(ev.ttp45, 0) +
					isnull(ev.ttp46, 0) +
					isnull(ev.ttp47, 0) +
					isnull(ev.ttp48, 0) +
					isnull(evx.ttp49, 0) +
					isnull(evx.ttp50, 0) +
					isnull(evx.ttp51, 0) +
					isnull(evx.ttp52, 0) +
					isnull(evx.ttp53, 0) +
					isnull(evx.ttp54, 0) +
					isnull(evx.ttp55, 0) +
					isnull(evx.ttp56, 0) +
					isnull(evx.ttp57, 0) +
					isnull(evx.ttp58, 0) +
					isnull(evx.ttp59, 0) +
					isnull(evx.ttp50, 0) +
					isnull(evx.ttp61, 0) +
					isnull(evx.ttp62, 0) +
					isnull(evx.ttp63, 0) +
					isnull(evx.ttp64, 0) +
					isnull(evx.ttp65, 0) +
					isnull(evx.ttp66, 0) +
					isnull(evx.ttp67, 0) +
					isnull(evx.ttp68, 0) +
					isnull(evx.ttp69, 0) +
					isnull(evx.ttp70, 0) +
					isnull(evx.ttp71, 0) +
					isnull(evx.ttp72, 0) +
					isnull(evx.ttp73, 0) +
					isnull(evx.ttp74, 0) +
					isnull(evx.ttp75, 0) +
					isnull(evx.ttp76, 0) +
					isnull(evx.ttp77, 0) +
					isnull(evx.ttp78, 0) +
					isnull(evx.ttp79, 0) +
					isnull(evx.ttp80, 0) +
					isnull(evx.ttp81, 0) +
					isnull(evx.ttp82, 0) +
					isnull(evx.ttp83, 0) +
					isnull(evx.ttp84, 0) +
					isnull(evx.ttp85, 0) +
					isnull(evx.ttp86, 0) +
					isnull(evx.ttp87, 0) +
					isnull(evx.ttp88, 0) +
					isnull(evx.ttp89, 0) +
					isnull(evx.ttp90, 0) +
					isnull(evx.ttp91, 0) +
					isnull(evx.ttp92, 0) +
					isnull(evx.ttp93, 0) +
					isnull(evx.ttp94, 0) +
					isnull(evx.ttp95, 0) +
					isnull(evx.ttp96, 0) +
					isnull(evx.ttp97, 0) +
					isnull(evx.ttp98, 0) +
					isnull(evx.ttp99, 0) +
					isnull(evx.ttp100, 0) +
					isnull(evx.ttp101, 0) +
					isnull(evx.ttp102, 0) +
					isnull(evx.ttp103, 0) +
					isnull(evx.ttp104, 0) +
					isnull(evx.ttp105, 0) +
					isnull(evx.ttp106, 0) +
					isnull(evx.ttp107, 0) +
					isnull(evx.ttp108, 0) +
					isnull(evx.ttp109, 0) +
					isnull(evx.ttp110, 0) +
					isnull(evx.ttp111, 0) +
					isnull(evx.ttp112, 0) +
					isnull(evx.ttp113, 0) +
					isnull(evx.ttp114, 0) +
					isnull(evx.ttp115, 0) +
					isnull(evx.ttp116, 0) +
					isnull(evx.ttp117, 0) +
					isnull(evx.ttp118, 0) +
					isnull(evx.ttp119, 0) +
					isnull(evx.ttp120, 0) +
					isnull(evx.ttp121, 0) +
					isnull(evx.ttp122, 0) +
					isnull(evx.ttp123, 0) +
					isnull(evx.ttp124, 0) +
					isnull(evx.ttp125, 0) +
					isnull(evx.ttp126, 0) +
					isnull(evx.ttp127, 0) +
					isnull(evx.ttp128, 0) +
					isnull(evx.ttp129, 0) +
					isnull(evx.ttp130, 0) +
					isnull(evx.ttp131, 0) +
					isnull(evx.ttp132, 0) +
					isnull(evx.ttp133, 0) +
					isnull(evx.ttp134, 0) +
					isnull(evx.ttp135, 0) +
					isnull(evx.ttp136, 0) +
					isnull(evx.ttp137, 0) +
					isnull(evx.ttp138, 0) +
					isnull(evx.ttp139, 0) +
					isnull(evx.ttp140, 0) +
					isnull(evx.ttp141, 0) +
					isnull(evx.ttp142, 0) +
					isnull(evx.ttp143, 0) +
					isnull(evx.ttp144, 0) +
					isnull(evx.ttp145, 0) +
					isnull(evx.ttp146, 0) +
					isnull(evx.ttp147, 0) +
					isnull(evx.ttp148, 0) +
					isnull(evx.ttp149, 0) +
					isnull(evx.ttp150, 0) +
					isnull(evx.ttp151, 0) +
					isnull(evx.ttp152, 0) +
					isnull(evx.ttp153, 0) +
					isnull(evx.ttp154, 0) +
					isnull(evx.ttp155, 0) +
					isnull(evx.ttp156, 0) +
					isnull(evx.ttp157, 0) +
					isnull(evx.ttp158, 0) +
					isnull(evx.ttp159, 0) +
					isnull(evx.ttp150, 0) +
					isnull(evx.ttp161, 0) +
					isnull(evx.ttp162, 0) +
					isnull(evx.ttp163, 0) +
					isnull(evx.ttp164, 0) +
					isnull(evx.ttp165, 0) +
					isnull(evx.ttp166, 0) +
					isnull(evx.ttp167, 0) +
					isnull(evx.ttp168, 0) +
					isnull(evx.ttp169, 0) +
					isnull(evx.ttp170, 0) +
					isnull(evx.ttp171, 0) +
					isnull(evx.ttp172, 0) +
					isnull(evx.ttp173, 0) +
					isnull(evx.ttp174, 0) +
					isnull(evx.ttp175, 0) +
					isnull(evx.ttp176, 0) +
					isnull(evx.ttp177, 0) +
					isnull(evx.ttp178, 0) +
					isnull(evx.ttp179, 0) +
					isnull(evx.ttp180, 0) +
					isnull(evx.ttp181, 0) +
					isnull(evx.ttp182, 0) +
					isnull(evx.ttp183, 0) +
					isnull(evx.ttp184, 0) +
					isnull(evx.ttp185, 0) +
					isnull(evx.ttp186, 0) +
					isnull(evx.ttp187, 0) +
					isnull(evx.ttp188, 0) +
					isnull(evx.ttp189, 0) +
					isnull(evx.ttp190, 0) +
					isnull(evx.ttp191, 0) +
					isnull(evx.ttp192, 0) +
					isnull(evx.ttp193, 0) +
					isnull(evx.ttp194, 0) +
					isnull(evx.ttp195, 0) +
					isnull(evx.ttp196, 0) +
					isnull(evx.ttp197, 0) +
					isnull(evx.ttp198, 0) +
					isnull(evx.ttp199, 0) +
					isnull(evx.ttp200, 0) +
					isnull(evx.ttp201, 0) +
					isnull(evx.ttp202, 0) +
					isnull(evx.ttp203, 0) +
					isnull(evx.ttp204, 0) +
					isnull(evx.ttp205, 0) +
					isnull(evx.ttp206, 0) +
					isnull(evx.ttp207, 0) +
					isnull(evx.ttp208, 0) +
					isnull(evx.ttp209, 0) +
					isnull(evx.ttp210, 0) +
					isnull(evx.ttp211, 0) +
					isnull(evx.ttp212, 0) +
					isnull(evx.ttp213, 0) +
					isnull(evx.ttp214, 0) +
					isnull(evx.ttp215, 0) +
					isnull(evx.ttp216, 0) +
					isnull(evx.ttp217, 0) +
					isnull(evx.ttp218, 0) +
					isnull(evx.ttp219, 0) +
					isnull(evx.ttp220, 0) +
					isnull(evx.ttp221, 0) +
					isnull(evx.ttp222, 0) +
					isnull(evx.ttp223, 0) +
					isnull(evx.ttp224, 0) +
					isnull(evx.ttp225, 0) +
					isnull(evx.ttp226, 0) +
					isnull(evx.ttp227, 0) +
					isnull(evx.ttp228, 0) +
					isnull(evx.ttp229, 0) +
					isnull(evx.ttp230, 0) +
					isnull(evx.ttp231, 0) +
					isnull(evx.ttp232, 0) +
					isnull(evx.ttp233, 0) +
					isnull(evx.ttp234, 0) +
					isnull(evx.ttp235, 0) +
					isnull(evx.ttp236, 0) +
					isnull(evx.ttp237, 0) +
					isnull(evx.ttp238, 0) +
					isnull(evx.ttp239, 0) +
					isnull(evx.ttp240, 0) +
					isnull(evx.ttp241, 0)
				) into v_value
	    from ml
	    left join ml_extd as mlx
			on	mlx.loc_n = ml.loc_n
			and	mlx.id = ml.id
	    left join ev
			on	ev.loc_n = ml.loc_n
			and	ev.id = ml.id
	    left join ev_extd as evx
			on	evx.loc_n = ev.loc_n
			and	evx.id = ev.id
			and evx.seq = ev.seq
		where	ml.id = v_id;
		
		if (v_value <> 0) then
			call gfisp_populate_ridership_data (v_loc_n, v_id, in_override);
		end if;
		
		set v_id = null;
		
		select
			min(ml.id) into v_id
		from ml
		where	ml.tday between in_dateBegin and in_dateEnd
			and	ml.id > v_cur_id 
		;
	end loop;
end
go

grant execute on dba.gfisp_populate_ridership_data_batch to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_populate_ridership_data_portion') then
	drop proc gfisp_populate_ridership_data_portion;
end if;
go

create procedure dba.gfisp_populate_ridership_data_portion (
	in in_days int default 7 )
begin
/*
call gfisp_populate_ridership_data_portion ();
call gfisp_populate_ridership_data_portion (1);
call gfisp_populate_ridership_data_portion (10);
*/
	declare v_dateBegin date;
	declare v_dateEnd date;
	
	set in_days = isnull(in_days, 7);
	
	select max(ml.tday) into v_dateEnd
    from ml
    left join ml_extd as mlx
		on	mlx.loc_n = ml.loc_n
		and	mlx.id = ml.id
    left join ev
		on	ev.loc_n = ml.loc_n
		and	ev.id = ml.id
    left join ev_extd as evx
		on	evx.loc_n = ev.loc_n
		and	evx.id = ev.id
		and evx.seq = ev.seq
	left join ridership as r
		on	r.loc_n = ml.loc_n
		and	r.id = ml.id
    where	ml.tday < dateadd(day, -6, getdate())
		and	r.loc_n is null
		and	(
				ml.fare_c +
				ml.key1 +
				ml.key2 +
				ml.key3 +
				ml.key4 +
				ml.key5 +
				ml.key6 +
				ml.key7 +
				ml.key8 +
				ml.key9 +
				ml.keyast +
				ml.keya +
				ml.keyb +
				ml.keyc +
				ml.keyd +
				ml.ttp1 +
				ml.ttp2 +
				ml.ttp3 +
				ml.ttp4 +
				ml.ttp5 +
				ml.ttp6 +
				ml.ttp7 +
				ml.ttp8 +
				ml.ttp9 +
				ml.ttp10 +
				ml.ttp11 +
				ml.ttp12 +
				ml.ttp13 +
				ml.ttp14 +
				ml.ttp15 +
				ml.ttp16 +
				ml.ttp17 +
				ml.ttp18 +
				ml.ttp19 +
				ml.ttp20 +
				ml.ttp21 +
				ml.ttp22 +
				ml.ttp23 +
				ml.ttp24 +
				ml.ttp25 +
				ml.ttp26 +
				ml.ttp27 +
				ml.ttp28 +
				ml.ttp29 +
				ml.ttp30 +
				ml.ttp31 +
				ml.ttp32 +
				ml.ttp33 +
				ml.ttp34 +
				ml.ttp35 +
				ml.ttp36 +
				ml.ttp37 +
				ml.ttp38 +
				ml.ttp39 +
				ml.ttp40 +
				ml.ttp41 +
				ml.ttp42 +
				ml.ttp43 +
				ml.ttp44 +
				ml.ttp45 +
				ml.ttp46 +
				ml.ttp47 +
				ml.ttp48 +
				isnull(mlx.ttp49, 0) +
				isnull(mlx.ttp50, 0) +
				isnull(mlx.ttp51, 0) +
				isnull(mlx.ttp52, 0) +
				isnull(mlx.ttp53, 0) +
				isnull(mlx.ttp54, 0) +
				isnull(mlx.ttp55, 0) +
				isnull(mlx.ttp56, 0) +
				isnull(mlx.ttp57, 0) +
				isnull(mlx.ttp58, 0) +
				isnull(mlx.ttp59, 0) +
				isnull(mlx.ttp50, 0) +
				isnull(mlx.ttp61, 0) +
				isnull(mlx.ttp62, 0) +
				isnull(mlx.ttp63, 0) +
				isnull(mlx.ttp64, 0) +
				isnull(mlx.ttp65, 0) +
				isnull(mlx.ttp66, 0) +
				isnull(mlx.ttp67, 0) +
				isnull(mlx.ttp68, 0) +
				isnull(mlx.ttp69, 0) +
				isnull(mlx.ttp70, 0) +
				isnull(mlx.ttp71, 0) +
				isnull(mlx.ttp72, 0) +
				isnull(mlx.ttp73, 0) +
				isnull(mlx.ttp74, 0) +
				isnull(mlx.ttp75, 0) +
				isnull(mlx.ttp76, 0) +
				isnull(mlx.ttp77, 0) +
				isnull(mlx.ttp78, 0) +
				isnull(mlx.ttp79, 0) +
				isnull(mlx.ttp80, 0) +
				isnull(mlx.ttp81, 0) +
				isnull(mlx.ttp82, 0) +
				isnull(mlx.ttp83, 0) +
				isnull(mlx.ttp84, 0) +
				isnull(mlx.ttp85, 0) +
				isnull(mlx.ttp86, 0) +
				isnull(mlx.ttp87, 0) +
				isnull(mlx.ttp88, 0) +
				isnull(mlx.ttp89, 0) +
				isnull(mlx.ttp90, 0) +
				isnull(mlx.ttp91, 0) +
				isnull(mlx.ttp92, 0) +
				isnull(mlx.ttp93, 0) +
				isnull(mlx.ttp94, 0) +
				isnull(mlx.ttp95, 0) +
				isnull(mlx.ttp96, 0) +
				isnull(mlx.ttp97, 0) +
				isnull(mlx.ttp98, 0) +
				isnull(mlx.ttp99, 0) +
				isnull(mlx.ttp100, 0) +
				isnull(mlx.ttp101, 0) +
				isnull(mlx.ttp102, 0) +
				isnull(mlx.ttp103, 0) +
				isnull(mlx.ttp104, 0) +
				isnull(mlx.ttp105, 0) +
				isnull(mlx.ttp106, 0) +
				isnull(mlx.ttp107, 0) +
				isnull(mlx.ttp108, 0) +
				isnull(mlx.ttp109, 0) +
				isnull(mlx.ttp110, 0) +
				isnull(mlx.ttp111, 0) +
				isnull(mlx.ttp112, 0) +
				isnull(mlx.ttp113, 0) +
				isnull(mlx.ttp114, 0) +
				isnull(mlx.ttp115, 0) +
				isnull(mlx.ttp116, 0) +
				isnull(mlx.ttp117, 0) +
				isnull(mlx.ttp118, 0) +
				isnull(mlx.ttp119, 0) +
				isnull(mlx.ttp120, 0) +
				isnull(mlx.ttp121, 0) +
				isnull(mlx.ttp122, 0) +
				isnull(mlx.ttp123, 0) +
				isnull(mlx.ttp124, 0) +
				isnull(mlx.ttp125, 0) +
				isnull(mlx.ttp126, 0) +
				isnull(mlx.ttp127, 0) +
				isnull(mlx.ttp128, 0) +
				isnull(mlx.ttp129, 0) +
				isnull(mlx.ttp130, 0) +
				isnull(mlx.ttp131, 0) +
				isnull(mlx.ttp132, 0) +
				isnull(mlx.ttp133, 0) +
				isnull(mlx.ttp134, 0) +
				isnull(mlx.ttp135, 0) +
				isnull(mlx.ttp136, 0) +
				isnull(mlx.ttp137, 0) +
				isnull(mlx.ttp138, 0) +
				isnull(mlx.ttp139, 0) +
				isnull(mlx.ttp140, 0) +
				isnull(mlx.ttp141, 0) +
				isnull(mlx.ttp142, 0) +
				isnull(mlx.ttp143, 0) +
				isnull(mlx.ttp144, 0) +
				isnull(mlx.ttp145, 0) +
				isnull(mlx.ttp146, 0) +
				isnull(mlx.ttp147, 0) +
				isnull(mlx.ttp148, 0) +
				isnull(mlx.ttp149, 0) +
				isnull(mlx.ttp150, 0) +
				isnull(mlx.ttp151, 0) +
				isnull(mlx.ttp152, 0) +
				isnull(mlx.ttp153, 0) +
				isnull(mlx.ttp154, 0) +
				isnull(mlx.ttp155, 0) +
				isnull(mlx.ttp156, 0) +
				isnull(mlx.ttp157, 0) +
				isnull(mlx.ttp158, 0) +
				isnull(mlx.ttp159, 0) +
				isnull(mlx.ttp150, 0) +
				isnull(mlx.ttp161, 0) +
				isnull(mlx.ttp162, 0) +
				isnull(mlx.ttp163, 0) +
				isnull(mlx.ttp164, 0) +
				isnull(mlx.ttp165, 0) +
				isnull(mlx.ttp166, 0) +
				isnull(mlx.ttp167, 0) +
				isnull(mlx.ttp168, 0) +
				isnull(mlx.ttp169, 0) +
				isnull(mlx.ttp170, 0) +
				isnull(mlx.ttp171, 0) +
				isnull(mlx.ttp172, 0) +
				isnull(mlx.ttp173, 0) +
				isnull(mlx.ttp174, 0) +
				isnull(mlx.ttp175, 0) +
				isnull(mlx.ttp176, 0) +
				isnull(mlx.ttp177, 0) +
				isnull(mlx.ttp178, 0) +
				isnull(mlx.ttp179, 0) +
				isnull(mlx.ttp180, 0) +
				isnull(mlx.ttp181, 0) +
				isnull(mlx.ttp182, 0) +
				isnull(mlx.ttp183, 0) +
				isnull(mlx.ttp184, 0) +
				isnull(mlx.ttp185, 0) +
				isnull(mlx.ttp186, 0) +
				isnull(mlx.ttp187, 0) +
				isnull(mlx.ttp188, 0) +
				isnull(mlx.ttp189, 0) +
				isnull(mlx.ttp190, 0) +
				isnull(mlx.ttp191, 0) +
				isnull(mlx.ttp192, 0) +
				isnull(mlx.ttp193, 0) +
				isnull(mlx.ttp194, 0) +
				isnull(mlx.ttp195, 0) +
				isnull(mlx.ttp196, 0) +
				isnull(mlx.ttp197, 0) +
				isnull(mlx.ttp198, 0) +
				isnull(mlx.ttp199, 0) +
				isnull(mlx.ttp200, 0) +
				isnull(mlx.ttp201, 0) +
				isnull(mlx.ttp202, 0) +
				isnull(mlx.ttp203, 0) +
				isnull(mlx.ttp204, 0) +
				isnull(mlx.ttp205, 0) +
				isnull(mlx.ttp206, 0) +
				isnull(mlx.ttp207, 0) +
				isnull(mlx.ttp208, 0) +
				isnull(mlx.ttp209, 0) +
				isnull(mlx.ttp210, 0) +
				isnull(mlx.ttp211, 0) +
				isnull(mlx.ttp212, 0) +
				isnull(mlx.ttp213, 0) +
				isnull(mlx.ttp214, 0) +
				isnull(mlx.ttp215, 0) +
				isnull(mlx.ttp216, 0) +
				isnull(mlx.ttp217, 0) +
				isnull(mlx.ttp218, 0) +
				isnull(mlx.ttp219, 0) +
				isnull(mlx.ttp220, 0) +
				isnull(mlx.ttp221, 0) +
				isnull(mlx.ttp222, 0) +
				isnull(mlx.ttp223, 0) +
				isnull(mlx.ttp224, 0) +
				isnull(mlx.ttp225, 0) +
				isnull(mlx.ttp226, 0) +
				isnull(mlx.ttp227, 0) +
				isnull(mlx.ttp228, 0) +
				isnull(mlx.ttp229, 0) +
				isnull(mlx.ttp230, 0) +
				isnull(mlx.ttp231, 0) +
				isnull(mlx.ttp232, 0) +
				isnull(mlx.ttp233, 0) +
				isnull(mlx.ttp234, 0) +
				isnull(mlx.ttp235, 0) +
				isnull(mlx.ttp236, 0) +
				isnull(mlx.ttp237, 0) +
				isnull(mlx.ttp238, 0) +
				isnull(mlx.ttp239, 0) +
				isnull(mlx.ttp240, 0) +
				isnull(mlx.ttp241, 0) +
				isnull(ev.fare_c, 0) +
				isnull(ev.key1, 0) +
				isnull(ev.key2, 0) +
				isnull(ev.key3, 0) +
				isnull(ev.key4, 0) +
				isnull(ev.key5, 0) +
				isnull(ev.key6, 0) +
				isnull(ev.key7, 0) +
				isnull(ev.key8, 0) +
				isnull(ev.key9, 0) +
				isnull(ev.keyast, 0) +
				isnull(ev.keya, 0) +
				isnull(ev.keyb, 0) +
				isnull(ev.keyc, 0) +
				isnull(ev.keyd, 0) +
				isnull(ev.ttp1, 0) +
				isnull(ev.ttp2, 0) +
				isnull(ev.ttp3, 0) +
				isnull(ev.ttp4, 0) +
				isnull(ev.ttp5, 0) +
				isnull(ev.ttp6, 0) +
				isnull(ev.ttp7, 0) +
				isnull(ev.ttp8, 0) +
				isnull(ev.ttp9, 0) +
				isnull(ev.ttp10, 0) +
				isnull(ev.ttp11, 0) +
				isnull(ev.ttp12, 0) +
				isnull(ev.ttp13, 0) +
				isnull(ev.ttp14, 0) +
				isnull(ev.ttp15, 0) +
				isnull(ev.ttp16, 0) +
				isnull(ev.ttp17, 0) +
				isnull(ev.ttp18, 0) +
				isnull(ev.ttp19, 0) +
				isnull(ev.ttp20, 0) +
				isnull(ev.ttp21, 0) +
				isnull(ev.ttp22, 0) +
				isnull(ev.ttp23, 0) +
				isnull(ev.ttp24, 0) +
				isnull(ev.ttp25, 0) +
				isnull(ev.ttp26, 0) +
				isnull(ev.ttp27, 0) +
				isnull(ev.ttp28, 0) +
				isnull(ev.ttp29, 0) +
				isnull(ev.ttp30, 0) +
				isnull(ev.ttp31, 0) +
				isnull(ev.ttp32, 0) +
				isnull(ev.ttp33, 0) +
				isnull(ev.ttp34, 0) +
				isnull(ev.ttp35, 0) +
				isnull(ev.ttp36, 0) +
				isnull(ev.ttp37, 0) +
				isnull(ev.ttp38, 0) +
				isnull(ev.ttp39, 0) +
				isnull(ev.ttp40, 0) +
				isnull(ev.ttp41, 0) +
				isnull(ev.ttp42, 0) +
				isnull(ev.ttp43, 0) +
				isnull(ev.ttp44, 0) +
				isnull(ev.ttp45, 0) +
				isnull(ev.ttp46, 0) +
				isnull(ev.ttp47, 0) +
				isnull(ev.ttp48, 0) +
				isnull(evx.ttp49, 0) +
				isnull(evx.ttp50, 0) +
				isnull(evx.ttp51, 0) +
				isnull(evx.ttp52, 0) +
				isnull(evx.ttp53, 0) +
				isnull(evx.ttp54, 0) +
				isnull(evx.ttp55, 0) +
				isnull(evx.ttp56, 0) +
				isnull(evx.ttp57, 0) +
				isnull(evx.ttp58, 0) +
				isnull(evx.ttp59, 0) +
				isnull(evx.ttp50, 0) +
				isnull(evx.ttp61, 0) +
				isnull(evx.ttp62, 0) +
				isnull(evx.ttp63, 0) +
				isnull(evx.ttp64, 0) +
				isnull(evx.ttp65, 0) +
				isnull(evx.ttp66, 0) +
				isnull(evx.ttp67, 0) +
				isnull(evx.ttp68, 0) +
				isnull(evx.ttp69, 0) +
				isnull(evx.ttp70, 0) +
				isnull(evx.ttp71, 0) +
				isnull(evx.ttp72, 0) +
				isnull(evx.ttp73, 0) +
				isnull(evx.ttp74, 0) +
				isnull(evx.ttp75, 0) +
				isnull(evx.ttp76, 0) +
				isnull(evx.ttp77, 0) +
				isnull(evx.ttp78, 0) +
				isnull(evx.ttp79, 0) +
				isnull(evx.ttp80, 0) +
				isnull(evx.ttp81, 0) +
				isnull(evx.ttp82, 0) +
				isnull(evx.ttp83, 0) +
				isnull(evx.ttp84, 0) +
				isnull(evx.ttp85, 0) +
				isnull(evx.ttp86, 0) +
				isnull(evx.ttp87, 0) +
				isnull(evx.ttp88, 0) +
				isnull(evx.ttp89, 0) +
				isnull(evx.ttp90, 0) +
				isnull(evx.ttp91, 0) +
				isnull(evx.ttp92, 0) +
				isnull(evx.ttp93, 0) +
				isnull(evx.ttp94, 0) +
				isnull(evx.ttp95, 0) +
				isnull(evx.ttp96, 0) +
				isnull(evx.ttp97, 0) +
				isnull(evx.ttp98, 0) +
				isnull(evx.ttp99, 0) +
				isnull(evx.ttp100, 0) +
				isnull(evx.ttp101, 0) +
				isnull(evx.ttp102, 0) +
				isnull(evx.ttp103, 0) +
				isnull(evx.ttp104, 0) +
				isnull(evx.ttp105, 0) +
				isnull(evx.ttp106, 0) +
				isnull(evx.ttp107, 0) +
				isnull(evx.ttp108, 0) +
				isnull(evx.ttp109, 0) +
				isnull(evx.ttp110, 0) +
				isnull(evx.ttp111, 0) +
				isnull(evx.ttp112, 0) +
				isnull(evx.ttp113, 0) +
				isnull(evx.ttp114, 0) +
				isnull(evx.ttp115, 0) +
				isnull(evx.ttp116, 0) +
				isnull(evx.ttp117, 0) +
				isnull(evx.ttp118, 0) +
				isnull(evx.ttp119, 0) +
				isnull(evx.ttp120, 0) +
				isnull(evx.ttp121, 0) +
				isnull(evx.ttp122, 0) +
				isnull(evx.ttp123, 0) +
				isnull(evx.ttp124, 0) +
				isnull(evx.ttp125, 0) +
				isnull(evx.ttp126, 0) +
				isnull(evx.ttp127, 0) +
				isnull(evx.ttp128, 0) +
				isnull(evx.ttp129, 0) +
				isnull(evx.ttp130, 0) +
				isnull(evx.ttp131, 0) +
				isnull(evx.ttp132, 0) +
				isnull(evx.ttp133, 0) +
				isnull(evx.ttp134, 0) +
				isnull(evx.ttp135, 0) +
				isnull(evx.ttp136, 0) +
				isnull(evx.ttp137, 0) +
				isnull(evx.ttp138, 0) +
				isnull(evx.ttp139, 0) +
				isnull(evx.ttp140, 0) +
				isnull(evx.ttp141, 0) +
				isnull(evx.ttp142, 0) +
				isnull(evx.ttp143, 0) +
				isnull(evx.ttp144, 0) +
				isnull(evx.ttp145, 0) +
				isnull(evx.ttp146, 0) +
				isnull(evx.ttp147, 0) +
				isnull(evx.ttp148, 0) +
				isnull(evx.ttp149, 0) +
				isnull(evx.ttp150, 0) +
				isnull(evx.ttp151, 0) +
				isnull(evx.ttp152, 0) +
				isnull(evx.ttp153, 0) +
				isnull(evx.ttp154, 0) +
				isnull(evx.ttp155, 0) +
				isnull(evx.ttp156, 0) +
				isnull(evx.ttp157, 0) +
				isnull(evx.ttp158, 0) +
				isnull(evx.ttp159, 0) +
				isnull(evx.ttp150, 0) +
				isnull(evx.ttp161, 0) +
				isnull(evx.ttp162, 0) +
				isnull(evx.ttp163, 0) +
				isnull(evx.ttp164, 0) +
				isnull(evx.ttp165, 0) +
				isnull(evx.ttp166, 0) +
				isnull(evx.ttp167, 0) +
				isnull(evx.ttp168, 0) +
				isnull(evx.ttp169, 0) +
				isnull(evx.ttp170, 0) +
				isnull(evx.ttp171, 0) +
				isnull(evx.ttp172, 0) +
				isnull(evx.ttp173, 0) +
				isnull(evx.ttp174, 0) +
				isnull(evx.ttp175, 0) +
				isnull(evx.ttp176, 0) +
				isnull(evx.ttp177, 0) +
				isnull(evx.ttp178, 0) +
				isnull(evx.ttp179, 0) +
				isnull(evx.ttp180, 0) +
				isnull(evx.ttp181, 0) +
				isnull(evx.ttp182, 0) +
				isnull(evx.ttp183, 0) +
				isnull(evx.ttp184, 0) +
				isnull(evx.ttp185, 0) +
				isnull(evx.ttp186, 0) +
				isnull(evx.ttp187, 0) +
				isnull(evx.ttp188, 0) +
				isnull(evx.ttp189, 0) +
				isnull(evx.ttp190, 0) +
				isnull(evx.ttp191, 0) +
				isnull(evx.ttp192, 0) +
				isnull(evx.ttp193, 0) +
				isnull(evx.ttp194, 0) +
				isnull(evx.ttp195, 0) +
				isnull(evx.ttp196, 0) +
				isnull(evx.ttp197, 0) +
				isnull(evx.ttp198, 0) +
				isnull(evx.ttp199, 0) +
				isnull(evx.ttp200, 0) +
				isnull(evx.ttp201, 0) +
				isnull(evx.ttp202, 0) +
				isnull(evx.ttp203, 0) +
				isnull(evx.ttp204, 0) +
				isnull(evx.ttp205, 0) +
				isnull(evx.ttp206, 0) +
				isnull(evx.ttp207, 0) +
				isnull(evx.ttp208, 0) +
				isnull(evx.ttp209, 0) +
				isnull(evx.ttp210, 0) +
				isnull(evx.ttp211, 0) +
				isnull(evx.ttp212, 0) +
				isnull(evx.ttp213, 0) +
				isnull(evx.ttp214, 0) +
				isnull(evx.ttp215, 0) +
				isnull(evx.ttp216, 0) +
				isnull(evx.ttp217, 0) +
				isnull(evx.ttp218, 0) +
				isnull(evx.ttp219, 0) +
				isnull(evx.ttp220, 0) +
				isnull(evx.ttp221, 0) +
				isnull(evx.ttp222, 0) +
				isnull(evx.ttp223, 0) +
				isnull(evx.ttp224, 0) +
				isnull(evx.ttp225, 0) +
				isnull(evx.ttp226, 0) +
				isnull(evx.ttp227, 0) +
				isnull(evx.ttp228, 0) +
				isnull(evx.ttp229, 0) +
				isnull(evx.ttp230, 0) +
				isnull(evx.ttp231, 0) +
				isnull(evx.ttp232, 0) +
				isnull(evx.ttp233, 0) +
				isnull(evx.ttp234, 0) +
				isnull(evx.ttp235, 0) +
				isnull(evx.ttp236, 0) +
				isnull(evx.ttp237, 0) +
				isnull(evx.ttp238, 0) +
				isnull(evx.ttp239, 0) +
				isnull(evx.ttp240, 0) +
				isnull(evx.ttp241, 0)
			) <> 0
	;
	
	if v_dateEnd is null then
		return;
	end if;
	
	set v_dateBegin = dateadd(day, 1 - in_days, v_dateEnd);
	
	call gfisp_populate_ridership_data_batch (v_dateBegin, v_dateEnd, 'N');
end;
go

grant execute on dba.gfisp_populate_ridership_data_portion to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_xerox_tr_insert') then
	drop proc gfisp_xerox_tr_insert;
end if;
go

create procedure dba.gfisp_xerox_tr_insert ( 
	in v_id  int,  
	in v_seq  int,  
	in v_loc_n  int , 
	in v_type1    varchar  (50), 
	in v_operation_datetime    datetime,  
	in v_route_id    int ,  
	in v_run_id    int ,
	in v_operation_type    int,  
	in v_operation_request_id int,
	in v_ticket_no_pos    int,
	in v_contract_id    int , 
	in v_contract_no    int , 
	in v_rider_cat_id    int , 
	in v_media_type    varchar  (4),
	in v_media_cd    int,  
	in v_media_serial_no    int,  
	in v_unit_bal_before    int,  
	in v_unit_bal_after    int ,  
	in v_start_date_after    date , 
	in v_end_date_before    date,  
	in v_end_date_after    date , 
	in v_number_riders    int , 
	in v_operation_amount    int, 
	in v_num_pending_reloads    int , 
	in v_Subscript_type    int , 
	in v_granted_bonus_units    int,  
	in v_granted_bonus_amount    int, 
	in v_sub_act_threshold    int , 
	in v_sub_reload_amount    int ,  
	in v_sub_owner_id    int,  
	in v_sub_status    int , 
	in v_act_auth    int , 
	in v_act_recorded    int,   
	in v_month_last_act    int,  
	in v_year_last_act    int, 
	in v_validity_cal    int ,  
	in v_card_seq_no    int , 
	in v_pay_type    int,  
	in v_validation_type    varchar  (1),
	in v_deducted_units    int , 
	in v_remaining_units    int, 
	in v_contract_end_date    date , 
	in v_reject_cd    int , 
	in v_list_type    varchar  (3),
	in v_xerox_tday date,
	in v_dir char(1)
)
begin
               if v_type1 = 'LIS' then
                              Insert into  dba.xerox_tr(
                                             id,  
                                             seq,  
                                             loc_n,
                                             type1,
                                             operation_datetime,
                                             route_id,
                                             run_id,
                                             list_type,
                                             media_type,
                                             media_serial_no,
                                             media_cd,
                                            xerox_tday,
            dir
                                             )
                                             
                              Values(
                                             v_id,  
                                             v_seq, 
                                             v_loc_n,
                                             v_type1,
                                             v_operation_datetime,
                                             v_route_id,
                                             v_run_id,
                                             v_list_type,
                                             v_media_type, 
                                             v_media_serial_no,
                                             v_media_cd,
                                            v_xerox_tday,
            v_dir
                                             );

               elseif v_type1 = 'REF' then
                              Insert into  dba.xerox_tr (
                                             id,  
                                             seq, 
                                             loc_n,
                                             type1,
                                             operation_datetime,
                                             route_id,
                                             run_id,
                                             contract_id,
                                             media_type,
                                             media_serial_no,
                                             media_cd,
                                             reject_cd,
                                            xerox_tday,
            dir
                                             )
                                             
                              Values(
                                             v_id,  
                                             v_seq, 
                                             v_loc_n,
                                             v_type1,
                                             v_operation_datetime,
                                             v_route_id,
                                             v_run_id,
                                             v_contract_id,
                                             v_media_type,
                                             v_media_serial_no,
                                             v_media_cd,
                                             v_reject_cd,
                                             v_xerox_tday,
            v_dir
                                             );

               elseif v_type1 = 'VAL' then
                              Insert into  dba.xerox_tr (
                                             id,  
                                             seq, 
                                             loc_n,
                                             type1,
                                             operation_datetime,
                                             route_id,
                                             run_id,
                                             contract_id,
                                             rider_cat_id,
                                             contract_no,
                                             validation_type,
                                             media_type,
                                             media_cd,
                                             media_serial_no,
                                             deducted_units,
                                             remaining_units,
                                             number_riders,
                                             contract_end_date,
                                             num_pending_reloads,
                                             card_seq_no,
                                             xerox_tday,
            dir
                                             )
                                             
                              Values(
                                             v_id,  
                                             v_seq, 
                                             v_loc_n,
                                             v_type1,
                                             v_operation_datetime,
                                             v_route_id,
                                             v_run_id,
                                             v_contract_id,
                                             v_rider_cat_id,
                                             v_contract_no,
                                             v_validation_type,
                                             v_media_type,
                                             v_media_cd,
                                             v_media_serial_no,
                                             v_deducted_units,
                                             v_remaining_units,
                                             v_number_riders,
                                             v_contract_end_date,
                                             v_num_pending_reloads,
                                             v_card_seq_no,
                                             v_xerox_tday,
            v_dir
                                             );

               elseif v_type1 = 'SAL' then
                              Insert into  dba.xerox_tr (
                                             id,  
                                             seq, 
                                             loc_n,
                                             type1,
                                             operation_datetime,
                                             route_id,
                                             run_id,
                                             operation_type,
                                             operation_request_id,
                                             ticket_no_pos,
                                             contract_id,
                                             contract_no,
                                             rider_cat_id,
                                             media_type,
                                             media_cd,
                                             media_serial_no,
                                             unit_bal_before,
                                             unit_bal_after,
                                             start_date_after,
                                             end_date_before,
                                             end_date_after,
                                             number_riders,
                                             operation_amount,
                                             num_pending_reloads,
                                             Subscript_type,
                                             granted_bonus_units,
                                             granted_bonus_amount,
                                             sub_act_threshold,
                                             sub_reload_amount,
                                             sub_owner_id,
                                             sub_status,
                                             act_auth,
                                             act_recorded,
                                             month_last_act,
                                             year_last_act,
                                             validity_cal,
                                             card_seq_no,
                                             pay_type,
                                             xerox_tday,
            dir
                                             )
                                             
                              Values(
                                             v_id,  
                                             v_seq, 
                                             v_loc_n,
                                             v_type1,
                                             v_operation_datetime,
                                             v_route_id,
                                             v_run_id,
                                             v_operation_type,
                                             v_operation_request_id,
                                             v_ticket_no_pos,
                                             v_contract_id,
                                             v_contract_no,
                                             v_rider_cat_id,
                                             v_media_type,
                                             v_media_cd,
                                             v_media_serial_no,
                                             v_unit_bal_before,
                                             v_unit_bal_after,
                                             v_start_date_after,
                                             v_end_date_before,
                                             v_end_date_after,
                                             v_number_riders,
                                             v_operation_amount,
                                             v_num_pending_reloads,
                                             v_Subscript_type,
                                             v_granted_bonus_units,
                                             v_granted_bonus_amount,
                                             v_sub_act_threshold,
                                             v_sub_reload_amount,
                                             v_sub_owner_id,
                                             v_sub_status,
                                             v_act_auth,
                                             v_act_recorded,
                                             v_month_last_act,
                                             v_year_last_act,
                                             v_validity_cal,
                                             v_card_seq_no,
                                             v_pay_type,
                                             v_xerox_tday,
            v_dir
                                             );

               elseif v_type1 = 'SRU' then
                              Insert into  dba.xerox_tr (
                                             id,  
                                             seq, 
                                             loc_n,
                                             type1,
                                             operation_datetime,
                                             route_id,
                                             run_id,
                                            xerox_tday,
											dir
                                             )
                                             
                              Values(
                                             v_id,  
                                             v_seq, 
                                             v_loc_n,
                                             v_type1,
                                             v_operation_datetime,
                                             v_route_id,
                                             v_run_id,
                                            v_xerox_tday,
											v_dir
                                             );

    end if;
end;
go

grant execute on dba.gfisp_xerox_tr_insert to public
go


if exists(select 1 from sys.sysprocedure where proc_name='gfisp_farebox_pm_rpt') then
   drop proc gfisp_farebox_pm_rpt;
end if;
go

CREATE PROCEDURE dba.gfisp_farebox_pm_rpt
( InLoc varchar(4000)
, InFromDate datetime
, InToDate datetime
, InBus varchar(4000)
, InMxCnUsg varchar(15)
, InMxBlUsg varchar(15)
, InMxTkUsg varchar(15)
, InMxBlRej varchar(15)
, InMxCdMsr varchar(15)
, InMndSrvSch varchar(15)
)
BEGIN
-- CALL gfisp_farebox_pm_rpt ('1,2', '20160509', '20160525', '1,3,5,8,9,111,222,333,321,334,400,444,445,2010,2011', '250000', '50000', '50000', '5', '0', '6');
	--print InLoc
	--print InFromDate 
	--print InToDate
	--print InBus
	--print InMxCnUsg
	--print InMxBlUsg
	--print InMxTkUsg
	--print InMxBlRej
	--print InMxCdMsr
	--print InMndSrvSch

--------------------------------------------------------------------------------------
	declare stringvar varchar(20);
	declare ind int;

	create table #Loc (LocType int);

	if (InLoc is not null)
	then
		set ind = charindex(',',InLoc);
		while ind > 0
		loop
			  set stringvar = substring(InLoc,1,ind-1);
			  set InLoc = substring(InLoc,ind+1,length(InLoc)-ind);
			  insert into #Loc values (stringvar);
			  set ind = charindex(',',InLoc);
		end loop;
		set stringvar = InLoc;
		insert into #Loc values (stringvar);
	end if;

	create table #Bus (BusType int);

	if(InBus is not null)
	then
		set ind = charindex(',',InBus);
		while ind > 0
		loop
			  set stringvar = substring(InBus,1,ind-1);
			  set InBus = substring(InBus,ind+1,length(InBus)-ind);
			  insert into #Bus values (stringvar);
			  set ind = charindex(',',InBus);
		end loop;
		set stringvar = InBus;
		insert into #Bus values (stringvar);
	end if;

------------------------------------------------------------------------------------

	SELECT
	   ml.loc_n
	  ,ml.fbx_n
	  ,MAX(ml.bus) bus
	  ,MAX(Allbus.ts) pm_ts
	  ,MIN(ml.ts) prb_ts
	  ,SUM(ml.penny + ml.nickel + ml.dime + ml.quarter + ml.half + ml.sba + ml.token_c) ct_c
	  ,SUM(ml.bill_c) b_c
	  ,SUM(ml.ticket_c + ml.pass_c) tp_c
	  ,SUM((CASE
			WHEN ml.tday BETWEEN InFromDate AND InToDate THEN ml.bill_e
			ELSE 0
			END)) brej_c
	  ,SUM((CASE
			WHEN ml.tday BETWEEN InFromDate AND InToDate THEN gfi_ml.misread_c
			ELSE 0
			END)) misread_c
	  ,MAX(ml.ts) last_prb_ts --new	  
  
	FROM (ml
		  JOIN (SELECT ml.loc_n loc, bus, MAX(ev.ts) ts 
							FROM ml, ev
							WHERE ml.loc_n = ev.loc_n
							AND ml.id = ev.id
							AND (type IN (21, 22)OR (type = 25 AND n BETWEEN 1001 AND 1006))
							AND ml.loc_n IN (select * from #Loc)
							AND ml.bus IN (select * from #Bus)
							GROUP BY ml.loc_n, bus) Allbus
		   ON ml.loc_n = Allbus.loc 
                  AND ml.bus = ALLBUS.bus 
                  AND ml.ts > Allbus.ts 
             LEFT OUTER JOIN gfi_ml 
                          ON ml.loc_n = gfi_ml.loc_n 
                             AND ml.id = gfi_ml.id )
		GROUP BY ml.loc_n, ml.fbx_n
	HAVING SUM(ml.penny + ml.nickel + ml.dime + ml.quarter + ml.half + ml.sba + ml.token_c) > InMxCnUsg
	OR SUM(ml.bill_c) > InMxBlUsg
	OR SUM(ml.ticket_c + ml.pass_c) > InMxTkUsg
	OR SUM((CASE
				WHEN ml.tday BETWEEN InFromDate AND InToDate THEN ml.bill_e
				ELSE 0
				END)) > InMxBlRej
	OR SUM((CASE
				WHEN ml.tday BETWEEN InFromDate AND InToDate THEN ml.pass_e
				ELSE 0
				END)) > InMxCdMsr
	OR Datediff(dd, ( CASE 
                               WHEN Max(Allbus .ts) IS NULL THEN Min(ml.ts) 
                               ELSE Max(Allbus.ts) 
                               END ), Getdate()) > Cast(InMndSrvSch AS INT) 
  
END;
go

grant execute on dba.gfisp_farebox_pm_rpt to public
go

if exists(select null from sys.sysprocedure where proc_name='gfisp_remove_probing_duplicates') then
   drop proc gfisp_remove_probing_duplicates;
end if;
go

create procedure dba.gfisp_remove_probing_duplicates(
	in in_locations varchar(8000) default null,
	in in_dateBegin date default null,
	in in_dateEnd date default null,
	in in_IncludeZeroRevenue char(1) default 'Y'
)
begin
/*
-- call gfisp_remove_probing_duplicates;
-- call gfisp_remove_probing_duplicates ('1,2,3', '2016-10-03', '2016-10-03');
call gfisp_remove_probing_duplicates ('1', '2016-10-03', '2016-10-03', 'N');
*/
	declare v_int int;
	declare v_command varchar(8000);
	declare v_count int;
	declare v_revenue int;
	declare v_process_result varchar(250);
	declare run_date  datetime;
	set run_date  = getdate();

	set in_locations = isnull(nullif(ltrim(in_locations), 'All'), '');

	if in_locations = '' then
		select
			list(loc_n order by loc_n) into in_locations
		from cnf;
	end if;

	set in_dateBegin = isnull(in_dateBegin, dateadd(day, -7, getdate()));
	set in_dateEnd = isnull(in_dateEnd, getdate());
	
	if in_locations = '' then
		set v_process_result = 'Error: in_locations = '''' !';
		
		return;
	end if;
	
	if in_dateBegin > in_dateEnd then
		set v_process_result = 'Error: in_dateBegin > in_dateEnd !';
		
		return;
	end if;
	
	create table #loc_ns (
		loc_n smallint,
		loc_name varchar(80)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	if in_IncludeZeroRevenue = 'Y' then
		set v_revenue = -1;
	else
		set v_revenue = 0;
	end if;
	
	if not exists(select 1 from sys.systable where table_name='ml_duplicates' and table_type='BASE' and creator=user_id('DBA')) then
		select * into ml_duplicates  from ml  where 0 = 1 ;
		alter table ml_duplicates add create_ts datetime null;	
		alter table ml_duplicates add [action] varchar(10) null	;
	end if;	
	
	create table #maxIDs(
		loc_n smallint,
		tday datetime,
		bus int,
		cbx_n smallint,
		dime smallint,
		nickel smallint,
		penny smallint,
		quarter smallint,
		sba smallint,
		half smallint,
		one smallint,
		two smallint,
		five smallint,
		ten smallint,
		twenty smallint,
		ts datetime,
		maxID int
	);

	-- Step #1
	-- there should be no duplicate EV records for the same location, tday, bus, n, route, run, trip, curr_r, uncl_r, type and ts
	-- last associated ML record should remain
	
	delete from #maxIDs;
	
	insert into #maxIDs
	select
		ml.loc_n,
		ml.tday,
		ml.bus,
		ml.cbx_n,		
		ml.dime,
		ml.nickel,
		ml.penny,
		ml.quarter,
		ml.sba,
		ml.half,
		ml.one,
		ml.two,
		ml.five,
		ml.ten,
		ml.twenty,
		ml.tday, 		
		max(ml.id)	-- maxID
	from ml
	inner join #loc_ns as l
		on	ml.loc_n = l.loc_n
	where	ml.tday between in_dateBegin and in_dateEnd
		and	ml.curr_r > v_revenue
	group by
		ml.loc_n,
		ml.tday,
		ml.bus
	having count(*) > 1;
	
	insert into ml_duplicates
	select distinct ml.*,run_date, 'kept' from ml, #maxIDs m  where ml.id = m.maxID;
	
	set v_count = 0;
	
	set v_command = null;
	
	select top 1
//		'call gfisp_purge_mlid (' + ltrim(str(ml.loc_n)) + ', ' + ltrim(str(ml.id)) + ', ' + ltrim(str(ml.id)) + ');' into v_command
		'insert into ml_duplicates select *,''' + convert(varchar(25),@run_date, 121) + ''', ''deleted'' from ml where ml.id =' + ltrim(str(ml.id)) + ';' 
			+	'exec gfisp_purge_mlid ' + ltrim(str(ml.loc_n)) + ', ' + ltrim(str(ml.id)) + ', ' + ltrim(str(ml.id)) + ';' into v_command
	from #loc_ns as l
	inner join ml
		on	ml.loc_n = l.loc_n
	inner join #maxIDs as m
			on	m.loc_n = ml.loc_n
			and	m.tday = ml.tday
			and	m.bus = ml.bus
			and m.cbx_n = ml.cbx_n
			and m.dime = ml.dime
			and m.nickel = ml.nickel
			and m.penny = ml.penny
			and m.quarter = ml.quarter
			and m.sba = ml.sba
			and m.half = ml.half
			and m.one = ml.one
			and m.two = ml.two
			and m.five = ml.five
			and m.ten = ml.ten
			and m.twenty = ml.twenty
			and	m.ts = ml.tday 
		and m.maxID <> ml.id
	where	ml.tday between in_dateBegin and in_dateEnd
		and	ml.curr_r > v_revenue
	order by
		ltrim(str(ml.loc_n)) + ', ' + ltrim(str(ml.id))
	;
	
	while v_command is not null loop
		execute (v_command);
		
		set v_count = v_count + 1;
	
		set v_command = null;
	
		select top 1
//		'call gfisp_purge_mlid (' + ltrim(str(ml.loc_n)) + ', ' + ltrim(str(ml.id)) + ', ' + ltrim(str(ml.id)) + ');' into v_command
		'insert into ml_duplicates select *,''' + convert(varchar(25),@run_date, 121) + ''', ''deleted'' from ml where ml.id =' + ltrim(str(ml.id)) + ';' 
			+	'exec gfisp_purge_mlid ' + ltrim(str(ml.loc_n)) + ', ' + ltrim(str(ml.id)) + ', ' + ltrim(str(ml.id)) + ';' into v_command
		from #loc_ns as l
		inner join ml
			on	ml.loc_n = l.loc_n
		inner join #maxIDs as m
			on	m.loc_n = ml.loc_n
			and	m.tday = ml.tday
			and	m.bus = ml.bus
			and m.cbx_n = ml.cbx_n
			and m.dime = ml.dime
			and m.nickel = ml.nickel
			and m.penny = ml.penny
			and m.quarter = ml.quarter
			and m.sba = ml.sba
			and m.half = ml.half
			and m.one = ml.one
			and m.two = ml.two
			and m.five = ml.five
			and m.ten = ml.ten
			and m.twenty = ml.twenty
			and	m.ts = ml.tday 
			and m.maxID <> ml.id
		where	ml.tday between in_dateBegin and in_dateEnd
			and	ev.curr_r > v_revenue
		order by
			ltrim(str(ml.loc_n)) + ', ' + ltrim(str(ml.id))
		;
	end loop;

	
	commit;
	
	if v_count = 0 then
		set v_process_result = 'No duplicates found.';
	else
		set v_process_result = 'Deleted ' || ltrim(str(v_count)) || ' duplicate(s).';
	end if;
	
	--	GD-2632 - 12/06/16 - MGM - Added select to pass output results to PB code
	select v_process_result;
end
go

grant execute on dba.gfisp_remove_probing_duplicates to public
go

////////////////////////////////////////////////////////////////////////
// gfisp_bus_history_revenue - GD-2206
////////////////////////////////////////////////////////////////////////
if exists(select null from sys.sysprocedure where proc_name='gfisp_bus_history_revenue') then
	drop proc gfisp_bus_history_revenue;
end if;
go

create procedure dba.gfisp_bus_history_revenue (
	in_locations varchar(8000),
	in_dateBegin date,
	in_dateEnd date,
	in_rrt char(1) default 'N',
	in_dll char(1) default 'N',
	in_alarm char(1) default 'N',
	in_event char(1) default 'N',
	in_busList varchar(8000) default '' )
begin
/*
call gfisp_bus_history_revenue('1,2', '2016-10-01', '2016-10-10');
call gfisp_bus_history_revenue('All', '2016-10-01', '2016-10-10');
call gfisp_bus_history_revenue('All', '2016-10-01', '2016-10-10', 'Y', 'N', 'N', 'N', '');
call gfisp_bus_history_revenue('All', '2016-10-01', '2016-10-10', 'N', 'N', 'Y', 'Y', '');
*/
	declare v_int int;

	set in_locations = isnull(nullif(ltrim(in_locations), 'All'), '');
	set in_dateBegin = isnull(in_dateBegin, getdate());
	set in_dateEnd = isnull(in_dateEnd, getdate());
	set in_rrt = isnull(in_rrt, 'N');
	set in_dll = isnull(in_dll, 'N');
	set in_alarm = isnull(in_alarm, 'N');
	set in_event = isnull(in_event, 'N');
	set in_busList = isnull(nullif(nullif(ltrim(rtrim(in_busList)), 'All'), '0'), '');
	
	if in_locations = '' then
		select list(loc_n order by loc_n) into in_locations
		from cnf;
	end if;
	
	if in_busList = '' then
		select list(bus order by bus) into in_busList
		from
		(
			select distinct
				bus
			from ml
			where	tday between in_dateBegin and in_dateEnd
		) as a;
	end if;
	
	-- let's get selected locations	
	create table #loc_ns(
		loc_n smallint,
		primary key (loc_n)
	);
	
	while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n
			from cnf with (nolock)
			where	loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n
			from cnf with (nolock)
			where	loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;
	
	-- let's get selected bus numbers
	create table #busNumbers(
		bus int,
		primary key (bus)
	);
	
	while in_busList <> '' loop
		set v_int = charindex(',', in_busList);
		
		if v_int = 0 then
			insert into #busNumbers
			values (cast(in_busList as int));
			
			set in_busList = '';
		else
			insert into #busNumbers
			values (cast(substring(in_busList, 1, v_int - 1) as int));

			set in_busList = substring(in_busList, v_int + 1, 8000);
		end if;
	end loop;
	
	-- let's get selected types
	create table #types (
		type smallint,
		primary key (type)
	);
	
	insert into #types
	select distinct
		ev.type
	from ml
	inner join ev
		on	ml.loc_n = ev.loc_n
		and ml.id = ev.id
	where	ml.tday between in_dateBegin and in_dateEnd;

	if in_dll = 'Y' and in_alarm = 'N'
	then
		-- only Driver Login/Logoff
		delete from #types
		where	type not in (3, 4, 36);
	elseif in_dll = 'N' and in_alarm = 'Y'
	then
		-- only Alarm
		delete from #types
		where	type in (3, 4, 36);
	end if;
	
	if in_rrt = 'N'
	and in_dll = 'N'
	and in_alarm = 'N'
	and in_event = 'N'
	then
		select
			ml.loc_n,
			ml.id,
			ml.tday,
			ml.prb_n,
			ml.bus,
			ml.ts,
			ml.fbx_n,
			ml.fbx_ts,
			ml.evnt_c,
			ml.cuml_r,
			ml.curr_r, 
			ml.uncl_r,
			ml.dump_c,
			ml.fare_c,
			ml.key1,ml.key2,ml.key3,ml.key4,ml.key5,ml.key6,ml.key7,ml.key8,ml.key9,
			ml.keyast,ml.keya,ml.keyb,ml.keyc,ml.keyd
		FROM ml
		inner join #loc_ns as l
			on	l.loc_n = ml.loc_n
		inner join #busNumbers as b
			on	b.bus = ml.bus
		where	ml.tday between in_dateBegin and in_dateEnd;
	elseif in_rrt = 'Y'
		and in_dll = 'N'
		and in_alarm = 'N'
		and in_event = 'N'
	then
		select distinct
			ml.loc_n,
			ml.id,
			ml.tday,
			ml.prb_n,
			ml.bus,
			ml.ts,
			ml.fbx_n,
			ml.fbx_ts,
			ml.evnt_c,
			ml.cuml_r,
			ml.curr_r, 
			ml.uncl_r,
			ml.dump_c,
			ml.fare_c,
			ml.key1,ml.key2,ml.key3,ml.key4,ml.key5,ml.key6,ml.key7,ml.key8,ml.key9,
			ml.keyast,ml.keya,ml.keyb,ml.keyc,ml.keyd,
			0 as seq,
			ev.ts,
			0 as type,
			0 as n,
			ev.fs, 
			ev.route,
			ev.run,
			ev.trip,
			ev.drv,
			ev.curr_r,
			ev.uncl_r,
			ev.dump_c,
			ev.fare_c,
			ev.key1,ev.key2,ev.key3,ev.key4,ev.key5,ev.key6,ev.key7,ev.key8,ev.key9,
			ev.keyast,ev.keya,ev.keyb,ev.keyc,ev.keyd,
			cast(null as varchar(100)) as tr_subtype,
			cast(null as varchar(8000)) as tr_events
		FROM ml
		inner join #loc_ns as l
			on	l.loc_n = ml.loc_n
		inner join #busNumbers as b
			on	b.bus = ml.bus
		inner join ev
			on	ml.loc_n = ev.loc_n
			and ml.id = ev.id
		where	ml.tday between in_dateBegin and in_dateEnd;
	else
		select
			ml.loc_n,
			ml.id,
			ml.tday,
			ml.prb_n,
			ml.bus,
			ml.ts,
			ml.fbx_n,
			ml.fbx_ts,
			ml.evnt_c,
			ml.cuml_r,
			ml.curr_r, 
			ml.uncl_r,
			ml.dump_c,
			ml.fare_c,
			ml.key1,ml.key2,ml.key3,ml.key4,ml.key5,ml.key6,ml.key7,ml.key8,ml.key9,
			ml.keyast,ml.keya,ml.keyb,ml.keyc,ml.keyd,
			ev.seq,
			ev.ts,
			ev.type,
			ev.n,
			ev.fs, 
			ev.route,
			ev.run,
			ev.trip,
			ev.drv,
			ev.curr_r,
			ev.uncl_r,
			ev.dump_c,
			ev.fare_c,
			ev.key1,ev.key2,ev.key3,ev.key4,ev.key5,ev.key6,ev.key7,ev.key8,ev.key9,
			ev.keyast,ev.keya,ev.keyb,ev.keyc,ev.keyd,
			cast(null as varchar(100)) as tr_subtype,
			cast(null as varchar(8000)) as tr_events
		FROM ml
		inner join #loc_ns as l
			on	l.loc_n = ml.loc_n
		inner join #busNumbers as b
			on	b.bus = ml.bus
		inner join ev
			on	ml.loc_n = ev.loc_n
			and ml.id = ev.id
		inner join #types as t
			on	t.type = ev.type
		where	ml.tday between in_dateBegin and in_dateEnd
			
		union

		select
			ml.loc_n,
			ml.id,
			ml.tday,
			ml.prb_n,
			ml.bus,
			ml.ts,
			ml.fbx_n,
			ml.fbx_ts,
			ml.evnt_c,
			ml.cuml_r,
			ml.curr_r, 
			ml.uncl_r,
			ml.dump_c,
			ml.fare_c,
			ml.key1,ml.key2,ml.key3,ml.key4,ml.key5,ml.key6,ml.key7,ml.key8,ml.key9,
			ml.keyast,ml.keya,ml.keyb,ml.keyc,ml.keyd,
			tr.tr_seq as seq,
			tr.ts,
			tr.type,
			tr.n,
			tr.fs, 
			tr.route,
			tr.run,
			tr.trip,
			tr.drv,
			null as curr_r,
			null as uncl_r,
			null as dump_c,
			null as fare_c,
			null as key1,null as key2,null as key3,null as key4,null as key5,null as key6,null as key7,null as key8,null as key9,
			null as keyast,null as keya,null as keyb,null as keyc,null as keyd,
			gfisf_tr_subtype(trmisc.tr_etype) as tr_subtype,
			gfisf_tr_events(trmisc.tr_etype, trmisc.tr_events) as tr_events
		FROM ml
		inner join #loc_ns as l
			on	ml.loc_n = l.loc_n
		inner join #busNumbers as b
			on	b.bus = ml.bus
		inner join tr
			on	ml.loc_n = tr.loc_n
			and ml.id = tr.id
		inner join trmisc
			on	trmisc.loc_n = tr.loc_n
			and	trmisc.id = tr.id
			and	trmisc.tr_seq = tr.tr_seq
			and	trmisc.tr_etype is not null
		where	in_event = 'Y'
			and	ml.tday between in_dateBegin and in_dateEnd
		;
	end if;
end;
go

grant execute on dba.gfisp_bus_history_revenue to public
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisp_cashbox_history') then
   drop proc gfisp_cashbox_history;
end if;
go

CREATE PROCEDURE DBA.gfisp_cashbox_history( 
	in_authority int,
	in_locations varchar(8000),
    in_datebegin date,
	in_dateend date,
    in_stm int,
	in_allcashboxes int,
	in_cashboxes varchar(8000)
)
BEGIN
-- =============================================
-- Author:		Syed Ibrahim
-- Create date: 05/23/2017
-- Description:	CashBox History Report
--				parameters:
--					in_authority - authority id
--					in_locations - comma separated list of location ids
--					in_dateBegin - date range begin
--					in_dateEnd - date range end
--					in_stm - how to process misc_c
--						1 - vlt.misc_c = pennies
--						2 - vlt.misc_c + vlt.quarter = quarters
--					in_allCashBoxes - included all cashboxes (0 or 1)
--					in_cashBoxes - comma separated list of cashbox ids
--
--              GD-424 - Cashbox History Report
-- =============================================

//call gfisp_cashbox_history (1, '1,2,3', '2016/09/01', '2016/09/30', 0, 1, '0');
//2. Add columns
//	a. Bus
//	b. Inserted Date
//	c. Removed Date
//	d. Current Probe Time
//	e. Current Probe Type (PDU, WIFI or IR)
//	f. Prior Probe Time
//	g. Prior Probe Type
//	h. Alarms 1-6 
//3. Section for Wireless Probing Detail from Cashbox Audit Report - We need to find out what is does in Albany but needed.
//4. Section for Vaulting Crank times as in Cashbox Audit Report
//5. Input
//	a. Change Date Range to "Cashbox Insert Date Range".
//	b. Add Bus List
//4. Wireless Probing Detail from Cashbox Audit Report � We need to find out what is does in Albany but needed.
//5. Cashbox Vault Time Statistics with Crank Time from Cashbox Audit Report Inputs
//6. Decommission Cashbox Last Seen Report
//7. Decommission Cashbox Reinsertion Removal Exception Report
//8. Decommission Cashbox Audit Report

declare v_int int;
set in_locations = isnull(nullif(ltrim(in_locations), 'All'), '');
    -- let''s get selected locations	
    create table #loc_ns(
		loc_n smallint,
        loc_name varchar(80),
		primary key (loc_n)
	);  
    
    while in_locations <> '' loop
		set v_int = charindex(',', in_locations);
		
		if v_int = 0 then
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where	loc_n = cast(in_locations as smallint);
			
			set in_locations = '';
		else
			insert into #loc_ns
			select loc_n, loc_name
			from cnf with (nolock)
			where	loc_n = cast(substring(in_locations, 1, v_int - 1) as smallint);

			set in_locations = substring(in_locations, v_int + 1, 8000);
		end if;
	end loop;

    -- let''s get all cash boxes
    create table #cbx_ns(
		cbx_n smallint
	);
    
   if in_allcashboxes = '0' then
     while in_cashboxes <> '' loop
        set v_int = charindex(',', in_cashboxes);
        
        if v_int = 0 then
    	    insert into #cbx_ns	values (cast(in_cashboxes as int));
    		set in_cashboxes = '';
    	else
    		insert into #cbx_ns	values (cast(substring(in_cashboxes, 1, v_int - 1) as int));
    		set in_cashBoxes = substring(in_cashBoxes, v_int + 1, 8000);
    	end if;

      end loop;
   else
     insert into #cbx_ns	values (0);
   end if;

    -- -------------
	-- return result
	-- -------------
    select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(1 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		vlt.bus,
		binid = cast(null as smallint),
		vlt.revenue, 
		vlt.probe_n,
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time =	(	select
									max(prv.probed)
								from vlt as prv
								where	prv.cbxid = vlt.cbxid
									and	prv.[type] = vlt.[type]
									and	prv.probed < vlt.probed
							),
		prior_probe_type =	(	select
									max(prv.probe_n)
								from vlt as prv
								where	prv.cbxid = vlt.cbxid
									and	prv.[type] = vlt.[type]
									and	prv.probed =	(	select
																max(prv.probed)
															from vlt as prv
															where	prv.cbxid = vlt.cbxid
																and	prv.[type] = vlt.[type]
																and	prv.probed < vlt.probed									
														)
							),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.probe_f > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(1 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] = 1
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(0 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (2, 3)
		and	vlt.probe_f = 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(2 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vlt.vault_n,
		bus = cast(null as int),
		vlt.binid,
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn =	case
						when bin.code is null then cast(vlt.binid as varchar(256))
						else bin.name
					end,
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	left join gfi_lst as bin
		on	bin.[type] = 'BIN ID'
		and	bin.class = in_authority
		and bin.code = vlt.binid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (2, 3)
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)
	
	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(3 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vlt.vault_n,
		bus = cast(null as int),
		vlt.binid,
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn =	case
						when bin.code is null then cast(vlt.binid as varchar(256))
						else bin.name
					end,
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	left join gfi_lst as bin
		on	bin.[type] = 'BIN ID'
		and	bin.class = in_authority
		and bin.code = vlt.binid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] = 3
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)
	
	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(4 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.timedsc > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(5 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.bypalm > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(6 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.outalm > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(7 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.memclr > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(8 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.inalm > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)

	union all
	
	select
		vlt.loc_n,
		vlt.tday,
		vlt.cbxid,
		[type] = cast(9 as smallint),
		vlt.probed,
		vlt.inserted,
		vlt.removed,
		vault_n = cast(null as smallint),
		bus = cast(null as int),
		binid = cast(null as smallint),
		revenue = cast(null as numeric(14, 2)),
		probe_n = cast(0 as tinyint),
		cbx_sn =	case
						when cbx.code is null then cast(vlt.cbxid as varchar(256))
						else cbx.name
					end,
		bin_sn = cast('' as varchar(256)),
		prior_probe_time = cast(null as datetime),
		prior_probe_type = cast(null as tinyint),
		vlt.outalm, vlt.inalm, vlt.bypalm, vlt.cbxalm, vlt.memclr, vlt.revdsc
	from vlt
	left join gfi_lst as cbx
		on	cbx.[type] = 'CBX ID'
		and	cbx.class = in_authority
		and cbx.code = vlt.cbxid
	where	vlt.loc_n in (select loc_n from #loc_ns)
		and vlt.tday between in_dateBegin and in_dateEnd
		and	vlt.[type] in (1, 2, 3)
		and	vlt.cbxalm > 0
		and	(	in_allCashBoxes = 1
			or	vlt.cbxid in (select cbx_n from #cbx_ns)
			)
	order by
		loc_n,
		tday,
		cbxid,
		[type],
		probed
	;
END
go

grant execute on dba.gfisp_cashbox_history to public
go

////////////////////////////////////////////////////////////////////////
Update dba.configurationsData set datavalue = 'Complete' where dataname in('GFIDBASA3');
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// END
////////////////////////////////////////////////////////////////////////