------------------------------------------------------------------------
-- Script to set Cloud setting in new install to Yes
-- DATABUILD-156 - RBB - 5/30/17
------------------------------------------------------------------------

Use gfi
go

------------------------------------------------------------------------
-- Make sure configurationsData table exists
------------------------------------------------------------------------
if object_id('configurationsData','U') is null
begin
	create table configurationsData(
		id              int          not null identity,
		configurationID smallint     not null,
		dataName        varchar(100) not null,
		dataValue       varchar(100) not null,
		isActive        bit          not null,
		comments        varchar(250) null
	);
end
go

grant select on configurationsData to public;
go

--------------------------------------------------------------------------
-- Add Cloud Setting to configurationsData
--------------------------------------------------------------------------	
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'Cloud')
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
		values(0, 'Cloud', 'Yes', 1, 'Cloud Setting');
else
	update configurationsData
		set dataValue = 'Yes'
		where configurationID = 0 and dataName = 'Cloud'
go



