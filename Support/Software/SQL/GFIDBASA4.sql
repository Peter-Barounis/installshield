﻿////////////////////////////////////////////////////////////////////////
// This SQL script updates GFI list items
// DBMS: Sybase Adaptive Server Anywhere 9
//
// $Author: rbecker $
// $Revision: 1.57 $
// $Date: 2017-04-04 22:35:50 $
// Note:
// - In gfisp_additem calls, | prefix in name and/or value arguments instruct the procedure to override name 
//		and/or value for existing entries
////////////////////////////////////////////////////////////////////////////

Begin
declare li_aut smallint;
declare ls     varchar(4000);
declare ls_eq  varchar(4000);
declare li     integer;
declare li_loc smallint;
declare li_v1  tinyint;
declare li_v2  tinyint;
declare li_v3  tinyint;
declare li_v4  tinyint;
declare pbVersion varchar(100);

set pbVersion = '3.1.2'; -- format #.##.##.##

////////////////////////////////////////////////////////////////////////
// * * * L I S T   D E F I N I T I O N S * * *
////////////////////////////////////////////////////////////////////////
call gfisp_deltype('TPBC');                       // obsolete lists
call gfisp_deltype('PROXY');
call gfisp_deltype('ALARM');
call gfisp_deltype('TVM IP');
call gfisp_deltype('SYS SUM');
call gfisp_deltype('VALID RANGE');
call gfisp_deltype('DB VER');

// format: column=display label <value range> (range default is from 1 to 2^31 - 1, for code only)
//         column.width=value
// flag: bit0-3 - hide class/code/name/value;
//       bit4-7 - readonly class/code/name/value (for existing rows only);
//       bit8   - do not allow add (256)
//       bit9   - do not allow delete (512) (delete new rows is always OK)
//       bit10  - readonly (no modify, no add, no delete) (1024)
//       bit11  - name in lower case
//       bit12  - name in upper case
// flag default: 0x0400
call gfisp_addtype('AUT',             'Transit Authority (code=ID <1-99>; name=Name; value=Description; logo=gfitransit.ico; flag=0x01)');
call gfisp_addtype('BIN ID',          'Bin ID and S/N Cross-Reference List (class=Transit Authority; code=ID <0-3999>; name=S/N; logo=Join!; flag=0x38)');
call gfisp_addtype('CBX ID',          'Cashbox ID and S/N Cross-Reference List (class=Transit Authority; code=ID <0-3999>; name=S/N; logo=Join!; flag=0x38)');
call gfisp_addtype('CARD TYPE',       'Card Category (code=Category <1-255>; name=Description; value=Note; logo=gfimedia.ico; flag=0x0321)');
call gfisp_addtype('CARD TYPE2',      'Card Brand Type (code=Type <0-255>; name=Description; value=Note; logo=gfimedia.ico; flag=0x0361)');
call gfisp_addtype('CC TYPE',         'Credit Card Type (code=Type <0-255>; name=Name; value=Description; logo=gfimedia.ico; flag=0x0321)');
call gfisp_addtype('CHANNEL',         'Sales and Service Channel (code=ID <1-255>; name=Description; value=Note; logo=OleGenReg!; flag=0x0321)');
call gfisp_addtype('COLOR',           'Color Code (code=ID; name=Code; value=Description; logo=ArrangeIcons!; flag=0x0401)');
call gfisp_addtype('COUNTRY',         'Country Code (code=ID <1-255>; name=Code; value=Name; logo=Start!; flag=0x0401)');
call gfisp_addtype('CS STATUS',       'Socket Connection Status between Network Manager Server and Data System Server (code=Location; name="Status (1/0 - connected/disconnected)"; name.width=923; value=Description; logo=Join!; flag=0x0401)');
call gfisp_addtype('EMAIL',           'Mailing List (code=ID; name=Name; value=Email addresses (use semicolon to delimit); value.width=2299; logo=gfiemail.ico; flag=0x21)');
call gfisp_addtype('EQ LOC',          'TVM/PEM Station (class=Transit Authority; code=ID <1-255>; name=Name; value="Description (name, address, etc.)"; logo=gfistation.ico)');
call gfisp_addtype('EQ STATUS',       'TVM/PEM Status Type (class=Color; class.width=302; code=Type <1-255>; code.width=151; name=Label; value=Description; value.width=2299; logo=gfilview.ico; flag=0x0320)');
call gfisp_addtype('EQ TYPE',         'Equipment Type (code=Type <1-255>; name=Name; value=Description; logo=gfieqpem16.ico; flag=0x0401)');
call gfisp_addtype('EV CLASS',        'Event Category (code=Category <1-255>; name=Label; value=Description; logo=Count!; flag=0x21)');
call gfisp_addtype('EV TYPE',         'Event Type (class=Event Category; class.width=654; code=Type <1-9999>; code.width=151; name=Description; name.width=864; value=Note; logo=EnglishEdit!; flag=0x0320)');
call gfisp_addtype('FARE TYPE',       'Fare Type (code=Type; name=Description; value=Note; logo=gfirider.ico; flag=0x0321)');
call gfisp_addtype('FIS PAY TYPE',    'FIS Payment Type (code=ID <1-255>; name=Type; value=Description; logo=gfimedia.ico; flag=0x1361)');
call gfisp_addtype('FS PEAK TIME',    'Fare Structure - Peak Time (class=Media; code=ID <1-255>; name=Peak On Time; value=Peak Off Time; logo=ShowWatch!; flag=0x22)');
call gfisp_addtype('FS HOLIDAY',      'Fare Structure - Holiday (class=Media; code=ID <1-255>; name=Name; value=Value; logo=ComputeToday5!; flag=0x22)');
call gfisp_addtype('INSP CODE',       'Inspection Code (code=Code; name=Label; value=Description; logo=EditDataTabular!; flag=0x0321)');
call gfisp_addtype('INST TYPE',       'Institution Type (code=Type <1-255>; name=Description; value=Note; logo=Structure!; flag=0x21)');
call gfisp_addtype('ISSU CODE',       'Infraction Inspection Issuance Code (code=Code; name=Label; value=Description; logo=DosEdit!; flag=0x21)');
call gfisp_addtype('L2G PARM',        'FIS Parameter (name=Parameter; value=Value; logo=l2g16.ico; flag=0x0343)');
call gfisp_addtype('L2G PARM HELP',   'FIS Parameter Descriptor (name=Parameter; value=Descriptor (data type;description))');
call gfisp_addtype('MNT CODE',        'Maintenance Code (code=Code <1-255>; name=Label; value=Description; logo=CreateLibrary!; flag=0x21)');
call gfisp_addtype('MOD STATUS',      'Module Status Code (code=Code <1-255>; name=Label; value=Description; logo=InsertReturn!; flag=0x21)');
call gfisp_addtype('MOD TYPE',        'Module Type (code=Type <1-255>; name=Label; value=Description; logo=InsertReturn!; flag=0x0361)');
call gfisp_addtype('NOTIFY TYPE',     'Notification Type (code=Type <1-255>; name=Label; value=Description; logo=gfiemail.ico; flag=0x0321)');
call gfisp_addtype('PASS CLASS',      'Passenger Type (code=Type <1-255>; name=Label; value=Description; logo=gfigroup.ico; flag=0x0321)');
call gfisp_addtype('PASS TYPE',       'Card Type (code=Type <1-255>; name=Label; value=Description; logo=gfimedia.ico; flag=0x21)');
call gfisp_addtype('PAY TYPE',        'Payment Type (code=Type <1-255>; name=Label; value=Description; logo=Custom048!; flag=0x0321)');
call gfisp_addtype('PDP PARM',        'Bluetooth Probe Parameter (code=ID; name=Parameter; value=Value; logo=bluetooth.ico; flag=0x0343)');
call gfisp_addtype('PDP PARM HELP',   'Bluetooth Probe Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;description))');
call gfisp_addtype('PROD TYPE',       'Product Type (code=Type <1-255>; name=Description; value=Note; logo=UserObject5!; flag=0x0321)');
call gfisp_addtype('PWD QUESTION',    'Password Security Question (code=ID <1-255>; code.width=151; name=Question; name.width=2094; value=Note; logo=Help!; flag=0x21)');
call gfisp_addtype('RAIL LINE',       'Rail Line (code=ID; code.width=151; name=Name; value=Description; value.width=2048; logo=gfirail.ico; flag=0x21)');
call gfisp_addtype('RAIL LN-ST-ZN',   'Rail Line-Station-Zone (class=Rail Line; code=GFI Station ID; name=Rail Fare Zone; value=Sort Order; logo=gfilnstz16.ico; flag=0)');
call gfisp_addtype('RAIL STATION',    'Rail Station (class=Transit Station ID; code=GFI Station ID; name=Name; value=Display Text; logo=gfistation.ico; flag=0)');
call gfisp_addtype('SHIPPER',         'Shipping Service (code=ID <1-255>; name=Name; value=Description; logo=CreateRuntime!; flag=0x21)');
call gfisp_addtype('SHROUD',          'Vault Shroud Interface (class=Location; code=Vault Number <1-8>; name=Enabled; logo=shroud.ico; flag=0x0328)');
call gfisp_addtype('STATE',           'State/Province Code (class=Country; class.width=434; code=ID <1-255>; code.width=151; name=Code; name.width=151; value=Name; value.width=850; logo=Start!)');
call gfisp_addtype('STOCK TYPE',      'Stock Type (code=Type <1-255>; name=Label; value=Description; logo=IncrementalBuild!; flag=0x0321)');
call gfisp_addtype('SUM LABEL',       'Summary Item Display Label (class=TVM/PEM; class.width=265; code=ID; name=Code; name.width=215; value=Display text; logo=ComputeSum!; flag=0x0360)');
call gfisp_addtype('SYS PARM',        'System Parameter (code=ID; name=Parameter; value=Value; logo=EditFuncDeclare!; flag=0x0343)');
call gfisp_addtype('SYS PARM HELP',   'System Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;description))');
call gfisp_addtype('TVM SUM',         'TVM System Summary for the Current Day (class=Transit Authority; code=ID; name=Label Code (SUM LABEL); value=Value; logo=gfieqtvm.ico)');
call gfisp_addtype('PEM SUM',         'PEM System Summary for the Current Day (class=Transit Authority; code=ID; name=Label Code (SUM LABEL); value=Value; logo=gfieqpem16.ico)');
call gfisp_addtype('TVM PARM',        'TVM Parameter (code=ID; name=Parameter; value=Value; logo=gfieqtvm.ico; flag=0x0343)');
call gfisp_addtype('TVM PARM HELP',   'TVM Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;TVM specific;description))');
call gfisp_addtype('PEM PARM',        'PEM Parameter (code=ID; name=Parameter; value=Value; logo=gfieqpem16.ico; flag=0x0343)');
call gfisp_addtype('PEM PARM HELP',   'PEM Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;PEM specific;description))');
call gfisp_addtype('TVM PARM SEQ',    'TVM Parameter File List (code=Sequence Number <1-10>; name=Name; value=Effective Date; logo=gfieqtvm.ico; flag=0x01)');
call gfisp_addtype('PEM PARM SEQ',    'PEM Parameter File List (code=Sequence Number <1-10>; name=Name; value=Effective Date; logo=gfieqpem16.ico; flag=0x01)');

////////////////////////////////////////////////////////////////////////
// List: AUT
////////////////////////////////////////////////////////////////////////
call gfisp_delitem('AUT',null,null,0);            // remove "Undefined" authority
                                                  // if no authority has been defined, migrate from aut table
if not exists(select 0 from gfi_lst where type='AUT' and code>0) then
   if exists(select 0 from aut, aut_cnf where aut.aut_n=aut_cnf.aut_n) then
      insert into gfi_lst (type,class,code,name,value)
      select distinct 'AUT',0,aut.aut_n,trim(aut_name),trim(aut_name) from aut, aut_cnf where aut.aut_n=aut_cnf.aut_n;
      commit;
   else                                           // for empty database, add transit authority 1
      call gfisp_additem('AUT','Transit authority 1',0,1,'Transit authority 1');
   end if;
end if;
                                                  // use the first authority as the default one
select isnull(min(code),1) into li_aut from gfi_lst where type='AUT' and class=0 and code>0;

////////////////////////////////////////////////////////////////////////
// List: CARD TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('CARD TYPE','Period card',0,1);
call gfisp_additem('CARD TYPE','Ride card',  0,2);
call gfisp_additem('CARD TYPE','Value card', 0,3);

////////////////////////////////////////////////////////////////////////
// List: CARD TYPE2
////////////////////////////////////////////////////////////////////////
call gfisp_additem('CARD TYPE2','|Magnetic',         0,0);
call gfisp_additem('CARD TYPE2','|MiFare DESFire',   0,1);
call gfisp_additem('CARD TYPE2','|MiFare Ultralight',0,2);
call gfisp_additem('CARD TYPE2','|MiFare Classic',   0,3);

delete from gfi_lst where type='CARD TYPE2' and class=0 and code not in (0,1,2,3);
commit;

////////////////////////////////////////////////////////////////////////
// List: CC TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('CC TYPE','Undefined',0,0,'Undefined credit card type');
call gfisp_additem('CC TYPE','VISA',     0,1,'VISA');
call gfisp_additem('CC TYPE','Master',   0,2,'Master Card');
call gfisp_additem('CC TYPE','Discover', 0,3,'Discover Card');
call gfisp_additem('CC TYPE','AMEX',     0,4,'American Express');

////////////////////////////////////////////////////////////////////////
// List: CHANNEL
////////////////////////////////////////////////////////////////////////
call gfisp_additem('CHANNEL','Ticket Vending Machine',         0,1);
call gfisp_additem('CHANNEL','Retail Outlets',                 0,2);
call gfisp_additem('CHANNEL','Transit Centers',                0,3);
call gfisp_additem('CHANNEL','Customer Service',               0,4);
call gfisp_additem('CHANNEL','Pass-by-Mail',                   0,5);
call gfisp_additem('CHANNEL','Web - Individual',               0,6);
call gfisp_additem('CHANNEL','Web - Bulk',                     0,7);
call gfisp_additem('CHANNEL','Farebox',                        0,8);
call gfisp_additem('CHANNEL','Administration',                 0,9);
call gfisp_additem('CHANNEL','Special Services',               0,10);
call gfisp_additem('CHANNEL','Registered Organization via POS',0,11);

////////////////////////////////////////////////////////////////////////
// List: COLOR
////////////////////////////////////////////////////////////////////////
call gfisp_additem('COLOR','|g',  0,0,'Green');
call gfisp_additem('COLOR','|b',  0,1,'Blue');
call gfisp_additem('COLOR','|y',  0,2,'Yellow');
call gfisp_additem('COLOR','|r',  0,3,'Red');
call gfisp_additem('COLOR','|w',  0,4,'White');
call gfisp_additem('COLOR','|amb',0,5,'Amber');
call gfisp_additem('COLOR','|uv', 0,6,'Ultra-violet');

////////////////////////////////////////////////////////////////////////
// List: COUNTRY
////////////////////////////////////////////////////////////////////////
call gfisp_additem('COUNTRY','|US',0,1,'United States');
call gfisp_additem('COUNTRY','|CA',0,2,'Canada');
call gfisp_additem('COUNTRY','|MX',0,3,'Mexico');

////////////////////////////////////////////////////////////////////////
// List: EQ LOC
////////////////////////////////////////////////////////////////////////
                                                  // delete stations without transit authorities
delete from gfi_lst where type='EQ LOC' and class not in (select code from gfi_lst where type='AUT');
                                                  // add "Undefined" stations, one for each transit authority
insert into gfi_lst (type,class,code,name,value)
   select 'EQ LOC',code,0,'Undefined','Undefined TVM/PEM station'
   from gfi_lst aut where type='AUT' and class=0 and not exists(select 0 from gfi_lst where type='EQ LOC' and class=aut.code and code=0);

commit;

if not exists(select 0 from gfi_lst where type='EQ LOC' and code>0) then
   call gfisp_additem('EQ LOC','Station 1',li_aut,1,'Station 1');
end if;

////////////////////////////////////////////////////////////////////////
// List: EQ STATUS
////////////////////////////////////////////////////////////////////////
call gfisp_additem('EQ STATUS','Normal',          0,0,'The equipment is functioning normally; events that are not associated with other status types automatically fall into this category');
call gfisp_additem('EQ STATUS','Priority 3 alarm',1,1,'Maintenance access in progress or attention needed');
call gfisp_additem('EQ STATUS','Priority 2 alarm',2,2,'The equipment is malfunctioning, out of service, or off-line');
call gfisp_additem('EQ STATUS','Priority 1 alarm',3,3,'Security alert such as intrusion, impact, and silent alarm');
call gfisp_additem('EQ STATUS','Disconnected',    4,4,'No communication');

////////////////////////////////////////////////////////////////////////
// List: EQ TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('EQ TYPE','|TVM',0,1,'|Ticket Vending Machine (TVM)');
call gfisp_additem('EQ TYPE','|PEM',0,2,'|Printer/Encoder Machine (PEM)');

////////////////////////////////////////////////////////////////////////
// List: EV CLASS
////////////////////////////////////////////////////////////////////////
call gfisp_additem('EV CLASS','Undefined',  0,0,'Undefined event/transaction type class/category');
call gfisp_additem('EV CLASS','Transaction',0,1,'Transactions');
call gfisp_additem('EV CLASS','Service',    0,2,'Service events');
call gfisp_additem('EV CLASS','Security',   0,3,'Security events');
call gfisp_additem('EV CLASS','Revenue',    0,4,'Revenue events');
call gfisp_additem('EV CLASS','Hot',        0,5,'Hot events');
call gfisp_additem('EV CLASS','Information',0,6,'Informational events');

////////////////////////////////////////////////////////////////////////
// List: EV TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_addevt(0,  0,'Undefined','Undefined event/transaction type');
call gfisp_addevt(6,  1,'Log event');
// Transactions
call gfisp_addevt(1,301,'Sign-in',           'PEM only');
call gfisp_addevt(1,302,'Issue card');
call gfisp_addevt(1,303,'Add value',         'PEM only');
call gfisp_addevt(1,304,'Cancel card');
call gfisp_addevt(1,305,'Replace card');
call gfisp_addevt(1,306,'Sign-out',          'PEM only');
call gfisp_addevt(1,307,'Issue badge',       'PEM only');
call gfisp_addevt(1,308,'Delete badge',      'PEM only');
call gfisp_addevt(1,309,'Event',             'PEM only');
call gfisp_addevt(1,310,'Clear bad-list',    'PEM only');
call gfisp_addevt(1,311,'Read card',         'PEM only');
call gfisp_addevt(1,312,'Recharge');
call gfisp_addevt(1,313,'Card cancelled',    'PEM only');
call gfisp_addevt(1,314,'Batch issue card',  'PEM only');
call gfisp_addevt(1,315,'Cancel transaction','PEM only (set the cancel bit on card and print CANCEL on card)');
call gfisp_addevt(1,316,'Validate');
call gfisp_addevt(1,317,'Deduct',            'e.g. Paying with a value card');
call gfisp_addevt(1,318,'Proof of purchase');
if exists(select 0 from gfi_lst where type='EV TYPE' and code=319 and name='Refund') then
   delete from gfi_lst where type='EV TYPE' and code=319 and name='Refund';
   commit;
end if;
call gfisp_addevt(1,319,'Cancel/Refund',     'TVM only');
call gfisp_addevt(1,320,'Master record',     'Multi-purchase transaction');
call gfisp_addevt(1,321,'Overwrite card',    'Overwrite card with the current format and settings');
// TRiM
call gfisp_addevt(6,401,'TRiM disabled');
call gfisp_addevt(6,402,'TRiM enabled');
call gfisp_addevt(2,403,'TRiM offline');
call gfisp_addevt(2,404,'TRiM online');
call gfisp_addevt(2,405,'TRiM swapped',                      'Replace the current module with a different one');
call gfisp_addevt(2,406,'TRiM card obstruction',             'Card jam in TRiM/cassette; n: 0 - condition on; 1 - condition off');
call gfisp_addevt(2,407,'TRiM incorrectly encoded cards in cassette');
call gfisp_addevt(4,408,'TRiM cassette empty');
call gfisp_addevt(4,409,'TRiM cassette low');
call gfisp_addevt(4,410,'TRiM cassette filled');
call gfisp_addevt(2,411,'TRiM write timeout');
call gfisp_addevt(2,412,'TRiM restart');
call gfisp_addevt(6,413,'TRiM diagnostics');
call gfisp_addevt(2,414,'TRiM stripe missing',               'Cards not loaded properly');
call gfisp_addevt(2,415,'TRiM too many verify errors');
call gfisp_addevt(6,416,'TRiM test');
call gfisp_addevt(2,417,'TRiM issuance failure',             'Give an indication of a TRiM having a failure to issue on request, a final catch-all event beyond a specific condition (jam, message queue full, etc.)');
call gfisp_addevt(2,418,'TRiM smart card reader offline',    'n: 0 - condition on; 1 - condition off');
call gfisp_addevt(2,419,'TRiM type wrong',                   'n: 0 - condition on; 1 - condition off');
// BTP
call gfisp_addevt(2,420,'Bill tekpak disabled');
call gfisp_addevt(2,421,'Bill tekpak enabled');
call gfisp_addevt(2,422,'Bill tekpak offline');
call gfisp_addevt(2,423,'Bill tekpak online');
call gfisp_addevt(4,424,'Bill stacker removed');
call gfisp_addevt(4,425,'Bill stacker inserted');
call gfisp_addevt(2,426,'Bill tekpak obstruction',           'n: 0 - condition on; 1 - condition off');
call gfisp_addevt(4,427,'Bill stacker near full');
call gfisp_addevt(4,428,'Bill stacker full');
call gfisp_addevt(2,429,'Bill tekpak too many invalid bills');
call gfisp_addevt(2,430,'Bill tekpak swapped',               'Replace the current module with a different one');
call gfisp_addevt(4,431,'Bill stacker swapped',              'Replace the current module with a different one (removed->inserted->swapped)');
call gfisp_addevt(4,432,'Bill stacker leave out');
call gfisp_addevt(2,433,'Bill stacker missing',              'Removed during power off');
call gfisp_addevt(4,434,'Bill stacker cash limit reached');
call gfisp_addevt(2,435,'Bill stacker non-transaction bills','Maintenance mode');
call gfisp_addevt(4,436,'Bill stacker bill count',           'Total bills in bill stacker');
call gfisp_addevt(2,437,'Bill tekpak alarm');
call gfisp_addevt(2,438,'Bill tekpak sensor alarm');
call gfisp_addevt(2,439,'Bill stacker invalid remove',       'Non-revenue mode remove');
call gfisp_addevt(2,670,'Bill stacker invalid insert',       'Non-revenue mode insert');
call gfisp_addevt(4,671,'Bill stacker locked out',           'Same module inserted (removed->inserted->locked out)');
call gfisp_addevt(2,672,'Bill tekpak power loss');
call gfisp_addevt(2,673,'Bill tekpak escrow return');
call gfisp_addevt(2,674,'Bill tekpak communication error',   'Bill tekpak has entered a state where communication is limited/non-existent; it responds to only certain commands');
call gfisp_addevt(2,675,'Bill tekpak cash recovery audit');
// CTP
call gfisp_addevt(2,440,'Coin tekpak disabled');
call gfisp_addevt(2,441,'Coin tekpak enabled');
call gfisp_addevt(2,442,'Coin tekpak offline');
call gfisp_addevt(2,443,'Coin tekpak online');
call gfisp_addevt(4,444,'Coin tekpak removed');
call gfisp_addevt(4,445,'Coin tekpak inserted');
call gfisp_addevt(2,446,'Coin tekpak tube obstruction');
call gfisp_addevt(4,447,'Coin tekpak near full');
call gfisp_addevt(2,448,'Coin tekpak validator obstruction');
call gfisp_addevt(2,449,'Coin tekpak too many invalid coins');
call gfisp_addevt(2,450,'Coin tekpak swapped',                        'Replace the current module with a different one (removed->inserted->swapped)');
call gfisp_addevt(2,451,'Coin tekpak routing alarm',                  'Validated coins are routed wrong');
call gfisp_addevt(2,452,'Coin tekpak checksum alarm');
call gfisp_addevt(2,453,'Coin tekpak no credit alarm',                'Validated, but no credit');
call gfisp_addevt(2,454,'Coin tekpak coin stuck');
call gfisp_addevt(2,455,'Coin tekpak leave out');
call gfisp_addevt(4,456,'Coin tekpak tube low');
call gfisp_addevt(2,457,'Coin tekpak missing',                        'Removed during power off');
call gfisp_addevt(4,458,'Coin tekpak cash limit reached');
call gfisp_addevt(4,459,'Coin tekpak coin count',                     'Total coins in coin tekpak');
call gfisp_addevt(2,460,'Coin tekpak non-transaction coins inserted', 'Maintenance mode');
call gfisp_addevt(2,461,'Coin tekpak non-transaction coins dispensed','Maintenance mode');
call gfisp_addevt(2,462,'Coin tekpak coins not dispensed');
call gfisp_addevt(2,463,'Coin tekpak defective tube sensor alarm');
call gfisp_addevt(2,464,'Coin tekpak invalid remove',                 'Non-revenue mode remove');
call gfisp_addevt(2,465,'Coin tekpak invalid insert',                 'Non-revenue mode insert');
call gfisp_addevt(4,466,'Coin tekpak locked out',                     'Same module inserted (removed->inserted->locked out)');
call gfisp_addevt(2,467,'Coin tekpak throat obstruction',             'Coin stuck at mouth forcing solenoid to activate');
call gfisp_addevt(2,468,'Coin tekpak dispensing error',               'n: 0 - condition on; 1 - condition off');
call gfisp_addevt(2,469,'Coin tekpak cassette tubes missing');
call gfisp_addevt(4,699,'Coin tekpak tube empty');
call gfisp_addevt(2,710,'Coin tekpak escrow stuck open');
call gfisp_addevt(2,711,'Coin tekpak accept gate open');
call gfisp_addevt(2,712,'Coin tekpak accept gate error');
call gfisp_addevt(2,713,'Coin tekpak separator error');
call gfisp_addevt(2,714,'Coin tekpak discriminator error');
call gfisp_addevt(2,715,'Coin tekpak disconnected',                   'n: 0 - condition on; 1 - condition off');
call gfisp_addevt(2,716,'Coin tekpak count mismatch');
call gfisp_addevt(2,717,'Coin tekpak cash recovery audit');
call gfisp_addevt(2,718,'Coin tekpak cash replenish audit');
// Hopper
call gfisp_addevt(2,470,'Hopper disabled');
call gfisp_addevt(2,471,'Hopper enabled');
call gfisp_addevt(2,472,'Hopper offline');
call gfisp_addevt(2,473,'Hopper online');
call gfisp_addevt(4,474,'Hopper removed');
call gfisp_addevt(4,475,'Hopper inserted');
call gfisp_addevt(4,476,'Hopper swapped',                         'Replace the current module with a different one (removed->inserted->swapped)');
call gfisp_addevt(4,477,'Hopper low');
call gfisp_addevt(4,478,'Hopper empty');
call gfisp_addevt(2,479,'Hopper obstruction');
call gfisp_addevt(2,480,'Hopper leave out');
call gfisp_addevt(2,481,'Hopper missing',                        'Removed during power off');
call gfisp_addevt(4,482,'Hopper near empty');
call gfisp_addevt(4,483,'Hopper coin count',                     'Total coins in hopper');
call gfisp_addevt(2,484,'Hopper non-transaction coins dispensed','Maintenance mode');
call gfisp_addevt(2,485,'Hopper coin blocked in payout');
call gfisp_addevt(4,486,'Hopper invalid remove',                 'Non-revenue mode remove');
call gfisp_addevt(2,487,'Hopper invalid insert',                 'Non-revenue mode insert');
call gfisp_addevt(2,488,'Hopper unauthorized coins dispensed');   // jackpot
call gfisp_addevt(4,489,'Hopper locked out',                     'Same module inserted (removed->inserted->locked out)');
call gfisp_addevt(2,690,'Hopper disconnected');
call gfisp_addevt(2,720,'Hopper dispensing error');
call gfisp_addevt(2,721,'Hopper coin type mismatch',             'n: 0 - condition on; 1 - condition off');
// Printer
call gfisp_addevt(2,490,'Printer offline');
call gfisp_addevt(2,491,'Printer online');
call gfisp_addevt(2,492,'Printer paper out');
call gfisp_addevt(2,493,'Printer paper low');
call gfisp_addevt(2,494,'Printer paper replaced');
// Cashbox
call gfisp_addevt(2,500,'Cashbox disabled');
call gfisp_addevt(2,501,'Cashbox enabled');
call gfisp_addevt(2,502,'Cashbox offline');
call gfisp_addevt(2,503,'Cashbox online');
call gfisp_addevt(4,504,'Cashbox removed');
call gfisp_addevt(4,505,'Cashbox inserted');
call gfisp_addevt(4,506,'Cashbox near full');
call gfisp_addevt(4,507,'Cashbox full');
call gfisp_addevt(4,508,'Cashbox swapped',                       'Replace the current module with a different one (removed->inserted->swapped)');
call gfisp_addevt(2,509,'Cashbox leave out');
call gfisp_addevt(2,510,'Cashbox missing',                       'Removed during power off');
call gfisp_addevt(4,511,'Cashbox cash limit reached');
call gfisp_addevt(4,512,'Cashbox coin count',                    'Total coins in cashbox');
call gfisp_addevt(2,513,'Cashbox non-transaction coins inserted','Maintenance mode');
call gfisp_addevt(2,514,'Cashbox invalid remove',                'Non-revenue mode remove');
call gfisp_addevt(2,515,'Cashbox invalid insert',                'Non-revenue mode insert');
call gfisp_addevt(4,516,'Cashbox locked out',                    'Same module inserted (removed->inserted->locked out)');
call gfisp_addevt(2,517,'Cashbox cash recovery audit');
call gfisp_addevt(2,518,'Cashbox cash replenish audit');
// Magnetic card reader
call gfisp_addevt(2,520,'Card reader disabled');
call gfisp_addevt(2,521,'Card reader enabled');
call gfisp_addevt(2,524,'Card reader online');
call gfisp_addevt(2,525,'Card reader offline');
// Pinpad
call gfisp_addevt(2,522,'Pinpad disabled');
call gfisp_addevt(2,523,'Pinpad enabled');
call gfisp_addevt(2,526,'Pinpad online');
call gfisp_addevt(2,527,'Pinpad offline');
call gfisp_addevt(2,528,'Wrong teminal ID','Smartcard processing');
// Alarm module
call gfisp_addevt(3,540,'Alarm module offline');
call gfisp_addevt(3,541,'Alarm module online');
call gfisp_addevt(3,542,'Silent alarm');
call gfisp_addevt(3,543,'Vibration alarm on');
call gfisp_addevt(3,544,'Vibration alarm off');
call gfisp_addevt(3,545,'Tilt alarm on');
call gfisp_addevt(3,546,'Tilt alarm off');
call gfisp_addevt(3,547,'Alarm calibrated');
call gfisp_addevt(3,548,'Alarm board door sensor status','n: 0 - sensor missing; 1 - bad sensor; 2 - closed; 3 - open; 4 - misaligned');
call gfisp_addevt(3,549,'Alarm shunted');	-- GD-233
call gfisp_addevt(3,585,'Door opened');
call gfisp_addevt(3,586,'Door closed');
// UPS
call gfisp_addevt(2,560,'UPS offline');
call gfisp_addevt(2,561,'UPS online');
call gfisp_addevt(2,562,'UPS on battery power');
call gfisp_addevt(2,563,'UPS battery low');
call gfisp_addevt(2,564,'UPS on AC power');
call gfisp_addevt(2,565,'UPS shutdown requested');
call gfisp_addevt(6,566,'UPS blower on');
call gfisp_addevt(6,567,'UPS blower off');
call gfisp_addevt(6,568,'UPS heater on');
call gfisp_addevt(6,569,'UPS heater off');
call gfisp_addevt(2,570,'UPS high temperature warning','n=temperature in °C');
call gfisp_addevt(2,571,'UPS low temperature warning', 'n=temperature in °C');
call gfisp_addevt(2,572,'UPS temperature stabilized',  'n=temperature in °C');
call gfisp_addevt(2,573,'UPS temperature sensor communication error');
// Smart card reader
call gfisp_addevt(6,630,'Smartcard reader disabled');
call gfisp_addevt(6,631,'Smartcard reader enabled');
call gfisp_addevt(2,632,'Smartcard reader offline');
call gfisp_addevt(2,633,'Smartcard reader online');
// System
call gfisp_addevt(4,580,'Into revenue service');
call gfisp_addevt(2,581,'Into maintenance service');
call gfisp_addevt(2,582,'Back to service');
call gfisp_addevt(2,583,'Local out of service (OOS) by maintenance');
call gfisp_addevt(2,584,'Reset local out of service (OOS)');
call gfisp_addevt(3,587,'Siren on');
call gfisp_addevt(3,588,'Siren off');
call gfisp_addevt(2,589,'Out of service (OOS)');
call gfisp_addevt(3,590,'Configuration file missing');
call gfisp_addevt(2,591,'Temperature alarm',       'n=temperature in °C');
call gfisp_addevt(3,592,'Sign-in debounce');
call gfisp_addevt(3,593,'Unauthorized entry',      'Invalid sign-in');
call gfisp_addevt(3,594,'Authorized entry',        'Valid pin number');
call gfisp_addevt(2,595,'Out of service (OOS) condition cleared');
call gfisp_addevt(2,596,'Terminal ID changed',     'Smartcard processing');
call gfisp_addevt(2,597,'Equipment number changed');
if exists(select 0 from gfi_lst where type='EV TYPE' and code=598 and name='Equipment shutdown requested') then
   delete from gfi_lst where type='EV TYPE' and code=598 and name='Equipment shutdown requested';
   commit;
end if;
call gfisp_addevt(3,598,'Equipment shutdown',      'n: 0 - no reason; 1 - local; 2 - remote; 3 - power loss; 4 - temperature');
call gfisp_addevt(2,599,'Equipment too many restarts');
call gfisp_addevt(2,600,'Equipment abrupt restart','Unclean shutdown');
call gfisp_addevt(2,601,'Equipment recovery from abrupt restart');
call gfisp_addevt(2,602,'Temperature',             'n=temperature in °C');
call gfisp_addevt(3,603,'Factory authorized entry','Into factory mode');
call gfisp_addevt(6,604,'Cancel event',            'Push Cancel button on equipment');
call gfisp_addevt(3,605,'IP address changed');
call gfisp_addevt(3,606,'Fare structure changed');
call gfisp_addevt(3,607,'Configuration changed');
call gfisp_addevt(3,608,'Sound file changed');

call gfisp_addevt(2,610,'Button stuck');
call gfisp_addevt(2,611,'Button freed');

call gfisp_addevt(2,650,'BIOS updated');
call gfisp_addevt(2,651,'BIOS update failed or too many BIOS updates');

call gfisp_addevt(2,691,'TVM offline');
call gfisp_addevt(2,692,'TVM online');
call gfisp_addevt(2,693,'Door board offline');
call gfisp_addevt(2,694,'Door board online');
call gfisp_addevt(2,695,'Media board offline');
call gfisp_addevt(2,696,'Media board online');
call gfisp_addevt(2,697,'Power distribution board offline');
call gfisp_addevt(2,698,'Power distribution board online');
call gfisp_addevt(4,700,'No change given mode');
call gfisp_addevt(4,701,'No bill accepted mode',        'n: 0 - no bill accepted; 1 - bill accepted');
call gfisp_addevt(4,702,'No coin accepted mode',        'n: 0 - no coin accepted; 1 - coin accepted');
call gfisp_addevt(2,703,'Coin shutter stuck',           'n: 0 - coin shutter stuck; 1 - condition cleared');
call gfisp_addevt(2,704,'Door shutter status',          'n: 0 - sensor missing; 1 - closed; 2 - open; 3 - partially open');
call gfisp_addevt(2,730,'Ticket detection problem at ticket tray','n>0 - corresponding transaction ID');

call gfisp_addevt(2,731,'Issue of tickets was successful');		-- LG-285
call gfisp_addevt(2,732,'Issue of tickets was failed');			-- LG-285

call gfisp_addevt(2,735,'Ticket tray sensor malfunction','n: 0 - condition on; 1 - condition off');
if exists(select 0 from gfi_lst where type='EV TYPE' and code=740 and name='Door sensor status') then
   delete from gfi_lst where type='EV TYPE' and code=740 and name='Door sensor status';
   commit;
end if;
call gfisp_addevt(2,740,'Door board door sensor status','n: 0 - sensor missing; 1 - bad sensor; 2 - closed; 3 - open; 4 - misaligned');
// Server generated events
call gfisp_addevt(6,900,'Poll equipment');
call gfisp_addevt(6,901,'Put equipment in service');
call gfisp_addevt(6,902,'Put equipment out of service (OOS)');
call gfisp_addevt(6,903,'Shutdown/reboot equipment');

call gfisp_addevt(5,951,'TVM not communicating',   'Server has not heard from TVM for a predefined period of time');
call gfisp_addevt(5,952,'TVM disk space',          'n: 0 - normal; 1 - near full; 2 - full');

call gfisp_addevt(3,993,'Acknowledge security alert');
call gfisp_addevt(6,994,'Fare structure download');
call gfisp_addevt(6,995,'Configuration download');
call gfisp_addevt(6,996,'Sound file download');
call gfisp_addevt(6,997,'All configuration download');

////////////////////////////////////////////////////////////////////////
// List: FARE TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('FARE TYPE','Regular', 0,1,'Full fare');
call gfisp_additem('FARE TYPE','Senior',  0,2,'Discount fare group 1');
call gfisp_additem('FARE TYPE','Youth',   0,3,'Discount fare group 2');
call gfisp_additem('FARE TYPE','Disabled',0,4,'Discount fare group 3');

////////////////////////////////////////////////////////////////////////
// List: FIS PAY TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('FIS PAY TYPE','|VI',0,1,'Visa');
call gfisp_additem('FIS PAY TYPE','|MC',0,2,'Master Card');
call gfisp_additem('FIS PAY TYPE','|AM',0,3,'American Express');
call gfisp_additem('FIS PAY TYPE','|DI',0,4,'Discover');
call gfisp_additem('FIS PAY TYPE','|DB',0,5,'Debit Card');
call gfisp_additem('FIS PAY TYPE','|PL',0,6,'Pinless Debit Card');
call gfisp_additem('FIS PAY TYPE','|EC',0,7,'eCheck');

////////////////////////////////////////////////////////////////////////
// List: INSP CODE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('INSP CODE','Undefined',     0,0,'Undefined inspection code');
call gfisp_additem('INSP CODE','No ticket',     0,1,'Did not have ticket or card in possession at time of inspection');
call gfisp_additem('INSP CODE','Expired ticket',0,2,'Had expired ticket or card');
call gfisp_additem('INSP CODE','Wrong ticket',  0,3,'Had wrong category of ticket or card');
call gfisp_additem('INSP CODE','Altered ticket',0,4,'Had altered ticket or card');

////////////////////////////////////////////////////////////////////////
// List: INST TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('INST TYPE','Undefined',   0,0,'Undefined institution type');
call gfisp_additem('INST TYPE','Government',  0,1);
call gfisp_additem('INST TYPE','Education',   0,2);
call gfisp_additem('INST TYPE','Non-profit',  0,3);
call gfisp_additem('INST TYPE','Corporation', 0,4,'Get-On-Board');
call gfisp_delitem('INST TYPE','Sales Outlet',0,5);
call gfisp_additem('INST TYPE','Sales Outlet',1,5);

////////////////////////////////////////////////////////////////////////
// List: ISSU CODE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('ISSU CODE','Undefined',0,0,'Undefined infraction inspection issuance code');
call gfisp_additem('ISSU CODE','|A',       0,1,'First time offender - written warning');
call gfisp_additem('ISSU CODE','|B',       0,2,'Second time offender - pay $10 fine');
call gfisp_additem('ISSU CODE','|C',       0,3,'Repeat offender - pay $50 fine');

////////////////////////////////////////////////////////////////////////
// List: MNT CODE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('MNT CODE','Undefined',0,0,'Undefined maintenance code');
call gfisp_additem('MNT CODE','Unjam',    0,1,'Unjam');
call gfisp_additem('MNT CODE','Adjust',   0,2,'Adjust');
call gfisp_additem('MNT CODE','Replace',  0,3,'Replace');
call gfisp_additem('MNT CODE','Clean',    0,4,'Clean');
call gfisp_additem('MNT CODE','Inspect',  0,5,'Inspect');
call gfisp_additem('MNT CODE','Other',    0,6,'Other');

////////////////////////////////////////////////////////////////////////
// List: MOD STATUS
////////////////////////////////////////////////////////////////////////
call gfisp_additem('MOD STATUS','In service',        0,0,'In service');
call gfisp_additem('MOD STATUS','Counting room',     0,1,'Counting room');
call gfisp_additem('MOD STATUS','Inventory',         0,2,'Inventory');
call gfisp_additem('MOD STATUS','Repair',            0,3,'Repair');
call gfisp_additem('MOD STATUS','Out of circulation',0,4,'Out of circulation');

////////////////////////////////////////////////////////////////////////
// List: MOD TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('MOD TYPE','Undefined',        0,0,'Undefined module type');
call gfisp_additem('MOD TYPE','Hopper',           0,1,'Hopper');
call gfisp_additem('MOD TYPE','TRiM',             0,2,'TRiM');
call gfisp_additem('MOD TYPE','Cashbox',          0,3,'Cashbox');
call gfisp_additem('MOD TYPE','Bill Stacker',     0,4,'Bill Stacker');
call gfisp_additem('MOD TYPE','Coin Tekpak',      0,5,'Coin Tekpak (CTP)');
call gfisp_additem('MOD TYPE','Bill Tekpak',      0,6,'Bill Tekpak (BTP)');

call gfisp_additem('MOD TYPE','BTP Board',        0,7,'Bill Tekpak Interface Board');
call gfisp_additem('MOD TYPE','CTP Board',        0,8,'Coin Tekpak Interface Board');
call gfisp_additem('MOD TYPE','TRiM Board',       0,9,'TRiM Interface Board');
call gfisp_additem('MOD TYPE','Main Controller',  0,10,'Main Controller');
call gfisp_additem('MOD TYPE','Alarm',            0,11,'Alarm');
call gfisp_additem('MOD TYPE','UPS',              0,12,'UPS');
call gfisp_additem('MOD TYPE','AC Controller',    0,13,'AC Controller');
call gfisp_additem('MOD TYPE','Door Board',       0,14,'Door Board');
call gfisp_additem('MOD TYPE','Media Board',      0,15,'Media Board');
call gfisp_additem('MOD TYPE','Power Dist. Board',0,16,'Power Distribution Board');
call gfisp_additem('MOD TYPE','Printer',          0,17,'Printer');
call gfisp_additem('MOD TYPE','Smartcard',        0,18,'Smartcard');
call gfisp_additem('MOD TYPE','Pinpad',           0,19,'Pinpad');
call gfisp_additem('MOD TYPE','Temperature Board',0,20,'Temperature/Light Sensor Board');

////////////////////////////////////////////////////////////////////////
// List: NOTIFY TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('NOTIFY TYPE','Undefined',0,0,'Undefined notification type');
call gfisp_additem('NOTIFY TYPE','|Email',    0,1,'Email');
call gfisp_additem('NOTIFY TYPE','|Print',    0,2,'Print');
call gfisp_additem('NOTIFY TYPE','|Popup',    0,3,'Popup');
call gfisp_additem('NOTIFY TYPE','|Log',      0,4,'Log');

////////////////////////////////////////////////////////////////////////
// List: PASS CLASS
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PASS CLASS','Undefined',0,0,'Undefined passenger type');
call gfisp_additem('PASS CLASS','Adult',    0,1,'Adult');
call gfisp_additem('PASS CLASS','Senior',   0,2,'Senior');
call gfisp_additem('PASS CLASS','Youth',    0,3,'Youth');
call gfisp_additem('PASS CLASS','Disabled', 0,4,'Disabled');

////////////////////////////////////////////////////////////////////////
// List: PASS TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PASS TYPE','Undefined',0,0,'Undefined pass type');
call gfisp_additem('PASS TYPE','1R',       0,1,'Single Ride Pass');
call gfisp_additem('PASS TYPE','1D',       0,2,'1 Day Pass');
call gfisp_additem('PASS TYPE','3D',       0,3,'3 Day Pass');
call gfisp_additem('PASS TYPE','5D',       0,4,'5 Day Pass');
call gfisp_additem('PASS TYPE','7D',       0,5,'7 Day Pass');
call gfisp_additem('PASS TYPE','30D',      0,6,'30 Day Pass');
call gfisp_additem('PASS TYPE','10R',      0,7,'10 Ride Pass');

////////////////////////////////////////////////////////////////////////
// List: PAY TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PAY TYPE','Undefined',0,0,'Undefined payment type');
call gfisp_additem('PAY TYPE','Cash',     0,1,'Cash');
call gfisp_additem('PAY TYPE','Credit',   0,2,'Credit Card');
call gfisp_additem('PAY TYPE','Debit',    0,3,'Debit Card');
call gfisp_additem('PAY TYPE','Check',    0,4,'Check');
call gfisp_additem('PAY TYPE','Account',  0,5,'Account');
call gfisp_additem('PAY TYPE','GFI card', 0,6,'GFI issued card');
call gfisp_additem('PAY TYPE','Voucher',  0,7,'Voucher');		-- LG-270


////////////////////////////////////////////////////////////////////////
// List: PDP PARM and PDP PARM HELP
////////////////////////////////////////////////////////////////////////
select list(loc_n) into ls from cnf;
while length(ls)>0 loop                           // loop through all locations
   set li=locate(ls,',');
   if li>0 then
      set li_loc=cast(substr(ls,1,li - 1) as smallint);
      set ls=substr(ls,li+1);
   else
      set li_loc=cast(ls as smallint);
      set ls='';
   end if;

   call gfisp_additem('PDP PARM','|Probe Limit',   li_loc,null,'0');
   call gfisp_additem('PDP PARM','|Probe Exp Date',li_loc,null,'12312100');
   call gfisp_additem('PDP PARM','|Probe Exp Time',li_loc,null,'2359');
end loop;

call gfisp_additem('PDP PARM HELP','|Probe Limit',   0,null,'|N0;Maximum number of probings allowed per session; use 0 to disable probing');
call gfisp_additem('PDP PARM HELP','|Probe Exp Date',0,null,'|N1012010-12312100;Probe session expiration date (mmddyyyy)');
call gfisp_additem('PDP PARM HELP','|Probe Exp Time',0,null,'|N0-2359;Probe session expiration time (hhmm)');

delete from gfi_lst where type='PDP PARM' and class not in (select loc_n from cnf);
commit;

////////////////////////////////////////////////////////////////////////
// List: PROD TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PROD TYPE','Period',  0,1);
call gfisp_additem('PROD TYPE','Ride',    0,2);
call gfisp_additem('PROD TYPE','Value',   0,3);
call gfisp_additem('PROD TYPE','Purse',   0,4);
call gfisp_additem('PROD TYPE','Employee',0,7);

////////////////////////////////////////////////////////////////////////
// List: PWD QUESTION
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PWD QUESTION','What street did you live on in third grade?',                                   0, 1);
call gfisp_additem('PWD QUESTION','What is your oldest sibling''s birthday month and year? (e.g., January 1900)',  0, 2);
call gfisp_additem('PWD QUESTION','What is the middle name of your youngest child?',                               0, 3);
call gfisp_additem('PWD QUESTION','What was your childhood nickname?',                                             0, 4);
call gfisp_additem('PWD QUESTION','In what city did you meet your spouse/significant other?',                      0, 5);
call gfisp_additem('PWD QUESTION','What is the name of your favorite childhood friend?',                           0, 6);
call gfisp_additem('PWD QUESTION','What is your oldest sibling''s middle name?',                                   0, 7);
call gfisp_additem('PWD QUESTION','What school did you attend for sixth grade?',                                   0, 8);
call gfisp_additem('PWD QUESTION','What was your childhood phone number including area code? (e.g., 000-000-0000)',0, 9);
//call gfisp_additem('PWD QUESTION','What is your oldest cousin''s first and last name?',                            0,10);
call gfisp_additem('PWD QUESTION','What was the name of your first stuffed animal?',                               0,11);
call gfisp_additem('PWD QUESTION','In what city or town did your mother and father meet?',                         0,12);
call gfisp_additem('PWD QUESTION','Where were you when you had your first kiss?',                                  0,13);
call gfisp_additem('PWD QUESTION','What is the first name of the boy or girl that you first kissed?',              0,14);
call gfisp_additem('PWD QUESTION','What was the last name of your third grade teacher?',                           0,15);
call gfisp_additem('PWD QUESTION','In what city does your nearest sibling live?',                                  0,16);
call gfisp_additem('PWD QUESTION','What is your youngest brother''s birthday month and year? (e.g., January 1900)',0,17);
call gfisp_additem('PWD QUESTION','What is your maternal grandmother''s maiden name?',                             0,18);
call gfisp_additem('PWD QUESTION','In what city or town was your first job?',                                      0,19);
call gfisp_additem('PWD QUESTION','What is the name of the place your wedding reception was held?',                0,20);
call gfisp_additem('PWD QUESTION','What is the name of a college you applied to but didn''t attend?',              0,21);
call gfisp_additem('PWD QUESTION','Where were you when you first heard about 9/11?',                               0,22);
call gfisp_additem('PWD QUESTION','What is the name of the company of your first job?',                            0,23);

call gfisp_delitem('PWD QUESTION','What is your oldest cousin''s first and last name?');

////////////////////////////////////////////////////////////////////////
// List: SHIPPER
////////////////////////////////////////////////////////////////////////
call gfisp_additem('SHIPPER','N/A',  0,0,'Undefined shipper');
call gfisp_additem('SHIPPER','USPS', 0,1,'United States Postal Service');
call gfisp_additem('SHIPPER','UPS',  0,2,'United Parcel Service');
call gfisp_additem('SHIPPER','FedEx',0,3,'Federal Express');
call gfisp_additem('SHIPPER','DHL',  0,4,'DHL Express');

////////////////////////////////////////////////////////////////////////
// List: STATE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('STATE','AL',1,  1,'Alabama');
call gfisp_additem('STATE','AK',1,  2,'Alaska');
call gfisp_additem('STATE','AZ',1,  3,'Arizona');
call gfisp_additem('STATE','AR',1,  4,'Arkansas');
call gfisp_additem('STATE','CA',1,  5,'California');
call gfisp_additem('STATE','CO',1,  6,'Colorado');
call gfisp_additem('STATE','CT',1,  7,'Connecticut');
call gfisp_additem('STATE','DE',1,  8,'Delaware');
call gfisp_additem('STATE','FL',1,  9,'Florida');
call gfisp_additem('STATE','GA',1, 10,'Georgia');
call gfisp_additem('STATE','HI',1, 11,'Hawaii');
call gfisp_additem('STATE','ID',1, 12,'Idaho');
call gfisp_additem('STATE','IL',1, 13,'Illinois');
call gfisp_additem('STATE','IN',1, 14,'Indiana');
call gfisp_additem('STATE','IA',1, 15,'Iowa');
call gfisp_additem('STATE','KS',1, 16,'Kansas');
call gfisp_additem('STATE','KY',1, 17,'Kentucky');
call gfisp_additem('STATE','LA',1, 18,'Louisiana');
call gfisp_additem('STATE','ME',1, 19,'Maine');
call gfisp_additem('STATE','MD',1, 20,'Maryland');
call gfisp_additem('STATE','MA',1, 21,'Massachusetts');
call gfisp_additem('STATE','MI',1, 22,'Michigan');
call gfisp_additem('STATE','MN',1, 23,'Minnesota');
call gfisp_additem('STATE','MS',1, 24,'Mississippi');
call gfisp_additem('STATE','MO',1, 25,'Missouri');
call gfisp_additem('STATE','MT',1, 26,'Montana');
call gfisp_additem('STATE','NE',1, 27,'Nebraska');
call gfisp_additem('STATE','NV',1, 28,'Nevada');
call gfisp_additem('STATE','NH',1, 29,'New Hampshire');
call gfisp_additem('STATE','NJ',1, 30,'New Jersey');
call gfisp_additem('STATE','NM',1, 31,'New Mexico');
call gfisp_additem('STATE','NY',1, 32,'New York');
call gfisp_additem('STATE','NC',1, 33,'North Carolina');
call gfisp_additem('STATE','ND',1, 34,'North Dakota');
call gfisp_additem('STATE','OH',1, 35,'Ohio');
call gfisp_additem('STATE','OK',1, 36,'Oklahoma');
call gfisp_additem('STATE','OR',1, 37,'Oregon');
call gfisp_additem('STATE','PA',1, 38,'Pennsylvania');
call gfisp_additem('STATE','RI',1, 39,'Rhode Island');
call gfisp_additem('STATE','SC',1, 40,'South Carolina');
call gfisp_additem('STATE','SD',1, 41,'South Dakota');
call gfisp_additem('STATE','TN',1, 42,'Tennessee');
call gfisp_additem('STATE','TX',1, 43,'Texas');
call gfisp_additem('STATE','UT',1, 44,'Utah');
call gfisp_additem('STATE','VT',1, 45,'Vermont');
call gfisp_additem('STATE','VA',1, 46,'Virginia');
call gfisp_additem('STATE','WA',1, 47,'Washington');
call gfisp_additem('STATE','WV',1, 48,'West Virginia');
call gfisp_additem('STATE','WI',1, 49,'Wisconsin');
call gfisp_additem('STATE','WY',1, 50,'Wyoming');

call gfisp_additem('STATE','AS',1, 51,'American Samoa');
call gfisp_additem('STATE','DC',1, 52,'District of Columbia');
call gfisp_additem('STATE','GU',1, 53,'Guam');
call gfisp_additem('STATE','MH',1, 54,'Marshall Islands');
call gfisp_additem('STATE','FM',1, 55,'Micronesia');
call gfisp_additem('STATE','MP',1, 56,'Northern Marianas');
call gfisp_additem('STATE','PW',1, 57,'Palau');
call gfisp_additem('STATE','PR',1, 58,'Puerto Rico');
call gfisp_additem('STATE','VI',1, 59,'Virgin Islands');

call gfisp_additem('STATE','AB',2, 61,'Alberta');
call gfisp_additem('STATE','BC',2, 62,'British Columbia');
call gfisp_additem('STATE','MB',2, 63,'Manitoba');
call gfisp_additem('STATE','NB',2, 64,'New Brunswick');
call gfisp_additem('STATE','NL',2, 65,'Newfoundland and Labrador');
call gfisp_additem('STATE','NT',2, 66,'Northwest Territories');
call gfisp_additem('STATE','NS',2, 67,'Nova Scotia');
call gfisp_additem('STATE','NU',2, 68,'Nunavut');
call gfisp_additem('STATE','ON',2, 69,'Ontario');
call gfisp_additem('STATE','PE',2, 70,'Prince Edward Island');
call gfisp_additem('STATE','QC',2, 71,'Quebec');
call gfisp_additem('STATE','SK',2, 72,'Saskatchewan');
call gfisp_additem('STATE','YT',2, 73,'Yukon');

call gfisp_additem('STATE','AG',3, 81,'Aguascalientes');
call gfisp_additem('STATE','BN',3, 82,'Baja California');
call gfisp_additem('STATE','BS',3, 83,'Baja California Sur');
call gfisp_additem('STATE','CM',3, 84,'Campeche');
call gfisp_additem('STATE','CP',3, 85,'Chiapas');
call gfisp_additem('STATE','CH',3, 86,'Chihuahua');
call gfisp_additem('STATE','CA',3, 87,'Coahuila');
call gfisp_additem('STATE','CL',3, 88,'Colima');
call gfisp_additem('STATE','DF',3, 89,'Distrito Federal');
call gfisp_additem('STATE','DU',3, 90,'Durango');
call gfisp_additem('STATE','GJ',3, 91,'Guanajuato');
call gfisp_additem('STATE','GR',3, 92,'Guerrero');
call gfisp_additem('STATE','HI',3, 93,'Hidalgo');
call gfisp_additem('STATE','JA',3, 94,'Jalisco');
call gfisp_additem('STATE','MX',3, 95,'México');
call gfisp_additem('STATE','MC',3, 96,'Michoacán');
call gfisp_additem('STATE','MR',3, 97,'Morelos');
call gfisp_additem('STATE','NA',3, 98,'Nayarit');
call gfisp_additem('STATE','NL',3, 99,'Nuevo León');
call gfisp_additem('STATE','OA',3,100,'Oaxaca');
call gfisp_additem('STATE','PU',3,101,'Puebla');
call gfisp_additem('STATE','QE',3,102,'Querétaro');
call gfisp_additem('STATE','QR',3,103,'Quintana Roo');
call gfisp_additem('STATE','SL',3,104,'San Luis Potosí');
call gfisp_additem('STATE','SI',3,105,'Sinaloa');
call gfisp_additem('STATE','SO',3,106,'Sonora');
call gfisp_additem('STATE','TB',3,107,'Tabasco');
call gfisp_additem('STATE','TM',3,108,'Tamaulipas');
call gfisp_additem('STATE','TL',3,109,'Tlaxcala');
call gfisp_additem('STATE','VE',3,110,'Veracruz');
call gfisp_additem('STATE','YU',3,111,'Yucatán');
call gfisp_additem('STATE','ZA',3,112,'Zacatecas');

////////////////////////////////////////////////////////////////////////
// List: STOCK TYPE
////////////////////////////////////////////////////////////////////////
call gfisp_additem('STOCK TYPE','Unknown type',0, 0,'Unknown stock type');
call gfisp_additem('STOCK TYPE','Nickel',      0, 1);
call gfisp_additem('STOCK TYPE','Dime',        0, 2);
call gfisp_additem('STOCK TYPE','Quarter',     0, 3);
call gfisp_additem('STOCK TYPE','SBA',         0, 4);
call gfisp_additem('STOCK TYPE','Token 1',     0, 5);
call gfisp_additem('STOCK TYPE','Token 2',     0, 6);
call gfisp_additem('STOCK TYPE','Spare coin',  0, 7);
call gfisp_additem('STOCK TYPE','Ride Pass',   0, 8,'TRiM ride pass stock');
call gfisp_additem('STOCK TYPE','Day Pass',    0, 9,'TRiM day pass stock');
call gfisp_additem('STOCK TYPE','30-Day Pass', 0,10,'TRiM 30-day pass stock');

////////////////////////////////////////////////////////////////////////
// List: SUM LABEL
////////////////////////////////////////////////////////////////////////
call gfisp_additem('SUM LABEL','TVM',  1, 1,'Number of TVMs');
call gfisp_additem('SUM LABEL','REV',  1, 2,'Revenue');
call gfisp_additem('SUM LABEL','ISS',  1, 3,'Cards issued');                // transaction type 302
call gfisp_additem('SUM LABEL','REC',  1, 4,'Cards recharged');             // transaction types 303 & 312
call gfisp_additem('SUM LABEL','VAL',  1, 5,'Cards validated');             // transaction type 316
call gfisp_additem('SUM LABEL','CUS',  1, 6,'Customers served');            // transaction types 302-305, 307, 308, 311, 312, 314, 316, 317, 319, 320, excluding child transactions (tr_seq>0)

call gfisp_additem('SUM LABEL','RVS',  1,21,'Need revenue service');        // srv_flags.S_SRV_Rev
call gfisp_additem('SUM LABEL','INRVS',1,22,'In revenue service');          // srv_flags.S_SRV_In_Rev
call gfisp_additem('SUM LABEL','MNT',  1,23,'Need maintenance');            // srv_flags.S_SRV_Mnt
call gfisp_additem('SUM LABEL','INMNT',1,24,'In maintenance');              // srv_flags.S_SRV_In_Mnt
call gfisp_additem('SUM LABEL','INSVC',1,25,'In service');                  // srv_flags.S_SRV_In_Svc
call gfisp_additem('SUM LABEL','OOS',  1,26,'Out of service');              // flags.S_LOCALOOS or flags.S_REMOTEOOS
call gfisp_additem('SUM LABEL','SEC',  1,27,'Security alert');              // srv_flags.S_SRV_Security
call gfisp_additem('SUM LABEL','DIS',  1,28,'Disconnected');                // srv_flags.S_SRV_In_Rev/S_SRV_In_Mnt/S_SRV_In_Svc are all off

call gfisp_additem('SUM LABEL','SEQ',  1,41,'System');                      // gfi_eq.status
call gfisp_additem('SUM LABEL','STRM', 1,42,'     TRiM');                   // gfi_eq.status_trim
call gfisp_additem('SUM LABEL','SHPR', 1,43,'     Hopper');                 // gfi_eq.status_hpr
call gfisp_additem('SUM LABEL','SBTP', 1,44,'     Bill tekpak');            // gfi_eq.status_btp
call gfisp_additem('SUM LABEL','SBST', 1,45,'     Bill stacker');           // gfi_eq.status_bst
call gfisp_additem('SUM LABEL','SCTP', 1,46,'     Coin tekpak');            // gfi_eq.status_ctp
call gfisp_additem('SUM LABEL','SCBX', 1,47,'     Cashbox');                // gfi_eq.status_cbx
call gfisp_additem('SUM LABEL','SCRD', 1,48,'     Card reader');            // gfi_eq.status_crd
call gfisp_additem('SUM LABEL','SPIN', 1,49,'     Pinpad');                 // gfi_eq.status_pin
call gfisp_additem('SUM LABEL','SSMC', 1,50,'     Smartcard');              // gfi_eq.status_smc
call gfisp_additem('SUM LABEL','SPRN', 1,51,'     Printer');                // gfi_eq.status_prn
call gfisp_additem('SUM LABEL','SUPS', 1,52,'     UPS');                    // gfi_eq.status_ups
call gfisp_additem('SUM LABEL','SCPU', 1,53,'     Computer');               // gfi_eq.status_cpu

call gfisp_additem('SUM LABEL','LUPT', 1,61,'Last updated');                // gfi_eq.last_update_ts
call gfisp_additem('SUM LABEL','LPOT', 1,62,'Last polled');                 // gfi_eq.last_poll_ts
call gfisp_additem('SUM LABEL','LPOU', 1,63,'Last polled by');              // gfi_eq.last_poll_uid

call gfisp_additem('SUM LABEL','PEM',  2, 1,'Number of PEMs');
call gfisp_additem('SUM LABEL','REV',  2, 2,'Revenue');
call gfisp_additem('SUM LABEL','ISS',  2, 3,'Cards issued');                // transaction type 302
call gfisp_additem('SUM LABEL','REC',  2, 4,'Cards recharged');             // transaction types 303 & 312
call gfisp_additem('SUM LABEL','VAL',  2, 5,'Cards validated');             // transaction type 316
call gfisp_additem('SUM LABEL','CUS',  2, 6,'Customers served');            // transaction types 302-305, 307, 308, 311, 312, 314, 316, 317, 319, 320, excluding child transactions (tr_seq>0)

call gfisp_additem('SUM LABEL','RVS',  2,21,'Need revenue service');        // srv_flags.S_SRV_Rev
call gfisp_additem('SUM LABEL','INRVS',2,22,'In revenue service');          // srv_flags.S_SRV_In_Rev
call gfisp_additem('SUM LABEL','MNT',  2,23,'Need maintenance');            // srv_flags.S_SRV_Mnt
call gfisp_additem('SUM LABEL','INMNT',2,24,'In maintenance');              // srv_flags.S_SRV_In_Mnt
call gfisp_additem('SUM LABEL','INSVC',2,25,'In service');                  // srv_flags.S_SRV_In_Svc
call gfisp_additem('SUM LABEL','OOS',  2,26,'Out of service');              // flags.S_LOCALOOS or flags.S_REMOTEOOS
call gfisp_additem('SUM LABEL','SEC',  2,27,'Security alert');              // srv_flags.S_SRV_Security
call gfisp_additem('SUM LABEL','DIS',  2,28,'Disconnected');                // srv_flags.S_SRV_In_Rev/S_SRV_In_Mnt/S_SRV_In_Svc are all off

call gfisp_delitem('SUM LABEL',null,0);                                     // remove old SUM LABEL items with class=0

////////////////////////////////////////////////////////////////////////
// List: SYS PARM
////////////////////////////////////////////////////////////////////////
call gfisp_additem('SYS PARM','|EQ DISK SPACE WARN 1',0,null,'90%');
call gfisp_additem('SYS PARM','|EQ DISK SPACE WARN 2',0,null,'95%');
call gfisp_additem('SYS PARM','|MAGNETIC FORMAT',     0,null,'0');
call gfisp_additem('SYS PARM','|RBBL',                0,null,'No');
call gfisp_additem('SYS PARM','|FBX FIRMWARE NEW',    0,null,'Yes');
call gfisp_additem('SYS PARM','|SOTD',                0,null,'00:00:00');
call gfisp_additem('SYS PARM','|EQ STATUS PRIORITY',  0,null,'4');
call gfisp_additem('SYS PARM','|DEBUG SP',            0,null,'No');
call gfisp_additem('SYS PARM','|DEBUG TRG',           0,null,'No');

////////////////////////////////////////////////////////////////////////
// List: SYS PARM HELP
////////////////////////////////////////////////////////////////////////
call gfisp_additem('SYS PARM HELP','|EQ DISK SPACE WARN 1',0,null,'|;TVM/PEM disk space usage percentage warning threshold 1 (n% full)');
call gfisp_additem('SYS PARM HELP','|EQ DISK SPACE WARN 2',0,null,'|;TVM/PEM disk space usage percentage warning threshold 2 (n% full)');
call gfisp_additem('SYS PARM HELP','|MAGNETIC FORMAT',     0,null,'|N0-1;Magnetic format version to use in generating bad list binary file (0 - original format; 1 - new format)');
call gfisp_additem('SYS PARM HELP','|RBBL',                0,null,'|BY;Real Big Bad List (maximum bad list records allowed): Yes - 10,000 records; No - 2,500 records');
call gfisp_additem('SYS PARM HELP','|FBX FIRMWARE NEW',    0,null,'|BY;New farebox firmware version: Yes - >=350 for CaB or >400 for Odyssey; No - legacy version (this setting has an impact on bad list processing)');
call gfisp_additem('SYS PARM HELP','|SOTD',                0,null,'|T;Start time of a transit date');
call gfisp_additem('SYS PARM HELP','|EQ STATUS PRIORITY',  0,null,'|N3-4;Top priority TVM/PEM status (3 - security alert; 4 - disconnected)');
call gfisp_additem('SYS PARM HELP','|DEBUG SP',            0,null,'|BY;Database stored procedure debug flag: Yes - debug mode on; No - debug mode off');
call gfisp_additem('SYS PARM HELP','|DEBUG TRG',           0,null,'|BY;Database trigger debug flag: Yes - debug mode on; No - debug mode off');

////////////////////////////////////////////////////////////////////////
// Populate TVM/PEM list table gfi_eq
////////////////////////////////////////////////////////////////////////
if not exists(select 0 from gfi_eq) then          // if no TVM/PEN was found, add a default one
   insert into gfi_eq (loc_n,eq_type,eq_n,eq_label,eq_desc) values(0,1,1,'TVM 1','TVM 1, not stationed');
   insert into gfi_eq (loc_n,eq_type,eq_n,eq_label,eq_desc) values(0,2,1,'PEM 1','PEM 1, not stationed');
   commit;
end if;

////////////////////////////////////////////////////////////////////////
// List: TVM PARM
////////////////////////////////////////////////////////////////////////
select list(eq_n)||',' into ls_eq from gfi_eq where eq_type=1 and eq_n>0;
while ls_eq<>'' loop
   set li=locate(ls_eq,',');
   set li_loc=cast(left(ls_eq,li - 1) as smallint);
   set ls_eq=substr(ls_eq,li+1);

   call gfisp_additem('TVM PARM','|IP@',                     li_loc);
   call gfisp_additem('TVM PARM','|MAC@',                    li_loc);

   select min(name) into ls from gfi_lst loc, gfi_eq eq where type='EQ LOC' and eq_type=1 and eq_n=li_loc and loc_n=code;
   call gfisp_additem('TVM PARM','|STATION NAME',            li_loc,null,ls);

   call gfisp_additem('TVM PARM','|STATION NAME SHORT',      li_loc);
   call gfisp_additem('TVM PARM','|ADDR1',                   li_loc);
   call gfisp_additem('TVM PARM','|ADDR2',                   li_loc);

   call gfisp_additem('TVM PARM','|L2G URL MODE',            li_loc,null,'TEST');
   call gfisp_additem('TVM PARM','|L2G MERCHANT CODE',       li_loc);
   call gfisp_additem('TVM PARM','|L2G SETTLE MERCHANT CODE',li_loc);
   call gfisp_additem('TVM PARM','|L2G PWD',                 li_loc);
   call gfisp_additem('TVM PARM','|ISP PHONE',               li_loc);
   call gfisp_additem('TVM PARM','|ISP UID',                 li_loc);
   call gfisp_additem('TVM PARM','|ISP PWD',                 li_loc);
   call gfisp_additem('TVM PARM','|DIALUP PHONE',            li_loc);
   call gfisp_additem('TVM PARM','|DIALUP UID',              li_loc,null,'VIP');
   call gfisp_additem('TVM PARM','|DIALUP PWD',              li_loc);
end loop;

delete from gfi_lst where type='TVM PARM' and class not in (select eq_n from gfi_eq where eq_type=1 and eq_n>0);

commit;

////////////////////////////////////////////////////////////////////////
// List: TVM PARM HELP (1 - TVM unique parameter)
////////////////////////////////////////////////////////////////////////
call gfisp_additem('TVM PARM HELP','|IP@',                     0,null,'|;1;TVM IP address');
call gfisp_additem('TVM PARM HELP','|MAC@',                    0,null,'|;1;TVM MAC address');
call gfisp_additem('TVM PARM HELP','|STATION NAME',            0,null,'|;1;TVM station name');
call gfisp_additem('TVM PARM HELP','|STATION NAME SHORT',      0,null,'|;1;TVM station abbreviated name');
call gfisp_additem('TVM PARM HELP','|ADDR1',                   0,null,'|;1;TVM address 1');
call gfisp_additem('TVM PARM HELP','|ADDR2',                   0,null,'|;1;TVM address 2');

call gfisp_additem('TVM PARM HELP','|L2G URL MODE',            0,null,'|;;FIS PayDirect (Link2Gov) connection type: TEST - test server; PROD - production server');
call gfisp_additem('TVM PARM HELP','|L2G MERCHANT CODE',       0,null,'|;;FIS PayDirect (Link2Gov) merchant code');
call gfisp_additem('TVM PARM HELP','|L2G SETTLE MERCHANT CODE',0,null,'|;;FIS PayDirect (Link2Gov) settlement merchant code');
call gfisp_additem('TVM PARM HELP','|L2G PWD',                 0,null,'|;;FIS PayDirect (Link2Gov) password');

call gfisp_additem('TVM PARM HELP','|ISP PHONE',               0,null,'|;;TVM ISP dial-up phone number');
call gfisp_additem('TVM PARM HELP','|ISP UID',                 0,null,'|;1;TVM ISP dial-up user ID');
call gfisp_additem('TVM PARM HELP','|ISP PWD',                 0,null,'|;1;TVM ISP dial-up password');
call gfisp_additem('TVM PARM HELP','|DIALUP PHONE',            0,null,'|;1;TVM dial-up connection phone number');
call gfisp_additem('TVM PARM HELP','|DIALUP UID',              0,null,'|;;TVM dial-up connection user ID');
call gfisp_additem('TVM PARM HELP','|DIALUP PWD',              0,null,'|;;TVM dial-up connection password');

////////////////////////////////////////////////////////////////////////
// List: PEM PARM
////////////////////////////////////////////////////////////////////////
select list(eq_n)||',' into ls_eq from gfi_eq where eq_type=2 and eq_n>0;
while ls_eq<>'' loop
   set li=locate(ls_eq,',');
   set li_loc=cast(left(ls_eq,li - 1) as smallint);
   set ls_eq=substr(ls_eq,li+1);

   call gfisp_additem('PEM PARM','|IP@',li_loc);
end loop;

delete from gfi_lst where type='PEM PARM' and class not in (select eq_n from gfi_eq where eq_type=2 and eq_n>0);

commit;

////////////////////////////////////////////////////////////////////////
// List: PEM PARM HELP (1 - PEM unique parameter)
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PEM PARM HELP','|IP@',0,null,'|;1;PEM IP address');

////////////////////////////////////////////////////////////////////////
// List: TVM PARM SEQ
////////////////////////////////////////////////////////////////////////
call gfisp_additem('TVM PARM SEQ','Default',    0,1,'2011-06-01 00:00:00');
call gfisp_additem('TVM PARM SEQ','Alternative',0,2,'2011-06-01 00:00:00');

////////////////////////////////////////////////////////////////////////
// List: PEM PARM SEQ
////////////////////////////////////////////////////////////////////////
call gfisp_additem('PEM PARM SEQ','Default',    0,1,'2011-06-01 00:00:00');
call gfisp_additem('PEM PARM SEQ','Alternative',0,2,'2011-06-01 00:00:00');

////////////////////////////////////////////////////////////////////////
// List: LG-176 - Modify the value in gfi_lst for RBBL (RELEASE 2.05.10.50)
////////////////////////////////////////////////////////////////////////
update gfi_lst
set value='BY;Real Big Bad List (maximum bad list records allowed): Yes - 10,000 records; No - 2,500 records; 100k (100k badlist);'
where type='SYS PARM HELP' and name= 'RBBL';
commit;

////////////////////////////////////////////////////////////////////////
// populate attr table
// LG-121 - 11/21/14 - MGM - Modified
// # 65 from 65,'RequireID','ReqID','Require an ID card being checked'
//			to 65,'Display the last transaction','DispLastTrans','Display the last transaction'
// # 95 from 95,'FareDisabled','FareDis','Fare is disabled'
//			to 95,'Attribute 95','Attr 95','Attribute 95');
// Added # 255,'FareDisabled','FareDis','Fare is disabled'
// LG-121 #2 - 24,'Swap Zone','SwapZone','Swap Zone'
// LG-194 #66 - 66,'TrmCrd_wPendFare','TrmCrd_wPendFare','Release card from escrow requiring upcharge'
// LG-188 Change # 65 Text field from DispLastTrans to DispLast(Maximum of 8 characters)
//		  Change # 66 Text field from TrmCrd_wPendFare to TrmCrd_w (Maximum of 8 characters)
//
//  *******PLEASE NOTE: TEXT (3rd Column) IS MAXIMUM OF 8 CHARACTERS***********
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sysconstraint where constraint_name='FK_FARECELL_ATTR') then
	alter table farecell drop constraint FK_FARECELL_ATTR;
end if;

truncate table attr;

insert into attr (attr,description,text,ext_description) values(0, 'Undefined','Undef','Undefined');
insert into attr (attr,description,text,ext_description) values(1, 'TallyClear','T & C','Fare value for tally and clear');
insert into attr (attr,description,text,ext_description) values(2, 'TallyDumpClear','T-D & C','Fare value for tally, dump and clear');
insert into attr (attr,description,text,ext_description) values(3, 'UseCurrentValue','CurrFare','Use current value to compute the fare');
insert into attr (attr,description,text,ext_description) values(4, 'OverrideTransfer','O/T','Fare value that flags the farebox to use');
insert into attr (attr,description,text,ext_description) values(5, '_05DollarBill','$5 Bill','$5 dollar bill');
insert into attr (attr,description,text,ext_description) values(6, '_10DollarBill','$10 Bill','$10 dollar bill');
insert into attr (attr,description,text,ext_description) values(7, '_20DollarBill','$20 Bill','$20 dollar bill');
insert into attr (attr,description,text,ext_description) values(8, 'RejectTRiMCard','RejCard','Return TRiM card without processing');
insert into attr (attr,description,text,ext_description) values(9, 'ReadTRiMCard','ReadCard','Read TRiM card without processing');
insert into attr (attr,description,text,ext_description) values(10,'RestoreTRiMCard','RestCard','Restore TRiM card without processing');
insert into attr (attr,description,text,ext_description) values(11,'IssueTransferKey','XferPrev','Issue transfer based on previous fare');
insert into attr (attr,description,text,ext_description) values(12,'ChangeCard','Change','Issue change card');
insert into attr (attr,description,text,ext_description) values(13,'HoldCard','HoldCard','Hold card in escrow');
insert into attr (attr,description,text,ext_description) values(14,'ReleaseCard','RelCard','Release card from escrow');
insert into attr (attr,description,text,ext_description) values(15,'HoldFare','HoldFare','Ignore current fare value');
insert into attr (attr,description,text,ext_description) values(16,'ProcessFare','ProcFare','Process current fare value');
insert into attr (attr,description,text,ext_description) values(17,'BillEnable','BillEnab','Enable/disable bill transport');
insert into attr (attr,description,text,ext_description) values(18,'BillOverride','BillOver','Bill override');
insert into attr (attr,description,text,ext_description) values(19,'TicketOverride','TickOver','Ticket override');
insert into attr (attr,description,text,ext_description) values(20,'Multi-Fare','MultFare','Multi-Fare');
insert into attr (attr,description,text,ext_description) values(21,'FullFare+Transfer','FF+Xfer','Full Fare + Transfer');
insert into attr (attr,description,text,ext_description) values(22,'TransferFareOverride','XferFOvr','Transfer Full Fare Override');
insert into attr (attr,description,text,ext_description) values(23,'Recharge','Recharge','Add to stored value');
insert into attr (attr,description,text,ext_description) values(24,'Swap Zone','SwapZone','Swap Zone');
insert into attr (attr,description,text,ext_description) values(25,'BillOverrideScreen','BlOvrScr','Display bill Override choice Screen (Odyssey with menued OCU only)');
insert into attr (attr,description,text,ext_description) values(26,'IssueScreen','IssueScr','Issue screen');
insert into attr (attr,description,text,ext_description) values(27,'CPUReset','CPUReset','Farebox warm or cold starts');
insert into attr (attr,description,text,ext_description) values(28,'IssueDollarValueCard','XValCard','When key is pressed, puts the farebox in hold and accepts money. When same key pressed, issues a value card for the dollar amount on the Drivers Display.');
insert into attr (attr,description,text,ext_description) values(29,'AutoCouponOverride','CoupOvr','Automatically override coupon');
insert into attr (attr,description,text,ext_description) values(30,'TwoBillReclassify','$2 Bill','$2 bill reclassify');
insert into attr (attr,description,text,ext_description) values(31,'DisplayFreeTally','DspFree','Display free tally');
insert into attr (attr,description,text,ext_description) values(32,'DisplayTally','DspTally','Display tally');
insert into attr (attr,description,text,ext_description) values(33,'DisplayNoTextTally','DspNoTxt','Display no text for tally');
insert into attr (attr,description,text,ext_description) values(34,'DisableCoinMech','DisablCM','Disable coin mech');
insert into attr (attr,description,text,ext_description) values(35,'DisableBillValidator','DisablBV','Disable bill validator');
insert into attr (attr,description,text,ext_description) values(36,'DisableCoinMech&BillValidator','DisCMBV','Disable coin mech and bill validator');
insert into attr (attr,description,text,ext_description) values(37,'TallyClearBlank','T&CBlank','Tally & Clear, but don''t show "T & C"');
insert into attr (attr,description,text,ext_description) values(38,'Swap Fareset 1','SwapFS1','Swap Fareset 1');
insert into attr (attr,description,text,ext_description) values(39,'Swap Fareset 2','SwapFS2','Swap Fareset 2');
insert into attr (attr,description,text,ext_description) values(40,'Swap Fareset 3','SwapFS3','Swap Fareset 3');
insert into attr (attr,description,text,ext_description) values(41,'Swap Fareset 4','SwapFS4','Swap Fareset 4');
insert into attr (attr,description,text,ext_description) values(42,'Swap Fareset 5','SwapFS5','Swap Fareset 5');
insert into attr (attr,description,text,ext_description) values(43,'Swap Fareset 6','SwapFS6','Swap Fareset 6');
insert into attr (attr,description,text,ext_description) values(44,'Swap Fareset 7','SwapFS7','Swap Fareset 7');
insert into attr (attr,description,text,ext_description) values(45,'Swap Fareset 8','SwapFS8','Swap Fareset 8');
insert into attr (attr,description,text,ext_description) values(46,'Swap Fareset 9','SwapFS9','Swap Fareset 9');
insert into attr (attr,description,text,ext_description) values(47,'Swap Fareset 10','SwapFS10','Swap Fareset 10');
insert into attr (attr,description,text,ext_description) values(48,'TallyKeyNoEffectOnDisplay','NoOCUEff','Tally key has no effect on OCU or patron display; count key only');
insert into attr (attr,description,text,ext_description) values(49,'IgnorePassbackOnNextCard','IgnorePB','Ignore passback on next card');
insert into attr (attr,description,text,ext_description) values(50,'TariffControl','TarifCtl','Toggle tariff control on/off');
insert into attr (attr,description,text,ext_description) values(51,'NextZone','NextZone','Next zone');
insert into attr (attr,description,text,ext_description) values(52,'PreviousZone','PrevZone','Previous zone');
insert into attr (attr,description,text,ext_description) values(53,'FaresetToggle','FSToggle','Toggle fareset');
insert into attr (attr,description,text,ext_description) values(54,'GroupAuthorizationKey','GroupKey','Group boarding authorization key');
insert into attr (attr,description,text,ext_description) values(55,'TariffControlOn/Off','TarifOff','Toggle tariff control on/off permanently');
insert into attr (attr,description,text,ext_description) values(56,'IssueProofofPay','PayProof','Issue a proof of payment document which carries designator 250 so that it cannot be accepted back into the farebox');
insert into attr (attr,description,text,ext_description) values(57,'PassCounter','PassCntr','Count valid passes');
insert into attr (attr,description,text,ext_description) values(58,'MemoryStatus','MemStat','Memory status');
insert into attr (attr,description,text,ext_description) values(59,'TallyWithFare','T & Fare','Modified tally that needed money');
insert into attr (attr,description,text,ext_description) values(60,'ActionKey','ActionK','Action key pressed');
insert into attr (attr,description,text,ext_description) values(61,'TempTRiMBypass','TmpTRMBP','Place TRiM in temporary bypass to issue current doc');
insert into attr (attr,description,text,ext_description) values(62,'LastCardBad','LastCBad','Driver reported bad card');
insert into attr (attr,description,text,ext_description) values(63,'ChangeUITheme','UITheme','SCRV - change display UI theme (light/dark)');
insert into attr (attr,description,text,ext_description) values(64,'CheckID','CheckID','Check ID to verify user profile');
insert into attr (attr,description,text,ext_description) values(65,'Display the last transaction','DispLast','Display the last transaction');
insert into attr (attr,description,text,ext_description) values(66,'TrmCrd_wPendFare','TrmCrd_w','Release card from escrow requiring upcharge');
insert into attr (attr,description,text,ext_description) values(67,'Attribute 67','Attr 67','Attribute 67');
insert into attr (attr,description,text,ext_description) values(68,'ProofPayment','PrfPymnt','Time Based Proof of Payment');
insert into attr (attr,description,text,ext_description) values(69,'Attribute 69','Attr 69','Attribute 69');
insert into attr (attr,description,text,ext_description) values(70,'Attribute 70','Attr 70','Attribute 70');
insert into attr (attr,description,text,ext_description) values(71,'Attribute 71','Attr 71','Attribute 71');
insert into attr (attr,description,text,ext_description) values(72,'Attribute 72','Attr 72','Attribute 72');
insert into attr (attr,description,text,ext_description) values(73,'Attribute 73','Attr 73','Attribute 73');
insert into attr (attr,description,text,ext_description) values(74,'Attribute 74','Attr 74','Attribute 74');
insert into attr (attr,description,text,ext_description) values(75,'Attribute 75','Attr 75','Attribute 75');
insert into attr (attr,description,text,ext_description) values(76,'Attribute 76','Attr 76','Attribute 76');
insert into attr (attr,description,text,ext_description) values(77,'Attribute 77','Attr 77','Attribute 77');
insert into attr (attr,description,text,ext_description) values(78,'Attribute 78','Attr 78','Attribute 78');
insert into attr (attr,description,text,ext_description) values(79,'Attribute 79','Attr 79','Attribute 79');
insert into attr (attr,description,text,ext_description) values(80,'Attribute 80','Attr 80','Attribute 80');
insert into attr (attr,description,text,ext_description) values(81,'Attribute 81','Attr 81','Attribute 81');
insert into attr (attr,description,text,ext_description) values(82,'Attribute 82','Attr 82','Attribute 82');
insert into attr (attr,description,text,ext_description) values(83,'Attribute 83','Attr 83','Attribute 83');
insert into attr (attr,description,text,ext_description) values(84,'Attribute 84','Attr 84','Attribute 84');
insert into attr (attr,description,text,ext_description) values(85,'Attribute 85','Attr 85','Attribute 85');
insert into attr (attr,description,text,ext_description) values(86,'Attribute 86','Attr 86','Attribute 86');
insert into attr (attr,description,text,ext_description) values(87,'Attribute 87','Attr 87','Attribute 87');
insert into attr (attr,description,text,ext_description) values(88,'Attribute 88','Attr 88','Attribute 88');
insert into attr (attr,description,text,ext_description) values(89,'Attribute 89','Attr 89','Attribute 89');
insert into attr (attr,description,text,ext_description) values(90,'Attribute 90','Attr 90','Attribute 90');
insert into attr (attr,description,text,ext_description) values(91,'Attribute 91','Attr 91','Attribute 91');
insert into attr (attr,description,text,ext_description) values(92,'Attribute 92','Attr 92','Attribute 92');
insert into attr (attr,description,text,ext_description) values(93,'Attribute 93','Attr 93','Attribute 93');
insert into attr (attr,description,text,ext_description) values(94,'Attribute 94','Attr 94','Attribute 94');
insert into attr (attr,description,text,ext_description) values(95,'Attribute 95','Attr 95','Attribute 95');
insert into attr (attr,description,text,ext_description) values(255,'FareDisabled','FareDis','Fare is disabled');


alter table farecell add constraint FK_FARECELL_ATTR foreign key (attr) references attr (attr);


//------------------------------------------------------------------------
// Add new row to gfi_lst for TVM EQ STATUS
// LG-223 - 04/28/15
//------------------------------------------------------------------------	
if not exists(SELECT st.code, co.name FROM gfi_lst co, gfi_lst st
				WHERE co.type='COLOR' AND co.class=0
				AND st.type='EQ STATUS'
				AND st.code = 6 AND co.code=st.class) then
   insert into gfi_lst (type,class,code,name,value)
   Values('EQ STATUS',5,5,'Instrusion','Instrusion, Vibration, or Tilt');
end if;

//------------------------------------------------------------------------
// Add new row to gfi_lst for TVM EQ STATUS (TMV Shunt processing)
// LG-223 - 04/28/15
//------------------------------------------------------------------------	
if not exists(SELECT st.code, co.name FROM gfi_lst co, gfi_lst st
				WHERE co.type='COLOR' AND co.class=0
				AND st.type='EQ STATUS'
				AND st.code = 6 AND co.code=st.class) then
   insert into gfi_lst (type,class,code,name,value)
   Values('EQ STATUS',6,6,'Instrusion','Instrusion, Vibration, or Tilt');
end if;

//------------------------------------------------------------------------
// Add default row into configurations
//------------------------------------------------------------------------	
if not exists(select null from configurations where id = 0) then
	insert into configurations values(0, 'Default', 1, 'Default configuration should be available all the time');
end if;

//------------------------------------------------------------------------
// Add Power Builder Version
//------------------------------------------------------------------------	

if not exists(select null from configurationsData where configurationID = 0 and dataName = 'Power Builder Version') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'Power Builder Version', pbVersion, 1, 'Power Builder Version Number (format #.##.##.##)');
else	
	update configurationsData set
		dataValue = pbVersion
	where configurationID = 0
	  and dataName = 'Power Builder Version';
end if;

// LG-1279
if not exists (select null from dba.[media_category] where [category] = 0) then
	insert dba.[media_category] ([category], [text]) values (0, 'Transfer');
end if;

if not exists (select null from dba.[media_category] where [category] = 1) then
	insert dba.[media_category] ([category], [text]) values (1, 'Period Pass');
end if;

if not exists (select null from dba.[media_category] where [category] = 2) then
	insert dba.[media_category] ([category], [text]) values (2, 'Stored Ride');
end if;

if not exists (select null from dba.[media_category] where [category] = 3) then
	insert dba.[media_category] ([category], [text]) values (3, 'Stored Value');
end if;

if exists (select null from dba.[media_category] where [category] = 4) then
	update dba.[media_category] set [text] = 'Credit Card/Special Card' where [category] = 4;
else
	insert dba.[media_category] ([category], [text]) values (4, 'Credit Card/Special Card');
end if;

if not exists (select null from dba.[media_category] where [category] = 5) then
	insert dba.[media_category] ([category], [text]) values (5, 'Token / Ticket');
end if;

if not exists (select null from dba.[media_category] where [category] = 7) then
	insert dba.[media_category] ([category], [text]) values (7, 'Maintenance / Employee Card');
end if;


if not exists (select null from dba.[media_category_transaction_type] where [category] = 0 and [type] = 118) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (0, 118);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 0 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (0, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 0 and [type] = 128) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (0, 128);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 0 and [type] = 142) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (0, 142);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 0 and [type] = 195) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (0, 195);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 1 and [type] = 115) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (1, 115);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 1 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (1, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 1 and [type] = 142) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (1, 142);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 1 and [type] = 194) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (1, 194);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 2 and [type] = 114) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (2, 114);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 2 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (2, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 2 and [type] = 142) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (2, 142);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 2 and [type] = 192) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (2, 192);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 116) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 116);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 135) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 135);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 139) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 139);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 142) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 142);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 145) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 145);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 3 and [type] = 193) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (3, 193);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 4 and [type] = 117) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (4, 117);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 4 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (4, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 4 and [type] = 129) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (4, 129);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 5 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (5, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 5 and [type] = 129) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (5, 129);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 7 and [type] = 119) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (7, 119);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 7 and [type] = 194) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (7, 194);
end if;

if not exists (select null from dba.[media_category_transaction_type] where [category] = 7 and [type] = 196) then
	insert dba.[media_category_transaction_type] ([category], [type]) values (7, 196);
end if;

-- GD-572 - Implement DB/PB related changes for Cleveland Xerox Integration
call gfisp_addtype('CLEVELAND XEROX',   'Cleveland Xerox: Input Parms Path, Output Parms Path, Output Transactions Path');

if not exists (select null from gfi_lst where [type] = 'Cleveland Xerox' and [class] = 0 and code = 1) then
	insert into gfi_lst
	values ('CLEVELAND XEROX', 0,1,'Input Parms Path','C:\Users\NMPN6R\Documents\Cleveland Input Parameters');
end if;
if not exists (select null from gfi_lst where [type] = 'Cleveland Xerox' and [class] = 0 and code = 2) then
	insert into gfi_lst
	values ('CLEVELAND XEROX', 0,2,'Output Parms Path' ,'C:\Users\NMPN6R\Documents\Cleveland Parameter Files');
end if;
if not exists (select null from gfi_lst where [type] = 'Cleveland Xerox' and [class] = 0 and code = 3) then
	insert into gfi_lst
	values ('CLEVELAND XEROX', 0,3,'Output Transactions Path','C:\Users\NMPN6R\Documents\Cleveland Transaction Files');
end if;

-- GD-2370 - Add option settings to run updtgs and updtrsf stored procedures
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'RUN_UPDTGS') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'RUN_UPDTGS', 'Yes', 1, 'Setting to run updtgs stored procedure');
end if;
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'RUN_UPDTRSF') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'RUN_UPDTRSF', 'Yes', 1, 'Setting to run updtrsf stored procedure');
end if;


------------------------------------------------------------------------
-- populate et table
------------------------------------------------------------------------
-- type 0
if exists (select null from et where type = 0) then
	update et set text = 'Idle event', ridership = 'N' where type = 0;
else
	insert into et values(0, 'Idle event', 'N');
end if;
-- type 1
if exists (select null from et where type = 1) then
	update et set text = 'Cashbox out', ridership = 'N' where type = 1;
else
	insert into et values(1, 'Cashbox out', 'N');
end if;
-- type 2
if exists (select null from et where type = 2) then
	update et set text = 'Cashbox in', ridership = 'N' where type = 2;
else
	insert into et values(2, 'Cashbox in', 'N');
end if;
-- type 3
if exists (select null from et where type = 3) then
	update et set text = 'Logon', ridership = 'N' where type = 3;
else
	insert into et values(3, 'Logon', 'N');
end if;
-- type 4
if exists (select null from et where type = 4) then
	update et set text = 'Logoff', ridership = 'N' where type = 4;
else
	insert into et values(4, 'Logoff', 'N');
end if;
-- type 5
if exists (select null from et where type = 5) then
	update et set text = 'Midnight', ridership = 'N' where type = 5;
else
	insert into et values(5, 'Midnight', 'N');
end if;
-- type 6
if exists (select null from et where type = 6) then
	update et set text = 'Event overflow', ridership = 'N' where type = 6;
else
	insert into et values(6, 'Event overflow', 'N');
end if;
-- type 7
if exists (select null from et where type = 7) then
	update et set text = 'Bypass on', ridership = 'N' where type = 7;
else
	insert into et values(7, 'Bypass on', 'N');
end if;
-- type 8
if exists (select null from et where type = 8) then
	update et set text = 'Bypass off', ridership = 'N' where type = 8;
else
	insert into et values(8, 'Bypass off', 'N');
end if;
-- type 9
if exists (select null from et where type = 9) then
	update et set text = 'Door opened unauthorized', ridership = 'N' where type = 9;
else
	insert into et values(9, 'Door opened unauthorized', 'N');
end if;
-- type 10
if exists (select null from et where type = 10) then
	update et set text = 'Door closed unauthorized', ridership = 'N' where type = 10;
else
	insert into et values(10, 'Door closed unauthorized', 'N');
end if;
-- type 11
if exists (select null from et where type = 11) then
	update et set text = 'Cashbox out unauthorized', ridership = 'N' where type = 11;
else
	insert into et values(11, 'Cashbox out unauthorized', 'N');
end if;
-- type 12
if exists (select null from et where type = 12) then
	update et set text = 'Cashbox door duration', ridership = 'N' where type = 12;
else
	insert into et values(12, 'Cashbox door duration', 'N');
end if;
-- type 13
if exists (select null from et where type = 13) then
	update et set text = 'Cold start', ridership = 'N' where type = 13;
else
	insert into et values(13, 'Cold start', 'N');
end if;
-- type 14
if exists (select null from et where type = 14) then
	update et set text = 'Hardware clock failure', ridership = 'N' where type = 14;
else
	insert into et values(14, 'Hardware clock failure', 'N');
end if;
-- type 15
if exists (select null from et where type = 15) then
	update et set text = 'Portable probe used', ridership = 'N' where type = 15;
else
	insert into et values(15, 'Portable probe used', 'N');
end if;
-- type 16
if exists (select null from et where type = 16) then
	update et set text = 'Time discrepancy', ridership = 'N' where type = 16;
else
	insert into et values(16, 'Time discrepancy', 'N');
end if;
-- type 17
if exists (select null from et where type = 17) then
	update et set text = 'Power restored', ridership = 'N' where type = 17;
else
	insert into et values(17, 'Power restored', 'N');
end if;
-- type 18
if exists (select null from et where type = 18) then
	update et set text = 'Probed, cashbox not removed', ridership = 'N' where type = 18;
else
	insert into et values(18, 'Probed, cashbox not removed', 'N');
end if;
-- type 19
if exists (select null from et where type = 19) then
	update et set text = 'Probed, no rev, CBX not removed', ridership = 'N' where type = 19;
else
	insert into et values(19, 'Probed, no rev, CBX not removed', 'N');
end if;
-- type 20
if exists (select null from et where type = 20) then
	update et set text = 'TRiM offline', ridership = 'N' where type = 20;
else
	insert into et values(20, 'TRiM offline', 'N');
end if;
-- type 21
if exists (select null from et where type = 21) then
	update et set text = 'Cashbox after memory clear', ridership = 'N' where type = 21;
else
	insert into et values(21, 'Cashbox after memory clear', 'N');
end if;
-- type 22
if exists (select null from et where type = 22) then
	update et set text = 'No cashbox after memory clear', ridership = 'N' where type = 22;
else
	insert into et values(22, 'No cashbox after memory clear', 'N');
end if;
-- type 23
if exists (select null from et where type = 23) then
	update et set text = 'Cashbox full', ridership = 'N' where type = 23;
else
	insert into et values(23, 'Cashbox full', 'N');
end if;
-- type 24
if exists (select null from et where type = 24) then
	update et set text = 'TRiM online', ridership = 'N' where type = 24;
else
	insert into et values(24, 'TRiM online', 'N');
end if;
-- type 25
if exists (select null from et where type = 25) then
	update et set text = 'Maintenance number entered', ridership = 'N' where type = 25;
else
	insert into et values(25, 'Maintenance number entered', 'N');
end if;
-- type 26
if exists (select null from et where type = 26) then
	update et set text = 'Fareset change', ridership = 'N' where type = 26;
else
	insert into et values(26, 'Fareset change', 'N');
end if;
-- type 27
if exists (select null from et where type = 27) then
	update et set text = 'Bill mechanism error', ridership = 'N' where type = 27;
else
	insert into et values(27, 'Bill mechanism error', 'N');
end if;
-- type 28
if exists (select null from et where type = 28) then
	update et set text = 'Coin mechanism error', ridership = 'N' where type = 28;
else
	insert into et values(28, 'Coin mechanism error', 'N');
end if;
-- type 29
if exists (select null from et where type = 29) then
	update et set text = 'Partial memory clear', ridership = 'N' where type = 29;
else
	insert into et values(29, 'Partial memory clear', 'N');
end if;
-- type 30
if exists (select null from et where type = 30) then
	update et set text = 'Door closed', ridership = 'N' where type = 30;
else
	insert into et values(30, 'Door closed', 'N');
end if;
-- type 31
if exists (select null from et where type = 31) then
	update et set text = 'Probed completed', ridership = 'N' where type = 31;
else
	insert into et values(31, 'Probed completed', 'N');
end if;
-- type 32
if exists (select null from et where type = 32) then
	update et set text = 'Bad list pass used', ridership = 'N' where type = 32;
else
	insert into et values(32, 'Bad list pass used', 'N');
end if;
-- type 33
if exists (select null from et where type = 33) then
	update et set text = 'Probe, door not opened', ridership = 'N' where type = 33;
else
	insert into et values(33, 'Probe, door not opened', 'N');
end if;
-- type 34
if exists (select null from et where type = 34) then
	update et set text = 'Automatic memory clear', ridership = 'N' where type = 34;
else
	insert into et values(34, 'Automatic memory clear', 'N');
end if;
-- type 35
if exists (select null from et where type = 35) then
	update et set text = 'Bypass after probe', ridership = 'N' where type = 35;
else
	insert into et values(35, 'Bypass after probe', 'N');
end if;
-- type 36
if exists (select null from et where type = 36) then
	update et set text = 'Auto logoff', ridership = 'N' where type = 36;
else
	insert into et values(36, 'Auto logoff', 'N');
end if;
-- type 37
if exists (select null from et where type = 37) then
	update et set text = 'Hardware clock error', ridership = 'N' where type = 37;
else
	insert into et values(37, 'Hardware clock error', 'N');
end if;
-- type 38
if exists (select null from et where type = 38) then
	update et set text = 'Maintenance pass entered', ridership = 'N' where type = 38;
else
	insert into et values(38, 'Maintenance pass entered', 'N');
end if;
-- type 39
if exists (select null from et where type = 39) then
	update et set text = 'Maint pass partial memory clear', ridership = 'N' where type = 39;
else
	insert into et values(39, 'Maint pass partial memory clear', 'N');
end if;
-- type 40
if exists (select null from et where type = 40) then
	update et set text = 'Card logon', ridership = 'N' where type = 40;
else
	insert into et values(40, 'Card logon', 'N');
end if;
-- type 41
if exists (select null from et where type = 41) then
	update et set text = 'Maintenance pass memory clear', ridership = 'N' where type = 41;
else
	insert into et values(41, 'Maintenance pass memory clear', 'N');
end if;
-- type 42
if exists (select null from et where type = 42) then
	update et set text = 'Keypad memory clear', ridership = 'N' where type = 42;
else
	insert into et values(42, 'Keypad memory clear', 'N');
end if;
-- type 43
if exists (select null from et where type = 43) then
	update et set text = 'Cashbox ID lost normal', ridership = 'N' where type = 43;
else
	insert into et values(43, 'Cashbox ID lost normal', 'N');
end if;
-- type 44
if exists (select null from et where type = 44) then
	update et set text = 'Cashbox ID lost on power-up', ridership = 'N' where type = 44;
else
	insert into et values(44, 'Cashbox ID lost on power-up', 'N');
end if;
-- type 45
if exists (select null from et where type = 45) then
	update et set text = 'Puller pass entered', ridership = 'N' where type = 45;
else
	insert into et values(45, 'Puller pass entered', 'N');
end if;
-- type 46
if exists (select null from et where type = 46) then
	update et set text = 'TRiM stock low', ridership = 'N' where type = 46;
else
	insert into et values(46, 'TRiM stock low', 'N');
end if;
-- type 47
if exists (select null from et where type = 47) then
	update et set text = 'TRiM bypass on', ridership = 'N' where type = 47;
else
	insert into et values(47, 'TRiM bypass on', 'N');
end if;
-- type 48
if exists (select null from et where type = 48) then
	update et set text = 'TRiM bypass off', ridership = 'N' where type = 48;
else
	insert into et values(48, 'TRiM bypass off', 'N');
end if;
-- type 49
if exists (select null from et where type = 49) then
	update et set text = 'Manual event', ridership = 'N' where type = 49;
else
	insert into et values(49, 'Manual event', 'N');
end if;
-- type 50
if exists (select null from et where type = 50) then
	update et set text = 'Hourly event', ridership = 'N' where type = 50;
else
	insert into et values(50, 'Hourly event', 'N');
end if;
-- type 51
if exists (select null from et where type = 51) then
	update et set text = 'Menu entry event', ridership = 'N' where type = 51;
else
	insert into et values(51, 'Menu entry event', 'N');
end if;
-- type 52
if exists (select null from et where type = 52) then
	update et set text = 'Probe event', ridership = 'N' where type = 52;
else
	insert into et values(52, 'Probe event', 'N');
end if;
-- type 53
if exists (select null from et where type = 53) then
	update et set text = 'TRiM stock added', ridership = 'N' where type = 53;
else
	insert into et values(53, 'TRiM stock added', 'N');
end if;
-- type 54
if exists (select null from et where type = 54) then
	update et set text = 'Lid open', ridership = 'N' where type = 54;
else
	insert into et values(54, 'Lid open', 'N');
end if;
-- type 55
if exists (select null from et where type = 55) then
	update et set text = 'Lid closed', ridership = 'N' where type = 55;
else
	insert into et values(55, 'Lid closed', 'N');
end if;
-- type 56
if exists (select null from et where type = 56) then
	update et set text = 'Periodic event', ridership = 'N' where type = 56;
else
	insert into et values(56, 'Periodic event', 'N');
end if;
-- type 57
if exists (select null from et where type = 57) then
	update et set text = 'Door open after memory clear', ridership = 'N' where type = 57;
else
	insert into et values(57, 'Door open after memory clear', 'N');
end if;
-- type 58
if exists (select null from et where type = 58) then
	update et set text = 'Lid open after memory clear', ridership = 'N' where type = 58;
else
	insert into et values(58, 'Lid open after memory clear', 'N');
end if;
-- type 59
if exists (select null from et where type = 59) then
	update et set text = 'Bypass on after memory clear', ridership = 'N' where type = 59;
else
	insert into et values(59, 'Bypass on after memory clear', 'N');
end if;
-- type 60
if exists (select null from et where type = 60) then
	update et set text = 'Future fare structure', ridership = 'N' where type = 60;
else
	insert into et values(60, 'Future fare structure', 'N');
end if;
-- type 61
if exists (select null from et where type = 61) then
	update et set text = 'Bad driver', ridership = 'N' where type = 61;
else
	insert into et values(61, 'Bad driver', 'N');
end if;
-- type 62
if exists (select null from et where type = 62) then
	update et set text = 'Bad route', ridership = 'N' where type = 62;
else
	insert into et values(62, 'Bad route', 'N');
end if;
-- type 63
if exists (select null from et where type = 63) then
	update et set text = 'TRiM card lost', ridership = 'N' where type = 63;
else
	insert into et values(63, 'TRiM card lost', 'N');
end if;
-- type 64
if exists (select null from et where type = 64) then
	update et set text = 'Barcode reader offline', ridership = 'N' where type = 64;
else
	insert into et values(64, 'Barcode reader offline', 'N');
end if;
-- type 65
if exists (select null from et where type = 65) then
	update et set text = 'Barcode reader online', ridership = 'N' where type = 65;
else
	insert into et values(65, 'Barcode reader online', 'N');
end if;
-- type 66
if exists (select null from et where type = 66) then
	update et set text = 'Barcode printer offline', ridership = 'N' where type = 66;
else
	insert into et values(66, 'Barcode printer offline', 'N');
end if;
-- type 67
if exists (select null from et where type = 67) then
	update et set text = 'Barcode printer online', ridership = 'N' where type = 67;
else
	insert into et values(67, 'Barcode printer online', 'N');
end if;
-- type 68
if exists (select null from et where type = 68) then
	update et set text = 'TRiM no stock', ridership = 'N' where type = 68;
else
	insert into et values(68, 'TRiM no stock', 'N');
end if;
-- type 69
if exists (select null from et where type = 69) then
	update et set text = 'Receive future FS via wi-fi probing', ridership = 'N' where type = 69;
else
	insert into et values(69, 'Receive future FS via wi-fi probing', 'N');
end if;
-- type 70
if exists (select null from et where type = 70) then
	update et set text = 'Event 70', ridership = 'N' where type = 70;
else
	insert into et values(70, 'Event 70', 'N');
end if;
-- type 71
if exists (select null from et where type = 71) then
	update et set text = 'Event 71', ridership = 'N' where type = 71;
else
	insert into et values(71, 'Event 71', 'N');
end if;
-- type 72
if exists (select null from et where type = 72) then
	update et set text = 'Event 72', ridership = 'N' where type = 72;
else
	insert into et values(72, 'Event 72', 'N');
end if;
-- type 73
if exists (select null from et where type = 73) then
	update et set text = 'Event 73', ridership = 'N' where type = 73;
else
	insert into et values(73, 'Event 73', 'N');
end if;
-- type 74
if exists (select null from et where type = 74) then
	update et set text = 'Event 74', ridership = 'N' where type = 74;
else
	insert into et values(74, 'Event 74', 'N');
end if;
-- type 75
if exists (select null from et where type = 75) then
	update et set text = 'Event 75', ridership = 'N' where type = 75;
else
	insert into et values(75, 'Event 75', 'N');
end if;
-- type 76
if exists (select null from et where type = 76) then
	update et set text = 'Event 76', ridership = 'N' where type = 76;
else
	insert into et values(76, 'Event 76', 'N');
end if;
-- type 77
if exists (select null from et where type = 77) then
	update et set text = 'Event 77', ridership = 'N' where type = 77;
else
	insert into et values(77, 'Event 77', 'N');
end if;
-- type 78
if exists (select null from et where type = 78) then
	update et set text = 'Event 78', ridership = 'N' where type = 78;
else
	insert into et values(78, 'Event 78', 'N');
end if;
-- type 79
if exists (select null from et where type = 79) then
	update et set text = 'Event 79', ridership = 'N' where type = 79;
else
	insert into et values(79, 'Event 79', 'N');
end if;
-- type 80
if exists (select null from et where type = 80) then
	update et set text = 'Event 80', ridership = 'N' where type = 80;
else
	insert into et values(80, 'Event 80', 'N');
end if;
-- type 81
if exists (select null from et where type = 81) then
	update et set text = 'Event 81', ridership = 'N' where type = 81;
else
	insert into et values(81, 'Event 81', 'N');
end if;
-- type 82
if exists (select null from et where type = 82) then
	update et set text = 'Event 82', ridership = 'N' where type = 82;
else
	insert into et values(82, 'Event 82', 'N');
end if;
-- type 83
if exists (select null from et where type = 83) then
	update et set text = 'Event 83', ridership = 'N' where type = 83;
else
	insert into et values(83, 'Event 83', 'N');
end if;
-- type 84
if exists (select null from et where type = 84) then
	update et set text = 'Event 84', ridership = 'N' where type = 84;
else
	insert into et values(84, 'Event 84', 'N');
end if;
-- type 85
if exists (select null from et where type = 85) then
	update et set text = 'Event 85', ridership = 'N' where type = 85;
else
	insert into et values(85, 'Event 85', 'N');
end if;
-- type 86
if exists (select null from et where type = 86) then
	update et set text = 'Event 86', ridership = 'N' where type = 86;
else
	insert into et values(86, 'Event 86', 'N');
end if;
-- type 87
if exists (select null from et where type = 87) then
	update et set text = 'Event 87', ridership = 'N' where type = 87;
else
	insert into et values(87, 'Event 87', 'N');
end if;
-- type 88
if exists (select null from et where type = 88) then
	update et set text = 'Event 88', ridership = 'N' where type = 88;
else
	insert into et values(88, 'Event 88', 'N');
end if;
-- type 89
if exists (select null from et where type = 89) then
	update et set text = 'Event 89', ridership = 'N' where type = 89;
else
	insert into et values(89, 'Event 89', 'N');
end if;
-- type 90
if exists (select null from et where type = 90) then
	update et set text = 'Event 90', ridership = 'N' where type = 90;
else
	insert into et values(90, 'Event 90', 'N');
end if;
-- type 91
if exists (select null from et where type = 91) then
	update et set text = 'Event 91', ridership = 'N' where type = 91;
else
	insert into et values(91, 'Event 91', 'N');
end if;
-- type 92
if exists (select null from et where type = 92) then
	update et set text = 'Event 92', ridership = 'N' where type = 92;
else
	insert into et values(92, 'Event 92', 'N');
end if;
-- type 93
if exists (select null from et where type = 93) then
	update et set text = 'Event 93', ridership = 'N' where type = 93;
else
	insert into et values(93, 'Event 93', 'N');
end if;
-- type 94
if exists (select null from et where type = 94) then
	update et set text = 'Event 94', ridership = 'N' where type = 94;
else
	insert into et values(94, 'Event 94', 'N');
end if;
-- type 95
if exists (select null from et where type = 95) then
	update et set text = 'Event 95', ridership = 'N' where type = 95;
else
	insert into et values(95, 'Event 95', 'N');
end if;
-- type 96
if exists (select null from et where type = 96) then
	update et set text = 'Event 96', ridership = 'N' where type = 96;
else
	insert into et values(96, 'Event 96', 'N');
end if;
-- type 97
if exists (select null from et where type = 97) then
	update et set text = 'Event 97', ridership = 'N' where type = 97;
else
	insert into et values(97, 'Event 97', 'N');
end if;
-- type 98
if exists (select null from et where type = 98) then
	update et set text = 'Event 98', ridership = 'N' where type = 98;
else
	insert into et values(98, 'Event 98', 'N');
end if;
-- type 99
if exists (select null from et where type = 99) then
	update et set text = 'Event 99', ridership = 'N' where type = 99;
else
	insert into et values(99, 'Event 99', 'N');
end if;
-- type 100
if exists (select null from et where type = 100) then
	update et set text = 'Event 100', ridership = 'N' where type = 100;
else
	insert into et values(100, 'Event 100', 'N');
end if;
-- type 101
if exists (select null from et where type = 101) then
	update et set text = 'Power-up', ridership = 'N' where type = 101;
else
	insert into et values(101, 'Power-up', 'N');
end if;
-- type 102
if exists (select null from et where type = 102) then
	update et set text = 'Midnight', ridership = 'N' where type = 102;
else
	insert into et values(102, 'Midnight', 'N');
end if;
-- type 103
if exists (select null from et where type = 103) then
	update et set text = 'Hourly timestamp', ridership = 'N' where type = 103;
else
	insert into et values(103, 'Hourly timestamp', 'N');
end if;
-- type 104
if exists (select null from et where type = 104) then
	update et set text = 'Driver change', ridership = 'N' where type = 104;
else
	insert into et values(104, 'Driver change', 'N');
end if;
-- type 105
if exists (select null from et where type = 105) then
	update et set text = 'Fareset change', ridership = 'N' where type = 105;
else
	insert into et values(105, 'Fareset change', 'N');
end if;
-- type 106
if exists (select null from et where type = 106) then
	update et set text = 'Route change', ridership = 'N' where type = 106;
else
	insert into et values(106, 'Route change', 'N');
end if;
-- type 107
if exists (select null from et where type = 107) then
	update et set text = 'Run change', ridership = 'N' where type = 107;
else
	insert into et values(107, 'Run change', 'N');
end if;
-- type 108
if exists (select null from et where type = 108) then
	update et set text = 'Trip change', ridership = 'N' where type = 108;
else
	insert into et values(108, 'Trip change', 'N');
end if;
-- type 109
if exists (select null from et where type = 109) then
	update et set text = 'Direction change', ridership = 'N' where type = 109;
else
	insert into et values(109, 'Direction change', 'N');
end if;
-- type 110
if exists (select null from et where type = 110) then
	update et set text = 'Bus stop change', ridership = 'N' where type = 110;
else
	insert into et values(110, 'Bus stop change', 'N');
end if;
-- type 111
if exists (select null from et where type = 111) then
	update et set text = 'Cut time change', ridership = 'N' where type = 111;
else
	insert into et values(111, 'Cut time change', 'N');
end if;
-- type 112
if exists (select null from et where type = 112) then
	update et set text = 'Ready for revenue', ridership = 'N' where type = 112;
else
	insert into et values(112, 'Ready for revenue', 'N');
end if;
-- type 113
if exists (select null from et where type = 113) then
	update et set text = 'Maintenance', ridership = 'N' where type = 113;
else
	insert into et values(113, 'Maintenance', 'N');
end if;
-- type 114
if exists (select null from et where type = 114) then
	update et set text = 'Stored ride card', ridership = 'Y' where type = 114;
else
	insert into et values(114, 'Stored ride card', 'Y');
end if;
-- type 115
if exists (select null from et where type = 115) then
	update et set text = 'Period pass', ridership = 'Y' where type = 115;
else
	insert into et values(115, 'Period pass', 'Y');
end if;
-- type 116
if exists (select null from et where type = 116) then
	update et set text = 'Stored value card', ridership = 'Y' where type = 116;
else
	insert into et values(116, 'Stored value card', 'Y');
end if;
-- type 117
if exists (select null from et where type = 117) then
	update et set text = 'Credit card', ridership = 'Y' where type = 117;
else
	insert into et values(117, 'Credit card', 'Y');
end if;
-- type 118
if exists (select null from et where type = 118) then
	update et set text = 'Transfer received', ridership = 'Y' where type = 118;
else
	insert into et values(118, 'Transfer received', 'Y');
end if;
-- type 119
if exists (select null from et where type = 119) then
	update et set text = 'Got fare', ridership = 'Y' where type = 119;
else
	insert into et values(119, 'Got fare', 'Y');
end if;
-- type 120
if exists (select null from et where type = 120) then
	update et set text = 'Bypass dump', ridership = 'N' where type = 120;
else
	insert into et values(120, 'Bypass dump', 'N');
end if;
-- type 121
if exists (select null from et where type = 121) then
	update et set text = 'Escrow dump', ridership = 'N' where type = 121;
else
	insert into et values(121, 'Escrow dump', 'N');
end if;
-- type 122
if exists (select null from et where type = 122) then
	update et set text = 'Escrow timeout', ridership = 'N' where type = 122;
else
	insert into et values(122, 'Escrow timeout', 'N');
end if;
-- type 123
if exists (select null from et where type = 123) then
	update et set text = 'General purpose', ridership = 'N' where type = 123;
else
	insert into et values(123, 'General purpose', 'N');
end if;
-- type 124
if exists (select null from et where type = 124) then
	update et set text = 'Badlisted card', ridership = 'N' where type = 124;
else
	insert into et values(124, 'Badlisted card', 'N');
end if;
-- type 125
if exists (select null from et where type = 125) then
	update et set text = 'Login', ridership = 'N' where type = 125;
else
	insert into et values(125, 'Login', 'N');
end if;
-- type 126
if exists (select null from et where type = 126) then
	update et set text = 'Restore card', ridership = 'N' where type = 126;
else
	insert into et values(126, 'Restore card', 'N');
end if;
-- type 127
if exists (select null from et where type = 127) then
	update et set text = 'Read card', ridership = 'N' where type = 127;
else
	insert into et values(127, 'Read card', 'N');
end if;
-- type 128
if exists (select null from et where type = 128) then
	update et set text = 'Transfer issued', ridership = 'N' where type = 128;
else
	insert into et values(128, 'Transfer issued', 'N');
end if;
-- type 129
if exists (select null from et where type = 129) then
	update et set text = 'Special card', ridership = 'Y' where type = 129;
else
	insert into et values(129, 'Special card', 'Y');
end if;
-- type 130
if exists (select null from et where type = 130) then
	update et set text = 'Cashbox code', ridership = 'N' where type = 130;
else
	insert into et values(130, 'Cashbox code', 'N');
end if;
-- type 131
if exists (select null from et where type = 131) then
	update et set text = 'Electronic key code', ridership = 'N' where type = 131;
else
	insert into et values(131, 'Electronic key code', 'N');
end if;
-- type 132
if exists (select null from et where type = 132) then
	update et set text = 'POST', ridership = 'N' where type = 132;
else
	insert into et values(132, 'POST', 'N');
end if;
-- type 133
if exists (select null from et where type = 133) then
	update et set text = 'TTP', ridership = 'N' where type = 133;
else
	insert into et values(133, 'TTP', 'N');
end if;
-- type 134
if exists (select null from et where type = 134) then
	update et set text = 'Revenue status', ridership = 'N' where type = 134;
else
	insert into et values(134, 'Revenue status', 'N');
end if;
-- type 135
if exists (select null from et where type = 135) then
	update et set text = 'Recharge', ridership = 'N' where type = 135;
else
	insert into et values(135, 'Recharge', 'N');
end if;
-- type 136
if exists (select null from et where type = 136) then
	update et set text = 'TRiM diagnostic', ridership = 'N' where type = 136;
else
	insert into et values(136, 'TRiM diagnostic', 'N');
end if;
-- type 137
if exists (select null from et where type = 137) then
	update et set text = 'TRiM bad verify', ridership = 'N' where type = 137;
else
	insert into et values(137, 'TRiM bad verify', 'N');
end if;
-- type 138
if exists (select null from et where type = 138) then
	update et set text = 'GPS latitude/longitude', ridership = 'N' where type = 138;
else
	insert into et values(138, 'GPS latitude/longitude', 'N');
end if;
-- type 139
if exists (select null from et where type = 139) then
	update et set text = 'MetroCard', ridership = 'Y' where type = 139;
else
	insert into et values(139, 'MetroCard', 'Y');
end if;
-- type 140
if exists (select null from et where type = 140) then
	update et set text = 'Coin reject threshold', ridership = 'N' where type = 140;
else
	insert into et values(140, 'Coin reject threshold', 'N');
end if;
-- type 141
if exists (select null from et where type = 141) then
	update et set text = 'TRiM misfeed', ridership = 'N' where type = 141;
else
	insert into et values(141, 'TRiM misfeed', 'N');
end if;
-- type 142
if exists (select null from et where type = 142) then
	update et set text = 'Issue card', ridership = 'N' where type = 142;
else
	insert into et values(142, 'Issue card', 'N');
end if;
-- type 143
if exists (select null from et where type = 143) then
	update et set text = 'Token or ticket', ridership = 'Y' where type = 143;
else
	insert into et values(143, 'Token or ticket', 'Y');
end if;
-- type 144
if exists (select null from et where type = 144) then
	update et set text = 'Bill validator OOS', ridership = 'N' where type = 144;
else
	insert into et values(144, 'Bill validator OOS', 'N');
end if;
-- type 145
if exists (select null from et where type = 145) then
	update et set text = 'Multifare metrocard', ridership = 'Y' where type = 145;
else
	insert into et values(145, 'Multifare metrocard', 'Y');
end if;
-- type 146
if exists (select null from et where type = 146) then
	update et set text = 'GPS diagnostics', ridership = 'N' where type = 146;
else
	insert into et values(146, 'GPS diagnostics', 'N');
end if;
-- type 147
if exists (select null from et where type = 147) then
	update et set text = 'Component firmware version/SN', ridership = 'N' where type = 147;
else
	insert into et values(147, 'Component firmware version/SN', 'N');
end if;
-- type 148
if exists (select null from et where type = 148) then
	update et set text = 'GPS driver status transaction', ridership = 'N' where type = 148;
else
	insert into et values(148, 'GPS driver status transaction', 'N');
end if;
-- type 149
if exists (select null from et where type = 149) then
	update et set text = 'Lid status', ridership = 'N' where type = 149;
else
	insert into et values(149, 'Lid status', 'N');
end if;
-- type 150
if exists (select null from et where type = 150) then
	update et set text = 'GPS time discrepancy', ridership = 'N' where type = 150;
else
	insert into et values(150, 'GPS time discrepancy', 'N');
end if;
-- type 151
if exists (select null from et where type = 151) then
	update et set text = 'Driver login via swipe card', ridership = 'N' where type = 151;
else
	insert into et values(151, 'Driver login via swipe card', 'N');
end if;
-- type 152
if exists (select null from et where type = 152) then
	update et set text = 'Driver login via smart card', ridership = 'N' where type = 152;
else
	insert into et values(152, 'Driver login via smart card', 'N');
end if;
-- type 153
if exists (select null from et where type = 153) then
	update et set text = 'Driver login via card in TRiM', ridership = 'N' where type = 153;
else
	insert into et values(153, 'Driver login via card in TRiM', 'N');
end if;
-- type 154
if exists (select null from et where type = 154) then
	update et set text = 'Driver login via GPS', ridership = 'N' where type = 154;
else
	insert into et values(154, 'Driver login via GPS', 'N');
end if;
-- type 155
if exists (select null from et where type = 155) then
	update et set text = 'Maintenance pass (enable probe)', ridership = 'N' where type = 155;
else
	insert into et values(155, 'Maintenance pass (enable probe)', 'N');
end if;
-- type 156
if exists (select null from et where type = 156) then
	update et set text = 'Smart card diagnostics', ridership = 'N' where type = 156;
else
	insert into et values(156, 'Smart card diagnostics', 'N');
end if;
-- type 157
if exists (select null from et where type = 157) then
	update et set text = 'CTS diagnostics', ridership = 'N' where type = 157;
else
	insert into et values(157, 'CTS diagnostics', 'N');
end if;
-- type 158
if exists (select null from et where type = 158) then
	update et set text = 'Bad driver via keypad', ridership = 'N' where type = 158;
else
	insert into et values(158, 'Bad driver via keypad', 'N');
end if;
-- type 159
if exists (select null from et where type = 159) then
	update et set text = 'Bad driver not via keypad', ridership = 'N' where type = 159;
else
	insert into et values(159, 'Bad driver not via keypad', 'N');
end if;
-- type 160
if exists (select null from et where type = 160) then
	update et set text = 'Bad route', ridership = 'N' where type = 160;
else
	insert into et values(160, 'Bad route', 'N');
end if;
-- type 161
if exists (select null from et where type = 161) then
	update et set text = 'ACS parameter error', ridership = 'N' where type = 161;
else
	insert into et values(161, 'ACS parameter error', 'N');
end if;
-- type 162
if exists (select null from et where type = 162) then
	update et set text = 'ACS card transaction', ridership = 'Y' where type = 162;
else
	insert into et values(162, 'ACS card transaction', 'Y');
end if;
-- type 163
if exists (select null from et where type = 163) then
	update et set text = 'ACS CMJ sale', ridership = 'N' where type = 163;
else
	insert into et values(163, 'ACS CMJ sale', 'N');
end if;
-- type 164
if exists (select null from et where type = 164) then
	update et set text = 'Probe without ACS parameter', ridership = 'N' where type = 164;
else
	insert into et values(164, 'Probe without ACS parameter', 'N');
end if;
-- type 165
if exists (select null from et where type = 165) then
	update et set text = 'Bus number not initialized', ridership = 'N' where type = 165;
else
	insert into et values(165, 'Bus number not initialized', 'N');
end if;
-- type 166
if exists (select null from et where type = 166) then
	update et set text = 'Smart card reader write error', ridership = 'N' where type = 166;
else
	insert into et values(166, 'Smart card reader write error', 'N');
end if;
-- type 167
if exists (select null from et where type = 167) then
	update et set text = 'Smart card reader offline', ridership = 'N' where type = 167;
else
	insert into et values(167, 'Smart card reader offline', 'N');
end if;
-- type 168
if exists (select null from et where type = 168) then
	update et set text = 'TRiM offline/online', ridership = 'N' where type = 168;
else
	insert into et values(168, 'TRiM offline/online', 'N');
end if;
-- type 169
if exists (select null from et where type = 169) then
	update et set text = 'Component state - TRiM', ridership = 'N' where type = 169;
else
	insert into et values(169, 'Component state - TRiM', 'N');
end if;
-- type 170
if exists (select null from et where type = 170) then
	update et set text = 'Component state - Smart Card Rdr', ridership = 'N' where type = 170;
else
	insert into et values(170, 'Component state - Smart Card Rdr', 'N');
end if;
-- type 171
if exists (select null from et where type = 171) then
	update et set text = 'Component state - Cashbox', ridership = 'N' where type = 171;
else
	insert into et values(171, 'Component state - Cashbox', 'N');
end if;
-- type 172
if exists (select null from et where type = 172) then
	update et set text = 'Transaction 172', ridership = 'N' where type = 172;
else
	insert into et values(172, 'Transaction 172', 'N');
end if;
-- type 173
if exists (select null from et where type = 173) then
	update et set text = 'New parameter received by FBX', ridership = 'N' where type = 173;
else
	insert into et values(173, 'New parameter received by FBX', 'N');
end if;
-- type 174
if exists (select null from et where type = 174) then
	update et set text = 'SAM inventory', ridership = 'N' where type = 174;
else
	insert into et values(174, 'SAM inventory', 'N');
end if;
-- type 175
if exists (select null from et where type = 175) then
	update et set text = 'Transaction 175', ridership = 'N' where type = 175;
else
	insert into et values(175, 'Transaction 175', 'N');
end if;
-- type 176
if exists (select null from et where type = 176) then
	update et set text = 'Farebox configuration mode', ridership = 'N' where type = 176;
else
	insert into et values(176, 'Farebox configuration mode', 'N');
end if;
-- type 177
if exists (select null from et where type = 177) then
	update et set text = 'New bus number', ridership = 'N' where type = 177;
else
	insert into et values(177, 'New bus number', 'N');
end if;
-- type 178
if exists (select null from et where type = 178) then
	update et set text = 'Component state - All', ridership = 'N' where type = 178;
else
	insert into et values(178, 'Component state - All', 'N');
end if;
-- type 179
if exists (select null from et where type = 179) then
	update et set text = 'Auto logoff (no activity)', ridership = 'N' where type = 179;
else
	insert into et values(179, 'Auto logoff (no activity)', 'N');
end if;
-- type 180
if exists (select null from et where type = 180) then
	update et set text = 'Bill transport diagnostics', ridership = 'N' where type = 180;
else
	insert into et values(180, 'Bill transport diagnostics', 'N');
end if;
-- type 181
if exists (select null from et where type = 181) then
	update et set text = 'Zone change', ridership = 'N' where type = 181;
else
	insert into et values(181, 'Zone change', 'N');
end if;
-- type 182
if exists (select null from et where type = 182) then
	update et set text = 'CTS DCU status', ridership = 'N' where type = 182;
else
	insert into et values(182, 'CTS DCU status', 'N');
end if;
-- type 183
if exists (select null from et where type = 183) then
	update et set text = 'Gate status', ridership = 'N' where type = 183;
else
	insert into et values(183, 'Gate status', 'N');
end if;
-- type 184
if exists (select null from et where type = 184) then
	update et set text = 'Wheelchair lift status', ridership = 'N' where type = 184;
else
	insert into et values(184, 'Wheelchair lift status', 'N');
end if;
-- type 185
if exists (select null from et where type = 185) then
	update et set text = 'Bad run', ridership = 'N' where type = 185;
else
	insert into et values(185, 'Bad run', 'N');
end if;
-- type 186
if exists (select null from et where type = 186) then
	update et set text = 'Bad trip', ridership = 'N' where type = 186;
else
	insert into et values(186, 'Bad trip', 'N');
end if;
-- type 187
if exists (select null from et where type = 187) then
	update et set text = 'Trip-ID/schedule number change', ridership = 'N' where type = 187;
else
	insert into et values(187, 'Trip-ID/schedule number change', 'N');
end if;
-- type 188
if exists (select null from et where type = 188) then
	update et set text = 'Memory status', ridership = 'N' where type = 188;
else
	insert into et values(188, 'Memory status', 'N');
end if;
-- type 189
if exists (select null from et where type = 189) then
	update et set text = 'ePay autoload add value', ridership = 'N' where type = 189;
else
	insert into et values(189, 'ePay autoload add value', 'N');
end if;
-- type 190
if exists (select null from et where type = 190) then
	update et set text = 'ePay autoload add product', ridership = 'N' where type = 190;
else
	insert into et values(190, 'ePay autoload add product', 'N');
end if;
-- type 191
if exists (select null from et where type = 191) then
	update et set text = 'ePay autoload card property', ridership = 'N' where type = 191;
else
	insert into et values(191, 'ePay autoload card property', 'N');
end if;
-- type 192
if exists (select null from et where type = 192) then
	update et set text = 'Smart card usage - ride', ridership = 'Y' where type = 192;
else
	insert into et values(192, 'Smart card usage - ride', 'Y');
end if;
-- type 193
if exists (select null from et where type = 193) then
	update et set text = 'Smart card usage - value', ridership = 'Y' where type = 193;
else
	insert into et values(193, 'Smart card usage - value', 'Y');
end if;
-- type 194
if exists (select null from et where type = 194) then
	update et set text = 'Smart card usage - period', ridership = 'Y' where type = 194;
else
	insert into et values(194, 'Smart card usage - period', 'Y');
end if;
-- type 195
if exists (select null from et where type = 195) then
	update et set text = 'Smart card usage - transfer', ridership = 'Y' where type = 195;
else
	insert into et values(195, 'Smart card usage - transfer', 'Y');
end if;
-- type 196
if exists (select null from et where type = 196) then
	update et set text = 'Door open via card', ridership = 'N' where type = 196;
else
	insert into et values(196, 'Door open via card', 'N');
end if;
-- type 197
if exists (select null from et where type = 197) then
	update et set text = 'Smart card deduct', ridership = 'N' where type = 197;
else
	insert into et values(197, 'Smart card deduct', 'N');
end if;
-- type 198
if exists (select null from et where type = 198) then
	update et set text = 'Smart card add product', ridership = 'N' where type = 198;
else
	insert into et values(198, 'Smart card add product', 'N');
end if;
-- type 199
if exists (select null from et where type = 199) then
	update et set text = 'Smart card add value', ridership = 'N' where type = 199;
else
	insert into et values(199, 'Smart card add value', 'N');
end if;
-- type 200
if exists (select null from et where type = 200) then
	update et set text = 'ID card', ridership = 'N' where type = 200;
else
	insert into et values(200, 'ID card', 'N');
end if;
-- type 201
if exists (select null from et where type = 201) then
	update et set text = 'Event 201', ridership = 'N' where type = 201;
else
	insert into et values(201, 'Event 201', 'N');
end if;
-- type 202
if exists (select null from et where type = 202) then
	update et set text = 'Event 202', ridership = 'N' where type = 202;
else
	insert into et values(202, 'Event 202', 'N');
end if;
-- type 203
if exists (select null from et where type = 203) then
	update et set text = 'Event 203', ridership = 'N' where type = 203;
else
	insert into et values(203, 'Event 203', 'N');
end if;
-- type 204
if exists (select null from et where type = 204) then
	update et set text = 'Bad card not a bad listed card', ridership = 'N' where type = 204;
else
	insert into et values(204, 'Bad card not a bad listed card', 'N');
end if;
-- type 206
if exists (select null from et where type = 206) then
	update et set text = 'PaygoRide', ridership = 'Y' where type = 206;
else
	insert into et values(206, 'PaygoRide', 'Y');
end if;
-- type 207
if exists (select null from et where type = 207) then
	update et set text = 'Bill Transport', ridership = 'N' where type = 207;
else
	insert into et values(207, 'Bill Transport', 'N');
end if;
-- type 208
if exists (select null from et where type = 208) then
	update et set text = 'Zigbee Info', ridership = 'N' where type = 208;
else
	insert into et values(208, 'Zigbee Info', 'N');
end if;
-- type 209
if exists (select null from et where type = 209) then
	update et set text = 'Validation Transaction', ridership = 'N' where type = 209;
else
	insert into et values(209, 'Validation Transaction', 'N');
end if;
-- type 210
if exists (select null from et where type = 210) then
	update et set text = 'Reload Transaction', ridership = 'N' where type = 210;
else
	insert into et values(210, 'Reload Transaction', 'N');
end if;
-- type 212
if exists (select null from et where type = 212) then
	update et set text = 'Mobile Ticket Transaction', ridership = 'Y' where type = 212;
else
	insert into et values(212, 'Mobile Ticket Transaction', 'Y');
end if;
-- type 221
if exists (select null from et where type = 221) then
	update et set text = 'MasterList', ridership = 'N' where type = 221;
else
	insert into et values(221, 'MasterList', 'N');
end if;
-- type 228
if exists (select null from et where type = 228) then
	update et set text = 'Event', ridership = 'N' where type = 228;
else
	insert into et values(228, 'Event', 'N');
end if;
-- type 300		LG-1384 - Error inserting HCV_IN table in GDS 
if exists (select null from et where type = 300) then
	update et set text = 'HCV validation', ridership = 'N' where type = 300;
else
	insert into et values(300, 'HCV validation', 'N');
end if;


-- GD-2206
if not exists (select null from event_subtype where etype = 0x80) then
	insert into event_subtype values (0x80, 'Clock discrepancy');
end if;
if not exists (select null from event_subtype where etype = 0x81) then
	insert into event_subtype values (0x81, 'Clock failure');
end if;
if not exists (select null from event_subtype where etype = 0x82) then
	insert into event_subtype values (0x82, 'Clock was running on back-up battery');
end if;
if not exists (select null from event_subtype where etype = 0x83) then
	insert into event_subtype values (0x83, 'Cold start');
end if;
if not exists (select null from event_subtype where etype = 0x84) then
	insert into event_subtype values (0x84, 'Memory cleared');
end if;
if not exists (select null from event_subtype where etype = 0x85) then
	insert into event_subtype values (0x85, 'Memory filling past high water mark');
end if;
if not exists (select null from event_subtype where etype = 0x86) then
	insert into event_subtype values (0x86, 'Memory full');
end if;
if not exists (select null from event_subtype where etype = 0x87) then
	insert into event_subtype values (0x87, 'Misread ( non-recoverable )');
end if;
if not exists (select null from event_subtype where etype = 0x88) then
	insert into event_subtype values (0x88, 'Clock Power lost');
end if;
if not exists (select null from event_subtype where etype = 0x89) then
	insert into event_subtype values (0x89, 'IP address was obtained from DHCP server');
end if;
if not exists (select null from event_subtype where etype = 0x8A) then
	insert into event_subtype values (0x8A, 'Bypass on');
end if;
if not exists (select null from event_subtype where etype = 0x8B) then
	insert into event_subtype values (0x8B, 'Bypass on after probe');
end if;
if not exists (select null from event_subtype where etype = 0x8C) then
	insert into event_subtype values (0x8C, 'Bypass off');
end if;
if not exists (select null from event_subtype where etype = 0x8D) then
	insert into event_subtype values (0x8D, 'Cashbox full');
end if;
if not exists (select null from event_subtype where etype = 0x8E) then
	insert into event_subtype values (0x8E, 'Probed, cashbox not removed, has no money');
end if;
if not exists (select null from event_subtype where etype = 0x8F) then
	insert into event_subtype values (0x8F, 'Probed, cashbox not removed, has money');
end if;
if not exists (select null from event_subtype where etype = 0x90) then
	insert into event_subtype values (0x90, 'Door opened authorized');
end if;
if not exists (select null from event_subtype where etype = 0x91) then
	insert into event_subtype values (0x91, 'Door opened unauthorized');
end if;
if not exists (select null from event_subtype where etype = 0x92) then
	insert into event_subtype values (0x92, 'Door/cashbox elapsed time too long');
end if;
if not exists (select null from event_subtype where etype = 0x93) then
	insert into event_subtype values (0x93, 'Door closed');
end if;
if not exists (select null from event_subtype where etype = 0x94) then
	insert into event_subtype values (0x94, 'Portable probe used');
end if;
if not exists (select null from event_subtype where etype = 0x95) then
	insert into event_subtype values (0x95, 'Farebox/TRIM communications lost');
end if;
if not exists (select null from event_subtype where etype = 0x96) then
	insert into event_subtype values (0x96, 'Farebox/TRIM communications restored');
end if;
if not exists (select null from event_subtype where etype = 0x97) then
	insert into event_subtype values (0x97, 'Farebox coin mech failure');
end if;
if not exists (select null from event_subtype where etype = 0x98) then
	insert into event_subtype values (0x98, 'Farebox bill mech failure');
end if;
-- where is 0x99 ?
if not exists (select null from event_subtype where etype = 0x9A) then
	insert into event_subtype values (0x9A, 'Proper driver log off event');
end if;
if not exists (select null from event_subtype where etype = 0x9B) then
	insert into event_subtype values (0x9B, 'Force log off of previous driver, supv etc');
end if;
if not exists (select null from event_subtype where etype = 0x9C) then
	insert into event_subtype values (0x9C, 'Door not open after farebox probed');
end if;
if not exists (select null from event_subtype where etype = 0x9D) then
	insert into event_subtype values (0x9D, 'cashbox removed, authorized');
end if;
if not exists (select null from event_subtype where etype = 0x9E) then
	insert into event_subtype values (0x9E, 'cashbox detected out, unauthorized');
end if;
if not exists (select null from event_subtype where etype = 0x9F) then
	insert into event_subtype values (0x9F, 'cashbox installed');
end if;
if not exists (select null from event_subtype where etype = 0xA0) then
	insert into event_subtype values (0xA0, 'IP address was obtained from DNS condition');
end if;
if not exists (select null from event_subtype where etype = 0xA1) then
	insert into event_subtype values (0xA1, 'status flags to indicate farebox condition');
end if;
-- 0xA1 - status flags to indicate farebox condition
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000001) then
	insert into event_status values (0xA1, 0x00000001, 'dump key active');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000002) then
	insert into event_status values (0xA1, 0x00000002, 'door switch active');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000004) then
	insert into event_status values (0xA1, 0x00000004, 'door latch up');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000008) then
	insert into event_status values (0xA1, 0x00000008, 'door latch down');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000010) then
	insert into event_status values (0xA1, 0x00000010, 'coin in coin return');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000020) then
	insert into event_status values (0xA1, 0x00000020, 'bypass switch active');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000040) then
	insert into event_status values (0xA1, 0x00000040, 'burnin test jumper');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000080) then
	insert into event_status values (0xA1, 0x00000080, 'memory clear jumper');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000100) then
	insert into event_status values (0xA1, 0x00000100, 'spare 1 jumper');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000200) then
	insert into event_status values (0xA1, 0x00000200, 'spare 2 jumper');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000400) then
	insert into event_status values (0xA1, 0x00000400, 'spare 3 jumper');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000800) then
	insert into event_status values (0xA1, 0x00000800, 'cashbox present timer');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00001000) then
	insert into event_status values (0xA1, 0x00001000, 'currently probing');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00002000) then
	insert into event_status values (0xA1, 0x00002000, 'probe just completed');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00004000) then
	insert into event_status values (0xA1, 0x00004000, 'full cashbox');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00008000) then
	insert into event_status values (0xA1, 0x00008000, 'bypass switch was active');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00010000) then
	insert into event_status values (0xA1, 0x00010000, 'invalid configuration');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00020000) then
	insert into event_status values (0xA1, 0x00020000, 'farebox in self test');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00040000) then
	insert into event_status values (0xA1, 0x00040000, 'TRIM comm. offline');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00080000) then
	insert into event_status values (0xA1, 0x00080000, 'invalid fare structure');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00100000) then
	insert into event_status values (0xA1, 0x00100000, 'test set flag');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00200000) then
	insert into event_status values (0xA1, 0x00200000, 'top lid switch active');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00400000) then
	insert into event_status values (0xA1, 0x00400000, 'bill override enabled via driver');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x00800000) then
	insert into event_status values (0xA1, 0x00800000, 'stock empty');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x01000000) then
	insert into event_status values (0xA1, 0x01000000, 'low stock');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x02000000) then
	insert into event_status values (0xA1, 0x02000000, 'trim in bypass');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x04000000) then
	insert into event_status values (0xA1, 0x04000000, 'bill validator oos');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x08000000) then
	insert into event_status values (0xA1, 0x08000000, 'probed with alarm delay');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x10000000) then
	insert into event_status values (0xA1, 0x10000000, 'bill jam detected');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x20000000) then
	insert into event_status values (0xA1, 0x20000000, 'trim card jam detected');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x40000000) then
	insert into event_status values (0xA1, 0x40000000, 'smart card off line');
end if;
if not exists (select null from event_status where etype = 0xA1 and id = 0x80000000) then
	insert into event_status values (0xA1, 0x80000000, 'OCU off line');
end if;
	
-- GD-2555
update scd set
	grp = 0,
	des = 0,
	ttp = 0
where grp > 9;

-- GD-2293
update top (1000) prb_log set
	user_id = 'n/a'
where user_id = '';

while @@rowcount = 1000 loop
	update top (1000) prb_log set
		user_id = 'n/a'
	where user_id = '';
end loop;

/////////////////////////////////////////
Update dba.configurationsData set datavalue = 'Complete' where dataname in('GFIDBASA4');
//////////////////////////////////////////


// these lines must be last ones!
commit;
end;
