-- =================
-- SCRIPT START
-- =================

print 'Script start: ' + convert(varchar(12),GETDATE(),114)

-- ===
-- END
-- ===-- =========
-- FUNCTIONS
-- 
-- $Author: rbecker $
-- $Revision: 1.38 $
-- $Date: 2017-04-05 20:53:05 $
-- =========

set quoted_identifier on
go

set ansi_nulls on
go

set ansi_padding on
go


if object_id('dbo.gfisf_islistname','FN') is not null drop function dbo.gfisf_islistname;
go

------------------------------------------------------------------------
-- gfisf_islistname - check if list name exists (1 - found, case sensitive; 2 - found, case insensitive; 0 - not found)
------------------------------------------------------------------------
create function dbo.gfisf_islistname(@type varchar(16), @class smallint, @name varchar(256)) returns tinyint
begin
   if exists(select null from gfi_lst where type=upper(rtrim(ltrim(@type))) and class=isnull(@class,0) and name=rtrim(ltrim(@name))) return 1
   if exists(select null from gfi_lst where type=upper(rtrim(ltrim(@type))) and class=isnull(@class,0) and upper(name)=upper(rtrim(ltrim(@name)))) return 2

   return 0;
end
go

grant execute on dbo.gfisf_islistname to public
go

if object_id('dbo.gfisf_islistcode','FN') is not null drop function dbo.gfisf_islistcode;
go

------------------------------------------------------------------------
-- gfisf_islistcode - check if list code exists
------------------------------------------------------------------------
create function dbo.gfisf_islistcode(@type varchar(16), @class smallint, @code integer) returns bit
begin
   if exists(select null from gfi_lst where type=upper(rtrim(ltrim(@type))) and class=isnull(@class,0) and code=@code) return 1

   return 0
end
go

grant execute on dbo.gfisf_islistcode to public
go

if object_id('dbo.gfisf_islistitem','FN') is not null drop function dbo.gfisf_islistitem;
go

------------------------------------------------------------------------
-- gfisf_islistitem - check if list item exists regardless of class
------------------------------------------------------------------------
create function dbo.gfisf_islistitem(@type varchar(16), @code integer) returns bit
begin
   if exists(select null from gfi_lst where type=upper(rtrim(ltrim(@type))) and code=@code) return 1

   return 0
end
go

grant execute on dbo.gfisf_islistitem to public
go

if object_id('dbo.gfisf_getlistcode','FN') is not null drop function dbo.gfisf_getlistcode;
go

------------------------------------------------------------------------
-- gfisf_getlistcode - get gfi_lst.code by name
------------------------------------------------------------------------
create function dbo.gfisf_getlistcode(@type varchar(16), @class smallint, @name varchar(256)) returns integer
begin
	declare @li integer

	select @li = min(code)
	from gfi_lst
	where type = upper(rtrim(ltrim(@type)))
	  and class = isnull(@class,0)
	  and upper(rtrim(ltrim(name))) = upper(rtrim(ltrim(@name)));

	return @li;
end
go

grant execute on dbo.gfisf_getlistcode to public
go

if object_id('dbo.fnbitwise','FN') is not null drop function dbo.fnbitwise;
go

------------------------------------------------------------------------
-- fnbitwise - perform bitwise operation
------------------------------------------------------------------------
create function dbo.fnbitwise(@typ char(1), @x int, @y int) returns int
begin
   /* typ - AND (&, A); OR (| O); XOR (^ X) */
   /* x, y - operands */
   return (case when @typ in ('&','a','A') then (@x & @y)
                when @typ in ('|','o','O') then (@x | @y)
                when @typ in ('^','x','X') then (@x ^ @y)
                else 0 end)
end
go

grant execute on dbo.fnbitwise to public
go


if object_id('dbo.ymd','FN') is not null drop function dbo.ymd
go

------------------------------------------------------------------------
-- ymd - generate a date from year, month, and day
------------------------------------------------------------------------
create function dbo.ymd(@yr smallint, @mth smallint, @day smallint) returns datetime
begin
   return convert(datetime,ltrim(str(@mth))+'/'+ltrim(str(@day))+'/'+ltrim(str(@yr)),101);
end
go

grant execute on dbo.ymd to public;
go


if object_id('dbo.fnrdrstr','FN') is not null drop function dbo.fnrdrstr;
go

------------------------------------------------------------------------
-- fnrdrstr - get ridership count formula for a location
------------------------------------------------------------------------
create function dbo.fnrdrstr(@tbl varchar(32), @loc smallint) returns varchar(1024)
begin
   /* tbl - table name to prefix column */
   /* loc - location number */
   declare @fc varchar(16)
   declare @rdr varchar(1024)

   set @rdr=''
   if @tbl<>'' set @tbl=@tbl+'.'

   declare cur_rdr cursor for
      select @tbl+(case when farecell_n=0  then 'fare_c'
                        when farecell_n<10 then 'key'+ltrim(str(farecell_n))
                        when farecell_n=10 then 'keyast'
                        when farecell_n<15 then 'key'+substring('abcd',farecell_n - 10,1)
                        else 'ttp'+ltrim(str(farecell_n - 14)) end)
      from farecell
      where loc_n=@loc and fs_id=(select fs_id from cnf where loc_n=@loc) and fareset_n=1 and included_f='Y'
      order by farecell_n for read only

   open cur_rdr
   fetch next from cur_rdr into @fc
   while @@fetch_status=0
   begin
      if @rdr<>''
         set @rdr=@rdr+'+'+@fc
      else
         set @rdr=@fc

      fetch next from cur_rdr into @fc
   end

   close cur_rdr

   if @rdr='' set @rdr='0'
      
   return @rdr;
end
go

grant execute on dbo.fnrdrstr to public;
go


if object_id('dbo.fnrdrstr2','FN') is not null drop function dbo.fnrdrstr2;
go

------------------------------------------------------------------------
-- fnrdrstr2 - get ridership count formula for a location (version 2)
------------------------------------------------------------------------
create function dbo.fnrdrstr2(@tbl varchar(32), @loc smallint) returns varchar(8000)
begin
/*
select dbo.fnrdrstr2('', 1);
*/
   /* tbl - table name to prefix column */
   /* loc - location number */
   declare @fc varchar(16)
   declare @rdr varchar(8000)

   set @rdr=''
   if @tbl<>'' set @tbl=@tbl+'.'

   declare cur_rdr cursor for
      select case when farecell_n=0  then 'fare_c'
                  when farecell_n<10 then 'key'+ltrim(str(farecell_n))
                  when farecell_n=10 then 'keyast'
                  when farecell_n<15 then 'key'+substring('abcd',farecell_n - 10,1)
                  else 'ttp'+ltrim(str(farecell_n - 14))
             end
      from farecell
      where loc_n=@loc and fs_id=(select fs_id from cnf where loc_n=@loc) and fareset_n=1 and included_f='Y'
      order by farecell_n for read only

   open cur_rdr
   fetch next from cur_rdr into @fc
   while @@fetch_status=0
   begin
      if @rdr <> ''
         set @rdr = @rdr + '+';

      set @fc = 'isnull(' + @tbl + @fc + ',0)';
      
      set @rdr = @rdr + @fc;

      fetch next from cur_rdr into @fc
   end

   close cur_rdr

   if @rdr='' set @rdr='0'
      
   return @rdr;
end
go

grant execute on dbo.fnrdrstr2 to public;
go


if object_id('dbo.fnttpstr','FN') is not null drop function dbo.fnttpstr;
go

------------------------------------------------------------------------
-- fnttpstr - get token, ticket, or pass count formula for a location
------------------------------------------------------------------------
create function dbo.fnttpstr(@tbl varchar(32), @typ smallint, @loc smallint) returns varchar(1024)
begin
   /* tbl - table name to prefix column */
   /* typ - 1: token; 2: ticket; 3: pass; anything else: return '0' */
   /* loc - location number */
   declare @m varchar(16)
   declare @ttp varchar(1024)

   set @ttp=''
   if @tbl<>'' set @tbl=@tbl+'.'

   declare cur_ttp cursor for
      select @tbl+'ttp'+ltrim(str(m_ndx))
      from media
      where loc_n=@loc and fs_id=(select fs_id from cnf where loc_n=@loc) and m_ndx>0
        and ((@typ=1 and grp=5 and des in (0,1,2,3,4,26)) or (@typ=2 and grp=5 AND des>=5 AND des<>26) or
             (@typ=3 and (grp in (1,2,3,4,7) or (grp=0 and (des>0 or ltrim(rtrim(text))<>'' or ltrim(rtrim(description))<>'')))))
      order by m_ndx for read only

   open cur_ttp
   fetch next from cur_ttp into @m
   while @@fetch_status=0
   begin
      if @ttp<>''
         set @ttp=@ttp+'+'+@m
      else
         set @ttp=@m

      fetch next from cur_ttp into @m
   end

   close cur_ttp

   if @ttp='' set @ttp='0'
      
   return @ttp;
end
go

grant execute on dbo.fnttpstr to public;
go


if object_id('dbo.fnttpstr2','FN') is not null drop function dbo.fnttpstr2;
go

------------------------------------------------------------------------
-- fnttpstr2 -	get token, ticket, or pass count formula for a location using extended TTPs
--				tbl - table name to prefix column
--				typ - 1: token; 2: ticket; 3: pass; anything else: return '0'
--				loc - location number
-- 
-- LG-1006			fnrdrstr and fnttpstr functions uses larger fare structure, but the statements which use them are not ready for it
-- GD-2451			updated code to look at attribute field when calculating pass_c value. Per Phil
------------------------------------------------------------------------
create function dbo.fnttpstr2(@tbl varchar(32), @typ smallint, @loc smallint) returns varchar(8000)
begin
/*
select dbo.fnttpstr2('', 3, 1);
*/
	declare @m varchar(50);
	declare @ttp varchar(8000);

	set @ttp = '';
	if @tbl <> '' set @tbl = @tbl + '.';

	declare cur_ttp cursor for
		select distinct
			'ttp' + ltrim(str(m.m_ndx))
		from media as m
		inner join farecell as fc
			on	m.loc_n = fc.loc_n
			and	m.fs_id = fc.fs_id
			and	m.m_ndx = fc.m_ndx
		inner join attr
			on	fc.attr = attr.attr
		where	m.loc_n = @loc
			and	m.fs_id = (select fs_id from cnf where loc_n = @loc)
			and	m.m_ndx > 0
			and (	@typ = 1 and grp = 5 and des in (0, 1, 2, 3, 4, 26)
				or	@typ = 2 and grp = 5 and des >= 5 and des <> 26
				or	@typ = 3 and	(	grp in (1, 2, 3, 4, 7)
									or	grp = 0 and des > 0 and attr.attr = 0
									)
				)
		order by 1 for read only
	;

	open cur_ttp;

	fetch next from cur_ttp into @m;

	while @@fetch_status = 0
	begin
		if @ttp <> ''
			set @ttp = @ttp + '+';

		set @m = 'isnull(' + @tbl + @m + ',0)';

		set @ttp = @ttp + @m;

		fetch next from cur_ttp into @m;
	end

	close cur_ttp;

	if @ttp = '' set @ttp = '0';
      
	return @ttp;
end
go

grant execute on dbo.fnttpstr2 to public;
go


if object_id('dbo.fnintime','FN') is not null drop function dbo.fnintime
go

------------------------------------------------------------------------
-- fnintime - perform time range comparison
------------------------------------------------------------------------
create function dbo.fnintime(@ts datetime, @tm1 datetime, @tm2 datetime) returns smallint
begin
   /* ts - timestamp to check whether it is within tm1 and tm2 */
   /* tm1, tm2 - time range (due to lack of time data type in MSSQL2005, datetime data type is used) */
   declare @rc smallint

   if datepart(hh,@ts)*10000.0+datepart(mi,@ts)*100.0+datepart(ss,@ts)*1.0 between
      datepart(hh,@tm1)*10000.0+datepart(mi,@tm1)*100.0+datepart(ss,@tm1)*1.0 and
      datepart(hh,@tm2)*10000.0+datepart(mi,@tm2)*100.0+datepart(ss,@tm2)*1.0
      set @rc=1                                   /* within time range */
   else
      set @rc= -1                                 /* out of time range */

   return @rc
end
go

grant execute on dbo.fnintime to public
go

if object_id('dbo.fnchkrrt','FN') is not null drop function dbo.fnchkrrt
go

------------------------------------------------------------------------
-- fnchkrrt - perform Route-Run-Trip (RRT) validation
------------------------------------------------------------------------
create function dbo.fnchkrrt(@loc smallint, @rte integer, @run integer, @trp integer, @ts datetime) returns tinyint
begin
   /* loc - location number */
   /* rte, run, trp - route, run, and trip to be validated */
   /* ts - time to be checked */
   declare @rc tinyint

   if exists(select 0 from rrtlst where loc_n=@loc and route=@rte and run=@run and trip=@trp and dbo.fnintime(@ts,tm1,tm2)=1 and active=1)
      set @rc=1                                   /* RRT is valid or defined */
   else
      set @rc=0                                   /* RRT is invalid or not defined */

   return @rc
end
go

grant execute on dbo.fnchkrrt to public
go

if object_id('dbo.gfisf_getlistname','FN') is not null drop function dbo.gfisf_getlistname
go

------------------------------------------------------------------------
-- gfisf_getlistname - get gfi_lst.name
------------------------------------------------------------------------
create function dbo.gfisf_getlistname(@list_type varchar(16), @list_class smallint, @list_code int) returns varchar(256)
begin
   declare @list_name varchar(256)

   set @list_type=upper(ltrim(rtrim(@list_type)))
   set @list_name=null

   if @list_type<>'' and (@list_code is not null)
   begin
      if @list_class is null
      begin
         select @list_class=min(class) from gfi_lst where type=@list_type and code=@list_code
         if @@rowcount<>1 return @list_name
      end

      select @list_name=name from gfi_lst where type=@list_type and class=@list_class and code=@list_code
   end

   return @list_name
end
go

grant execute on dbo.gfisf_getlistname to public
go

if object_id('dbo.gfisf_getlistvalue','FN') is not null drop function dbo.gfisf_getlistvalue
go

------------------------------------------------------------------------
-- gfisf_getlistvalue - get gfi_lst.value by name
------------------------------------------------------------------------
create function dbo.gfisf_getlistvalue(@list_type varchar(16), @list_class smallint, @list_name varchar(256)) returns varchar(4000)
begin
   declare @list_value varchar(4000)
   declare @list_code  int

   set @list_type=upper(ltrim(rtrim(@list_type)))
   set @list_value=null

   if @list_type<>'' and ltrim(rtrim(@list_name))<>''
   begin
      if @list_class is null
      begin
         select @list_class=min(class) from gfi_lst where type=@list_type and name=@list_name
         if @@rowcount<>1 return @list_value
      end

      select @list_code=min(code) from gfi_lst where type=@list_type and class=@list_class and name=@list_name
      if @@rowcount=1
         select @list_value=value from gfi_lst where type=@list_type and class=@list_class and code=@list_code
   end

   return @list_value
end
go

grant execute on dbo.gfisf_getlistvalue to public
go

if object_id('dbo.gfisf_getlistvalue2','FN') is not null drop function dbo.gfisf_getlistvalue2
go

------------------------------------------------------------------------
-- gfisf_getlistvalue2 - get gfi_lst.value by code
------------------------------------------------------------------------
create function dbo.gfisf_getlistvalue2(@list_type varchar(16), @list_class smallint, @list_code int) returns varchar(4000)
begin
   declare @list_value varchar(4000)

   set @list_type=upper(ltrim(rtrim(@list_type)))
   set @list_value=null

   if @list_type<>'' and (@list_code is not null)
   begin
      if @list_class is null
      begin
         select @list_class=min(class) from gfi_lst where type=@list_type and code=@list_code
         if @@rowcount<>1 return @list_value
      end
      select @list_value=value from gfi_lst where type=@list_type and class=@list_class and code=@list_code
   end

   return @list_value
end
go

grant execute on dbo.gfisf_getlistvalue2 to public
go

if object_id('dbo.gfisf_getlastmlid','FN') is not null drop function dbo.gfisf_getlastmlid
go

------------------------------------------------------------------------
-- gfisf_getlastmlid - get last probing ml.id
------------------------------------------------------------------------
create function dbo.gfisf_getlastmlid(@loc smallint, @mlid integer) returns integer
begin
   declare @lastid integer

   set @lastid=null

   select top 1 @lastid=m1.id from ml m1, ml m2
   where m1.loc_n=@loc and m2.loc_n=@loc and m2.id=@mlid and m1.bus=m2.bus and m1.ts<=m2.ts and m1.id<>@mlid
   order by m1.ts desc

   return @lastid
end
go

grant execute on dbo.gfisf_getlastmlid to public
go

if object_id('dbo.gfisf_getlastmlts','FN') is not null drop function dbo.gfisf_getlastmlts
go

------------------------------------------------------------------------
-- gfisf_getlastmlts - get last probing ml.ts
------------------------------------------------------------------------
create function dbo.gfisf_getlastmlts(@loc smallint, @mlid integer) returns datetime
begin
   declare @lastts datetime

   select @lastts=max(m1.ts) from ml m1, ml m2
   where m1.loc_n=@loc and m2.loc_n=@loc and m2.id=@mlid and m1.bus=m2.bus and m1.ts<=m2.ts and m1.id<>@mlid

   return @lastts
end
go

grant execute on dbo.gfisf_getlastmlts to public
go

if object_id('dbo.gfisf_iseqvalid','FN') is not null drop function dbo.gfisf_iseqvalid;
go

------------------------------------------------------------------------
-- gfisf_iseqvalid - check if an equipment is defined
------------------------------------------------------------------------
create function dbo.gfisf_iseqvalid(@eqt tinyint, @eqn smallint) returns bit
begin
	if exists(select 0 from gfi_eq where eq_type=@eqt and eq_n=@eqn) return 1;

	return 0
end
go

grant execute on dbo.gfisf_iseqvalid to public;
go

if object_id('dbo.gfisf_seconds','FN') is not null drop function dbo.gfisf_seconds
go

------------------------------------------------------------------------
-- gfisf_seconds - get difference in seconds between two datetimes
------------------------------------------------------------------------
create function dbo.gfisf_seconds(@ts1 datetime, @ts2 datetime) returns integer
begin
   return datediff(second,@ts1,@ts2)
end
go

grant execute on dbo.gfisf_seconds to public
go

if object_id('dbo.gfisf_getxcnt','FN') is not null drop function dbo.gfisf_getxcnt
go

------------------------------------------------------------------------
-- gfisf_getxcnt - get invalid route/run/trip/driver count per probing
--                 @loc: location number
--                 @mlid: master list record ID
--                 @type: exception type (1 - driver; 2 - route; 3 - run; 4 - trip; 0 - all)
------------------------------------------------------------------------
create function dbo.gfisf_getxcnt(@loc smallint, @mlid integer, @type tinyint) returns integer
begin
   declare @cnt integer
   declare @li  integer

   set @cnt=0
   if @type>=0 and @type<=4 and exists(select 0 from ev where loc_n=@loc and id=@mlid and (curr_r>0 or rdr_c>0))
   begin
      if @type in (0, 1)                         -- driver
      begin
         select @li=count(*) from ev
         where loc_n=@loc and id=@mlid and (curr_r>0 or rdr_c>0) and drv not in (select drv from drvlst where loc_n=@loc)
           and (not exists(select 0 from gfi_range where loc_n=@loc and type=1 and drv between v1 and v2))
         if @li>0 set @cnt=@cnt+@li
      end

      if @type in (0, 2)                         -- route
      begin
         select @li=count(*) from ev
         where loc_n=@loc and id=@mlid and (curr_r>0 or rdr_c>0) and route not in (select route from rtelst where loc_n=@loc)
           and (not exists(select 0 from gfi_range where loc_n=@loc and type=2 and route between v1 and v2))
         if @li>0 set @cnt=@cnt+@li
      end

      if @type in (0, 3)                         -- run
      begin
         select @li=count(*) from ev
         where loc_n=@loc and id=@mlid and (curr_r>0 or rdr_c>0) and run not in (select run from runlst where loc_n=@loc)
           and (not exists(select 0 from gfi_range where loc_n=@loc and type=3 and run between v1 and v2))
         if @li>0 set @cnt=@cnt+@li
      end

      if @type in (0, 4)                         -- trip
      begin
         select @li=count(*) from ev
         where loc_n=@loc and id=@mlid and (curr_r>0 or rdr_c>0) and trip not in (select trip from trplst where loc_n=@loc)
           and (not exists(select 0 from gfi_range where loc_n=@loc and type=4 and trip between v1 and v2))
         if @li>0 set @cnt=@cnt+@li
      end
   end

   return @cnt
end
go

grant execute on dbo.gfisf_getxcnt to public
go

if object_id('dbo.gfisf_calcsn','FN') is not null drop function dbo.gfisf_calcsn;
go

------------------------------------------------------------------------
-- gfisf_calcsn - calculate serial number		NOT USED
------------------------------------------------------------------------
create function dbo.gfisf_calcsn(@grp tinyint, @des tinyint, @aid smallint, @sc tinyint, @mid tinyint, @tpbc smallint, @seq integer) returns bigint
begin
	return cast(((@grp & 3)*32+(@des & 31))*72057594037927936+(@seq & 16777215)*4294967296+((@sc & 15)*4096+(@aid & 4095))*65536+(@mid & 15)*4096+(@tpbc & 4095) as bigint);
end;
go

grant execute on dbo.gfisf_calcsn to public;
go

if object_id('dbo.gfisf_isvalid','FN') is not null drop function dbo.gfisf_isvalid
go

------------------------------------------------------------------------
-- gfisf_isvalid - check validity of route/run/trip/driver
--                 @loc: location number
--                 @type: item type (1 - driver; 2 - route; 3 - run; 4 - trip)
--                 @item: item value
--
-- Return: 1 - valid; 0 - invalid
------------------------------------------------------------------------
create function dbo.gfisf_isvalid(@loc smallint, @type tinyint, @item integer) returns bit
begin
   if exists(select 0 from gfi_range where loc_n=@loc and type=@type and @item between v1 and v2) return 1

   if @type=1
   begin
      if exists(select 0 from drvlst where loc_n=@loc and drv=@item) return 1
   end
   else if @type=2
   begin
      if exists(select 0 from rtelst where loc_n=@loc and route=@item) return 1
   end
   else if @type=3
   begin
      if exists(select 0 from runlst where loc_n=@loc and run=@item) return 1
   end
   else if @type=4
   begin
      if exists(select 0 from trplst where loc_n=@loc and trip=@item) return 1
   end

   return 0
end
go

grant execute on dbo.gfisf_isvalid to public
go

if object_id('dbo.gfisf_istrue','FN') is not null drop function dbo.gfisf_istrue
go

------------------------------------------------------------------------
-- gfisf_istrue - check if a list value is true
------------------------------------------------------------------------
create function dbo.gfisf_istrue(@list_type varchar(16), @list_class smallint, @list_name varchar(256)) returns bit
begin
   if upper(ltrim(rtrim(dbo.gfisf_getlistvalue(@list_type,@list_class,@list_name)))) in ('TRUE','T','YES','Y','1') return 1

   return 0
end
go

grant execute on dbo.gfisf_istrue to public
go

if object_id('dbo.gfisf_isfalse','FN') is not null drop function dbo.gfisf_isfalse
go

------------------------------------------------------------------------
-- gfisf_isfalse - check if a list value is false
------------------------------------------------------------------------
create function dbo.gfisf_isfalse(@list_type varchar(16), @list_class smallint, @list_name varchar(256)) returns bit
begin
   if upper(ltrim(rtrim(dbo.gfisf_getlistvalue(@list_type,@list_class,@list_name)))) in ('FALSE','F','NO','N','0') return 1

   return 0
end
go

grant execute on dbo.gfisf_isfalse to public
go

if object_id('dbo.gfisf_removenonnumchar','FN') is not null drop function dbo.gfisf_removenonnumchar
go

------------------------------------------------------------------------
-- gfisf_removenonnumchar - remove non-numeric characters from a string
------------------------------------------------------------------------
create function dbo.gfisf_removenonnumchar(@str varchar(4000)) returns varchar(4000)
begin
   declare @li integer

   set @li=patindex('%[^0-9]%',@str)
   while @li>0
   begin
      set @str=stuff(@str,@li,1,'')
      set @li=patindex('%[^0-9]%',@str)
   end

   return @str
end
go

grant execute on dbo.gfisf_removenonnumchar to public
go

if object_id('dbo.gfisf_getdistance','FN') is not null drop function dbo.gfisf_getdistance
go

------------------------------------------------------------------------
-- gfisf_getdistance - calculate distance between two coordinates in meters using haversine algorithm
------------------------------------------------------------------------
create function dbo.gfisf_getdistance(@lat1 float, @long1 float, @lat2 float, @long2 float) returns float
begin
   declare @dist float

   set @lat1=power(sin(radians(@lat2 - @lat1)/2.0),2)+
            (cos(radians(@lat1))*cos(radians(@lat2))*power(sin(radians(@long2 - @long1)/2.0),2))

   set @dist=6371007.2*2.0*atn2(sqrt(@lat1),sqrt(1.0 - @lat1))

   return @dist
end
go

grant execute on dbo.gfisf_getdistance to public
go

if object_id('dbo.gfisf_gethostip','FN') is not null drop function dbo.gfisf_gethostip
go

------------------------------------------------------------------------
-- gfisf_gethostip - retrieve client hostname+' '+ip for the current connection
------------------------------------------------------------------------
create function dbo.gfisf_gethostip() returns varchar(32)
begin
   declare @hostip varchar(32)

   select @hostip=(case when len(ltrim(host_name))>0 then substring(ltrim(host_name),1,31 - len(client_net_address))+' '+client_net_address
                        else client_net_address end)
   from sys.dm_exec_sessions a, sys.dm_exec_connections b
   where a.session_id=@@spid and a.session_id=b.session_id

   return @hostip
end
go

grant execute on dbo.gfisf_gethostip to public
go

if object_id('dbo.gfisf_get_pwd_hash','FN') is not null drop function dbo.gfisf_get_pwd_hash
go

------------------------------------------------------------------------
-- gfisf_get_pwd_hash - generate password hash value (return hash is a base64 encoded string)
------------------------------------------------------------------------
create function dbo.gfisf_get_pwd_hash(@pwd varchar(64), @salt varchar(50)) returns varchar(40)
begin
   declare @bin  varbinary(max)
   declare @n    smallint
   declare @hash varchar(40)

   if len(@pwd)>0
   begin
      set @bin=cast(@pwd as varbinary(max))
      if len(@salt)>0 set @bin=cast(@salt as varbinary(max))+@bin

      set @n=0

      while @n<=1000
         select @bin=hashbytes('sha1',@bin), @n=@n+1

      set @hash=cast(N'' as xml).value('xs:base64Binary(xs:hexBinary(sql:variable("@bin")))','varchar(max)')
   end
   else
      select @hash=@pwd

   return @hash
end
go

grant execute on dbo.gfisf_get_pwd_hash to public
go


if object_id('dbo.gfisf_convert_eid','FN') is not null drop function dbo.gfisf_convert_eid;
go

------------------------------------------------------------------------
-- gfisf_convert_eid - convert 7-byte card_eid to hex using little endian format
------------------------------------------------------------------------
create function dbo.gfisf_convert_eid(@card_eid bigint) returns varchar(20)
begin
/* example of usage
	select dbo.gfisf_convert_eid(1194564535001728);
	select dbo.gfisf_convert_eid(0);
	select dbo.gfisf_convert_eid(1);

	select iv.card_pid
	     , iv.card_eid
		 , calc_hex = dbo.gfisf_convert_eid (iv.card_eid)
		 , iv.card_eid_hex
	from gfi_epay_card_inv as iv
	where dbo.gfisf_convert_eid (iv.card_eid) <> iv.card_eid_hex;
*/
	declare @rc     smallint;
	declare @hex    varchar(20);
	declare @eid    bigint;

	set @hex = '';
	set @eid = @card_eid;

	if isnull(@eid, 0) <= 0 return null;

	while @eid > 0
	begin
		set @rc = @eid % 256;
		set @eid = @eid / 256;

		set @hex = @hex + substring('0123456789ABCDEF', @rc / 16 + 1, 1)
		                + substring('0123456789ABCDEF', @rc % 16 + 1, 1);
                           
	end

	while len(@hex) < 14
	begin
	  set @hex = '0' + @hex;
	end

	return @hex;
end
go

grant execute on dbo.gfisf_convert_eid to public;
go

if object_id('dbo.gfisf_epay_get_fare_id','FN') is not null drop function dbo.gfisf_epay_get_fare_id
go

------------------------------------------------------------------------
--
-- gfisf_epay_get_fare_id - get e-Fare fare ID based on input info
--
-- Arguments: @prod_type - product type (1 - period; 2 - ride; 3 - value; etc.)
--            @des       - designator
--            @card_type - card type (1 - DESFire; 2 - LUCC; etc.)
--            @value     - product value
--
-- Return:  0 - fare ID not found
--         >0 - fare ID found
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_get_fare_id(@prod_type tinyint, @des tinyint, @card_type tinyint, @value integer) returns smallint
begin
	declare @fare_id smallint

	if @value>0
		select @fare_id=fare_id
		from (select fare_id,
			  row_number() over (order by active desc,
								 (case when des=@des then 0 else 1 end),
								 (case when (flags % 2)=(case @card_type when 2 then 1 else 0 end) then 0 else 1 end),
								 (case when value=@value then 0 else 1 end),
								 abs(value - @value)) r
			  from gfi_epay_fare where fare_id>0 and grp=@prod_type and (des=@des or @des=0)) f
		where r=1
	else
		select @fare_id=fare_id
		from (select fare_id,
			  row_number() over (order by active desc,
								 (case when (flags % 2)=(case @card_type when 2 then 1 else 0 end) then 0 else 1 end),
								 value) r
			  from gfi_epay_fare where fare_id>0 and grp=@prod_type and des=@des) f
		where r=1

	return isnull(@fare_id, 0);
end
go

grant execute on dbo.gfisf_epay_get_fare_id to public
go

if object_id('dbo.gfisf_epay_get_fare_id2','FN') is not null drop function dbo.gfisf_epay_get_fare_id2
go

------------------------------------------------------------------------ 
-- 
-- gfisf_epay_get_fare_id2 - get e-Fare fare ID based on input info with start date/time 
-- 
-- Arguments: @prod_type - product type (1 - period; 2 - ride; 3 - value; etc.) 
--            @des       - designator 
--            @card_type - card type (1 - DESFire; 2 - LUCC; etc.) 
--            @value     - product value 
--            @start_ts  - start date/time 
-- 
-- Return:  0 - fare ID not found 
--         >0 - fare ID found 
--March 20,2017/JIRA - DR# 221/Sruthi M
--
--
------------------------------------------------------------------------ 
CREATE FUNCTION [dbo].[Gfisf_epay_get_fare_id2](@prod_type TINYINT, 
                                            @des       TINYINT, 
                                            @card_type TINYINT, 
                                            @value     INTEGER, 
                                            @start_ts  DATETIME) 
returns SMALLINT 
  BEGIN 
      /* 
      select epay_get_fare_id2 = dbo.gfisf_epay_get_fare_id2(1, 1, 1, 1, null); 
      select epay_get_fare_id2 = dbo.gfisf_epay_get_fare_id2(1, 1, 1, 1, '2015-06-04'); 
      select epay_get_fare_id2 = dbo.gfisf_epay_get_fare_id2(1, 14, 1, 30, '2015-06-04'); 
      */ 
      IF @start_ts IS NULL 
        BEGIN 
            RETURN dbo.Gfisf_epay_get_fare_id(@prod_type, @des, @card_type, 
                   @value 
                   ); 
        END 

      DECLARE @fare_id SMALLINT; 

      IF @value > 0 
        SELECT @fare_id = fare_id 
        FROM   (SELECT fare_id, 
                       Row_number() 
                         OVER ( 
                           ORDER BY active DESC, (CASE WHEN des=@des THEN 0 ELSE 
                         1 
                         END 
                         ), 
                         (CASE 
                         WHEN ( 
                         flags % 2)=(CASE @card_type WHEN 2 THEN 1 ELSE 0 END) 
                         THEN 0 
                         ELSE 1 
                         END), ( 
                         CASE WHEN value=@value THEN 0 ELSE 1 END), Abs(value 
                         - @value 
                         ), 
                         start_ts, 
                         fare_id) AS r 
                FROM   gfi_epay_fare 
                WHERE  fare_id > 0 
                       AND grp = @prod_type 
                       AND ( des = @des 
                              OR @des = 0 ) 
                       AND 
                       --isnull(exp_ts, dateadd(day, 1, @start_ts)) > @start_ts /**Commented on March 20th, replaced with case statement JIRA - DR# 221
                       CASE 
                         WHEN exp_ts is NULL THEN Dateadd(day, 1, @start_ts) 
                         WHEN exp_ts < '1/1/1980' THEN 
                         Dateadd(day, 1, @start_ts) 
                         ELSE exp_ts 
                       END > @start_ts) AS f 
        WHERE  r = 1; 
      ELSE 
        SELECT @fare_id = fare_id 
        FROM   (SELECT fare_id, 
                       Row_number() 
                         OVER ( 
                           ORDER BY active DESC, (CASE WHEN (flags % 2)=(CASE 
                         @card_type 
                         WHEN 2 
                         THEN 1 
                         ELSE 0 END) THEN 0 ELSE 1 END), value, start_ts, 
                         fare_id) 
                       AS 
                       r 
                FROM   gfi_epay_fare 
                WHERE  fare_id > 0 
                       AND grp = @prod_type 
                       AND des = @des 
                       -- and isnull(exp_ts, dateadd(day, 1, @start_ts)) > @start_ts /**Commented on March 20th, replaced with case statement JIRA - DR# 221
                       AND CASE 
                             WHEN exp_ts is NULL THEN Dateadd(day, 1, @start_ts) 
                             WHEN exp_ts < '1/1/1980' THEN 
                             Dateadd(day, 1, @start_ts) 
                             ELSE exp_ts 
                           END > @start_ts) AS f 
        WHERE  r = 1; 

      RETURN Isnull(@fare_id, 0); 
  END 
go

grant execute on dbo.gfisf_epay_get_fare_id2 to public
go

if object_id('dbo.gfisf_epay_user_id','FN') is not null drop function dbo.gfisf_epay_user_id
go

------------------------------------------------------------------------
--
-- gfisf_epay_user_id - get user ID from acct_n2 (third party account number)
--			
-- Return:  0 - not found
--         >0 - user ID
-- Last modified by Marina DAT-210		 
-- 
------------------------------------------------------------------------
create function dbo.gfisf_epay_user_id(@acct_n2 integer) returns integer
begin
    return  (select isnull(max(user_id),0) from gfi_epay_usr u where acct_n2=@acct_n2 and user_id>0 and 
			  not exists (select 0 from gfi_epay_inst_usr iu where u.user_id = iu.user_id)) 
end
go

grant execute on dbo.gfisf_epay_user_id to public
go

if object_id('dbo.gfisf_GetAutoloadInactiveDays','FN') is not null drop function dbo.gfisf_GetAutoloadInactiveDays;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 05/13/2015
-- Description:	Get Autoload Inactive Days
-- =============================================
create function dbo.gfisf_GetAutoloadInactiveDays()
returns int
as
begin
/*
select AutoloadInactiveDays = dbo.gfisf_GetAutoloadInactiveDays();
*/
	declare @result int;

	select @result = CAST(d.dataValue as int)
	from configurations as c
	inner join configurationsData as d
	        on d.configurationID = c.id
	where c.id = 1 -- Autoload
	  and c.isActive = 1
	  and d.dataName = 'InactiveDays'
	  and d.isActive = 1;

	if (isnull(@result, 0) <= 0)
	begin
		set @result = 36500; -- 100 years ago
	end

	return @result;
end
go

grant execute on dbo.gfisf_GetAutoloadInactiveDays to public;
go

if object_id('dbo.gfisf_GetIndividualAutoLoadSize','FN') is not null drop function dbo.gfisf_GetIndividualAutoLoadSize;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 05/13/2015
-- Description:	Get Individual Autoload Size
-- =============================================
create function dbo.gfisf_GetIndividualAutoLoadSize()
returns int
as
begin
/*
select IndividualAutoLoadSize = dbo.gfisf_GetIndividualAutoLoadSize();
*/
	declare @result int;

	select @result = CAST(d.dataValue as int)
	from configurations as c
	inner join configurationsData as d
	        on d.configurationID = c.id
	where c.id = 1 -- Autoload
	  and c.isActive = 1
	  and d.dataName = 'IndividualAutoLoadSize'
	  and d.isActive = 1;

	if (isnull(@result, 0) <= 0)
	begin
		set @result = 2000000000; -- 2GB
	end

	return @result;
end
go

grant execute on dbo.gfisf_GetIndividualAutoLoadSize to public;
go

if object_id('dbo.gfisf_GetIndividualAutoLoadRecordSize','FN') is not null drop function dbo.gfisf_GetIndividualAutoLoadRecordSize;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 06/23/2015
-- Description:	Get Individual Autoload Record Size
-- =============================================
create function dbo.gfisf_GetIndividualAutoLoadRecordSize(@load_type tinyint)
returns int
as
begin
/*
select IndividualAutoLoadRecordSize = dbo.gfisf_GetIndividualAutoLoadRecordSize(0);
*/
	declare @result int;

	set @result = case @load_type
	                   when  1 then 18
			           when  2 then 36
			           when  3 then 18
			           when  4 then 18
			           when  5 then 18
			           when  6 then 18
			           when 13 then 36
			           else         36 /* max posible for other */ 
	              end;

	return @result;
end
go

grant execute on dbo.gfisf_GetIndividualAutoLoadRecordSize to public;
go

if object_id('dbo.gfisf_GetNetworkManagerQAVersion','FN') is not null drop function dbo.gfisf_GetNetworkManagerQAVersion;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 06/01/2015
-- Description:	Get Network Manager QA Version
-- =============================================
create function dbo.gfisf_GetNetworkManagerQAVersion()
returns varchar(100)
as
begin
/*
select NetworkManagerQAVersion = dbo.gfisf_GetNetworkManagerQAVersion();
*/
	declare @result varchar(100);

	select @result = dataValue
	from configurationsData
	where configurationID = 2 -- Network Manager
	  and dataName = 'QA Release'
	  and isActive = 1;

	if @result is null
	begin
		set @result = ''; -- default value
	end

	return @result;
end
go

grant execute on dbo.gfisf_GetNetworkManagerQAVersion to public;
go

if object_id('dbo.gfisf_epay_getname','FN') is not null drop function dbo.gfisf_epay_getname
go

------------------------------------------------------------------------
--
-- gfisf_epay_getname - construct name string
--
-- Arguments: @user_id    - user ID
--            @show_login - 1: show login ID after name; 0: do not show login ID
--            @show_uid   - 1: show user ID after name; 0: do not show user ID
--
-- Return: user name; if user name is not found, return user ID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_getname(@user_id integer, @show_login bit, @show_uid bit) returns varchar(256)
begin
   declare @fn  varchar(256)
   declare @ln  varchar(64)
   declare @id  varchar(64)

   select @fn=ltrim(rtrim(fname)), @ln=ltrim(rtrim(lname)), @id=ltrim(rtrim(login_id))
   from gfi_epay_usr u, gfi_epay_contact c
   where u.contact_id=c.contact_id and user_id=@user_id

   if len(@fn)>0
   begin
      if len(@ln)>0 set @fn=@fn+' '+@ln
   end
   else if len(@ln)>0
      set @fn=@ln
   else
      return ltrim(@user_id)

   if @show_uid=1 set @fn=@fn+' ('+ltrim(@user_id)

   if @show_login=1 and len(@id)>0
      if @show_uid=1
         set @fn=@fn+' - '+@id+')'
      else
         set @fn=@fn+' ('+@id+')'
   else if @show_uid=1
      set @fn=@fn+')'

   return @fn
end
go

grant execute on dbo.gfisf_epay_getname to public
go

if object_id('dbo.gfisf_isvalid_login','FN') is not null drop function dbo.gfisf_isvalid_login
go

------------------------------------------------------------------------
--
-- gfisf_isvalid_login - syntax check a login ID
--
-- Argument: @login_id - login ID to check
--
-- Return code: >0 - valid, size of the login ID
--               0 - invalid: empty
--              -1 - invalid: leading or trailing space(s)
--              -2 - invalid: upper case letter(s)
--              -3 - invalid: reserved ASCII character(s)
--              -4 - invalid: reserved word(s)
--              -5 - invalid: already exist
--
------------------------------------------------------------------------
create function dbo.gfisf_isvalid_login(@login_id varchar(64)) returns smallint
begin
   declare @id varchar(64)
   declare @ls varchar(1)
   declare @li smallint

   if isnull(len(@login_id),0)=0 return 0

   set @id=ltrim(rtrim(@login_id))
   if @id<>@login_id return -1

   set @id=lower(@id)
   if @id<>@login_id return -2

   set @li=1
   while @li<=len(@id)
   begin
      set @ls=substring(@id,@li,1)
      set @li=@li+1
                                                 -- control characters
      if (ascii(@ls)<32 or ascii(@ls)>254) return -3
                                                 -- ", &, ', ^, ~, DEL
      if ascii(@ls) in (34, 38, 39, 94, 126, 127) return -3
   end

   if @id like '%gfi%'        return -4
   if @id like '%gfigenfare%' return -4
   if @id like '%genfare%'    return -4
   if @id like '%epay%'       return -4
   if @id like '%gfiepay%'    return -4

   if @id in ('super','sysop','administrator','gfidba','dba') return -4

   if exists(select 0 from gfi_epay_usr where login_id=@id) return -5

   return len(@id)
end
go

grant execute on dbo.gfisf_isvalid_login to public
go

if object_id('dbo.gfisf_isvalid_pwd','FN') is not null drop function dbo.gfisf_isvalid_pwd
go

------------------------------------------------------------------------
--
-- gfisf_isvalid_pwd - validate password using password policy settings
--
-- Arguments: @login_id - login ID (if not provided, login ID related checks are skipped)
--            @pwd      - password to check
--
-- Return code: >0 - valid, size of the password
--               0 - invalid: empty
--              -1 - invalid: violation of PWD MIN LENGTH (set to 0 to disable this check)
--              -2 - invalid: violation of PWD ALPHANUMERIC (both alphabet and digit must exist) (set to no to disable this check)
--              -3 - invalid: violation of PWD MIX (must have characters from at least three groups) (set to no to disable this check)
--              -4 - invalid: violation of PWD VS UID (cannot contain each other case sensitive) (set to no to disable this check)
--              -5 - invalid: violation of PWD DICTIONARY (set to no to disable this check)
--              -6 - invalid: violation of PWD HISTORY (set to 0 to disable this check)
--
------------------------------------------------------------------------
create function dbo.gfisf_isvalid_pwd(@login_id varchar(64), @pwd varchar(64)) returns smallint
begin
   declare @len tinyint
   declare @li  smallint

   set @len=isnull(len(@pwd),0)

   set @li=cast(ltrim(rtrim(dbo.gfisf_getlistvalue('EPAY PARM',0,'PWD MIN LENGTH'))) as smallint)
   if @li>0
      if @len<@li return -1

   if dbo.gfisf_istrue('EPAY PARM',0,'PWD ALPHANUMERIC')>0
   begin
      if @len=0 return -2
      if patindex('%[0-9]%',@pwd)=0 return -2
      if patindex('%[a-z]%',@pwd)=0 return -2
   end

   if dbo.gfisf_istrue('EPAY PARM',0,'PWD MIX')>0
   begin
      if @len=0 return -3

      set @li=0
      if patindex('%[0-9]%',@pwd)>0 set @li=@li+1
      if patindex('%[ABCDEFGHIJKLMNOPQRSTUVWXYZ]%',@pwd COLLATE SQL_Latin1_General_CP1_CS_AS)>0 set @li=@li+1
      if patindex('%[abcdefghijklmnopqrstuvwxyz]%',@pwd COLLATE SQL_Latin1_General_CP1_CS_AS)>0 set @li=@li+1
      if patindex('%[%&#$!@*?]%',@pwd)>0 set @li=@li+1

      if @li<3 return -3
   end

   if (@len>1 and len(@login_id)>1 and dbo.gfisf_istrue('EPAY PARM',0,'PWD VS UID')>0)
   begin
      if charindex(@pwd,@login_id COLLATE SQL_Latin1_General_CP1_CS_AS)>0 return -4
      if charindex(@login_id,@pwd COLLATE SQL_Latin1_General_CP1_CS_AS)>0 return -4
   end

   if (@len>0 and dbo.gfisf_istrue('EPAY PARM',0,'PWD DICTIONARY')>0)
      if exists(select 0 from security_dict where phrase=@pwd) return -5

   if len(@login_id)>0 and @len>0
   begin
      set @li=cast(ltrim(rtrim(dbo.gfisf_getlistvalue('EPAY PARM',0,'PWD HISTORY'))) as smallint)
      if @li>0
         if exists(select 0 from gfi_epay_usr u, gfi_epay_usr_pwd p
                   where u.user_id=p.user_id and login_id=lower(@login_id)
                     and digest=dbo.gfisf_get_pwd_hash(@pwd,cast(cast(N'' as xml).value('xs:base64Binary(sql:column("salt"))','varbinary(max)') as varchar(50)))
                     and pwd_id<=@li) return -6
   end

   return @len
end
go

grant execute on dbo.gfisf_isvalid_pwd to public
go

if object_id('dbo.gfisf_swab','FN') is not null drop function dbo.gfisf_swab
go

------------------------------------------------------------------------
--
-- gfisf_swab - swab bytes based on the number of bytes specified
--
-- Arguments: @v    - value to swab
--            @byte - number of bytes (1-7), counting from right, to swab; excluded bytes on the left are ignored and not included in the result
--
-- Return code: >=0 - result
--             null - @v is null
--               -1 - @byte is out of range or invalid
--
------------------------------------------------------------------------
create function dbo.gfisf_swab(@v bigint, @byte tinyint) returns bigint
begin
   declare @x bigint
   declare @y bigint

   if isnull(@v,0)=0 return @v

   if isnull(@byte,0)<1 or isnull(@byte,0)>7 return -1

   select @x=0, @y=256
   while @byte>0
      select @x=@x+(@v % 256)*power(@y,@byte - 1), @v=@v/@y, @byte=@byte - 1

   return @x
end
go

grant execute on dbo.gfisf_swab to public
go

if object_id('dbo.gfisf_epay_card_status','FN') is not null drop function dbo.gfisf_epay_card_status
go

------------------------------------------------------------------------
--
-- gfisf_epay_card_status - get card status information (bad conditions)
--
-- Argument: @card_id - card record ID
--
-- Return code: card status bitmap flag
--              0x0001 - card record does not exist (gfi_epay_card table)
--              0x0002 - expired (gfi_epay_card.exp_ts)
--              0x0004 - expire in three or less days (gfi_epay_card.exp_ts)
--              0x0008 - deleted (gfi_epay_card.delete_ts)
--              0x0010 - lost or stolen (gfi_epay_card.flags 0x0001)
--              0x0020 - inactive (gfi_epay_card.flags 0x0002)
--              0x0040 - badlisted (gfi_epay_card.flags 0x0004)
--              0x0080 - empty (no product)
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_card_status(@card_id integer) returns smallint
begin
   declare @rc smallint

   select @rc=(case when exp_ts is null then 0
                    when datediff(d,0,exp_ts)<=datediff(d,0,current_timestamp) then 2
                    when datediff(d,0,exp_ts) - datediff(d,0,current_timestamp)<=3 then 4 else 0 end)+
              (case when delete_ts is null then 0 else 8 end)+(flags & 7)*16+
              (case when exists(select 0 from gfi_epay_card_dtl where card_id=@card_id and prod_id>0) then 0 else 128 end)
   from gfi_epay_card where card_id=@card_id
   if @@rowcount>0 return @rc

   return 1
end
go

grant execute on dbo.gfisf_epay_card_status to public
go

if object_id('dbo.gfisf_epay_prod_status','FN') is not null drop function dbo.gfisf_epay_prod_status
go

------------------------------------------------------------------------
--
-- gfisf_epay_prod_status - get product status information (bad conditions)
--
-- Arguments: @card_id - card record ID
--            @prod_id - product ID
--
-- Return code: product status bitmap flag
--              0x0001 - product does not exist (gfi_epay_card_dtl table)
--              0x0002 - inactive (gfi_epay_card_dtl.flags 0x0002)
--              0x0004 - badlisted (gfi_epay_card_dtl.flags 0x0004)
--              0x0008 - expired (gfi_epay_card_dtl.exp_ts)
--              0x0010 - expire in three or less days (gfi_epay_card_dtl.exp_ts)
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_prod_status(@card_id integer, @prod_id tinyint) returns smallint
begin
   declare @rc smallint

   select @rc=(flags & 0x06)+
              (case when exp_ts is null then 0
                    when datediff(d,0,exp_ts)<=datediff(d,0,current_timestamp) then 8
                    when datediff(d,0,exp_ts) - datediff(d,0,current_timestamp)<=3 then 16 else 0 end)
   from gfi_epay_card_dtl where card_id=@card_id and prod_id=@prod_id
   if @@rowcount>0 return @rc

   return 1
end
go

grant execute on dbo.gfisf_epay_prod_status to public
go

if object_id('dbo.gfisf_epay_pending_recharge','FN') is not null drop function dbo.gfisf_epay_pending_recharge
go

------------------------------------------------------------------------
--
-- gfisf_epay_pending_recharge - get product pending recharge count
--
-- Arguments: @card_type - card type
--            @card_eid  - card UID
--            @prod_id   - product ID
--
-- Return: product pending recharge count
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_pending_recharge(@card_type tinyint, @card_eid bigint, @prod_id tinyint) returns integer
begin
   declare @cnt     integer
   declare @card_id integer

   select @card_id=c.card_id, @cnt=pending
   from gfi_epay_card c, gfi_epay_card_dtl d
   where c.card_id=d.card_id and c.card_type=@card_type and c.card_eid=@card_eid and d.prod_id=@prod_id
   if @card_id>0
      select @cnt=@cnt+count(*)
      from gfi_epay_order_item i, gfi_epay_order_recharge r
      where i.ord_id=r.ord_id and i.item_n=r.item_n and i.type in (1, 4) and i.fulfill_ts is null
        and card_id=@card_id and prod_id=@prod_id
   else
      set @cnt=0

   select @cnt=@cnt+count(*)
   from gfi_epay_order_item i, gfi_epay_order_autoload a
   where i.ord_id=a.ord_id and i.item_n=a.item_n and i.type=5 and i.fulfill_ts is null and a.load_type=1
     and card_type=@card_type and card_eid=@card_eid and prod_id=@prod_id and status=0

   return @cnt
end
go

grant execute on dbo.gfisf_epay_pending_recharge to public
go

if object_id('dbo.gfisf_epay_fare_start_date','FN') is not null drop function dbo.gfisf_epay_fare_start_date
go

------------------------------------------------------------------------
--
-- gfisf_epay_fare_start_date - convert fare start_ts into start_date for end device
--
-- Argument: @start_ts - fare start timestamp
--
-- Return: start date (0 - rolling start; >0 - fixed start date (local Unix date))
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_fare_start_date(@start_ts datetime) returns smallint
begin
   return (case when @start_ts is null then 0 else datediff(d,dbo.ymd(1970,1,1),@start_ts) end)
end
go

grant execute on dbo.gfisf_epay_fare_start_date to public
go

if object_id('dbo.gfisf_epay_fare_exp_date','FN') is not null drop function dbo.gfisf_epay_fare_exp_date
go

------------------------------------------------------------------------
--
-- gfisf_epay_fare_exp_date - convert fare exp_ts into exp_date for end device
--
-- Argument: @exp_ts - fare expiration timestamp
--
-- Return: expiration date (0 - never expire; >0 - valid days for rolling start and fixed date (local Unix day) for non-rolling start)
-- 2017-03-01 - RBB - DATARUN-201: Change return value to int instead of smallint, to handle 100 year employee passes for WTA
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_fare_exp_date(@exp_ts datetime) returns int
begin
   return (case when @exp_ts is null then 0 else datediff(d,dbo.ymd(1970,1,1),@exp_ts) end)
end
go

grant execute on dbo.gfisf_epay_fare_exp_date to public
go

if object_id('dbo.gfisf_epay_fare_addtime','FN') is not null drop function dbo.gfisf_epay_fare_addtime
go

------------------------------------------------------------------------
--
-- gfisf_epay_fare_addtime - extract "additional time" from fare exp_ts for end device
--
-- Arguments: @exp_ts - fare expiration timestamp
--            @exp_type - expiration type
--
-- Return: additional time in minutes (0 - expire on expiration day)
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_fare_addtime(@exp_ts datetime, @exp_type tinyint) returns smallint
begin
   declare @sotd varchar(32)

   if @exp_type=1                                -- expire at end of transit day
   begin
      set @sotd=dbo.gfisf_getlistvalue('SYS PARM',0,'SOTD')

      if isdate(@sotd)>0 return datediff(mi,convert(datetime,0,8),CONVERT(datetime,@sotd,8))

      return 0
   end
                                                 -- <24-hour expiration
   if @exp_type=3 return datediff(mi,dateadd(d,0,datediff(d,0,@exp_ts)),@exp_ts)

   return 0
end
go

grant execute on dbo.gfisf_epay_fare_addtime to public
go

if object_id('dbo.gfisf_epay_load_seq','FN') is not null drop function dbo.gfisf_epay_load_seq
go

------------------------------------------------------------------------
--
-- gfisf_epay_load_seq - recycle load sequence number at 255
--
-- Argument: @load_seq - load sequence number
--
-- Return: load sequence number, recycled back to start from 0 when 255 is reached
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_load_seq(@load_seq smallint) returns tinyint
begin
   return (case when @load_seq>255 then (@load_seq % 256) else @load_seq end)
end
go

grant execute on dbo.gfisf_epay_load_seq to public
go

if object_id('dbo.gfisf_epay_card_id','FN') is not null drop function dbo.gfisf_epay_card_id
go

------------------------------------------------------------------------
--
-- gfisf_epay_card_id - get internal card record ID
--
-- Arguments: @card_type - card type
--            @card_eid  - card UID
--
-- Return: 0 - not found; >0 - internal card record ID (gfi_epay_card.card_id)
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_card_id(@card_type tinyint, @card_eid bigint) returns integer
begin
   declare @id integer

   select @id=card_id from gfi_epay_card where card_type=@card_type and card_eid=@card_eid
   if @@rowcount<=0 set @id=0

   return @id
end
go

grant execute on dbo.gfisf_epay_card_id to public
go

if object_id('dbo.gfisf_epay_card_exp_ts2','FN') is not null drop function dbo.gfisf_epay_card_exp_ts2
go

------------------------------------------------------------------------
--
-- gfisf_epay_card_exp_ts2 - get card expiration date
--
-- Argument: @create_ts - card creation timestamp
--           @card_type - 1: DESFire; 2 - Ultralight; 3 - MiFare Classic
--
-- Return: card expiration date
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_card_exp_ts2(@create_ts datetime, @card_type tinyint) returns datetime
begin
   declare @ts datetime

   set @ts=isnull(@create_ts,current_timestamp)

   select @ts=(case when isdate(value)>0         -- absolute expiration date
                    then convert(datetime,value)
                                                 -- relative expiration by number of years (1-99)
                    when value like '[0-9]y' or value like '[0-9][0-9]y'
                    then dateadd(yy,cast(left(value,len(value) - 1) as integer),@ts)
                                                 -- relative expiration by number of quarters (1-99)
                    when value like '[0-9]q' or value like '[0-9][0-9]q'
                    then dateadd(qq,cast(left(value,len(value) - 1) as integer),@ts)
                                                 -- relative expiration by number of months (1-999)
                    when value like '[0-9]m' or value like '[0-9][0-9]m' or value like '[0-9][0-9][0-9]m' or value like '[0-9][0-9][0-9][0-9]m'
                    then dateadd(mm,cast(left(value,len(value) - 1) as integer),@ts)
                                                 -- relative expiration by number of days (1-99)
                    when value like '[0-9]d' or value like '[0-9][0-9]d' or value like '[0-9][0-9][0-9]d' or value like '[0-9][0-9][0-9][0-9]d' or value like '[0-9][0-9][0-9][0-9][0-9]d'
                    then dateadd(dd,cast(left(value,len(value) - 1) as integer),@ts)
                                                 -- default to never expire
                    else null end)
   from gfi_lst where type='EPAY PARM' and class=0 and name=(case @card_type when 2 then 'CARD EXP DATE 2' else 'CARD EXP DATE' end)
   if @@rowcount<=0 set @ts=null

   return @ts
end
go

grant execute on dbo.gfisf_epay_card_exp_ts2 to public
go

if object_id('dbo.gfisf_epay_card_exp_ts','FN') is not null drop function dbo.gfisf_epay_card_exp_ts
go

------------------------------------------------------------------------
--
-- gfisf_epay_card_exp_ts - get card expiration date
--
-- Argument: @create_ts - card creation timestamp
--
-- Return: card expiration date
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_card_exp_ts(@create_ts datetime) returns datetime
begin
   return dbo.gfisf_epay_card_exp_ts2(@create_ts,1)
end
go

grant execute on dbo.gfisf_epay_card_exp_ts to public
go

if object_id('dbo.gfisf_epay_aid','FN') is not null drop function dbo.gfisf_epay_aid
go

------------------------------------------------------------------------
--
-- gfisf_epay_aid - get agency ID from EPAY PARM.AGENCY ID
--
-- Return: 0 - setting not found; >0 - agency ID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_aid() returns smallint
begin
   declare @aid smallint

   select @aid=(case when isnumeric(value)>0 then (case when cast(value as bigint) between 1 and 32767 then cast(value as smallint) else 0 end)
                     else 0 end)
   from gfi_lst where type='EPAY PARM' and class=0 and name='AGENCY ID'
   if @@rowcount<=0 set @aid=0

   return @aid
end
go

grant execute on dbo.gfisf_epay_aid to public
go

if object_id('dbo.gfisf_epay_sc','FN') is not null drop function dbo.gfisf_epay_sc
go

------------------------------------------------------------------------
--
-- gfisf_epay_sc - get security code from EPAY PARM.SEC CODE SMART
--
-- Return: 0 - setting not found; >0 - security code for smart card
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_sc() returns tinyint
begin
   declare @sc tinyint

   select @sc=(case when isnumeric(value)>0 then (case when cast(value as bigint) between 1 and 255 then cast(value as tinyint) else 0 end)
                    else 0 end)
   from gfi_lst where type='EPAY PARM' and class=0 and name='SEC CODE SMART'
   if @@rowcount<=0 set @sc=5

   return @sc
end
go

grant execute on dbo.gfisf_epay_sc to public
go

if object_id('dbo.gfisf_epay_uid','FN') is not null drop function dbo.gfisf_epay_uid
go

------------------------------------------------------------------------
--
-- gfisf_epay_uid - get decimal UID from hex UID
--
-- Return: 0 - not found; >0 - decimal UID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_uid(@card_type tinyint, @card_eid_hex varchar(20)) returns bigint
begin
   return (select isnull(max(card_eid),0) from gfi_epay_card_inv where card_type=@card_type and card_eid_hex=@card_eid_hex)
end
go

grant execute on dbo.gfisf_epay_uid to public
go

if object_id('dbo.gfisf_epay_cc_n','FN') is not null drop function dbo.gfisf_epay_cc_n
go

------------------------------------------------------------------------
--
-- gfisf_epay_cc_n - convert text credit card number to numeric
--
-- Return: null - can't convert; >0 - converted credit card number
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_cc_n(@cc_n varchar(20)) returns bigint
begin
   declare @cc bigint

   set @cc_n=replace(@cc_n,'*','')
   if isnumeric(@cc_n)>0
      set @cc=cast(@cc_n as bigint)
   else
   begin
      set @cc_n=right(@cc_n,4)
      if isnumeric(@cc_n)>0
         set @cc=cast(@cc_n as bigint)
      else
         set @cc=null
   end

   return @cc
end
go

grant execute on dbo.gfisf_epay_cc_n to public
go

if object_id('dbo.gfisf_epay_card_created_by','FN') is not null drop function dbo.gfisf_epay_card_created_by
go

------------------------------------------------------------------------
--
-- gfisf_epay_card_created_by - check which type of equipment created the card
--
-- Return: 0 - card not found
--         1 - card issued by TVM
--         2 - card issued by PEM
--         3 - card issued by farebox
--         4 - card issued by PS
--         other - card issued by unknown equipment
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_card_created_by(@card_id integer) returns tinyint
begin
   declare @by tinyint

   select @by=count(*)*(case max(mid) when 5  then (case max(eq_type) when 1 then 1 when 2 then 2 else 5 end)
                                      when 13 then 3
                                      when 9  then 4 else 5 end)
   from gfi_epay_card where card_id=@card_id

   return @by
end
go

grant execute on dbo.gfisf_epay_card_created_by to public
go

if object_id('dbo.gfisf_epay_contact_user','FN') is not null drop function dbo.gfisf_epay_contact_user
go

------------------------------------------------------------------------
--
-- gfisf_epay_contact_user - get user primary contact record ID
--
-- Return:  0 - user not found
--         >0 - user primary contact ID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_contact_user(@user_id integer) returns integer
begin
   return (select isnull(max(contact_id),0) from gfi_epay_usr where user_id=@user_id and contact_id>0)
end
go

grant execute on dbo.gfisf_epay_contact_user to public
go

if object_id('dbo.gfisf_epay_contact_bill','FN') is not null drop function dbo.gfisf_epay_contact_bill
go

------------------------------------------------------------------------
--
-- gfisf_epay_contact_bill - get user default billing contact record ID
--
-- Return:  0 - user or user billing record not found
--         >0 - user billing contact ID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_contact_bill(@user_id integer) returns integer
begin
   return (select isnull(max(contact_id),0) from gfi_epay_usr_billing where user_id=@user_id and contact_id>0 and default_billing=1)
end
go

grant execute on dbo.gfisf_epay_contact_bill to public
go

if object_id('dbo.gfisf_epay_contact_inst','FN') is not null drop function dbo.gfisf_epay_contact_inst
go

------------------------------------------------------------------------
--
-- gfisf_epay_contact_inst - get institution primary contact record ID
--
-- Return:  0 - institution not found
--         >0 - institution primary contact ID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_contact_inst(@inst_id smallint) returns integer
begin
   return (select isnull(max(contact_id),0) from gfi_epay_inst where inst_id=@inst_id and contact_id>0)
end
go

grant execute on dbo.gfisf_epay_contact_inst to public
go

if object_id('dbo.gfisf_epay_inst_id','FN') is not null drop function dbo.gfisf_epay_inst_id
go

------------------------------------------------------------------------
--
-- gfisf_epay_inst_id - get institution ID from name
--
-- Return:  0 - institution not found
--         >0 - institution ID
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_inst_id(@name varchar(256)) returns smallint
begin
   return (select isnull(max(inst_id),0) from gfi_epay_inst where description=@name and inst_id>0)
end
go

grant execute on dbo.gfisf_epay_inst_id to public
go

if object_id('dbo.gfisf_epay_inst_avail_credit','FN') is not null drop function dbo.gfisf_epay_inst_avail_credit
go

------------------------------------------------------------------------
--
-- gfisf_epay_inst_avail_credit - get institution available credit
--
-- Return: institution available credit in cents (0 may indicate institution not found)
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_inst_avail_credit(@inst_id smallint) returns integer
begin
   return (select isnull(max(avail_credit),0) from gfi_epay_inst where inst_id=@inst_id)
end
go

grant execute on dbo.gfisf_epay_inst_avail_credit to public
go

if object_id('dbo.gfisf_epay_fare_modify_ts','FN') is not null drop function dbo.gfisf_epay_fare_modify_ts
go

------------------------------------------------------------------------
--
-- gfisf_epay_fare_modify_ts - get e-Fare fare last modification timestamp
--
-- Return: e-Fare fare last modification timestamp
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_fare_modify_ts() returns datetime
begin
   return (select max(modify_ts) from gfi_epay_fare)
end
go

grant execute on dbo.gfisf_epay_fare_modify_ts to public
go

if object_id('dbo.gfisf_epay_inst_credit_limit','FN') is not null drop function dbo.gfisf_epay_inst_credit_limit
go

------------------------------------------------------------------------
--
-- gfisf_epay_inst_credit_limit - get institution credit limit
--
-- Return: institution credit limit in cents (0 may indicate institution not found)
--
------------------------------------------------------------------------
create function dbo.gfisf_epay_inst_credit_limit(@inst_id smallint) returns integer
begin
   return (select isnull(max(credit_limit),0) from gfi_epay_inst where inst_id=@inst_id)
end
go

grant execute on dbo.gfisf_epay_inst_credit_limit to public
go

if object_id('dbo.gfisf_GetSavePointName','FN') is not null drop function dbo.gfisf_GetSavePointName;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 06/11/2015
-- Description:	Get Save Point Name
-- =============================================
create function dbo.gfisf_GetSavePointName(@uid uniqueidentifier)
returns varchar(32)
as
begin
/*
select SavePointName = dbo.gfisf_GetSavePointName(newid());
*/
	declare @result varchar(32);

	set @result = replace(cast(@uid as varchar(36)), '-', '');

	return @result;
end
go

grant execute on dbo.gfisf_GetSavePointName to public;
go

if object_id('dbo.gfisf_GetPowerBuilderVersion','FN') is not null drop function dbo.gfisf_GetPowerBuilderVersion;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 06/30/2015
-- Description:	Get Power Builder Version
-- =============================================
create function dbo.gfisf_GetPowerBuilderVersion()
returns varchar(100)
as
begin
/*
select PowerBuilderVersion = dbo.gfisf_GetPowerBuilderVersion();
*/
	declare @result varchar(100);

	select @result = dataValue
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'Power Builder Version'
	  and isActive = 1;

	if @result is null
	begin
		set @result = ''; -- default value
	end

	return @result;
end
go

grant execute on dbo.gfisf_GetPowerBuilderVersion to public;
go


if object_id('dbo.gfisf_split_comma_list','TF') is not null drop function dbo.gfisf_split_comma_list;
go

-- =============================================
-- Author:		Richard B. Becker
-- Create date: 02/16/2016
-- Description:	Split comma delimited list of integers into table
-- =============================================
create function dbo.gfisf_split_comma_list( @input AS varchar(max) )
returns @Result table(value bigint)
begin
      declare @str varchar(20)
      declare @ind int
      if(@input is not null)
      begin
            set @ind = charindex(',',@input)
            while @ind > 0
            begin
                  set @str = substring(@input,1,@ind-1)
                  set @input = substring(@input,@ind+1,len(@input)-@ind)
                  insert into @Result values (@str)
                  set @ind = charindex(',',@input)
            end
            set @str = @input
            insert into @Result values (@str)
      end
      return
end
go


if object_id('dbo.gfisf_GetCloudSetting','FN') is not null drop function dbo.gfisf_GetCloudSetting;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 02/03/2016
-- Description:	Get Cloud Setting	Yes/No
-- 
--				LG-1507 - Merge the SQL code for cloud and non cloud branches
-- =============================================
create function dbo.gfisf_GetCloudSetting()
returns varchar(100)
as
begin
/*
select cloudSetting = dbo.gfisf_GetCloudSetting();
*/
	declare @result varchar(100);

	select @result = dataValue
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'Cloud'
	  and isActive = 1;

	if isnull(@result, '') <> 'Yes'
	begin
		set @result = 'No'; -- default value
	end

	return @result;
end
go

grant execute on dbo.gfisf_GetCloudSetting to public;
go

if object_id('dbo.gfisf_GetFaresetNumber','FN') is not null drop function dbo.gfisf_GetFaresetNumber;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 01/27/2016
-- Description:	Get Fareset Number
-- 
--				LG-1507 - Merge the SQL code for cloud and non cloud branches
-- =============================================
create function dbo.gfisf_GetFaresetNumber()
returns int
as
begin
/*
select faresetNumber = dbo.gfisf_GetFaresetNumber();
*/
	declare @result int;

	select @result = cast(dataValue as int)
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'Fareset Number'
	  and isActive = 1;

	if isnull(@result, 0) <= 0
	begin
		if dbo.gfisf_GetCloudSetting() = 'Yes'
		begin
			set @result = 20; -- default value for Cloud
		end
		else
		begin
			set @result = 10; -- default value for Non-Cloud
		end
	end

	return @result;
end
go

grant execute on dbo.gfisf_GetFaresetNumber to public;
go

if object_id('dbo.gfisf_TTP_Number','FN') is not null drop function dbo.gfisf_TTP_Number;
go

-- =============================================
-- Author:		Igor Mikhalyev
-- Create date: 01/27/2016
-- Description:	Get TTP Number
-- 
--				LG-1507 - Merge the SQL code for cloud and non cloud branches
-- =============================================
create function dbo.gfisf_TTP_Number()
returns int
as
begin
/*
select TTP_Number = dbo.gfisf_TTP_Number();
*/
	declare @result int;

	select @result = cast(dataValue as int)
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'TTP Number'
	  and isActive = 1;

	if @result is null
	or @result <= 0
	begin
		if dbo.gfisf_GetCloudSetting() = 'Yes'
		begin
			set @result = 241; -- default value for Cloud
		end
		else
		begin
			set @result = 48; -- default value for Non-Cloud
		end
	end

	return @result;
end
go

grant execute on dbo.gfisf_TTP_Number to public;
go

if object_id('dbo.fnmundec','FN') is not null drop function dbo.fnmundec;
go

------------------------------------------------------------------------
-- Function dec(14,2) fnmundec(loc, ad, choice)
--
-- Description: Get monthly UNKNOWN (ml - ev) value for decimal items
--
-- Argument: loc - location number; NULL or 0 for all locations
--           ad  - transit date (first day of the month)
--           choice - 1: curr_r; 2: uncl_r
------------------------------------------------------------------------
create function dbo.fnmundec(@loc smallint, @ad date, @choice smallint)
returns decimal(14, 2)
begin
/*
select dbo.fnmundec(0, '2016-03-01', 1);
select dbo.fnmundec(0, '2016-03-01', 2);
*/
	declare @result decimal(14, 2);

	set @ad = dbo.ymd(year(@ad), month(@ad), 1);
	
	select @result = isnull(abs(sum(	case @choice
											when 1 then curr_r
											when 2 then uncl_r
											else 0
										end))
	                      , 0)
	from mrtesum
	where	loc_n =	case isnull(@loc, 0)
						when 0 then loc_n
						else @loc
					end
		and	mday = @ad
		and	route = 1000001	-- or maybe -3?
	;

	return @result;
end
go

grant execute on dbo.fnmundec to public
go


if object_id('dbo.fnmunint','FN') is not null drop function dbo.fnmunint;
go

------------------------------------------------------------------------
-- Function integer fnmunint(loc, ad, choice)
--
-- Description: Get monthly UNKNOWN (ml - ev) value for integer items
--
-- Argument: loc - location number; NULL or 0 for all locations
--           ad  - transit date (first day of the month)
--           choice - see case statement in codes
------------------------------------------------------------------------
create function dbo.fnmunint(@loc smallint, @ad date, @choice smallint)
returns int
begin
/*
select dbo.fnmunint(1, '2015-07-02', 50) as result;
select dbo.fnmunint(1, '2015-07-02', 281) as result;
select dbo.fnmunint(0, '2015-07-02', 281) as result;
*/
	declare @result int;

	set @loc = nullif(@loc, 0);

	set @ad = dbo.ymd(year(@ad), month(@ad), 1);
	
	select @result = isnull(abs(sum(
		case @choice
			when 1 then dump_c
			when 2 then token_c
			when 3 then ticket_c
			when 4 then pass_c
			when 5 then bill_c
			when 6 then fare_c
			when 7 then fare1_c
			when 8 then fare2_c
			when 9 then fare3_c
			when 10 then fare4_c
			when 11 then fare5_c
			when 12 then fare6_c
			when 13 then fare7_c
			when 14 then fare8_c
			when 15 then fare9_c
			when 16 then fare10_c
			when 21 then key1
			when 22 then key2
			when 23 then key3
			when 24 then key4
			when 25 then key5
			when 26 then key6
			when 27 then key7
			when 28 then key8
			when 29 then key9
			when 30 then keyast
			when 31 then keya
			when 32 then keyb
			when 33 then keyc
			when 34 then keyd
			when 41 then ttp1
			when 42 then ttp2
			when 43 then ttp3
			when 44 then ttp4
			when 45 then ttp5
			when 46 then ttp6
			when 47 then ttp7
			when 48 then ttp8
			when 49 then ttp9
			when 50 then ttp10
			when 51 then ttp11
			when 52 then ttp12
			when 53 then ttp13
			when 54 then ttp14
			when 55 then ttp15
			when 56 then ttp16
			when 57 then ttp17
			when 58 then ttp18
			when 59 then ttp19
			when 60 then ttp20
			when 61 then ttp21
			when 62 then ttp22
			when 63 then ttp23
			when 64 then ttp24
			when 65 then ttp25
			when 66 then ttp26
			when 67 then ttp27
			when 68 then ttp28
			when 69 then ttp29
			when 70 then ttp30
			when 71 then ttp31
			when 72 then ttp32
			when 73 then ttp33
			when 74 then ttp34
			when 75 then ttp35
			when 76 then ttp36
			when 77 then ttp37
			when 78 then ttp38
			when 79 then ttp39
			when 80 then ttp40
			when 81 then ttp41
			when 82 then ttp42
			when 83 then ttp43
			when 84 then ttp44
			when 85 then ttp45
			when 86 then ttp46
			when 87 then ttp47
			when 88 then ttp48
			when 89 then ttp49
			when 90 then ttp50
			when 91 then ttp51
			when 92 then ttp52
			when 93 then ttp53
			when 94 then ttp54
			when 95 then ttp55
			when 96 then ttp56
			when 97 then ttp57
			when 98 then ttp58
			when 99 then ttp59
			when 100 then ttp60
			when 101 then ttp61
			when 102 then ttp62
			when 103 then ttp63
			when 104 then ttp64
			when 105 then ttp65
			when 106 then ttp66
			when 107 then ttp67
			when 108 then ttp68
			when 109 then ttp69
			when 110 then ttp70
			when 111 then ttp71
			when 112 then ttp72
			when 113 then ttp73
			when 114 then ttp74
			when 115 then ttp75
			when 116 then ttp76
			when 117 then ttp77
			when 118 then ttp78
			when 119 then ttp79
			when 120 then ttp80
			when 121 then ttp81
			when 122 then ttp82
			when 123 then ttp83
			when 124 then ttp84
			when 125 then ttp85
			when 126 then ttp86
			when 127 then ttp87
			when 128 then ttp88
			when 129 then ttp89
			when 130 then ttp90
			when 131 then ttp91
			when 132 then ttp92
			when 133 then ttp93
			when 134 then ttp94
			when 135 then ttp95
			when 136 then ttp96
			when 137 then ttp97
			when 138 then ttp98
			when 139 then ttp99
			when 140 then ttp100
			when 141 then ttp101
			when 142 then ttp102
			when 143 then ttp103
			when 144 then ttp104
			when 145 then ttp105
			when 146 then ttp106
			when 147 then ttp107
			when 148 then ttp108
			when 149 then ttp109
			when 150 then ttp110
			when 151 then ttp111
			when 152 then ttp112
			when 153 then ttp113
			when 154 then ttp114
			when 155 then ttp115
			when 156 then ttp116
			when 157 then ttp117
			when 158 then ttp118
			when 159 then ttp119
			when 160 then ttp120
			when 161 then ttp121
			when 162 then ttp122
			when 163 then ttp123
			when 164 then ttp124
			when 165 then ttp125
			when 166 then ttp126
			when 167 then ttp127
			when 168 then ttp128
			when 169 then ttp129
			when 170 then ttp130
			when 171 then ttp131
			when 172 then ttp132
			when 173 then ttp133
			when 174 then ttp134
			when 175 then ttp135
			when 176 then ttp136
			when 717 then ttp137
			when 178 then ttp138
			when 719 then ttp139
			when 180 then ttp140
			when 181 then ttp141
			when 182 then ttp142
			when 183 then ttp143
			when 184 then ttp144
			when 185 then ttp145
			when 186 then ttp146
			when 187 then ttp147
			when 188 then ttp148
			when 189 then ttp149
			when 190 then ttp150
			when 191 then ttp151
			when 192 then ttp152
			when 193 then ttp153
			when 194 then ttp154
			when 195 then ttp155
			when 196 then ttp156
			when 197 then ttp157
			when 198 then ttp158
			when 199 then ttp159
			when 200 then ttp160
			when 201 then ttp161
			when 202 then ttp162
			when 203 then ttp163
			when 204 then ttp164
			when 205 then ttp165
			when 206 then ttp166
			when 207 then ttp167
			when 208 then ttp168
			when 209 then ttp169
			when 210 then ttp170
			when 211 then ttp171
			when 212 then ttp172
			when 213 then ttp173
			when 214 then ttp174
			when 215 then ttp175
			when 216 then ttp176
			when 217 then ttp177
			when 218 then ttp178
			when 219 then ttp179
			when 210 then ttp180
			when 211 then ttp181
			when 212 then ttp182
			when 213 then ttp183
			when 214 then ttp184
			when 215 then ttp185
			when 216 then ttp186
			when 217 then ttp187
			when 218 then ttp188
			when 219 then ttp189
			when 220 then ttp190
			when 221 then ttp191
			when 222 then ttp192
			when 223 then ttp193
			when 224 then ttp194
			when 225 then ttp195
			when 226 then ttp196
			when 227 then ttp197
			when 228 then ttp198
			when 229 then ttp199
			when 230 then ttp200
			when 241 then ttp201
			when 242 then ttp202
			when 243 then ttp203
			when 244 then ttp204
			when 245 then ttp205
			when 246 then ttp206
			when 247 then ttp207
			when 248 then ttp208
			when 249 then ttp209
			when 250 then ttp210
			when 251 then ttp211
			when 252 then ttp212
			when 253 then ttp213
			when 254 then ttp214
			when 255 then ttp215
			when 256 then ttp216
			when 257 then ttp217
			when 258 then ttp218
			when 259 then ttp219
			when 260 then ttp220
			when 261 then ttp221
			when 262 then ttp222
			when 263 then ttp223
			when 264 then ttp224
			when 265 then ttp225
			when 266 then ttp226
			when 267 then ttp227
			when 268 then ttp228
			when 269 then ttp229
			when 270 then ttp230
			when 271 then ttp231
			when 272 then ttp232
			when 273 then ttp233
			when 274 then ttp234
			when 275 then ttp235
			when 276 then ttp236
			when 277 then ttp237
			when 278 then ttp238
			when 279 then ttp239
			when 280 then ttp240
			when 281 then ttp241
			else 0
		end					)), 0)
	from mrtesum as a
	left join mrtesum_extd as b
		on	b.loc_n = a.loc_n
		and b.mday = a.mday
		and b.route = a.route
	where	a.mday = @ad
		and	a.route = 1000001
		and	a.loc_n = isnull(@loc, a.loc_n)
	;
	
	return @result;
end
go

grant execute on dbo.fnmunint to public
go

if object_id('dbo.gfisf_get_filename','FN') is not null drop function dbo.gfisf_get_filename;
go

------------------------------------------------------------------------
-- gfisf_get_filename - GD-1936
------------------------------------------------------------------------
create function dbo.gfisf_get_filename(@fullFileName varchar(250)) returns varchar(250)
begin
/*
select dbo.gfisf_get_filename(null);
select dbo.gfisf_get_filename('');
select dbo.gfisf_get_filename('C:\gficvs_cloud\db\JIRAs\GD-1936\NM\cnf\gaci.bin');
select dbo.gfisf_get_filename('gaci.bin');
select dbo.gfisf_get_filename('C:\gficvs_cloud\db\JIRAs\GD-1936\GDS1\aa\*.raw');
*/
	declare @result varchar(250);
	declare @int int;
	
	if @fullFileName is null return null;
	
	set @fullFileName = ltrim(rtrim(@fullFileName));
	if @fullFileName = '' return '';
	
	set @result = reverse(@fullFileName);
	set @int = charindex('\', @result);
	
	if @int > 0 set @result = substring(@result, 1, @int - 1);
	
	set @result = reverse(@result);
	
	return @result;
end
go

grant execute on dbo.gfisf_get_filename to public
go

if object_id('dbo.gfisf_get_filepath','FN') is not null drop function dbo.gfisf_get_filepath;
go

------------------------------------------------------------------------
-- gfisf_get_filepath - GD-1936
------------------------------------------------------------------------
create function dbo.gfisf_get_filepath(@fullFileName varchar(250)) returns varchar(250)
begin
/*
select dbo.gfisf_get_filepath(null);
select dbo.gfisf_get_filepath('');
select dbo.gfisf_get_filepath('C:\gficvs_cloud\db\JIRAs\GD-1936\NM\cnf\gaci.bin');
select dbo.gfisf_get_filepath('gaci.bin');
select dbo.gfisf_get_filepath('C:\gficvs_cloud\db\JIRAs\GD-1936\GDS1\aa\*.raw');
*/
	declare @result varchar(250);
	declare @int int;
	
	if @fullFileName is null return null;
	
	set @fullFileName = ltrim(rtrim(@fullFileName));
	if @fullFileName = '' return '';
	
	set @result = reverse(@fullFileName);
	set @int = charindex('\', @result);
	
	if @int = 0 return '';

	set @result = substring(@result, @int, 250);
	set @result = reverse(@result);
	
	return @result;
end
go

grant execute on dbo.gfisf_get_filepath to public
go

if object_id('dbo.fnSplitString','TF') is not null drop function dbo.fnSplitString;
go

CREATE FUNCTION [dbo].[fnSplitString] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(Value NVARCHAR(MAX) 
) 
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (Value)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END
go


if object_id('dbo.gfisf_GetLogFileFullName','FN') is not null drop function dbo.gfisf_GetLogFileFullName;
go
 
-- =============================================
-- Author		: Rajesh Shanmugasundaram
-- Create date	: 05/13/2016
-- Description	: Get Log File Full Name
--
--					GD-443 - Make writing to log persistant inside transactions
-- =============================================
create function dbo.gfisf_GetLogFileFullName()
returns varchar(100)
as
begin
/*
select LOG_FILE_FULL_NAME = dbo.gfisf_GetLogFileFullName();
*/
	declare @result varchar(100);
 
	select @result = dataValue
	from configurationsData
	where	configurationID = 0 -- Default
		and dataName = 'LOG_FILE_FULL_NAME'
		and isActive = 1
	;
 
	if @result is null
	begin
		set @result = ''; -- default value
	end
 
	return @result;
end
go
 
grant execute on dbo.gfisf_GetLogFileFullName to public;
go


if object_id('dbo.gfisf_tr_subtype','FN') is not null drop function dbo.gfisf_tr_subtype;
go

------------------------------------------------------------------------
-- gfisf_tr_subtype - return tr status - GD-2206
------------------------------------------------------------------------
create function dbo.gfisf_tr_subtype(
	@tr_etype tinyint
) returns varchar(100)
begin
/*
select tr_subtype = dbo.gfisf_tr_subtype(128);
select tr_subtype = dbo.gfisf_tr_subtype(161);
*/
	declare @result varchar(100) = '';
	declare @value varchar(100) = null;
	
	select
		@value = description
	from event_subtype
	where	etype = @tr_etype;
	
	if @value is null
	begin
		set @value = 'Sub-Type #' + isnull(ltrim(cast(@tr_etype as varchar(10))), 'null');
	end
	
	set @result = @value;

	return @result;
end
go

grant execute on dbo.gfisf_tr_subtype to public
go


if object_id('dbo.gfisf_tr_events','FN') is not null drop function dbo.gfisf_tr_events;
go

------------------------------------------------------------------------
-- gfisf_tr_events - return tr events - GD-2206
------------------------------------------------------------------------
create function dbo.gfisf_tr_events(
	@tr_etype tinyint,
	@tr_events bigint
) returns varchar(8000)
begin
/*
select tr_events = dbo.gfisf_tr_events(128, 515);
select tr_events = dbo.gfisf_tr_events(161, 515);
*/
	declare @result varchar(8000) = '';
	declare @value varchar(8000) = '';
	
	select
		@value = @value + '; ' + description
	from event_status
	where	etype = @tr_etype
		and	id & @tr_events = id
	order by
		id
	;
	
	if @value <> ''
	begin
		set @value = substring(@value, 3, 8000);
	end
	else
	begin
		set @value = cast(@tr_events as varchar(8000));
	end

	set @result = @value;

	return @result;
end
go

grant execute on dbo.gfisf_tr_events to public
go

if object_id('dbo.DecimalToBinary','FN') is not null drop function dbo.DecimalToBinary;
go
----------------------------
--dATARUN-563\
-------------------------------
CREATE FUNCTION [dbo].[DecimalToBinary]
(
@Input bigint
)
RETURNS varchar(255)
AS
BEGIN
DECLARE @Output varchar(255) = ''
WHILE @Input > 0 BEGIN
SET @Output = @Output + CAST((@Input % 2) AS varchar)
SET @Input = @Input / 2
END
RETURN REVERSE(@Output)

END
GO

grant execute on dbo.DecimalToBinary to public
go
-- ===
-- END
-- ===
-- ===================
-- DEFAULT CONSTRAINTS
-- 
-- $Author: imikhalyev $
-- $Revision: 1.15 $
-- $Date: 2016-08-25 20:02:32 $
-- ===================

set quoted_identifier on
go

set ansi_nulls on
go

set ansi_padding on
go

if not exists(select null from sys.objects where name = 'DF__BLST__AID')
	alter table blst add constraint DF__BLST__AID default 0 for aid
go
if not exists(select null from sys.objects where name = 'DF__BLST__DES')
	alter table blst add constraint DF__BLST__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__BLST__GRP')
	alter table blst add constraint DF__BLST__GRP default 0 for grp
go
if not exists(select null from sys.objects where name = 'DF__BLST__MID')
	alter table blst add constraint DF__BLST__MID default 0 for mid
go
if not exists(select null from sys.objects where name = 'DF__BLST__SC')
	alter table blst add constraint DF__BLST__SC default 0 for sc
go
if not exists(select null from sys.objects where name = 'DF__BLST__SEQ')
	alter table blst add constraint DF__BLST__SEQ default 0 for seq
go
if not exists(select null from sys.objects where name = 'DF__BLST__TPBC')
	alter table blst add constraint DF__BLST__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__BLST__TS')
	alter table blst add constraint DF__BLST__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__BLST__TYPE')
	alter table blst add constraint DF__BLST__TYPE default 1 for type
go
if not exists(select null from sys.objects where name = 'DF__BUSLST__LOC_N')
	alter table buslst add constraint DF__BUSLST__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__BUSLST__TDAY')
	alter table buslst add constraint DF__BUSLST__TDAY default getdate() for tday
go
if not exists(select null from sys.objects where name = 'DF__BUSLST__TS')
	alter table buslst add constraint DF__BUSLST__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__CCD__DES')
	alter table ccd add constraint DF__CCD__DES default 4 for des
go
if not exists(select null from sys.objects where name = 'DF__CCD__GRP')
	alter table ccd add constraint DF__CCD__GRP default 4 for grp
go
if not exists(select null from sys.objects where name = 'DF__CMDMGR__ERRNO')
	alter table cmdmgr add constraint DF__CMDMGR__ERRNO default 0 for errno
go
if not exists(select null from sys.objects where name = 'DF__CMDMGR__ERRSTR')
	alter table cmdmgr add constraint DF__CMDMGR__ERRSTR default ' ' for errstr
go
if not exists(select null from sys.objects where name = 'DF__CMDMGR__FLAGS')
	alter table cmdmgr add constraint DF__CMDMGR__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__CMDMGR__RETRY')
	alter table cmdmgr add constraint DF__CMDMGR__RETRY default 0 for retry
go
if not exists(select null from sys.objects where name = 'DF__CMDMGR__STATUS')
	alter table cmdmgr add constraint DF__CMDMGR__STATUS default 0 for status
go
if not exists(select null from sys.objects where name = 'DF__CMDMGR__SUBMITTED')
	alter table cmdmgr add constraint DF__CMDMGR__SUBMITTED default getdate() for submitted
go
if not exists(select null from sys.objects where name = 'DF__CNF__AUTHORITY')
	alter table cnf add constraint DF__CNF__AUTHORITY default 'GFI Genfare' for authority
go
if not exists(select null from sys.objects where name = 'DF__CNF__BILL_PM')
	alter table cnf add constraint DF__CNF__BILL_PM default 50000 for bill_pm
go
if not exists(select null from sys.objects where name = 'DF__CNF__BINID')
	alter table cnf add constraint DF__CNF__BINID default 1 for binid
go
if not exists(select null from sys.objects where name = 'DF__CNF__CBXID')
	alter table cnf add constraint DF__CNF__CBXID default 1 for cbxid
go
if not exists(select null from sys.objects where name = 'DF__CNF__CBXOUT_CMAX')
	alter table cnf add constraint DF__CNF__CBXOUT_CMAX default 2 for cbxout_cmax
go
if not exists(select null from sys.objects where name = 'DF__CNF__COIN_PEMAX')
	alter table cnf add constraint DF__CNF__COIN_PEMAX default 100 for coin_pemax
go
if not exists(select null from sys.objects where name = 'DF__CNF__COIN_PM')
	alter table cnf add constraint DF__CNF__COIN_PM default 250000 for coin_pm
go
if not exists(select null from sys.objects where name = 'DF__CNF__COLD_CMAX')
	alter table cnf add constraint DF__CNF__COLD_CMAX default 15 for cold_cmax
go
if not exists(select null from sys.objects where name = 'DF__CNF__CUST_N')
	alter table cnf add constraint DF__CNF__CUST_N default 0 for cust_n
go
if not exists(select null from sys.objects where name = 'DF__CNF__DETAILDAYS')
	alter table cnf add constraint DF__CNF__DETAILDAYS default 731 for detaildays
go
if not exists(select null from sys.objects where name = 'DF__CNF__DISPREV')
	alter table cnf add constraint DF__CNF__DISPREV default 0 for disprev
go
if not exists(select null from sys.objects where name = 'DF__CNF__DOOR_CMAX')
	alter table cnf add constraint DF__CNF__DOOR_CMAX default 2 for door_cmax
go
if not exists(select null from sys.objects where name = 'DF__CNF__ENDTDAY')
	alter table cnf add constraint DF__CNF__ENDTDAY default '19000101 04:00:00' for endtday
go
if not exists(select null from sys.objects where name = 'DF__CNF__FARESETS')
	alter table cnf add constraint DF__CNF__FARESETS default 10 for faresets
go
if not exists(select null from sys.objects where name = 'DF__CNF__FBXLOCK')
	alter table cnf add constraint DF__CNF__FBXLOCK default 1 for fbxlock
go
if not exists(select null from sys.objects where name = 'DF__CNF__FBXVER')
	alter table cnf add constraint DF__CNF__FBXVER default 0 for fbxver
go
if not exists(select null from sys.objects where name = 'DF__CNF__FS_ID')
	alter table cnf add constraint DF__CNF__FS_ID default 1 for fs_id
go
if not exists(select null from sys.objects where name = 'DF__CNF__LOC_N')
	alter table cnf add constraint DF__CNF__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__CNF__LOC_NAME')
	alter table cnf add constraint DF__CNF__LOC_NAME default 'Location 1' for loc_name
go
if not exists(select null from sys.objects where name = 'DF__CNF__MODEM')
	alter table cnf add constraint DF__CNF__MODEM default 0 for modem
go
if not exists(select null from sys.objects where name = 'DF__CNF__PASS_PEMAX')
	alter table cnf add constraint DF__CNF__PASS_PEMAX default 100 for pass_pemax
go
if not exists(select null from sys.objects where name = 'DF__CNF__PASS_PM')
	alter table cnf add constraint DF__CNF__PASS_PM default 50000 for pass_pm
go
if not exists(select null from sys.objects where name = 'DF__CNF__PRINTPRB')
	alter table cnf add constraint DF__CNF__PRINTPRB default 0 for printprb
go
if not exists(select null from sys.objects where name = 'DF__CNF__RDR_ID')
	alter table cnf add constraint DF__CNF__RDR_ID default 1 for rdr_id
go
if not exists(select null from sys.objects where name = 'DF__CNF__SUMMARYDAYS')
	alter table cnf add constraint DF__CNF__SUMMARYDAYS default 120 for summarydays
go
if not exists(select null from sys.objects where name = 'DF__CNF__VLT_LAST_SYNC_TS')
	alter table cnf add constraint DF__CNF__VLT_LAST_SYNC_TS default getdate() for vlt_last_sync_ts
go
if not exists(select null from sys.objects where name = 'DF__CNF__WARM_CMAX')
	alter table cnf add constraint DF__CNF__WARM_CMAX default 50 for warm_cmax
go
if not exists(select null from sys.objects where name = 'DF__DRVLST__LOC_N')
	alter table drvlst add constraint DF__DRVLST__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__DS__EXPORTFLAG')
	alter table ds add constraint DF__DS__EXPORTFLAG default 0 for exportflag
go
if not exists(select null from sys.objects where name = 'DF__DS__LASTEXPORT')
	alter table ds add constraint DF__DS__LASTEXPORT default getdate() for lastexport
go
if not exists(select null from sys.objects where name = 'DF__DS__VLT_EXPORT')
	alter table ds add constraint DF__DS__VLT_EXPORT default 0 for vlt_export
go
if not exists(select null from sys.objects where name = 'DF__DS__VLT_EXPORTFLAG')
	alter table ds add constraint DF__DS__VLT_EXPORTFLAG default 0 for vlt_exportflag
go
if not exists(select null from sys.objects where name = 'DF__DS__VLT_LASTEXPORT')
	alter table ds add constraint DF__DS__VLT_LASTEXPORT default getdate() for vlt_lastexport
go
if not exists(select null from sys.objects where name = 'DF__EQLST__BILL_MECH_T')
	alter table eqlst add constraint DF__EQLST__BILL_MECH_T default 0 for bill_mech_t
go
if not exists(select null from sys.objects where name = 'DF__EQLST__BILL_MECH_TS')
	alter table eqlst add constraint DF__EQLST__BILL_MECH_TS default getdate() for bill_mech_ts
go
if not exists(select null from sys.objects where name = 'DF__EQLST__BILL_TRAN_T')
	alter table eqlst add constraint DF__EQLST__BILL_TRAN_T default 0 for bill_tran_t
go
if not exists(select null from sys.objects where name = 'DF__EQLST__BILL_TRAN_TS')
	alter table eqlst add constraint DF__EQLST__BILL_TRAN_TS default getdate() for bill_tran_ts
go
if not exists(select null from sys.objects where name = 'DF__EQLST__COIN_MECH_T')
	alter table eqlst add constraint DF__EQLST__COIN_MECH_T default 0 for coin_mech_t
go
if not exists(select null from sys.objects where name = 'DF__EQLST__COIN_MECH_TS')
	alter table eqlst add constraint DF__EQLST__COIN_MECH_TS default getdate() for coin_mech_ts
go
if not exists(select null from sys.objects where name = 'DF__EQLST__FBX_VER')
	alter table eqlst add constraint DF__EQLST__FBX_VER default 0 for fbx_ver
go
if not exists(select null from sys.objects where name = 'DF__EQLST__SWIPE_RDR_T')
	alter table eqlst add constraint DF__EQLST__SWIPE_RDR_T default 0 for swipe_rdr_t
go
if not exists(select null from sys.objects where name = 'DF__EQLST__SWIPE_RDR_TS')
	alter table eqlst add constraint DF__EQLST__SWIPE_RDR_TS default getdate() for swipe_rdr_ts
go
if not exists(select null from sys.objects where name = 'DF__EQLST__TRIM_N')
	alter table eqlst add constraint DF__EQLST__TRIM_N default 0 for trim_n
go
if not exists(select null from sys.objects where name = 'DF__EQLST__TRIM_T')
	alter table eqlst add constraint DF__EQLST__TRIM_T default 0 for trim_t
go
if not exists(select null from sys.objects where name = 'DF__EQLST__TRIM_TS')
	alter table eqlst add constraint DF__EQLST__TRIM_TS default getdate() for trim_ts
go
if not exists(select null from sys.objects where name = 'DF__EQLST__TRIM_VER')
	alter table eqlst add constraint DF__EQLST__TRIM_VER default 0 for trim_ver
go
if not exists(select null from sys.objects where name = 'DF__EV__BILL_C')
	alter table ev add constraint DF__EV__BILL_C default 0 for bill_c
go
if not exists(select null from sys.objects where name = 'DF__EV__CURR_R')
	alter table ev add constraint DF__EV__CURR_R default 0 for curr_r
go
if not exists(select null from sys.objects where name = 'DF__EV__DRV')
	alter table ev add constraint DF__EV__DRV default 0 for drv
go
if not exists(select null from sys.objects where name = 'DF__EV__DUMP_C')
	alter table ev add constraint DF__EV__DUMP_C default 0 for dump_c
go
if not exists(select null from sys.objects where name = 'DF__EV__FARE_C')
	alter table ev add constraint DF__EV__FARE_C default 0 for fare_c
go
if not exists(select null from sys.objects where name = 'DF__EV__FS')
	alter table ev add constraint DF__EV__FS default 0 for fs
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY1')
	alter table ev add constraint DF__EV__KEY1 default 0 for key1
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY2')
	alter table ev add constraint DF__EV__KEY2 default 0 for key2
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY3')
	alter table ev add constraint DF__EV__KEY3 default 0 for key3
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY4')
	alter table ev add constraint DF__EV__KEY4 default 0 for key4
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY5')
	alter table ev add constraint DF__EV__KEY5 default 0 for key5
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY6')
	alter table ev add constraint DF__EV__KEY6 default 0 for key6
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY7')
	alter table ev add constraint DF__EV__KEY7 default 0 for key7
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY8')
	alter table ev add constraint DF__EV__KEY8 default 0 for key8
go
if not exists(select null from sys.objects where name = 'DF__EV__KEY9')
	alter table ev add constraint DF__EV__KEY9 default 0 for key9
go
if not exists(select null from sys.objects where name = 'DF__EV__KEYA')
	alter table ev add constraint DF__EV__KEYA default 0 for keya
go
if not exists(select null from sys.objects where name = 'DF__EV__KEYAST')
	alter table ev add constraint DF__EV__KEYAST default 0 for keyast
go
if not exists(select null from sys.objects where name = 'DF__EV__KEYB')
	alter table ev add constraint DF__EV__KEYB default 0 for keyb
go
if not exists(select null from sys.objects where name = 'DF__EV__KEYC')
	alter table ev add constraint DF__EV__KEYC default 0 for keyc
go
if not exists(select null from sys.objects where name = 'DF__EV__KEYD')
	alter table ev add constraint DF__EV__KEYD default 0 for keyd
go
if not exists(select null from sys.objects where name = 'DF__EV__N')
	alter table ev add constraint DF__EV__N default 0 for n
go
if not exists(select null from sys.objects where name = 'DF__EV__PASS_C')
	alter table ev add constraint DF__EV__PASS_C default 0 for pass_c
go
if not exists(select null from sys.objects where name = 'DF__EV__RDR_C')
	alter table ev add constraint DF__EV__RDR_C default 0 for rdr_c
go
if not exists(select null from sys.objects where name = 'DF__EV__ROUTE')
	alter table ev add constraint DF__EV__ROUTE default 0 for route
go
if not exists(select null from sys.objects where name = 'DF__EV__RUN')
	alter table ev add constraint DF__EV__RUN default 0 for run
go
if not exists(select null from sys.objects where name = 'DF__EV__TICKET_C')
	alter table ev add constraint DF__EV__TICKET_C default 0 for ticket_c
go
if not exists(select null from sys.objects where name = 'DF__EV__TOKEN_C')
	alter table ev add constraint DF__EV__TOKEN_C default 0 for token_c
go
if not exists(select null from sys.objects where name = 'DF__EV__TRIP')
	alter table ev add constraint DF__EV__TRIP default 0 for trip
go
if not exists(select null from sys.objects where name = 'DF__EV__TS')
	alter table ev add constraint DF__EV__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP1')
	alter table ev add constraint DF__EV__TTP1 default 0 for ttp1
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP10')
	alter table ev add constraint DF__EV__TTP10 default 0 for ttp10
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP11')
	alter table ev add constraint DF__EV__TTP11 default 0 for ttp11
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP12')
	alter table ev add constraint DF__EV__TTP12 default 0 for ttp12
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP13')
	alter table ev add constraint DF__EV__TTP13 default 0 for ttp13
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP14')
	alter table ev add constraint DF__EV__TTP14 default 0 for ttp14
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP15')
	alter table ev add constraint DF__EV__TTP15 default 0 for ttp15
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP16')
	alter table ev add constraint DF__EV__TTP16 default 0 for ttp16
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP17')
	alter table ev add constraint DF__EV__TTP17 default 0 for ttp17
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP18')
	alter table ev add constraint DF__EV__TTP18 default 0 for ttp18
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP19')
	alter table ev add constraint DF__EV__TTP19 default 0 for ttp19
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP2')
	alter table ev add constraint DF__EV__TTP2 default 0 for ttp2
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP20')
	alter table ev add constraint DF__EV__TTP20 default 0 for ttp20
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP21')
	alter table ev add constraint DF__EV__TTP21 default 0 for ttp21
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP22')
	alter table ev add constraint DF__EV__TTP22 default 0 for ttp22
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP23')
	alter table ev add constraint DF__EV__TTP23 default 0 for ttp23
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP24')
	alter table ev add constraint DF__EV__TTP24 default 0 for ttp24
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP25')
	alter table ev add constraint DF__EV__TTP25 default 0 for ttp25
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP26')
	alter table ev add constraint DF__EV__TTP26 default 0 for ttp26
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP27')
	alter table ev add constraint DF__EV__TTP27 default 0 for ttp27
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP28')
	alter table ev add constraint DF__EV__TTP28 default 0 for ttp28
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP29')
	alter table ev add constraint DF__EV__TTP29 default 0 for ttp29
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP3')
	alter table ev add constraint DF__EV__TTP3 default 0 for ttp3
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP30')
	alter table ev add constraint DF__EV__TTP30 default 0 for ttp30
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP31')
	alter table ev add constraint DF__EV__TTP31 default 0 for ttp31
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP32')
	alter table ev add constraint DF__EV__TTP32 default 0 for ttp32
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP33')
	alter table ev add constraint DF__EV__TTP33 default 0 for ttp33
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP34')
	alter table ev add constraint DF__EV__TTP34 default 0 for ttp34
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP35')
	alter table ev add constraint DF__EV__TTP35 default 0 for ttp35
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP36')
	alter table ev add constraint DF__EV__TTP36 default 0 for ttp36
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP37')
	alter table ev add constraint DF__EV__TTP37 default 0 for ttp37
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP38')
	alter table ev add constraint DF__EV__TTP38 default 0 for ttp38
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP39')
	alter table ev add constraint DF__EV__TTP39 default 0 for ttp39
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP4')
	alter table ev add constraint DF__EV__TTP4 default 0 for ttp4
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP40')
	alter table ev add constraint DF__EV__TTP40 default 0 for ttp40
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP41')
	alter table ev add constraint DF__EV__TTP41 default 0 for ttp41
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP42')
	alter table ev add constraint DF__EV__TTP42 default 0 for ttp42
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP43')
	alter table ev add constraint DF__EV__TTP43 default 0 for ttp43
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP44')
	alter table ev add constraint DF__EV__TTP44 default 0 for ttp44
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP45')
	alter table ev add constraint DF__EV__TTP45 default 0 for ttp45
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP46')
	alter table ev add constraint DF__EV__TTP46 default 0 for ttp46
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP47')
	alter table ev add constraint DF__EV__TTP47 default 0 for ttp47
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP48')
	alter table ev add constraint DF__EV__TTP48 default 0 for ttp48
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP5')
	alter table ev add constraint DF__EV__TTP5 default 0 for ttp5
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP6')
	alter table ev add constraint DF__EV__TTP6 default 0 for ttp6
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP7')
	alter table ev add constraint DF__EV__TTP7 default 0 for ttp7
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP8')
	alter table ev add constraint DF__EV__TTP8 default 0 for ttp8
go
if not exists(select null from sys.objects where name = 'DF__EV__TTP9')
	alter table ev add constraint DF__EV__TTP9 default 0 for ttp9
go
if not exists(select null from sys.objects where name = 'DF__EV__TYPE')
	alter table ev add constraint DF__EV__TYPE default 0 for type
go
if not exists(select null from sys.objects where name = 'DF__EV__UNCL_R')
	alter table ev add constraint DF__EV__UNCL_R default 0 for uncl_r
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP49')
	alter table ev_extd add constraint DF__EV_EXTD__TTP49 default 0 for ttp49
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP50')
	alter table ev_extd add constraint DF__EV_EXTD__TTP50 default 0 for ttp50
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP51')
	alter table ev_extd add constraint DF__EV_EXTD__TTP51 default 0 for ttp51
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP52')
	alter table ev_extd add constraint DF__EV_EXTD__TTP52 default 0 for ttp52
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP53')
	alter table ev_extd add constraint DF__EV_EXTD__TTP53 default 0 for ttp53
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP54')
	alter table ev_extd add constraint DF__EV_EXTD__TTP54 default 0 for ttp54
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP55')
	alter table ev_extd add constraint DF__EV_EXTD__TTP55 default 0 for ttp55
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP56')
	alter table ev_extd add constraint DF__EV_EXTD__TTP56 default 0 for ttp56
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP57')
	alter table ev_extd add constraint DF__EV_EXTD__TTP57 default 0 for ttp57
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP58')
	alter table ev_extd add constraint DF__EV_EXTD__TTP58 default 0 for ttp58
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP59')
	alter table ev_extd add constraint DF__EV_EXTD__TTP59 default 0 for ttp59
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP60')
	alter table ev_extd add constraint DF__EV_EXTD__TTP60 default 0 for ttp60
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP61')
	alter table ev_extd add constraint DF__EV_EXTD__TTP61 default 0 for ttp61
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP62')
	alter table ev_extd add constraint DF__EV_EXTD__TTP62 default 0 for ttp62
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP63')
	alter table ev_extd add constraint DF__EV_EXTD__TTP63 default 0 for ttp63
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP64')
	alter table ev_extd add constraint DF__EV_EXTD__TTP64 default 0 for ttp64
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP65')
	alter table ev_extd add constraint DF__EV_EXTD__TTP65 default 0 for ttp65
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP66')
	alter table ev_extd add constraint DF__EV_EXTD__TTP66 default 0 for ttp66
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP67')
	alter table ev_extd add constraint DF__EV_EXTD__TTP67 default 0 for ttp67
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP68')
	alter table ev_extd add constraint DF__EV_EXTD__TTP68 default 0 for ttp68
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP69')
	alter table ev_extd add constraint DF__EV_EXTD__TTP69 default 0 for ttp69
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP70')
	alter table ev_extd add constraint DF__EV_EXTD__TTP70 default 0 for ttp70
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP71')
	alter table ev_extd add constraint DF__EV_EXTD__TTP71 default 0 for ttp71
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP72')
	alter table ev_extd add constraint DF__EV_EXTD__TTP72 default 0 for ttp72
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP73')
	alter table ev_extd add constraint DF__EV_EXTD__TTP73 default 0 for ttp73
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP74')
	alter table ev_extd add constraint DF__EV_EXTD__TTP74 default 0 for ttp74
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP75')
	alter table ev_extd add constraint DF__EV_EXTD__TTP75 default 0 for ttp75
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP76')
	alter table ev_extd add constraint DF__EV_EXTD__TTP76 default 0 for ttp76
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP77')
	alter table ev_extd add constraint DF__EV_EXTD__TTP77 default 0 for ttp77
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP78')
	alter table ev_extd add constraint DF__EV_EXTD__TTP78 default 0 for ttp78
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP79')
	alter table ev_extd add constraint DF__EV_EXTD__TTP79 default 0 for ttp79
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP80')
	alter table ev_extd add constraint DF__EV_EXTD__TTP80 default 0 for ttp80
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP81')
	alter table ev_extd add constraint DF__EV_EXTD__TTP81 default 0 for ttp81
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP82')
	alter table ev_extd add constraint DF__EV_EXTD__TTP82 default 0 for ttp82
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP83')
	alter table ev_extd add constraint DF__EV_EXTD__TTP83 default 0 for ttp83
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP84')
	alter table ev_extd add constraint DF__EV_EXTD__TTP84 default 0 for ttp84
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP85')
	alter table ev_extd add constraint DF__EV_EXTD__TTP85 default 0 for ttp85
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP86')
	alter table ev_extd add constraint DF__EV_EXTD__TTP86 default 0 for ttp86
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP87')
	alter table ev_extd add constraint DF__EV_EXTD__TTP87 default 0 for ttp87
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP88')
	alter table ev_extd add constraint DF__EV_EXTD__TTP88 default 0 for ttp88
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP89')
	alter table ev_extd add constraint DF__EV_EXTD__TTP89 default 0 for ttp89
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP90')
	alter table ev_extd add constraint DF__EV_EXTD__TTP90 default 0 for ttp90
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP91')
	alter table ev_extd add constraint DF__EV_EXTD__TTP91 default 0 for ttp91
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP92')
	alter table ev_extd add constraint DF__EV_EXTD__TTP92 default 0 for ttp92
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP93')
	alter table ev_extd add constraint DF__EV_EXTD__TTP93 default 0 for ttp93
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP94')
	alter table ev_extd add constraint DF__EV_EXTD__TTP94 default 0 for ttp94
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP95')
	alter table ev_extd add constraint DF__EV_EXTD__TTP95 default 0 for ttp95
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP96')
	alter table ev_extd add constraint DF__EV_EXTD__TTP96 default 0 for ttp96
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP97')
	alter table ev_extd add constraint DF__EV_EXTD__TTP97 default 0 for ttp97
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP98')
	alter table ev_extd add constraint DF__EV_EXTD__TTP98 default 0 for ttp98
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP99')
	alter table ev_extd add constraint DF__EV_EXTD__TTP99 default 0 for ttp99
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP100')
	alter table ev_extd add constraint DF__EV_EXTD__TTP100 default 0 for ttp100
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP101')
	alter table ev_extd add constraint DF__EV_EXTD__TTP101 default 0 for ttp101
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP102')
	alter table ev_extd add constraint DF__EV_EXTD__TTP102 default 0 for ttp102
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP103')
	alter table ev_extd add constraint DF__EV_EXTD__TTP103 default 0 for ttp103
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP104')
	alter table ev_extd add constraint DF__EV_EXTD__TTP104 default 0 for ttp104
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP105')
	alter table ev_extd add constraint DF__EV_EXTD__TTP105 default 0 for ttp105
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP106')
	alter table ev_extd add constraint DF__EV_EXTD__TTP106 default 0 for ttp106
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP107')
	alter table ev_extd add constraint DF__EV_EXTD__TTP107 default 0 for ttp107
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP108')
	alter table ev_extd add constraint DF__EV_EXTD__TTP108 default 0 for ttp108
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP109')
	alter table ev_extd add constraint DF__EV_EXTD__TTP109 default 0 for ttp109
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP110')
	alter table ev_extd add constraint DF__EV_EXTD__TTP110 default 0 for ttp110
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP111')
	alter table ev_extd add constraint DF__EV_EXTD__TTP111 default 0 for ttp111
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP112')
	alter table ev_extd add constraint DF__EV_EXTD__TTP112 default 0 for ttp112
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP113')
	alter table ev_extd add constraint DF__EV_EXTD__TTP113 default 0 for ttp113
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP114')
	alter table ev_extd add constraint DF__EV_EXTD__TTP114 default 0 for ttp114
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP115')
	alter table ev_extd add constraint DF__EV_EXTD__TTP115 default 0 for ttp115
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP116')
	alter table ev_extd add constraint DF__EV_EXTD__TTP116 default 0 for ttp116
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP117')
	alter table ev_extd add constraint DF__EV_EXTD__TTP117 default 0 for ttp117
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP118')
	alter table ev_extd add constraint DF__EV_EXTD__TTP118 default 0 for ttp118
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP119')
	alter table ev_extd add constraint DF__EV_EXTD__TTP119 default 0 for ttp119
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP120')
	alter table ev_extd add constraint DF__EV_EXTD__TTP120 default 0 for ttp120
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP121')
	alter table ev_extd add constraint DF__EV_EXTD__TTP121 default 0 for ttp121
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP122')
	alter table ev_extd add constraint DF__EV_EXTD__TTP122 default 0 for ttp122
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP123')
	alter table ev_extd add constraint DF__EV_EXTD__TTP123 default 0 for ttp123
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP124')
	alter table ev_extd add constraint DF__EV_EXTD__TTP124 default 0 for ttp124
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP125')
	alter table ev_extd add constraint DF__EV_EXTD__TTP125 default 0 for ttp125
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP126')
	alter table ev_extd add constraint DF__EV_EXTD__TTP126 default 0 for ttp126
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP127')
	alter table ev_extd add constraint DF__EV_EXTD__TTP127 default 0 for ttp127
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP128')
	alter table ev_extd add constraint DF__EV_EXTD__TTP128 default 0 for ttp128
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP129')
	alter table ev_extd add constraint DF__EV_EXTD__TTP129 default 0 for ttp129
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP130')
	alter table ev_extd add constraint DF__EV_EXTD__TTP130 default 0 for ttp130
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP131')
	alter table ev_extd add constraint DF__EV_EXTD__TTP131 default 0 for ttp131
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP132')
	alter table ev_extd add constraint DF__EV_EXTD__TTP132 default 0 for ttp132
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP133')
	alter table ev_extd add constraint DF__EV_EXTD__TTP133 default 0 for ttp133
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP134')
	alter table ev_extd add constraint DF__EV_EXTD__TTP134 default 0 for ttp134
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP135')
	alter table ev_extd add constraint DF__EV_EXTD__TTP135 default 0 for ttp135
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP136')
	alter table ev_extd add constraint DF__EV_EXTD__TTP136 default 0 for ttp136
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP137')
	alter table ev_extd add constraint DF__EV_EXTD__TTP137 default 0 for ttp137
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP138')
	alter table ev_extd add constraint DF__EV_EXTD__TTP138 default 0 for ttp138
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP139')
	alter table ev_extd add constraint DF__EV_EXTD__TTP139 default 0 for ttp139
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP140')
	alter table ev_extd add constraint DF__EV_EXTD__TTP140 default 0 for ttp140
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP141')
	alter table ev_extd add constraint DF__EV_EXTD__TTP141 default 0 for ttp141
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP142')
	alter table ev_extd add constraint DF__EV_EXTD__TTP142 default 0 for ttp142
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP143')
	alter table ev_extd add constraint DF__EV_EXTD__TTP143 default 0 for ttp143
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP144')
	alter table ev_extd add constraint DF__EV_EXTD__TTP144 default 0 for ttp144
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP145')
	alter table ev_extd add constraint DF__EV_EXTD__TTP145 default 0 for ttp145
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP146')
	alter table ev_extd add constraint DF__EV_EXTD__TTP146 default 0 for ttp146
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP147')
	alter table ev_extd add constraint DF__EV_EXTD__TTP147 default 0 for ttp147
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP148')
	alter table ev_extd add constraint DF__EV_EXTD__TTP148 default 0 for ttp148
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP149')
	alter table ev_extd add constraint DF__EV_EXTD__TTP149 default 0 for ttp149
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP150')
	alter table ev_extd add constraint DF__EV_EXTD__TTP150 default 0 for ttp150
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP151')
	alter table ev_extd add constraint DF__EV_EXTD__TTP151 default 0 for ttp151
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP152')
	alter table ev_extd add constraint DF__EV_EXTD__TTP152 default 0 for ttp152
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP153')
	alter table ev_extd add constraint DF__EV_EXTD__TTP153 default 0 for ttp153
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP154')
	alter table ev_extd add constraint DF__EV_EXTD__TTP154 default 0 for ttp154
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP155')
	alter table ev_extd add constraint DF__EV_EXTD__TTP155 default 0 for ttp155
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP156')
	alter table ev_extd add constraint DF__EV_EXTD__TTP156 default 0 for ttp156
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP157')
	alter table ev_extd add constraint DF__EV_EXTD__TTP157 default 0 for ttp157
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP158')
	alter table ev_extd add constraint DF__EV_EXTD__TTP158 default 0 for ttp158
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP159')
	alter table ev_extd add constraint DF__EV_EXTD__TTP159 default 0 for ttp159
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP160')
	alter table ev_extd add constraint DF__EV_EXTD__TTP160 default 0 for ttp160
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP161')
	alter table ev_extd add constraint DF__EV_EXTD__TTP161 default 0 for ttp161
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP162')
	alter table ev_extd add constraint DF__EV_EXTD__TTP162 default 0 for ttp162
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP163')
	alter table ev_extd add constraint DF__EV_EXTD__TTP163 default 0 for ttp163
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP164')
	alter table ev_extd add constraint DF__EV_EXTD__TTP164 default 0 for ttp164
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP165')
	alter table ev_extd add constraint DF__EV_EXTD__TTP165 default 0 for ttp165
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP166')
	alter table ev_extd add constraint DF__EV_EXTD__TTP166 default 0 for ttp166
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP167')
	alter table ev_extd add constraint DF__EV_EXTD__TTP167 default 0 for ttp167
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP168')
	alter table ev_extd add constraint DF__EV_EXTD__TTP168 default 0 for ttp168
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP169')
	alter table ev_extd add constraint DF__EV_EXTD__TTP169 default 0 for ttp169
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP170')
	alter table ev_extd add constraint DF__EV_EXTD__TTP170 default 0 for ttp170
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP171')
	alter table ev_extd add constraint DF__EV_EXTD__TTP171 default 0 for ttp171
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP172')
	alter table ev_extd add constraint DF__EV_EXTD__TTP172 default 0 for ttp172
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP173')
	alter table ev_extd add constraint DF__EV_EXTD__TTP173 default 0 for ttp173
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP174')
	alter table ev_extd add constraint DF__EV_EXTD__TTP174 default 0 for ttp174
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP175')
	alter table ev_extd add constraint DF__EV_EXTD__TTP175 default 0 for ttp175
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP176')
	alter table ev_extd add constraint DF__EV_EXTD__TTP176 default 0 for ttp176
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP177')
	alter table ev_extd add constraint DF__EV_EXTD__TTP177 default 0 for ttp177
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP178')
	alter table ev_extd add constraint DF__EV_EXTD__TTP178 default 0 for ttp178
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP179')
	alter table ev_extd add constraint DF__EV_EXTD__TTP179 default 0 for ttp179
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP180')
	alter table ev_extd add constraint DF__EV_EXTD__TTP180 default 0 for ttp180
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP181')
	alter table ev_extd add constraint DF__EV_EXTD__TTP181 default 0 for ttp181
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP182')
	alter table ev_extd add constraint DF__EV_EXTD__TTP182 default 0 for ttp182
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP183')
	alter table ev_extd add constraint DF__EV_EXTD__TTP183 default 0 for ttp183
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP184')
	alter table ev_extd add constraint DF__EV_EXTD__TTP184 default 0 for ttp184
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP185')
	alter table ev_extd add constraint DF__EV_EXTD__TTP185 default 0 for ttp185
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP186')
	alter table ev_extd add constraint DF__EV_EXTD__TTP186 default 0 for ttp186
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP187')
	alter table ev_extd add constraint DF__EV_EXTD__TTP187 default 0 for ttp187
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP188')
	alter table ev_extd add constraint DF__EV_EXTD__TTP188 default 0 for ttp188
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP189')
	alter table ev_extd add constraint DF__EV_EXTD__TTP189 default 0 for ttp189
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP190')
	alter table ev_extd add constraint DF__EV_EXTD__TTP190 default 0 for ttp190
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP191')
	alter table ev_extd add constraint DF__EV_EXTD__TTP191 default 0 for ttp191
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP192')
	alter table ev_extd add constraint DF__EV_EXTD__TTP192 default 0 for ttp192
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP193')
	alter table ev_extd add constraint DF__EV_EXTD__TTP193 default 0 for ttp193
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP194')
	alter table ev_extd add constraint DF__EV_EXTD__TTP194 default 0 for ttp194
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP195')
	alter table ev_extd add constraint DF__EV_EXTD__TTP195 default 0 for ttp195
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP196')
	alter table ev_extd add constraint DF__EV_EXTD__TTP196 default 0 for ttp196
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP197')
	alter table ev_extd add constraint DF__EV_EXTD__TTP197 default 0 for ttp197
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP198')
	alter table ev_extd add constraint DF__EV_EXTD__TTP198 default 0 for ttp198
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP199')
	alter table ev_extd add constraint DF__EV_EXTD__TTP199 default 0 for ttp199
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP200')
	alter table ev_extd add constraint DF__EV_EXTD__TTP200 default 0 for ttp200
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP201')
	alter table ev_extd add constraint DF__EV_EXTD__TTP201 default 0 for ttp201
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP202')
	alter table ev_extd add constraint DF__EV_EXTD__TTP202 default 0 for ttp202
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP203')
	alter table ev_extd add constraint DF__EV_EXTD__TTP203 default 0 for ttp203
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP204')
	alter table ev_extd add constraint DF__EV_EXTD__TTP204 default 0 for ttp204
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP205')
	alter table ev_extd add constraint DF__EV_EXTD__TTP205 default 0 for ttp205
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP206')
	alter table ev_extd add constraint DF__EV_EXTD__TTP206 default 0 for ttp206
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP207')
	alter table ev_extd add constraint DF__EV_EXTD__TTP207 default 0 for ttp207
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP208')
	alter table ev_extd add constraint DF__EV_EXTD__TTP208 default 0 for ttp208
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP209')
	alter table ev_extd add constraint DF__EV_EXTD__TTP209 default 0 for ttp209
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP210')
	alter table ev_extd add constraint DF__EV_EXTD__TTP210 default 0 for ttp210
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP211')
	alter table ev_extd add constraint DF__EV_EXTD__TTP211 default 0 for ttp211
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP212')
	alter table ev_extd add constraint DF__EV_EXTD__TTP212 default 0 for ttp212
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP213')
	alter table ev_extd add constraint DF__EV_EXTD__TTP213 default 0 for ttp213
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP214')
	alter table ev_extd add constraint DF__EV_EXTD__TTP214 default 0 for ttp214
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP215')
	alter table ev_extd add constraint DF__EV_EXTD__TTP215 default 0 for ttp215
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP216')
	alter table ev_extd add constraint DF__EV_EXTD__TTP216 default 0 for ttp216
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP217')
	alter table ev_extd add constraint DF__EV_EXTD__TTP217 default 0 for ttp217
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP218')
	alter table ev_extd add constraint DF__EV_EXTD__TTP218 default 0 for ttp218
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP219')
	alter table ev_extd add constraint DF__EV_EXTD__TTP219 default 0 for ttp219
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP220')
	alter table ev_extd add constraint DF__EV_EXTD__TTP220 default 0 for ttp220
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP221')
	alter table ev_extd add constraint DF__EV_EXTD__TTP221 default 0 for ttp221
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP222')
	alter table ev_extd add constraint DF__EV_EXTD__TTP222 default 0 for ttp222
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP223')
	alter table ev_extd add constraint DF__EV_EXTD__TTP223 default 0 for ttp223
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP224')
	alter table ev_extd add constraint DF__EV_EXTD__TTP224 default 0 for ttp224
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP225')
	alter table ev_extd add constraint DF__EV_EXTD__TTP225 default 0 for ttp225
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP226')
	alter table ev_extd add constraint DF__EV_EXTD__TTP226 default 0 for ttp226
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP227')
	alter table ev_extd add constraint DF__EV_EXTD__TTP227 default 0 for ttp227
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP228')
	alter table ev_extd add constraint DF__EV_EXTD__TTP228 default 0 for ttp228
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP229')
	alter table ev_extd add constraint DF__EV_EXTD__TTP229 default 0 for ttp229
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP230')
	alter table ev_extd add constraint DF__EV_EXTD__TTP230 default 0 for ttp230
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP231')
	alter table ev_extd add constraint DF__EV_EXTD__TTP231 default 0 for ttp231
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP232')
	alter table ev_extd add constraint DF__EV_EXTD__TTP232 default 0 for ttp232
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP233')
	alter table ev_extd add constraint DF__EV_EXTD__TTP233 default 0 for ttp233
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP234')
	alter table ev_extd add constraint DF__EV_EXTD__TTP234 default 0 for ttp234
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP235')
	alter table ev_extd add constraint DF__EV_EXTD__TTP235 default 0 for ttp235
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP236')
	alter table ev_extd add constraint DF__EV_EXTD__TTP236 default 0 for ttp236
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP237')
	alter table ev_extd add constraint DF__EV_EXTD__TTP237 default 0 for ttp237
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP238')
	alter table ev_extd add constraint DF__EV_EXTD__TTP238 default 0 for ttp238
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP239')
	alter table ev_extd add constraint DF__EV_EXTD__TTP239 default 0 for ttp239
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP240')
	alter table ev_extd add constraint DF__EV_EXTD__TTP240 default 0 for ttp240
go
if not exists(select null from sys.objects where name = 'DF__EV_EXTD__TTP241')
	alter table ev_extd add constraint DF__EV_EXTD__TTP241 default 0 for ttp241
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__ATTR')
	alter table farecell add constraint DF__FARECELL__ATTR default 0 for attr
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__ENABLED_F')
	alter table farecell add constraint DF__FARECELL__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__FLAGS')
	alter table farecell add constraint DF__FARECELL__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__INCLUDED_F')
	alter table farecell add constraint DF__FARECELL__INCLUDED_F default 'Y' for included_f
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__LED1')
	alter table farecell add constraint DF__FARECELL__LED1 default 0 for led1
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__LED2')
	alter table farecell add constraint DF__FARECELL__LED2 default 0 for led2
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__M_ENABLED_F')
	alter table farecell add constraint DF__FARECELL__M_ENABLED_F default 'N' for m_enabled_f
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__M_NDX')
	alter table farecell add constraint DF__FARECELL__M_NDX default 0 for m_ndx
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__SND1')
	alter table farecell add constraint DF__FARECELL__SND1 default 15 for snd1
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__SND2')
	alter table farecell add constraint DF__FARECELL__SND2 default 0 for snd2
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__T_ENABLED_F')
	alter table farecell add constraint DF__FARECELL__T_ENABLED_F default 'N' for t_enabled_f
go
if not exists(select null from sys.objects where name = 'DF__FARECELL__T_NDX')
	alter table farecell add constraint DF__FARECELL__T_NDX default 0 for t_ndx
go
if not exists(select null from sys.objects where name = 'DF__FARESET__ENABLED_F')
	alter table fareset add constraint DF__FARESET__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__BOOT_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__BOOT_V default 0 for boot_v
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__BV_N')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__BV_N default 0 for bv_n
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__BV_P')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__BV_P default 0 for bv_p
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__BV_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__BV_V default 0 for bv_v
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__FBX_N')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__FBX_N default 0 for fbx_n
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__FBX_P')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__FBX_P default 0 for fbx_p
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__FBX_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__FBX_V default 0 for fbx_v
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__MUX_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__MUX_V default 0 for mux_v
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__OCU_N')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__OCU_N default 0 for ocu_n
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__OCU_P')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__OCU_P default 0 for ocu_p
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__OCU_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__OCU_V default 0 for ocu_v
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__SMART_N')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__SMART_N default 0 for smart_n
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__SMART_P')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__SMART_P default 0 for smart_p
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__SMART_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__SMART_V default 0 for smart_v
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__TRIM_N')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__TRIM_N default 0 for trim_n
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__TRIM_P')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__TRIM_P default 0 for trim_p
go
if not exists(select null from sys.objects where name = 'DF__FIRMWARE_INFO__TRIM_V')
	alter table firmware_info add constraint DF__FIRMWARE_INFO__TRIM_V default 0 for trim_v
go
if not exists(select null from sys.objects where name = 'DF__FSC__ALT_KEY')
	alter table fsc add constraint DF__FSC__ALT_KEY default '*' for alt_key
go
if not exists(select null from sys.objects where name = 'DF__FSC__CREATED_TS')
	alter table fsc add constraint DF__FSC__CREATED_TS default getdate() for created_ts
go
if not exists(select null from sys.objects where name = 'DF__FSC__CREATED_USERID')
	alter table fsc add constraint DF__FSC__CREATED_USERID default 'gfi' for created_userid
go
if not exists(select null from sys.objects where name = 'DF__FSC__EFFECTIVE_TS')
	alter table fsc add constraint DF__FSC__EFFECTIVE_TS default getdate() for effective_ts
go
if not exists(select null from sys.objects where name = 'DF__FSC__FARESET_C')
	alter table fsc add constraint DF__FSC__FARESET_C default 10 for fareset_c
go
if not exists(select null from sys.objects where name = 'DF__FSC__LOCKCODE_F')
	alter table fsc add constraint DF__FSC__LOCKCODE_F default 2 for lockcode_f
go
if not exists(select null from sys.objects where name = 'DF__FSC__MODIFIED_TS')
	alter table fsc add constraint DF__FSC__MODIFIED_TS default getdate() for modified_ts
go
if not exists(select null from sys.objects where name = 'DF__FSC__MODIFIED_USERID')
	alter table fsc add constraint DF__FSC__MODIFIED_USERID default 'gfi' for modified_userid
go
if not exists(select null from sys.objects where name = 'DF__FSC__PEAK1OFF')
	alter table fsc add constraint DF__FSC__PEAK1OFF default '19000101' for peak1off
go
if not exists(select null from sys.objects where name = 'DF__FSC__PEAK1ON')
	alter table fsc add constraint DF__FSC__PEAK1ON default '19000101' for peak1on
go
if not exists(select null from sys.objects where name = 'DF__FSC__PEAK2OFF')
	alter table fsc add constraint DF__FSC__PEAK2OFF default '19000101' for peak2off
go
if not exists(select null from sys.objects where name = 'DF__FSC__PEAK2ON')
	alter table fsc add constraint DF__FSC__PEAK2ON default '19000101' for peak2on
go
if not exists(select null from sys.objects where name = 'DF__FSC__STORE_F')
	alter table fsc add constraint DF__FSC__STORE_F default 'Y' for store_f
go
if not exists(select null from sys.objects where name = 'DF__FSC__VER')
	alter table fsc add constraint DF__FSC__VER default 0 for ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_DB_VER__V1')
	alter table gfi_db_ver add constraint DF__GFI_DB_VER__V1 default 0 for v1
go
if not exists(select null from sys.objects where name = 'DF__GFI_DB_VER__V2')
	alter table gfi_db_ver add constraint DF__GFI_DB_VER__V2 default 0 for v2
go
if not exists(select null from sys.objects where name = 'DF__GFI_DB_VER__V3')
	alter table gfi_db_ver add constraint DF__GFI_DB_VER__V3 default 0 for v3
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__CHANGE')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__CHANGE default 0 for change
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__EQ_TYPE')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__EQ_TYPE default 0 for eq_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__PAY_TYPE')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__PAY_TYPE default 0 for pay_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__PENDING')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__PRICE')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__PRICE default 0 for price
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__PROD_ID')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__PROD_TYPE')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__PROD_TYPE default 0 for prod_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_BUS__VALUE')
	alter table gfi_epay_act_bus add constraint DF__GFI_EPAY_ACT_BUS__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__CHANGE')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__CHANGE default 0 for change
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__PAY_TYPE')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__PAY_TYPE default 0 for pay_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__PENDING')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__PRICE')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__PRICE default 0 for price
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__PROD_ID')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__PROD_TYPE')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__PROD_TYPE default 0 for prod_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_EQ__VALUE')
	alter table gfi_epay_act_eq add constraint DF__GFI_EPAY_ACT_EQ__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__CARD_ID')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__CARD_ID default 0 for card_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__CHANGE')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__CHANGE default 0 for change
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__PAY_TYPE')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__PAY_TYPE default 0 for pay_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__PENDING')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__PRICE')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__PRICE default 0 for price
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__PROD_ID')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__PROD_TYPE')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__PROD_TYPE default 0 for prod_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__REC_ID')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__REC_ID default 0 for rec_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ACT_ORDER__VALUE')
	alter table gfi_epay_act_order add constraint DF__GFI_EPAY_ACT_ORDER__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD__FARE_ID')
	alter table gfi_epay_autoload add constraint DF__GFI_EPAY_AUTOLOAD__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD__REC_ID')
	alter table gfi_epay_autoload add constraint DF__GFI_EPAY_AUTOLOAD__REC_ID default 0 for rec_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD__TPBC')
	alter table gfi_epay_autoload add constraint DF__GFI_EPAY_AUTOLOAD__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD__VALUE')
	alter table gfi_epay_autoload add constraint DF__GFI_EPAY_AUTOLOAD__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__ACTIVE')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__ACTIVE default 1 for active
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__ADDTIME')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__ADDTIME default 0 for addtime
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__CARD_TYPE')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__CARD_TYPE default 0 for card_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__EXP_DATE')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__EXP_DATE default 0 for exp_date
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__EXP_TYPE')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__EXP_TYPE default 0 for exp_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__FLAG')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__FLAG default 127 for flag
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_FARE__START_DATE')
	alter table gfi_epay_autoload_fare add constraint DF__GFI_EPAY_AUTOLOAD_FARE__START_DATE default 0 for start_date
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_PARM__N')
	alter table gfi_epay_autoload_parm add constraint DF__GFI_EPAY_AUTOLOAD_PARM__N default 0 for n
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_PKG__START_TS')
	alter table gfi_epay_autoload_pkg add constraint DF__GFI_EPAY_AUTOLOAD_PKG__START_TS default getdate() for start_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_RANGE__FARE_ID')
	alter table gfi_epay_autoload_range add constraint DF__GFI_EPAY_AUTOLOAD_RANGE__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_RANGE__TPBC')
	alter table gfi_epay_autoload_range add constraint DF__GFI_EPAY_AUTOLOAD_RANGE__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_RANGE__VALUE')
	alter table gfi_epay_autoload_range add constraint DF__GFI_EPAY_AUTOLOAD_RANGE__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_SEQ__LOAD_SEQ')
	alter table gfi_epay_autoload_seq add constraint DF__GFI_EPAY_AUTOLOAD_SEQ__LOAD_SEQ default 0 for load_seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_SEQ__PKG_ID')
	alter table gfi_epay_autoload_seq add constraint DF__GFI_EPAY_AUTOLOAD_SEQ__PKG_ID default 0 for pkg_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_SEQ__SEQ')
	alter table gfi_epay_autoload_seq add constraint DF__GFI_EPAY_AUTOLOAD_SEQ__SEQ default 0 for seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__CARD_ID')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__CARD_ID default 0 for card_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__FLAGS')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__ID')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__ID default 0 for id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__LOC_N')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__LOC_N default 0 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__PENDING')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__RC')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__RC default 1 for rc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__TR_SEQ')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__TR_SEQ default 0 for tr_seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_AUTOLOAD_TR__VALUE')
	alter table gfi_epay_autoload_tr add constraint DF__GFI_EPAY_AUTOLOAD_TR__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__CARD_TYPE')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__CARD_TYPE default 0 for card_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__CLASS')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__CLASS default 0 for class
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__CREATE_TS')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__CREATE_TS default getdate() for create_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__FLAGS')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__LOAD_SEQ')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__LOAD_SEQ default 0 for load_seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__LOAD_SEQ2')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__LOAD_SEQ2 default 0 for load_seq2
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__LOAD_TS')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__LOAD_TS default getdate() for load_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__LOAD_TS2')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__LOAD_TS2 default getdate() for load_ts2
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__SC')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__SC default 5 for sc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD__USER_PROFILE')
	alter table gfi_epay_card add constraint DF__GFI_EPAY_CARD__USER_PROFILE default 0 for user_profile
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__FARE_ID')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__FLAGS')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__FLAGS default 32512 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__PENDING')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__PROD_ID')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__PROD_ID default 1 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__PROD_N')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__PROD_N default 0 for prod_n
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__TPBC')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_DTL__VALUE')
	alter table gfi_epay_card_dtl add constraint DF__GFI_EPAY_CARD_DTL__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_INV_LOG__TS')
	alter table gfi_epay_card_inv_log add constraint DF__GFI_EPAY_CARD_INV_LOG__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CARD_USR__ROLE')
	alter table gfi_epay_card_usr add constraint DF__GFI_EPAY_CARD_USR__ROLE default 1 for role
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CONTACT__COUNTRY')
	alter table gfi_epay_contact add constraint DF__GFI_EPAY_CONTACT__COUNTRY default 'US' for country
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_CONTACT__PREF')
	alter table gfi_epay_contact add constraint DF__GFI_EPAY_CONTACT__PREF default 1 for pref
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__ACTIVE')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__ACTIVE default 1 for active
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__ADD_VALUE_MAX')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__ADD_VALUE_MAX default 20000 for add_value_max
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__ADD_VALUE_MIN')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__ADD_VALUE_MIN default 0 for add_value_min
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__EXP_TYPE')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__EXP_TYPE default 0 for exp_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__FLAGS')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__FLAGS default 32638 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__ISBLANK')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__ISBLANK default 'N' for ISBlank
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__PRICE')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__PRICE default 0 for price
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_FARE__VALUE')
	alter table gfi_epay_fare add constraint DF__GFI_EPAY_FARE__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_GRP__ACTIVE')
	alter table gfi_epay_grp add constraint DF__GFI_EPAY_GRP__ACTIVE default 1 for active
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__ACTIVE')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__ACTIVE default 1 for active
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__ADD_VALUE_MAX')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__ADD_VALUE_MAX default 20000 for add_value_max
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__ADD_VALUE_MIN')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__ADD_VALUE_MIN default 0 for add_value_min
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__AVAIL_CREDIT')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__AVAIL_CREDIT default 0 for avail_credit
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__CONTACT_ID')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__CONTACT_ID default 0 for contact_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__CREATE_TS')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__CREATE_TS default getdate() for create_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__CREDIT_LIMIT')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__CREDIT_LIMIT default 0 for credit_limit
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__FLAGS')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__INVOICE_LIMIT')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__INVOICE_LIMIT default 1 for invoice_limit
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__PURSE')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__PURSE default 0 for purse
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST__TYPE')
	alter table gfi_epay_inst add constraint DF__GFI_EPAY_INST__TYPE default 0 for type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST_PROD_ARC__ARCHIVE_TS')
	alter table gfi_epay_inst_prod_arc add constraint DF__GFI_EPAY_INST_PROD_ARC__ARCHIVE_TS default getdate() for archive_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INST_USR__ROLE')
	alter table gfi_epay_inst_usr add constraint DF__GFI_EPAY_INST_USR__ROLE default 1 for role
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INVOICE__CNT')
	alter table gfi_epay_invoice add constraint DF__GFI_EPAY_INVOICE__CNT default 1 for cnt
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INVOICE__LAST_TS')
	alter table gfi_epay_invoice add constraint DF__GFI_EPAY_INVOICE__LAST_TS default getdate() for last_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INVOICE__STATUS')
	alter table gfi_epay_invoice add constraint DF__GFI_EPAY_INVOICE__STATUS default 0 for status
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_INVOICE__TS')
	alter table gfi_epay_invoice add constraint DF__GFI_EPAY_INVOICE__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_LOG__TS')
	alter table gfi_epay_log add constraint DF__GFI_EPAY_LOG__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_MOBILE_TICKETS__CREATE_TS')
	alter table gfi_epay_mobile_tickets add constraint DF__GFI_EPAY_MOBILE_TICKETS__CREATE_TS default getdate() for create_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__CARD_ID')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__CARD_ID default 0 for card_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__CHANGE')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__CHANGE default 0 for change
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__DES')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__FLAGS')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__ID')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__ID default 0 for id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__LOC_N')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__LOC_N default 0 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__PENDING')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__PRICE')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__PRICE default 0 for price
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__PROD_ID')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__PROD_TYPE')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__PROD_TYPE default 0 for prod_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__SC')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__SC default 5 for sc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__TPBC')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__TR_SEQ')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__TR_SEQ default 0 for tr_seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NON_AUTOLOAD_TR__VALUE')
	alter table gfi_epay_non_autoload_tr add constraint DF__GFI_EPAY_NON_AUTOLOAD_TR__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NOTIFY__TS')
	alter table gfi_epay_notify add constraint DF__GFI_EPAY_NOTIFY__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NOTIFY_DONE__FULFILL_TS')
	alter table gfi_epay_notify_done add constraint DF__GFI_EPAY_NOTIFY_DONE__FULFILL_TS default getdate() for fulfill_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_NOTIFY_DONE__STATUS')
	alter table gfi_epay_notify_done add constraint DF__GFI_EPAY_NOTIFY_DONE__STATUS default 0 for status
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER__AMT')
	alter table gfi_epay_order add constraint DF__GFI_EPAY_ORDER__AMT default 0 for amt
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER__CONTACT_ID')
	alter table gfi_epay_order add constraint DF__GFI_EPAY_ORDER__CONTACT_ID default 0 for contact_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER__ORD_BY')
	alter table gfi_epay_order add constraint DF__GFI_EPAY_ORDER__ORD_BY default 0 for ord_by
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER__ORD_TS')
	alter table gfi_epay_order add constraint DF__GFI_EPAY_ORDER__ORD_TS default getdate() for ord_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER__USER_ID')
	alter table gfi_epay_order add constraint DF__GFI_EPAY_ORDER__USER_ID default 0 for user_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD__FARE_ID')
	alter table gfi_epay_order_autoload add constraint DF__GFI_EPAY_ORDER_AUTOLOAD__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD__STATUS')
	alter table gfi_epay_order_autoload add constraint DF__GFI_EPAY_ORDER_AUTOLOAD__STATUS default 0 for status
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD__TPBC')
	alter table gfi_epay_order_autoload add constraint DF__GFI_EPAY_ORDER_AUTOLOAD__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD__VALUE')
	alter table gfi_epay_order_autoload add constraint DF__GFI_EPAY_ORDER_AUTOLOAD__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__CNT_ERR')
	alter table gfi_epay_order_autoload_range add constraint DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__CNT_ERR default 0 for cnt_err
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__CNT_OK')
	alter table gfi_epay_order_autoload_range add constraint DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__CNT_OK default 0 for cnt_ok
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__FARE_ID')
	alter table gfi_epay_order_autoload_range add constraint DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__TPBC')
	alter table gfi_epay_order_autoload_range add constraint DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__VALUE')
	alter table gfi_epay_order_autoload_range add constraint DF__GFI_EPAY_ORDER_AUTOLOAD_RANGE__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_ITEM__FEE')
	alter table gfi_epay_order_item add constraint DF__GFI_EPAY_ORDER_ITEM__FEE default 0 for fee
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_ITEM__QTY')
	alter table gfi_epay_order_item add constraint DF__GFI_EPAY_ORDER_ITEM__QTY default 1 for qty
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_ITEM__TYPE')
	alter table gfi_epay_order_item add constraint DF__GFI_EPAY_ORDER_ITEM__TYPE default 1 for type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_ITEM_NOTE__TS')
	alter table gfi_epay_order_item_note add constraint DF__GFI_EPAY_ORDER_ITEM_NOTE__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_NEW__FARE_ID')
	alter table gfi_epay_order_new add constraint DF__GFI_EPAY_ORDER_NEW__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_NEW__FLAGS')
	alter table gfi_epay_order_new add constraint DF__GFI_EPAY_ORDER_NEW__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_NEW__PROD_ID')
	alter table gfi_epay_order_new add constraint DF__GFI_EPAY_ORDER_NEW__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_PROD__FARE_ID')
	alter table gfi_epay_order_prod add constraint DF__GFI_EPAY_ORDER_PROD__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_PROD__FLAGS')
	alter table gfi_epay_order_prod add constraint DF__GFI_EPAY_ORDER_PROD__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_RECHARGE__FARE_ID')
	alter table gfi_epay_order_recharge add constraint DF__GFI_EPAY_ORDER_RECHARGE__FARE_ID default 0 for fare_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_SHIP__SHIP_BY')
	alter table gfi_epay_order_ship add constraint DF__GFI_EPAY_ORDER_SHIP__SHIP_BY default 0 for ship_by
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_ORDER_SHIP__TS')
	alter table gfi_epay_order_ship add constraint DF__GFI_EPAY_ORDER_SHIP__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR__ACTIVE')
	alter table gfi_epay_usr add constraint DF__GFI_EPAY_USR__ACTIVE default 1 for active
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR__CONTACT_ID')
	alter table gfi_epay_usr add constraint DF__GFI_EPAY_USR__CONTACT_ID default 0 for contact_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR__CREATE_TS')
	alter table gfi_epay_usr add constraint DF__GFI_EPAY_USR__CREATE_TS default getdate() for create_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR__FLAG')
	alter table gfi_epay_usr add constraint DF__GFI_EPAY_USR__FLAG default 0 for flag
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR__PURSE')
	alter table gfi_epay_usr add constraint DF__GFI_EPAY_USR__PURSE default 0 for purse
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR__USER_PROFILE')
	alter table gfi_epay_usr add constraint DF__GFI_EPAY_USR__USER_PROFILE default 0 for user_profile
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR_BILLING__CONTACT_ID')
	alter table gfi_epay_usr_billing add constraint DF__GFI_EPAY_USR_BILLING__CONTACT_ID default 0 for contact_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EPAY_USR_BILLING__TS')
	alter table gfi_epay_usr_billing add constraint DF__GFI_EPAY_USR_BILLING__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ__EQ_TYPE')
	alter table gfi_eq add constraint DF__GFI_EQ__EQ_TYPE default 1 for eq_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ__MAP_X')
	alter table gfi_eq add constraint DF__GFI_EQ__MAP_X default 0 for map_x
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ__MAP_Y')
	alter table gfi_eq add constraint DF__GFI_EQ__MAP_Y default 0 for map_y
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__ALM_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__ALM_FLAGS default 0 for alm_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__BTP_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__BTP_FLAGS default 0 for btp_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__BTP_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__BTP_VER default 0 for btp_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_C10')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_C10 default 0 for cbx_c10
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_C100')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_C100 default 0 for cbx_c100
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_C25')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_C25 default 0 for cbx_c25
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_C5')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_C5 default 0 for cbx_c5
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_FLAGS default 0 for cbx_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_SP')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_SP default 0 for cbx_sp
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_T1')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_T1 default 0 for cbx_t1
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CBX_T2')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CBX_T2 default 0 for cbx_t2
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_C10')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_C10 default 0 for cmt_c10
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_C100')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_C100 default 0 for cmt_c100
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_C25')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_C25 default 0 for cmt_c25
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_C5')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_C5 default 0 for cmt_c5
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_C50')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_C50 default 0 for cmt_c50
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_SP')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_SP default 0 for cmt_sp
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_T1')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_T1 default 0 for cmt_t1
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CMT_T2')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CMT_T2 default 0 for cmt_t2
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CNF_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CNF_FLAGS default 0 for cnf_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CTP_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CTP_FLAGS default 0 for ctp_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__CTP_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__CTP_VER default 0 for ctp_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__DISK_SPACE')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__DISK_SPACE default 0 for disk_space
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR1_CT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR1_CT default 0 for hpr1_ct
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR1_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR1_FLAGS default 0 for hpr1_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR1_STOCK')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR1_STOCK default 0 for hpr1_stock
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR1_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR1_VER default 0 for hpr1_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR2_CT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR2_CT default 0 for hpr2_ct
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR2_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR2_FLAGS default 0 for hpr2_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR2_STOCK')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR2_STOCK default 0 for hpr2_stock
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__HPR2_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__HPR2_VER default 0 for hpr2_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__INSRDR_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__INSRDR_VER default 0 for insrdr_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__KBD_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__KBD_FLAGS default 0 for kbd_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__LAST_USERID')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__LAST_USERID default 0 for last_userid
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__MAX_CHANGE')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__MAX_CHANGE default 0 for max_change
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__PINPAD_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__PINPAD_FLAGS default 0 for pinpad_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__PINPAD_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__PINPAD_VER default 0 for pinpad_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__PORT_N')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__PORT_N default 0 for port_n
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__PRN_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__PRN_FLAGS default 0 for prn_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__PRN_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__PRN_VER default 0 for prn_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SMTCD_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SMTCD_FLAGS default 0 for smtcd_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SMTCD_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SMTCD_VER default 0 for smtcd_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SRV_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SRV_FLAGS default 0 for srv_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__ST_ID')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__ST_ID default 0 for st_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B1')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B1 default 0 for stkr_b1
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B10')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B10 default 0 for stkr_b10
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B100')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B100 default 0 for stkr_b100
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B2')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B2 default 0 for stkr_b2
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B20')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B20 default 0 for stkr_b20
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B5')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B5 default 0 for stkr_b5
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__STKR_B50')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__STKR_B50 default 0 for stkr_b50
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_12VOLT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_12VOLT default 0 for sys_12volt
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_5VOLT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_5VOLT default 0 for sys_5volt
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_CPU_FAN')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_CPU_FAN default 0 for sys_cpu_fan
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_CPU_TEMP')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_CPU_TEMP default 0 for sys_cpu_temp
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_FLAGS default 0 for sys_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_PER_DISKFULL')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_PER_DISKFULL default 0 for sys_per_diskfull
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__SYS_TEMP')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__SYS_TEMP default 0 for sys_temp
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM0_CT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM0_CT default 0 for trim0_ct
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM0_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM0_FLAGS default 0 for trim0_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM0_STOCK')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM0_STOCK default 0 for trim0_stock
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM0_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM0_VER default 0 for trim0_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM1_CT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM1_CT default 0 for trim1_ct
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM1_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM1_FLAGS default 0 for trim1_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM1_STOCK')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM1_STOCK default 0 for trim1_stock
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM1_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM1_VER default 0 for trim1_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM2_CT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM2_CT default 0 for trim2_ct
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM2_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM2_FLAGS default 0 for trim2_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM2_STOCK')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM2_STOCK default 0 for trim2_stock
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM2_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM2_VER default 0 for trim2_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM3_CT')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM3_CT default 0 for trim3_ct
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM3_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM3_FLAGS default 0 for trim3_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM3_STOCK')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM3_STOCK default 0 for trim3_stock
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__TRIM3_VER')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__TRIM3_VER default 0 for trim3_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__UPD_TS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__UPD_TS default getdate() for upd_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__UPS_FLAGS')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__UPS_FLAGS default 0 for ups_flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__UPS_INPUT_VOLTAGE')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__UPS_INPUT_VOLTAGE default 0 for ups_input_voltage
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__UPS_LOADLEVEL')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__UPS_LOADLEVEL default 0 for ups_loadlevel
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__UPS_PER_CHARGE')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__UPS_PER_CHARGE default 0 for ups_per_charge
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__UPS_TEMP')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__UPS_TEMP default 0 for ups_temp
go
if not exists(select null from sys.objects where name = 'DF__GFI_EQ_STATUS__VERSION')
	alter table gfi_eq_status add constraint DF__GFI_EQ_STATUS__VERSION default 0 for version
go
if not exists(select null from sys.objects where name = 'DF__GFI_FIS_MAF_LOG__TS')
	alter table gfi_fis_maf_log add constraint DF__GFI_FIS_MAF_LOG__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_LDR__EQ_N')
	alter table gfi_ldr add constraint DF__GFI_LDR__EQ_N default 0 for eq_n
go
if not exists(select null from sys.objects where name = 'DF__GFI_LDR__EQ_TYPE')
	alter table gfi_ldr add constraint DF__GFI_LDR__EQ_TYPE default 0 for eq_type
go
if not exists(select null from sys.objects where name = 'DF__GFI_LDR__FILETYPE')
	alter table gfi_ldr add constraint DF__GFI_LDR__FILETYPE default 0 for filetype
go
if not exists(select null from sys.objects where name = 'DF__GFI_LDR__LOAD_TS')
	alter table gfi_ldr add constraint DF__GFI_LDR__LOAD_TS default getdate() for load_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_LDR__LOC_N')
	alter table gfi_ldr add constraint DF__GFI_LDR__LOC_N default 0 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__GFI_LST__CLASS')
	alter table gfi_lst add constraint DF__GFI_LST__CLASS default 0 for class
go
if not exists(select null from sys.objects where name = 'DF__GFI_ML__BADLIST_C')
	alter table gfi_ml add constraint DF__GFI_ML__BADLIST_C default 0 for badlist_c
go
if not exists(select null from sys.objects where name = 'DF__GFI_ML__EXPIRED_C')
	alter table gfi_ml add constraint DF__GFI_ML__EXPIRED_C default 0 for expired_c
go
if not exists(select null from sys.objects where name = 'DF__GFI_ML__INVALID_C')
	alter table gfi_ml add constraint DF__GFI_ML__INVALID_C default 0 for invalid_c
go
if not exists(select null from sys.objects where name = 'DF__GFI_ML__MISREAD_C')
	alter table gfi_ml add constraint DF__GFI_ML__MISREAD_C default 0 for misread_c
go
if not exists(select null from sys.objects where name = 'DF__GFI_ML__PASSBACK_C')
	alter table gfi_ml add constraint DF__GFI_ML__PASSBACK_C default 0 for passback_c
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__B1')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__B1 default 0 for b1
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__B10')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__B10 default 0 for b10
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__B2')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__B2 default 0 for b2
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__B20')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__B20 default 0 for b20
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__B5')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__B5 default 0 for b5
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__C1')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__C1 default 0 for c1
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__C10')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__C10 default 0 for c10
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__C100')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__C100 default 0 for c100
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__C25')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__C25 default 0 for c25
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__C5')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__C5 default 0 for c5
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__C50')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__C50 default 0 for c50
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__IR')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__IR default 0 for ir
go
if not exists(select null from sys.objects where name = 'DF__GFI_PV_RECON__REV')
	alter table gfi_pv_recon add constraint DF__GFI_PV_RECON__REV default 0 for rev
go
if not exists(select null from sys.objects where name = 'DF__GFI_STOP__LATITUDE')
	alter table gfi_stop add constraint DF__GFI_STOP__LATITUDE default 0 for latitude
go
if not exists(select null from sys.objects where name = 'DF__GFI_STOP__LONGITUDE')
	alter table gfi_stop add constraint DF__GFI_STOP__LONGITUDE default 0 for longitude
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__CHARGE')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__CHARGE default 0 for charge
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__DES')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__F_USE_F')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__F_USE_F default 'N' for f_use_f
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__FS')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__FS default 1 for fs
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__LOAD_SEQ')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__LOAD_SEQ default 0 for load_seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__PENDING')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__PKG_ID')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__PKG_ID default 0 for pkg_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__PKG_PROD_ID')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__PKG_PROD_ID default 0 for pkg_prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__PROD_ID')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__RANGE')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__RANGE default 0 for range
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__RC')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__RC default 1 for rc
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__VALUE')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL__VER')
	alter table gfi_tr_journal add constraint DF__GFI_TR_JOURNAL__VER default 0 for ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__ARCHIVE_TS')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__ARCHIVE_TS default getdate() for archive_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__CHARGE')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__CHARGE default 0 for charge
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__DES')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__F_USE_F')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__F_USE_F default 'N' for f_use_f
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__FS')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__FS default 1 for fs
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__LOAD_SEQ')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__LOAD_SEQ default 0 for load_seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__PENDING')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__PKG_ID')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__PKG_ID default 0 for pkg_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__PKG_PROD_ID')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__PKG_PROD_ID default 0 for pkg_prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__PROC_RC')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__PROC_RC default 0 for proc_rc
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__PROD_ID')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__PROD_ID default 0 for prod_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__RANGE')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__RANGE default 0 for range
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__RC')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__RC default 1 for rc
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__VALUE')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__VALUE default 0 for value
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_JOURNAL_ARC__VER')
	alter table gfi_tr_journal_arc add constraint DF__GFI_TR_JOURNAL_ARC__VER default 0 for ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_SCARD__CARD_ID')
	alter table gfi_tr_scard add constraint DF__GFI_TR_SCARD__CARD_ID default 0 for card_id
go
if not exists(select null from sys.objects where name = 'DF__GFI_TR_SCARD__PENDING')
	alter table gfi_tr_scard add constraint DF__GFI_TR_SCARD__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__GFI_TVM_MOD__MOD_POS')
	alter table gfi_tvm_mod add constraint DF__GFI_TVM_MOD__MOD_POS default 0 for mod_pos
go
if not exists(select null from sys.objects where name = 'DF__GFI_TVM_MOD__SEQ')
	alter table gfi_tvm_mod add constraint DF__GFI_TVM_MOD__SEQ default 0 for seq
go
if not exists(select null from sys.objects where name = 'DF__GFI_TVM_MOD__TS')
	alter table gfi_tvm_mod add constraint DF__GFI_TVM_MOD__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_UPGRADE__CHECK_VER')
	alter table gfi_upgrade add constraint DF__GFI_UPGRADE__CHECK_VER default 1 for check_ver
go
if not exists(select null from sys.objects where name = 'DF__GFI_UPGRADE__NAME')
	alter table gfi_upgrade add constraint DF__GFI_UPGRADE__NAME default 'NM' for name
go
if not exists(select null from sys.objects where name = 'DF__GFI_UPGRADE__START_TS')
	alter table gfi_upgrade add constraint DF__GFI_UPGRADE__START_TS default getdate() for start_ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__DIME')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__DIME default 0 for dime
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__FIVE')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__FIVE default 0 for five
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__FLAGS')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__HALF')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__HALF default 0 for half
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__MISC_C')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__MISC_C default 0 for misc_c
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__NICKEL')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__NICKEL default 0 for nickel
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__ONE')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__ONE default 0 for one
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__PENNY')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__PENNY default 0 for penny
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__QUARTER')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__QUARTER default 0 for quarter
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__REVENUE')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__REVENUE default 0 for revenue
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__SBA')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__SBA default 0 for sba
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__TEN')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__TEN default 0 for ten
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__TICKET')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__TICKET default 0 for ticket
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__TOKEN')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__TOKEN default 0 for token
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__TS')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__TWENTY')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__TWENTY default 0 for twenty
go
if not exists(select null from sys.objects where name = 'DF__GFI_VLT_PRB_TOTAL__TWO')
	alter table gfi_vlt_prb_total add constraint DF__GFI_VLT_PRB_TOTAL__TWO default 0 for two
go
if not exists(select null from sys.objects where name = 'DF__GS__BILL_T')
	alter table gs add constraint DF__GS__BILL_T default 0 for bill_t
go
if not exists(select null from sys.objects where name = 'DF__GS__BILLOVR')
	alter table gs add constraint DF__GS__BILLOVR default 0 for billovr
go
if not exists(select null from sys.objects where name = 'DF__GS__BUS_C')
	alter table gs add constraint DF__GS__BUS_C default 0 for bus_c
go
if not exists(select null from sys.objects where name = 'DF__GS__BYPASS')
	alter table gs add constraint DF__GS__BYPASS default 0 for bypass
go
if not exists(select null from sys.objects where name = 'DF__GS__CBXALM')
	alter table gs add constraint DF__GS__CBXALM default 0 for cbxalm
go
if not exists(select null from sys.objects where name = 'DF__GS__COIN_E')
	alter table gs add constraint DF__GS__COIN_E default 0 for coin_e
go
if not exists(select null from sys.objects where name = 'DF__GS__COIN_T')
	alter table gs add constraint DF__GS__COIN_T default 0 for coin_t
go
if not exists(select null from sys.objects where name = 'DF__GS__COINERROVR')
	alter table gs add constraint DF__GS__COINERROVR default 0 for coinerrovr
go
if not exists(select null from sys.objects where name = 'DF__GS__COINOVR')
	alter table gs add constraint DF__GS__COINOVR default 0 for coinovr
go
if not exists(select null from sys.objects where name = 'DF__GS__COLD_C')
	alter table gs add constraint DF__GS__COLD_C default 0 for cold_c
go
if not exists(select null from sys.objects where name = 'DF__GS__COLDOVR')
	alter table gs add constraint DF__GS__COLDOVR default 0 for coldovr
go
if not exists(select null from sys.objects where name = 'DF__GS__CURR_R')
	alter table gs add constraint DF__GS__CURR_R default 0 for curr_r
go
if not exists(select null from sys.objects where name = 'DF__GS__DD')
	alter table gs add constraint DF__GS__DD default 0 for dd
go
if not exists(select null from sys.objects where name = 'DF__GS__DIME')
	alter table gs add constraint DF__GS__DIME default 0 for dime
go
if not exists(select null from sys.objects where name = 'DF__GS__DN')
	alter table gs add constraint DF__GS__DN default 0 for dn
go
if not exists(select null from sys.objects where name = 'DF__GS__DOORALM')
	alter table gs add constraint DF__GS__DOORALM default 0 for dooralm
go
if not exists(select null from sys.objects where name = 'DF__GS__DP')
	alter table gs add constraint DF__GS__DP default 0 for dp
go
if not exists(select null from sys.objects where name = 'DF__GS__DQ')
	alter table gs add constraint DF__GS__DQ default 0 for dq
go
if not exists(select null from sys.objects where name = 'DF__GS__DUMP_C')
	alter table gs add constraint DF__GS__DUMP_C default 0 for dump_c
go
if not exists(select null from sys.objects where name = 'DF__GS__EVNTOVR')
	alter table gs add constraint DF__GS__EVNTOVR default 0 for evntovr
go
if not exists(select null from sys.objects where name = 'DF__GS__FARE_C')
	alter table gs add constraint DF__GS__FARE_C default 0 for fare_c
go
if not exists(select null from sys.objects where name = 'DF__GS__FIVE')
	alter table gs add constraint DF__GS__FIVE default 0 for five
go
if not exists(select null from sys.objects where name = 'DF__GS__FS')
	alter table gs add constraint DF__GS__FS default 0 for fs
go
if not exists(select null from sys.objects where name = 'DF__GS__FSCHANGE')
	alter table gs add constraint DF__GS__FSCHANGE default 0 for fschange
go
if not exists(select null from sys.objects where name = 'DF__GS__HALF')
	alter table gs add constraint DF__GS__HALF default 0 for half
go
if not exists(select null from sys.objects where name = 'DF__GS__INVALIDVER')
	alter table gs add constraint DF__GS__INVALIDVER default 0 for invalidver
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY1')
	alter table gs add constraint DF__GS__KEY1 default 0 for key1
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY2')
	alter table gs add constraint DF__GS__KEY2 default 0 for key2
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY3')
	alter table gs add constraint DF__GS__KEY3 default 0 for key3
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY4')
	alter table gs add constraint DF__GS__KEY4 default 0 for key4
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY5')
	alter table gs add constraint DF__GS__KEY5 default 0 for key5
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY6')
	alter table gs add constraint DF__GS__KEY6 default 0 for key6
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY7')
	alter table gs add constraint DF__GS__KEY7 default 0 for key7
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY8')
	alter table gs add constraint DF__GS__KEY8 default 0 for key8
go
if not exists(select null from sys.objects where name = 'DF__GS__KEY9')
	alter table gs add constraint DF__GS__KEY9 default 0 for key9
go
if not exists(select null from sys.objects where name = 'DF__GS__KEYA')
	alter table gs add constraint DF__GS__KEYA default 0 for keya
go
if not exists(select null from sys.objects where name = 'DF__GS__KEYAST')
	alter table gs add constraint DF__GS__KEYAST default 0 for keyast
go
if not exists(select null from sys.objects where name = 'DF__GS__KEYB')
	alter table gs add constraint DF__GS__KEYB default 0 for keyb
go
if not exists(select null from sys.objects where name = 'DF__GS__KEYC')
	alter table gs add constraint DF__GS__KEYC default 0 for keyc
go
if not exists(select null from sys.objects where name = 'DF__GS__KEYD')
	alter table gs add constraint DF__GS__KEYD default 0 for keyd
go
if not exists(select null from sys.objects where name = 'DF__GS__MEMCLR')
	alter table gs add constraint DF__GS__MEMCLR default 0 for memclr
go
if not exists(select null from sys.objects where name = 'DF__GS__NICKEL')
	alter table gs add constraint DF__GS__NICKEL default 0 for nickel
go
if not exists(select null from sys.objects where name = 'DF__GS__NQ')
	alter table gs add constraint DF__GS__NQ default 0 for nq
go
if not exists(select null from sys.objects where name = 'DF__GS__ONE')
	alter table gs add constraint DF__GS__ONE default 0 for one
go
if not exists(select null from sys.objects where name = 'DF__GS__PASS_E')
	alter table gs add constraint DF__GS__PASS_E default 0 for pass_e
go
if not exists(select null from sys.objects where name = 'DF__GS__PASS_T')
	alter table gs add constraint DF__GS__PASS_T default 0 for pass_t
go
if not exists(select null from sys.objects where name = 'DF__GS__PASSERROVR')
	alter table gs add constraint DF__GS__PASSERROVR default 0 for passerrovr
go
if not exists(select null from sys.objects where name = 'DF__GS__PASSOVR')
	alter table gs add constraint DF__GS__PASSOVR default 0 for passovr
go
if not exists(select null from sys.objects where name = 'DF__GS__PDU')
	alter table gs add constraint DF__GS__PDU default 0 for pdu
go
if not exists(select null from sys.objects where name = 'DF__GS__PENNY')
	alter table gs add constraint DF__GS__PENNY default 0 for penny
go
if not exists(select null from sys.objects where name = 'DF__GS__PN')
	alter table gs add constraint DF__GS__PN default 0 for pn
go
if not exists(select null from sys.objects where name = 'DF__GS__PP')
	alter table gs add constraint DF__GS__PP default 0 for pp
go
if not exists(select null from sys.objects where name = 'DF__GS__PQ')
	alter table gs add constraint DF__GS__PQ default 0 for pq
go
if not exists(select null from sys.objects where name = 'DF__GS__PRB_T')
	alter table gs add constraint DF__GS__PRB_T default 0 for prb_t
go
if not exists(select null from sys.objects where name = 'DF__GS__QQ')
	alter table gs add constraint DF__GS__QQ default 0 for qq
go
if not exists(select null from sys.objects where name = 'DF__GS__QUARTER')
	alter table gs add constraint DF__GS__QUARTER default 0 for quarter
go
if not exists(select null from sys.objects where name = 'DF__GS__RDR_C')
	alter table gs add constraint DF__GS__RDR_C default 0 for rdr_c
go
if not exists(select null from sys.objects where name = 'DF__GS__REVDSC')
	alter table gs add constraint DF__GS__REVDSC default 0 for revdsc
go
if not exists(select null from sys.objects where name = 'DF__GS__SBA')
	alter table gs add constraint DF__GS__SBA default 0 for sba
go
if not exists(select null from sys.objects where name = 'DF__GS__TBIG')
	alter table gs add constraint DF__GS__TBIG default 0 for tbig
go
if not exists(select null from sys.objects where name = 'DF__GS__TEN')
	alter table gs add constraint DF__GS__TEN default 0 for ten
go
if not exists(select null from sys.objects where name = 'DF__GS__TICKET_T')
	alter table gs add constraint DF__GS__TICKET_T default 0 for ticket_t
go
if not exists(select null from sys.objects where name = 'DF__GS__TIMEDSC')
	alter table gs add constraint DF__GS__TIMEDSC default 0 for timedsc
go
if not exists(select null from sys.objects where name = 'DF__GS__TOKEN_T')
	alter table gs add constraint DF__GS__TOKEN_T default 0 for token_t
go
if not exists(select null from sys.objects where name = 'DF__GS__TSMALL')
	alter table gs add constraint DF__GS__TSMALL default 0 for tsmall
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP1')
	alter table gs add constraint DF__GS__TTP1 default 0 for ttp1
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP10')
	alter table gs add constraint DF__GS__TTP10 default 0 for ttp10
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP11')
	alter table gs add constraint DF__GS__TTP11 default 0 for ttp11
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP12')
	alter table gs add constraint DF__GS__TTP12 default 0 for ttp12
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP13')
	alter table gs add constraint DF__GS__TTP13 default 0 for ttp13
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP14')
	alter table gs add constraint DF__GS__TTP14 default 0 for ttp14
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP15')
	alter table gs add constraint DF__GS__TTP15 default 0 for ttp15
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP16')
	alter table gs add constraint DF__GS__TTP16 default 0 for ttp16
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP17')
	alter table gs add constraint DF__GS__TTP17 default 0 for ttp17
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP18')
	alter table gs add constraint DF__GS__TTP18 default 0 for ttp18
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP19')
	alter table gs add constraint DF__GS__TTP19 default 0 for ttp19
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP2')
	alter table gs add constraint DF__GS__TTP2 default 0 for ttp2
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP20')
	alter table gs add constraint DF__GS__TTP20 default 0 for ttp20
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP21')
	alter table gs add constraint DF__GS__TTP21 default 0 for ttp21
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP22')
	alter table gs add constraint DF__GS__TTP22 default 0 for ttp22
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP23')
	alter table gs add constraint DF__GS__TTP23 default 0 for ttp23
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP24')
	alter table gs add constraint DF__GS__TTP24 default 0 for ttp24
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP25')
	alter table gs add constraint DF__GS__TTP25 default 0 for ttp25
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP26')
	alter table gs add constraint DF__GS__TTP26 default 0 for ttp26
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP27')
	alter table gs add constraint DF__GS__TTP27 default 0 for ttp27
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP28')
	alter table gs add constraint DF__GS__TTP28 default 0 for ttp28
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP29')
	alter table gs add constraint DF__GS__TTP29 default 0 for ttp29
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP3')
	alter table gs add constraint DF__GS__TTP3 default 0 for ttp3
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP30')
	alter table gs add constraint DF__GS__TTP30 default 0 for ttp30
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP31')
	alter table gs add constraint DF__GS__TTP31 default 0 for ttp31
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP32')
	alter table gs add constraint DF__GS__TTP32 default 0 for ttp32
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP33')
	alter table gs add constraint DF__GS__TTP33 default 0 for ttp33
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP34')
	alter table gs add constraint DF__GS__TTP34 default 0 for ttp34
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP35')
	alter table gs add constraint DF__GS__TTP35 default 0 for ttp35
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP36')
	alter table gs add constraint DF__GS__TTP36 default 0 for ttp36
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP37')
	alter table gs add constraint DF__GS__TTP37 default 0 for ttp37
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP38')
	alter table gs add constraint DF__GS__TTP38 default 0 for ttp38
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP39')
	alter table gs add constraint DF__GS__TTP39 default 0 for ttp39
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP4')
	alter table gs add constraint DF__GS__TTP4 default 0 for ttp4
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP40')
	alter table gs add constraint DF__GS__TTP40 default 0 for ttp40
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP41')
	alter table gs add constraint DF__GS__TTP41 default 0 for ttp41
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP42')
	alter table gs add constraint DF__GS__TTP42 default 0 for ttp42
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP43')
	alter table gs add constraint DF__GS__TTP43 default 0 for ttp43
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP44')
	alter table gs add constraint DF__GS__TTP44 default 0 for ttp44
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP45')
	alter table gs add constraint DF__GS__TTP45 default 0 for ttp45
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP46')
	alter table gs add constraint DF__GS__TTP46 default 0 for ttp46
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP47')
	alter table gs add constraint DF__GS__TTP47 default 0 for ttp47
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP48')
	alter table gs add constraint DF__GS__TTP48 default 0 for ttp48
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP5')
	alter table gs add constraint DF__GS__TTP5 default 0 for ttp5
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP6')
	alter table gs add constraint DF__GS__TTP6 default 0 for ttp6
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP7')
	alter table gs add constraint DF__GS__TTP7 default 0 for ttp7
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP8')
	alter table gs add constraint DF__GS__TTP8 default 0 for ttp8
go
if not exists(select null from sys.objects where name = 'DF__GS__TTP9')
	alter table gs add constraint DF__GS__TTP9 default 0 for ttp9
go
if not exists(select null from sys.objects where name = 'DF__GS__TWENTY')
	alter table gs add constraint DF__GS__TWENTY default 0 for twenty
go
if not exists(select null from sys.objects where name = 'DF__GS__TWO')
	alter table gs add constraint DF__GS__TWO default 0 for two
go
if not exists(select null from sys.objects where name = 'DF__GS__UNCL_R')
	alter table gs add constraint DF__GS__UNCL_R default 0 for uncl_r
go
if not exists(select null from sys.objects where name = 'DF__GS__WARM_C')
	alter table gs add constraint DF__GS__WARM_C default 0 for warm_c
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP49')
	alter table gs_extd add constraint DF__GS_EXTD__TTP49 default 0 for ttp49
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP50')
	alter table gs_extd add constraint DF__GS_EXTD__TTP50 default 0 for ttp50
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP51')
	alter table gs_extd add constraint DF__GS_EXTD__TTP51 default 0 for ttp51
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP52')
	alter table gs_extd add constraint DF__GS_EXTD__TTP52 default 0 for ttp52
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP53')
	alter table gs_extd add constraint DF__GS_EXTD__TTP53 default 0 for ttp53
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP54')
	alter table gs_extd add constraint DF__GS_EXTD__TTP54 default 0 for ttp54
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP55')
	alter table gs_extd add constraint DF__GS_EXTD__TTP55 default 0 for ttp55
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP56')
	alter table gs_extd add constraint DF__GS_EXTD__TTP56 default 0 for ttp56
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP57')
	alter table gs_extd add constraint DF__GS_EXTD__TTP57 default 0 for ttp57
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP58')
	alter table gs_extd add constraint DF__GS_EXTD__TTP58 default 0 for ttp58
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP59')
	alter table gs_extd add constraint DF__GS_EXTD__TTP59 default 0 for ttp59
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP60')
	alter table gs_extd add constraint DF__GS_EXTD__TTP60 default 0 for ttp60
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP61')
	alter table gs_extd add constraint DF__GS_EXTD__TTP61 default 0 for ttp61
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP62')
	alter table gs_extd add constraint DF__GS_EXTD__TTP62 default 0 for ttp62
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP63')
	alter table gs_extd add constraint DF__GS_EXTD__TTP63 default 0 for ttp63
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP64')
	alter table gs_extd add constraint DF__GS_EXTD__TTP64 default 0 for ttp64
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP65')
	alter table gs_extd add constraint DF__GS_EXTD__TTP65 default 0 for ttp65
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP66')
	alter table gs_extd add constraint DF__GS_EXTD__TTP66 default 0 for ttp66
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP67')
	alter table gs_extd add constraint DF__GS_EXTD__TTP67 default 0 for ttp67
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP68')
	alter table gs_extd add constraint DF__GS_EXTD__TTP68 default 0 for ttp68
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP69')
	alter table gs_extd add constraint DF__GS_EXTD__TTP69 default 0 for ttp69
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP70')
	alter table gs_extd add constraint DF__GS_EXTD__TTP70 default 0 for ttp70
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP71')
	alter table gs_extd add constraint DF__GS_EXTD__TTP71 default 0 for ttp71
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP72')
	alter table gs_extd add constraint DF__GS_EXTD__TTP72 default 0 for ttp72
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP73')
	alter table gs_extd add constraint DF__GS_EXTD__TTP73 default 0 for ttp73
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP74')
	alter table gs_extd add constraint DF__GS_EXTD__TTP74 default 0 for ttp74
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP75')
	alter table gs_extd add constraint DF__GS_EXTD__TTP75 default 0 for ttp75
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP76')
	alter table gs_extd add constraint DF__GS_EXTD__TTP76 default 0 for ttp76
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP77')
	alter table gs_extd add constraint DF__GS_EXTD__TTP77 default 0 for ttp77
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP78')
	alter table gs_extd add constraint DF__GS_EXTD__TTP78 default 0 for ttp78
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP79')
	alter table gs_extd add constraint DF__GS_EXTD__TTP79 default 0 for ttp79
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP80')
	alter table gs_extd add constraint DF__GS_EXTD__TTP80 default 0 for ttp80
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP81')
	alter table gs_extd add constraint DF__GS_EXTD__TTP81 default 0 for ttp81
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP82')
	alter table gs_extd add constraint DF__GS_EXTD__TTP82 default 0 for ttp82
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP83')
	alter table gs_extd add constraint DF__GS_EXTD__TTP83 default 0 for ttp83
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP84')
	alter table gs_extd add constraint DF__GS_EXTD__TTP84 default 0 for ttp84
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP85')
	alter table gs_extd add constraint DF__GS_EXTD__TTP85 default 0 for ttp85
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP86')
	alter table gs_extd add constraint DF__GS_EXTD__TTP86 default 0 for ttp86
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP87')
	alter table gs_extd add constraint DF__GS_EXTD__TTP87 default 0 for ttp87
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP88')
	alter table gs_extd add constraint DF__GS_EXTD__TTP88 default 0 for ttp88
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP89')
	alter table gs_extd add constraint DF__GS_EXTD__TTP89 default 0 for ttp89
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP90')
	alter table gs_extd add constraint DF__GS_EXTD__TTP90 default 0 for ttp90
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP91')
	alter table gs_extd add constraint DF__GS_EXTD__TTP91 default 0 for ttp91
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP92')
	alter table gs_extd add constraint DF__GS_EXTD__TTP92 default 0 for ttp92
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP93')
	alter table gs_extd add constraint DF__GS_EXTD__TTP93 default 0 for ttp93
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP94')
	alter table gs_extd add constraint DF__GS_EXTD__TTP94 default 0 for ttp94
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP95')
	alter table gs_extd add constraint DF__GS_EXTD__TTP95 default 0 for ttp95
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP96')
	alter table gs_extd add constraint DF__GS_EXTD__TTP96 default 0 for ttp96
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP97')
	alter table gs_extd add constraint DF__GS_EXTD__TTP97 default 0 for ttp97
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP98')
	alter table gs_extd add constraint DF__GS_EXTD__TTP98 default 0 for ttp98
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP99')
	alter table gs_extd add constraint DF__GS_EXTD__TTP99 default 0 for ttp99
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP100')
	alter table gs_extd add constraint DF__GS_EXTD__TTP100 default 0 for ttp100
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP101')
	alter table gs_extd add constraint DF__GS_EXTD__TTP101 default 0 for ttp101
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP102')
	alter table gs_extd add constraint DF__GS_EXTD__TTP102 default 0 for ttp102
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP103')
	alter table gs_extd add constraint DF__GS_EXTD__TTP103 default 0 for ttp103
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP104')
	alter table gs_extd add constraint DF__GS_EXTD__TTP104 default 0 for ttp104
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP105')
	alter table gs_extd add constraint DF__GS_EXTD__TTP105 default 0 for ttp105
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP106')
	alter table gs_extd add constraint DF__GS_EXTD__TTP106 default 0 for ttp106
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP107')
	alter table gs_extd add constraint DF__GS_EXTD__TTP107 default 0 for ttp107
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP108')
	alter table gs_extd add constraint DF__GS_EXTD__TTP108 default 0 for ttp108
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP109')
	alter table gs_extd add constraint DF__GS_EXTD__TTP109 default 0 for ttp109
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP110')
	alter table gs_extd add constraint DF__GS_EXTD__TTP110 default 0 for ttp110
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP111')
	alter table gs_extd add constraint DF__GS_EXTD__TTP111 default 0 for ttp111
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP112')
	alter table gs_extd add constraint DF__GS_EXTD__TTP112 default 0 for ttp112
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP113')
	alter table gs_extd add constraint DF__GS_EXTD__TTP113 default 0 for ttp113
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP114')
	alter table gs_extd add constraint DF__GS_EXTD__TTP114 default 0 for ttp114
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP115')
	alter table gs_extd add constraint DF__GS_EXTD__TTP115 default 0 for ttp115
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP116')
	alter table gs_extd add constraint DF__GS_EXTD__TTP116 default 0 for ttp116
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP117')
	alter table gs_extd add constraint DF__GS_EXTD__TTP117 default 0 for ttp117
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP118')
	alter table gs_extd add constraint DF__GS_EXTD__TTP118 default 0 for ttp118
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP119')
	alter table gs_extd add constraint DF__GS_EXTD__TTP119 default 0 for ttp119
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP120')
	alter table gs_extd add constraint DF__GS_EXTD__TTP120 default 0 for ttp120
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP121')
	alter table gs_extd add constraint DF__GS_EXTD__TTP121 default 0 for ttp121
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP122')
	alter table gs_extd add constraint DF__GS_EXTD__TTP122 default 0 for ttp122
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP123')
	alter table gs_extd add constraint DF__GS_EXTD__TTP123 default 0 for ttp123
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP124')
	alter table gs_extd add constraint DF__GS_EXTD__TTP124 default 0 for ttp124
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP125')
	alter table gs_extd add constraint DF__GS_EXTD__TTP125 default 0 for ttp125
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP126')
	alter table gs_extd add constraint DF__GS_EXTD__TTP126 default 0 for ttp126
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP127')
	alter table gs_extd add constraint DF__GS_EXTD__TTP127 default 0 for ttp127
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP128')
	alter table gs_extd add constraint DF__GS_EXTD__TTP128 default 0 for ttp128
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP129')
	alter table gs_extd add constraint DF__GS_EXTD__TTP129 default 0 for ttp129
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP130')
	alter table gs_extd add constraint DF__GS_EXTD__TTP130 default 0 for ttp130
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP131')
	alter table gs_extd add constraint DF__GS_EXTD__TTP131 default 0 for ttp131
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP132')
	alter table gs_extd add constraint DF__GS_EXTD__TTP132 default 0 for ttp132
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP133')
	alter table gs_extd add constraint DF__GS_EXTD__TTP133 default 0 for ttp133
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP134')
	alter table gs_extd add constraint DF__GS_EXTD__TTP134 default 0 for ttp134
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP135')
	alter table gs_extd add constraint DF__GS_EXTD__TTP135 default 0 for ttp135
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP136')
	alter table gs_extd add constraint DF__GS_EXTD__TTP136 default 0 for ttp136
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP137')
	alter table gs_extd add constraint DF__GS_EXTD__TTP137 default 0 for ttp137
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP138')
	alter table gs_extd add constraint DF__GS_EXTD__TTP138 default 0 for ttp138
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP139')
	alter table gs_extd add constraint DF__GS_EXTD__TTP139 default 0 for ttp139
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP140')
	alter table gs_extd add constraint DF__GS_EXTD__TTP140 default 0 for ttp140
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP141')
	alter table gs_extd add constraint DF__GS_EXTD__TTP141 default 0 for ttp141
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP142')
	alter table gs_extd add constraint DF__GS_EXTD__TTP142 default 0 for ttp142
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP143')
	alter table gs_extd add constraint DF__GS_EXTD__TTP143 default 0 for ttp143
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP144')
	alter table gs_extd add constraint DF__GS_EXTD__TTP144 default 0 for ttp144
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP145')
	alter table gs_extd add constraint DF__GS_EXTD__TTP145 default 0 for ttp145
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP146')
	alter table gs_extd add constraint DF__GS_EXTD__TTP146 default 0 for ttp146
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP147')
	alter table gs_extd add constraint DF__GS_EXTD__TTP147 default 0 for ttp147
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP148')
	alter table gs_extd add constraint DF__GS_EXTD__TTP148 default 0 for ttp148
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP149')
	alter table gs_extd add constraint DF__GS_EXTD__TTP149 default 0 for ttp149
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP150')
	alter table gs_extd add constraint DF__GS_EXTD__TTP150 default 0 for ttp150
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP151')
	alter table gs_extd add constraint DF__GS_EXTD__TTP151 default 0 for ttp151
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP152')
	alter table gs_extd add constraint DF__GS_EXTD__TTP152 default 0 for ttp152
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP153')
	alter table gs_extd add constraint DF__GS_EXTD__TTP153 default 0 for ttp153
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP154')
	alter table gs_extd add constraint DF__GS_EXTD__TTP154 default 0 for ttp154
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP155')
	alter table gs_extd add constraint DF__GS_EXTD__TTP155 default 0 for ttp155
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP156')
	alter table gs_extd add constraint DF__GS_EXTD__TTP156 default 0 for ttp156
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP157')
	alter table gs_extd add constraint DF__GS_EXTD__TTP157 default 0 for ttp157
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP158')
	alter table gs_extd add constraint DF__GS_EXTD__TTP158 default 0 for ttp158
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP159')
	alter table gs_extd add constraint DF__GS_EXTD__TTP159 default 0 for ttp159
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP160')
	alter table gs_extd add constraint DF__GS_EXTD__TTP160 default 0 for ttp160
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP161')
	alter table gs_extd add constraint DF__GS_EXTD__TTP161 default 0 for ttp161
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP162')
	alter table gs_extd add constraint DF__GS_EXTD__TTP162 default 0 for ttp162
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP163')
	alter table gs_extd add constraint DF__GS_EXTD__TTP163 default 0 for ttp163
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP164')
	alter table gs_extd add constraint DF__GS_EXTD__TTP164 default 0 for ttp164
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP165')
	alter table gs_extd add constraint DF__GS_EXTD__TTP165 default 0 for ttp165
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP166')
	alter table gs_extd add constraint DF__GS_EXTD__TTP166 default 0 for ttp166
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP167')
	alter table gs_extd add constraint DF__GS_EXTD__TTP167 default 0 for ttp167
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP168')
	alter table gs_extd add constraint DF__GS_EXTD__TTP168 default 0 for ttp168
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP169')
	alter table gs_extd add constraint DF__GS_EXTD__TTP169 default 0 for ttp169
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP170')
	alter table gs_extd add constraint DF__GS_EXTD__TTP170 default 0 for ttp170
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP171')
	alter table gs_extd add constraint DF__GS_EXTD__TTP171 default 0 for ttp171
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP172')
	alter table gs_extd add constraint DF__GS_EXTD__TTP172 default 0 for ttp172
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP173')
	alter table gs_extd add constraint DF__GS_EXTD__TTP173 default 0 for ttp173
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP174')
	alter table gs_extd add constraint DF__GS_EXTD__TTP174 default 0 for ttp174
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP175')
	alter table gs_extd add constraint DF__GS_EXTD__TTP175 default 0 for ttp175
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP176')
	alter table gs_extd add constraint DF__GS_EXTD__TTP176 default 0 for ttp176
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP177')
	alter table gs_extd add constraint DF__GS_EXTD__TTP177 default 0 for ttp177
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP178')
	alter table gs_extd add constraint DF__GS_EXTD__TTP178 default 0 for ttp178
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP179')
	alter table gs_extd add constraint DF__GS_EXTD__TTP179 default 0 for ttp179
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP180')
	alter table gs_extd add constraint DF__GS_EXTD__TTP180 default 0 for ttp180
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP181')
	alter table gs_extd add constraint DF__GS_EXTD__TTP181 default 0 for ttp181
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP182')
	alter table gs_extd add constraint DF__GS_EXTD__TTP182 default 0 for ttp182
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP183')
	alter table gs_extd add constraint DF__GS_EXTD__TTP183 default 0 for ttp183
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP184')
	alter table gs_extd add constraint DF__GS_EXTD__TTP184 default 0 for ttp184
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP185')
	alter table gs_extd add constraint DF__GS_EXTD__TTP185 default 0 for ttp185
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP186')
	alter table gs_extd add constraint DF__GS_EXTD__TTP186 default 0 for ttp186
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP187')
	alter table gs_extd add constraint DF__GS_EXTD__TTP187 default 0 for ttp187
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP188')
	alter table gs_extd add constraint DF__GS_EXTD__TTP188 default 0 for ttp188
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP189')
	alter table gs_extd add constraint DF__GS_EXTD__TTP189 default 0 for ttp189
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP190')
	alter table gs_extd add constraint DF__GS_EXTD__TTP190 default 0 for ttp190
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP191')
	alter table gs_extd add constraint DF__GS_EXTD__TTP191 default 0 for ttp191
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP192')
	alter table gs_extd add constraint DF__GS_EXTD__TTP192 default 0 for ttp192
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP193')
	alter table gs_extd add constraint DF__GS_EXTD__TTP193 default 0 for ttp193
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP194')
	alter table gs_extd add constraint DF__GS_EXTD__TTP194 default 0 for ttp194
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP195')
	alter table gs_extd add constraint DF__GS_EXTD__TTP195 default 0 for ttp195
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP196')
	alter table gs_extd add constraint DF__GS_EXTD__TTP196 default 0 for ttp196
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP197')
	alter table gs_extd add constraint DF__GS_EXTD__TTP197 default 0 for ttp197
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP198')
	alter table gs_extd add constraint DF__GS_EXTD__TTP198 default 0 for ttp198
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP199')
	alter table gs_extd add constraint DF__GS_EXTD__TTP199 default 0 for ttp199
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP200')
	alter table gs_extd add constraint DF__GS_EXTD__TTP200 default 0 for ttp200
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP201')
	alter table gs_extd add constraint DF__GS_EXTD__TTP201 default 0 for ttp201
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP202')
	alter table gs_extd add constraint DF__GS_EXTD__TTP202 default 0 for ttp202
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP203')
	alter table gs_extd add constraint DF__GS_EXTD__TTP203 default 0 for ttp203
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP204')
	alter table gs_extd add constraint DF__GS_EXTD__TTP204 default 0 for ttp204
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP205')
	alter table gs_extd add constraint DF__GS_EXTD__TTP205 default 0 for ttp205
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP206')
	alter table gs_extd add constraint DF__GS_EXTD__TTP206 default 0 for ttp206
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP207')
	alter table gs_extd add constraint DF__GS_EXTD__TTP207 default 0 for ttp207
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP208')
	alter table gs_extd add constraint DF__GS_EXTD__TTP208 default 0 for ttp208
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP209')
	alter table gs_extd add constraint DF__GS_EXTD__TTP209 default 0 for ttp209
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP210')
	alter table gs_extd add constraint DF__GS_EXTD__TTP210 default 0 for ttp210
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP211')
	alter table gs_extd add constraint DF__GS_EXTD__TTP211 default 0 for ttp211
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP212')
	alter table gs_extd add constraint DF__GS_EXTD__TTP212 default 0 for ttp212
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP213')
	alter table gs_extd add constraint DF__GS_EXTD__TTP213 default 0 for ttp213
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP214')
	alter table gs_extd add constraint DF__GS_EXTD__TTP214 default 0 for ttp214
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP215')
	alter table gs_extd add constraint DF__GS_EXTD__TTP215 default 0 for ttp215
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP216')
	alter table gs_extd add constraint DF__GS_EXTD__TTP216 default 0 for ttp216
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP217')
	alter table gs_extd add constraint DF__GS_EXTD__TTP217 default 0 for ttp217
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP218')
	alter table gs_extd add constraint DF__GS_EXTD__TTP218 default 0 for ttp218
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP219')
	alter table gs_extd add constraint DF__GS_EXTD__TTP219 default 0 for ttp219
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP220')
	alter table gs_extd add constraint DF__GS_EXTD__TTP220 default 0 for ttp220
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP221')
	alter table gs_extd add constraint DF__GS_EXTD__TTP221 default 0 for ttp221
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP222')
	alter table gs_extd add constraint DF__GS_EXTD__TTP222 default 0 for ttp222
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP223')
	alter table gs_extd add constraint DF__GS_EXTD__TTP223 default 0 for ttp223
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP224')
	alter table gs_extd add constraint DF__GS_EXTD__TTP224 default 0 for ttp224
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP225')
	alter table gs_extd add constraint DF__GS_EXTD__TTP225 default 0 for ttp225
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP226')
	alter table gs_extd add constraint DF__GS_EXTD__TTP226 default 0 for ttp226
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP227')
	alter table gs_extd add constraint DF__GS_EXTD__TTP227 default 0 for ttp227
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP228')
	alter table gs_extd add constraint DF__GS_EXTD__TTP228 default 0 for ttp228
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP229')
	alter table gs_extd add constraint DF__GS_EXTD__TTP229 default 0 for ttp229
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP230')
	alter table gs_extd add constraint DF__GS_EXTD__TTP230 default 0 for ttp230
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP231')
	alter table gs_extd add constraint DF__GS_EXTD__TTP231 default 0 for ttp231
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP232')
	alter table gs_extd add constraint DF__GS_EXTD__TTP232 default 0 for ttp232
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP233')
	alter table gs_extd add constraint DF__GS_EXTD__TTP233 default 0 for ttp233
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP234')
	alter table gs_extd add constraint DF__GS_EXTD__TTP234 default 0 for ttp234
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP235')
	alter table gs_extd add constraint DF__GS_EXTD__TTP235 default 0 for ttp235
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP236')
	alter table gs_extd add constraint DF__GS_EXTD__TTP236 default 0 for ttp236
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP237')
	alter table gs_extd add constraint DF__GS_EXTD__TTP237 default 0 for ttp237
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP238')
	alter table gs_extd add constraint DF__GS_EXTD__TTP238 default 0 for ttp238
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP239')
	alter table gs_extd add constraint DF__GS_EXTD__TTP239 default 0 for ttp239
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP240')
	alter table gs_extd add constraint DF__GS_EXTD__TTP240 default 0 for ttp240
go
if not exists(select null from sys.objects where name = 'DF__GS_EXTD__TTP241')
	alter table gs_extd add constraint DF__GS_EXTD__TTP241 default 0 for ttp241
go
if not exists(select null from sys.objects where name = 'DF__HOLIDAYS__ENABLED_F')
	alter table holidays add constraint DF__HOLIDAYS__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__LANGUAGES__DATEMODIFIED')
	alter table Languages add constraint DF__LANGUAGES__DATEMODIFIED default getdate() for DateModified
go
if not exists(select null from sys.objects where name = 'DF__LANGUAGES__MODIFIEDBY')
	alter table Languages add constraint DF__LANGUAGES__MODIFIEDBY default user_name() for ModifiedBy
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__ENABLED_F')
	alter table media add constraint DF__MEDIA__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__FRIENDLY_F')
	alter table media add constraint DF__MEDIA__FRIENDLY_F default 'N' for friendly_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__HOL_F')
	alter table media add constraint DF__MEDIA__HOL_F default 'Y' for hol_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__OFFPEAK_F')
	alter table media add constraint DF__MEDIA__OFFPEAK_F default 'Y' for offpeak_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__PEAK_F')
	alter table media add constraint DF__MEDIA__PEAK_F default 'Y' for peak_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__SAT_F')
	alter table media add constraint DF__MEDIA__SAT_F default 'Y' for sat_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__SUN_F')
	alter table media add constraint DF__MEDIA__SUN_F default 'Y' for sun_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA__WKD_F')
	alter table media add constraint DF__MEDIA__WKD_F default 'Y' for wkd_f
go
if not exists(select null from sys.objects where name = 'DF__MEDIA_TRACK2__DATE_FMT')
	alter table media_track2 add constraint DF__MEDIA_TRACK2__DATE_FMT default 5 for date_fmt
go
if not exists(select null from sys.objects where name = 'DF__MEDIA_TRACK2__MOD10')
	alter table media_track2 add constraint DF__MEDIA_TRACK2__MOD10 default 1 for mod10
go
if not exists(select null from sys.objects where name = 'DF__MEDIA_TRACK2__TRK')
	alter table media_track2 add constraint DF__MEDIA_TRACK2__TRK default 0 for trk
go
if not exists(select null from sys.objects where name = 'DF__MESSAGELANGUAGES__DATEMODIFIED')
	alter table MessageLanguages add constraint DF__MESSAGELANGUAGES__DATEMODIFIED default getdate() for DateModified
go
if not exists(select null from sys.objects where name = 'DF__MESSAGELANGUAGES__MODIFIEDBY')
	alter table MessageLanguages add constraint DF__MESSAGELANGUAGES__MODIFIEDBY default user_name() for ModifiedBy
go
if not exists(select null from sys.objects where name = 'DF__MESSAGES__DATEMODIFIED')
	alter table Messages add constraint DF__MESSAGES__DATEMODIFIED default getdate() for DateModified
go
if not exists(select null from sys.objects where name = 'DF__MESSAGES__EDITJUSTIFY')
	alter table Messages add constraint DF__MESSAGES__EDITJUSTIFY default 'L' for EditJustify
go
if not exists(select null from sys.objects where name = 'DF__MESSAGES__MODIFIEDBY')
	alter table Messages add constraint DF__MESSAGES__MODIFIEDBY default user_name() for ModifiedBy
go
if not exists(select null from sys.objects where name = 'DF__ML__BILL_C')
	alter table ml add constraint DF__ML__BILL_C default 0 for bill_c
go
if not exists(select null from sys.objects where name = 'DF__ML__BILL_E')
	alter table ml add constraint DF__ML__BILL_E default 0 for bill_e
go
if not exists(select null from sys.objects where name = 'DF__ML__BILL_T')
	alter table ml add constraint DF__ML__BILL_T default 0 for bill_t
go
if not exists(select null from sys.objects where name = 'DF__ML__BILLOVR')
	alter table ml add constraint DF__ML__BILLOVR default 0 for billovr
go
if not exists(select null from sys.objects where name = 'DF__ML__BUS')
	alter table ml add constraint DF__ML__BUS default 0 for bus
go
if not exists(select null from sys.objects where name = 'DF__ML__BYPASS')
	alter table ml add constraint DF__ML__BYPASS default 0 for bypass
go
if not exists(select null from sys.objects where name = 'DF__ML__CBX_N')
	alter table ml add constraint DF__ML__CBX_N default 0 for cbx_n
go
if not exists(select null from sys.objects where name = 'DF__ML__CBXALM')
	alter table ml add constraint DF__ML__CBXALM default 0 for cbxalm
go
if not exists(select null from sys.objects where name = 'DF__ML__COIN_E')
	alter table ml add constraint DF__ML__COIN_E default 0 for coin_e
go
if not exists(select null from sys.objects where name = 'DF__ML__COIN_T')
	alter table ml add constraint DF__ML__COIN_T default 0 for coin_t
go
if not exists(select null from sys.objects where name = 'DF__ML__COIN_X')
	alter table ml add constraint DF__ML__COIN_X default 0 for coin_x
go
if not exists(select null from sys.objects where name = 'DF__ML__COINERROVR')
	alter table ml add constraint DF__ML__COINERROVR default 0 for coinerrovr
go
if not exists(select null from sys.objects where name = 'DF__ML__COINOVR')
	alter table ml add constraint DF__ML__COINOVR default 0 for coinovr
go
if not exists(select null from sys.objects where name = 'DF__ML__COLD_C')
	alter table ml add constraint DF__ML__COLD_C default 0 for cold_c
go
if not exists(select null from sys.objects where name = 'DF__ML__COLDOVR')
	alter table ml add constraint DF__ML__COLDOVR default 0 for coldovr
go
if not exists(select null from sys.objects where name = 'DF__ML__CUML_R')
	alter table ml add constraint DF__ML__CUML_R default 0 for cuml_r
go
if not exists(select null from sys.objects where name = 'DF__ML__CURR_R')
	alter table ml add constraint DF__ML__CURR_R default 0 for curr_r
go
if not exists(select null from sys.objects where name = 'DF__ML__DD')
	alter table ml add constraint DF__ML__DD default 0 for dd
go
if not exists(select null from sys.objects where name = 'DF__ML__DIME')
	alter table ml add constraint DF__ML__DIME default 0 for dime
go
if not exists(select null from sys.objects where name = 'DF__ML__DN')
	alter table ml add constraint DF__ML__DN default 0 for dn
go
if not exists(select null from sys.objects where name = 'DF__ML__DOORALM')
	alter table ml add constraint DF__ML__DOORALM default 0 for dooralm
go
if not exists(select null from sys.objects where name = 'DF__ML__DP')
	alter table ml add constraint DF__ML__DP default 0 for dp
go
if not exists(select null from sys.objects where name = 'DF__ML__DQ')
	alter table ml add constraint DF__ML__DQ default 0 for dq
go
if not exists(select null from sys.objects where name = 'DF__ML__DUMP_C')
	alter table ml add constraint DF__ML__DUMP_C default 0 for dump_c
go
if not exists(select null from sys.objects where name = 'DF__ML__EVNT_C')
	alter table ml add constraint DF__ML__EVNT_C default 0 for evnt_c
go
if not exists(select null from sys.objects where name = 'DF__ML__EVNTOVR')
	alter table ml add constraint DF__ML__EVNTOVR default 0 for evntovr
go
if not exists(select null from sys.objects where name = 'DF__ML__FARE_C')
	alter table ml add constraint DF__ML__FARE_C default 0 for fare_c
go
if not exists(select null from sys.objects where name = 'DF__ML__FBX_N')
	alter table ml add constraint DF__ML__FBX_N default 0 for fbx_n
go
if not exists(select null from sys.objects where name = 'DF__ML__FBX_TS')
	alter table ml add constraint DF__ML__FBX_TS default getdate() for fbx_ts
go
if not exists(select null from sys.objects where name = 'DF__ML__FIVE')
	alter table ml add constraint DF__ML__FIVE default 0 for five
go
if not exists(select null from sys.objects where name = 'DF__ML__FSCHANGE')
	alter table ml add constraint DF__ML__FSCHANGE default 0 for fschange
go
if not exists(select null from sys.objects where name = 'DF__ML__HALF')
	alter table ml add constraint DF__ML__HALF default 0 for half
go
if not exists(select null from sys.objects where name = 'DF__ML__INVALIDVER')
	alter table ml add constraint DF__ML__INVALIDVER default 0 for invalidver
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY1')
	alter table ml add constraint DF__ML__KEY1 default 0 for key1
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY2')
	alter table ml add constraint DF__ML__KEY2 default 0 for key2
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY3')
	alter table ml add constraint DF__ML__KEY3 default 0 for key3
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY4')
	alter table ml add constraint DF__ML__KEY4 default 0 for key4
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY5')
	alter table ml add constraint DF__ML__KEY5 default 0 for key5
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY6')
	alter table ml add constraint DF__ML__KEY6 default 0 for key6
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY7')
	alter table ml add constraint DF__ML__KEY7 default 0 for key7
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY8')
	alter table ml add constraint DF__ML__KEY8 default 0 for key8
go
if not exists(select null from sys.objects where name = 'DF__ML__KEY9')
	alter table ml add constraint DF__ML__KEY9 default 0 for key9
go
if not exists(select null from sys.objects where name = 'DF__ML__KEYA')
	alter table ml add constraint DF__ML__KEYA default 0 for keya
go
if not exists(select null from sys.objects where name = 'DF__ML__KEYAST')
	alter table ml add constraint DF__ML__KEYAST default 0 for keyast
go
if not exists(select null from sys.objects where name = 'DF__ML__KEYB')
	alter table ml add constraint DF__ML__KEYB default 0 for keyb
go
if not exists(select null from sys.objects where name = 'DF__ML__KEYC')
	alter table ml add constraint DF__ML__KEYC default 0 for keyc
go
if not exists(select null from sys.objects where name = 'DF__ML__KEYD')
	alter table ml add constraint DF__ML__KEYD default 0 for keyd
go
if not exists(select null from sys.objects where name = 'DF__ML__MEMCLR')
	alter table ml add constraint DF__ML__MEMCLR default 0 for memclr
go
if not exists(select null from sys.objects where name = 'DF__ML__NICKEL')
	alter table ml add constraint DF__ML__NICKEL default 0 for nickel
go
if not exists(select null from sys.objects where name = 'DF__ML__NQ')
	alter table ml add constraint DF__ML__NQ default 0 for nq
go
if not exists(select null from sys.objects where name = 'DF__ML__ONE')
	alter table ml add constraint DF__ML__ONE default 0 for one
go
if not exists(select null from sys.objects where name = 'DF__ML__PASS_C')
	alter table ml add constraint DF__ML__PASS_C default 0 for pass_c
go
if not exists(select null from sys.objects where name = 'DF__ML__PASS_E')
	alter table ml add constraint DF__ML__PASS_E default 0 for pass_e
go
if not exists(select null from sys.objects where name = 'DF__ML__PASS_T')
	alter table ml add constraint DF__ML__PASS_T default 0 for pass_t
go
if not exists(select null from sys.objects where name = 'DF__ML__PASS_X')
	alter table ml add constraint DF__ML__PASS_X default 0 for pass_x
go
if not exists(select null from sys.objects where name = 'DF__ML__PASSERROVR')
	alter table ml add constraint DF__ML__PASSERROVR default 0 for passerrovr
go
if not exists(select null from sys.objects where name = 'DF__ML__PASSOVR')
	alter table ml add constraint DF__ML__PASSOVR default 0 for passovr
go
if not exists(select null from sys.objects where name = 'DF__ML__PDU')
	alter table ml add constraint DF__ML__PDU default 0 for pdu
go
if not exists(select null from sys.objects where name = 'DF__ML__PENNY')
	alter table ml add constraint DF__ML__PENNY default 0 for penny
go
if not exists(select null from sys.objects where name = 'DF__ML__PN')
	alter table ml add constraint DF__ML__PN default 0 for pn
go
if not exists(select null from sys.objects where name = 'DF__ML__PP')
	alter table ml add constraint DF__ML__PP default 0 for pp
go
if not exists(select null from sys.objects where name = 'DF__ML__PQ')
	alter table ml add constraint DF__ML__PQ default 0 for pq
go
if not exists(select null from sys.objects where name = 'DF__ML__PRB_N')
	alter table ml add constraint DF__ML__PRB_N default 0 for prb_n
go
if not exists(select null from sys.objects where name = 'DF__ML__PRB_T')
	alter table ml add constraint DF__ML__PRB_T default 0 for prb_t
go
if not exists(select null from sys.objects where name = 'DF__ML__QQ')
	alter table ml add constraint DF__ML__QQ default 0 for qq
go
if not exists(select null from sys.objects where name = 'DF__ML__QUARTER')
	alter table ml add constraint DF__ML__QUARTER default 0 for quarter
go
if not exists(select null from sys.objects where name = 'DF__ML__RDR_C')
	alter table ml add constraint DF__ML__RDR_C default 0 for rdr_c
go
if not exists(select null from sys.objects where name = 'DF__ML__REVDSC')
	alter table ml add constraint DF__ML__REVDSC default 0 for revdsc
go
if not exists(select null from sys.objects where name = 'DF__ML__SBA')
	alter table ml add constraint DF__ML__SBA default 0 for sba
go
if not exists(select null from sys.objects where name = 'DF__ML__TBIG')
	alter table ml add constraint DF__ML__TBIG default 0 for tbig
go
if not exists(select null from sys.objects where name = 'DF__ML__TEN')
	alter table ml add constraint DF__ML__TEN default 0 for ten
go
if not exists(select null from sys.objects where name = 'DF__ML__TICKET_C')
	alter table ml add constraint DF__ML__TICKET_C default 0 for ticket_c
go
if not exists(select null from sys.objects where name = 'DF__ML__TIMEDSC')
	alter table ml add constraint DF__ML__TIMEDSC default 0 for timedsc
go
if not exists(select null from sys.objects where name = 'DF__ML__TOKEN_C')
	alter table ml add constraint DF__ML__TOKEN_C default 0 for token_c
go
if not exists(select null from sys.objects where name = 'DF__ML__TS')
	alter table ml add constraint DF__ML__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__ML__TSMALL')
	alter table ml add constraint DF__ML__TSMALL default 0 for tsmall
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP1')
	alter table ml add constraint DF__ML__TTP1 default 0 for ttp1
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP10')
	alter table ml add constraint DF__ML__TTP10 default 0 for ttp10
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP11')
	alter table ml add constraint DF__ML__TTP11 default 0 for ttp11
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP12')
	alter table ml add constraint DF__ML__TTP12 default 0 for ttp12
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP13')
	alter table ml add constraint DF__ML__TTP13 default 0 for ttp13
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP14')
	alter table ml add constraint DF__ML__TTP14 default 0 for ttp14
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP15')
	alter table ml add constraint DF__ML__TTP15 default 0 for ttp15
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP16')
	alter table ml add constraint DF__ML__TTP16 default 0 for ttp16
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP17')
	alter table ml add constraint DF__ML__TTP17 default 0 for ttp17
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP18')
	alter table ml add constraint DF__ML__TTP18 default 0 for ttp18
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP19')
	alter table ml add constraint DF__ML__TTP19 default 0 for ttp19
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP2')
	alter table ml add constraint DF__ML__TTP2 default 0 for ttp2
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP20')
	alter table ml add constraint DF__ML__TTP20 default 0 for ttp20
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP21')
	alter table ml add constraint DF__ML__TTP21 default 0 for ttp21
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP22')
	alter table ml add constraint DF__ML__TTP22 default 0 for ttp22
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP23')
	alter table ml add constraint DF__ML__TTP23 default 0 for ttp23
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP24')
	alter table ml add constraint DF__ML__TTP24 default 0 for ttp24
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP25')
	alter table ml add constraint DF__ML__TTP25 default 0 for ttp25
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP26')
	alter table ml add constraint DF__ML__TTP26 default 0 for ttp26
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP27')
	alter table ml add constraint DF__ML__TTP27 default 0 for ttp27
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP28')
	alter table ml add constraint DF__ML__TTP28 default 0 for ttp28
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP29')
	alter table ml add constraint DF__ML__TTP29 default 0 for ttp29
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP3')
	alter table ml add constraint DF__ML__TTP3 default 0 for ttp3
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP30')
	alter table ml add constraint DF__ML__TTP30 default 0 for ttp30
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP31')
	alter table ml add constraint DF__ML__TTP31 default 0 for ttp31
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP32')
	alter table ml add constraint DF__ML__TTP32 default 0 for ttp32
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP33')
	alter table ml add constraint DF__ML__TTP33 default 0 for ttp33
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP34')
	alter table ml add constraint DF__ML__TTP34 default 0 for ttp34
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP35')
	alter table ml add constraint DF__ML__TTP35 default 0 for ttp35
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP36')
	alter table ml add constraint DF__ML__TTP36 default 0 for ttp36
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP37')
	alter table ml add constraint DF__ML__TTP37 default 0 for ttp37
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP38')
	alter table ml add constraint DF__ML__TTP38 default 0 for ttp38
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP39')
	alter table ml add constraint DF__ML__TTP39 default 0 for ttp39
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP4')
	alter table ml add constraint DF__ML__TTP4 default 0 for ttp4
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP40')
	alter table ml add constraint DF__ML__TTP40 default 0 for ttp40
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP41')
	alter table ml add constraint DF__ML__TTP41 default 0 for ttp41
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP42')
	alter table ml add constraint DF__ML__TTP42 default 0 for ttp42
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP43')
	alter table ml add constraint DF__ML__TTP43 default 0 for ttp43
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP44')
	alter table ml add constraint DF__ML__TTP44 default 0 for ttp44
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP45')
	alter table ml add constraint DF__ML__TTP45 default 0 for ttp45
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP46')
	alter table ml add constraint DF__ML__TTP46 default 0 for ttp46
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP47')
	alter table ml add constraint DF__ML__TTP47 default 0 for ttp47
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP48')
	alter table ml add constraint DF__ML__TTP48 default 0 for ttp48
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP5')
	alter table ml add constraint DF__ML__TTP5 default 0 for ttp5
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP6')
	alter table ml add constraint DF__ML__TTP6 default 0 for ttp6
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP7')
	alter table ml add constraint DF__ML__TTP7 default 0 for ttp7
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP8')
	alter table ml add constraint DF__ML__TTP8 default 0 for ttp8
go
if not exists(select null from sys.objects where name = 'DF__ML__TTP9')
	alter table ml add constraint DF__ML__TTP9 default 0 for ttp9
go
if not exists(select null from sys.objects where name = 'DF__ML__TWENTY')
	alter table ml add constraint DF__ML__TWENTY default 0 for twenty
go
if not exists(select null from sys.objects where name = 'DF__ML__TWO')
	alter table ml add constraint DF__ML__TWO default 0 for two
go
if not exists(select null from sys.objects where name = 'DF__ML__UNCL_R')
	alter table ml add constraint DF__ML__UNCL_R default 0 for uncl_r
go
if not exists(select null from sys.objects where name = 'DF__ML__VER_N')
	alter table ml add constraint DF__ML__VER_N default 0 for ver_n
go
if not exists(select null from sys.objects where name = 'DF__ML__WARM_C')
	alter table ml add constraint DF__ML__WARM_C default 0 for warm_c
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP49')
	alter table ml_extd add constraint DF__ML_EXTD__TTP49 default 0 for ttp49
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP50')
	alter table ml_extd add constraint DF__ML_EXTD__TTP50 default 0 for ttp50
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP51')
	alter table ml_extd add constraint DF__ML_EXTD__TTP51 default 0 for ttp51
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP52')
	alter table ml_extd add constraint DF__ML_EXTD__TTP52 default 0 for ttp52
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP53')
	alter table ml_extd add constraint DF__ML_EXTD__TTP53 default 0 for ttp53
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP54')
	alter table ml_extd add constraint DF__ML_EXTD__TTP54 default 0 for ttp54
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP55')
	alter table ml_extd add constraint DF__ML_EXTD__TTP55 default 0 for ttp55
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP56')
	alter table ml_extd add constraint DF__ML_EXTD__TTP56 default 0 for ttp56
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP57')
	alter table ml_extd add constraint DF__ML_EXTD__TTP57 default 0 for ttp57
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP58')
	alter table ml_extd add constraint DF__ML_EXTD__TTP58 default 0 for ttp58
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP59')
	alter table ml_extd add constraint DF__ML_EXTD__TTP59 default 0 for ttp59
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP60')
	alter table ml_extd add constraint DF__ML_EXTD__TTP60 default 0 for ttp60
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP61')
	alter table ml_extd add constraint DF__ML_EXTD__TTP61 default 0 for ttp61
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP62')
	alter table ml_extd add constraint DF__ML_EXTD__TTP62 default 0 for ttp62
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP63')
	alter table ml_extd add constraint DF__ML_EXTD__TTP63 default 0 for ttp63
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP64')
	alter table ml_extd add constraint DF__ML_EXTD__TTP64 default 0 for ttp64
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP65')
	alter table ml_extd add constraint DF__ML_EXTD__TTP65 default 0 for ttp65
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP66')
	alter table ml_extd add constraint DF__ML_EXTD__TTP66 default 0 for ttp66
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP67')
	alter table ml_extd add constraint DF__ML_EXTD__TTP67 default 0 for ttp67
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP68')
	alter table ml_extd add constraint DF__ML_EXTD__TTP68 default 0 for ttp68
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP69')
	alter table ml_extd add constraint DF__ML_EXTD__TTP69 default 0 for ttp69
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP70')
	alter table ml_extd add constraint DF__ML_EXTD__TTP70 default 0 for ttp70
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP71')
	alter table ml_extd add constraint DF__ML_EXTD__TTP71 default 0 for ttp71
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP72')
	alter table ml_extd add constraint DF__ML_EXTD__TTP72 default 0 for ttp72
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP73')
	alter table ml_extd add constraint DF__ML_EXTD__TTP73 default 0 for ttp73
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP74')
	alter table ml_extd add constraint DF__ML_EXTD__TTP74 default 0 for ttp74
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP75')
	alter table ml_extd add constraint DF__ML_EXTD__TTP75 default 0 for ttp75
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP76')
	alter table ml_extd add constraint DF__ML_EXTD__TTP76 default 0 for ttp76
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP77')
	alter table ml_extd add constraint DF__ML_EXTD__TTP77 default 0 for ttp77
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP78')
	alter table ml_extd add constraint DF__ML_EXTD__TTP78 default 0 for ttp78
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP79')
	alter table ml_extd add constraint DF__ML_EXTD__TTP79 default 0 for ttp79
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP80')
	alter table ml_extd add constraint DF__ML_EXTD__TTP80 default 0 for ttp80
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP81')
	alter table ml_extd add constraint DF__ML_EXTD__TTP81 default 0 for ttp81
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP82')
	alter table ml_extd add constraint DF__ML_EXTD__TTP82 default 0 for ttp82
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP83')
	alter table ml_extd add constraint DF__ML_EXTD__TTP83 default 0 for ttp83
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP84')
	alter table ml_extd add constraint DF__ML_EXTD__TTP84 default 0 for ttp84
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP85')
	alter table ml_extd add constraint DF__ML_EXTD__TTP85 default 0 for ttp85
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP86')
	alter table ml_extd add constraint DF__ML_EXTD__TTP86 default 0 for ttp86
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP87')
	alter table ml_extd add constraint DF__ML_EXTD__TTP87 default 0 for ttp87
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP88')
	alter table ml_extd add constraint DF__ML_EXTD__TTP88 default 0 for ttp88
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP89')
	alter table ml_extd add constraint DF__ML_EXTD__TTP89 default 0 for ttp89
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP90')
	alter table ml_extd add constraint DF__ML_EXTD__TTP90 default 0 for ttp90
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP91')
	alter table ml_extd add constraint DF__ML_EXTD__TTP91 default 0 for ttp91
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP92')
	alter table ml_extd add constraint DF__ML_EXTD__TTP92 default 0 for ttp92
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP93')
	alter table ml_extd add constraint DF__ML_EXTD__TTP93 default 0 for ttp93
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP94')
	alter table ml_extd add constraint DF__ML_EXTD__TTP94 default 0 for ttp94
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP95')
	alter table ml_extd add constraint DF__ML_EXTD__TTP95 default 0 for ttp95
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP96')
	alter table ml_extd add constraint DF__ML_EXTD__TTP96 default 0 for ttp96
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP97')
	alter table ml_extd add constraint DF__ML_EXTD__TTP97 default 0 for ttp97
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP98')
	alter table ml_extd add constraint DF__ML_EXTD__TTP98 default 0 for ttp98
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP99')
	alter table ml_extd add constraint DF__ML_EXTD__TTP99 default 0 for ttp99
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP100')
	alter table ml_extd add constraint DF__ML_EXTD__TTP100 default 0 for ttp100
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP101')
	alter table ml_extd add constraint DF__ML_EXTD__TTP101 default 0 for ttp101
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP102')
	alter table ml_extd add constraint DF__ML_EXTD__TTP102 default 0 for ttp102
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP103')
	alter table ml_extd add constraint DF__ML_EXTD__TTP103 default 0 for ttp103
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP104')
	alter table ml_extd add constraint DF__ML_EXTD__TTP104 default 0 for ttp104
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP105')
	alter table ml_extd add constraint DF__ML_EXTD__TTP105 default 0 for ttp105
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP106')
	alter table ml_extd add constraint DF__ML_EXTD__TTP106 default 0 for ttp106
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP107')
	alter table ml_extd add constraint DF__ML_EXTD__TTP107 default 0 for ttp107
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP108')
	alter table ml_extd add constraint DF__ML_EXTD__TTP108 default 0 for ttp108
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP109')
	alter table ml_extd add constraint DF__ML_EXTD__TTP109 default 0 for ttp109
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP110')
	alter table ml_extd add constraint DF__ML_EXTD__TTP110 default 0 for ttp110
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP111')
	alter table ml_extd add constraint DF__ML_EXTD__TTP111 default 0 for ttp111
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP112')
	alter table ml_extd add constraint DF__ML_EXTD__TTP112 default 0 for ttp112
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP113')
	alter table ml_extd add constraint DF__ML_EXTD__TTP113 default 0 for ttp113
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP114')
	alter table ml_extd add constraint DF__ML_EXTD__TTP114 default 0 for ttp114
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP115')
	alter table ml_extd add constraint DF__ML_EXTD__TTP115 default 0 for ttp115
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP116')
	alter table ml_extd add constraint DF__ML_EXTD__TTP116 default 0 for ttp116
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP117')
	alter table ml_extd add constraint DF__ML_EXTD__TTP117 default 0 for ttp117
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP118')
	alter table ml_extd add constraint DF__ML_EXTD__TTP118 default 0 for ttp118
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP119')
	alter table ml_extd add constraint DF__ML_EXTD__TTP119 default 0 for ttp119
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP120')
	alter table ml_extd add constraint DF__ML_EXTD__TTP120 default 0 for ttp120
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP121')
	alter table ml_extd add constraint DF__ML_EXTD__TTP121 default 0 for ttp121
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP122')
	alter table ml_extd add constraint DF__ML_EXTD__TTP122 default 0 for ttp122
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP123')
	alter table ml_extd add constraint DF__ML_EXTD__TTP123 default 0 for ttp123
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP124')
	alter table ml_extd add constraint DF__ML_EXTD__TTP124 default 0 for ttp124
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP125')
	alter table ml_extd add constraint DF__ML_EXTD__TTP125 default 0 for ttp125
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP126')
	alter table ml_extd add constraint DF__ML_EXTD__TTP126 default 0 for ttp126
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP127')
	alter table ml_extd add constraint DF__ML_EXTD__TTP127 default 0 for ttp127
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP128')
	alter table ml_extd add constraint DF__ML_EXTD__TTP128 default 0 for ttp128
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP129')
	alter table ml_extd add constraint DF__ML_EXTD__TTP129 default 0 for ttp129
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP130')
	alter table ml_extd add constraint DF__ML_EXTD__TTP130 default 0 for ttp130
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP131')
	alter table ml_extd add constraint DF__ML_EXTD__TTP131 default 0 for ttp131
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP132')
	alter table ml_extd add constraint DF__ML_EXTD__TTP132 default 0 for ttp132
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP133')
	alter table ml_extd add constraint DF__ML_EXTD__TTP133 default 0 for ttp133
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP134')
	alter table ml_extd add constraint DF__ML_EXTD__TTP134 default 0 for ttp134
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP135')
	alter table ml_extd add constraint DF__ML_EXTD__TTP135 default 0 for ttp135
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP136')
	alter table ml_extd add constraint DF__ML_EXTD__TTP136 default 0 for ttp136
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP137')
	alter table ml_extd add constraint DF__ML_EXTD__TTP137 default 0 for ttp137
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP138')
	alter table ml_extd add constraint DF__ML_EXTD__TTP138 default 0 for ttp138
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP139')
	alter table ml_extd add constraint DF__ML_EXTD__TTP139 default 0 for ttp139
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP140')
	alter table ml_extd add constraint DF__ML_EXTD__TTP140 default 0 for ttp140
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP141')
	alter table ml_extd add constraint DF__ML_EXTD__TTP141 default 0 for ttp141
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP142')
	alter table ml_extd add constraint DF__ML_EXTD__TTP142 default 0 for ttp142
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP143')
	alter table ml_extd add constraint DF__ML_EXTD__TTP143 default 0 for ttp143
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP144')
	alter table ml_extd add constraint DF__ML_EXTD__TTP144 default 0 for ttp144
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP145')
	alter table ml_extd add constraint DF__ML_EXTD__TTP145 default 0 for ttp145
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP146')
	alter table ml_extd add constraint DF__ML_EXTD__TTP146 default 0 for ttp146
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP147')
	alter table ml_extd add constraint DF__ML_EXTD__TTP147 default 0 for ttp147
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP148')
	alter table ml_extd add constraint DF__ML_EXTD__TTP148 default 0 for ttp148
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP149')
	alter table ml_extd add constraint DF__ML_EXTD__TTP149 default 0 for ttp149
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP150')
	alter table ml_extd add constraint DF__ML_EXTD__TTP150 default 0 for ttp150
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP151')
	alter table ml_extd add constraint DF__ML_EXTD__TTP151 default 0 for ttp151
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP152')
	alter table ml_extd add constraint DF__ML_EXTD__TTP152 default 0 for ttp152
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP153')
	alter table ml_extd add constraint DF__ML_EXTD__TTP153 default 0 for ttp153
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP154')
	alter table ml_extd add constraint DF__ML_EXTD__TTP154 default 0 for ttp154
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP155')
	alter table ml_extd add constraint DF__ML_EXTD__TTP155 default 0 for ttp155
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP156')
	alter table ml_extd add constraint DF__ML_EXTD__TTP156 default 0 for ttp156
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP157')
	alter table ml_extd add constraint DF__ML_EXTD__TTP157 default 0 for ttp157
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP158')
	alter table ml_extd add constraint DF__ML_EXTD__TTP158 default 0 for ttp158
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP159')
	alter table ml_extd add constraint DF__ML_EXTD__TTP159 default 0 for ttp159
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP160')
	alter table ml_extd add constraint DF__ML_EXTD__TTP160 default 0 for ttp160
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP161')
	alter table ml_extd add constraint DF__ML_EXTD__TTP161 default 0 for ttp161
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP162')
	alter table ml_extd add constraint DF__ML_EXTD__TTP162 default 0 for ttp162
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP163')
	alter table ml_extd add constraint DF__ML_EXTD__TTP163 default 0 for ttp163
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP164')
	alter table ml_extd add constraint DF__ML_EXTD__TTP164 default 0 for ttp164
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP165')
	alter table ml_extd add constraint DF__ML_EXTD__TTP165 default 0 for ttp165
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP166')
	alter table ml_extd add constraint DF__ML_EXTD__TTP166 default 0 for ttp166
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP167')
	alter table ml_extd add constraint DF__ML_EXTD__TTP167 default 0 for ttp167
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP168')
	alter table ml_extd add constraint DF__ML_EXTD__TTP168 default 0 for ttp168
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP169')
	alter table ml_extd add constraint DF__ML_EXTD__TTP169 default 0 for ttp169
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP170')
	alter table ml_extd add constraint DF__ML_EXTD__TTP170 default 0 for ttp170
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP171')
	alter table ml_extd add constraint DF__ML_EXTD__TTP171 default 0 for ttp171
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP172')
	alter table ml_extd add constraint DF__ML_EXTD__TTP172 default 0 for ttp172
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP173')
	alter table ml_extd add constraint DF__ML_EXTD__TTP173 default 0 for ttp173
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP174')
	alter table ml_extd add constraint DF__ML_EXTD__TTP174 default 0 for ttp174
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP175')
	alter table ml_extd add constraint DF__ML_EXTD__TTP175 default 0 for ttp175
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP176')
	alter table ml_extd add constraint DF__ML_EXTD__TTP176 default 0 for ttp176
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP177')
	alter table ml_extd add constraint DF__ML_EXTD__TTP177 default 0 for ttp177
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP178')
	alter table ml_extd add constraint DF__ML_EXTD__TTP178 default 0 for ttp178
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP179')
	alter table ml_extd add constraint DF__ML_EXTD__TTP179 default 0 for ttp179
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP180')
	alter table ml_extd add constraint DF__ML_EXTD__TTP180 default 0 for ttp180
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP181')
	alter table ml_extd add constraint DF__ML_EXTD__TTP181 default 0 for ttp181
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP182')
	alter table ml_extd add constraint DF__ML_EXTD__TTP182 default 0 for ttp182
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP183')
	alter table ml_extd add constraint DF__ML_EXTD__TTP183 default 0 for ttp183
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP184')
	alter table ml_extd add constraint DF__ML_EXTD__TTP184 default 0 for ttp184
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP185')
	alter table ml_extd add constraint DF__ML_EXTD__TTP185 default 0 for ttp185
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP186')
	alter table ml_extd add constraint DF__ML_EXTD__TTP186 default 0 for ttp186
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP187')
	alter table ml_extd add constraint DF__ML_EXTD__TTP187 default 0 for ttp187
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP188')
	alter table ml_extd add constraint DF__ML_EXTD__TTP188 default 0 for ttp188
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP189')
	alter table ml_extd add constraint DF__ML_EXTD__TTP189 default 0 for ttp189
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP190')
	alter table ml_extd add constraint DF__ML_EXTD__TTP190 default 0 for ttp190
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP191')
	alter table ml_extd add constraint DF__ML_EXTD__TTP191 default 0 for ttp191
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP192')
	alter table ml_extd add constraint DF__ML_EXTD__TTP192 default 0 for ttp192
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP193')
	alter table ml_extd add constraint DF__ML_EXTD__TTP193 default 0 for ttp193
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP194')
	alter table ml_extd add constraint DF__ML_EXTD__TTP194 default 0 for ttp194
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP195')
	alter table ml_extd add constraint DF__ML_EXTD__TTP195 default 0 for ttp195
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP196')
	alter table ml_extd add constraint DF__ML_EXTD__TTP196 default 0 for ttp196
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP197')
	alter table ml_extd add constraint DF__ML_EXTD__TTP197 default 0 for ttp197
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP198')
	alter table ml_extd add constraint DF__ML_EXTD__TTP198 default 0 for ttp198
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP199')
	alter table ml_extd add constraint DF__ML_EXTD__TTP199 default 0 for ttp199
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP200')
	alter table ml_extd add constraint DF__ML_EXTD__TTP200 default 0 for ttp200
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP201')
	alter table ml_extd add constraint DF__ML_EXTD__TTP201 default 0 for ttp201
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP202')
	alter table ml_extd add constraint DF__ML_EXTD__TTP202 default 0 for ttp202
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP203')
	alter table ml_extd add constraint DF__ML_EXTD__TTP203 default 0 for ttp203
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP204')
	alter table ml_extd add constraint DF__ML_EXTD__TTP204 default 0 for ttp204
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP205')
	alter table ml_extd add constraint DF__ML_EXTD__TTP205 default 0 for ttp205
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP206')
	alter table ml_extd add constraint DF__ML_EXTD__TTP206 default 0 for ttp206
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP207')
	alter table ml_extd add constraint DF__ML_EXTD__TTP207 default 0 for ttp207
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP208')
	alter table ml_extd add constraint DF__ML_EXTD__TTP208 default 0 for ttp208
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP209')
	alter table ml_extd add constraint DF__ML_EXTD__TTP209 default 0 for ttp209
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP210')
	alter table ml_extd add constraint DF__ML_EXTD__TTP210 default 0 for ttp210
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP211')
	alter table ml_extd add constraint DF__ML_EXTD__TTP211 default 0 for ttp211
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP212')
	alter table ml_extd add constraint DF__ML_EXTD__TTP212 default 0 for ttp212
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP213')
	alter table ml_extd add constraint DF__ML_EXTD__TTP213 default 0 for ttp213
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP214')
	alter table ml_extd add constraint DF__ML_EXTD__TTP214 default 0 for ttp214
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP215')
	alter table ml_extd add constraint DF__ML_EXTD__TTP215 default 0 for ttp215
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP216')
	alter table ml_extd add constraint DF__ML_EXTD__TTP216 default 0 for ttp216
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP217')
	alter table ml_extd add constraint DF__ML_EXTD__TTP217 default 0 for ttp217
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP218')
	alter table ml_extd add constraint DF__ML_EXTD__TTP218 default 0 for ttp218
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP219')
	alter table ml_extd add constraint DF__ML_EXTD__TTP219 default 0 for ttp219
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP220')
	alter table ml_extd add constraint DF__ML_EXTD__TTP220 default 0 for ttp220
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP221')
	alter table ml_extd add constraint DF__ML_EXTD__TTP221 default 0 for ttp221
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP222')
	alter table ml_extd add constraint DF__ML_EXTD__TTP222 default 0 for ttp222
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP223')
	alter table ml_extd add constraint DF__ML_EXTD__TTP223 default 0 for ttp223
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP224')
	alter table ml_extd add constraint DF__ML_EXTD__TTP224 default 0 for ttp224
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP225')
	alter table ml_extd add constraint DF__ML_EXTD__TTP225 default 0 for ttp225
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP226')
	alter table ml_extd add constraint DF__ML_EXTD__TTP226 default 0 for ttp226
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP227')
	alter table ml_extd add constraint DF__ML_EXTD__TTP227 default 0 for ttp227
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP228')
	alter table ml_extd add constraint DF__ML_EXTD__TTP228 default 0 for ttp228
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP229')
	alter table ml_extd add constraint DF__ML_EXTD__TTP229 default 0 for ttp229
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP230')
	alter table ml_extd add constraint DF__ML_EXTD__TTP230 default 0 for ttp230
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP231')
	alter table ml_extd add constraint DF__ML_EXTD__TTP231 default 0 for ttp231
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP232')
	alter table ml_extd add constraint DF__ML_EXTD__TTP232 default 0 for ttp232
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP233')
	alter table ml_extd add constraint DF__ML_EXTD__TTP233 default 0 for ttp233
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP234')
	alter table ml_extd add constraint DF__ML_EXTD__TTP234 default 0 for ttp234
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP235')
	alter table ml_extd add constraint DF__ML_EXTD__TTP235 default 0 for ttp235
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP236')
	alter table ml_extd add constraint DF__ML_EXTD__TTP236 default 0 for ttp236
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP237')
	alter table ml_extd add constraint DF__ML_EXTD__TTP237 default 0 for ttp237
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP238')
	alter table ml_extd add constraint DF__ML_EXTD__TTP238 default 0 for ttp238
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP239')
	alter table ml_extd add constraint DF__ML_EXTD__TTP239 default 0 for ttp239
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP240')
	alter table ml_extd add constraint DF__ML_EXTD__TTP240 default 0 for ttp240
go
if not exists(select null from sys.objects where name = 'DF__ML_EXTD__TTP241')
	alter table ml_extd add constraint DF__ML_EXTD__TTP241 default 0 for ttp241
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__FIFTY')
	alter table mrbill add constraint DF__MRBILL__FIFTY default 0 for fifty
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__FIVE')
	alter table mrbill add constraint DF__MRBILL__FIVE default 0 for five
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__HUNDRED')
	alter table mrbill add constraint DF__MRBILL__HUNDRED default 0 for hundred
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__ONE')
	alter table mrbill add constraint DF__MRBILL__ONE default 0 for one
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__TEN')
	alter table mrbill add constraint DF__MRBILL__TEN default 0 for ten
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__TWENTY')
	alter table mrbill add constraint DF__MRBILL__TWENTY default 0 for twenty
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__TWO')
	alter table mrbill add constraint DF__MRBILL__TWO default 0 for two
go
if not exists(select null from sys.objects where name = 'DF__MRBILL__TYPE')
	alter table mrbill add constraint DF__MRBILL__TYPE default 1 for type
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__DIME')
	alter table mrcoin add constraint DF__MRCOIN__DIME default 0 for dime
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__HALF')
	alter table mrcoin add constraint DF__MRCOIN__HALF default 0 for half
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__NICKEL')
	alter table mrcoin add constraint DF__MRCOIN__NICKEL default 0 for nickel
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__PENNY')
	alter table mrcoin add constraint DF__MRCOIN__PENNY default 0 for penny
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__QUARTER')
	alter table mrcoin add constraint DF__MRCOIN__QUARTER default 0 for quarter
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__SBA')
	alter table mrcoin add constraint DF__MRCOIN__SBA default 0 for sba
go
if not exists(select null from sys.objects where name = 'DF__MRCOIN__TYPE')
	alter table mrcoin add constraint DF__MRCOIN__TYPE default 1 for type
go
if not exists(select null from sys.objects where name = 'DF__MRSUM__BILL_VAL')
	alter table mrsum add constraint DF__MRSUM__BILL_VAL default 0 for bill_val
go
if not exists(select null from sys.objects where name = 'DF__MRSUM__COIN_VAL')
	alter table mrsum add constraint DF__MRSUM__COIN_VAL default 0 for coin_val
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__BILL_C')
	alter table mrtesum add constraint DF__MRTESUM__BILL_C default 0 for bill_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__CURR_R')
	alter table mrtesum add constraint DF__MRTESUM__CURR_R default 0 for curr_r
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__DUMP_C')
	alter table mrtesum add constraint DF__MRTESUM__DUMP_C default 0 for dump_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE_C default 0 for fare_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE1_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE1_C default 0 for fare1_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE10_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE10_C default 0 for fare10_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE2_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE2_C default 0 for fare2_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE3_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE3_C default 0 for fare3_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE4_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE4_C default 0 for fare4_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE5_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE5_C default 0 for fare5_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE6_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE6_C default 0 for fare6_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE7_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE7_C default 0 for fare7_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE8_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE8_C default 0 for fare8_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__FARE9_C')
	alter table mrtesum add constraint DF__MRTESUM__FARE9_C default 0 for fare9_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY1')
	alter table mrtesum add constraint DF__MRTESUM__KEY1 default 0 for key1
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY2')
	alter table mrtesum add constraint DF__MRTESUM__KEY2 default 0 for key2
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY3')
	alter table mrtesum add constraint DF__MRTESUM__KEY3 default 0 for key3
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY4')
	alter table mrtesum add constraint DF__MRTESUM__KEY4 default 0 for key4
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY5')
	alter table mrtesum add constraint DF__MRTESUM__KEY5 default 0 for key5
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY6')
	alter table mrtesum add constraint DF__MRTESUM__KEY6 default 0 for key6
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY7')
	alter table mrtesum add constraint DF__MRTESUM__KEY7 default 0 for key7
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY8')
	alter table mrtesum add constraint DF__MRTESUM__KEY8 default 0 for key8
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEY9')
	alter table mrtesum add constraint DF__MRTESUM__KEY9 default 0 for key9
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEYA')
	alter table mrtesum add constraint DF__MRTESUM__KEYA default 0 for keya
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEYAST')
	alter table mrtesum add constraint DF__MRTESUM__KEYAST default 0 for keyast
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEYB')
	alter table mrtesum add constraint DF__MRTESUM__KEYB default 0 for keyb
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEYC')
	alter table mrtesum add constraint DF__MRTESUM__KEYC default 0 for keyc
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__KEYD')
	alter table mrtesum add constraint DF__MRTESUM__KEYD default 0 for keyd
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__PASS_C')
	alter table mrtesum add constraint DF__MRTESUM__PASS_C default 0 for pass_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__RDR_C')
	alter table mrtesum add constraint DF__MRTESUM__RDR_C default 0 for rdr_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TICKET_C')
	alter table mrtesum add constraint DF__MRTESUM__TICKET_C default 0 for ticket_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TOKEN_C')
	alter table mrtesum add constraint DF__MRTESUM__TOKEN_C default 0 for token_c
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP1')
	alter table mrtesum add constraint DF__MRTESUM__TTP1 default 0 for ttp1
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP10')
	alter table mrtesum add constraint DF__MRTESUM__TTP10 default 0 for ttp10
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP11')
	alter table mrtesum add constraint DF__MRTESUM__TTP11 default 0 for ttp11
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP12')
	alter table mrtesum add constraint DF__MRTESUM__TTP12 default 0 for ttp12
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP13')
	alter table mrtesum add constraint DF__MRTESUM__TTP13 default 0 for ttp13
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP14')
	alter table mrtesum add constraint DF__MRTESUM__TTP14 default 0 for ttp14
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP15')
	alter table mrtesum add constraint DF__MRTESUM__TTP15 default 0 for ttp15
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP16')
	alter table mrtesum add constraint DF__MRTESUM__TTP16 default 0 for ttp16
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP17')
	alter table mrtesum add constraint DF__MRTESUM__TTP17 default 0 for ttp17
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP18')
	alter table mrtesum add constraint DF__MRTESUM__TTP18 default 0 for ttp18
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP19')
	alter table mrtesum add constraint DF__MRTESUM__TTP19 default 0 for ttp19
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP2')
	alter table mrtesum add constraint DF__MRTESUM__TTP2 default 0 for ttp2
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP20')
	alter table mrtesum add constraint DF__MRTESUM__TTP20 default 0 for ttp20
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP21')
	alter table mrtesum add constraint DF__MRTESUM__TTP21 default 0 for ttp21
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP22')
	alter table mrtesum add constraint DF__MRTESUM__TTP22 default 0 for ttp22
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP23')
	alter table mrtesum add constraint DF__MRTESUM__TTP23 default 0 for ttp23
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP24')
	alter table mrtesum add constraint DF__MRTESUM__TTP24 default 0 for ttp24
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP25')
	alter table mrtesum add constraint DF__MRTESUM__TTP25 default 0 for ttp25
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP26')
	alter table mrtesum add constraint DF__MRTESUM__TTP26 default 0 for ttp26
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP27')
	alter table mrtesum add constraint DF__MRTESUM__TTP27 default 0 for ttp27
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP28')
	alter table mrtesum add constraint DF__MRTESUM__TTP28 default 0 for ttp28
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP29')
	alter table mrtesum add constraint DF__MRTESUM__TTP29 default 0 for ttp29
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP3')
	alter table mrtesum add constraint DF__MRTESUM__TTP3 default 0 for ttp3
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP30')
	alter table mrtesum add constraint DF__MRTESUM__TTP30 default 0 for ttp30
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP31')
	alter table mrtesum add constraint DF__MRTESUM__TTP31 default 0 for ttp31
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP32')
	alter table mrtesum add constraint DF__MRTESUM__TTP32 default 0 for ttp32
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP33')
	alter table mrtesum add constraint DF__MRTESUM__TTP33 default 0 for ttp33
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP34')
	alter table mrtesum add constraint DF__MRTESUM__TTP34 default 0 for ttp34
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP35')
	alter table mrtesum add constraint DF__MRTESUM__TTP35 default 0 for ttp35
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP36')
	alter table mrtesum add constraint DF__MRTESUM__TTP36 default 0 for ttp36
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP37')
	alter table mrtesum add constraint DF__MRTESUM__TTP37 default 0 for ttp37
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP38')
	alter table mrtesum add constraint DF__MRTESUM__TTP38 default 0 for ttp38
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP39')
	alter table mrtesum add constraint DF__MRTESUM__TTP39 default 0 for ttp39
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP4')
	alter table mrtesum add constraint DF__MRTESUM__TTP4 default 0 for ttp4
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP40')
	alter table mrtesum add constraint DF__MRTESUM__TTP40 default 0 for ttp40
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP41')
	alter table mrtesum add constraint DF__MRTESUM__TTP41 default 0 for ttp41
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP42')
	alter table mrtesum add constraint DF__MRTESUM__TTP42 default 0 for ttp42
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP43')
	alter table mrtesum add constraint DF__MRTESUM__TTP43 default 0 for ttp43
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP44')
	alter table mrtesum add constraint DF__MRTESUM__TTP44 default 0 for ttp44
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP45')
	alter table mrtesum add constraint DF__MRTESUM__TTP45 default 0 for ttp45
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP46')
	alter table mrtesum add constraint DF__MRTESUM__TTP46 default 0 for ttp46
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP47')
	alter table mrtesum add constraint DF__MRTESUM__TTP47 default 0 for ttp47
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP48')
	alter table mrtesum add constraint DF__MRTESUM__TTP48 default 0 for ttp48
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP5')
	alter table mrtesum add constraint DF__MRTESUM__TTP5 default 0 for ttp5
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP6')
	alter table mrtesum add constraint DF__MRTESUM__TTP6 default 0 for ttp6
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP7')
	alter table mrtesum add constraint DF__MRTESUM__TTP7 default 0 for ttp7
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP8')
	alter table mrtesum add constraint DF__MRTESUM__TTP8 default 0 for ttp8
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__TTP9')
	alter table mrtesum add constraint DF__MRTESUM__TTP9 default 0 for ttp9
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM__UNCL_R')
	alter table mrtesum add constraint DF__MRTESUM__UNCL_R default 0 for uncl_r
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP49')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP49 default 0 for ttp49
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP50')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP50 default 0 for ttp50
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP51')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP51 default 0 for ttp51
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP52')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP52 default 0 for ttp52
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP53')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP53 default 0 for ttp53
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP54')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP54 default 0 for ttp54
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP55')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP55 default 0 for ttp55
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP56')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP56 default 0 for ttp56
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP57')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP57 default 0 for ttp57
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP58')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP58 default 0 for ttp58
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP59')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP59 default 0 for ttp59
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP60')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP60 default 0 for ttp60
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP61')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP61 default 0 for ttp61
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP62')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP62 default 0 for ttp62
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP63')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP63 default 0 for ttp63
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP64')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP64 default 0 for ttp64
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP65')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP65 default 0 for ttp65
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP66')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP66 default 0 for ttp66
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP67')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP67 default 0 for ttp67
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP68')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP68 default 0 for ttp68
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP69')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP69 default 0 for ttp69
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP70')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP70 default 0 for ttp70
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP71')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP71 default 0 for ttp71
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP72')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP72 default 0 for ttp72
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP73')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP73 default 0 for ttp73
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP74')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP74 default 0 for ttp74
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP75')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP75 default 0 for ttp75
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP76')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP76 default 0 for ttp76
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP77')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP77 default 0 for ttp77
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP78')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP78 default 0 for ttp78
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP79')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP79 default 0 for ttp79
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP80')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP80 default 0 for ttp80
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP81')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP81 default 0 for ttp81
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP82')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP82 default 0 for ttp82
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP83')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP83 default 0 for ttp83
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP84')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP84 default 0 for ttp84
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP85')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP85 default 0 for ttp85
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP86')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP86 default 0 for ttp86
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP87')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP87 default 0 for ttp87
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP88')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP88 default 0 for ttp88
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP89')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP89 default 0 for ttp89
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP90')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP90 default 0 for ttp90
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP91')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP91 default 0 for ttp91
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP92')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP92 default 0 for ttp92
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP93')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP93 default 0 for ttp93
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP94')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP94 default 0 for ttp94
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP95')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP95 default 0 for ttp95
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP96')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP96 default 0 for ttp96
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP97')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP97 default 0 for ttp97
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP98')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP98 default 0 for ttp98
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP99')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP99 default 0 for ttp99
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP100')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP100 default 0 for ttp100
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP101')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP101 default 0 for ttp101
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP102')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP102 default 0 for ttp102
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP103')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP103 default 0 for ttp103
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP104')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP104 default 0 for ttp104
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP105')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP105 default 0 for ttp105
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP106')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP106 default 0 for ttp106
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP107')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP107 default 0 for ttp107
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP108')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP108 default 0 for ttp108
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP109')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP109 default 0 for ttp109
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP110')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP110 default 0 for ttp110
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP111')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP111 default 0 for ttp111
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP112')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP112 default 0 for ttp112
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP113')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP113 default 0 for ttp113
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP114')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP114 default 0 for ttp114
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP115')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP115 default 0 for ttp115
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP116')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP116 default 0 for ttp116
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP117')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP117 default 0 for ttp117
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP118')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP118 default 0 for ttp118
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP119')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP119 default 0 for ttp119
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP120')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP120 default 0 for ttp120
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP121')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP121 default 0 for ttp121
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP122')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP122 default 0 for ttp122
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP123')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP123 default 0 for ttp123
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP124')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP124 default 0 for ttp124
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP125')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP125 default 0 for ttp125
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP126')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP126 default 0 for ttp126
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP127')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP127 default 0 for ttp127
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP128')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP128 default 0 for ttp128
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP129')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP129 default 0 for ttp129
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP130')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP130 default 0 for ttp130
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP131')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP131 default 0 for ttp131
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP132')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP132 default 0 for ttp132
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP133')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP133 default 0 for ttp133
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP134')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP134 default 0 for ttp134
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP135')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP135 default 0 for ttp135
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP136')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP136 default 0 for ttp136
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP137')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP137 default 0 for ttp137
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP138')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP138 default 0 for ttp138
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP139')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP139 default 0 for ttp139
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP140')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP140 default 0 for ttp140
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP141')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP141 default 0 for ttp141
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP142')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP142 default 0 for ttp142
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP143')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP143 default 0 for ttp143
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP144')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP144 default 0 for ttp144
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP145')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP145 default 0 for ttp145
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP146')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP146 default 0 for ttp146
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP147')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP147 default 0 for ttp147
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP148')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP148 default 0 for ttp148
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP149')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP149 default 0 for ttp149
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP150')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP150 default 0 for ttp150
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP151')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP151 default 0 for ttp151
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP152')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP152 default 0 for ttp152
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP153')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP153 default 0 for ttp153
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP154')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP154 default 0 for ttp154
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP155')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP155 default 0 for ttp155
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP156')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP156 default 0 for ttp156
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP157')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP157 default 0 for ttp157
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP158')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP158 default 0 for ttp158
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP159')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP159 default 0 for ttp159
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP160')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP160 default 0 for ttp160
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP161')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP161 default 0 for ttp161
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP162')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP162 default 0 for ttp162
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP163')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP163 default 0 for ttp163
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP164')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP164 default 0 for ttp164
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP165')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP165 default 0 for ttp165
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP166')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP166 default 0 for ttp166
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP167')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP167 default 0 for ttp167
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP168')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP168 default 0 for ttp168
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP169')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP169 default 0 for ttp169
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP170')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP170 default 0 for ttp170
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP171')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP171 default 0 for ttp171
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP172')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP172 default 0 for ttp172
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP173')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP173 default 0 for ttp173
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP174')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP174 default 0 for ttp174
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP175')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP175 default 0 for ttp175
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP176')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP176 default 0 for ttp176
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP177')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP177 default 0 for ttp177
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP178')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP178 default 0 for ttp178
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP179')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP179 default 0 for ttp179
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP180')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP180 default 0 for ttp180
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP181')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP181 default 0 for ttp181
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP182')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP182 default 0 for ttp182
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP183')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP183 default 0 for ttp183
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP184')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP184 default 0 for ttp184
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP185')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP185 default 0 for ttp185
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP186')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP186 default 0 for ttp186
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP187')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP187 default 0 for ttp187
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP188')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP188 default 0 for ttp188
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP189')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP189 default 0 for ttp189
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP190')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP190 default 0 for ttp190
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP191')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP191 default 0 for ttp191
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP192')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP192 default 0 for ttp192
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP193')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP193 default 0 for ttp193
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP194')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP194 default 0 for ttp194
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP195')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP195 default 0 for ttp195
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP196')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP196 default 0 for ttp196
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP197')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP197 default 0 for ttp197
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP198')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP198 default 0 for ttp198
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP199')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP199 default 0 for ttp199
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP200')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP200 default 0 for ttp200
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP201')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP201 default 0 for ttp201
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP202')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP202 default 0 for ttp202
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP203')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP203 default 0 for ttp203
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP204')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP204 default 0 for ttp204
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP205')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP205 default 0 for ttp205
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP206')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP206 default 0 for ttp206
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP207')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP207 default 0 for ttp207
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP208')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP208 default 0 for ttp208
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP209')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP209 default 0 for ttp209
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP210')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP210 default 0 for ttp210
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP211')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP211 default 0 for ttp211
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP212')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP212 default 0 for ttp212
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP213')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP213 default 0 for ttp213
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP214')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP214 default 0 for ttp214
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP215')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP215 default 0 for ttp215
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP216')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP216 default 0 for ttp216
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP217')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP217 default 0 for ttp217
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP218')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP218 default 0 for ttp218
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP219')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP219 default 0 for ttp219
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP220')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP220 default 0 for ttp220
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP221')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP221 default 0 for ttp221
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP222')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP222 default 0 for ttp222
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP223')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP223 default 0 for ttp223
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP224')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP224 default 0 for ttp224
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP225')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP225 default 0 for ttp225
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP226')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP226 default 0 for ttp226
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP227')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP227 default 0 for ttp227
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP228')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP228 default 0 for ttp228
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP229')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP229 default 0 for ttp229
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP230')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP230 default 0 for ttp230
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP231')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP231 default 0 for ttp231
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP232')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP232 default 0 for ttp232
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP233')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP233 default 0 for ttp233
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP234')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP234 default 0 for ttp234
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP235')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP235 default 0 for ttp235
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP236')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP236 default 0 for ttp236
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP237')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP237 default 0 for ttp237
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP238')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP238 default 0 for ttp238
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP239')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP239 default 0 for ttp239
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP240')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP240 default 0 for ttp240
go
if not exists(select null from sys.objects where name = 'DF__MRTESUM_EXTD__TTP241')
	alter table mrtesum_extd add constraint DF__MRTESUM_EXTD__TTP241 default 0 for ttp241
go
if not exists(select null from sys.objects where name = 'DF__PRB_LOG__TS')
	alter table prb_log add constraint DF__PRB_LOG__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__PRB_LOG__TYPE')
	alter table prb_log add constraint DF__PRB_LOG__TYPE default 1 for type
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__LOC_N')
	alter table probing_status add constraint DF__PROBING_STATUS__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__RCVD_TRANS')
	alter table probing_status add constraint DF__PROBING_STATUS__RCVD_TRANS default 0 for Rcvd_Trans
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_FBX_CFG')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_FBX_CFG default 0 for Sent_Fbx_Cfg
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__RCVD_MBTK_EVENTS')
	alter table probing_status add constraint DF__PROBING_STATUS__RCVD_MBTK_EVENTS default 0 for Rcvd_MBTK_Events
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_NET_CFG')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_NET_CFG default 0 for Sent_Net_cfg
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_FARESTRUCTURE')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_FARESTRUCTURE default 0 for Sent_FareStructure
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_FUTUREFARESTRUCTURE')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_FUTUREFARESTRUCTURE default 0 for Sent_FutureFareStructure
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_AUTOLOAD_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_AUTOLOAD_LIST default 0 for Sent_Autoload_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_BAD_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_BAD_LIST default 0 for Sent_Bad_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_DRIVER_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_DRIVER_LIST default 0 for Sent_Driver_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_ROUTE_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_ROUTE_LIST default 0 for Sent_Route_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_RUN_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_RUN_LIST default 0 for Sent_Run_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_TRIP_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_TRIP_LIST default 0 for Sent_Trip_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_RANGE_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_RANGE_LIST default 0 for Sent_Range_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_SCD_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_SCD_LIST default 0 for Sent_SCD_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__BANNER')
	alter table probing_status add constraint DF__PROBING_STATUS__BANNER default 0 for Banner
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_MBTK_CFG')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_MBTK_CFG default 0 for Sent_MBTK_cfg
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_MBTK_BASLIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_MBTK_BASLIST default 0 for Sent_MBTK_Baslist
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_ALPHA_ROUTE_LIST')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_ALPHA_ROUTE_LIST default 0 for Sent_Alpha_route_list
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_SSL_CERT')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_SSL_CERT default 0 for Sent_SSL_Cert
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__CLEAR_MEM')
	alter table probing_status add constraint DF__PROBING_STATUS__CLEAR_MEM default 0 for Clear_Mem
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_LOGIC_FW_VERSION')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_LOGIC_FW_VERSION default 0 for Sent_Logic_FW_version
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__SENT_LID_FW_VERSION')
	alter table probing_status add constraint DF__PROBING_STATUS__SENT_LID_FW_VERSION default 0 for Sent_Lid_FW_Version
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__CBX_ID')
	alter table probing_status add constraint DF__PROBING_STATUS__CBX_ID default 0 for Cbx_ID
go
if not exists(select null from sys.objects where name = 'DF__PROBING_STATUS__PRB_N')
	alter table probing_status add constraint DF__PROBING_STATUS__PRB_N default 10 for Prb_n
go
if not exists(select null from sys.objects where name = 'DF__RDR__INCLUDED_F')
	alter table rdr add constraint DF__RDR__INCLUDED_F default 'Y' for included_f
go
if not exists(select null from sys.objects where name = 'DF__RIDERSHIP__ROUTE')
	alter table ridership add constraint DF__RIDERSHIP__ROUTE default ('') for [route]
go
if not exists(select null from sys.objects where name = 'DF__RRTLST__ACTIVE')
	alter table rrtlst add constraint DF__RRTLST__ACTIVE default 1 for active
go
if not exists(select null from sys.objects where name = 'DF__RRTLST__ROUTE')
	alter table rrtlst add constraint DF__RRTLST__ROUTE default 0 for route
go
if not exists(select null from sys.objects where name = 'DF__RRTLST__RUN')
	alter table rrtlst add constraint DF__RRTLST__RUN default 0 for run
go
if not exists(select null from sys.objects where name = 'DF__RRTLST__TRIP')
	alter table rrtlst add constraint DF__RRTLST__TRIP default 0 for trip
go
if not exists(select null from sys.objects where name = 'DF__RRTLST__TS')
	alter table rrtlst add constraint DF__RRTLST__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__RTELST__LOC_N')
	alter table rtelst add constraint DF__RTELST__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__BILL_C')
	alter table rtesum add constraint DF__RTESUM__BILL_C default 0 for bill_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__CURR_R')
	alter table rtesum add constraint DF__RTESUM__CURR_R default 0 for curr_r
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__DUMP_C')
	alter table rtesum add constraint DF__RTESUM__DUMP_C default 0 for dump_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE_C')
	alter table rtesum add constraint DF__RTESUM__FARE_C default 0 for fare_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE1_C')
	alter table rtesum add constraint DF__RTESUM__FARE1_C default 0 for fare1_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE10_C')
	alter table rtesum add constraint DF__RTESUM__FARE10_C default 0 for fare10_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE2_C')
	alter table rtesum add constraint DF__RTESUM__FARE2_C default 0 for fare2_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE3_C')
	alter table rtesum add constraint DF__RTESUM__FARE3_C default 0 for fare3_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE4_C')
	alter table rtesum add constraint DF__RTESUM__FARE4_C default 0 for fare4_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE5_C')
	alter table rtesum add constraint DF__RTESUM__FARE5_C default 0 for fare5_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE6_C')
	alter table rtesum add constraint DF__RTESUM__FARE6_C default 0 for fare6_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE7_C')
	alter table rtesum add constraint DF__RTESUM__FARE7_C default 0 for fare7_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE8_C')
	alter table rtesum add constraint DF__RTESUM__FARE8_C default 0 for fare8_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__FARE9_C')
	alter table rtesum add constraint DF__RTESUM__FARE9_C default 0 for fare9_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY1')
	alter table rtesum add constraint DF__RTESUM__KEY1 default 0 for key1
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY2')
	alter table rtesum add constraint DF__RTESUM__KEY2 default 0 for key2
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY3')
	alter table rtesum add constraint DF__RTESUM__KEY3 default 0 for key3
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY4')
	alter table rtesum add constraint DF__RTESUM__KEY4 default 0 for key4
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY5')
	alter table rtesum add constraint DF__RTESUM__KEY5 default 0 for key5
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY6')
	alter table rtesum add constraint DF__RTESUM__KEY6 default 0 for key6
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY7')
	alter table rtesum add constraint DF__RTESUM__KEY7 default 0 for key7
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY8')
	alter table rtesum add constraint DF__RTESUM__KEY8 default 0 for key8
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEY9')
	alter table rtesum add constraint DF__RTESUM__KEY9 default 0 for key9
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEYA')
	alter table rtesum add constraint DF__RTESUM__KEYA default 0 for keya
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEYAST')
	alter table rtesum add constraint DF__RTESUM__KEYAST default 0 for keyast
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEYB')
	alter table rtesum add constraint DF__RTESUM__KEYB default 0 for keyb
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEYC')
	alter table rtesum add constraint DF__RTESUM__KEYC default 0 for keyc
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__KEYD')
	alter table rtesum add constraint DF__RTESUM__KEYD default 0 for keyd
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__PASS_C')
	alter table rtesum add constraint DF__RTESUM__PASS_C default 0 for pass_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__RDR_C')
	alter table rtesum add constraint DF__RTESUM__RDR_C default 0 for rdr_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TICKET_C')
	alter table rtesum add constraint DF__RTESUM__TICKET_C default 0 for ticket_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TOKEN_C')
	alter table rtesum add constraint DF__RTESUM__TOKEN_C default 0 for token_c
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP1')
	alter table rtesum add constraint DF__RTESUM__TTP1 default 0 for ttp1
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP10')
	alter table rtesum add constraint DF__RTESUM__TTP10 default 0 for ttp10
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP11')
	alter table rtesum add constraint DF__RTESUM__TTP11 default 0 for ttp11
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP12')
	alter table rtesum add constraint DF__RTESUM__TTP12 default 0 for ttp12
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP13')
	alter table rtesum add constraint DF__RTESUM__TTP13 default 0 for ttp13
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP14')
	alter table rtesum add constraint DF__RTESUM__TTP14 default 0 for ttp14
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP15')
	alter table rtesum add constraint DF__RTESUM__TTP15 default 0 for ttp15
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP16')
	alter table rtesum add constraint DF__RTESUM__TTP16 default 0 for ttp16
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP17')
	alter table rtesum add constraint DF__RTESUM__TTP17 default 0 for ttp17
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP18')
	alter table rtesum add constraint DF__RTESUM__TTP18 default 0 for ttp18
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP19')
	alter table rtesum add constraint DF__RTESUM__TTP19 default 0 for ttp19
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP2')
	alter table rtesum add constraint DF__RTESUM__TTP2 default 0 for ttp2
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP20')
	alter table rtesum add constraint DF__RTESUM__TTP20 default 0 for ttp20
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP21')
	alter table rtesum add constraint DF__RTESUM__TTP21 default 0 for ttp21
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP22')
	alter table rtesum add constraint DF__RTESUM__TTP22 default 0 for ttp22
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP23')
	alter table rtesum add constraint DF__RTESUM__TTP23 default 0 for ttp23
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP24')
	alter table rtesum add constraint DF__RTESUM__TTP24 default 0 for ttp24
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP25')
	alter table rtesum add constraint DF__RTESUM__TTP25 default 0 for ttp25
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP26')
	alter table rtesum add constraint DF__RTESUM__TTP26 default 0 for ttp26
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP27')
	alter table rtesum add constraint DF__RTESUM__TTP27 default 0 for ttp27
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP28')
	alter table rtesum add constraint DF__RTESUM__TTP28 default 0 for ttp28
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP29')
	alter table rtesum add constraint DF__RTESUM__TTP29 default 0 for ttp29
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP3')
	alter table rtesum add constraint DF__RTESUM__TTP3 default 0 for ttp3
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP30')
	alter table rtesum add constraint DF__RTESUM__TTP30 default 0 for ttp30
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP31')
	alter table rtesum add constraint DF__RTESUM__TTP31 default 0 for ttp31
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP32')
	alter table rtesum add constraint DF__RTESUM__TTP32 default 0 for ttp32
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP33')
	alter table rtesum add constraint DF__RTESUM__TTP33 default 0 for ttp33
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP34')
	alter table rtesum add constraint DF__RTESUM__TTP34 default 0 for ttp34
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP35')
	alter table rtesum add constraint DF__RTESUM__TTP35 default 0 for ttp35
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP36')
	alter table rtesum add constraint DF__RTESUM__TTP36 default 0 for ttp36
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP37')
	alter table rtesum add constraint DF__RTESUM__TTP37 default 0 for ttp37
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP38')
	alter table rtesum add constraint DF__RTESUM__TTP38 default 0 for ttp38
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP39')
	alter table rtesum add constraint DF__RTESUM__TTP39 default 0 for ttp39
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP4')
	alter table rtesum add constraint DF__RTESUM__TTP4 default 0 for ttp4
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP40')
	alter table rtesum add constraint DF__RTESUM__TTP40 default 0 for ttp40
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP41')
	alter table rtesum add constraint DF__RTESUM__TTP41 default 0 for ttp41
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP42')
	alter table rtesum add constraint DF__RTESUM__TTP42 default 0 for ttp42
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP43')
	alter table rtesum add constraint DF__RTESUM__TTP43 default 0 for ttp43
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP44')
	alter table rtesum add constraint DF__RTESUM__TTP44 default 0 for ttp44
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP45')
	alter table rtesum add constraint DF__RTESUM__TTP45 default 0 for ttp45
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP46')
	alter table rtesum add constraint DF__RTESUM__TTP46 default 0 for ttp46
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP47')
	alter table rtesum add constraint DF__RTESUM__TTP47 default 0 for ttp47
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP48')
	alter table rtesum add constraint DF__RTESUM__TTP48 default 0 for ttp48
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP5')
	alter table rtesum add constraint DF__RTESUM__TTP5 default 0 for ttp5
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP6')
	alter table rtesum add constraint DF__RTESUM__TTP6 default 0 for ttp6
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP7')
	alter table rtesum add constraint DF__RTESUM__TTP7 default 0 for ttp7
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP8')
	alter table rtesum add constraint DF__RTESUM__TTP8 default 0 for ttp8
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__TTP9')
	alter table rtesum add constraint DF__RTESUM__TTP9 default 0 for ttp9
go
if not exists(select null from sys.objects where name = 'DF__RTESUM__UNCL_R')
	alter table rtesum add constraint DF__RTESUM__UNCL_R default 0 for uncl_r
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP49')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP49 default 0 for ttp49
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP50')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP50 default 0 for ttp50
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP51')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP51 default 0 for ttp51
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP52')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP52 default 0 for ttp52
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP53')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP53 default 0 for ttp53
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP54')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP54 default 0 for ttp54
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP55')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP55 default 0 for ttp55
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP56')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP56 default 0 for ttp56
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP57')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP57 default 0 for ttp57
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP58')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP58 default 0 for ttp58
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP59')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP59 default 0 for ttp59
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP60')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP60 default 0 for ttp60
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP61')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP61 default 0 for ttp61
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP62')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP62 default 0 for ttp62
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP63')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP63 default 0 for ttp63
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP64')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP64 default 0 for ttp64
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP65')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP65 default 0 for ttp65
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP66')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP66 default 0 for ttp66
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP67')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP67 default 0 for ttp67
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP68')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP68 default 0 for ttp68
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP69')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP69 default 0 for ttp69
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP70')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP70 default 0 for ttp70
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP71')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP71 default 0 for ttp71
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP72')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP72 default 0 for ttp72
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP73')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP73 default 0 for ttp73
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP74')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP74 default 0 for ttp74
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP75')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP75 default 0 for ttp75
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP76')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP76 default 0 for ttp76
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP77')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP77 default 0 for ttp77
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP78')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP78 default 0 for ttp78
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP79')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP79 default 0 for ttp79
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP80')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP80 default 0 for ttp80
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP81')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP81 default 0 for ttp81
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP82')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP82 default 0 for ttp82
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP83')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP83 default 0 for ttp83
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP84')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP84 default 0 for ttp84
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP85')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP85 default 0 for ttp85
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP86')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP86 default 0 for ttp86
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP87')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP87 default 0 for ttp87
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP88')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP88 default 0 for ttp88
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP89')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP89 default 0 for ttp89
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP90')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP90 default 0 for ttp90
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP91')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP91 default 0 for ttp91
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP92')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP92 default 0 for ttp92
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP93')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP93 default 0 for ttp93
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP94')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP94 default 0 for ttp94
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP95')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP95 default 0 for ttp95
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP96')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP96 default 0 for ttp96
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP97')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP97 default 0 for ttp97
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP98')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP98 default 0 for ttp98
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP99')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP99 default 0 for ttp99
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP100')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP100 default 0 for ttp100
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP101')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP101 default 0 for ttp101
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP102')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP102 default 0 for ttp102
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP103')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP103 default 0 for ttp103
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP104')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP104 default 0 for ttp104
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP105')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP105 default 0 for ttp105
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP106')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP106 default 0 for ttp106
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP107')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP107 default 0 for ttp107
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP108')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP108 default 0 for ttp108
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP109')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP109 default 0 for ttp109
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP110')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP110 default 0 for ttp110
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP111')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP111 default 0 for ttp111
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP112')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP112 default 0 for ttp112
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP113')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP113 default 0 for ttp113
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP114')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP114 default 0 for ttp114
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP115')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP115 default 0 for ttp115
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP116')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP116 default 0 for ttp116
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP117')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP117 default 0 for ttp117
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP118')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP118 default 0 for ttp118
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP119')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP119 default 0 for ttp119
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP120')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP120 default 0 for ttp120
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP121')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP121 default 0 for ttp121
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP122')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP122 default 0 for ttp122
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP123')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP123 default 0 for ttp123
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP124')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP124 default 0 for ttp124
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP125')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP125 default 0 for ttp125
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP126')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP126 default 0 for ttp126
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP127')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP127 default 0 for ttp127
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP128')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP128 default 0 for ttp128
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP129')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP129 default 0 for ttp129
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP130')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP130 default 0 for ttp130
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP131')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP131 default 0 for ttp131
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP132')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP132 default 0 for ttp132
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP133')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP133 default 0 for ttp133
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP134')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP134 default 0 for ttp134
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP135')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP135 default 0 for ttp135
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP136')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP136 default 0 for ttp136
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP137')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP137 default 0 for ttp137
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP138')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP138 default 0 for ttp138
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP139')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP139 default 0 for ttp139
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP140')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP140 default 0 for ttp140
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP141')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP141 default 0 for ttp141
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP142')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP142 default 0 for ttp142
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP143')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP143 default 0 for ttp143
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP144')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP144 default 0 for ttp144
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP145')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP145 default 0 for ttp145
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP146')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP146 default 0 for ttp146
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP147')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP147 default 0 for ttp147
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP148')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP148 default 0 for ttp148
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP149')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP149 default 0 for ttp149
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP150')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP150 default 0 for ttp150
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP151')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP151 default 0 for ttp151
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP152')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP152 default 0 for ttp152
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP153')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP153 default 0 for ttp153
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP154')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP154 default 0 for ttp154
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP155')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP155 default 0 for ttp155
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP156')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP156 default 0 for ttp156
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP157')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP157 default 0 for ttp157
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP158')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP158 default 0 for ttp158
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP159')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP159 default 0 for ttp159
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP160')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP160 default 0 for ttp160
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP161')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP161 default 0 for ttp161
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP162')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP162 default 0 for ttp162
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP163')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP163 default 0 for ttp163
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP164')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP164 default 0 for ttp164
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP165')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP165 default 0 for ttp165
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP166')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP166 default 0 for ttp166
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP167')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP167 default 0 for ttp167
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP168')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP168 default 0 for ttp168
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP169')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP169 default 0 for ttp169
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP170')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP170 default 0 for ttp170
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP171')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP171 default 0 for ttp171
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP172')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP172 default 0 for ttp172
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP173')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP173 default 0 for ttp173
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP174')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP174 default 0 for ttp174
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP175')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP175 default 0 for ttp175
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP176')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP176 default 0 for ttp176
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP177')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP177 default 0 for ttp177
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP178')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP178 default 0 for ttp178
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP179')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP179 default 0 for ttp179
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP180')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP180 default 0 for ttp180
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP181')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP181 default 0 for ttp181
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP182')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP182 default 0 for ttp182
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP183')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP183 default 0 for ttp183
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP184')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP184 default 0 for ttp184
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP185')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP185 default 0 for ttp185
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP186')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP186 default 0 for ttp186
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP187')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP187 default 0 for ttp187
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP188')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP188 default 0 for ttp188
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP189')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP189 default 0 for ttp189
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP190')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP190 default 0 for ttp190
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP191')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP191 default 0 for ttp191
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP192')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP192 default 0 for ttp192
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP193')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP193 default 0 for ttp193
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP194')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP194 default 0 for ttp194
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP195')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP195 default 0 for ttp195
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP196')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP196 default 0 for ttp196
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP197')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP197 default 0 for ttp197
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP198')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP198 default 0 for ttp198
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP199')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP199 default 0 for ttp199
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP200')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP200 default 0 for ttp200
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP201')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP201 default 0 for ttp201
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP202')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP202 default 0 for ttp202
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP203')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP203 default 0 for ttp203
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP204')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP204 default 0 for ttp204
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP205')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP205 default 0 for ttp205
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP206')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP206 default 0 for ttp206
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP207')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP207 default 0 for ttp207
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP208')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP208 default 0 for ttp208
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP209')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP209 default 0 for ttp209
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP210')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP210 default 0 for ttp210
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP211')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP211 default 0 for ttp211
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP212')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP212 default 0 for ttp212
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP213')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP213 default 0 for ttp213
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP214')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP214 default 0 for ttp214
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP215')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP215 default 0 for ttp215
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP216')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP216 default 0 for ttp216
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP217')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP217 default 0 for ttp217
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP218')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP218 default 0 for ttp218
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP219')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP219 default 0 for ttp219
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP220')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP220 default 0 for ttp220
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP221')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP221 default 0 for ttp221
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP222')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP222 default 0 for ttp222
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP223')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP223 default 0 for ttp223
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP224')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP224 default 0 for ttp224
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP225')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP225 default 0 for ttp225
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP226')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP226 default 0 for ttp226
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP227')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP227 default 0 for ttp227
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP228')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP228 default 0 for ttp228
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP229')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP229 default 0 for ttp229
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP230')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP230 default 0 for ttp230
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP231')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP231 default 0 for ttp231
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP232')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP232 default 0 for ttp232
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP233')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP233 default 0 for ttp233
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP234')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP234 default 0 for ttp234
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP235')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP235 default 0 for ttp235
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP236')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP236 default 0 for ttp236
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP237')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP237 default 0 for ttp237
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP238')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP238 default 0 for ttp238
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP239')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP239 default 0 for ttp239
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP240')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP240 default 0 for ttp240
go
if not exists(select null from sys.objects where name = 'DF__RTESUM_EXTD__TTP241')
	alter table rtesum_extd add constraint DF__RTESUM_EXTD__TTP241 default 0 for ttp241
go
if not exists(select null from sys.objects where name = 'DF__RUNLST__LOC_N')
	alter table runlst add constraint DF__RUNLST__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__SCD__DES')
	alter table scd add constraint DF__SCD__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__SCD__GRP')
	alter table scd add constraint DF__SCD__GRP default 4 for grp
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_GRP__ACTIVE')
	alter table security_grp add constraint DF__SECURITY_GRP__ACTIVE default 'Y' for active
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_LOG__LOG_DT')
	alter table security_log add constraint DF__SECURITY_LOG__LOG_DT default getdate() for log_dt
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_SYNC__INTERVAL')
	alter table security_sync add constraint DF__SECURITY_SYNC__INTERVAL default 0 for interval
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_SYNC__STATUS')
	alter table security_sync add constraint DF__SECURITY_SYNC__STATUS default 0 for status
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_SYNC__TIMEOUT')
	alter table security_sync add constraint DF__SECURITY_SYNC__TIMEOUT default 0 for timeout
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_SYNC__TYPE')
	alter table security_sync add constraint DF__SECURITY_SYNC__TYPE default 1 for type
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_USR__ACTIVE')
	alter table security_usr add constraint DF__SECURITY_USR__ACTIVE default 'Y' for active
go
if not exists(select null from sys.objects where name = 'DF__SECURITY_USR__TS')
	alter table security_usr add constraint DF__SECURITY_USR__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__SRD__RESTORED_F')
	alter table srd add constraint DF__SRD__RESTORED_F default 'N' for restored_f
go
if not exists(select null from sys.objects where name = 'DF__SVD__RESTORED_F')
	alter table svd add constraint DF__SVD__RESTORED_F default 'N' for restored_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__ATTR')
	alter table target_farecell add constraint DF__TARGET_FARECELL__ATTR default 0 for attr
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__ENABLED_F')
	alter table target_farecell add constraint DF__TARGET_FARECELL__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__INCLUDED_F')
	alter table target_farecell add constraint DF__TARGET_FARECELL__INCLUDED_F default 'Y' for included_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__T_NDX')
	alter table target_farecell add constraint DF__TARGET_FARECELL__T_NDX default 0 for t_ndx
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__T_ENABLED_F')
	alter table target_farecell add constraint DF__TARGET_FARECELL__T_ENABLED_F default 'N' for t_enabled_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__M_NDX')
	alter table target_farecell add constraint DF__TARGET_FARECELL__M_NDX default 0 for m_ndx
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__M_ENABLED_F')
	alter table target_farecell add constraint DF__TARGET_FARECELL__M_ENABLED_F default 'N' for m_enabled_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__SND1')
	alter table target_farecell add constraint DF__TARGET_FARECELL__SND1 default 15 for snd1
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__SND2')
	alter table target_farecell add constraint DF__TARGET_FARECELL__SND2 default 0 for snd2
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__LED1')
	alter table target_farecell add constraint DF__TARGET_FARECELL__LED1 default 0 for led1
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__LED2')
	alter table target_farecell add constraint DF__TARGET_FARECELL__LED2 default 0 for led2
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARECELL__FLAGS')
	alter table target_farecell add constraint DF__TARGET_FARECELL__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FARESET__ENABLED_F')
	alter table target_fareset add constraint DF__TARGET_FARESET__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__VER')
	alter table target_fsc add constraint DF__TARGET_FSC__VER default 0 for ver
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__CREATED_TS')
	alter table target_fsc add constraint DF__TARGET_FSC__CREATED_TS default getdate() for created_ts
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__LOCKCODE_F')
	alter table target_fsc add constraint DF__TARGET_FSC__LOCKCODE_F default 2 for lockcode_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__STORE_F')
	alter table target_fsc add constraint DF__TARGET_FSC__STORE_F default 'Y' for store_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__FARESET_C')
	alter table target_fsc add constraint DF__TARGET_FSC__FARESET_C default 10 for fareset_c
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__ALT_KEY')
	alter table target_fsc add constraint DF__TARGET_FSC__ALT_KEY default '*' for alt_key
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__PEAK1ON')
	alter table target_fsc add constraint DF__TARGET_FSC__PEAK1ON default '19000101' for peak1on
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__PEAK1OFF')
	alter table target_fsc add constraint DF__TARGET_FSC__PEAK1OFF default '19000101' for peak1off
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__PEAK2ON')
	alter table target_fsc add constraint DF__TARGET_FSC__PEAK2ON default '19000101' for peak2on
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__PEAK2OFF')
	alter table target_fsc add constraint DF__TARGET_FSC__PEAK2OFF default '19000101' for peak2off
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__MODIFIED_TS')
	alter table target_fsc add constraint DF__TARGET_FSC__MODIFIED_TS default getdate() for modified_ts
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__MODIFIED_USERID')
	alter table target_fsc add constraint DF__TARGET_FSC__MODIFIED_USERID default 'gfi' for modified_userid
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__EFFECTIVE_TS')
	alter table target_fsc add constraint DF__TARGET_FSC__EFFECTIVE_TS default getdate() for effective_ts
go
if not exists(select null from sys.objects where name = 'DF__TARGET_FSC__CREATED_USERID')
	alter table target_fsc add constraint DF__TARGET_FSC__CREATED_USERID default 'gfi' for created_userid
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__PEAK_F')
	alter table target_media add constraint DF__TARGET_MEDIA__PEAK_F default 'N' for peak_f -- LG-1336
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__OFFPEAK_F')
	alter table target_media add constraint DF__TARGET_MEDIA__OFFPEAK_F default 'N' for offpeak_f -- LG-1336
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__WKD_F')
	alter table target_media add constraint DF__TARGET_MEDIA__WKD_F default 'N' for wkd_f -- LG-1336
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__SAT_F')
	alter table target_media add constraint DF__TARGET_MEDIA__SAT_F default 'N' for sat_f -- LG-1336
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__SUN_F')
	alter table target_media add constraint DF__TARGET_MEDIA__SUN_F default 'N' for sun_f -- LG-1336
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__HOL_F')
	alter table target_media add constraint DF__TARGET_MEDIA__HOL_F default 'Y' for hol_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__ENABLED_F')
	alter table target_media add constraint DF__TARGET_MEDIA__ENABLED_F default 'Y' for enabled_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA__FRIENDLY_F')
	alter table target_media add constraint DF__TARGET_MEDIA__FRIENDLY_F default 'N' for friendly_f
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA_TRACK2__DATE_FMT')
	alter table target_media_track2 add constraint DF__TARGET_MEDIA_TRACK2__DATE_FMT default 5 for date_fmt
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA_TRACK2__MOD10')
	alter table target_media_track2 add constraint DF__TARGET_MEDIA_TRACK2__MOD10 default 1 for mod10
go
if not exists(select null from sys.objects where name = 'DF__TARGET_MEDIA_TRACK2__TRK')
	alter table target_media_track2 add constraint DF__TARGET_MEDIA_TRACK2__TRK default 0 for trk
go
if not exists(select null from sys.objects where name = 'DF__TARGET_RDR__INCLUDED_F')
	alter table target_rdr add constraint DF__TARGET_RDR__INCLUDED_F default 'Y' for included_f
go
if not exists(select null from sys.objects where name = 'DF__TR__BUS')
	alter table tr add constraint DF__TR__BUS default 0 for bus
go
if not exists(select null from sys.objects where name = 'DF__TR__DIR')
	alter table tr add constraint DF__TR__DIR default ' ' for dir
go
if not exists(select null from sys.objects where name = 'DF__TR__DRV')
	alter table tr add constraint DF__TR__DRV default 0 for drv
go
if not exists(select null from sys.objects where name = 'DF__TR__FS')
	alter table tr add constraint DF__TR__FS default 1 for fs
go
if not exists(select null from sys.objects where name = 'DF__TR__N')
	alter table tr add constraint DF__TR__N default 0 for n
go
if not exists(select null from sys.objects where name = 'DF__TR__ROUTE')
	alter table tr add constraint DF__TR__ROUTE default 0 for route
go
if not exists(select null from sys.objects where name = 'DF__TR__RUN')
	alter table tr add constraint DF__TR__RUN default 0 for run
go
if not exists(select null from sys.objects where name = 'DF__TR__TRIP')
	alter table tr add constraint DF__TR__TRIP default 0 for trip
go
if not exists(select null from sys.objects where name = 'DF__TR__TS')
	alter table tr add constraint DF__TR__TS default getdate() for ts
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__BADVERIFY')
	alter table trim_diag add constraint DF__TRIM_DIAG__BADVERIFY default 0 for badverify
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__BADVERIFY_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__BADVERIFY_T default 0 for badverify_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__COLD')
	alter table trim_diag add constraint DF__TRIM_DIAG__COLD default 0 for cold
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__CYCLE_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__CYCLE_T default 0 for cycle_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__ISSUE')
	alter table trim_diag add constraint DF__TRIM_DIAG__ISSUE default 0 for issue
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__ISSUE_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__ISSUE_T default 0 for issue_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__JAM')
	alter table trim_diag add constraint DF__TRIM_DIAG__JAM default 0 for jam
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__JAM_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__JAM_T default 0 for jam_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__MISREAD')
	alter table trim_diag add constraint DF__TRIM_DIAG__MISREAD default 0 for misread
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__MISREAD_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__MISREAD_T default 0 for misread_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__PARTNUM')
	alter table trim_diag add constraint DF__TRIM_DIAG__PARTNUM default 0 for partnum
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__PRINT')
	alter table trim_diag add constraint DF__TRIM_DIAG__PRINT default 0 for [print]
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__PRINT_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__PRINT_T default 0 for print_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__PWRDOWN')
	alter table trim_diag add constraint DF__TRIM_DIAG__PWRDOWN default 0 for pwrdown
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__READ')
	alter table trim_diag add constraint DF__TRIM_DIAG__READ default 0 for [read]
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__READ_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__READ_T default 0 for read_t
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__RESET')
	alter table trim_diag add constraint DF__TRIM_DIAG__RESET default 0 for reset
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__WARM')
	alter table trim_diag add constraint DF__TRIM_DIAG__WARM default 0 for warm
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__WRITE')
	alter table trim_diag add constraint DF__TRIM_DIAG__WRITE default 0 for write
go
if not exists(select null from sys.objects where name = 'DF__TRIM_DIAG__WRITE_T')
	alter table trim_diag add constraint DF__TRIM_DIAG__WRITE_T default 0 for write_t
go
if not exists(select null from sys.objects where name = 'DF__TRPLST__LOC_N')
	alter table trplst add constraint DF__TRPLST__LOC_N default 1 for loc_n
go
if not exists(select null from sys.objects where name = 'DF__VLT__ABREC')
	alter table vlt add constraint DF__VLT__ABREC default 0 for abrec
go
if not exists(select null from sys.objects where name = 'DF__VLT__BILLOVR')
	alter table vlt add constraint DF__VLT__BILLOVR default 0 for billovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__BINID')
	alter table vlt add constraint DF__VLT__BINID default 0 for binid
go
if not exists(select null from sys.objects where name = 'DF__VLT__BINSN')
	alter table vlt add constraint DF__VLT__BINSN default 0 for binsn
go
if not exists(select null from sys.objects where name = 'DF__VLT__BIREC')
	alter table vlt add constraint DF__VLT__BIREC default 0 for birec
go
if not exists(select null from sys.objects where name = 'DF__VLT__BRREC')
	alter table vlt add constraint DF__VLT__BRREC default 0 for brrec
go
if not exists(select null from sys.objects where name = 'DF__VLT__BUS')
	alter table vlt add constraint DF__VLT__BUS default 0 for bus
go
if not exists(select null from sys.objects where name = 'DF__VLT__BYPALM')
	alter table vlt add constraint DF__VLT__BYPALM default 0 for bypalm
go
if not exists(select null from sys.objects where name = 'DF__VLT__CBXALM')
	alter table vlt add constraint DF__VLT__CBXALM default 0 for cbxalm
go
if not exists(select null from sys.objects where name = 'DF__VLT__CBXID')
	alter table vlt add constraint DF__VLT__CBXID default 0 for cbxid
go
if not exists(select null from sys.objects where name = 'DF__VLT__CBXSN')
	alter table vlt add constraint DF__VLT__CBXSN default 0 for cbxsn
go
if not exists(select null from sys.objects where name = 'DF__VLT__CBXVLT')
	alter table vlt add constraint DF__VLT__CBXVLT default 0 for cbxvlt
go
if not exists(select null from sys.objects where name = 'DF__VLT__COINERROVR')
	alter table vlt add constraint DF__VLT__COINERROVR default 0 for coinerrovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__COINOVR')
	alter table vlt add constraint DF__VLT__COINOVR default 0 for coinovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__COLDOVR')
	alter table vlt add constraint DF__VLT__COLDOVR default 0 for coldovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__DIME')
	alter table vlt add constraint DF__VLT__DIME default 0 for dime
go
if not exists(select null from sys.objects where name = 'DF__VLT__DOORALM')
	alter table vlt add constraint DF__VLT__DOORALM default 0 for dooralm
go
if not exists(select null from sys.objects where name = 'DF__VLT__EVNTOVR')
	alter table vlt add constraint DF__VLT__EVNTOVR default 0 for evntovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__FIVE')
	alter table vlt add constraint DF__VLT__FIVE default 0 for five
go
if not exists(select null from sys.objects where name = 'DF__VLT__FSCHANGE')
	alter table vlt add constraint DF__VLT__FSCHANGE default 0 for fschange
go
if not exists(select null from sys.objects where name = 'DF__VLT__HALF')
	alter table vlt add constraint DF__VLT__HALF default 0 for half
go
if not exists(select null from sys.objects where name = 'DF__VLT__ID')
	alter table vlt add constraint DF__VLT__ID default 0 for id
go
if not exists(select null from sys.objects where name = 'DF__VLT__INALM')
	alter table vlt add constraint DF__VLT__INALM default 0 for inalm
go
if not exists(select null from sys.objects where name = 'DF__VLT__INSERTED')
	alter table vlt add constraint DF__VLT__INSERTED default getdate() for inserted
go
if not exists(select null from sys.objects where name = 'DF__VLT__LOGOFF')
	alter table vlt add constraint DF__VLT__LOGOFF default 0 for logoff
go
if not exists(select null from sys.objects where name = 'DF__VLT__MEMCLR')
	alter table vlt add constraint DF__VLT__MEMCLR default 0 for memclr
go
if not exists(select null from sys.objects where name = 'DF__VLT__MISC_C')
	alter table vlt add constraint DF__VLT__MISC_C default 0 for misc_c
go
if not exists(select null from sys.objects where name = 'DF__VLT__NICKEL')
	alter table vlt add constraint DF__VLT__NICKEL default 0 for nickel
go
if not exists(select null from sys.objects where name = 'DF__VLT__NOBIN')
	alter table vlt add constraint DF__VLT__NOBIN default 0 for nobin
go
if not exists(select null from sys.objects where name = 'DF__VLT__NOTPRB')
	alter table vlt add constraint DF__VLT__NOTPRB default 0 for notprb
go
if not exists(select null from sys.objects where name = 'DF__VLT__NULL_F')
	alter table vlt add constraint DF__VLT__NULL_F default 0 for null_f
go
if not exists(select null from sys.objects where name = 'DF__VLT__ONE')
	alter table vlt add constraint DF__VLT__ONE default 0 for one
go
if not exists(select null from sys.objects where name = 'DF__VLT__OUTALM')
	alter table vlt add constraint DF__VLT__OUTALM default 0 for outalm
go
if not exists(select null from sys.objects where name = 'DF__VLT__PASSERROVR')
	alter table vlt add constraint DF__VLT__PASSERROVR default 0 for passerrovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__PASSOVR')
	alter table vlt add constraint DF__VLT__PASSOVR default 0 for passovr
go
if not exists(select null from sys.objects where name = 'DF__VLT__PDU')
	alter table vlt add constraint DF__VLT__PDU default 0 for pdu
go
if not exists(select null from sys.objects where name = 'DF__VLT__PENNY')
	alter table vlt add constraint DF__VLT__PENNY default 0 for penny
go
if not exists(select null from sys.objects where name = 'DF__VLT__PROBE_F')
	alter table vlt add constraint DF__VLT__PROBE_F default 0 for probe_f
go
if not exists(select null from sys.objects where name = 'DF__VLT__PROBE_N')
	alter table vlt add constraint DF__VLT__PROBE_N default 0 for probe_n
go
if not exists(select null from sys.objects where name = 'DF__VLT__PROBED')
	alter table vlt add constraint DF__VLT__PROBED default getdate() for probed
go
if not exists(select null from sys.objects where name = 'DF__VLT__QUARTER')
	alter table vlt add constraint DF__VLT__QUARTER default 0 for quarter
go
if not exists(select null from sys.objects where name = 'DF__VLT__REMOVE_F')
	alter table vlt add constraint DF__VLT__REMOVE_F default 0 for remove_f
go
if not exists(select null from sys.objects where name = 'DF__VLT__REMOVED')
	alter table vlt add constraint DF__VLT__REMOVED default getdate() for removed
go
if not exists(select null from sys.objects where name = 'DF__VLT__REVDSC')
	alter table vlt add constraint DF__VLT__REVDSC default 0 for revdsc
go
if not exists(select null from sys.objects where name = 'DF__VLT__REVENUE')
	alter table vlt add constraint DF__VLT__REVENUE default 0 for revenue
go
if not exists(select null from sys.objects where name = 'DF__VLT__SBA')
	alter table vlt add constraint DF__VLT__SBA default 0 for sba
go
if not exists(select null from sys.objects where name = 'DF__VLT__STARTUP')
	alter table vlt add constraint DF__VLT__STARTUP default 0 for startup
go
if not exists(select null from sys.objects where name = 'DF__VLT__TEN')
	alter table vlt add constraint DF__VLT__TEN default 0 for ten
go
if not exists(select null from sys.objects where name = 'DF__VLT__TICKET')
	alter table vlt add constraint DF__VLT__TICKET default 0 for ticket
go
if not exists(select null from sys.objects where name = 'DF__VLT__TIMEDSC')
	alter table vlt add constraint DF__VLT__TIMEDSC default 0 for timedsc
go
if not exists(select null from sys.objects where name = 'DF__VLT__TOKEN')
	alter table vlt add constraint DF__VLT__TOKEN default 0 for token
go
if not exists(select null from sys.objects where name = 'DF__VLT__TWENTY')
	alter table vlt add constraint DF__VLT__TWENTY default 0 for twenty
go
if not exists(select null from sys.objects where name = 'DF__VLT__TWO')
	alter table vlt add constraint DF__VLT__TWO default 0 for two
go
if not exists(select null from sys.objects where name = 'DF__VLT__VAULT_F')
	alter table vlt add constraint DF__VLT__VAULT_F default 0 for vault_f
go
if not exists(select null from sys.objects where name = 'DF__VLT__VAULT_N')
	alter table vlt add constraint DF__VLT__VAULT_N default 1 for vault_n
go
if not exists(select null from sys.objects where name = 'DF__VLT__VEREC')
	alter table vlt add constraint DF__VLT__VEREC default 0 for verec
go
if not exists(select null from sys.objects where name = 'DF__VLT__VERERR')
	alter table vlt add constraint DF__VLT__VERERR default 0 for vererr
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B1')
	alter table vnd_cr add constraint DF__VND_CR__B1 default 0 for b1
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B10')
	alter table vnd_cr add constraint DF__VND_CR__B10 default 0 for b10
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B100')
	alter table vnd_cr add constraint DF__VND_CR__B100 default 0 for b100
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B2')
	alter table vnd_cr add constraint DF__VND_CR__B2 default 0 for b2
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B20')
	alter table vnd_cr add constraint DF__VND_CR__B20 default 0 for b20
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B5')
	alter table vnd_cr add constraint DF__VND_CR__B5 default 0 for b5
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__B50')
	alter table vnd_cr add constraint DF__VND_CR__B50 default 0 for b50
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__C1')
	alter table vnd_cr add constraint DF__VND_CR__C1 default 0 for c1
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__C10')
	alter table vnd_cr add constraint DF__VND_CR__C10 default 0 for c10
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__C100')
	alter table vnd_cr add constraint DF__VND_CR__C100 default 0 for c100
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__C25')
	alter table vnd_cr add constraint DF__VND_CR__C25 default 0 for c25
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__C5')
	alter table vnd_cr add constraint DF__VND_CR__C5 default 0 for c5
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__C50')
	alter table vnd_cr add constraint DF__VND_CR__C50 default 0 for c50
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__MOD_TYPE')
	alter table vnd_cr add constraint DF__VND_CR__MOD_TYPE default 0 for mod_type
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__T1')
	alter table vnd_cr add constraint DF__VND_CR__T1 default 0 for t1
go
if not exists(select null from sys.objects where name = 'DF__VND_CR__T2')
	alter table vnd_cr add constraint DF__VND_CR__T2 default 0 for t2
go
if not exists(select null from sys.objects where name = 'DF__VND_DEF__DES')
	alter table vnd_def add constraint DF__VND_DEF__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__VND_DEF__GRP')
	alter table vnd_def add constraint DF__VND_DEF__GRP default 0 for grp
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__AUDIT')
	alter table vnd_ev add constraint DF__VND_EV__AUDIT default 0 for audit
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__CLR_F')
	alter table vnd_ev add constraint DF__VND_EV__CLR_F default 'N' for clr_f
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__FAULT')
	alter table vnd_ev add constraint DF__VND_EV__FAULT default 0 for fault
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__MOD_POS')
	alter table vnd_ev add constraint DF__VND_EV__MOD_POS default 0 for mod_pos
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__MOD_TYPE')
	alter table vnd_ev add constraint DF__VND_EV__MOD_TYPE default 0 for mod_type
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__N')
	alter table vnd_ev add constraint DF__VND_EV__N default 0 for n
go
if not exists(select null from sys.objects where name = 'DF__VND_EV__USERID')
	alter table vnd_ev add constraint DF__VND_EV__USERID default 0 for userid
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__AMT')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__AMT default 0 for amt
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B1')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B1 default 0 for b1
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B10')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B10 default 0 for b10
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B100')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B100 default 0 for b100
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B2')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B2 default 0 for b2
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B20')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B20 default 0 for b20
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B5')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B5 default 0 for b5
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__B50')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__B50 default 0 for b50
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__C10')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__C10 default 0 for c10
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__C100')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__C100 default 0 for c100
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__C25')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__C25 default 0 for c25
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__C5')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__C5 default 0 for c5
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__T1')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__T1 default 0 for t1
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_CASH__T2')
	alter table vnd_evd_cash add constraint DF__VND_EVD_CASH__T2 default 0 for t2
go
if not exists(select null from sys.objects where name = 'DF__VND_EVD_SVC__STOCK_CNT')
	alter table vnd_evd_svc add constraint DF__VND_EVD_SVC__STOCK_CNT default 0 for stock_cnt
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__AC_UNIT_M')
	alter table vnd_mnt add constraint DF__VND_MNT__AC_UNIT_M default 0 for ac_unit_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__ALM_M')
	alter table vnd_mnt add constraint DF__VND_MNT__ALM_M default 0 for alm_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__BTP_M')
	alter table vnd_mnt add constraint DF__VND_MNT__BTP_M default 0 for btp_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__CPU_M')
	alter table vnd_mnt add constraint DF__VND_MNT__CPU_M default 0 for cpu_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__CTP_M')
	alter table vnd_mnt add constraint DF__VND_MNT__CTP_M default 0 for ctp_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__EXT_DAMAGE_M')
	alter table vnd_mnt add constraint DF__VND_MNT__EXT_DAMAGE_M default 0 for ext_damage_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__HPR1_M')
	alter table vnd_mnt add constraint DF__VND_MNT__HPR1_M default 0 for hpr1_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__HPR2_M')
	alter table vnd_mnt add constraint DF__VND_MNT__HPR2_M default 0 for hpr2_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__INSRDR_M')
	alter table vnd_mnt add constraint DF__VND_MNT__INSRDR_M default 0 for insrdr_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__LCD_M')
	alter table vnd_mnt add constraint DF__VND_MNT__LCD_M default 0 for lcd_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__OTHER_M')
	alter table vnd_mnt add constraint DF__VND_MNT__OTHER_M default 0 for other_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__PINPAD_M')
	alter table vnd_mnt add constraint DF__VND_MNT__PINPAD_M default 0 for pinpad_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__PRN_M')
	alter table vnd_mnt add constraint DF__VND_MNT__PRN_M default 0 for prn_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__PUSH_BUTTON_M')
	alter table vnd_mnt add constraint DF__VND_MNT__PUSH_BUTTON_M default 0 for push_button_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__PWR_M')
	alter table vnd_mnt add constraint DF__VND_MNT__PWR_M default 0 for pwr_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__TRIM0_M')
	alter table vnd_mnt add constraint DF__VND_MNT__TRIM0_M default 0 for trim0_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__TRIM1_M')
	alter table vnd_mnt add constraint DF__VND_MNT__TRIM1_M default 0 for trim1_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__TRIM2_M')
	alter table vnd_mnt add constraint DF__VND_MNT__TRIM2_M default 0 for trim2_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__TRIM3_M')
	alter table vnd_mnt add constraint DF__VND_MNT__TRIM3_M default 0 for trim3_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MNT__UPS_M')
	alter table vnd_mnt add constraint DF__VND_MNT__UPS_M default 0 for ups_m
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_INV__MOD_POS')
	alter table vnd_mod_inv add constraint DF__VND_MOD_INV__MOD_POS default 0 for mod_pos
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_INV__MOD_STATUS')
	alter table vnd_mod_inv add constraint DF__VND_MOD_INV__MOD_STATUS default 0 for mod_status
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__BTP_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__BTP_ID default '0' for btp_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__BTP_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__BTP_ID_PREV default '0' for btp_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__CBX_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__CBX_ID default '0' for cbx_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__CBX_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__CBX_ID_PREV default '0' for cbx_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__CTP_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__CTP_ID default '0' for ctp_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__CTP_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__CTP_ID_PREV default '0' for ctp_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__HPR1_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__HPR1_ID default '0' for hpr1_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__HPR1_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__HPR1_ID_PREV default '0' for hpr1_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__HPR2_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__HPR2_ID default '0' for hpr2_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__HPR2_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__HPR2_ID_PREV default '0' for hpr2_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__STKR_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__STKR_ID default '0' for stkr_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__STKR_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__STKR_ID_PREV default '0' for stkr_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM0_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM0_ID default '0' for trim0_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM0_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM0_ID_PREV default '0' for trim0_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM1_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM1_ID default '0' for trim1_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM1_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM1_ID_PREV default '0' for trim1_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM2_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM2_ID default '0' for trim2_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM2_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM2_ID_PREV default '0' for trim2_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM3_ID')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM3_ID default '0' for trim3_id
go
if not exists(select null from sys.objects where name = 'DF__VND_MOD_STATUS__TRIM3_ID_PREV')
	alter table vnd_mod_status add constraint DF__VND_MOD_STATUS__TRIM3_ID_PREV default '0' for trim3_id_prev
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__AID')
	alter table vnd_tr add constraint DF__VND_TR__AID default 0 for aid
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__BILL_AMT')
	alter table vnd_tr add constraint DF__VND_TR__BILL_AMT default 0 for bill_amt
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__CHG_AMT')
	alter table vnd_tr add constraint DF__VND_TR__CHG_AMT default 0 for chg_amt
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__CHG_AMT_ERR')
	alter table vnd_tr add constraint DF__VND_TR__CHG_AMT_ERR default 0 for chg_amt_err
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__COIN_AMT')
	alter table vnd_tr add constraint DF__VND_TR__COIN_AMT default 0 for coin_amt
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__DEDUCTION')
	alter table vnd_tr add constraint DF__VND_TR__DEDUCTION default 0 for deduction
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__DES')
	alter table vnd_tr add constraint DF__VND_TR__DES default 0 for des
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__EXP_OFF')
	alter table vnd_tr add constraint DF__VND_TR__EXP_OFF default 0 for exp_off
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__FLAGS')
	alter table vnd_tr add constraint DF__VND_TR__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__GRP')
	alter table vnd_tr add constraint DF__VND_TR__GRP default 0 for grp
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__MID')
	alter table vnd_tr add constraint DF__VND_TR__MID default 0 for mid
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__MOD_POS')
	alter table vnd_tr add constraint DF__VND_TR__MOD_POS default 0 for mod_pos
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__MOD_TYPE')
	alter table vnd_tr add constraint DF__VND_TR__MOD_TYPE default 0 for mod_type
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__PASS_CATEGORY')
	alter table vnd_tr add constraint DF__VND_TR__PASS_CATEGORY default 0 for pass_category
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__PASS_TYPE')
	alter table vnd_tr add constraint DF__VND_TR__PASS_TYPE default 0 for pass_type
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__PAY_TYPE')
	alter table vnd_tr add constraint DF__VND_TR__PAY_TYPE default 0 for pay_type
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__PENDING')
	alter table vnd_tr add constraint DF__VND_TR__PENDING default 0 for pending
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__PRICE')
	alter table vnd_tr add constraint DF__VND_TR__PRICE default 0 for price
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__REMVAL')
	alter table vnd_tr add constraint DF__VND_TR__REMVAL default 0 for remval
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__SC')
	alter table vnd_tr add constraint DF__VND_TR__SC default 0 for sc
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__SEQ')
	alter table vnd_tr add constraint DF__VND_TR__SEQ default 0 for seq
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__T1_IN')
	alter table vnd_tr add constraint DF__VND_TR__T1_IN default 0 for t1_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__T1_OUT')
	alter table vnd_tr add constraint DF__VND_TR__T1_OUT default 0 for t1_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__T2_IN')
	alter table vnd_tr add constraint DF__VND_TR__T2_IN default 0 for t2_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__T2_OUT')
	alter table vnd_tr add constraint DF__VND_TR__T2_OUT default 0 for t2_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__TPBC')
	alter table vnd_tr add constraint DF__VND_TR__TPBC default 0 for tpbc
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__TR_SEQ')
	alter table vnd_tr add constraint DF__VND_TR__TR_SEQ default 0 for tr_seq
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__UNCL_AMT')
	alter table vnd_tr add constraint DF__VND_TR__UNCL_AMT default 0 for uncl_amt
go
if not exists(select null from sys.objects where name = 'DF__VND_TR__USERID')
	alter table vnd_tr add constraint DF__VND_TR__USERID default 0 for userid
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CARD__TERM_ID')
	alter table vnd_trd_card add constraint DF__VND_TRD_CARD__TERM_ID default 0 for term_id
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B1_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B1_IN default 0 for b1_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B1_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B1_OUT default 0 for b1_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B10_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B10_IN default 0 for b10_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B10_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B10_OUT default 0 for b10_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B100_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B100_IN default 0 for b100_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B100_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B100_OUT default 0 for b100_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B2_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B2_IN default 0 for b2_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B2_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B2_OUT default 0 for b2_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B20_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B20_IN default 0 for b20_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B20_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B20_OUT default 0 for b20_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B5_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B5_IN default 0 for b5_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B5_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B5_OUT default 0 for b5_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B50_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B50_IN default 0 for b50_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__B50_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__B50_OUT default 0 for b50_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C10_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C10_IN default 0 for c10_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C10_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C10_OUT default 0 for c10_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C100_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C100_IN default 0 for c100_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C100_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C100_OUT default 0 for c100_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C25_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C25_IN default 0 for c25_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C25_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C25_OUT default 0 for c25_out
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C5_IN')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C5_IN default 0 for c5_in
go
if not exists(select null from sys.objects where name = 'DF__VND_TRD_CASH__C5_OUT')
	alter table vnd_trd_cash add constraint DF__VND_TRD_CASH__C5_OUT default 0 for c5_out
go
if not exists(select null from sys.objects where name = 'DF__VS__BINID')
	alter table vs add constraint DF__VS__BINID default 0 for binid
go
if not exists(select null from sys.objects where name = 'DF__VS__BINSN')
	alter table vs add constraint DF__VS__BINSN default 0 for binsn
go
if not exists(select null from sys.objects where name = 'DF__VS__BYPASS')
	alter table vs add constraint DF__VS__BYPASS default 0 for bypass
go
if not exists(select null from sys.objects where name = 'DF__VS__CBXALM')
	alter table vs add constraint DF__VS__CBXALM default 0 for cbxalm
go
if not exists(select null from sys.objects where name = 'DF__VS__CBXID')
	alter table vs add constraint DF__VS__CBXID default 0 for cbxid
go
if not exists(select null from sys.objects where name = 'DF__VS__CBXINS')
	alter table vs add constraint DF__VS__CBXINS default getdate() for cbxins
go
if not exists(select null from sys.objects where name = 'DF__VS__CBXREM')
	alter table vs add constraint DF__VS__CBXREM default getdate() for cbxrem
go
if not exists(select null from sys.objects where name = 'DF__VS__CBXSN')
	alter table vs add constraint DF__VS__CBXSN default 0 for cbxsn
go
if not exists(select null from sys.objects where name = 'DF__VS__CBXVLT')
	alter table vs add constraint DF__VS__CBXVLT default 0 for cbxvlt
go
if not exists(select null from sys.objects where name = 'DF__VS__DIME')
	alter table vs add constraint DF__VS__DIME default 0 for dime
go
if not exists(select null from sys.objects where name = 'DF__VS__EMP_ID')
	alter table vs add constraint DF__VS__EMP_ID default 0 for emp_id
go
if not exists(select null from sys.objects where name = 'DF__VS__EMP_LOGOFF')
	alter table vs add constraint DF__VS__EMP_LOGOFF default getdate() for emp_logoff
go
if not exists(select null from sys.objects where name = 'DF__VS__EMP_LOGON')
	alter table vs add constraint DF__VS__EMP_LOGON default getdate() for emp_logon
go
if not exists(select null from sys.objects where name = 'DF__VS__ERROR')
	alter table vs add constraint DF__VS__ERROR default 0 for error
go
if not exists(select null from sys.objects where name = 'DF__VS__FIVE')
	alter table vs add constraint DF__VS__FIVE default 0 for five
go
if not exists(select null from sys.objects where name = 'DF__VS__FLAGS')
	alter table vs add constraint DF__VS__FLAGS default 0 for flags
go
if not exists(select null from sys.objects where name = 'DF__VS__HALF')
	alter table vs add constraint DF__VS__HALF default 0 for half
go
if not exists(select null from sys.objects where name = 'DF__VS__INALM')
	alter table vs add constraint DF__VS__INALM default 0 for inalm
go
if not exists(select null from sys.objects where name = 'DF__VS__INSERTED')
	alter table vs add constraint DF__VS__INSERTED default getdate() for inserted
go
if not exists(select null from sys.objects where name = 'DF__VS__MEMCLR')
	alter table vs add constraint DF__VS__MEMCLR default 0 for memclr
go
if not exists(select null from sys.objects where name = 'DF__VS__MISC_C')
	alter table vs add constraint DF__VS__MISC_C default 0 for misc_c
go
if not exists(select null from sys.objects where name = 'DF__VS__NICKEL')
	alter table vs add constraint DF__VS__NICKEL default 0 for nickel
go
if not exists(select null from sys.objects where name = 'DF__VS__NOTPRB')
	alter table vs add constraint DF__VS__NOTPRB default 0 for notprb
go
if not exists(select null from sys.objects where name = 'DF__VS__ONE')
	alter table vs add constraint DF__VS__ONE default 0 for one
go
if not exists(select null from sys.objects where name = 'DF__VS__OUTALM')
	alter table vs add constraint DF__VS__OUTALM default 0 for outalm
go
if not exists(select null from sys.objects where name = 'DF__VS__PENNY')
	alter table vs add constraint DF__VS__PENNY default 0 for penny
go
if not exists(select null from sys.objects where name = 'DF__VS__QUARTER')
	alter table vs add constraint DF__VS__QUARTER default 0 for quarter
go
if not exists(select null from sys.objects where name = 'DF__VS__REMOVED')
	alter table vs add constraint DF__VS__REMOVED default getdate() for removed
go
if not exists(select null from sys.objects where name = 'DF__VS__REVDSC')
	alter table vs add constraint DF__VS__REVDSC default 0 for revdsc
go
if not exists(select null from sys.objects where name = 'DF__VS__REVENUE')
	alter table vs add constraint DF__VS__REVENUE default 0 for revenue
go
if not exists(select null from sys.objects where name = 'DF__VS__SBA')
	alter table vs add constraint DF__VS__SBA default 0 for sba
go
if not exists(select null from sys.objects where name = 'DF__VS__TEN')
	alter table vs add constraint DF__VS__TEN default 0 for ten
go
if not exists(select null from sys.objects where name = 'DF__VS__TICKET')
	alter table vs add constraint DF__VS__TICKET default 0 for ticket
go
if not exists(select null from sys.objects where name = 'DF__VS__TOKEN')
	alter table vs add constraint DF__VS__TOKEN default 0 for token
go
if not exists(select null from sys.objects where name = 'DF__VS__TWENTY')
	alter table vs add constraint DF__VS__TWENTY default 0 for twenty
go
if not exists(select null from sys.objects where name = 'DF__VS__TWO')
	alter table vs add constraint DF__VS__TWO default 0 for two
go
if not exists(select null from sys.objects where name = 'DF__VS__VAULT_N')
	alter table vs add constraint DF__VS__VAULT_N default 1 for vault_n
go

-- ===
-- END
-- ===
-- =================
-- CHECK CONSTRAINTS
-- 
-- $Author: imikhalyev $
-- $Revision: 1.6 $
-- $Date: 2016-05-03 17:03:00 $
-- =================

set quoted_identifier on
go

set ansi_nulls on
go

set ansi_padding on
go

if not exists(select null from sys.objects where name = 'CK__CNF__FARESETS')
	alter table cnf with nocheck add constraint CK__CNF__FARESETS check ([faresets]<=(10))
go
if not exists(select null from sys.objects where name = 'CK__DS__EXPORTFLAG')
	alter table ds with nocheck add constraint CK__DS__EXPORTFLAG check ([exportflag]=(1) OR [exportflag]=(0))
go
if not exists(select null from sys.objects where name = 'CK__DS__VLT_EXPORTFLAG')
	alter table ds with nocheck add constraint CK__DS__VLT_EXPORTFLAG check ([vlt_exportflag] IS NULL OR ([vlt_exportflag]=(1) OR [vlt_exportflag]=(0)))
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__ENABLED_F')
	alter table farecell with nocheck add constraint CK__FARECELL__ENABLED_F check ([enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__INCLUDED_F')
	alter table farecell with nocheck add constraint CK__FARECELL__INCLUDED_F check ([included_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [included_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__LED1')
	alter table farecell with nocheck add constraint CK__FARECELL__LED1 check ([led1]>=(0) AND [led1]<=(7))
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__LED2')
	alter table farecell with nocheck add constraint CK__FARECELL__LED2 check ([led2]>=(0) AND [led2]<=(7))
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__M_ENABLED_F')
	alter table farecell with nocheck add constraint CK__FARECELL__M_ENABLED_F check ([m_enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [m_enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__SND1')
	alter table farecell with nocheck add constraint CK__FARECELL__SND1 check ([snd1]>=(0) AND [snd1]<=(15))
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__SND2')
	alter table farecell with nocheck add constraint CK__FARECELL__SND2 check ([snd2]>=(0) AND [snd2]<=(15))
go
if not exists(select null from sys.objects where name = 'CK__FARECELL__T_ENABLED_F')
	alter table farecell with nocheck add constraint CK__FARECELL__T_ENABLED_F check ([t_enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [t_enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__FARESET__ENABLED_F')
	alter table fareset with nocheck add constraint CK__FARESET__ENABLED_F check ([enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__FSC__LOCKCODE_F')
	alter table fsc with nocheck add constraint CK__FSC__LOCKCODE_F check ([lockcode_f]=(2) OR [lockcode_f]=(1) OR [lockcode_f]=(0))
go
if not exists(select null from sys.objects where name = 'CK__FSC__STORE_F')
	alter table fsc with nocheck add constraint CK__FSC__STORE_F check ([store_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [store_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__GFI_DB_VER__ID')
	alter table gfi_db_ver with nocheck add constraint CK__GFI_DB_VER__ID check ([id]>=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_AUTOLOAD_FARE__CARD_TYPE')
	alter table gfi_epay_autoload_fare with nocheck add constraint CK__GFI_EPAY_AUTOLOAD_FARE__CARD_TYPE check ([dbo].[gfisf_islistcode]('CARD TYPE2',(0),[card_type])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_AUTOLOAD_PARM__NAME')
	alter table gfi_epay_autoload_parm with nocheck add constraint CK__GFI_EPAY_AUTOLOAD_PARM__NAME check ([name]=upper([name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_AUTOLOAD_TR__RC')
	alter table gfi_epay_autoload_tr with nocheck add constraint CK__GFI_EPAY_AUTOLOAD_TR__RC check ([rc]>=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CARD__CARD_TYPE')
	alter table gfi_epay_card with nocheck add constraint CK__GFI_EPAY_CARD__CARD_TYPE check ([dbo].[gfisf_islistcode]('CARD TYPE2',(0),[card_type])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CARD__USER_PROFILE')
	alter table gfi_epay_card with nocheck add constraint CK__GFI_EPAY_CARD__USER_PROFILE check ([dbo].[gfisf_islistitem]('USER TYPE',[user_profile])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CARD_DTL__PROD_ID')
	alter table gfi_epay_card_dtl with nocheck add constraint CK__GFI_EPAY_CARD_DTL__PROD_ID check ([prod_id]>=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CARD_DTL__PROD_TYPE')
	alter table gfi_epay_card_dtl with nocheck add constraint CK__GFI_EPAY_CARD_DTL__PROD_TYPE check ([dbo].[gfisf_islistcode]('PROD TYPE',(0),[prod_type])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CARD_USR__ROLE')
	alter table gfi_epay_card_usr with nocheck add constraint CK__GFI_EPAY_CARD_USR__ROLE check ([dbo].[gfisf_islistcode]('USER ROLE',(0),[role])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CHANNEL_FARE__CHANNEL')
	alter table gfi_epay_channel_fare with nocheck add constraint CK__GFI_EPAY_CHANNEL_FARE__CHANNEL check ([dbo].[gfisf_islistcode]('CHANNEL',(0),[channel])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CHANNEL_FARE__TYPE')
	alter table gfi_epay_channel_fare with nocheck add constraint CK__GFI_EPAY_CHANNEL_FARE__TYPE check ([type]=(2) OR [type]=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CONTACT__COUNTRY')
	alter table gfi_epay_contact with nocheck add constraint CK__GFI_EPAY_CONTACT__COUNTRY check ([country]=upper([country]) collate SQL_Latin1_General_CP1_CS_AS AND [dbo].[gfisf_islistname]('COUNTRY',(0),[country])=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_CONTACT__STATE')
	alter table gfi_epay_contact with nocheck add constraint CK__GFI_EPAY_CONTACT__STATE check ([state]=upper([state]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_FARE__GRP')
	alter table gfi_epay_fare with nocheck add constraint CK__GFI_EPAY_FARE__GRP check ([dbo].[gfisf_islistcode]('PROD TYPE',(0),[grp])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_FARE__TYPE')
	alter table gfi_epay_fare with nocheck add constraint CK__GFI_EPAY_FARE__TYPE check ([dbo].[gfisf_islistcode]('FARE TYPE',(0),[type])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_GRP_RIGHT__ITEM')
	alter table gfi_epay_grp_right with nocheck add constraint CK__GFI_EPAY_GRP_RIGHT__ITEM check ([dbo].[gfisf_islistname]('EPAY RIGHT',(0),[item])=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_INST__INST_ID')
	alter table gfi_epay_inst with nocheck add constraint CK__GFI_EPAY_INST__INST_ID check ([inst_id]<=(4095))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_INST__TYPE')
	alter table gfi_epay_inst with nocheck add constraint CK__GFI_EPAY_INST__TYPE check ([dbo].[gfisf_islistcode]('INST TYPE',(0),[type])>(0) OR [dbo].[gfisf_islistcode]('INST TYPE',(1),[type])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_INST_USR__ROLE')
	alter table gfi_epay_inst_usr with nocheck add constraint CK__GFI_EPAY_INST_USR__ROLE check ([dbo].[gfisf_islistcode]('USER ROLE',(0),[role])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_LOG__ACTION')
	alter table gfi_epay_log with nocheck add constraint CK__GFI_EPAY_LOG__ACTION check ([action]=upper([action]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_NOTIFY__NOTIFY_TYPE')
	alter table gfi_epay_notify with nocheck add constraint CK__GFI_EPAY_NOTIFY__NOTIFY_TYPE check ([dbo].[gfisf_islistcode]('EPAY NOTIFY TYPE',(0),[notify_type])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_ORDER_CC__PAY_TYPE')
	alter table gfi_epay_order_cc with nocheck add constraint CK__GFI_EPAY_ORDER_CC__PAY_TYPE check ([pay_type] IS NULL OR [dbo].[gfisf_islistname]('FIS PAY TYPE',(0),[pay_type])=(1) OR [dbo].[gfisf_islistname]('EXACT PAY TYPE',(0),[pay_type])=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_ORDER_ITEM_NOTE__REASON_CODE')
	alter table gfi_epay_order_item_note with nocheck add constraint CK__GFI_EPAY_ORDER_ITEM_NOTE__REASON_CODE check ([reason_code] IS NULL OR [dbo].[gfisf_islistcode]('REASON CODE',(0),[reason_code])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_ORDER_SHIP__SHIPPER')
	alter table gfi_epay_order_ship with nocheck add constraint CK__GFI_EPAY_ORDER_SHIP__SHIPPER check ([dbo].[gfisf_islistcode]('SHIPPER',(0),[shipper])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USER_TYPE_FARE__USER_PROFILE')
	alter table gfi_epay_user_type_fare with nocheck add constraint CK__GFI_EPAY_USER_TYPE_FARE__USER_PROFILE check ([dbo].[gfisf_islistitem]('USER TYPE',[user_profile])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR__LOGIN_ID')
	alter table gfi_epay_usr with nocheck add constraint CK__GFI_EPAY_USR__LOGIN_ID check ([login_id]=lower([login_id]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR__PWD_QID1')
	alter table gfi_epay_usr with nocheck add constraint CK__GFI_EPAY_USR__PWD_QID1 check ([pwd_qid1] IS NULL OR [dbo].[gfisf_islistcode]('PWD QUESTION',(0),[pwd_qid1])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR__PWD_QID2')
	alter table gfi_epay_usr with nocheck add constraint CK__GFI_EPAY_USR__PWD_QID2 check ([pwd_qid2] IS NULL OR [dbo].[gfisf_islistcode]('PWD QUESTION',(0),[pwd_qid2])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR__USER_PROFILE')
	alter table gfi_epay_usr with nocheck add constraint CK__GFI_EPAY_USR__USER_PROFILE check ([dbo].[gfisf_islistitem]('USER TYPE',[user_profile])>(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR_BILLING__PAY_TYPE')
	alter table gfi_epay_usr_billing with nocheck add constraint CK__GFI_EPAY_USR_BILLING__PAY_TYPE check ([pay_type] IS NULL OR [pay_type]=upper([pay_type]) collate SQL_Latin1_General_CP1_CS_AS AND ([dbo].[gfisf_islistname]('FIS PAY TYPE',(0),[pay_type])>(0) OR [dbo].[gfisf_islistname]('EXACT PAY TYPE',(0),[pay_type])>(0)))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR_CONTACT__CONTACT_TYPE')
	alter table gfi_epay_usr_contact with nocheck add constraint CK__GFI_EPAY_USR_CONTACT__CONTACT_TYPE check ([dbo].[gfisf_getlistcode]('CONTACT',(0),[contact_type])=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR_PWD__PWD_ID')
	alter table gfi_epay_usr_pwd with nocheck add constraint CK__GFI_EPAY_USR_PWD__PWD_ID check ([pwd_id]>=(0))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EPAY_USR_RIGHT__ITEM')
	alter table gfi_epay_usr_right with nocheck add constraint CK__GFI_EPAY_USR_RIGHT__ITEM check ([dbo].[gfisf_islistname]('EPAY RIGHT',(0),[item])=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EQ__EQ_TYPE')
	alter table gfi_eq with nocheck add constraint CK__GFI_EQ__EQ_TYPE check ([eq_type]=(2) OR [eq_type]=(1))
go
if not exists(select null from sys.objects where name = 'CK__GFI_EQ_COMPONENT__IN_SVC')	-- GD-949
	alter table gfi_eq_component with nocheck add constraint CK__GFI_EQ_COMPONENT__IN_SVC check ([in_svc] in ('Y', 'N', 'U'))
go
if not exists(select null from sys.objects where name = 'CK__GFI_FIS_MAF__CC_EXP_M')
	alter table gfi_fis_maf with nocheck add constraint CK__GFI_FIS_MAF__CC_EXP_M check ([cc_exp_m] IS NULL OR [cc_exp_m]>=(1) AND [cc_exp_m]<=(12))
go
if not exists(select null from sys.objects where name = 'CK__GFI_FIS_MAF__PAY_TYPE')
	alter table gfi_fis_maf with nocheck add constraint CK__GFI_FIS_MAF__PAY_TYPE check ([pay_type]=upper([pay_type]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_FIS_MAF_LOG__FILENAME')
	alter table gfi_fis_maf_log with nocheck add constraint CK__GFI_FIS_MAF_LOG__FILENAME check ([filename]=upper([filename]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_LDR__FILENAME')
	alter table gfi_ldr with nocheck add constraint CK__GFI_LDR__FILENAME check ([filename]=upper([filename]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_LST__TYPE')
	alter table gfi_lst with nocheck add constraint CK__GFI_LST__TYPE check ([type]=upper([type]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__GFI_PV_RECON__TYPE')
	alter table gfi_pv_recon with nocheck add constraint CK__GFI_PV_RECON__TYPE check ([type]>=(1) AND [type]<=(5))
go
if not exists(select null from sys.objects where name = 'CK__GFI_TR_JOURNAL__F_USE_F')
	alter table gfi_tr_journal with nocheck add constraint CK__GFI_TR_JOURNAL__F_USE_F check ([f_use_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [f_use_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__GFI_UPGRADE__NAME')
	alter table gfi_upgrade with nocheck add constraint CK__GFI_UPGRADE__NAME check ([name]=upper([name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__HOLIDAYS__DEFAULT_FS_F')
	alter table holidays with nocheck add constraint CK__HOLIDAYS__DEFAULT_FS_F check ([default_fs_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [default_fs_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__HOLIDAYS__ENABLED_F')
	alter table holidays with nocheck add constraint CK__HOLIDAYS__ENABLED_F check ([enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__ENABLED_F')
	alter table media with nocheck add constraint CK__MEDIA__ENABLED_F check ([enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__FRIENDLY_F')
	alter table media with nocheck add constraint CK__MEDIA__FRIENDLY_F check ([friendly_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [friendly_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__HOL_F')
	alter table media with nocheck add constraint CK__MEDIA__HOL_F check ([hol_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [hol_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__OFFPEAK_F')
	alter table media with nocheck add constraint CK__MEDIA__OFFPEAK_F check ([offpeak_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [offpeak_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__PEAK_F')
	alter table media with nocheck add constraint CK__MEDIA__PEAK_F check ([peak_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [peak_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__SAT_F')
	alter table media with nocheck add constraint CK__MEDIA__SAT_F check ([sat_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [sat_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__SUN_F')
	alter table media with nocheck add constraint CK__MEDIA__SUN_F check ([sun_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [sun_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA__WKD_F')
	alter table media with nocheck add constraint CK__MEDIA__WKD_F check ([wkd_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [wkd_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__MEDIA_TRACK2__DATE_MTH')
	alter table media_track2 with nocheck add constraint CK__MEDIA_TRACK2__DATE_MTH check ([date_mth] IS NULL OR [date_mth]>=(1) AND [date_mth]<=(12))
go
if not exists(select null from sys.objects where name = 'CK__MEDIA_TRACK2__RPT_MASK')
	alter table media_track2 with nocheck add constraint CK__MEDIA_TRACK2__RPT_MASK check ([rpt_mask] IS NULL OR patindex('%[^0-9A-F]%',[rpt_mask] collate SQL_Latin1_General_CP1_CS_AS)=(0))
go
if not exists(select null from sys.objects where name = 'CK__MESSAGES__EDITJUSTIFY')
	alter table Messages with nocheck add constraint CK__MESSAGES__EDITJUSTIFY check ([EditJustify]='L' OR [EditJustify]='R')
go
if not exists(select null from sys.objects where name = 'CK__MRBILL__TYPE')
	alter table mrbill with nocheck add constraint CK__MRBILL__TYPE check ([type]=(2) OR [type]=(1))
go
if not exists(select null from sys.objects where name = 'CK__MRCOIN__TYPE')
	alter table mrcoin with nocheck add constraint CK__MRCOIN__TYPE check ([type]=(2) OR [type]=(1))
go
if not exists(select null from sys.objects where name = 'CK__RDR__INCLUDED_F')
	alter table rdr with nocheck add constraint CK__RDR__INCLUDED_F check ([included_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [included_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_APP__APPLICATION')
	alter table security_app with nocheck add constraint CK__SECURITY_APP__APPLICATION check ([application]=lower([application]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_DEF__APPLICATION')
	alter table security_def with nocheck add constraint CK__SECURITY_DEF__APPLICATION check ([application]=lower([application]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_DICT__PHRASE')
	alter table security_dict with nocheck add constraint CK__SECURITY_DICT__PHRASE check ([phrase]=lower([phrase]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_GRP__ACTIVE')
	alter table security_grp with nocheck add constraint CK__SECURITY_GRP__ACTIVE check ([active] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [active] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_GRP__GROUP_NAME')
	alter table security_grp with nocheck add constraint CK__SECURITY_GRP__GROUP_NAME check ([group_name]=lower([group_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_GRP_USR__GROUP_NAME')
	alter table security_grp_usr with nocheck add constraint CK__SECURITY_GRP_USR__GROUP_NAME check ([group_name]=lower([group_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_GRP_USR__USER_NAME')
	alter table security_grp_usr with nocheck add constraint CK__SECURITY_GRP_USR__USER_NAME check ([user_name]=lower([user_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_HIST__USER_NAME')
	alter table security_hist with nocheck add constraint CK__SECURITY_HIST__USER_NAME check ([user_name]=lower([user_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_LOG__ACTION')
	alter table security_log with nocheck add constraint CK__SECURITY_LOG__ACTION check ([action]=upper([action]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_LOG__APPLICATION')
	alter table security_log with nocheck add constraint CK__SECURITY_LOG__APPLICATION check ([application]=lower([application]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_LOG__USER_NAME')
	alter table security_log with nocheck add constraint CK__SECURITY_LOG__USER_NAME check ([user_name]=lower([user_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_PREF__APPLICATION')
	alter table security_pref with nocheck add constraint CK__SECURITY_PREF__APPLICATION check ([application]=lower([application]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_PREF__TYPE')
	alter table security_pref with nocheck add constraint CK__SECURITY_PREF__TYPE check ([type] IS NULL OR [type]=upper([type]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_SETTING__APPLICATION')
	alter table security_setting with nocheck add constraint CK__SECURITY_SETTING__APPLICATION check ([application]=lower([application]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_SETTING__GROUP_NAME')
	alter table security_setting with nocheck add constraint CK__SECURITY_SETTING__GROUP_NAME check ([group_name]=lower([group_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_SYNC__ITEM')
	alter table security_sync with nocheck add constraint CK__SECURITY_SYNC__ITEM check ([item]=lower([item]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_SYNC__TYPE')
	alter table security_sync with nocheck add constraint CK__SECURITY_SYNC__TYPE check ([type]=(3) OR [type]=(2) OR [type]=(1))
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_USR__ACTIVE')
	alter table security_usr with nocheck add constraint CK__SECURITY_USR__ACTIVE check ([active] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [active] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_USR__USER_NAME')
	alter table security_usr with nocheck add constraint CK__SECURITY_USR__USER_NAME check ([user_name]=lower([user_name]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SECURITY_VER__APPLICATION')
	alter table security_ver with nocheck add constraint CK__SECURITY_VER__APPLICATION check ([application]=lower([application]) collate SQL_Latin1_General_CP1_CS_AS)
go
if not exists(select null from sys.objects where name = 'CK__SRD__F_USE_F')
	alter table srd with nocheck add constraint CK__SRD__F_USE_F check ([f_use_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [f_use_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__SRD__RESTORED_F')
	alter table srd with nocheck add constraint CK__SRD__RESTORED_F check ([restored_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [restored_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__SVD__F_USE_F')
	alter table svd with nocheck add constraint CK__SVD__F_USE_F check ([f_use_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [f_use_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__SVD__RESTORED_F')
	alter table svd with nocheck add constraint CK__SVD__RESTORED_F check ([restored_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [restored_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__CAP_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__CAP_F check ([cap_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [cap_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__HOLD_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__HOLD_F check ([hold_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [hold_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__ODDR_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__ODDR_F check ([oddr_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [oddr_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__ODSR_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__ODSR_F check ([odsr_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [odsr_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__PAYREQ_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__PAYREQ_F check ([payreq_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [payreq_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__SDDR_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__SDDR_F check ([sddr_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [sddr_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__SDSR_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__SDSR_F check ([sdsr_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [sdsr_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__TRANSFERS__UPGRADE_F')
	alter table transfers with nocheck add constraint CK__TRANSFERS__UPGRADE_F check ([upgrade_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [upgrade_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__VND_DEF__ENABLED_F')
	alter table vnd_def with nocheck add constraint CK__VND_DEF__ENABLED_F check ([enabled_f] collate SQL_Latin1_General_CP1_CS_AS='Y' OR [enabled_f] collate SQL_Latin1_General_CP1_CS_AS='N')
go
if not exists(select null from sys.objects where name = 'CK__VND_EV__CLR_F')
	alter table vnd_ev with nocheck add constraint CK__VND_EV__CLR_F check ([clr_f]='Y' OR [clr_f]='N')
go

-- ===
-- END
-- ===
-- ============
-- FOREIGN KEYS
-- 
-- $Author: imikhalyev $
-- $Revision: 1.18 $
-- $Date: 2016-08-11 21:54:12 $
-- ============

set quoted_identifier on
go

set ansi_nulls on
go

set ansi_padding on
go

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__AUT_CNF__AUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[aut_cnf]'))
ALTER TABLE [dbo].[aut_cnf]  WITH NOCHECK ADD  CONSTRAINT [FK__AUT_CNF__AUT] FOREIGN KEY([aut_n])
REFERENCES [dbo].[aut] ([aut_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__AUT_CNF__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aut_cnf]'))
ALTER TABLE [dbo].[aut_cnf]  WITH NOCHECK ADD  CONSTRAINT [FK__AUT_CNF__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__BUSLST__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[buslst]'))
ALTER TABLE [dbo].[buslst]  WITH NOCHECK ADD  CONSTRAINT [FK__BUSLST__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__CCD__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[ccd]'))
ALTER TABLE [dbo].[ccd]  WITH NOCHECK ADD  CONSTRAINT [FK__CCD__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__CCD__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ccd]'))
ALTER TABLE [dbo].[ccd]  WITH NOCHECK ADD  CONSTRAINT [FK__CCD__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__DRVLST__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[drvlst]'))
ALTER TABLE [dbo].[drvlst]  WITH NOCHECK ADD  CONSTRAINT [FK__DRVLST__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__EV__ET]') AND parent_object_id = OBJECT_ID(N'[dbo].[ev]'))
ALTER TABLE [dbo].[ev]  WITH NOCHECK ADD  CONSTRAINT [FK__EV__ET] FOREIGN KEY([type])
REFERENCES [dbo].[et] ([type])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__EV__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[ev]'))
ALTER TABLE [dbo].[ev]  WITH NOCHECK ADD  CONSTRAINT [FK__EV__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FARECELL__ATTR]') AND parent_object_id = OBJECT_ID(N'[dbo].[farecell]'))
ALTER TABLE [dbo].[farecell]  WITH NOCHECK ADD  CONSTRAINT [FK__FARECELL__ATTR] FOREIGN KEY([attr])
REFERENCES [dbo].[attr] ([attr])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FARECELL__FARESET]') AND parent_object_id = OBJECT_ID(N'[dbo].[farecell]'))
ALTER TABLE [dbo].[farecell]  WITH NOCHECK ADD  CONSTRAINT [FK__FARECELL__FARESET] FOREIGN KEY([loc_n], [fs_id], [fareset_n])
REFERENCES [dbo].[fareset] ([loc_n], [fs_id], [fareset_n])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FARECELL__FSC]') AND parent_object_id = OBJECT_ID(N'[dbo].[farecell]'))
ALTER TABLE [dbo].[farecell]  WITH NOCHECK ADD  CONSTRAINT [FK__FARECELL__FSC] FOREIGN KEY([loc_n], [fs_id])
REFERENCES [dbo].[fsc] ([loc_n], [fs_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FARECELL__MEDIA]') AND parent_object_id = OBJECT_ID(N'[dbo].[farecell]'))
ALTER TABLE [dbo].[farecell]  WITH NOCHECK ADD  CONSTRAINT [FK__FARECELL__MEDIA] FOREIGN KEY([loc_n], [fs_id], [m_ndx])
REFERENCES [dbo].[media] ([loc_n], [fs_id], [m_ndx])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FARECELL__TRANSFERS]') AND parent_object_id = OBJECT_ID(N'[dbo].[farecell]'))
ALTER TABLE [dbo].[farecell]  WITH NOCHECK ADD  CONSTRAINT [FK__FARECELL__TRANSFERS] FOREIGN KEY([loc_n], [fs_id], [t_ndx])
REFERENCES [dbo].[transfers] ([loc_n], [fs_id], [t_ndx])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FARESET__FSC]') AND parent_object_id = OBJECT_ID(N'[dbo].[fareset]'))
ALTER TABLE [dbo].[fareset]  WITH NOCHECK ADD  CONSTRAINT [FK__FARESET__FSC] FOREIGN KEY([loc_n], [fs_id])
REFERENCES [dbo].[fsc] ([loc_n], [fs_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__FIRMWARE_INFO__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[firmware_info]'))
ALTER TABLE [dbo].[firmware_info]  WITH NOCHECK ADD  CONSTRAINT [FK__FIRMWARE_INFO__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

-- LG-665
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_DRIVER__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_driver]'))
alter table [dbo].[etl_cloud_gfi_driver]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_DRIVER__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
go

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_FARECODE__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_farecode]'))
ALTER TABLE [dbo].[etl_cloud_gfi_farecode]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_FARECODE__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_FARESTRUCTURE__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_farestructure]'))
ALTER TABLE [dbo].[etl_cloud_gfi_farestructure]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_FARESTRUCTURE__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_HEADER__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_header]'))
ALTER TABLE [dbo].[etl_cloud_gfi_header]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_HEADER__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_TAXRATE__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_taxrate]'))
ALTER TABLE [dbo].[etl_cloud_gfi_taxrate]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_TAXRATE__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_TICKET__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_ticket]'))
ALTER TABLE [dbo].[etl_cloud_gfi_ticket]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_TICKET__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-1336
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_TICKET_VALIDDAY__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_ticket_validday]'))
ALTER TABLE [dbo].[etl_cloud_gfi_ticket_validday]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_TICKET_VALIDDAY__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_TICKETTYPE__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_tickettype]'))
ALTER TABLE [dbo].[etl_cloud_gfi_tickettype]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_TICKETTYPE__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_WALLETTYPE__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_wallettype]'))
ALTER TABLE [dbo].[etl_cloud_gfi_wallettype]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_WALLETTYPE__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- LG-722
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_CLOUD_GFI_ZONE__ETL_PROCESSINSTANCE]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_cloud_gfi_zone]'))
ALTER TABLE [dbo].[etl_cloud_gfi_zone]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_CLOUD_GFI_ZONE__ETL_PROCESSINSTANCE] FOREIGN KEY([ProcessInstanceID])
REFERENCES [dbo].[etl_processinstance] ([ProcessInstanceID])
GO

-- for build 2.6.10.61
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ETL_PROCESSINSTANCE__ETL_PROCESS]') AND parent_object_id = OBJECT_ID(N'[dbo].[etl_processinstance]'))
ALTER TABLE [dbo].[etl_processinstance]  WITH NOCHECK ADD  CONSTRAINT [FK__ETL_PROCESSINSTANCE__ETL_PROCESS] FOREIGN KEY([ProcessID])
REFERENCES [dbo].[etl_process] ([ProcessID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_CARD_DTL__GFI_EPAY_CARD]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_card_dtl]'))
ALTER TABLE [dbo].[gfi_epay_card_dtl]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_CARD_DTL__GFI_EPAY_CARD] FOREIGN KEY([card_id])
REFERENCES [dbo].[gfi_epay_card] ([card_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_CARD_DTL__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_card_dtl]'))
ALTER TABLE [dbo].[gfi_epay_card_dtl]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_CARD_DTL__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_CARD_INV__GFI_EPAY_CARD_INV_LOG]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_card_inv]'))
ALTER TABLE [dbo].[gfi_epay_card_inv]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_CARD_INV__GFI_EPAY_CARD_INV_LOG] FOREIGN KEY([file_id])
REFERENCES [dbo].[gfi_epay_card_inv_log] ([file_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_CARD_USR__GFI_EPAY_CARD]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_card_usr]'))
ALTER TABLE [dbo].[gfi_epay_card_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_CARD_USR__GFI_EPAY_CARD] FOREIGN KEY([card_id])
REFERENCES [dbo].[gfi_epay_card] ([card_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_CARD_USR__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_card_usr]'))
ALTER TABLE [dbo].[gfi_epay_card_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_CARD_USR__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_CHANNEL_FARE__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_channel_fare]'))
ALTER TABLE [dbo].[gfi_epay_channel_fare]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_CHANNEL_FARE__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_GRP_RIGHT__GFI_EPAY_GRP]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_grp_right]'))
ALTER TABLE [dbo].[gfi_epay_grp_right]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_GRP_RIGHT__GFI_EPAY_GRP] FOREIGN KEY([grp])
REFERENCES [dbo].[gfi_epay_grp] ([grp])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_GRP_USR__GFI_EPAY_GRP]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_grp_usr]'))
ALTER TABLE [dbo].[gfi_epay_grp_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_GRP_USR__GFI_EPAY_GRP] FOREIGN KEY([grp])
REFERENCES [dbo].[gfi_epay_grp] ([grp])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_GRP_USR__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_grp_usr]'))
ALTER TABLE [dbo].[gfi_epay_grp_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_GRP_USR__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST__GFI_EPAY_CONTACT]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst]'))
ALTER TABLE [dbo].[gfi_epay_inst]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST__GFI_EPAY_CONTACT] FOREIGN KEY([contact_id])
REFERENCES [dbo].[gfi_epay_contact] ([contact_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_FARE__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_fare]'))
ALTER TABLE [dbo].[gfi_epay_inst_fare]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST_FARE__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_FARE__GFI_EPAY_INST]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_fare]'))
ALTER TABLE [dbo].[gfi_epay_inst_fare]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST_FARE__GFI_EPAY_INST] FOREIGN KEY([inst_id])
REFERENCES [dbo].[gfi_epay_inst] ([inst_id])
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_PROD__GFI_EPAY_CARD_DTL]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_prod]'))
ALTER TABLE [dbo].[gfi_epay_inst_prod] DROP CONSTRAINT [FK__GFI_EPAY_INST_PROD__GFI_EPAY_CARD_DTL]
GO
ALTER TABLE [dbo].[gfi_epay_inst_prod]  WITH NOCHECK ADD CONSTRAINT [FK__GFI_EPAY_INST_PROD__GFI_EPAY_CARD_DTL] FOREIGN KEY([card_id], [prod_id])
REFERENCES [dbo].[gfi_epay_card_dtl] ([card_id], [prod_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_PROD__GFI_EPAY_INST]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_prod]'))
ALTER TABLE [dbo].[gfi_epay_inst_prod]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST_PROD__GFI_EPAY_INST] FOREIGN KEY([inst_id])
REFERENCES [dbo].[gfi_epay_inst] ([inst_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_RANGE__GFI_EPAY_INST]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_range]'))
ALTER TABLE [dbo].[gfi_epay_inst_range]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST_RANGE__GFI_EPAY_INST] FOREIGN KEY([inst_id])
REFERENCES [dbo].[gfi_epay_inst] ([inst_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_USR__GFI_EPAY_INST]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_usr]'))
ALTER TABLE [dbo].[gfi_epay_inst_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST_USR__GFI_EPAY_INST] FOREIGN KEY([inst_id])
REFERENCES [dbo].[gfi_epay_inst] ([inst_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_INST_USR__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_inst_usr]'))
ALTER TABLE [dbo].[gfi_epay_inst_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_INST_USR__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_LOG__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_log]'))
ALTER TABLE [dbo].[gfi_epay_log]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_LOG__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER__GFI_EPAY_INST]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order]'))
ALTER TABLE [dbo].[gfi_epay_order]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER__GFI_EPAY_INST] FOREIGN KEY([inst_id])
REFERENCES [dbo].[gfi_epay_inst] ([inst_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER__GFI_EPAY_INVOICE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order]'))
ALTER TABLE [dbo].[gfi_epay_order]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER__GFI_EPAY_INVOICE] FOREIGN KEY([inv_id])
REFERENCES [dbo].[gfi_epay_invoice] ([inv_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER__GFI_EPAY_USR_BILLING]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order]'))
ALTER TABLE [dbo].[gfi_epay_order]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER__GFI_EPAY_USR_BILLING] FOREIGN KEY([user_id], [contact_id])
REFERENCES [dbo].[gfi_epay_usr_billing] ([user_id], [contact_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_AUTOLOAD__GFI_EPAY_ORDER_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_autoload]'))
ALTER TABLE [dbo].[gfi_epay_order_autoload]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_AUTOLOAD__GFI_EPAY_ORDER_ITEM] FOREIGN KEY([ord_id], [item_n])
REFERENCES [dbo].[gfi_epay_order_item] ([ord_id], [item_n])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_AUTOLOAD_RANGE__GFI_EPAY_ORDER_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_autoload_range]'))
ALTER TABLE [dbo].[gfi_epay_order_autoload_range]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_AUTOLOAD_RANGE__GFI_EPAY_ORDER_ITEM] FOREIGN KEY([ord_id], [item_n])
REFERENCES [dbo].[gfi_epay_order_item] ([ord_id], [item_n])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_CC__GFI_EPAY_ORDER]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_cc]'))
ALTER TABLE [dbo].[gfi_epay_order_cc]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_CC__GFI_EPAY_ORDER] FOREIGN KEY([ord_id])
REFERENCES [dbo].[gfi_epay_order] ([ord_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_ITEM__GFI_EPAY_ORDER]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_item]'))
ALTER TABLE [dbo].[gfi_epay_order_item]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_ITEM__GFI_EPAY_ORDER] FOREIGN KEY([ord_id])
REFERENCES [dbo].[gfi_epay_order] ([ord_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_ITEM__GFI_EPAY_ORDER_SHIP]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_item]'))
ALTER TABLE [dbo].[gfi_epay_order_item]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_ITEM__GFI_EPAY_ORDER_SHIP] FOREIGN KEY([ship_id])
REFERENCES [dbo].[gfi_epay_order_ship] ([ship_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_ITEM_NOTE__GFI_EPAY_ORDER_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_item_note]'))
ALTER TABLE [dbo].[gfi_epay_order_item_note]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_ITEM_NOTE__GFI_EPAY_ORDER_ITEM] FOREIGN KEY([ord_id], [item_n])
REFERENCES [dbo].[gfi_epay_order_item] ([ord_id], [item_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_NEW__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_new]'))
ALTER TABLE [dbo].[gfi_epay_order_new]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_NEW__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_NEW__GFI_EPAY_ORDER_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_new]'))
ALTER TABLE [dbo].[gfi_epay_order_new]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_NEW__GFI_EPAY_ORDER_ITEM] FOREIGN KEY([ord_id], [item_n])
REFERENCES [dbo].[gfi_epay_order_item] ([ord_id], [item_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_PROD__GFI_EPAY_CARD]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_prod]'))
ALTER TABLE [dbo].[gfi_epay_order_prod]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_PROD__GFI_EPAY_CARD] FOREIGN KEY([card_id])
REFERENCES [dbo].[gfi_epay_card] ([card_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_PROD__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_prod]'))
ALTER TABLE [dbo].[gfi_epay_order_prod]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_PROD__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_PROD__GFI_EPAY_ORDER_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_prod]'))
ALTER TABLE [dbo].[gfi_epay_order_prod]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_PROD__GFI_EPAY_ORDER_ITEM] FOREIGN KEY([ord_id], [item_n])
REFERENCES [dbo].[gfi_epay_order_item] ([ord_id], [item_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_RECHARGE__GFI_EPAY_CARD_DTL]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_recharge]'))
ALTER TABLE [dbo].[gfi_epay_order_recharge]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_RECHARGE__GFI_EPAY_CARD_DTL] FOREIGN KEY([card_id], [prod_id])
REFERENCES [dbo].[gfi_epay_card_dtl] ([card_id], [prod_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_RECHARGE__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_recharge]'))
ALTER TABLE [dbo].[gfi_epay_order_recharge]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_RECHARGE__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_ORDER_RECHARGE__GFI_EPAY_ORDER_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_order_recharge]'))
ALTER TABLE [dbo].[gfi_epay_order_recharge]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_ORDER_RECHARGE__GFI_EPAY_ORDER_ITEM] FOREIGN KEY([ord_id], [item_n])
REFERENCES [dbo].[gfi_epay_order_item] ([ord_id], [item_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USER_TYPE_FARE__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_user_type_fare]'))
ALTER TABLE [dbo].[gfi_epay_user_type_fare]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USER_TYPE_FARE__GFI_EPAY_FARE] FOREIGN KEY([fare_id])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR__GFI_EPAY_CONTACT]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr]'))
ALTER TABLE [dbo].[gfi_epay_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR__GFI_EPAY_CONTACT] FOREIGN KEY([contact_id])
REFERENCES [dbo].[gfi_epay_contact] ([contact_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_FARE__GFI_EPAY_FARE]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_fare]'))
ALTER TABLE [dbo].[gfi_epay_fare]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_FARE__GFI_EPAY_FARE] FOREIGN KEY([nextfareid])
REFERENCES [dbo].[gfi_epay_fare] ([fare_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR_BILLING__GFI_EPAY_CONTACT]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr_billing]'))
ALTER TABLE [dbo].[gfi_epay_usr_billing]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR_BILLING__GFI_EPAY_CONTACT] FOREIGN KEY([contact_id])
REFERENCES [dbo].[gfi_epay_contact] ([contact_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR_BILLING__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr_billing]'))
ALTER TABLE [dbo].[gfi_epay_usr_billing]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR_BILLING__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR_CONTACT__GFI_EPAY_CONTACT]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr_contact]'))
ALTER TABLE [dbo].[gfi_epay_usr_contact]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR_CONTACT__GFI_EPAY_CONTACT] FOREIGN KEY([contact_id])
REFERENCES [dbo].[gfi_epay_contact] ([contact_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR_CONTACT__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr_contact]'))
ALTER TABLE [dbo].[gfi_epay_usr_contact]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR_CONTACT__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR_PWD__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr_pwd]'))
ALTER TABLE [dbo].[gfi_epay_usr_pwd]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR_PWD__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_USR_RIGHT__GFI_EPAY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_usr_right]'))
ALTER TABLE [dbo].[gfi_epay_usr_right]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_USR_RIGHT__GFI_EPAY_USR] FOREIGN KEY([user_id])
REFERENCES [dbo].[gfi_epay_usr] ([user_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EQ_STATUS__GFI_EQ]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_eq_status]'))
ALTER TABLE [dbo].[gfi_eq_status]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EQ_STATUS__GFI_EQ] FOREIGN KEY([loc_n], [eq_type], [eq_n])
REFERENCES [dbo].[gfi_eq] ([loc_n], [eq_type], [eq_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EQ_TRD_STATION__VND_TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_eq_trd_station]'))
ALTER TABLE [dbo].[gfi_eq_trd_station]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EQ_TRD_STATION__VND_TR] FOREIGN KEY([tr_id])
REFERENCES [dbo].[vnd_tr] ([tr_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_FIS_MAF__GFI_FIS_MAF_LOG]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_fis_maf]'))
ALTER TABLE [dbo].[gfi_fis_maf]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_FIS_MAF__GFI_FIS_MAF_LOG] FOREIGN KEY([file_id])
REFERENCES [dbo].[gfi_fis_maf_log] ([file_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GS__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[gs]'))
ALTER TABLE [dbo].[gs]  WITH NOCHECK ADD  CONSTRAINT [FK__GS__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__HOLIDAYS__FSC]') AND parent_object_id = OBJECT_ID(N'[dbo].[holidays]'))
ALTER TABLE [dbo].[holidays]  WITH NOCHECK ADD  CONSTRAINT [FK__HOLIDAYS__FSC] FOREIGN KEY([loc_n], [fs_id])
REFERENCES [dbo].[fsc] ([loc_n], [fs_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__LANG_EN_US__LANG_APP]') AND parent_object_id = OBJECT_ID(N'[dbo].[lang_en_us]'))
ALTER TABLE [dbo].[lang_en_us]  WITH NOCHECK ADD  CONSTRAINT [FK__LANG_EN_US__LANG_APP] FOREIGN KEY([app_id])
REFERENCES [dbo].[lang_app] ([app_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__LANG_ES_MX__LANG_EN_US]') AND parent_object_id = OBJECT_ID(N'[dbo].[lang_es_mx]'))
ALTER TABLE [dbo].[lang_es_mx]  WITH NOCHECK ADD  CONSTRAINT [FK__LANG_ES_MX__LANG_EN_US] FOREIGN KEY([app_id], [phrase_id])
REFERENCES [dbo].[lang_en_us] ([app_id], [phrase_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__LANG_FR_CA__LANG_EN_US]') AND parent_object_id = OBJECT_ID(N'[dbo].[lang_fr_ca]'))
ALTER TABLE [dbo].[lang_fr_ca]  WITH NOCHECK ADD  CONSTRAINT [FK__LANG_FR_CA__LANG_EN_US] FOREIGN KEY([app_id], [phrase_id])
REFERENCES [dbo].[lang_en_us] ([app_id], [phrase_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MEDIA__FSC]') AND parent_object_id = OBJECT_ID(N'[dbo].[media]'))
ALTER TABLE [dbo].[media]  WITH NOCHECK ADD  CONSTRAINT [FK__MEDIA__FSC] FOREIGN KEY([loc_n], [fs_id])
REFERENCES [dbo].[fsc] ([loc_n], [fs_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MEDIA_CATEGORY_TRANSACTION_TYPE__MEDIA_CATEGORY]') AND parent_object_id = OBJECT_ID(N'[dbo].[media_category_transaction_type]'))
ALTER TABLE [dbo].[media_category_transaction_type]  WITH NOCHECK ADD  CONSTRAINT [FK__MEDIA_CATEGORY_TRANSACTION_TYPE__MEDIA_CATEGORY] FOREIGN KEY([category])
REFERENCES [dbo].[media_category] ([category])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MEDIA_CATEGORY_TRANSACTION_TYPE__ET]') AND parent_object_id = OBJECT_ID(N'[dbo].[media_category_transaction_type]'))
ALTER TABLE [dbo].[media_category_transaction_type]  WITH NOCHECK ADD  CONSTRAINT [FK__MEDIA_CATEGORY_TRANSACTION_TYPE__ET] FOREIGN KEY([type])
REFERENCES [dbo].[et] ([type])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MESSAGELANGUAGES__LANGUAGES]') AND parent_object_id = OBJECT_ID(N'[dbo].[MessageLanguages]'))
ALTER TABLE [dbo].[MessageLanguages]  WITH NOCHECK ADD  CONSTRAINT [FK__MESSAGELANGUAGES__LANGUAGES] FOREIGN KEY([LanguageID])
REFERENCES [dbo].[Languages] ([LanguageID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MESSAGELANGUAGES__MESSAGES]') AND parent_object_id = OBJECT_ID(N'[dbo].[MessageLanguages]'))
ALTER TABLE [dbo].[MessageLanguages]  WITH NOCHECK ADD  CONSTRAINT [FK__MESSAGELANGUAGES__MESSAGES] FOREIGN KEY([MessageID])
REFERENCES [dbo].[Messages] ([MessageID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__ML__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[ml]'))
ALTER TABLE [dbo].[ml]  WITH NOCHECK ADD  CONSTRAINT [FK__ML__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MRTESUM__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[mrtesum]'))
ALTER TABLE [dbo].[mrtesum]  WITH NOCHECK ADD  CONSTRAINT [FK__MRTESUM__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__PPD__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[ppd]'))
ALTER TABLE [dbo].[ppd]  WITH NOCHECK ADD  CONSTRAINT [FK__PPD__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__PPD__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ppd]'))
ALTER TABLE [dbo].[ppd]  WITH NOCHECK ADD  CONSTRAINT [FK__PPD__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__RRTLST__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[rrtlst]'))
ALTER TABLE [dbo].[rrtlst]  WITH NOCHECK ADD  CONSTRAINT [FK__RRTLST__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__RTELST__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[rtelst]'))
ALTER TABLE [dbo].[rtelst]  WITH NOCHECK ADD  CONSTRAINT [FK__RTELST__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__RTESUM__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[rtesum]'))
ALTER TABLE [dbo].[rtesum]  WITH NOCHECK ADD  CONSTRAINT [FK__RTESUM__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__RUNLST__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[runlst]'))
ALTER TABLE [dbo].[runlst]  WITH NOCHECK ADD  CONSTRAINT [FK__RUNLST__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SCD__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[scd]'))
ALTER TABLE [dbo].[scd]  WITH NOCHECK ADD  CONSTRAINT [FK__SCD__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SCD__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[scd]'))
ALTER TABLE [dbo].[scd]  WITH NOCHECK ADD  CONSTRAINT [FK__SCD__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_DEF__SECURITY_APP]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_def]'))
ALTER TABLE [dbo].[security_def]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_DEF__SECURITY_APP] FOREIGN KEY([application])
REFERENCES [dbo].[security_app] ([application])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_GRP_USR__SECURITY_GRP]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_grp_usr]'))
ALTER TABLE [dbo].[security_grp_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_GRP_USR__SECURITY_GRP] FOREIGN KEY([group_name])
REFERENCES [dbo].[security_grp] ([group_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_GRP_USR__SECURITY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_grp_usr]'))
ALTER TABLE [dbo].[security_grp_usr]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_GRP_USR__SECURITY_USR] FOREIGN KEY([user_name])
REFERENCES [dbo].[security_usr] ([user_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_HIST__SECURITY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_hist]'))
ALTER TABLE [dbo].[security_hist]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_HIST__SECURITY_USR] FOREIGN KEY([user_name])
REFERENCES [dbo].[security_usr] ([user_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_LOG__SECURITY_APP]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_log]'))
ALTER TABLE [dbo].[security_log]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_LOG__SECURITY_APP] FOREIGN KEY([application])
REFERENCES [dbo].[security_app] ([application])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_LOG__SECURITY_USR]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_log]'))
ALTER TABLE [dbo].[security_log]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_LOG__SECURITY_USR] FOREIGN KEY([user_name])
REFERENCES [dbo].[security_usr] ([user_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_PREF__SECURITY_APP]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_pref]'))
ALTER TABLE [dbo].[security_pref]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_PREF__SECURITY_APP] FOREIGN KEY([application])
REFERENCES [dbo].[security_app] ([application])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_SETTING__SECURITY_DEF]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_setting]'))
ALTER TABLE [dbo].[security_setting]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_SETTING__SECURITY_DEF] FOREIGN KEY([application], [item])
REFERENCES [dbo].[security_def] ([application], [item])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_SETTING__SECURITY_GRP]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_setting]'))
ALTER TABLE [dbo].[security_setting]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_SETTING__SECURITY_GRP] FOREIGN KEY([group_name])
REFERENCES [dbo].[security_grp] ([group_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SECURITY_VER__SECURITY_APP]') AND parent_object_id = OBJECT_ID(N'[dbo].[security_ver]'))
ALTER TABLE [dbo].[security_ver]  WITH NOCHECK ADD  CONSTRAINT [FK__SECURITY_VER__SECURITY_APP] FOREIGN KEY([application])
REFERENCES [dbo].[security_app] ([application])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SRD__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[srd]'))
ALTER TABLE [dbo].[srd]  WITH NOCHECK ADD  CONSTRAINT [FK__SRD__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SRD__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[srd]'))
ALTER TABLE [dbo].[srd]  WITH NOCHECK ADD  CONSTRAINT [FK__SRD__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SVD__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[svd]'))
ALTER TABLE [dbo].[svd]  WITH NOCHECK ADD  CONSTRAINT [FK__SVD__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__SVD__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[svd]'))
ALTER TABLE [dbo].[svd]  WITH NOCHECK ADD  CONSTRAINT [FK__SVD__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TR__ET]') AND parent_object_id = OBJECT_ID(N'[dbo].[tr]'))
ALTER TABLE [dbo].[tr]  WITH NOCHECK ADD  CONSTRAINT [FK__TR__ET] FOREIGN KEY([type])
REFERENCES [dbo].[et] ([type])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TR__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[tr]'))
ALTER TABLE [dbo].[tr]  WITH NOCHECK ADD  CONSTRAINT [FK__TR__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRANSFERS__FSC]') AND parent_object_id = OBJECT_ID(N'[dbo].[transfers]'))
ALTER TABLE [dbo].[transfers]  WITH NOCHECK ADD  CONSTRAINT [FK__TRANSFERS__FSC] FOREIGN KEY([loc_n], [fs_id])
REFERENCES [dbo].[fsc] ([loc_n], [fs_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRD__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[trd]'))
ALTER TABLE [dbo].[trd]  WITH NOCHECK ADD  CONSTRAINT [FK__TRD__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRD__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[trd]'))
ALTER TABLE [dbo].[trd]  WITH NOCHECK ADD  CONSTRAINT [FK__TRD__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRIM_DIAG__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[trim_diag]'))
ALTER TABLE [dbo].[trim_diag]  WITH NOCHECK ADD  CONSTRAINT [FK__TRIM_DIAG__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRMISC__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[trmisc]'))
ALTER TABLE [dbo].[trmisc]  WITH NOCHECK ADD  CONSTRAINT [FK__TRMISC__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRMISC__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[trmisc]'))
ALTER TABLE [dbo].[trmisc]  WITH NOCHECK ADD  CONSTRAINT [FK__TRMISC__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TRPLST__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[trplst]'))
ALTER TABLE [dbo].[trplst]  WITH NOCHECK ADD  CONSTRAINT [FK__TRPLST__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VLT__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[vlt]'))
ALTER TABLE [dbo].[vlt]  WITH NOCHECK ADD  CONSTRAINT [FK__VLT__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VND_CR_EV_LNK__VND_EV]') AND parent_object_id = OBJECT_ID(N'[dbo].[vnd_cr_ev_lnk]'))
ALTER TABLE [dbo].[vnd_cr_ev_lnk]  WITH NOCHECK ADD  CONSTRAINT [FK__VND_CR_EV_LNK__VND_EV] FOREIGN KEY([ev_id])
REFERENCES [dbo].[vnd_ev] ([ev_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VND_EVD_CASH__VND_EV]') AND parent_object_id = OBJECT_ID(N'[dbo].[vnd_evd_cash]'))
ALTER TABLE [dbo].[vnd_evd_cash]  WITH NOCHECK ADD  CONSTRAINT [FK__VND_EVD_CASH__VND_EV] FOREIGN KEY([ev_id])
REFERENCES [dbo].[vnd_ev] ([ev_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VND_EVD_SVC__VND_EV]') AND parent_object_id = OBJECT_ID(N'[dbo].[vnd_evd_svc]'))
ALTER TABLE [dbo].[vnd_evd_svc]  WITH NOCHECK ADD  CONSTRAINT [FK__VND_EVD_SVC__VND_EV] FOREIGN KEY([ev_id])
REFERENCES [dbo].[vnd_ev] ([ev_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VND_TR__VND_DEF]') AND parent_object_id = OBJECT_ID(N'[dbo].[vnd_tr]'))
ALTER TABLE [dbo].[vnd_tr]  WITH NOCHECK ADD  CONSTRAINT [FK__VND_TR__VND_DEF] FOREIGN KEY([pass_type], [pass_category])
REFERENCES [dbo].[vnd_def] ([pass_type], [pass_category])
ON UPDATE SET DEFAULT
ON DELETE SET DEFAULT
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VND_TRD_CARD__VND_TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[vnd_trd_card]'))
ALTER TABLE [dbo].[vnd_trd_card]  WITH NOCHECK ADD  CONSTRAINT [FK__VND_TRD_CARD__VND_TR] FOREIGN KEY([tr_id])
REFERENCES [dbo].[vnd_tr] ([tr_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VND_TRD_CASH__VND_TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[vnd_trd_cash]'))
ALTER TABLE [dbo].[vnd_trd_cash]  WITH NOCHECK ADD  CONSTRAINT [FK__VND_TRD_CASH__VND_TR] FOREIGN KEY([tr_id])
REFERENCES [dbo].[vnd_tr] ([tr_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__VS__CNF]') AND parent_object_id = OBJECT_ID(N'[dbo].[vs]'))
ALTER TABLE [dbo].[vs]  WITH NOCHECK ADD  CONSTRAINT [FK__VS__CNF] FOREIGN KEY([loc_n])
REFERENCES [dbo].[cnf] ([loc_n])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_MOBILE_TICKETS__ML]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_mobile_tickets]'))
ALTER TABLE [dbo].[gfi_epay_mobile_tickets]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_MOBILE_TICKETS__ML] FOREIGN KEY([loc_n], [id])
REFERENCES [dbo].[ml] ([loc_n], [id])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__GFI_EPAY_MOBILE_TICKETS__TR]') AND parent_object_id = OBJECT_ID(N'[dbo].[gfi_epay_mobile_tickets]'))
ALTER TABLE [dbo].[gfi_epay_mobile_tickets]  WITH NOCHECK ADD  CONSTRAINT [FK__GFI_EPAY_MOBILE_TICKETS__TR] FOREIGN KEY([loc_n], [id], [tr_seq])
REFERENCES [dbo].[tr] ([loc_n], [id], [tr_seq])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__CONFIGURATIONSDATA__CONFIGURATIONS]') AND parent_object_id = OBJECT_ID(N'[dbo].[configurationsData]'))
ALTER TABLE [dbo].[configurationsData]  WITH NOCHECK ADD  CONSTRAINT [FK__CONFIGURATIONSDATA__CONFIGURATIONS] FOREIGN KEY([configurationID])
REFERENCES [dbo].[configurations] ([id])
GO

-- ===
-- END
-- ===
-- =====
-- VIEWS
-- 
-- $Author: imikhalyev $
-- $Revision: 1.30 $
-- $Date: 2016-11-02 16:20:54 $
-- =====

set quoted_identifier on
go

set ansi_nulls on
go

set ansi_padding on
go


if exists (select * from sys.views where object_id = object_id(N'AllTransData')) drop view AllTransData;
go

create view [dbo].[AllTransData]
as
/*
select top 10 * from AllTransData;

select top 10 * from AllTransData
where tbl_name = 'svd';

select top 10 * from AllTransData
where tbl_name = 'mbl'
order by ts desc;
*/
select
	tr.loc_n,
	tr.id,
	tr.tr_seq,
	tr.ts,
	tr.type,
	tr.bus,
	tr.route,
	tr.run,
	tr.trip,
	tr.dir,
	tr.n,
	tr.longitude,
	tr.latitude,
	tr.stop_name,
	tr.fs,
	tr.drv,
	u.tbl_name,
	u.grp,
	u.des,
	u.ttp,
	u.aid,
	u.mid,
	u.flags,
	u.trk2,
	u.amt,
	et.text,
	u.org_dir,
	media.description,
	media.m_ndx,
	m.tday,
	u.keys,
	u.seq,
	u.card_id,
	u.tpbc,
	u.sc,
	u.remval,
	u.impval,
	u.f_use_f,
	u.restored_f,
	m.fbx_n,
	fsc.description as fsc_desc,
	media.text as media_text,
	media.fs_id
from tr
inner join et
	on	et.type = tr.type
inner join ml as m
	on	m.id = tr.id
	and	m.loc_n = tr.loc_n
inner join cnf
	on	tr.loc_n = cnf.loc_n
left join
(
	select
		'ppd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		p.mid,
		p.flags,
		null as trk2,
		0 as amt,
		null as org_dir,
		null as keys,
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id, 
		tpbc,
		sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f
	from ppd as p
	left join gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
			
	union all
	 
	select
		'svd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		p.mid,
		p.flags,
		null as trk2,
		deduction as amt,
		null as org_dir,
		null as keys,
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id, 
		null as tpbc,
		null as sc,
		remval,
		impval,
		f_use_f,
		restored_f
	from svd as p
	left join gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'srd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		0 as mid,
		p.flags,
		null as trk2,
		0 as amt,
		null as org_dir,
		null as keys, 
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id,
		null as tpbc,
		null as sc,
		remval,
		impval,
		f_use_f,
		restored_f
	from srd as p
	left join gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'scd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		0 as seq,
		p.fs,
		p.ttp,
		dbo.gfisf_epay_aid() as aid,
		0 as mid,
		0 as flags,
		p.trk2,
		0 as amt,
		null as prg_dir,
		null as keys,
		null as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f
	from scd as p
	 
	union all
	 
	select
		'trd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		p.seq,
		p.fs,
		p.ttp,
		p.aid,
		p.mid,
		p.flags,
		null as trk2,
		0 as amt,
		org_dir,
		null as keys,
		case
			when x.card_pid is null then	case
												when p.seq != 0 then cast(p.seq as varchar(25))
												else null
											end
			else x.card_pid
		end as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		impval,
		null as f_use_f,
		restored_f
	from trd as p
	left join gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'ccd' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		0 as seq,
		p.fs,
		p.ttp,
		dbo.gfisf_epay_aid() as aid,
		0 as mid,
		0 as flags,
		p.trk2,
		0 as amt,
		null as org_dir,
		null as keys,
		x.card_pid as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f
	from ccd as p
	left join gfi_epay_card_inv x 
		on	p.card_eid = x.card_eid	
	 
	union all
	 
	select
		'trmisc' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		0 as grp,
		0 as des,
		0 as seq,
		0 as fs,
		p.ttp,
		dbo.gfisf_epay_aid() as aid,
		0 as mid,
		0 as flags,
		null as trk2,
		p.amt,
		null as org_dir,
		keys, 
		cast(p.card_id as varchar(25)) as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f
	from trmisc as p
	 
	union all
	 
	select
		'mbl' as tbl_name,
		p.id,
		p.loc_n,
		p.tr_seq,
		p.grp,
		p.des,
		0 as seq,
		p.fs_id as fs,
		p.ttp,
		p.aid,
		p.mid,
		0 as flags,
		null as trk2,
		p.deduction as amt,
		null as org_dir,
		null as keys, 
		p.barcode as card_id,
		null as tpbc,
		null as sc,
		null as remval,
		null as impval,
		null as f_use_f,
		null as restored_f
	from gfi_epay_mobile_tickets as p	-- GD-2561
) as u	on	tr.id = u.id 
		and tr.loc_n = u.loc_n 
		and tr.tr_seq = u.tr_seq
left join fsc
	on	fsc.loc_n = cnf.loc_n
	and fsc.fs_id = cnf.fs_id
left join media
	on	media.loc_n = cnf.loc_n
	and media.fs_id = cnf.fs_id
	and	(
			media.grp = u.grp and media.des = u.des and u.grp > 0
		or
			media.m_ndx = u.ttp and u.grp = 0
		)
;
go


if exists (select * from sys.views where object_id = object_id(N'etl_vw_gfi_transaction')) drop view etl_vw_gfi_transaction;
go
--DATARUN-487/Sruthi/Aug 22,2017 - Modified to include magnetics
create view etl_vw_gfi_transaction
as
/*
select count(*) from etl_vw_gfi_transaction;
select * from etl_vw_gfi_transaction;
*/
select
	sc.loc_n,
	sc.id,
	sc.tr_seq,
	sc.card_type,
	Identifier = cast(sc.card_eid as varchar(50)), -- LG-1754
	Slot = case when u.grp=0 and u.des=31 then 11 else sc.prod_id end,	-- GENCLD-7663
	DateStart =		case tname 
						when 'ppd'
						then cast(sc.start_ts as datetime)
						else cast(null as datetime)
					end,
	DateExpires =	case tname 
						when 'ppd'
						then cast(sc.exp_ts as datetime)
						else cast(null as datetime)
					end,
	PendingCount = sc.pending,
	DateofUsage = tr.ts,
	tr.type,
	tr.bus,
	tr.route,
	tr.run,
	tr.trip,
	tr.dir,
	tr.n,
	tr.longitude,
	tr.latitude,
	tr.stop_name,
	tr.fs,
	tr.drv,
	u.tname,
	[Group] = u.grp,
	u.des,
	u.ttp,
	u.aid,
	u.mid,
	u.flags,
	u.trk2,
	u.amt,   
	u.ValueOriginal,
	u.ValueRemaining,
	u.ValueUse,
	u.EarnedPoints,
	u.Paygo_type,
    FareboxNumber = right('000000'+ isnull(cast(ml.fbx_n as varchar(6)), ''), 6),
    FareboxType =	case
						when ver_n like '3[0-9][0-9][0-9]' then 'ODYSSEY'
						when ver_n like '4[0-9][0-9][0-9]' then 'ODYSSEY'
						when ver_n like '5[0-9][0-9][0-9]' then 'ODYSSEY PLUS'
						when ver_n like '6[0-9][0-9][0-9]' then 'FASTFARE'
						when ver_n like '7[0-9][0-9][0-9]' then 'FASTFARE-E'
						else 'FASTFARE'  
					end,
	[Status] = 'New'
from gfi_tr_scard as sc
inner join tr
	on	tr.id = sc.id
	and	tr.loc_n = sc.loc_n
	and	tr.tr_seq = sc.tr_seq
left join ml
	on	ml.id = tr.id
	and	ml.loc_n = tr.loc_n
left join	(	select
					tname = 'ppd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					p.mid,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = 0,
					ValueRemaining = 0,
					ValueUse = 0,
					EarnedPoints = 0,
					Paygo_Type = 0
				from ppd as p
				
				union all
				
				select
					tname = 'svd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					p.mid,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = p.remval - p.deduction,
					ValueRemaining = p.remval,
					ValueUse = p.deduction,
					p.EarnedPoints,
					Paygo_Type = 0 
				from svd as p
			
				union all
			
				select
					tname = 'srd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					mid = 0,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = p.remval + 1,
					ValueRemaining = p.remval,
					ValueUse = 1,
					EarnedPoints = 0,
					Paygo_Type = 0 
				from srd as p
				
				union all

				select
					tname = 'trd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					p.mid,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = 0,
					ValueRemaining = 0,
					ValueUse = 0,
					p.EarnedPoints,
					p.Paygo_type
				from trd as p
			) as u	on	tr.id = u.id
					and	tr.loc_n = u.loc_n
					and	tr.tr_seq = u.tr_seq
where	tname is not null
--mobile
union all

select
	mbl.loc_n,
	mbl.id,
	mbl.tr_seq,
	card_type = 0,
	Identifier = mbl.barcode,
	Slot = 0,
	DateStart =	null,
	DateExpires = null,
	PendingCount = 0,
	DateofUsage = tr.ts,
	tr.type,
	tr.bus,
	tr.route,
	tr.run,
	tr.trip,
	tr.dir,
	tr.n,
	tr.longitude,
	tr.latitude,
	tr.stop_name,
	tr.fs,
	tr.drv,
	tname = 'mbl',
	[Group] = mbl.grp,
	mbl.des,
	mbl.ttp,
	mbl.aid,
	mbl.mid,
	flags = 0,
	trk2 = 0,
	amt = 0,   
	ValueOriginal = 0,
	ValueRemaining = 0,
	ValueUse = 0,
	EarnedPoints = 0,
	Paygo_type = 0,
    FareboxNumber = right('000000'+ isnull(cast(ml.fbx_n as varchar(6)), ''), 6),
    FareboxType =	case
						when ver_n like '3[0-9][0-9][0-9]' then 'ODYSSEY'
						when ver_n like '4[0-9][0-9][0-9]' then 'ODYSSEY'
						when ver_n like '5[0-9][0-9][0-9]' then 'ODYSSEY PLUS'
						when ver_n like '6[0-9][0-9][0-9]' then 'FASTFARE'
						when ver_n like '7[0-9][0-9][0-9]' then 'FASTFARE-E'
						else 'FASTFARE'  
					end,
	[Status] = 'New'
from gfi_epay_mobile_tickets as mbl
inner join tr
	on	tr.id = mbl.id
	and	tr.loc_n = mbl.loc_n
	and	tr.tr_seq = mbl.tr_seq
inner join ml
	on	ml.id = tr.id
	and	ml.loc_n = tr.loc_n
where	mbl.validity = 0 -- validity > 0 - errors

--Magnetics
union all

select
	tr.loc_n,
	tr.id,
	tr.tr_seq,
	100 card_type,
	Identifier = cast(u.card_eid as varchar(50)), -- LG-1754
	Slot = 0,	-- GENCLD-7663
	DateStart =		null,
	DateExpires =	null,
	PendingCount = 0,
	DateofUsage = tr.ts,
	tr.type,
	tr.bus,
	tr.route,
	tr.run,
	tr.trip,
	tr.dir,
	tr.n,
	tr.longitude,
	tr.latitude,
	tr.stop_name,
	tr.fs,
	tr.drv,
	u.tname,
	[Group] = u.grp,
	u.des,
	u.ttp,
	u.aid,
	u.mid,
	u.flags,
	u.trk2,
	u.amt,   
	u.ValueOriginal,
	u.ValueRemaining,
	u.ValueUse,
	u.EarnedPoints,
	u.Paygo_type,
    FareboxNumber = right('000000'+ isnull(cast(ml.fbx_n as varchar(6)), ''), 6),
    FareboxType =	case
						when ver_n like '3[0-9][0-9][0-9]' then 'ODYSSEY'
						when ver_n like '4[0-9][0-9][0-9]' then 'ODYSSEY'
						when ver_n like '5[0-9][0-9][0-9]' then 'ODYSSEY PLUS'
						when ver_n like '6[0-9][0-9][0-9]' then 'FASTFARE'
						when ver_n like '7[0-9][0-9][0-9]' then 'FASTFARE-E'
						else 'FASTFARE'  
					end,
	[Status] = 'New'
from   tr
left join ml
	on	ml.id = tr.id
	and	ml.loc_n = tr.loc_n
left join	(	select
					tname = 'ppd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					p.mid,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = 0,
					ValueRemaining = 0,
					ValueUse = 0,
					EarnedPoints = 0,
					Paygo_Type = 0,card_eid 
				from ppd as p where SUBSTRING (reverse(dbo.[DecimalToBinary](flags)),10,1)<>'1'
				
				union all
				
				select
					tname = 'svd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					p.mid,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = p.remval - p.deduction,
					ValueRemaining = p.remval,
					ValueUse = p.deduction,
					p.EarnedPoints,
					Paygo_Type = 0 ,card_eid 
				from svd as p where SUBSTRING (reverse(dbo.[DecimalToBinary](flags)),10,1)<>'1'
			
				union all
			
				select
					tname = 'srd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					mid = 0,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = p.remval + 1,
					ValueRemaining = p.remval,
					ValueUse = 1,
					EarnedPoints = 0,
					Paygo_Type = 0 ,card_eid 
				from srd as p where SUBSTRING (reverse(dbo.[DecimalToBinary](flags)),10,1)<>'1'
				
				union all

				select
					tname = 'trd',
					p.id,
					p.loc_n,
					p.tr_seq,
					p.grp,
					p.des,
					p.seq,
					p.fs,
					p.ttp,
					p.aid,
					p.mid,
					p.flags,
					trk2 = 0,
					amt = 0,
					ValueOriginal = 0,
					ValueRemaining = 0,
					ValueUse = 0,
					p.EarnedPoints,
					p.Paygo_type,card_eid 
				from trd as p where SUBSTRING (reverse(dbo.[DecimalToBinary](flags)),10,1)<>'1'
			) as u	on	tr.id = u.id
					and	tr.loc_n = u.loc_n
					and	tr.tr_seq = u.tr_seq
where	tname is not null


go


if exists (select * from sys.views where object_id = object_id(N'Sales')) drop view Sales;
go

declare @afmDatabaseName varchar(100) = null;
declare @command varchar(4000);

select @afmDatabaseName = ltrim(rtrim(value))
from gfi_lst
where type = 'EPAY PARM'
  and name = 'AFM DB';

if not exists(select null from sys.databases where name=@afmDatabaseName)
begin
	set @afmDatabaseName = null;
end

if isnull(@afmDatabaseName, '') <> ''
begin
	if exists (select null from sys.synonyms where name = N'afm_card_products' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_card_products;'
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_card_products for [' + @afmDatabaseName + '].dbo.card_products;';
	print @command;
	exec (@command);

	if exists (select null from sys.synonyms where name = N'afm_card_types' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_card_types;';
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_card_types for [' + @afmDatabaseName + '].dbo.card_types;';
	print @command;
	exec (@command);

	if exists (select null from sys.synonyms where name = N'afm_sales_trans_detail' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_sales_trans_detail;';
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_sales_trans_detail for [' + @afmDatabaseName + '].dbo.sales_trans_detail;';
	print @command;
	exec (@command);

	if exists (select null from sys.synonyms where name = N'afm_sales_trans_header' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_sales_trans_header;';
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_sales_trans_header for [' + @afmDatabaseName + '].dbo.sales_trans_header;';
	print @command;
	exec (@command);

	if exists (select null from sys.synonyms where name = N'afm_sales_trans_tender' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_sales_trans_tender;';
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_sales_trans_tender for [' + @afmDatabaseName + '].dbo.sales_trans_tender;';
	print @command;
	exec (@command);

	if exists (select * from sys.synonyms where name = N'afm_simple_card_types' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_simple_card_types;';
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_simple_card_types for [' + @afmDatabaseName + '].dbo.simple_card_types;';
	print @command;
	exec (@command);

	if exists (select null from sys.synonyms where name = N'afm_tender_type' and schema_id = schema_id(N'dbo'))
	begin
		set @command = 'drop synonym afm_tender_type;';
		print @command;
		exec (@command);
	end

	set @command = 'create synonym afm_tender_type for [' + @afmDatabaseName + '].dbo.tender_type;';
	print @command;
	exec (@command);
end
go

if  exists (select * from sys.synonyms where name = N'afm_card_products'      and schema_id = schema_id(N'dbo'))
and exists (select * from sys.synonyms where name = N'afm_card_types'         and schema_id = schema_id(N'dbo'))
and exists (select * from sys.synonyms where name = N'afm_sales_trans_detail' and schema_id = schema_id(N'dbo'))
and exists (select * from sys.synonyms where name = N'afm_sales_trans_header' and schema_id = schema_id(N'dbo'))
and exists (select * from sys.synonyms where name = N'afm_sales_trans_tender' and schema_id = schema_id(N'dbo'))
and exists (select * from sys.synonyms where name = N'afm_simple_card_types'  and schema_id = schema_id(N'dbo'))
and exists (select * from sys.synonyms where name = N'afm_tender_type'        and schema_id = schema_id(N'dbo'))
exec dbo.sp_executesql @statement = N'
CREATE view [dbo].[Sales] (
	channel
	,location
	,ord_id
	,item_n
	,product
	,fare_des
	,prod_type
	,payment
	,qty
	,price
	,fee
	,submitted
	,filled
	,placed
	)
as
-- eFare Credit card
-- Last Modified by Marina at 01/20/2015
select 
	''ePay''
	,''''
	,i.ord_id
	,i.item_n
	,case i.TYPE when 1 then ''Recharge''
		when 2 then ''New Card''
		when 3 then ''Add Product''
		when 4 then ''Auto Buy''
		when 7 then ''Negative Recovery''
	end
	,f.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,f.grp)
	--,case when inv_id IS NULL  then ''Credit'' else ''Invoiced'' end
	,''Credit''
	,qty
	,(case when i.type =7 then r.value else f.price end)/100.00 
	,(case when i.type=2 and n.prod_id=1 then i.fee else 0 end)/100.00
	,submit_ts
	,fulfill_ts
	,ord_ts
from gfi_epay_order_item i
inner join gfi_epay_order o on i.ord_id = o.ord_id
inner join gfi_epay_order_cc c on o.ord_id=c.ord_id and isnull(c.rc,0) in (0, 1)
left outer join gfi_epay_order_recharge r on i.ord_id = r.ord_id and i.item_n = r.item_n
left outer join gfi_epay_order_prod p on i.ord_id = p.ord_id and i.item_n = p.item_n
left outer join gfi_epay_order_new n on i.ord_id = n.ord_id and i.item_n = n.item_n
left outer join gfi_epay_fare f on f.fare_id = (case i.type 
    when 2 then n.fare_id
    when 3 then p.fare_id
    else r.fare_id 
    end)
where 1 = 1
and i.type in (1,2,3,4,7)
and o.cancel_ts is null
union all
-- eFare Invoices
select 
	''ePay''
	,''''
	,i.ord_id
	,i.item_n
	,case i.TYPE when 1 then ''Recharge''
		when 2 then ''New Card''
		when 3 then ''Add Product''
		when 4 then ''Auto Buy''
		when 7 then ''Negative Recovery''
	end
	,f.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,f.grp)	
	--,case when inv_id IS NULL  then ''Credit'' else ''Invoiced'' end
	,''Invoiced''
	,qty
	,(case when i.type =7 then r.value else f.price end)/100.00 
	,(case when i.type=2 and n.prod_id=1 then i.fee else 0 end)/100.00
	,submit_ts
	,fulfill_ts
	,ord_ts
from gfi_epay_order_item i
inner join gfi_epay_order o on i.ord_id = o.ord_id
left outer join gfi_epay_order_recharge r on i.ord_id = r.ord_id and i.item_n = r.item_n
left outer join gfi_epay_order_prod p on i.ord_id = p.ord_id and i.item_n = p.item_n
left outer join gfi_epay_order_new n on i.ord_id = n.ord_id and i.item_n = n.item_n
left outer join gfi_epay_fare f on f.fare_id = (case i.type 
    when 2 then n.fare_id
    when 3 then p.fare_id
    else r.fare_id 
    end)
where 1 = 1
and i.type in (1,2,3,4,7)
and o.inv_id is not null
and o.cancel_ts is null
and not exists (
	Select 1
	from gfi_epay_order_cc c 
	where o.ord_id=c.ord_id 
	--and isnull(c.rc,0) in (0, 1) by Marina
	)

union all
-- TVM/PEM data
select
	''TVM/PEM''
	,eq.eq_label ''Equipment''
	,tr_id
	,tr_seq
	,bt.name
	,m.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,m.grp)	
	,at.name ''Payment Type''
	,1
	,t.price ''Unit Price''
	,0
	,ts
	,tday
	,tday
from vnd_tr t
inner join gfi_eq eq on t.eq_n = eq.eq_n
inner join gfi_lst at on t.pay_type = at.code and at.type = ''pay type''
inner join gfi_lst bt on t.type = bt.code and bt.type = ''ev type'' and bt.class = 1
left outer join media m on t.des = m.des and t.grp = m.grp and t.loc_n = m.loc_n and fs_id = 1 and m.enabled_f = ''Y''
where 1 = 1
and t.type IN ( 302, 312, 314, 319, 321 ) and t.flags&64 = 0 -- flags added by Marina
 
UNION ALL 
-- from AFM DB
SELECT distinct
	''POS/RPASS''
	,''PS ''  + Cast(sth.location_id AS VARCHAR(10)) 
   ,std.trans_header_id
   ,std.id
   ,case std.type when ''C'' then ''Issue Card''
		when ''P'' then ''Add Product''
		else std.type --''Other''
	end ''Product''
	,f.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,f.grp)	
	,tt.type ''Payment''
	,std.qty 
	,std.amount 
	,std.fee
	,std.created_at
	,std.created_at
	,std.created_at
FROM afm_sales_trans_header sth 
inner join afm_sales_trans_detail std on sth.id = std.trans_header_id 
LEFT OUTER JOIN afm_sales_trans_tender stt 
on sth.id = stt.sales_trans_header_id --   ON std.trans_header_id = stt.sales_trans_detail_id changed by Marina 
 left outer join afm_card_products cp on std.stock_item_id = cp.stock_item_product_id 
 left outer join afm_card_types ct on cp.stock_item_product_id = ct.stock_item_id
left outer join afm_simple_card_types sct on ct.simple_card_type_id = sct.id
 join gfi..gfi_epay_fare f on cp.gfi_epay_fare_id = f.fare_id
 left outer join afm_tender_type tt on stt.tender_type_id = tt.id
WHERE  1 = 1 
       AND std.stock_item_id > 0
       and 
       exists (SELECT 0  fROM afm_sales_trans_tender stt1
				where stt1.sales_trans_header_id = sth.id
               group by stt1.sales_trans_header_id
               having count(*) =1)   
union all
SELECT distinct
	''POS/RPASS''
   ,''PS ''  + Cast(sth.location_id AS VARCHAR(10)) 
   ,std.trans_header_id
   ,std.id
   ,case std.type when ''C'' then ''Issue Card''
		when ''P'' then ''Add Product''
		else std.type --''Other''
	end ''Product''
	,f.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,f.grp)	
	,''Multiple''	
	,std.qty
	,std.amount
	,std.fee
	,std.created_at
	,std.created_at
	,std.created_at
FROM afm_sales_trans_header sth 
inner join afm_sales_trans_detail std on sth.id = std.trans_header_id 
 left outer join afm_card_products cp on std.stock_item_id = cp.stock_item_product_id 
 left outer join afm_card_types ct on cp.stock_item_product_id = ct.stock_item_id
left outer join afm_simple_card_types sct on ct.simple_card_type_id = sct.id
 join gfi..gfi_epay_fare f on cp.gfi_epay_fare_id = f.fare_id
WHERE  1 = 1 
       AND std.stock_item_id > 0
       and exists (SELECT 0  fROM afm_sales_trans_tender stt1
				where stt1.sales_trans_header_id = sth.id
               group by stt1.sales_trans_header_id
               having count(*) >1)   
';
else
exec dbo.sp_executesql @statement = N'
CREATE view [dbo].[Sales] (
	channel
	,location
	,ord_id
	,item_n
	,product
	,fare_des
	,prod_type
	,payment
	,qty
	,price
	,fee
	,submitted
	,filled
	,placed
	)
as
-- eFare Credit card
-- Last Modified by Marina at 01/20/2015
select 
	''ePay''
	,''''
	,i.ord_id
	,i.item_n
	,case i.TYPE when 1 then ''Recharge''
		when 2 then ''New Card''
		when 3 then ''Add Product''
		when 4 then ''Auto Buy''
		when 7 then ''Negative Recovery''
	end
	,f.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,f.grp)
	--,case when inv_id IS NULL  then ''Credit'' else ''Invoiced'' end
	,''Credit''
	,qty
	,(case when i.type =7 then r.value else f.price end)/100.00 
	,(case when i.type=2 and n.prod_id=1 then i.fee else 0 end)/100.00
	,submit_ts
	,fulfill_ts
	,ord_ts
from gfi_epay_order_item i
inner join gfi_epay_order o on i.ord_id = o.ord_id
inner join gfi_epay_order_cc c on o.ord_id=c.ord_id and isnull(c.rc,0) in (0, 1)
left outer join gfi_epay_order_recharge r on i.ord_id = r.ord_id and i.item_n = r.item_n
left outer join gfi_epay_order_prod p on i.ord_id = p.ord_id and i.item_n = p.item_n
left outer join gfi_epay_order_new n on i.ord_id = n.ord_id and i.item_n = n.item_n
left outer join gfi_epay_fare f on f.fare_id = (case i.type 
    when 2 then n.fare_id
    when 3 then p.fare_id
    else r.fare_id 
    end)
where 1 = 1
and i.type in (1,2,3,4,7)
and o.cancel_ts is null
union all
-- eFare Invoices
select 
	''ePay''
	,''''
	,i.ord_id
	,i.item_n
	,case i.TYPE when 1 then ''Recharge''
		when 2 then ''New Card''
		when 3 then ''Add Product''
		when 4 then ''Auto Buy''
		when 7 then ''Negative Recovery''
	end
	,f.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,f.grp)	
	--,case when inv_id IS NULL  then ''Credit'' else ''Invoiced'' end
	,''Invoiced''
	,qty
	,(case when i.type =7 then r.value else f.price end)/100.00 
	,(case when i.type=2 and n.prod_id=1 then i.fee else 0 end)/100.00
	,submit_ts
	,fulfill_ts
	,ord_ts
from gfi_epay_order_item i
inner join gfi_epay_order o on i.ord_id = o.ord_id
left outer join gfi_epay_order_recharge r on i.ord_id = r.ord_id and i.item_n = r.item_n
left outer join gfi_epay_order_prod p on i.ord_id = p.ord_id and i.item_n = p.item_n
left outer join gfi_epay_order_new n on i.ord_id = n.ord_id and i.item_n = n.item_n
left outer join gfi_epay_fare f on f.fare_id = (case i.type 
    when 2 then n.fare_id
    when 3 then p.fare_id
    else r.fare_id 
    end)
where 1 = 1
and i.type in (1,2,3,4,7)
and o.inv_id is not null
and o.cancel_ts is null
and not exists (
	Select 1
	from gfi_epay_order_cc c 
	where o.ord_id=c.ord_id 
	--and isnull(c.rc,0) in (0, 1) by Marina
	)

union all
-- TVM/PEM data
select
	''TVM/PEM''
	,eq.eq_label ''Equipment''
	,tr_id
	,tr_seq
	,bt.name
	,m.description
	,dbo.gfisf_getlistname(''PROD TYPE'',0,m.grp)	
	,at.name ''Payment Type''
	,1
	,t.price ''Unit Price''
	,0
	,ts
	,tday
	,tday
from vnd_tr t
inner join gfi_eq eq on t.eq_n = eq.eq_n
inner join gfi_lst at on t.pay_type = at.code and at.type = ''pay type''
inner join gfi_lst bt on t.type = bt.code and bt.type = ''ev type'' and bt.class = 1
left outer join media m on t.des = m.des and t.grp = m.grp and t.loc_n = m.loc_n and fs_id = 1 and m.enabled_f = ''Y''
where 1 = 1
and t.type IN ( 302, 312, 314, 319, 321 ) and t.flags&64 = 0 -- flags added by Marina
';
go


if exists (select * from sys.views where object_id = object_id(N'etl_vw_gfi_autoload_tr')) drop view etl_vw_gfi_autoload_tr;
go

create view etl_vw_gfi_autoload_tr
as
/*
LG-655 - Move InActive Autoloads from NM to Cloud

Version History: 12/22/2015 Pavit Anand added union to handle range autoload data
Version History: 01/04/2016 Pavit Anand added case statment for slot. If bonus ride then return slot 11  LG-1440

*/
select 
	tr.tr_id,
	OrderId = al.ord_id,
	OrderItemNumber = al.item_n,
	slot = case when al.Prod_type=0 and al.[des]=31 then 11 else tr.prod_id_card end, 
	TicketId = al.fare_id,
	ActivityTypeDescription =	case al.load_type
									when 1 then 'Add Value'
									when 2 then 'Add Product'
									when 5 then 'CARD_BAD' 
									when 6 then 'CARD_GOOD'
									when 7 then 'PROD_BAD'
									when 8 then 'PROD_GOOD'
									else 'Unknown'
								end,
	Identifier = al.Card_eid,
	[Group] = al.Prod_type,
	al.[des],
	[status] = tr.rc,
	ValueActivity =	case al.Prod_type
						when 3 then al.value / 100.0
						else al.value
					end,
	RemValue =	case al.Prod_type
					when 3 then tr.value / 100.0
					else tr.value
				end,
	tr.ts,
	tr.mid,
	tr.eq_type,
	EquipmentNumber = tr.eq_n,
	LoadSequence = tr.load_seq,
	LoadSequenceCard = tr.load_seq_card,
	PendingCount = tr.pending,
	ETL_Status =	case tr.rc
						when 1 then 'Ready'
						else 'Error ' + ltrim(str(tr.rc))
					end
from gfi_epay_autoload_tr as tr
inner join gfi_epay_autoload_pkg as p
	on	tr.pkg_id = p.pkg_id
inner join gfi_epay_autoload as al
	on	al.card_eid = tr.card_eid
	and	al.id = p.auto_id
	and	al.load_seq = tr.load_seq 
	where range = 0
	
Union

select 	tr.tr_id,
	OrderId = al.ord_id,
	OrderItemNumber = al.item_n,
	slot = case when al.Prod_type=0 and al.[des]=31 then 11 else tr.prod_id_card end,
	TicketId = al.fare_id,
	ActivityTypeDescription =	case al.load_type
									when 1 then 'ADD_VALUE_R'
									when 2 then 'ADD_PRODUCT_R'
									when 5 then 'CARD_BAD' 
									when 6 then 'CARD_GOOD'
									when 7 then 'PROD_BAD'
									when 8 then 'PROD_GOOD'
									else 'Unknown'
								end,
	Identifier = tr.Card_eid,
	[Group] = al.Prod_type,
	al.[des],
	[status] = tr.rc,
	ValueActivity =	case al.Prod_type
						when 3 then al.value / 100.0
						else al.value
					end,
	RemValue =	case al.Prod_type
					when 3 then tr.value / 100.0
					else tr.value
				end,
	tr.ts,
	tr.mid,
	tr.eq_type,
	EquipmentNumber = tr.eq_n,
	LoadSequence = tr.load_seq,
	LoadSequenceCard = tr.load_seq_card,
	PendingCount = tr.pending,
	ETL_Status =	case tr.rc
						when 1 then 'Range Ready'
						else 'Range Error ' + ltrim(str(tr.rc))
					end
from gfi_epay_autoload_tr as tr
inner join gfi_epay_autoload_pkg as p
	on	tr.pkg_id = p.pkg_id -- 

inner join gfi_epay_autoload_range as al
	on	 al.id = p.auto_id_range
	and	al.load_seq = tr.load_seq     
where range = 1	 
go

if exists (select * from sys.views where object_id = object_id(N'etl_vw_files_dependancy')) drop view etl_vw_files_dependancy;
go

create view etl_vw_files_dependancy
as
/*
select * from etl_vw_files_dependancy
order by
	dstFileID,
	srcFileID
;
select
	ID = row_number() over (order by dstFileID, srcFileID),
	[FileName] = srcName,
	[FolderPathSource] = srcFolder,
	[FolderPathDestination] = dstFolder,
	[FolderPathArchive] = srcBackupFolder
from etl_vw_files_dependancy
where dstProcessName is null
order by
	ID
;
*/
with cte_ft (
	[dstFileID],
	[srcFileID]
)
as
(
select
	[dstFileID] = f.[ID],
	[srcFileID] = f.sourceFileID
from etl_files as f
where	f.isActive = 1
	and f.sourceFileID is null

union all

select
	[dstFileID] = f.[ID],
	[srcFileID] = f.sourceFileID
from etl_files as f
inner join cte_ft as ft
	on	ft.dstFileID = f.sourceFileID
where	f.isActive = 1
)
select
	[dstFileID] = isnull(ft.[dstFileID], 0),
	[dstName] = isnull(dbo.gfisf_get_filename(isnull(dff.folderPath, '') + '\' + df.name), ''),
	[dstFolderType] = isnull(dff.[type], ''),
	[dstFolder] = isnull(dbo.gfisf_get_filepath(isnull(dff.folderPath, '')	+ '\' + df.name), ''),
	[dstProcessName] = isnull(df.processName, ''),
    [dstProcessSeconds] = isnull(df.processSeconds, 0),
    [dstTsFileName] = isnull(df.[tsFileName], ''),
	[srcFileID] = isnull(ft.[srcFileID], 0),
	[srcName] = isnull(dbo.gfisf_get_filename(isnull(sff.folderPath, '') + '\' + sf.name), ''),
	[srcFolderType] = isnull(sff.[type], ''),
	[srcFolder] = isnull(dbo.gfisf_get_filepath(isnull(sff.folderPath, '') + '\' + sf.name), ''),
	[srcBackupFolder] = isnull(isnull(sff.folderPath, '') + '\' + sf.backupSubfolder + '\', ''),
    [srcTsFileName] = isnull(sf.[tsFileName], ''),
    [dstFileEmails] = isnull(df.emails, ''),
    [dstMessageText] = isnull(df.messageText, '')
from cte_ft as ft
inner join etl_files as df
	on	df.id = ft.dstFileID
inner join etl_folders as dff
	on	dff.[type] = df.folderType
left join etl_files as sf
	on	sf.id = ft.srcFileID
left join etl_folders as sff
	on	sff.[type] = sf.folderType
where	ft.[srcFileID] is null
	or	ft.[srcFileID] is not null and sff.folderPath <> '' and sf.name <> ''
go


-- ===
-- END
-- ===
-- =================
-- SCRIPT END
-- =================

print 'Script end: ' + convert(varchar(12),GETDATE(),114)

-- ===
-- END
-- ===