////////////////////////////////////////////////////////////////////////
// This SQL script updates GFI database objects to the current version
// DBMS: Sybase Adaptive Server Anywhere 9
// $Author: rbecker $
// $Revision: 1.47 $
// $Date: 2017-04-07 19:06:11 $

/////////////////////////////////////////////////////////////////////////
// * * * T A B L E   D E F I N I T I O N S * * *
/////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Sybase build control
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'GFIDBASA1') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'GFIDBASA1', 'Incomplete', 1, 'Sybase build control');
end if;	

if not exists(select null from configurationsData where configurationID = 0 and dataName = 'GFIDBASA2') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'GFIDBASA2', 'Incomplete', 1, 'Sybase build control');
end if;	

if not exists(select null from configurationsData where configurationID = 0 and dataName = 'GFIDBASA3') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'GFIDBASA3', 'Incomplete', 1, 'Sybase build control');
end if;	

if not exists(select null from configurationsData where configurationID = 0 and dataName = 'GFIDBASA4') then
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'GFIDBASA4', 'Incomplete', 1, 'Sybase build control');
end if;

if exists(select 1 from sysconstraint where constraint_name='CKC_TYPE_BLST') then
   alter table blst drop constraint CKC_TYPE_BLST;// type column constraint allowed 1 (individual) and 2 (range); remove it to allow 3 (good list)
end if;
go

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Update dba.configurationsData set datavalue = 'Incomplete' where dataname in('GFIDBASA1','GFIDBASA2','GFIDBASA3','GFIDBASA4');
go
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

begin
	declare li     smallint;
	declare ls     varchar(1024);
	declare ls_tab varchar(32);

	//set ls='cbxid,gfi_eq_cnf,vnd_fileheader,vnd_english,vnd_spanish,vnd_init,vnd_screens,vnd_screentext,vnd_speedtickets,vnd_selectticketview,vnd_cassettes,vnd_media,vnd_farecell,vnd_status,vnd_sum,vnd_list,vnd_cnf,vnd_alarm_mgr,';
	set ls='cbxid,gfi_eq_cnf,vnd_status,vnd_sum,vnd_cnf,vnd_alarm_mgr,gfi_db_log,gfi_epay_hot,';
	while length(ls)>0 loop
	   set li=locate(ls,',');
	   set ls_tab=substr(ls,1,li - 1);
	   set ls=substr(ls,li+1);

	   if exists(select 0 from sys.systable where table_name=ls_tab and upper(table_type)='BASE') then
		  execute immediate 'drop table '||ls_tab;
	   end if;
	end loop;
end;
go

if not exists(select 0 from sys.sysfile where upper(dbspace_name)='VIP') then
   create dbspace VIP as 'gfi_vip.db';
end if;
go

/////////////////////////////////////////////////////////////////////////
// vnd_def
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='vnd_def' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sys.syscolumns where tname='vnd_def' and cname='grp') then
      alter table dba.vnd_def add grp tinyint default 0;
      alter table dba.vnd_def modify grp not null;
   end if;
   if not exists(select 0 from sys.syscolumns where tname='vnd_def' and cname='des') then
      alter table dba.vnd_def add des tinyint default 0;
      alter table dba.vnd_def modify des not null;
   end if;
else
   create table DBA.vnd_def (
      pass_type     smallint     not null,
      pass_category smallint     not null,
      t_ndx         smallint     not null,
      enabled_f     char         not null constraint CKC_ENABLED_F_VND_DEF check (enabled_f in ('Y','N')),
      pass_desc     varchar(255) not null,
      grp 	    tinyint default 0, 
      des 	    tinyint default 0,
      constraint PK_VND_DEF primary key (pass_type, pass_category),
      constraint AK_VND_DEF unique (t_ndx)) in VIP;
end if;
go

grant SELECT on DBA.vnd_def to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_tr
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='vnd_tr' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sys.syscolumns where tname='vnd_tr' and cname='card_type') then
      alter table dba.vnd_tr add card_type tinyint null, add card_eid bigint null, add prod_id tinyint null;
   end if;

   if not exists(select 0 from sys.syscolumns where tname='vnd_tr' and cname='start_ts') then
      alter table dba.vnd_tr add start_ts timestamp null, add exp_ts timestamp null;
   end if;

   if not exists(select 0 from sys.syscolumns where tname='vnd_tr' and cname='pending') then
      alter table dba.vnd_tr add pending tinyint not null default 0;
   end if;

   if not exists(select 0 from sys.syscolumns where tname='vnd_tr' and cname='card_exp_ts') then
      alter table dba.vnd_tr add card_exp_ts timestamp null;
   end if;

   if not exists(select 0 from sys.syscolumns where tname='vnd_tr' and cname='transaction_tag') then
      alter table dba.vnd_tr add transaction_tag varchar(20) null;  -- LG-728
   end if;
else
   create table DBA.vnd_tr (
      tr_id         integer      not null default autoincrement,
      tr_seq        integer      not null default 0,
      tday          date         not null,
      loc_n         smallint     not null,
      eq_type       tinyint      not null,
      eq_n          smallint     not null,
      pass_type     smallint     not null,
      pass_category smallint     not null,
      ts            timestamp    not null,
      eq_seq        integer      not null,
      type          smallint     not null,
      grp           tinyint      not null default 0,
      des           tinyint      not null default 0,
      seq           integer      not null default 0,
      aid           smallint     not null default 0,
      sc            tinyint      not null default 0,
      mid           smallint     not null default 0,
      tpbc          smallint     not null default 0,
      exp_off       smallint     not null default 0,
      deduction     numeric(6,2) not null default 0,
      remval        numeric(6,2) not null default 0,
      price         numeric(6,2) not null default 0,
      bill_amt      numeric(6,2) not null default 0,
      coin_amt      numeric(6,2) not null default 0,
      chg_amt       numeric(6,2) not null default 0,
      chg_amt_err   numeric(6,2) not null default 0,
      uncl_amt      numeric(6,2) not null default 0,
      t1_in         tinyint      not null default 0,
      t2_in         tinyint      not null default 0,
      t1_out        tinyint      not null default 0,
      t2_out        tinyint      not null default 0,
      pay_type      tinyint      not null default 0,
      flags         integer      not null default 0,
      mod_type      tinyint      not null default 0,
      mod_pos       smallint     not null default 0,
      mod_id        varchar(15),
      userid        integer      not null default 0,
      card_type     tinyint,
      card_eid      bigint,
      prod_id       tinyint,
      start_ts      timestamp,
      exp_ts        timestamp,
      pending       tinyint      not null default 0,
      card_exp_ts   timestamp,
      transaction_tag varchar(20),		-- LG-728
      constraint PK_VND_TR primary key (tr_id),
      constraint FK_VND_TR_VND_DEF foreign key (pass_type, pass_category)
         references DBA.vnd_def (pass_type, pass_category)
         on update restrict
         on delete restrict) in VIP;

   create index NDX_VND_TR_TS on DBA.vnd_tr (ts ASC) in VIP;
   create index NDX_VND_TR_TDAY on DBA.vnd_tr (tday ASC) in VIP;
   create index NDX_VND_TR_LOC_EQ_TDAY on DBA.vnd_tr (loc_n ASC, eq_type ASC, eq_n ASC, tday ASC) in VIP;
   create index NDX_VND_TR_TR_SEQ on DBA.vnd_tr (tr_seq ASC) in VIP;
end if;
go

grant SELECT on DBA.vnd_tr to PUBLIC;
go

if not exists(select 0 from sysindex where index_name='NDX_VND_TR_LOC_EQ_TS') then
   create index NDX_VND_TR_LOC_EQ_TS on dba.vnd_tr (loc_n ASC, eq_type ASC, eq_n ASC, ts ASC) in VIP;
end if;
go

if not exists(select 0 from sysindex where index_name='NDX_VND_TR_UID') then
   create index NDX_VND_TR_UID on dba.vnd_tr (card_type ASC, card_eid ASC) in VIP;
end if;
go

/////////////////////////////////////////////////////////////////////////
// cmdmgr
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.syscolumns where tname='cmdmgr' and cname='cmd' and length=4000) then
  alter table cmdmgr modify cmd varchar(4000) not null;
end if;
go

/////////////////////////////////////////////////////////////////////////
// configurations - list of configurations
/////////////////////////////////////////////////////////////////////////

if not exists(select 1 from sys.systable where table_name='configurations' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.configurations (
	  id smallint not null,
	  name varchar(50) not null,
	  isActive bit not null,
	  comments varchar(250) null,
      constraint PK_CONFIGURATIONS primary key (id),
      constraint AK_CONFIGURATIONS unique (name)
	);

   comment on table DBA.configurations is 'List of configurations';
   comment on column DBA.configurations.id is 'Configuration unique ID';
   comment on column DBA.configurations.name is 'Configuration unique name';
   comment on column DBA.configurations.isActive is '1 - active, 0 - inactive';
   comment on column DBA.configurations.comments is 'Configuration comments';
end if;
go

grant SELECT on dba.configurations to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// configurationsData - configurations data
/////////////////////////////////////////////////////////////////////////

if not exists(select 1 from sys.systable where table_name='configurationsData' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.configurationsData (
	  id int not null identity,
	  configurationID smallint not null,
	  dataName varchar(100) not null,
	  dataValue varchar(100) not null,
	  isActive bit not null,
	  comments varchar(250) null,
      constraint PK_CONFIGURATIONSDATA primary key (id),
      constraint AK_CONFIGURATIONSDATA unique (configurationID, dataName),
      constraint FK_CONFIGURATIONSDATA_CONFIGURATIONS foreign key (configurationID)
         references DBA.configurations (id)
	);

   comment on table DBA.configurationsData is 'configurations data';
   comment on column DBA.configurationsData.id is 'Configuration data unique ID';
   comment on column DBA.configurationsData.configurationID is 'Reference to configurations table';
   comment on column DBA.configurationsData.dataName is 'Configuration data name';
   comment on column DBA.configurationsData.dataValue is 'Configuration data value';
   comment on column DBA.configurationsData.isActive is '1 - active, 0 - inactive';
   comment on column DBA.configurationsData.comments is 'Configuration data comments';
end if;
go

grant SELECT on dba.configurationsData to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_db_ver - GFI database version
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_db_ver' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_db_ver(
		id      tinyint         not null,
		v1      tinyint         not null,
		v2      tinyint         not null,
		v3      tinyint         not null,
		note    varchar(512)    null,
		constraint PK_GFI_DB_VER primary key (id)
	);
end if;
go

grant SELECT on DBA.gfi_db_ver to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_act_bus - Card activities on bus (farebox) (ePay autoload fulfillment is not included)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_act_bus' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_act_bus(
		loc_n          smallint    not null,
		id             int         not null,
		tr_seq         smallint    not null,
		type           smallint    not null,
		ts             datetime    not null,
		tr_d 		   datetime    compute(dateadd(day,datediff(day,('1900-01-01'),ts),('1900-01-01'))),
		eq_type        tinyint     not null,
		bus            int         not null,
		route          int         not null,
		fbx_n          int         not null,
		card_id        int         not null,
		prod_id        tinyint     not null,
		prod_type      tinyint     not null,
		price          int         not null,
		change         int         not null,
		value          int         not null,
		pay_type       tinyint     not null,
		pay_cc_type    tinyint     null,
		pay_card_n     bigint      null,
		pending        tinyint     not null,
		constraint PK_GFI_EPAY_ACT_BUS primary key (loc_n, id, tr_seq)
	);

   create index NDX_EPAY_ACT_BUS_1 on dba.gfi_epay_act_bus (card_id, tr_d);
   create index NDX_EPAY_ACT_BUS_2 on dba.gfi_epay_act_bus (card_id, prod_id, tr_d);
   create index NDX_EPAY_ACT_BUS_LOC on dba.gfi_epay_act_bus (loc_n, card_id, ts);
end if;
go

grant SELECT on dba.gfi_epay_act_bus to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_act_eq - Card activities at vending equipment (TVM/PEM) (ePay autoload fulfillment is not included)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_act_eq' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_act_eq(
		tr_id          int         not null,
		type           smallint    not null,
		ts             datetime    not null,
		tr_d 		   datetime    compute(dateadd(day,datediff(day,('1900-01-01'),ts),('1900-01-01'))),
		loc_n          smallint    not null,
		eq_type        tinyint     not null,
		eq_n           smallint    not null,
		card_id        int         not null,
		prod_id        tinyint     not null,
		prod_type      tinyint     not null,
		price          int         not null,
		change         int         not null,
		value          int         not null,
		pay_type       tinyint     not null,
		pay_cc_type    tinyint     null,
		pay_card_n     bigint      null,
		pending        tinyint     not null,
		constraint PK_GFI_EPAY_ACT_EQ primary key (tr_id)
	);

   create index NDX_EPAY_ACT_EQ_1 on dba.gfi_epay_act_eq (card_id, tr_d);
   create index NDX_EPAY_ACT_EQ_2 on dba.gfi_epay_act_eq (card_id, prod_id, tr_d);
end if;
go

grant SELECT on dba.gfi_epay_act_eq to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_act_order - Card activities initiated from ePay
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_act_order' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_act_order(
		ord_id             int         not null,
		item_n             tinyint     not null,
		rec_id             int         not null,
		type               smallint    not null,
		ts                 datetime    not null,
		tr_d               datetime        compute(coalesce(dateadd(day,datediff(day,('1900-01-01'),fulfill_ts),('1900-01-01')), dateadd(day,datediff(day,('1900-01-01'),submit_ts),('1900-01-01')), dateadd(day,datediff(day,('1900-01-01'),ts),('1900-01-01')))),
		submit_ts          datetime    null,
		fulfill_ts         datetime    null,
		fulfill_mid        tinyint     null,
		fulfill_eq_type    tinyint     null,
		fulfill_eq_n       int         null,
		rc                 smallint    null,
		card_id            int         not null,
		prod_id            tinyint     not null,
		prod_type          tinyint     not null,
		price              int         not null,
		change             int         not null,
		value              int         not null,
		pay_type           tinyint     not null,
		pay_cc_type        tinyint     null,
		pay_card_n         bigint      null,
		pending            tinyint     not null,
		constraint PK_GFI_EPAY_ACT_ORDER primary key (ord_id, item_n, rec_id, card_id)
	);

   create index NDX_EPAY_ACT_ORDER_1 on dba.gfi_epay_act_order (card_id, tr_d);
   create index NDX_EPAY_ACT_ORDER_2 on dba.gfi_epay_act_order (card_id, prod_id, tr_d);
   create index NDX_EPAY_ACT_ORDER_TS on dba.gfi_epay_act_order (ts);
   create index NDX_EPAY_ACT_ORDER_TS_TYPE on dba.gfi_epay_act_order (ts, type);
end if;
go

grant SELECT on dba.gfi_epay_act_order to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload - ePay card and product individual autoload list (list type=2)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload(
		id           int         not null,
		seq          int         not null,
		card_type    tinyint     not null,
		card_eid     bigint      not null,
		prod_id      tinyint     not null,
		prod_type    tinyint     not null,
		des          tinyint     not null,
		load_seq     tinyint     not null,
		load_type    tinyint     not null,
		tpbc         smallint    not null,
		fare_id      smallint    not null,
		value        int         not null,
		ord_id       int         not null,
		item_n       tinyint     not null,
		rec_id       int         not null,
		constraint PK_GFI_EPAY_AUTOLOAD primary key (id, seq)
	);

   create index NDX_EPAY_AUTOLOAD1 on dba.gfi_epay_autoload (id, card_type, card_eid, load_seq);
end if;
go

grant SELECT on dba.gfi_epay_autoload to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_fare - ePay fares
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload_fare' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload_fare(
		id            int         not null,
		fare_id       smallint    not null,
		active        bit         not null,
		fare_type     tinyint     not null,
		card_type     tinyint     not null,
		prod_type     tinyint     not null,
		des           tinyint     not null,
		price         int         not null,
		value         int         not null,
		flag          tinyint     not null,
		start_date    int         not null,
		exp_date      int         not null,
		exp_type      tinyint     not null,
		addtime       smallint    not null,
		constraint PK_GFI_EPAY_AUTOLOAD_FARE primary key (id, fare_id)
	);
end if;
go

grant SELECT on dba.gfi_epay_autoload_fare to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_parm - ePay autoload parameters
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload_parm' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload_parm(
		id      int             not null,
		name    varchar(32)     not null,
		n       int             not null,
		s       varchar(255)    null,
		constraint PK_GFI_EPAY_AUTOLOAD_PARM primary key (id, name)
	);
end if;
go

grant SELECT on dba.gfi_epay_autoload_parm to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_pkg - ePay autoload package definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload_pkg' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload_pkg(
		pkg_id           int         not null,
		start_ts         datetime    not null,
		end_ts1          datetime    null,
		end_ts2          datetime    null,
		auto_id          int         null,
		auto_id_range    int         null,
		fare_id          int         null,
		parm_id          int         null,
		constraint PK_GFI_EPAY_AUTOLOAD_PKG primary key (pkg_id)
	);
end if;
go

grant SELECT on dba.gfi_epay_autoload_pkg to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_range - ePay card and product range autoload list (list type=3)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload_range' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload_range(
		id           int         not null,
		load_seq     int         not null,
		card_pid1    bigint      not null,
		card_pid2    bigint      not null,
		prod_id      tinyint     not null,
		prod_type    tinyint     not null,
		des          tinyint     not null,
		load_type    tinyint     not null,
		tpbc         smallint    not null,
		fare_id      smallint    not null,
		value        int         not null,
		ord_id       int         not null,
		item_n       tinyint     not null,
		rec_id       int         not null,
		constraint PK_GFI_EPAY_AUTOLOAD_RANGE primary key (id, load_seq)
	);
end if;
go

grant SELECT on dba.gfi_epay_autoload_range to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_seq - ePay card individual autoload last load sequence number
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload_seq' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload_seq(
		card_type    tinyint     not null,
		card_eid     bigint      not null,
		load_seq     tinyint     not null,
		pkg_id       int         not null,
		seq          int         not null,
		create_ts    datetime    null,
		create_by    int         null,
		modify_ts    datetime    null,
		modify_by    int         null,
		constraint PK_GFI_EPAY_AUTOLOAD_SEQ primary key (card_type, card_eid)
	);
end if;
go

grant SELECT on dba.gfi_epay_autoload_seq to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_tr
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_epay_autoload_tr' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_epay_autoload_tr (
      tr_id         integer  not null default autoincrement,
      loc_n         smallint not null default 0,
      id            integer  not null default 0,
      tr_seq        smallint not null default 0,
      load_type     tinyint  not null,
      ts            datetime not null,
      mid           tinyint  not null,
      eq_type       tinyint  not null,
      eq_n          integer  not null,
      eq_tr_id      integer  not null,
      card_id       integer  not null default 0,
      card_type     tinyint  not null,
      card_eid      bigint   not null,
      prod_id       tinyint  not null,
      prod_id_card  tinyint  not null,
      pkg_id        integer  not null,
      load_seq      integer  not null,
      load_seq_card integer  not null,
      range         bit      not null,
      rc            tinyint  not null default 1 constraint CKC_EPAY_AUTOLOAD_TR_RC check (rc >= 1),
      value         integer  not null default 0,
      pending       tinyint  not null default 0,
      flags         integer  not null default 0,
      constraint PK_GFI_EPAY_AUTOLOAD_TR primary key (tr_id));
end if;
go

grant SELECT on DBA.gfi_epay_autoload_tr to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_tr_load
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_epay_autoload_tr_load' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_epay_autoload_tr_load (
      tr_id         int      identity,
      loc_n         smallint not null,
      id            integer  not null,
      tr_seq        smallint not null,
      load_type     tinyint  not null,
      ts            datetime not null,
      mid           tinyint  not null,
      eq_type       tinyint  not null,
      eq_n          integer  not null,
      eq_tr_id      integer  not null,
      card_id       integer  not null,
      card_type     tinyint  not null,
      card_eid      bigint   not null,
      prod_id       tinyint  not null,
      prod_id_card  tinyint  not null,
      pkg_id        integer  not null,
      load_seq      integer  not null,
      load_seq_card integer  not null,
      range         bit      not null,
      rc            tinyint  not null,
      value         integer  not null,
      pending       tinyint  not null,
      flags         integer  not null,
      constraint PK_GFI_EPAY_AUTOLOAD_TR_LOAD primary key (tr_id));
end if;
go

grant SELECT on DBA.gfi_epay_autoload_tr_load to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_autoload_tr_status - Autoload transaction processing status (to record most recent processing information in order to deal with out of sequence transactions)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_autoload_tr_status' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_autoload_tr_status(
		card_id           int         not null,
		prod_id           tinyint     not null,
		load_type         tinyint     not null,
		last_ts           datetime    null,
		last_id           int         null,
		last_seq          int         null,
		last_id_range     int         null,
		last_seq_range    int         null,
		constraint PK_GFI_EPAY_AUTOLOAD_TR_STATUS primary key (card_id, prod_id, load_type)
	);
end if;
go

grant SELECT on dba.gfi_epay_autoload_tr_status to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card - ePay card definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card(
		card_id         int            not null,
		card_type       tinyint        not null,
		card_eid        bigint         not null,
		card_pid        varchar(25)    null,
		exp_ts          datetime       null,
		user_profile    tinyint        not null,
		user_exp_ts     datetime       null,
		class           tinyint        not null,
		aid             smallint       not null,
		mid             tinyint        not null,
		load_seq        tinyint        not null,
		load_ts         datetime       not null,
		load_seq2       int            not null,
		load_ts2        datetime       not null,
		eq_type         tinyint        not null,
		eq_n            int            not null,
		sc              tinyint        not null,
		create_ts       datetime       not null,
		flags           smallint       not null,
		status_ts       datetime       null,
		delete_ts       datetime       null,
		ord_id          int            null,
		item_n          tinyint        null,
		create_by       int            null,
		modify_ts       datetime       null,
		modify_by       int            null,
		lost_ts         datetime       null,
		constraint PK_GFI_EPAY_CARD primary key (card_id),
		constraint AK_GFI_EPAY_CARD unique (card_type, card_eid)
	);

   create index NDX_EPAY_CARD_ORD_ITEM on dba.gfi_epay_card (ord_id, item_n);
   create index NDX_EPAY_CARD_PID on dba.gfi_epay_card (card_pid);
   create index NDX_EPAY_CARD_STATUS_TS on dba.gfi_epay_card (status_ts);
end if;
go

grant SELECT on dba.gfi_epay_card to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_dtl - ePay card definition detail
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_dtl' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_dtl(
		card_id      int         not null,
		prod_id      tinyint     not null,
		prod_n       tinyint     not null,
		fare_id      smallint    not null,
		prod_type    tinyint     not null,
		des          tinyint     not null,
		aid          smallint    not null,
		mid          tinyint     not null,
		tpbc         smallint    not null,
		eq_type      tinyint     not null,
		eq_n         int         not null,
		value        int         not null,
		start_ts     datetime    null,
		exp_ts       datetime    null,
		flags        smallint    not null,
		pending      tinyint     not null,
		ord_id       int         null,
		item_n       tinyint     null,
		rec_id       int         null,
		create_ts    datetime    null,
		create_by    int         null,
		modify_ts    datetime    null,
		modify_by    int         null,
		constraint PK_GFI_EPAY_CARD_DTL primary key (card_id, prod_id)
	);
end if;
go

grant SELECT on dba.gfi_epay_card_dtl to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_dtl_shdw
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_dtl_shdw' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_dtl_shdw(
		mirror_id        int identity not null,
		process_id       int          not null,
		capture_ts       datetime     not null,
		note             varchar(50)  null,
		card_id          int          not null,
		prod_id          tinyint      not null,
		prod_n           tinyint      not null,
		fare_id          smallint     not null,
		prod_type        tinyint      not null,
		des              tinyint      not null,
		aid              smallint     not null,
		mid              tinyint      not null,
		tpbc             smallint     not null,
		eq_type          tinyint      not null,
		eq_n             int          not null,
		value            int          not null,
		start_ts         datetime     null,
		exp_ts           datetime     null,
		flags            smallint     not null,
		pending          tinyint      not null,
		ord_id           int          null,
		item_n           tinyint      null,
		rec_id           int          null,
		create_ts        datetime     null,
		create_by        int          null,
		modify_ts        datetime     null,
		modify_by        int          null,
		constraint PK_GFI_EPAY_CARD_DTL_SHDW primary key (mirror_id)
	);

   create index NDX_EPAY_CARD_DTL_SHDW_CARDID on dba.gfi_epay_card_dtl_shdw (card_id);
   create index NDX_EPAY_CARD_DTL_SHDW_PROCESSID on dba.gfi_epay_card_dtl_shdw (process_id);
end if;
go

grant SELECT on dba.gfi_epay_card_dtl_shdw to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_import_log
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_import_log' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_import_log(
		process_id          int             not null,
		log_date            datetime        not null,
		user_id             int             null,
		agency              smallint        null,
		card_pid            varchar(25)     null,
		bytes               varchar(32767)   null,
		[message]           varchar(250)   null,
		stack_trace         varchar(32767)   null,
		error_code          varchar(100)   null
	);

	create index NDX_EPAY_CARD_IMORT_LOG_PID on dba.gfi_epay_card_import_log (card_pid);
	create index NDX_EPAY_CARD_IMPORT_LOG_PROCESS_ID on dba.gfi_epay_card_import_log (process_id);
	create index NDX_EPAY_CARD_IMPORT_LOG_DATE on dba.gfi_epay_card_import_log (log_date);
end if;
go

grant select on dba.gfi_epay_card_import_log to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_inv - ePay card inventory
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_inv' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_inv(
		card_type       tinyint        not null,
		card_eid        bigint         not null,
		file_id         int            not null,
		card_pid        varchar(25)    null,
		card_pid_n      bigint         null,
		card_eid_hex    varchar(20)    not null,
		cvv             varchar(4)     null,
		constraint PK_GFI_EPAY_CARD_INV primary key (card_type, card_eid)
	);

	create index NDX_EPAY_CARD_INV_PID on dba.gfi_epay_card_inv (card_pid);
	create index NDX_EPAY_CARD_INV_PID_N on dba.gfi_epay_card_inv (card_pid_n);
end if;
go

grant SELECT on dba.gfi_epay_card_inv to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_inv_log - Card manufacturer cross reference file processing log
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_inv_log' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_inv_log(
		file_id    int              identity,
		ts         datetime         not null,
		note       varchar(1024)    null,
		constraint PK_GFI_EPAY_CARD_INV_LOG primary key (file_id)
	);
end if;
go

grant SELECT on dba.gfi_epay_card_inv_log to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_shdw
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_shdw' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_shdw(
		mirror_id    int identity     not null,
		process_id   int              not null,
		capture_ts   datetime         not null,
		note         varchar(50)      null,
		card_id      int              not null,
		card_type    tinyint          not null,
		card_eid     bigint           not null,
		card_pid     varchar(25)      null,
		exp_ts       datetime         null,
		user_profile tinyint          not null,
		user_exp_ts  datetime         null,
		class        tinyint          not null,
		aid          smallint         not null,
		mid          tinyint          not null,
		load_seq     tinyint          not null,
		load_ts      datetime         not null,
		load_seq2    int              not null,
		load_ts2     datetime         not null,
		eq_type      tinyint          not null,
		eq_n         int              not null,
		sc           tinyint          not null,
		create_ts    datetime         not null,
		flags        smallint         not null,
		status_ts    datetime         null,
		delete_ts    datetime         null,
		ord_id       int              null,
		item_n       tinyint          null,
		create_by    int              null,
		modify_ts    datetime         null,
		modify_by    int              null,
		lost_ts      datetime         null,
		constraint PK_GFI_EPAY_CARD_SHDW primary key (mirror_id)
	);

	create index NDX_EPAY_CARD_SHDW_PROCESSID on dba.gfi_epay_card_shdw (process_id);
	create index NDX_EPAY_CARD_SHDW_CARDID on dba.gfi_epay_card_shdw (card_id);
	create index NDX_EPAY_CARD_SHDW_CARD_PID on dba.gfi_epay_card_shdw (card_pid);
end if;
go

grant SELECT on dba.gfi_epay_card_shdw to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_card_usr - ePay card user definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_card_usr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_card_usr(
		card_id      int            not null,
		user_id      int            not null,
		role         tinyint        not null,
		card_name    varchar(64)    null,
		create_ts    datetime       null,
		create_by    int            null,
		modify_ts    datetime       null,
		modify_by    int            null,
		constraint PK_GFI_EPAY_CARD_USR primary key (card_id, user_id)
	);
end if;
go

grant SELECT on dba.gfi_epay_card_usr to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_channel_fare - Sales channel and fare definition cross reference table (If no cross reference is defined for a channel, the channel should be able to see all fares subject to other restrictions)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_channel_fare' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_channel_fare(
		channel      tinyint         not null,
		fare_id      smallint        not null,
		type         tinyint         not null,
		text         varchar(256)    null,
		create_ts    datetime        null,
		create_by    int             null,
		modify_ts    datetime        null,
		modify_by    int             null,
		constraint PK_GFI_EPAY_CHANNEL_FARE primary key (channel, fare_id, type)
	);
end if;
go

grant SELECT on dba.gfi_epay_channel_fare to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_contact - ePay contact information
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_contact' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_contact(
		contact_id    int             not null,
		title         varchar(8)      null,
		fname         varchar(64)     not null,
		mname         varchar(64)     null,
		lname         varchar(64)     not null,
		suffix        varchar(8)      null,
		addr1         varchar(256)    not null,
		addr2         varchar(256)    null,
		addr3         varchar(256)    null,
		city          varchar(64)     not null,
		state         varchar(2)      not null,
		zip           varchar(16)     null,
		country       varchar(2)      not null,
		email         varchar(256)    null,
		phone1        varchar(16)     null,
		phone2        varchar(16)     null,
		pref          smallint        not null,
		create_ts     datetime        null,
		create_by     int             null,
		modify_ts     datetime        null,
		modify_by     int             null,
		constraint PK_GFI_EPAY_CONTACT primary key (contact_id)
	);

	create index NDX_EPAY_CONTACT_NAME on dba.gfi_epay_contact (lname, fname);
end if;
go

grant SELECT on dba.gfi_epay_contact to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_equipment
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_equipment' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_equipment(
		equipment_id  int         not null,
		serial_number varchar(32) not null,
		create_ts     datetime    not null,
		create_by     int         null,
		modify_ts     datetime    null,
		modify_by     int         null,
		constraint PK_GFI_EPAY_EQUIPMENT primary key (equipment_id),
		constraint AK_GFI_EPAY_EQUIPMENT unique (serial_number)
	);
end if;
go

grant SELECT on dba.gfi_epay_equipment to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_fare - ePay fare definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_fare' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_fare(
		fare_id               smallint        not null,
		active                bit             not null,
		img_file              varchar(256)    null,
		type                  tinyint         not null,
		grp                   tinyint         not null,
		des                   tinyint         not null,
		description           varchar(256)    not null,
		price                 int             not null,
		value                 int             not null,
		text                  varchar(256)    null,
		flags                 int             not null,
		start_ts              datetime        null,
		exp_ts                datetime        null,
		exp_type              tinyint         not null,
		auto_rep_threshold    int             null,
		auto_rep_price        int             null,
		auto_rep_value        int             null,
		add_value_min         int             not null,
		add_value_max         int             not null,
		create_ts             datetime        null,
		create_by             int             null,
		modify_ts             datetime        null,
		modify_by             int             null,
		ISBlank				  char(1)		  null,    
		nextfareid            smallint        null,
		constraint PK_GFI_EPAY_FARE primary key (fare_id)
	);
end if;
go

grant SELECT on dba.gfi_epay_fare to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_grp - ePay access control group definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_grp' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_grp(
		grp            varchar(16)     not null,
		description    varchar(256)    not null,
		active         bit             not null,
		create_ts      datetime        null,
		create_by      int             null,
		modify_ts      datetime        null,
		modify_by      int             null,
		constraint PK_GFI_EPAY_GRP primary key (grp)
	);
end if;
go

grant select on dba.gfi_epay_grp to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_grp_right - ePay group level access rights
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_grp_right' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_grp_right(
		grp          varchar(16)    not null,
		item         varchar(32)    not null,
		create_ts    datetime       null,
		create_by    int            null,
		modify_ts    datetime       null,
		modify_by    int            null,
		constraint PK_GFI_EPAY_GRP_RIGHT primary key (grp, item)
	);
end if;
go

grant select on dba.gfi_epay_grp_right to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_grp_usr - ePay access control group membership definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_grp_usr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_grp_usr(
		grp          varchar(16)    not null,
		user_id      int            not null,
		create_ts    datetime       null,
		create_by    int            null,
		modify_ts    datetime       null,
		modify_by    int            null,
		constraint PK_GFI_EPAY_GRP_USR primary key (grp, user_id)
	);
end if;
go

grant select on dba.gfi_epay_grp_usr to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst - ePay institution definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst(
		inst_id               smallint        not null,
		acct_n                int             not null,
		active                bit             not null,
		description           varchar(256)    not null,
		type                  tinyint         not null,
		contact_id            int             not null,
		purse                 int             not null,
		auto_rep_threshold    int             null,
		auto_rep_value        int             null,
		credit_limit          int             not null,
		invoice_limit         tinyint         not null,
		avail_credit          int             not null,
		add_value_min         int             not null,
		add_value_max         int             not null,
		flags                 smallint        not null,
		create_ts             datetime        not null,
		delete_ts             datetime        null,
		create_by             int             null,
		modify_ts             datetime        null,
		modify_by             int             null,
		constraint PK_GFI_EPAY_INST primary key (inst_id),
		constraint AK_GFI_EPAY_INST unique (acct_n)
	);
end if;
go

grant select on dba.gfi_epay_inst to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst_fare - ePay institution and fare definition cross reference table (If no cross reference is defined for an institution, the institution should be able to see all fares subject to other restrictions)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst_fare' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst_fare(
		inst_id      smallint        not null,
		fare_id      smallint        not null,
		text         varchar(256)    null,
		create_ts    datetime        null,
		create_by    int             null,
		modify_ts    datetime        null,
		modify_by    int             null,
		constraint PK_GFI_EPAY_INST_FARE primary key (inst_id, fare_id)
	);
end if;
go

grant select on dba.gfi_epay_inst_fare to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst_payment
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst_payment' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst_payment (
		payment_id       int         not null,
		inst_id          smallint    not null,
		amount           int         not null,
		invoice_number   varchar(50) null,
		reference_number varchar(50) null,
		receive_ts       datetime    not null,
		create_ts        datetime    not null,
		create_by        int         null,
		modify_ts        datetime    null,
		modify_by        int         null,
		constraint PK_GFI_EPAY_INST_PAYMENT primary key (payment_id)
	);
end if;
go

grant select on dba.gfi_epay_inst_payment to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst_prod - ePay institution and card product cross reference
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst_prod' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst_prod(
		inst_id      smallint       not null,
		card_id      int            not null,
		prod_id      tinyint        not null,
		prod_name    varchar(64)    null,
		create_ts    datetime       null,
		create_by    int            null,
		modify_ts    datetime       null,
		modify_by    int            null,
		constraint PK_GFI_EPAY_INST_PROD primary key (inst_id, card_id, prod_id)
	);
end if;
go

grant select on dba.gfi_epay_inst_prod to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst_prod_arc - Archived ePay institution and card product cross reference
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst_prod_arc' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst_prod_arc(
		inst_id       smallint       not null,
		card_id       int            not null,
		prod_id       tinyint        not null,
		archive_ts    datetime       not null,
		prod_name     varchar(64)    null,
		create_ts     datetime       null,
		create_by     int            null,
		modify_ts     datetime       null,
		modify_by     int            null,
		constraint PK_GFI_EPAY_INST_PROD_ARC primary key (inst_id, card_id, prod_id, archive_ts)
	);
end if;
go

grant select on dba.gfi_epay_inst_prod_arc to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst_range - ePay institution card range tracking
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst_range' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst_range(
		inst_id      smallint        not null,
		card_pid1    bigint          not null,
		card_pid2    bigint          not null,
		note         varchar(256)    null,
		create_ts    datetime        null,
		create_by    int             null,
		modify_ts    datetime        null,
		modify_by    int             null,
		constraint PK_GFI_EPAY_INST_RANGE primary key (inst_id, card_pid1, card_pid2)
	);
end if;
go

grant select on dba.gfi_epay_inst_range to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_inst_usr - ePay institution authorized user list
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_inst_usr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_inst_usr(
		inst_id        smallint        not null,
		user_id        int             not null,
		description    varchar(256)    null,
		role           tinyint         not null,
		create_ts      datetime        null,
		create_by      int             null,
		modify_ts      datetime        null,
		modify_by      int             null,
		constraint PK_GFI_EPAY_INST_USR primary key (inst_id, user_id)
	);
end if;
go

grant select on dba.gfi_epay_inst_usr to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_invoice - Invoice status
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_invoice' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_invoice(
		inv_id       int               not null,
		ts           datetime          not null,
		due_ts       datetime          not null,
		amt          numeric(10, 2)    not null,
		cnt          tinyint           not null,
		last_ts      datetime          not null,
		status       tinyint           not null,
		pay_ts       datetime          null,
		pay_amt      numeric(10, 2)    null,
		create_ts    datetime          null,
		create_by    int               null,
		modify_ts    datetime          null,
		modify_by    int               null,
		constraint PK_GFI_EPAY_INVOICE primary key (inv_id)
	);
end if;
go

grant select on dba.gfi_epay_invoice to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_log - ePay log
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_log' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_log(
		log_id         int              identity,
		user_id        int              not null,
		card_id        int              null,
		ts             datetime         not null,
		node           varchar(32)      not null,
		action         varchar(25)      null,
		description    varchar(4000)    not null,
		constraint PK_GFI_EPAY_LOG primary key (log_id)
	);

	create index NDX_EPAY_LOG_UID_TS on dba.gfi_epay_log (user_id, ts);
end if;
go

grant select on dba.gfi_epay_log to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_non_autoload_tr - Non-autoload transaction for ePay
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_non_autoload_tr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_non_autoload_tr(
		tr_id          int         identity,
		loc_n          smallint    not null,
		id             int         not null,
		tr_seq         int         not null,
		tr_type        tinyint     not null,
		ts             datetime    not null,
		mid            tinyint     not null,
		eq_type        tinyint     not null,
		eq_n           int         not null,
		card_id        int         not null,
		card_type      tinyint     not null,
		card_eid       bigint      not null,
		prod_id        tinyint     not null,
		prod_type      tinyint     not null,
		des            tinyint     not null,
		aid            smallint    not null,
		sc             tinyint     not null,
		tpbc           smallint    not null,
		price          int         not null,
		change         int         not null,
		value          int         not null,
		start_ts       datetime    null,
		exp_ts         datetime    null,
		pending        tinyint     not null,
		card_exp_ts    datetime    null,
		flags          int         not null,
		constraint PK_GFI_EPAY_NON_AUTOLOAD_TR primary key (tr_id)
	);

	create index NDX_EPAY_NON_AUTOLOAD_TR on dba.gfi_epay_non_autoload_tr (card_id, tr_type, ts);
end if;
go

grant SELECT on dba.gfi_epay_non_autoload_tr to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_non_autoload_tr_st - Non-autoload transaction processing status (to record most recent processing information in order to deal with out of sequence transactions)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_non_autoload_tr_st' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_non_autoload_tr_st(
		card_id    int         not null,
		prod_id    tinyint     not null,
		tr_type    tinyint     not null,
		last_ts    datetime    not null,
		constraint PK_GFI_EPAY_NON_AUTOLOAD_TR_ST primary key (card_id, prod_id, tr_type)
	);
end if;
go

grant SELECT on dba.gfi_epay_non_autoload_tr_st to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_notify - ePay new notification jobs
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_notify' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_notify(
		rec_id         int              identity,
		send_type      tinyint          not null,
		notify_type    tinyint          not null,
		inst_id        smallint         null,
		user_id        int              null,
		card_id        int              null,
		prod_id        tinyint          null,
		ord_id         int              null,
		item_n         tinyint          null,
		value		   varchar(1024)    null,
		ts             datetime         not null,
		create_by      int              null,
		constraint PK_GFI_EPAY_NOTIFY primary key (rec_id)
	);
end if;
go

grant select on dba.gfi_epay_notify to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_notify_done - ePay completed notification jobs
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_notify_done' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_notify_done(
		rec_id         int              not null,
		send_type      tinyint          not null,
		notify_type    tinyint          not null,
		inst_id        smallint         null,
		user_id        int              null,
		card_id        int              null,
		prod_id        tinyint          null,
		ord_id         int              null,
		item_n         tinyint          null,
		value          varchar(1024)    null,
		ts             datetime         not null,
		create_by      int              null,
		fulfill_ts     datetime         not null,
		status         smallint         not null,
		status_txt     varchar(4000)    null,
		hardcopy       varchar(256)     null,
		constraint PK_GFI_EPAY_NOTIFY_DONE primary key (rec_id)
	);
end if;
go

grant select on dba.gfi_epay_notify_done to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order - ePay order
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order(
		ord_id        int               not null,
		user_id       int               not null,
		contact_id    int               not null,
		ord_ts        datetime          not null,
		ord_by        int               not null,
		node          varchar(32)       not null,
		inst_id       smallint          null,
		inv_id        int               null,
		amt           numeric(10, 2)    not null,
		ship_addr     int               null,
		cancel_ts     datetime          null,
		cancel_by     int               null,
		constraint PK_GFI_EPAY_ORDER primary key (ord_id)
	);
end if;
go

grant select on dba.gfi_epay_order to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_autoload - ePay individual autoload order items (hotlist, registration, and manual entry)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_autoload' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_autoload(
		ord_id       int         not null,
		item_n       tinyint     not null,
		rec_id       int         not null,
		card_type    tinyint     not null,
		card_eid     bigint      not null,
		prod_id      tinyint     not null,
		prod_type    tinyint     not null,
		des          tinyint     not null,
		load_type    tinyint     not null,
		tpbc         smallint    not null,
		fare_id      smallint    not null,
		value        int         not null,
		status       tinyint     not null,
		load_seq     tinyint     null,
		constraint PK_GFI_EPAY_ORDER_AUTOLOAD primary key (ord_id, item_n, rec_id)
	);
end if;
go

grant select on dba.gfi_epay_order_autoload to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_autoload_range - ePay range autoload order items (hotlist, registration, and manual entry)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_autoload_range' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_autoload_range(
		ord_id       int         not null,
		item_n       tinyint     not null,
		rec_id       int         not null,
		card_pid1    bigint      not null,
		card_pid2    bigint      not null,
		prod_id      tinyint     not null,
		prod_type    tinyint     not null,
		des          tinyint     not null,
		load_type    tinyint     not null,
		tpbc         smallint    not null,
		fare_id      smallint    not null,
		value        int         not null,
		cnt_ok       int         not null,
		cnt_err      int         not null,
		constraint PK_GFI_EPAY_ORDER_AUTOLOAD_RANGE primary key (ord_id, item_n, rec_id)
	);
end if;
go

grant select on dba.gfi_epay_order_autoload_range to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_cc - Order credit card transaction detail
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_cc' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_cc(
		ord_id      int               not null,
		ts1         datetime          not null,
		ts2         datetime          null,
		amt         numeric(10, 2)    not null,
		rc          int               null,
		rc_txt      varchar(256)      null,
		rc_ts       datetime          null,
		tr_n        bigint            null,
		auth        varchar(50)       null,
		status      tinyint           null,
		cc_n        varchar(20)       null,
		pay_type    varchar(50)       null,
		service     varchar(256)      null,
		constraint PK_GFI_EPAY_ORDER_CC primary key (ord_id)
	);
end if;
go

grant select on dba.gfi_epay_order_cc to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_item - ePay order item
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_item' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_item(
		ord_id             int              not null,
		item_n             tinyint          not null,
		type               tinyint          not null,
		qty                smallint         not null,
		fee                numeric(5, 2)    not null,
		submit_ts          datetime         null,
		fulfill_ts         datetime         null,
		fulfill_by         int              null,
		fulfill_mid        tinyint          null,
		fulfill_eq_type    tinyint          null,
		fulfill_eq_n       int              null,
		ship_id            int              null,
		rc                 smallint         null,
		constraint PK_GFI_EPAY_ORDER_ITEM primary key (ord_id, item_n)
	);
end if;
go

grant select on dba.gfi_epay_order_item to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_item_note - ePay order item note
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_item_note' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_item_note(
		ord_id         int              not null,
		item_n         tinyint          not null,
		seq            smallint         not null,
		ts             datetime         not null,
		user_id        int              not null,
		reason_code    tinyint          null,
		note           varchar(4000)    not null,
		constraint PK_GFI_EPAY_ORDER_ITEM_NOTE primary key (ord_id, item_n, seq)
	);
end if;
go

grant select on dba.gfi_epay_order_item_note to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_new - ePay new card order detail
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_new' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_new(
		ord_id       int            not null,
		item_n       tinyint        not null,
		prod_id      tinyint        not null,
		fare_id      smallint       not null,
		value        int            null,
		owner        int            null,
		card_name    varchar(64)    null,
		flags        smallint       not null,
		prod_name    varchar(64)    null,
		constraint PK_GFI_EPAY_ORDER_NEW primary key (ord_id, item_n, prod_id)
	);
end if; 
go

grant select on dba.gfi_epay_order_new to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_prod - ePay add product order detail
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_prod' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_prod(
		ord_id       int            not null,
		item_n       tinyint        not null,
		card_id      int            not null,
		fare_id      smallint       not null,
		value        int            not null,
		flags        smallint       not null,
		prod_name    varchar(64)    null,
		constraint PK_GFI_EPAY_ORDER_PROD primary key (ord_id, item_n)
	);
end if;
go

grant select on dba.gfi_epay_order_prod to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_recharge - ePay recharge order detail
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_recharge' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_recharge(
		ord_id       int            not null,
		item_n       tinyint        not null,
		card_id      int            not null,
		prod_id      tinyint        not null,
		fare_id      smallint       not null,
		value        int            not null,
		prod_name    varchar(64)    null,
		constraint PK_GFI_EPAY_ORDER_RECHARGE primary key (ord_id, item_n)
	);
end if;
go

grant select on dba.gfi_epay_order_recharge to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_order_ship - ePay order shipping
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_order_ship' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_order_ship(
		ship_id    int            not null,
		ts         datetime       not null,
		ship_by    int            not null,
		shipper    tinyint        not null,
		track_n    varchar(64)    null,
		constraint PK_GFI_EPAY_ORDER_SHIP primary key (ship_id)
	);
end if;
go

grant select on dba.gfi_epay_order_ship to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_user_type_fare - ePay user type and fare definition cross reference table (If no cross reference is 
// defined for a user type, the user type should be able to see all fares subject to other restrictions)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_user_type_fare' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_user_type_fare(
		user_profile    tinyint         not null,
		fare_id         smallint        not null,
		text            varchar(256)    null,
		create_ts       datetime        null,
		create_by       int             null,
		modify_ts       datetime        null,
		modify_by       int             null,
		constraint PK_GFI_EPAY_USER_TYPE_FARE primary key (user_profile, fare_id)
	);
end if; 
go

grant select on dba.gfi_epay_user_type_fare to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_usr - ePay access control user account definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_usr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_usr(
		user_id           int             not null,
		acct_n            int             not null,
		login_id          varchar(256)    not null,
		active            bit             not null,
		contact_id        int             not null,
		gender            char(10)        null,
		language          tinyint         null,
		disability        tinyint         null,
		affiliation       varchar(256)    null,
		affiliation_id    varchar(256)    null,
		user_profile      tinyint         not null,
		user_exp_ts       datetime        null,
		purse             int             not null,
		pwdexp_ts         datetime        null,
		pwd_qid1          tinyint         null,
		pwd_answer1       varchar(256)    null,
		pwd_qid2          tinyint         null,
		pwd_answer2       varchar(256)    null,
		dob_ts            datetime        null,
		login_ts          datetime        null,
		lockout_ts        datetime        null,
		lockout_node      varchar(32)     null,
		flag              smallint        not null,
		acct_n2           int             null,
		create_ts         datetime        not null,
		delete_ts         datetime        null,
		create_by         int             null,
		modify_ts         datetime        null,
		modify_by         int             null,
		constraint PK_GFI_EPAY_USR primary key (user_id),
		constraint AK_GFI_EPAY_USR1 unique (acct_n),
		constraint AK_GFI_EPAY_USR2 unique (login_id)
	);
end if;
go

grant select on dba.gfi_epay_usr to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_usr_billing - ePay user account billing information
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_usr_billing' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_usr_billing(
		user_id            int             not null,
		contact_id         int             not null,
		default_billing    bit             not null,
		name               varchar(64)     null,
		token              bigint          null,
		cc_n               varchar(20)     null,
		cc_exp             datetime        null,
		cvv                smallint        null,
		pay_type           varchar(50)     null,
		service            varchar(256)    null,
		auth               varchar(50)     null,
		ts                 datetime        not null,
		create_ts          datetime        null,
		create_by          int             null,
		modify_ts          datetime        null,
		modify_by          int             null,
		constraint PK_GFI_EPAY_USR_BILLING primary key (user_id, contact_id)
	);
end if;
go

grant select on dba.gfi_epay_usr_billing to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_usr_contact - EPay user contacts
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_usr_contact' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_usr_contact(
		user_id         int         not null,
		contact_id      int         not null,
		contact_type    tinyint     not null,
		create_ts       datetime    null,
		create_by       int         null,
		modify_ts       datetime    null,
		modify_by       int         null,
		constraint PK166 primary key (user_id, contact_id, contact_type)
	);
end if;
go

grant select on dba.gfi_epay_usr_contact to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_usr_pwd - ePay user current and historical passwords
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_usr_pwd' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_usr_pwd(
		user_id      int            not null,
		pwd_id       tinyint        not null,
		salt         varchar(50)    not null,
		digest       varchar(40)    not null,
		create_ts    datetime       null,
		constraint PK_GFI_EPAY_USR_PWD primary key (user_id, pwd_id)
	);
end if;
go

grant select on dba.gfi_epay_usr_pwd to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_usr_right - ePay user level access rights
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_usr_right' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_usr_right(
		user_id      int            not null,
		item         varchar(32)    not null,
		create_ts    datetime       null,
		create_by    int            null,
		modify_ts    datetime       null,
		modify_by    int            null,
		constraint PK_GFI_EPAY_USR_RIGHT primary key (user_id, item)
	);
end if;
go

grant select on dba.gfi_epay_usr_right to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_epay_usr_session - ePay user login session
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_epay_usr_session' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_epay_usr_session(
		user_id       int            not null,
		session_id    int            not null,
		ts            datetime       not null,
		node          varchar(32)    not null,
		constraint PK_GFI_EPAY_USR_SESSION primary key (user_id, session_id)
	);
end if;
go

grant select on dba.gfi_epay_usr_session to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_eq - equipment definition, configuration, and status
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_eq' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_eq (
      loc_n          smallint     not null,
      eq_type        tinyint      not null default 1 constraint CKC_EQ_TYPE_GFI_EQ check (eq_type in (1,2)),
      eq_n           smallint     not null,
      eq_label       varchar(16)  not null,
      eq_desc        varchar(255),
      map_x          integer      not null default 0,
      map_y          integer      not null default 0,
      last_update_ts timestamp,
      last_poll_ts   timestamp,
      last_poll_uid  varchar(16),
      status         tinyint,
      status_trim    tinyint,
      status_hpr     tinyint,
      status_btp     tinyint,
      status_bst     tinyint,
      status_ctp     tinyint,
      status_cbx     tinyint,
      status_crd     tinyint,
      status_pin     tinyint,
      status_smc     tinyint,
      status_prn     tinyint,
      status_ups     tinyint,
      status_cpu     tinyint,
      constraint PK_GFI_EQ primary key (loc_n, eq_type, eq_n)) in VIP;
end if;
go

grant SELECT on dba.gfi_eq to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_eq_status - equipment status
/////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.systable where table_name='gfi_eq_status' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='max_change' and length>=9) then
      alter table dba.gfi_eq_status modify sys_per_diskfull  numeric(9,2);
      alter table dba.gfi_eq_status modify sys_cpu_temp      numeric(9,2);
      alter table dba.gfi_eq_status modify sys_cpu_fan       numeric(9,2);
      alter table dba.gfi_eq_status modify sys_temp          numeric(9,2);
      alter table dba.gfi_eq_status modify sys_5volt         numeric(9,2);
      alter table dba.gfi_eq_status modify sys_12volt        numeric(9,2);
      alter table dba.gfi_eq_status modify ups_loadlevel     numeric(9,2);
      alter table dba.gfi_eq_status modify ups_per_charge    numeric(9,2);
      alter table dba.gfi_eq_status modify ups_input_voltage numeric(9,2);
      alter table dba.gfi_eq_status modify ups_temp          numeric(9,2);
      alter table dba.gfi_eq_status modify max_change        numeric(9,2);
   end if;
   
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='card_eid') then
      alter table dba.gfi_eq_status add card_eid bigint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='pp_serial') then
      alter table dba.gfi_eq_status add pp_serial varchar(15) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='pp_version') then
      alter table dba.gfi_eq_status add pp_version varchar(50) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='cr_serial') then
      alter table dba.gfi_eq_status add cr_serial varchar(50) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='cr_version') then
      alter table dba.gfi_eq_status add cr_version varchar(50) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='bm_ver') then
      alter table dba.gfi_eq_status add bm_ver varchar(15) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='bm_uptodate') then
      alter table dba.gfi_eq_status add bm_uptodate int null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='bm_id') then
      alter table dba.gfi_eq_status add bm_id varchar(15) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_id') then
      alter table dba.gfi_eq_status add br_id varchar(15) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_ver') then
      alter table dba.gfi_eq_status add br_ver varchar(15) null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_flags') then
      alter table dba.gfi_eq_status add br_flags int null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_0') then
      alter table dba.gfi_eq_status add br_bills_contained_0 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_1') then
      alter table dba.gfi_eq_status add br_bills_contained_1 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_2') then
      alter table dba.gfi_eq_status add br_bills_contained_2 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_3') then
      alter table dba.gfi_eq_status add br_bills_contained_3 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_4') then
      alter table dba.gfi_eq_status add br_bills_contained_4 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_5') then
      alter table dba.gfi_eq_status add br_bills_contained_5 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_bills_contained_6') then
      alter table dba.gfi_eq_status add br_bills_contained_6 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_denoms_0') then
      alter table dba.gfi_eq_status add br_denoms_0 smallint null;
   end if;
   if not exists(select 1 from sys.syscolumns where tname='gfi_eq_status' and cname='br_denoms_1') then
      alter table dba.gfi_eq_status add br_denoms_1 smallint null;
   end if;
else
   create table dba.gfi_eq_status (
      loc_n                smallint                       not null,
      eq_type              tinyint                        not null,
      eq_n                 smallint                       not null,
      st_id                integer                        not null,
      tday                 datetime                       not null,
      col_ts               timestamp                      not null,
      upd_ts               timestamp                      not null,
      filename             varchar(64),
      version              integer                        not null default 0,
      last_reboot_ts       timestamp                      not null,
      disk_space           integer                        not null default 0,
      port_n               unsigned smallint              not null default 0,
      last_userid          integer                        not null default 0,
      flags                unsigned integer               not null default 0,
      alm_flags            unsigned integer               not null default 0,
      srv_flags            unsigned integer               not null default 0,
      sys_flags            unsigned integer               not null default 0,
      cnf_flags            unsigned integer               not null default 0,
      trim0_flags          unsigned integer               not null default 0,
      trim1_flags          unsigned integer               not null default 0,
      trim2_flags          unsigned integer               not null default 0,
      trim3_flags          unsigned integer               not null default 0,
      hpr1_flags           unsigned integer               not null default 0,
      hpr2_flags           unsigned integer               not null default 0,
      btp_flags            unsigned integer               not null default 0,
      ctp_flags            unsigned integer               not null default 0,
      cbx_flags            unsigned integer               not null default 0,
      ups_flags            unsigned integer               not null default 0,
      pinpad_flags         unsigned integer               not null default 0,
      smtcd_flags          unsigned integer               not null default 0,
      prn_flags            unsigned integer               not null default 0,
      kbd_flags            unsigned integer               not null default 0,
      sys_per_diskfull     numeric(9,2)                   not null default 0,
      sys_cpu_temp         numeric(9,2)                   not null default 0,
      sys_cpu_fan          numeric(9,2)                   not null default 0,
      sys_temp             numeric(9,2)                   not null default 0,
      sys_5volt            numeric(9,2)                   not null default 0,
      sys_12volt           numeric(9,2)                   not null default 0,
      ups_loadlevel        numeric(9,2)                   not null default 0,
      ups_per_charge       numeric(9,2)                   not null default 0,
      ups_input_voltage    numeric(9,2)                   not null default 0,
      ups_temp             numeric(9,2)                   not null default 0,
      cbx_id               varchar(15),
      ctp_id               varchar(15),
      btp_id               varchar(15),
      stkr_id              varchar(15),
      hpr1_id              varchar(15),
      hpr2_id              varchar(15),
      trim0_id             varchar(15),
      trim1_id             varchar(15),
      trim2_id             varchar(15),
      trim3_id             varchar(15),
      cbx_c5               smallint                       not null default 0,
      cbx_c10              smallint                       not null default 0,
      cbx_c25              smallint                       not null default 0,
      cbx_c100             smallint                       not null default 0,
      cbx_t1               smallint                       not null default 0,
      cbx_t2               smallint                       not null default 0,
      cbx_sp               smallint                       not null default 0,
      stkr_b1              smallint                       not null default 0,
      stkr_b2              smallint                       not null default 0,
      stkr_b5              smallint                       not null default 0,
      stkr_b10             smallint                       not null default 0,
      stkr_b20             smallint                       not null default 0,
      stkr_b50             smallint                       not null default 0,
      stkr_b100            smallint                       not null default 0,
      cmt_c5               smallint                       not null default 0,
      cmt_c10              smallint                       not null default 0,
      cmt_c25              smallint                       not null default 0,
      cmt_c50              smallint                       not null default 0,
      cmt_c100             smallint                       not null default 0,
      cmt_t1               smallint                       not null default 0,
      cmt_t2               smallint                       not null default 0,
      cmt_sp               smallint                       not null default 0,
      hpr1_ct              smallint                       not null default 0,
      hpr2_ct              smallint                       not null default 0,
      hpr1_stock           tinyint                        not null default 0,
      hpr2_stock           tinyint                        not null default 0,
      trim0_ct             smallint                       not null default 0,
      trim1_ct             smallint                       not null default 0,
      trim2_ct             smallint                       not null default 0,
      trim3_ct             smallint                       not null default 0,
      trim0_stock          tinyint                        not null default 0,
      trim1_stock          tinyint                        not null default 0,
      trim2_stock          tinyint                        not null default 0,
      trim3_stock          tinyint                        not null default 0,
      max_change           numeric(9,2)                   not null default 0,
      hpr1_ver             integer                        not null default 0,
      hpr2_ver             integer                        not null default 0,
      trim0_ver            integer                        not null default 0,
      trim1_ver            integer                        not null default 0,
      trim2_ver            integer                        not null default 0,
      trim3_ver            integer                        not null default 0,
      btp_ver              integer                        not null default 0,
      ctp_ver              integer                        not null default 0,
      prn_ver              integer                        not null default 0,
      pinpad_ver           integer                        not null default 0,
      smtcd_ver            integer                        not null default 0,
      insrdr_ver           integer                        not null default 0,
      card_eid             bigint           null,
      pp_serial            varchar(15)      null,	-- GD-1228, GD-2062
      pp_version           varchar(50)      null,	-- GD-1228
      cr_serial            varchar(50)      null,	-- GD-1228
      cr_version           varchar(50)      null,	-- GD-1228
      bm_ver	             varchar(15)    null,	-- GD-1228
      bm_uptodate	         int            null,	-- GD-1228
      bm_id	             varchar(15)        null,	-- GD-1228
      br_id	             varchar(15)        null,	-- GD-1228
      br_ver	             varchar(15)    null,	-- GD-1228
      br_flags	         int                null,	-- GD-1228
      br_bills_contained_0 smallint         null,	-- GD-1228, GD-2062
      br_bills_contained_1 smallint         null,	-- GD-1228, GD-2062
      br_bills_contained_2 smallint         null,	-- GD-1228, GD-2062
      br_bills_contained_3 smallint         null,	-- GD-1228, GD-2062
      br_bills_contained_4 smallint         null,	-- GD-1228, GD-2062
      br_bills_contained_5 smallint         null,	-- GD-1228, GD-2062
      br_bills_contained_6 smallint         null,	-- GD-1228, GD-2062
      br_denoms_0          smallint         null,	-- GD-1228, GD-2062
      br_denoms_1          smallint         null,	-- GD-1228, GD-2062
      constraint PK_GFI_EQ_STATUS primary key (loc_n, eq_type, eq_n, st_id),
      constraint FK_GFI_EQ_STATUS_GFI_EQ foreign key (loc_n, eq_type, eq_n)
         references DBA.gfi_eq (loc_n, eq_type, eq_n)
            on update cascade on delete cascade) in VIP;
end if;
go

grant SELECT on dba.gfi_eq_status to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_eq_trd_station - transaction rail line, station, zone details
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_eq_trd_station' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_eq_trd_station (
      tr_id      integer    not null,
      line1      smallint   not null,
      line2      smallint   not null,
      station1   smallint   not null,
      station2   smallint   not null,
      constraint PK_GFI_EQ_TRD_STATION primary key (tr_id),
      constraint FK_GFI_EQ_TRD_STATION_TR foreign key (tr_id) references vnd_tr (tr_id) on update cascade on delete cascade) in VIP;
end if;
go

grant SELECT on dba.gfi_eq_trd_station to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_fis_maf - FIS merchant activity file
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_fis_maf' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_fis_maf(
		rec_id       int               identity,
		file_id      int               not null,
		fis_tr_id    bigint            not null,
		fis_tr_ts    datetime          not null,
		m_code       varchar(256)      not null,
		pay_type     varchar(2)        not null,
		amt          numeric(10, 2)    not null,
		cc_n         varchar(20)       not null,
		cc_exp_m     tinyint           null,
		cc_exp_y     smallint          null,
		gfi_tr_id    bigint            not null,
		usr_p1       varchar(256)      null,
		usr_p2       varchar(256)      null,
		usr_p3       varchar(256)      null,
		usr_p4       varchar(256)      null,
		usr_p5       varchar(256)      null,
		usr_p6       varchar(256)      null,
		name         varchar(256)      null,
		addr         varchar(256)      null,
		city         varchar(256)      null,
		state        varchar(256)      null,
		zip          varchar(256)      null,
		email        varchar(256)      null,
		phone        varchar(256)      null,
		fis_auth     varchar(256)      null,
		batch        int               null,
		constraint PK_GFI_FIS_MAF primary key (rec_id)
	);

	create index NDX_FIS_MAF_FILE_GFI_TR_ID on dba.gfi_fis_maf (file_id, gfi_tr_id);
	create index NDX_FIS_MAF_FIS_TR_ID on dba.gfi_fis_maf (fis_tr_id);
end if;
go

grant select on dba.gfi_fis_maf to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_fis_maf_log - FIS merchant activity file processing log
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_fis_maf_log' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_fis_maf_log(
		file_id     int            identity,
		filename    varchar(64)    not null,
		ts          datetime       not null,
		constraint PK_GFI_FIS_MAF_LOG primary key (file_id)
	);
end if;
go

grant select on dba.gfi_fis_maf_log to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_icap - TODO (created automatically by n_icap.sru)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_icap' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_icap(
		inst_id int not null,
		seq varchar(20) not null,
		sn varchar(40) not null,
		constraint PK_GFI_ICAP primary key (inst_id, seq)
	);

	create index NDX_GFI_ICAP_SN on dba.gfi_icap (sn);
end if;
go

grant select on dba.gfi_icap to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_icap_last_file_date - TODO (created automatically by n_icap.sru)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_icap_last_file_date' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_icap_last_file_date(
		Inst_ID int not null,
		DateOfMostRecentFile datetime not null,
		TypeOfCard char(1) not null,
		constraint PK_GFI_ICAP_LAST_FILE_DATE primary key (Inst_ID, TypeOfCard)
	);
end if;
go

grant select on dba.gfi_icap_last_file_date to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_icap_sc - TODO (created automatically by n_icap.sru)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_icap_sc' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_icap_sc(
		inst_id smallint not null,
		card_pid varchar(25) not null,
		card_type tinyint not null,
		card_eid bigint not null,
		constraint PK_GFI_ICAP_SC primary key (inst_id, card_pid),
		constraint AK_GFI_ICAP_SC unique (card_type, card_eid)
	);
end if;
go

grant select on dba.gfi_icap_sc to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_ldr - data file processing log
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_ldr' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_ldr (
      id       integer     not null,
      filename varchar(64) not null constraint CKC_FILENAME_GFI_LDR check (filename=upper(filename)),
      load_ts  timestamp   not null default CURRENT_TIMESTAMP,
      file_ts  timestamp   not null,
      filetype tinyint     not null default 0,
      loc_n    smallint    not null default 0,
      eq_type  tinyint     not null default 0,
      eq_n     smallint    not null default 0,
      constraint PK_GFI_LDR primary key (id));

   create index NDX_GFI_LDR_FILENAME on dba.gfi_ldr (filename ASC);
end if;
go

grant SELECT on dba.gfi_ldr to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_lst - general list table for various lookup data
/////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.systable where table_name='gfi_lst' and table_type='BASE' and creator=user_id('DBA')) then
   if exists(select 1 from sysconstraint where constraint_name='AK_GFI_LST') then
      alter table dba.gfi_lst drop constraint AK_GFI_LST;
   end if;

   if not exists(select 1 from sysindex where index_name='NDX_GFI_LST_TYPE_CLASS_NAME') then
      create index NDX_GFI_LST_TYPE_CLASS_NAME on dba.gfi_lst (type ASC, class ASC, name ASC) in NDX;
   end if;
else
   create table dba.gfi_lst (
      type       varchar(16)  not null constraint CKC_TYPE_GFI_LST check (type=upper(type)),
      class      smallint     not null default 0,
      code       integer      not null,
      name       varchar(256) not null,
      value      varchar(4000),
      constraint PK_GFI_LST primary key (type, class, code),
      constraint CKT_GFI_LST check (type<>'TYPE' or name=upper(name)));

   create index NDX_GFI_LST_TYPE_CODE_NAME on dba.gfi_lst (type ASC, code ASC, name ASC) in NDX;
   create index NDX_GFI_LST_TYPE_CLASS_NAME on dba.gfi_lst (type ASC, class ASC, name ASC) in NDX;
end if;
go

grant SELECT on dba.gfi_lst to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_ml - master list
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_ml' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_ml (
      loc_n      smallint not null,
      id         integer  not null,
      misread_c  smallint not null default 0,
      passback_c smallint not null default 0,
      invalid_c  smallint not null default 0,
      expired_c  smallint not null default 0,
      badlist_c  smallint not null default 0,
      constraint PK_GFI_ML primary key (loc_n, id));
end if;
go

grant SELECT on dba.gfi_ml to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_pv_recon - Probing revenue and vaulting revenue reconciliation
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='gfi_pv_recon' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sys.syscolumns where tname='gfi_pv_recon' and cname='type') then
      execute immediate 'drop table gfi_pv_recon';
   end if;
end if;
go

if not exists(select 0 from sys.systable where table_name='gfi_pv_recon' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_pv_recon (
      loc_n  smallint      not null,
      tday   date          not null,
      bus    integer       not null,
      type   tinyint       not null constraint CKC_GFI_PV_RECON_TYPE check (type between 1 and 5),
      rev    numeric(10,2) not null default 0,
      c1     integer       not null default 0,
      c5     integer       not null default 0,
      c10    integer       not null default 0,
      c25    integer       not null default 0,
      c50    integer       not null default 0,
      c100   integer       not null default 0,
      b1     integer       not null default 0,
      b2     integer       not null default 0,
      b5     integer       not null default 0,
      b10    integer       not null default 0,
      b20    integer       not null default 0,
      ir     bit           not null default 0,
      constraint PK_GFI_PV_RECON primary key (loc_n, tday, bus, type));
end if;
go

grant SELECT on DBA.gfi_pv_recon to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_range - range definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_range' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_range (
      loc_n   smallint   not null,
      type    tinyint    not null,
      v1      integer    not null,
      v2      integer    not null,
      constraint PK_GFI_RANGE primary key (loc_n, type, v1, v2));
end if;
go

grant SELECT on dba.gfi_range to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_refund - refund manager
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_refund' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_refund (
      rec_id               integer        not null,
      name                 varchar(256)   not null,
      addr                 varchar(256)   not null,
      city                 varchar(64)    not null,
      state                varchar(32)    not null,
      zip                  varchar(16)    not null,
      country              varchar(32)    not null,
      email                varchar(64)    not null,
      phone1               varchar(32)    not null,
      phone2               varchar(32)    not null,
      ts                   timestamp      not null,
      loss_ts              timestamp      not null,
      reason               tinyint        not null,
      story                varchar(4000)  not null,
      loc_n                smallint       not null,
      eq_n                 smallint       not null,
      receipt_id           integer        not null,
      amt                  numeric(6,2)   not null,
      refund_decision      bit            null,
      refund_decision_ts   timestamp      null,
      refund_decision_uid  varchar(16)    null,
      refund_ts            timestamp      null,
      refund_uid           varchar(16)    null,
      refund_type          tinyint        null,
      refund_amt           numeric(6,2)   null,
      refund_note          varchar(4000)  null,
      constraint PK_GFI_REFUND primary key (rec_id));
end if;
go

grant SELECT on dba.gfi_refund to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_stop - Bus stop list
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_stop' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_stop(
		stop_id      varchar(8)        not null,
		route        int               not null,
		dir          char(1)           not null,
		name         varchar(256)      not null,
		longitude    numeric(11, 8)    not null,
		latitude     numeric(11, 8)    not null,
		constraint PK_GFI_STOP primary key (stop_id, route, dir)
	);
end if;
go

grant select on dba.gfi_stop to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_tr_journal
/////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.syscolumns where tname='gfi_tr_journal' and cname='ttp') then
   execute immediate 'drop table gfi_tr_journal';
end if;
go

if not exists(select 0 from sys.systable where table_name='gfi_tr_journal' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.gfi_tr_journal (
      rec_id      integer      not null default autoincrement,
      card_type   tinyint      not null,
      card_eid    bigint       not null,
      prod_id     tinyint      not null default 0,
      type        smallint     not null,
      mid         tinyint      not null,
      eq_type     tinyint      not null,
      eq_n        integer      not null,
      ver         tinyint      not null default 0,
      ts          timestamp    not null,
      fs          tinyint      not null default 1,
      grp         tinyint      not null,
      des         tinyint      not null default 0,
      f_use_f     char(1)      not null default 'N' constraint CKC_GFI_TR_JOURNAL_F_USE_F check (f_use_f in ('Y','N')),
      charge      numeric(6,2) not null default 0,
      value       integer      not null default 0,
      pkg_id      integer      not null default 0,
      pkg_prod_id tinyint      not null default 0,
      range       bit          not null default 0,
      load_seq    integer      not null default 0,
      pending     tinyint      not null default 0,
      rc          tinyint      not null default 1,
      constraint PK_GFI_TR_JOURNAL primary key (rec_id));
end if;
go

grant SELECT on DBA.gfi_tr_journal to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_tr_journal_arc - Card journal transaction archive
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_tr_journal_arc' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_tr_journal_arc(
		rec_id         int              not null,
		card_type      tinyint          not null,
		card_eid       bigint           not null,
		prod_id        tinyint          not null,
		type           smallint         not null,
		mid            tinyint          not null,
		eq_type        tinyint          not null,
		eq_n           int              not null,
		ver            tinyint          not null,
		ts             datetime         not null,
		fs             tinyint          not null,
		grp            tinyint          not null,
		des            tinyint          not null,
		f_use_f        char(1)          not null,
		charge         numeric(6, 2)    not null,
		value          int              not null,
		pkg_id         int              not null,
		pkg_prod_id    tinyint          not null,
		range          bit              not null,
		load_seq       int              not null,
		pending        tinyint          not null,
		rc             tinyint          not null,
		archive_ts     datetime         not null,
		proc_rc        int              not null,
		constraint PK_GFI_TR_JOURNAL_ARC primary key (rec_id)
	);
end if;
go

grant select on dba.gfi_tr_journal_arc to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_tr_journal_load - TODO
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_tr_journal_load' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.gfi_tr_journal_load(
		rec_id         int              identity,
		card_type      tinyint          not null,
		card_eid       bigint           not null,
		prod_id        tinyint          not null,
		type           smallint         not null,
		mid            tinyint          not null,
		eq_type        tinyint          not null,
		eq_n           int              not null,
		ver            tinyint          not null,
		ts             datetime         not null,
		fs             tinyint          not null,
		grp            tinyint          not null,
		des            tinyint          not null,
		f_use_f        char(1)          not null,
		charge         numeric(6, 2)    not null,
		value          int              not null,
		pkg_id         int              not null,
		pkg_prod_id    tinyint          not null,
		range          bit              not null,
		load_seq       int              not null,
		pending        tinyint          not null,
		rc             tinyint          not null,
		constraint PK_GFI_TR_JOURNAL_LOAD primary key (rec_id)
	);
end if;
go

grant select on dba.gfi_tr_journal_load to public;
go

/////////////////////////////////////////////////////////////////////////
// gfi_tr_scard
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='gfi_tr_scard' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sys.syscolumns where tname='gfi_tr_scard' and cname='start_ts') then
      alter table dba.gfi_tr_scard add start_ts timestamp null, add exp_ts timestamp null;
   end if;

   if not exists(select 0 from sys.syscolumns where tname='gfi_tr_scard' and cname='pending') then
      alter table dba.gfi_tr_scard add pending tinyint not null default 0;
   end if;
else
   create table dba.gfi_tr_scard (
      loc_n     smallint  not null,
      id        integer   not null,
      tr_seq    smallint  not null,
      card_id   integer   not null default 0,
      card_type tinyint   not null,
      card_eid  bigint    not null,
      prod_id   tinyint   not null,
      start_ts  timestamp null,
      exp_ts    timestamp null,
      pending   tinyint   not null default 0,
      constraint PK_GFI_TR_SCARD primary key (loc_n, id, tr_seq)) on TRN_HDR;
end if;
go

grant SELECT on DBA.gfi_tr_scard to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_tvm_mod - TVM module S/N and firmware version
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_tvm_mod' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_tvm_mod (
      eq_n     smallint    not null,
      mod_type tinyint     not null,
      mod_pos  tinyint     not null default 0,
      seq      tinyint     not null default 0,
      ts       timestamp   not null default current timestamp,
      mod_id   varchar(15) null,
      mod_ver  varchar(10) null,
      constraint PK_GFI_TVM_MOD primary key (eq_n, mod_type, mod_pos, seq)) in VIP;
end if;
go

grant SELECT on dba.gfi_tvm_mod to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_upgrade - automatic software upgrade packages and settings
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='gfi_upgrade' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_upgrade (
      name      varchar(8)       not null default 'NM' constraint CKC_NAME_GFI_UPGRADE check (name=upper(name)),
      ver       varchar(16)      not null,
      check_ver bit              not null default 1,
      start_ts  datetime         not null default current timestamp,
      pkg       long binary      null,
      pkg_crc   unsigned integer null,
      constraint PK_GFI_UPGRADE primary key (name));
end if;
go

grant SELECT on dba.gfi_upgrade to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// gfi_vlt_prb_total - Probe-without-vault totals (reset at vaulting)
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='gfi_vlt_prb_total' and table_type='BASE' and creator=user_id('DBA')) then
   create table dba.gfi_vlt_prb_total (
      cbxid     smallint        not null,
      loc_n     smallint        not null,
      seq1      integer         not null,
      seq2      integer         not null,
      bus       integer         not null,
      revenue   numeric(14,2)   not null default 0,
      penny     integer         not null default 0,
      nickel    integer         not null default 0,
      dime      integer         not null default 0,
      quarter   integer         not null default 0,
      half      integer         not null default 0,
      sba       integer         not null default 0,
      one       integer         not null default 0,
      two       integer         not null default 0,
      five      integer         not null default 0,
      ten       integer         not null default 0,
      twenty    integer         not null default 0,
      token     integer         not null default 0,
      ticket    integer         not null default 0,
      misc_c    integer         not null default 0,
      flags     integer         not null default 0,
      constraint PK_GFI_VLT_PRB_TOTAL primary key (cbxid));
end if;
go

grant SELECT on dba.gfi_vlt_prb_total to PUBLIC;
go

//
//  Add timestamp to the gfi_vlt_prb_total table
//
if not exists(select 0 from sys.syscolumns where tname='gfi_vlt_prb_total' and cname='ts') then
   alter table dba.gfi_vlt_prb_total add ts  timestamp    default current timestamp;
end if;
go

/////////////////////////////////////////////////////////////////////////
// lang_app - Application definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='lang_app' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.lang_app(
	   app_id               smallint             not null,
	   application          varchar(32)          not null,
	   description          varchar(64)          not null,
		constraint PK_LANG_APP primary key (app_id),
		constraint AK_LANG_APP unique (application)
	);
end if;
go

grant select on dba.lang_app to public;
go

/////////////////////////////////////////////////////////////////////////
// lang_en_us - English (US) dictionary
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='lang_en_us' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.lang_en_us (
	   app_id               smallint             not null,
	   phrase_id            smallint             not null,
	   phrase               varchar(4000)        not null,
	   note                 varchar(1024)        null,
		constraint PK_LANG_EN_US primary key (app_id, phrase_id)
	);
end if;
go

grant select on dba.lang_en_us to public;
go

/////////////////////////////////////////////////////////////////////////
// lang_es_mx - Spanish (Mexico) dictionary
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='lang_es_mx' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.lang_es_mx (
	   app_id               smallint             not null,
	   phrase_id            smallint             not null,
	   phrase               varchar(4000)        not null,
	   note                 varchar(1024)        null,
		constraint PK_LANG_ES_MX primary key (app_id, phrase_id)
	);
end if;
go

grant select on dba.lang_es_mx to public;
go

/////////////////////////////////////////////////////////////////////////
// lang_fr_ca - French (Canada) dictionary
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='lang_fr_ca' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.lang_fr_ca (
	   app_id               smallint             not null,
	   phrase_id            smallint             not null,
	   phrase               varchar(4000)        not null,
	   note                 varchar(1024)        null,
		constraint PK_LANG_FR_CA primary key (app_id, phrase_id)
	);
end if;
go

grant select on dba.lang_fr_ca to public;
go

/////////////////////////////////////////////////////////////////////////
// Languages
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='Languages' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.Languages(
		LanguageID      tinyint          identity,
		Description     varchar(256)     not null,
		DateModified    smalldatetime    not null,
		ModifiedBy      varchar(64)      not null,
		farebox_id		tinyint null,
		constraint PK_LANGUAGES primary key (LanguageID)
	);
end if;
go

grant select on dba.Languages to public;
go

/////////////////////////////////////////////////////////////////////////
// media_track2
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.syscolumns where tname='media_track2' and cname='trk') then
   alter table dba.media_track2 add trk bit not null default 0;
   comment on column media_track2.trk is '0 - read track2 only; 1 - read track 1 and track2';
end if;
go

/////////////////////////////////////////////////////////////////////////
// MessageLanguages
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='MessageLanguages' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.MessageLanguages(
		MessageID       smallint          not null,
		LanguageID      tinyint           not null,
		MessageText     varchar(4000)    not null,
		DateModified    smalldatetime     not null,
		ModifiedBy      varchar(64)       not null,
		constraint PK_MESSAGELANGUAGES primary key (MessageID, LanguageID)
	);
end if;
go

grant select on dba.MessageLanguages to public;
go

/////////////////////////////////////////////////////////////////////////
// Messages
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='Messages' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.Messages(
		MessageID        smallint         identity,
		Description      varchar(256)     not null,
		EditWidth        tinyint          null,
		ExportWidth      tinyint          null,
		EditJustify      char(1)          null,
		FareBoxString    char(20)         null,
		DateModified     smalldatetime    not null,
		ModifiedBy       varchar(64)      not null,
		constraint PK_MESSAGES primary key (MessageID)
	);
end if;
go

grant select on dba.Messages to public;
go

/////////////////////////////////////////////////////////////////////////
// security_app - Application definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_app' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_app(
		application    varchar(32)    not null,
		description    varchar(64)    not null,
		constraint PK_SECURITY_APP primary key (application)
	);
end if;
go

grant select on dba.security_app to public;
go

/////////////////////////////////////////////////////////////////////////
// security_def - Security definition (all supported security access items)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_def' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_def(
		application    varchar(32)     not null,
		item           varchar(64)     not null,
		description    varchar(255)    not null,
		constraint PK_SECURITY_DEF primary key (application, item)
	);
end if;
go

grant select on dba.security_def to public;
go

/////////////////////////////////////////////////////////////////////////
// security_dict - Password dictionary
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_dict' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_dict(
		phrase    varchar(50)    not null,
		constraint PK_SECURITY_DICT primary key (phrase)
	);
end if;
go

grant select on dba.security_dict to public;
go

/////////////////////////////////////////////////////////////////////////
// security_grp - Group definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_grp' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_grp(
		group_name     varchar(16)    not null,
		description    varchar(64)    not null,
		active         char(1)        not null,
		constraint PK_SECURITY_GRP primary key (group_name)
	);
end if;
go

grant select on dba.security_grp to public;
go

/////////////////////////////////////////////////////////////////////////
// security_grp_usr - User grouping
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_grp_usr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_grp_usr(
		group_name    varchar(16)    not null,
		user_name     varchar(16)    not null,
		constraint PK_SECURITY_GRP_USR primary key (group_name, user_name)
	);
end if;
go

grant select on dba.security_grp_usr to public;
go

/////////////////////////////////////////////////////////////////////////
// security_hist - Password history (encrypted)
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_hist' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_hist(
		user_name    varchar(16)    not null,
		id           smallint       not null,
		password     varchar(32)    not null,
		constraint PK_SECURITY_HIST primary key (user_name, id)
	);
end if;
go

grant select on dba.security_hist to public;
go

/////////////////////////////////////////////////////////////////////////
// security_log - Security log
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_log' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_log(
		log_id         int              identity,
		application    varchar(32)      not null,
		user_name      varchar(16)      not null,
		action	       varchar(8)       not null,
		log_dt         datetime         not null,
		description    varchar(1024)    null,
		constraint PK_SECURITY_LOG primary key (log_id)
	);
end if;
go

grant select on dba.security_log to public;
go

/////////////////////////////////////////////////////////////////////////
// security_pref - System preferences
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_pref' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_pref(
		application    varchar(32)     not null,
		code           varchar(32)     not null,
		value          varchar(255)    null,
		type           varchar(10)     null,
		description    varchar(255)    null,
		constraint PK_SECURITY_PREF primary key (application, code)
	);
end if;
go

grant select on dba.security_pref to public;
go

/////////////////////////////////////////////////////////////////////////
// security_setting - Security setting
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_setting' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_setting(
		application    varchar(32)    not null,
		item           varchar(64)    not null,
		group_name     varchar(16)    not null,
		constraint PK_SECURITY_SETTING primary key (application, item, group_name)
	);
end if;
go

grant select on dba.security_setting to public;
go

/////////////////////////////////////////////////////////////////////////
// security_sync - Data synchronization configuration and status
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_sync' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_sync(
		item           varchar(16)      not null,
		description    varchar(64)      not null,
		type           tinyint          not null,
		interval       int              not null,
		status         tinyint          not null,
		msg            varchar(1024)    null,
		timeout        int              not null,
		start_ts       datetime         null,
		end_ts         datetime         null,
		pid            int              null,
		constraint PK_SECURITY_SYNC primary key (item)
	);
end if;
go

grant select on dba.security_sync to public;
go

/////////////////////////////////////////////////////////////////////////
// security_usr - User definition
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_usr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_usr(
		user_name       varchar(16)    not null,
		description     varchar(64)    not null,
		password        varchar(32)    null,
		ts              datetime       null,
		active          char(1)        not null,
		term_ts         datetime       null,
		lockout_ts      datetime       null,
		lockout_node    varchar(32)    null,
		pwdexp_ts       datetime       null,
		constraint PK_SECURITY_USR primary key (user_name)
	);
end if;
go

grant select on dba.security_usr to public;
go

/////////////////////////////////////////////////////////////////////////
// security_ver - Security version
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='security_ver' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.security_ver(
		application    varchar(32)     not null,
		version1       smallint        not null,
		version2       smallint        not null,
		version3       smallint        not null,
		note           varchar(512)    null,
		constraint PK_SECURITY_VER primary key (application)
	);
end if;
go

grant select on dba.security_ver to public;
go

/////////////////////////////////////////////////////////////////////////
// vlt - Modify the contraint on type in the vlt table to 1,2,3,4,5,99
/////////////////////////////////////////////////////////////////////////
if exists(select 1 from sysconstraint where constraint_name='CKC_TYPE_VLT') then
  alter table dba.vlt drop constraint CKC_TYPE_VLT;
end if;
go

ALTER TABLE dba.vlt
add constraint ckc_type_vlt
CHECK ( type IN (1,2,3,4,5,99 ) );
go

/////////////////////////////////////////////////////////////////////////
// vnd_cr - Counting room counts
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='vnd_cr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.vnd_cr(
		cr_id       int             identity,
		ts_in       datetime        not null,
		ts_cnt      datetime        null,
		ts_out      datetime        null,
		tday        datetime        null,
		barcode     varchar(20)     not null,
		mod_type    tinyint         not null,
		mod_id      varchar(15)     null,
		user_id     varchar(16)     null,
		note        varchar(255)    null,
		t1          int             not null,
		t2          int             not null,
		c1          int             not null,
		c5          int             not null,
		c10         int             not null,
		c25         int             not null,
		c50         int             not null,
		c100        int             not null,
		b1          int             not null,
		b2          int             not null,
		b5          int             not null,
		b10         int             not null,
		b20         int             not null,
		b50         int             not null,
		b100        int             not null,
		constraint PK_VND_CR primary key (cr_id)
	);

	create index NDX_VND_CR_BARCODE on dba.vnd_cr (barcode);
	create index NDX_VND_CR_TS_IN on dba.vnd_cr (ts_in);
end if;
go

grant select on dba.vnd_cr to public;
go

/////////////////////////////////////////////////////////////////////////
// vnd_cr_ev_lnk - Counting room and event link table
/////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='vnd_cr_ev_lnk' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.vnd_cr_ev_lnk(
		cr_id    int    not null,
		ev_id    int    null,
		constraint PK_VND_CR_EV_LNK primary key (cr_id)
	);
end if;
go

grant select on dba.vnd_cr_ev_lnk to public;
go

/////////////////////////////////////////////////////////////////////////
// vnd_ev
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='vnd_ev' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sys.syscolumns where tname='vnd_ev' and cname='log') then
      alter table vnd_ev add log varchar(256) null;
   end if;
else
   create table DBA.vnd_ev (
      ev_id    integer      not null default autoincrement,
      tday     date         not null,
      loc_n    smallint     not null,
      eq_type  tinyint      not null,
      eq_n     smallint     not null,
      ts       timestamp    not null,
      eq_seq   integer      not null,
      type     smallint     not null,
      n        integer      not null default 0,
      mod_type tinyint      not null default 0,
      mod_pos  tinyint      not null default 0,
      mod_id   varchar(15),
      userid   integer      not null default 0,
      fault    smallint     not null default 0,
      audit    integer      not null default 0,
      clr_f    char         not null default 'N' constraint CKC_CLR_F_VND_EV check (clr_f in ('Y','N')),
      log      varchar(256),
      constraint PK_VND_EV primary key (ev_id)) in VIP;

   create index NDX_VND_EV_TS on DBA.vnd_ev (ts ASC) in VIP;
   create index NDX_VND_EV_TDAY on DBA.vnd_ev (tday ASC) in VIP;
   create index NDX_VND_EV_LOC_EQ_TDAY on DBA.vnd_ev (loc_n ASC, eq_type ASC, eq_n ASC, tday ASC) in VIP;
end if;
go

grant SELECT on DBA.vnd_ev to PUBLIC;
go

if not exists(select 0 from sysindex where index_name='NDX_VND_EV_LOC_EQ_TS') then
   create index NDX_VND_EV_LOC_EQ_TS on vnd_ev (loc_n ASC, eq_type ASC, eq_n ASC, ts ASC) in VIP;
end if;
go

/////////////////////////////////////////////////////////////////////////
// vnd_evd_cash
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='vnd_evd_cash' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.vnd_evd_cash (
      ev_id integer     not  null,
      amt  numeric(6,2) not  null default 0,
      c5   smallint     not  null default 0,
      c10  smallint     not  null default 0,
      c25  smallint     not  null default 0,
      c100 smallint     not  null default 0,
      b1   smallint     not  null default 0,
      b2   smallint     not  null default 0,
      b5   smallint     not  null default 0,
      b10  smallint     not  null default 0,
      b20  smallint     not  null default 0,
      b50  smallint     not  null default 0,
      b100 smallint     not  null default 0,
      t1   smallint     not  null default 0,
      t2   smallint     not  null default 0,
      constraint PK_VND_EVD_CASH primary key (ev_id),
      constraint FK_VND_EVD_CASH_VND_EV foreign key (ev_id)
         references DBA.vnd_ev (ev_id)
         on update cascade
         on delete cascade) in VIP;
end if;
go

grant SELECT on DBA.vnd_evd_cash to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_evd_svc
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='vnd_evd_svc' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.vnd_evd_svc (
      ev_id      integer  not null,
      stock_type tinyint  not null,
      stock_cnt  smallint not null default 0,
      constraint PK_VND_EVD_SVC primary key (ev_id),
      constraint FK_VND_EVD_SVC_VND_EV foreign key (ev_id)
         references DBA.vnd_ev (ev_id)
         on update cascade
         on delete cascade) in VIP;
end if;
go

grant SELECT on DBA.vnd_evd_svc to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_loader
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='vnd_loader' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sysindex where index_name='NDX_VND_LOADER_CS_CRC') then
      create index NDX_VND_LOADER_CS_CRC on vnd_loader (cs ASC, crc ASC) in VIP;
   end if;
else
   create table DBA.vnd_loader (
      id       integer not null default autoincrement,
      ts       timestamp,
      fn       char(80),
      cs       numeric(10),
      crc      numeric(10),
      source   char(80),
      loc_n    smallint,
      eq_n     smallint,
      eq_type  tinyint,
      tr_c     smallint,
      ev_c     smallint,
      cr_c     smallint,
      flags    smallint,
      status   smallint,
      start_ts timestamp,
      end_ts   timestamp,
      loader_v numeric(4,2),
      constraint PK_VND_LOADER primary key (id)) in VIP;

   create index NDX_VND_LOADER_CS_CRC on dba.vnd_loader (cs ASC, crc ASC) in VIP;
end if;
go

grant SELECT on DBA.vnd_loader to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_mnt
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='vnd_mnt' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.vnd_mnt (
      loc_n         smallint     not null,
      eq_type       tinyint      not null,
      eq_n          smallint     not null,
      tday          date         not null,
      ts            timestamp    not null,
      user_id       varchar(16),
      user_m        varchar(16),
      trim0_m       tinyint      default 0,
      trim1_m       tinyint      default 0,
      trim2_m       tinyint      default 0,
      trim3_m       tinyint      default 0,
      ctp_m         tinyint      default 0,
      btp_m         tinyint      default 0,
      hpr1_m        tinyint      default 0,
      hpr2_m        tinyint      default 0,
      cpu_m         tinyint      default 0,
      alm_m         tinyint      default 0,
      ups_m         tinyint      default 0,
      pwr_m         tinyint      default 0,
      ac_unit_m     tinyint      default 0,
      prn_m         tinyint      default 0,
      lcd_m         tinyint      default 0,
      push_button_m tinyint      default 0,
      insrdr_m      tinyint      default 0,
      pinpad_m      tinyint      default 0,
      ext_damage_m  tinyint      default 0,
      other_m       tinyint      default 0,
      constraint PK_VND_MNT primary key (loc_n, eq_type, eq_n, tday, ts)) in VIP;

   create index NDX_VND_MNT_TDAY on DBA.vnd_mnt (tday ASC) in VIP;
end if;
go

grant SELECT on DBA.vnd_mnt to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_mod_inv
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='vnd_mod_inv' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.vnd_mod_inv (
      mod_type     tinyint      not null,
      mod_id       varchar(15)  not null,
      mod_pos      tinyint      not null default 0,
      ts           timestamp    not null,
      user_id      varchar(16),
      loc_n        smallint,
      eq_type      tinyint,
      eq_n         smallint,
      mod_status   tinyint      not null default 0,
      mod_sub_type tinyint,
      constraint PK_VND_MOD_INV primary key (mod_type, mod_id)) in VIP;
end if;
go

grant SELECT on DBA.vnd_mod_inv to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_mod_status
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='vnd_mod_status' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.vnd_mod_status (
      loc_n         smallint    not null,
      eq_type       tinyint     not null,
      eq_n          smallint    not null,
      last_upd_ts   timestamp,
      btp_id        varchar(15) default '0',
      stkr_id       varchar(15) default '0',
      cbx_id        varchar(15) default '0',
      ctp_id        varchar(15) default '0',
      hpr1_id       varchar(15) default '0',
      hpr2_id       varchar(15) default '0',
      trim0_id      varchar(15) default '0',
      trim1_id      varchar(15) default '0',
      trim2_id      varchar(15) default '0',
      trim3_id      varchar(15) default '0',
      btp_ts        timestamp,
      stkr_ts       timestamp,
      cbx_ts        timestamp,
      ctp_ts        timestamp,
      hpr1_ts       timestamp,
      hpr2_ts       timestamp,
      trim0_ts      timestamp,
      trim1_ts      timestamp,
      trim2_ts      timestamp,
      trim3_ts      timestamp,
      btp_id_prev   varchar(15) default '0',
      stkr_id_prev  varchar(15) default '0',
      cbx_id_prev   varchar(15) default '0',
      ctp_id_prev   varchar(15) default '0',
      hpr1_id_prev  varchar(15) default '0',
      hpr2_id_prev  varchar(15) default '0',
      trim0_id_prev varchar(15) default '0',
      trim1_id_prev varchar(15) default '0',
      trim2_id_prev varchar(15) default '0',
      trim3_id_prev varchar(15) default '0',
      btp_prev_ts   timestamp,
      stkr_prev_ts  timestamp,
      cbx_prev_ts   timestamp,
      ctp_prev_ts   timestamp,
      hpr1_prev_ts  timestamp,
      hpr2_prev_ts  timestamp,
      trim0_prev_ts timestamp,
      trim1_prev_ts timestamp,
      trim2_prev_ts timestamp,
      trim3_prev_ts timestamp,
      constraint PK_VND_MOD_STATUS primary key (loc_n, eq_type, eq_n)) in VIP;
end if;
go

grant SELECT on DBA.vnd_mod_status to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_trd_card
/////////////////////////////////////////////////////////////////////////
if exists(select 0 from sys.systable where table_name='vnd_trd_card' and table_type='BASE' and creator=user_id('DBA')) then
   if not exists(select 0 from sys.syscolumns where tname='vnd_trd_card' and cname='auth' and length=50) then
      alter table dba.vnd_trd_card modify auth varchar(50) not null;
   end if;

   if exists(select 0 from sys.syscolumns where tname='vnd_trd_card' and cname='rc_txt') then
      execute immediate 'alter table dba.vnd_trd_card drop rc_txt';
      execute immediate 'alter table dba.vnd_trd_card drop cc_n';
   end if;

   if not exists(select 0 from sys.syscolumns where tname='vnd_trd_card' and cname='amt') then
      alter table dba.vnd_trd_card add amt      numeric(10,2) null,
                               add rc       integer       null,
                               add rc_ts    timestamp     null,
                               add tr_n     bigint        null,
                               add status   tinyint       null,
                               add pay_type varchar(2)    null;
   end if;
else
   create table DBA.vnd_trd_card (
      tr_id      integer     not null,
      cd_type    tinyint     not null,
      cd_n       varchar(30) not null,
      auth       varchar(50) not null,
      credit_seq varchar(20) not null,
      term_id    integer     not null default 0,
      amt        numeric(10,2),
      rc         integer,
      rc_ts      timestamp,
      tr_n       bigint,
      status     tinyint,
      pay_type   varchar(2),
      constraint PK_VND_TRD_CARD primary key (tr_id),
      constraint FK_VND_TRD_CARD_VND_TR foreign key (tr_id)
         references DBA.vnd_tr (tr_id)
         on update cascade
         on delete cascade) in VIP;
end if;
go

grant SELECT on DBA.vnd_trd_card to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vnd_trd_cash
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='vnd_trd_cash' and table_type='BASE' and creator=user_id('DBA')) then
   create table DBA.vnd_trd_cash (
      tr_id    integer  not null,
      c5_in    smallint not null default 0,
      c10_in   smallint not null default 0,
      c25_in   smallint not null default 0,
      c100_in  smallint not null default 0,
      b1_in    smallint not null default 0,
      b2_in    smallint not null default 0,
      b5_in    smallint not null default 0,
      b10_in   smallint not null default 0,
      b20_in   smallint not null default 0,
      b50_in   smallint not null default 0,
      b100_in  smallint not null default 0,
      c5_out   smallint not null default 0,
      c10_out  smallint not null default 0,
      c25_out  smallint not null default 0,
      c100_out smallint not null default 0,
      b1_out   smallint not null default 0,
      b2_out   smallint not null default 0,
      b5_out   smallint not null default 0,
      b10_out  smallint not null default 0,
      b20_out  smallint not null default 0,
      b50_out  smallint not null default 0,
      b100_out smallint not null default 0,
      constraint PK_VND_TRD_CASH primary key (tr_id),
      constraint FK_VND_TRD_CASH_VND_TR foreign key (tr_id)
         references DBA.vnd_tr (tr_id)
         on update cascade
         on delete cascade) in VIP;
end if;
go

grant SELECT on DBA.vnd_trd_cash to PUBLIC;
go

/////////////////////////////////////////////////////////////////////////
// vs
/////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sys.syscolumns where tname='vs' and cname='misc_c') then
   alter table dba.vs add misc_c integer not null default 0;
end if;
go

///////////////////////////////////////////////
// LG-263
///////////////////////////////////////////////
if not exists(select 0 from sys.syscolumns where tname='trd' and cname='EarnedPoints') then
   alter table dba.trd add EarnedPoints smallint null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='trd' and cname='Bonus_Start') then
   alter table dba.trd add Bonus_Start datetime null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='trd' and cname='Bonus_End') then
   alter table dba.trd add Bonus_End datetime null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='trd' and cname='Paygo_type') then
   alter table dba.trd add Paygo_type varchar(10) null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='svd' and cname='EarnedPoints') then
   alter table dba.svd add EarnedPoints smallint null;
end if;
go

///////////////////////////////////////////////
// LG-480
///////////////////////////////////////////////
if not exists(select 0 from sys.syscolumns where tname='ppd' and cname='card_eid') then
   alter table dba.ppd add card_eid bigint null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='svd' and cname='card_eid') then
   alter table dba.svd add card_eid bigint null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='srd' and cname='card_eid') then
   alter table dba.srd add card_eid bigint null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='ccd' and cname='card_eid') then
   alter table dba.ccd add card_eid bigint null;
end if;
go

if not exists(select 0 from sys.syscolumns where tname='trd' and cname='card_eid') then
   alter table dba.trd add card_eid bigint null;
end if;
go


///////////////////////////////////////////////
// LG-481
///////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='trmisc' and cname='keys') then
   alter table dba.trmisc add keys varchar(3) null;
elseif not exists(select null from sys.syscolumns where tname='trmisc' and cname='keys' and coltype='varchar') then
   alter table dba.trmisc modify keys varchar(3) null;
end if;
go

if not exists(select null from sys.syscolumns where tname='trmisc' and cname='ttp') then
   alter table dba.trmisc add ttp tinyint null;
end if;
go

if not exists(select null from sys.syscolumns where tname='scd' and cname='ttp') then
   alter table dba.scd add ttp tinyint null;
end if;
go


///////////////////////////////////////////////
// FASTFARE-282
///////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='PedBoardSerialNum') then
   alter table dba.firmware_info add PedBoardSerialNum bigint null;
   comment on column DBA.firmware_info.PedBoardSerialNum is 'Pedestal board serial number';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='PedBoardBase') then
   alter table dba.firmware_info add PedBoardBase bigint null;
   comment on column DBA.firmware_info.PedBoardBase is 'Pedestal board firmware base';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='PedBoardVer') then
   alter table dba.firmware_info add PedBoardVer integer null;
   comment on column DBA.firmware_info.PedBoardVer is 'Pedestal board firmware version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='LidBoardSerialNum') then
   alter table dba.firmware_info add LidBoardSerialNum bigint null;
   comment on column DBA.firmware_info.LidBoardSerialNum is 'Lid board serial number';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='LidBoardBase') then
   alter table dba.firmware_info add LidBoardBase bigint null;
   comment on column DBA.firmware_info.LidBoardBase is 'Lid board firmware base';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='LidBoardVer') then
   alter table dba.firmware_info add LidBoardVer integer null;
   comment on column DBA.firmware_info.LidBoardVer is 'Lid board firmware version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='SwipeBase') then
   alter table dba.firmware_info add SwipeBase bigint null;
   comment on column DBA.firmware_info.SwipeBase is 'Mag card reader f/w base';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='SwipeVer') then
   alter table dba.firmware_info add SwipeVer integer null;
   comment on column DBA.firmware_info.SwipeVer is 'Mag card reader f/w version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='SC_Main_Base') then
   alter table dba.firmware_info add SC_Main_Base bigint null;
   comment on column DBA.firmware_info.SC_Main_Base is 'Smart card firmware base';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='TSC_Main_ver') then
   alter table dba.firmware_info add TSC_Main_ver binary(10) null;
   comment on column DBA.firmware_info.TSC_Main_ver is 'TSCR firmware version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='WifiModuleFWVer') then
   alter table dba.firmware_info add WifiModuleFWVer binary(5) null;
   comment on column DBA.firmware_info.WifiModuleFWVer is 'Wifi module f/w version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='BillSoftwareVer') then
   alter table dba.firmware_info add BillSoftwareVer binary(10) null;
   comment on column DBA.firmware_info.BillSoftwareVer is 'Bill validator f/w version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='CoinSoftwareVer') then
   alter table dba.firmware_info add CoinSoftwareVer binary(10) null;
   comment on column DBA.firmware_info.CoinSoftwareVer is 'Coin validator f/w version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='OmniPrnt_ver') then
   alter table dba.firmware_info add OmniPrnt_ver binary(5) null;
   comment on column DBA.firmware_info.OmniPrnt_ver is 'Barcode printer f/w version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='OmniPrnt_model') then
   alter table dba.firmware_info add OmniPrnt_model binary(8) null;
   comment on column DBA.firmware_info.OmniPrnt_model is 'Barcode printer model';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='TrimStockVer') then
   alter table dba.firmware_info add TrimStockVer bigint null;
   comment on column DBA.firmware_info.TrimStockVer is 'Trim stock sensor version';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='Major_ver') then
   alter table dba.firmware_info add Major_ver smallint null;
   comment on column DBA.firmware_info.Major_ver is 'Major Version, e.g. 00.324, XX';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='Fpga_ver') then
   alter table dba.firmware_info add Fpga_ver smallint null;
   comment on column DBA.firmware_info.Fpga_ver is 'FPGA version, YY';
end if;
go

if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='Upgrade_status') then
   alter table dba.firmware_info add Upgrade_status smallint null;
   comment on column DBA.firmware_info.Upgrade_status is 'Disposition of last attempted upgrade';
end if;
go


///////////////////////////////////////////////
// LG-722
///////////////////////////////////////////////
if not exists(select 0 from sys.systable where table_name='ev_extd' and table_type='BASE' and creator=user_id('DBA')) then
	create table DBA.ev_extd(
		loc_n       smallint          not null,
		id          int               not null,
		seq         smallint          not null,
		ttp49 smallint NOT NULL,
		ttp50 smallint NOT NULL,
		ttp51 smallint NOT NULL,
		ttp52 smallint NOT NULL,
		ttp53 smallint NOT NULL,
		ttp54 smallint NOT NULL,
		ttp55 smallint NOT NULL,
		ttp56 smallint NOT NULL,
		ttp57 smallint NOT NULL,
		ttp58 smallint NOT NULL,
		ttp59 smallint NOT NULL,
		ttp60 smallint NOT NULL,
		ttp61 smallint NOT NULL,
		ttp62 smallint NOT NULL,
		ttp63 smallint NOT NULL,
		ttp64 smallint NOT NULL,
		ttp65 smallint NOT NULL,
		ttp66 smallint NOT NULL,
		ttp67 smallint NOT NULL,
		ttp68 smallint NOT NULL,
		ttp69 smallint NOT NULL,
		ttp70 smallint NOT NULL,
		ttp71 smallint NOT NULL,
		ttp72 smallint NOT NULL,
		ttp73 smallint NOT NULL,
		ttp74 smallint NOT NULL,
		ttp75 smallint NOT NULL,
		ttp76 smallint NOT NULL,
		ttp77 smallint NOT NULL,
		ttp78 smallint NOT NULL,
		ttp79 smallint NOT NULL,
		ttp80 smallint NOT NULL,
		ttp81 smallint NOT NULL,
		ttp82 smallint NOT NULL,
		ttp83 smallint NOT NULL,
		ttp84 smallint NOT NULL,
		ttp85 smallint NOT NULL,
		ttp86 smallint NOT NULL,
		ttp87 smallint NOT NULL,
		ttp88 smallint NOT NULL,
		ttp89 smallint NOT NULL,
		ttp90 smallint NOT NULL,
		ttp91 smallint NOT NULL,
		ttp92 smallint NOT NULL,
		ttp93 smallint NOT NULL,
		ttp94 smallint NOT NULL,
		ttp95 smallint NOT NULL,
		ttp96 smallint NOT NULL,
		ttp97 smallint NOT NULL,
		ttp98 smallint NOT NULL,
		ttp99 smallint NOT NULL,
		ttp100 smallint NOT NULL,
		ttp101 smallint NOT NULL,
		ttp102 smallint NOT NULL,
		ttp103 smallint NOT NULL,
		ttp104 smallint NOT NULL,
		ttp105 smallint NOT NULL,
		ttp106 smallint NOT NULL,
		ttp107 smallint NOT NULL,
		ttp108 smallint NOT NULL,
		ttp109 smallint NOT NULL,
		ttp110 smallint NOT NULL,
		ttp111 smallint NOT NULL,
		ttp112 smallint NOT NULL,
		ttp113 smallint NOT NULL,
		ttp114 smallint NOT NULL,
		ttp115 smallint NOT NULL,
		ttp116 smallint NOT NULL,
		ttp117 smallint NOT NULL,
		ttp118 smallint NOT NULL,
		ttp119 smallint NOT NULL,
		ttp120 smallint NOT NULL,
		ttp121 smallint NOT NULL,
		ttp122 smallint NOT NULL,
		ttp123 smallint NOT NULL,
		ttp124 smallint NOT NULL,
		ttp125 smallint NOT NULL,
		ttp126 smallint NOT NULL,
		ttp127 smallint NOT NULL,
		ttp128 smallint NOT NULL,
		ttp129 smallint NOT NULL,
		ttp130 smallint NOT NULL,
		ttp131 smallint NOT NULL,
		ttp132 smallint NOT NULL,
		ttp133 smallint NOT NULL,
		ttp134 smallint NOT NULL,
		ttp135 smallint NOT NULL,
		ttp136 smallint NOT NULL,
		ttp137 smallint NOT NULL,
		ttp138 smallint NOT NULL,
		ttp139 smallint NOT NULL,
		ttp140 smallint NOT NULL,
		ttp141 smallint NOT NULL,
		ttp142 smallint NOT NULL,
		ttp143 smallint NOT NULL,
		ttp144 smallint NOT NULL,
		ttp145 smallint NOT NULL,
		ttp146 smallint NOT NULL,
		ttp147 smallint NOT NULL,
		ttp148 smallint NOT NULL,
		ttp149 smallint NOT NULL,
		ttp150 smallint NOT NULL,
		ttp151 smallint NOT NULL,
		ttp152 smallint NOT NULL,
		ttp153 smallint NOT NULL,
		ttp154 smallint NOT NULL,
		ttp155 smallint NOT NULL,
		ttp156 smallint NOT NULL,
		ttp157 smallint NOT NULL,
		ttp158 smallint NOT NULL,
		ttp159 smallint NOT NULL,
		ttp160 smallint NOT NULL,
		ttp161 smallint NOT NULL,
		ttp162 smallint NOT NULL,
		ttp163 smallint NOT NULL,
		ttp164 smallint NOT NULL,
		ttp165 smallint NOT NULL,
		ttp166 smallint NOT NULL,
		ttp167 smallint NOT NULL,
		ttp168 smallint NOT NULL,
		ttp169 smallint NOT NULL,
		ttp170 smallint NOT NULL,
		ttp171 smallint NOT NULL,
		ttp172 smallint NOT NULL,
		ttp173 smallint NOT NULL,
		ttp174 smallint NOT NULL,
		ttp175 smallint NOT NULL,
		ttp176 smallint NOT NULL,
		ttp177 smallint NOT NULL,
		ttp178 smallint NOT NULL,
		ttp179 smallint NOT NULL,
		ttp180 smallint NOT NULL,
		ttp181 smallint NOT NULL,
		ttp182 smallint NOT NULL,
		ttp183 smallint NOT NULL,
		ttp184 smallint NOT NULL,
		ttp185 smallint NOT NULL,
		ttp186 smallint NOT NULL,
		ttp187 smallint NOT NULL,
		ttp188 smallint NOT NULL,
		ttp189 smallint NOT NULL,
		ttp190 smallint NOT NULL,
		ttp191 smallint NOT NULL,
		ttp192 smallint NOT NULL,
		ttp193 smallint NOT NULL,
		ttp194 smallint NOT NULL,
		ttp195 smallint NOT NULL,
		ttp196 smallint NOT NULL,
		ttp197 smallint NOT NULL,
		ttp198 smallint NOT NULL,
		ttp199 smallint NOT NULL,
		ttp200 smallint NOT NULL,
		ttp201 smallint NOT NULL,
		ttp202 smallint NOT NULL,
		ttp203 smallint NOT NULL,
		ttp204 smallint NOT NULL,
		ttp205 smallint NOT NULL,
		ttp206 smallint NOT NULL,
		ttp207 smallint NOT NULL,
		ttp208 smallint NOT NULL,
		ttp209 smallint NOT NULL,
		ttp210 smallint NOT NULL,
		ttp211 smallint NOT NULL,
		ttp212 smallint NOT NULL,
		ttp213 smallint NOT NULL,
		ttp214 smallint NOT NULL,
		ttp215 smallint NOT NULL,
		ttp216 smallint NOT NULL,
		ttp217 smallint NOT NULL,
		ttp218 smallint NOT NULL,
		ttp219 smallint NOT NULL,
		ttp220 smallint NOT NULL,
		ttp221 smallint NOT NULL,
		ttp222 smallint NOT NULL,
		ttp223 smallint NOT NULL,
		ttp224 smallint NOT NULL,
		ttp225 smallint NOT NULL,
		ttp226 smallint NOT NULL,
		ttp227 smallint NOT NULL,
		ttp228 smallint NOT NULL,
		ttp229 smallint NOT NULL,
		ttp230 smallint NOT NULL,
		ttp231 smallint NOT NULL,
		ttp232 smallint NOT NULL,
		ttp233 smallint NOT NULL,
		ttp234 smallint NOT NULL,
		ttp235 smallint NOT NULL,
		ttp236 smallint NOT NULL,
		ttp237 smallint NOT NULL,
		ttp238 smallint NOT NULL,
		ttp239 smallint NOT NULL,
		ttp240 smallint NOT NULL,
		ttp241 smallint NOT NULL,
		constraint PK_EV_EXTD primary key (loc_n, id, seq)
	);
end if;
go

grant select on DBA.ev_extd to public;
go

if not exists(select 0 from sys.systable where table_name='gs_extd' and table_type='BASE' and creator=user_id('DBA')) then
	create table DBA.gs_extd(
		loc_n         smallint          not null,
		tday          date              not null, -- LG-1454
		fs            tinyint           not null,
		ttp49 int NOT NULL,
		ttp50 int NOT NULL,
		ttp51 int NOT NULL,
		ttp52 int NOT NULL,
		ttp53 int NOT NULL,
		ttp54 int NOT NULL,
		ttp55 int NOT NULL,
		ttp56 int NOT NULL,
		ttp57 int NOT NULL,
		ttp58 int NOT NULL,
		ttp59 int NOT NULL,
		ttp60 int NOT NULL,
		ttp61 int NOT NULL,
		ttp62 int NOT NULL,
		ttp63 int NOT NULL,
		ttp64 int NOT NULL,
		ttp65 int NOT NULL,
		ttp66 int NOT NULL,
		ttp67 int NOT NULL,
		ttp68 int NOT NULL,
		ttp69 int NOT NULL,
		ttp70 int NOT NULL,
		ttp71 int NOT NULL,
		ttp72 int NOT NULL,
		ttp73 int NOT NULL,
		ttp74 int NOT NULL,
		ttp75 int NOT NULL,
		ttp76 int NOT NULL,
		ttp77 int NOT NULL,
		ttp78 int NOT NULL,
		ttp79 int NOT NULL,
		ttp80 int NOT NULL,
		ttp81 int NOT NULL,
		ttp82 int NOT NULL,
		ttp83 int NOT NULL,
		ttp84 int NOT NULL,
		ttp85 int NOT NULL,
		ttp86 int NOT NULL,
		ttp87 int NOT NULL,
		ttp88 int NOT NULL,
		ttp89 int NOT NULL,
		ttp90 int NOT NULL,
		ttp91 int NOT NULL,
		ttp92 int NOT NULL,
		ttp93 int NOT NULL,
		ttp94 int NOT NULL,
		ttp95 int NOT NULL,
		ttp96 int NOT NULL,
		ttp97 int NOT NULL,
		ttp98 int NOT NULL,
		ttp99 int NOT NULL,
		ttp100 int NOT NULL,
		ttp101 int NOT NULL,
		ttp102 int NOT NULL,
		ttp103 int NOT NULL,
		ttp104 int NOT NULL,
		ttp105 int NOT NULL,
		ttp106 int NOT NULL,
		ttp107 int NOT NULL,
		ttp108 int NOT NULL,
		ttp109 int NOT NULL,
		ttp110 int NOT NULL,
		ttp111 int NOT NULL,
		ttp112 int NOT NULL,
		ttp113 int NOT NULL,
		ttp114 int NOT NULL,
		ttp115 int NOT NULL,
		ttp116 int NOT NULL,
		ttp117 int NOT NULL,
		ttp118 int NOT NULL,
		ttp119 int NOT NULL,
		ttp120 int NOT NULL,
		ttp121 int NOT NULL,
		ttp122 int NOT NULL,
		ttp123 int NOT NULL,
		ttp124 int NOT NULL,
		ttp125 int NOT NULL,
		ttp126 int NOT NULL,
		ttp127 int NOT NULL,
		ttp128 int NOT NULL,
		ttp129 int NOT NULL,
		ttp130 int NOT NULL,
		ttp131 int NOT NULL,
		ttp132 int NOT NULL,
		ttp133 int NOT NULL,
		ttp134 int NOT NULL,
		ttp135 int NOT NULL,
		ttp136 int NOT NULL,
		ttp137 int NOT NULL,
		ttp138 int NOT NULL,
		ttp139 int NOT NULL,
		ttp140 int NOT NULL,
		ttp141 int NOT NULL,
		ttp142 int NOT NULL,
		ttp143 int NOT NULL,
		ttp144 int NOT NULL,
		ttp145 int NOT NULL,
		ttp146 int NOT NULL,
		ttp147 int NOT NULL,
		ttp148 int NOT NULL,
		ttp149 int NOT NULL,
		ttp150 int NOT NULL,
		ttp151 int NOT NULL,
		ttp152 int NOT NULL,
		ttp153 int NOT NULL,
		ttp154 int NOT NULL,
		ttp155 int NOT NULL,
		ttp156 int NOT NULL,
		ttp157 int NOT NULL,
		ttp158 int NOT NULL,
		ttp159 int NOT NULL,
		ttp160 int NOT NULL,
		ttp161 int NOT NULL,
		ttp162 int NOT NULL,
		ttp163 int NOT NULL,
		ttp164 int NOT NULL,
		ttp165 int NOT NULL,
		ttp166 int NOT NULL,
		ttp167 int NOT NULL,
		ttp168 int NOT NULL,
		ttp169 int NOT NULL,
		ttp170 int NOT NULL,
		ttp171 int NOT NULL,
		ttp172 int NOT NULL,
		ttp173 int NOT NULL,
		ttp174 int NOT NULL,
		ttp175 int NOT NULL,
		ttp176 int NOT NULL,
		ttp177 int NOT NULL,
		ttp178 int NOT NULL,
		ttp179 int NOT NULL,
		ttp180 int NOT NULL,
		ttp181 int NOT NULL,
		ttp182 int NOT NULL,
		ttp183 int NOT NULL,
		ttp184 int NOT NULL,
		ttp185 int NOT NULL,
		ttp186 int NOT NULL,
		ttp187 int NOT NULL,
		ttp188 int NOT NULL,
		ttp189 int NOT NULL,
		ttp190 int NOT NULL,
		ttp191 int NOT NULL,
		ttp192 int NOT NULL,
		ttp193 int NOT NULL,
		ttp194 int NOT NULL,
		ttp195 int NOT NULL,
		ttp196 int NOT NULL,
		ttp197 int NOT NULL,
		ttp198 int NOT NULL,
		ttp199 int NOT NULL,
		ttp200 int NOT NULL,
		ttp201 int NOT NULL,
		ttp202 int NOT NULL,
		ttp203 int NOT NULL,
		ttp204 int NOT NULL,
		ttp205 int NOT NULL,
		ttp206 int NOT NULL,
		ttp207 int NOT NULL,
		ttp208 int NOT NULL,
		ttp209 int NOT NULL,
		ttp210 int NOT NULL,
		ttp211 int NOT NULL,
		ttp212 int NOT NULL,
		ttp213 int NOT NULL,
		ttp214 int NOT NULL,
		ttp215 int NOT NULL,
		ttp216 int NOT NULL,
		ttp217 int NOT NULL,
		ttp218 int NOT NULL,
		ttp219 int NOT NULL,
		ttp220 int NOT NULL,
		ttp221 int NOT NULL,
		ttp222 int NOT NULL,
		ttp223 int NOT NULL,
		ttp224 int NOT NULL,
		ttp225 int NOT NULL,
		ttp226 int NOT NULL,
		ttp227 int NOT NULL,
		ttp228 int NOT NULL,
		ttp229 int NOT NULL,
		ttp230 int NOT NULL,
		ttp231 int NOT NULL,
		ttp232 int NOT NULL,
		ttp233 int NOT NULL,
		ttp234 int NOT NULL,
		ttp235 int NOT NULL,
		ttp236 int NOT NULL,
		ttp237 int NOT NULL,
		ttp238 int NOT NULL,
		ttp239 int NOT NULL,
		ttp240 int NOT NULL,
		ttp241 int NOT NULL,
		constraint PK_GS_EXTD primary key (loc_n, tday, fs)
	);
else
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='tday' and coltype='date') then
		alter table gs_extd drop constraint PK_GS_EXTD;
		alter table gs_extd modify tday date not null;
		alter table gs_extd add constraint PK_GS_EXTD primary key (loc_n, tday, fs);
	end if;

	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp49' and coltype='integer') then
		alter table gs_extd modify ttp49 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp50' and coltype='integer') then
		alter table gs_extd modify ttp50 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp51' and coltype='integer') then
		alter table gs_extd modify ttp51 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp52' and coltype='integer') then
		alter table gs_extd modify ttp52 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp53' and coltype='integer') then
		alter table gs_extd modify ttp53 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp54' and coltype='integer') then
		alter table gs_extd modify ttp54 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp55' and coltype='integer') then
		alter table gs_extd modify ttp55 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp56' and coltype='integer') then
		alter table gs_extd modify ttp56 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp57' and coltype='integer') then
		alter table gs_extd modify ttp57 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp58' and coltype='integer') then
		alter table gs_extd modify ttp58 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp59' and coltype='integer') then
		alter table gs_extd modify ttp59 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp60' and coltype='integer') then
		alter table gs_extd modify ttp60 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp61' and coltype='integer') then
		alter table gs_extd modify ttp61 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp62' and coltype='integer') then
		alter table gs_extd modify ttp62 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp63' and coltype='integer') then
		alter table gs_extd modify ttp63 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp64' and coltype='integer') then
		alter table gs_extd modify ttp64 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp65' and coltype='integer') then
		alter table gs_extd modify ttp65 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp66' and coltype='integer') then
		alter table gs_extd modify ttp66 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp67' and coltype='integer') then
		alter table gs_extd modify ttp67 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp68' and coltype='integer') then
		alter table gs_extd modify ttp68 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp69' and coltype='integer') then
		alter table gs_extd modify ttp69 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp70' and coltype='integer') then
		alter table gs_extd modify ttp70 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp71' and coltype='integer') then
		alter table gs_extd modify ttp71 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp72' and coltype='integer') then
		alter table gs_extd modify ttp72 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp73' and coltype='integer') then
		alter table gs_extd modify ttp73 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp74' and coltype='integer') then
		alter table gs_extd modify ttp74 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp75' and coltype='integer') then
		alter table gs_extd modify ttp75 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp76' and coltype='integer') then
		alter table gs_extd modify ttp76 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp77' and coltype='integer') then
		alter table gs_extd modify ttp77 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp78' and coltype='integer') then
		alter table gs_extd modify ttp78 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp79' and coltype='integer') then
		alter table gs_extd modify ttp79 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp80' and coltype='integer') then
		alter table gs_extd modify ttp80 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp81' and coltype='integer') then
		alter table gs_extd modify ttp81 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp82' and coltype='integer') then
		alter table gs_extd modify ttp82 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp83' and coltype='integer') then
		alter table gs_extd modify ttp83 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp84' and coltype='integer') then
		alter table gs_extd modify ttp84 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp85' and coltype='integer') then
		alter table gs_extd modify ttp85 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp86' and coltype='integer') then
		alter table gs_extd modify ttp86 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp87' and coltype='integer') then
		alter table gs_extd modify ttp87 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp88' and coltype='integer') then
		alter table gs_extd modify ttp88 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp89' and coltype='integer') then
		alter table gs_extd modify ttp89 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp90' and coltype='integer') then
		alter table gs_extd modify ttp90 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp91' and coltype='integer') then
		alter table gs_extd modify ttp91 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp92' and coltype='integer') then
		alter table gs_extd modify ttp92 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp93' and coltype='integer') then
		alter table gs_extd modify ttp93 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp94' and coltype='integer') then
		alter table gs_extd modify ttp94 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp95' and coltype='integer') then
		alter table gs_extd modify ttp95 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp96' and coltype='integer') then
		alter table gs_extd modify ttp96 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp97' and coltype='integer') then
		alter table gs_extd modify ttp97 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp98' and coltype='integer') then
		alter table gs_extd modify ttp98 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp99' and coltype='integer') then
		alter table gs_extd modify ttp99 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp100' and coltype='integer') then
		alter table gs_extd modify ttp100 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp101' and coltype='integer') then
		alter table gs_extd modify ttp101 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp102' and coltype='integer') then
		alter table gs_extd modify ttp102 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp103' and coltype='integer') then
		alter table gs_extd modify ttp103 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp104' and coltype='integer') then
		alter table gs_extd modify ttp104 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp105' and coltype='integer') then
		alter table gs_extd modify ttp105 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp106' and coltype='integer') then
		alter table gs_extd modify ttp106 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp107' and coltype='integer') then
		alter table gs_extd modify ttp107 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp108' and coltype='integer') then
		alter table gs_extd modify ttp108 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp109' and coltype='integer') then
		alter table gs_extd modify ttp109 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp110' and coltype='integer') then
		alter table gs_extd modify ttp110 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp111' and coltype='integer') then
		alter table gs_extd modify ttp111 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp112' and coltype='integer') then
		alter table gs_extd modify ttp112 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp113' and coltype='integer') then
		alter table gs_extd modify ttp113 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp114' and coltype='integer') then
		alter table gs_extd modify ttp114 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp115' and coltype='integer') then
		alter table gs_extd modify ttp115 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp116' and coltype='integer') then
		alter table gs_extd modify ttp116 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp117' and coltype='integer') then
		alter table gs_extd modify ttp117 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp118' and coltype='integer') then
		alter table gs_extd modify ttp118 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp119' and coltype='integer') then
		alter table gs_extd modify ttp119 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp120' and coltype='integer') then
		alter table gs_extd modify ttp120 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp121' and coltype='integer') then
		alter table gs_extd modify ttp121 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp122' and coltype='integer') then
		alter table gs_extd modify ttp122 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp123' and coltype='integer') then
		alter table gs_extd modify ttp123 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp124' and coltype='integer') then
		alter table gs_extd modify ttp124 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp125' and coltype='integer') then
		alter table gs_extd modify ttp125 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp126' and coltype='integer') then
		alter table gs_extd modify ttp126 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp127' and coltype='integer') then
		alter table gs_extd modify ttp127 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp128' and coltype='integer') then
		alter table gs_extd modify ttp128 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp129' and coltype='integer') then
		alter table gs_extd modify ttp129 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp130' and coltype='integer') then
		alter table gs_extd modify ttp130 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp131' and coltype='integer') then
		alter table gs_extd modify ttp131 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp132' and coltype='integer') then
		alter table gs_extd modify ttp132 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp133' and coltype='integer') then
		alter table gs_extd modify ttp133 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp134' and coltype='integer') then
		alter table gs_extd modify ttp134 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp135' and coltype='integer') then
		alter table gs_extd modify ttp135 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp136' and coltype='integer') then
		alter table gs_extd modify ttp136 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp137' and coltype='integer') then
		alter table gs_extd modify ttp137 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp138' and coltype='integer') then
		alter table gs_extd modify ttp138 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp139' and coltype='integer') then
		alter table gs_extd modify ttp139 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp140' and coltype='integer') then
		alter table gs_extd modify ttp140 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp141' and coltype='integer') then
		alter table gs_extd modify ttp141 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp142' and coltype='integer') then
		alter table gs_extd modify ttp142 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp143' and coltype='integer') then
		alter table gs_extd modify ttp143 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp144' and coltype='integer') then
		alter table gs_extd modify ttp144 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp145' and coltype='integer') then
		alter table gs_extd modify ttp145 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp146' and coltype='integer') then
		alter table gs_extd modify ttp146 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp147' and coltype='integer') then
		alter table gs_extd modify ttp147 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp148' and coltype='integer') then
		alter table gs_extd modify ttp148 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp149' and coltype='integer') then
		alter table gs_extd modify ttp149 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp150' and coltype='integer') then
		alter table gs_extd modify ttp150 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp151' and coltype='integer') then
		alter table gs_extd modify ttp151 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp152' and coltype='integer') then
		alter table gs_extd modify ttp152 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp153' and coltype='integer') then
		alter table gs_extd modify ttp153 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp154' and coltype='integer') then
		alter table gs_extd modify ttp154 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp155' and coltype='integer') then
		alter table gs_extd modify ttp155 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp156' and coltype='integer') then
		alter table gs_extd modify ttp156 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp157' and coltype='integer') then
		alter table gs_extd modify ttp157 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp158' and coltype='integer') then
		alter table gs_extd modify ttp158 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp159' and coltype='integer') then
		alter table gs_extd modify ttp159 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp160' and coltype='integer') then
		alter table gs_extd modify ttp160 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp161' and coltype='integer') then
		alter table gs_extd modify ttp161 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp162' and coltype='integer') then
		alter table gs_extd modify ttp162 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp163' and coltype='integer') then
		alter table gs_extd modify ttp163 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp164' and coltype='integer') then
		alter table gs_extd modify ttp164 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp165' and coltype='integer') then
		alter table gs_extd modify ttp165 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp166' and coltype='integer') then
		alter table gs_extd modify ttp166 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp167' and coltype='integer') then
		alter table gs_extd modify ttp167 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp168' and coltype='integer') then
		alter table gs_extd modify ttp168 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp169' and coltype='integer') then
		alter table gs_extd modify ttp169 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp170' and coltype='integer') then
		alter table gs_extd modify ttp170 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp171' and coltype='integer') then
		alter table gs_extd modify ttp171 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp172' and coltype='integer') then
		alter table gs_extd modify ttp172 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp173' and coltype='integer') then
		alter table gs_extd modify ttp173 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp174' and coltype='integer') then
		alter table gs_extd modify ttp174 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp175' and coltype='integer') then
		alter table gs_extd modify ttp175 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp176' and coltype='integer') then
		alter table gs_extd modify ttp176 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp177' and coltype='integer') then
		alter table gs_extd modify ttp177 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp178' and coltype='integer') then
		alter table gs_extd modify ttp178 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp179' and coltype='integer') then
		alter table gs_extd modify ttp179 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp180' and coltype='integer') then
		alter table gs_extd modify ttp180 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp181' and coltype='integer') then
		alter table gs_extd modify ttp181 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp182' and coltype='integer') then
		alter table gs_extd modify ttp182 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp183' and coltype='integer') then
		alter table gs_extd modify ttp183 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp184' and coltype='integer') then
		alter table gs_extd modify ttp184 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp185' and coltype='integer') then
		alter table gs_extd modify ttp185 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp186' and coltype='integer') then
		alter table gs_extd modify ttp186 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp187' and coltype='integer') then
		alter table gs_extd modify ttp187 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp188' and coltype='integer') then
		alter table gs_extd modify ttp188 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp189' and coltype='integer') then
		alter table gs_extd modify ttp189 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp190' and coltype='integer') then
		alter table gs_extd modify ttp190 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp191' and coltype='integer') then
		alter table gs_extd modify ttp191 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp192' and coltype='integer') then
		alter table gs_extd modify ttp192 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp193' and coltype='integer') then
		alter table gs_extd modify ttp193 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp194' and coltype='integer') then
		alter table gs_extd modify ttp194 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp195' and coltype='integer') then
		alter table gs_extd modify ttp195 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp196' and coltype='integer') then
		alter table gs_extd modify ttp196 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp197' and coltype='integer') then
		alter table gs_extd modify ttp197 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp198' and coltype='integer') then
		alter table gs_extd modify ttp198 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp199' and coltype='integer') then
		alter table gs_extd modify ttp199 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp200' and coltype='integer') then
		alter table gs_extd modify ttp200 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp201' and coltype='integer') then
		alter table gs_extd modify ttp201 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp202' and coltype='integer') then
		alter table gs_extd modify ttp202 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp203' and coltype='integer') then
		alter table gs_extd modify ttp203 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp204' and coltype='integer') then
		alter table gs_extd modify ttp204 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp205' and coltype='integer') then
		alter table gs_extd modify ttp205 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp206' and coltype='integer') then
		alter table gs_extd modify ttp206 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp207' and coltype='integer') then
		alter table gs_extd modify ttp207 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp208' and coltype='integer') then
		alter table gs_extd modify ttp208 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp209' and coltype='integer') then
		alter table gs_extd modify ttp209 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp210' and coltype='integer') then
		alter table gs_extd modify ttp210 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp211' and coltype='integer') then
		alter table gs_extd modify ttp211 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp212' and coltype='integer') then
		alter table gs_extd modify ttp212 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp213' and coltype='integer') then
		alter table gs_extd modify ttp213 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp214' and coltype='integer') then
		alter table gs_extd modify ttp214 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp215' and coltype='integer') then
		alter table gs_extd modify ttp215 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp216' and coltype='integer') then
		alter table gs_extd modify ttp216 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp217' and coltype='integer') then
		alter table gs_extd modify ttp217 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp218' and coltype='integer') then
		alter table gs_extd modify ttp218 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp219' and coltype='integer') then
		alter table gs_extd modify ttp219 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp220' and coltype='integer') then
		alter table gs_extd modify ttp220 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp221' and coltype='integer') then
		alter table gs_extd modify ttp221 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp222' and coltype='integer') then
		alter table gs_extd modify ttp222 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp223' and coltype='integer') then
		alter table gs_extd modify ttp223 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp224' and coltype='integer') then
		alter table gs_extd modify ttp224 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp225' and coltype='integer') then
		alter table gs_extd modify ttp225 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp226' and coltype='integer') then
		alter table gs_extd modify ttp226 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp227' and coltype='integer') then
		alter table gs_extd modify ttp227 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp228' and coltype='integer') then
		alter table gs_extd modify ttp228 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp229' and coltype='integer') then
		alter table gs_extd modify ttp229 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp230' and coltype='integer') then
		alter table gs_extd modify ttp230 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp231' and coltype='integer') then
		alter table gs_extd modify ttp231 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp232' and coltype='integer') then
		alter table gs_extd modify ttp232 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp233' and coltype='integer') then
		alter table gs_extd modify ttp233 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp234' and coltype='integer') then
		alter table gs_extd modify ttp234 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp235' and coltype='integer') then
		alter table gs_extd modify ttp235 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp236' and coltype='integer') then
		alter table gs_extd modify ttp236 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp237' and coltype='integer') then
		alter table gs_extd modify ttp237 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp238' and coltype='integer') then
		alter table gs_extd modify ttp238 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp239' and coltype='integer') then
		alter table gs_extd modify ttp239 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp240' and coltype='integer') then
		alter table gs_extd modify ttp240 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='gs_extd' and cname='ttp241' and coltype='integer') then
		alter table gs_extd modify ttp241 int not null;
	end if;
end if;
go

grant select on DBA.gs_extd to public;
go

if not exists(select 0 from sys.systable where table_name='ml_extd' and table_type='BASE' and creator=user_id('DBA')) then
	create table DBA.ml_extd(
		loc_n         smallint          not null,
		id            int               not null,
		ttp49 smallint NOT NULL,
		ttp50 smallint NOT NULL,
		ttp51 smallint NOT NULL,
		ttp52 smallint NOT NULL,
		ttp53 smallint NOT NULL,
		ttp54 smallint NOT NULL,
		ttp55 smallint NOT NULL,
		ttp56 smallint NOT NULL,
		ttp57 smallint NOT NULL,
		ttp58 smallint NOT NULL,
		ttp59 smallint NOT NULL,
		ttp60 smallint NOT NULL,
		ttp61 smallint NOT NULL,
		ttp62 smallint NOT NULL,
		ttp63 smallint NOT NULL,
		ttp64 smallint NOT NULL,
		ttp65 smallint NOT NULL,
		ttp66 smallint NOT NULL,
		ttp67 smallint NOT NULL,
		ttp68 smallint NOT NULL,
		ttp69 smallint NOT NULL,
		ttp70 smallint NOT NULL,
		ttp71 smallint NOT NULL,
		ttp72 smallint NOT NULL,
		ttp73 smallint NOT NULL,
		ttp74 smallint NOT NULL,
		ttp75 smallint NOT NULL,
		ttp76 smallint NOT NULL,
		ttp77 smallint NOT NULL,
		ttp78 smallint NOT NULL,
		ttp79 smallint NOT NULL,
		ttp80 smallint NOT NULL,
		ttp81 smallint NOT NULL,
		ttp82 smallint NOT NULL,
		ttp83 smallint NOT NULL,
		ttp84 smallint NOT NULL,
		ttp85 smallint NOT NULL,
		ttp86 smallint NOT NULL,
		ttp87 smallint NOT NULL,
		ttp88 smallint NOT NULL,
		ttp89 smallint NOT NULL,
		ttp90 smallint NOT NULL,
		ttp91 smallint NOT NULL,
		ttp92 smallint NOT NULL,
		ttp93 smallint NOT NULL,
		ttp94 smallint NOT NULL,
		ttp95 smallint NOT NULL,
		ttp96 smallint NOT NULL,
		ttp97 smallint NOT NULL,
		ttp98 smallint NOT NULL,
		ttp99 smallint NOT NULL,
		ttp100 smallint NOT NULL,
		ttp101 smallint NOT NULL,
		ttp102 smallint NOT NULL,
		ttp103 smallint NOT NULL,
		ttp104 smallint NOT NULL,
		ttp105 smallint NOT NULL,
		ttp106 smallint NOT NULL,
		ttp107 smallint NOT NULL,
		ttp108 smallint NOT NULL,
		ttp109 smallint NOT NULL,
		ttp110 smallint NOT NULL,
		ttp111 smallint NOT NULL,
		ttp112 smallint NOT NULL,
		ttp113 smallint NOT NULL,
		ttp114 smallint NOT NULL,
		ttp115 smallint NOT NULL,
		ttp116 smallint NOT NULL,
		ttp117 smallint NOT NULL,
		ttp118 smallint NOT NULL,
		ttp119 smallint NOT NULL,
		ttp120 smallint NOT NULL,
		ttp121 smallint NOT NULL,
		ttp122 smallint NOT NULL,
		ttp123 smallint NOT NULL,
		ttp124 smallint NOT NULL,
		ttp125 smallint NOT NULL,
		ttp126 smallint NOT NULL,
		ttp127 smallint NOT NULL,
		ttp128 smallint NOT NULL,
		ttp129 smallint NOT NULL,
		ttp130 smallint NOT NULL,
		ttp131 smallint NOT NULL,
		ttp132 smallint NOT NULL,
		ttp133 smallint NOT NULL,
		ttp134 smallint NOT NULL,
		ttp135 smallint NOT NULL,
		ttp136 smallint NOT NULL,
		ttp137 smallint NOT NULL,
		ttp138 smallint NOT NULL,
		ttp139 smallint NOT NULL,
		ttp140 smallint NOT NULL,
		ttp141 smallint NOT NULL,
		ttp142 smallint NOT NULL,
		ttp143 smallint NOT NULL,
		ttp144 smallint NOT NULL,
		ttp145 smallint NOT NULL,
		ttp146 smallint NOT NULL,
		ttp147 smallint NOT NULL,
		ttp148 smallint NOT NULL,
		ttp149 smallint NOT NULL,
		ttp150 smallint NOT NULL,
		ttp151 smallint NOT NULL,
		ttp152 smallint NOT NULL,
		ttp153 smallint NOT NULL,
		ttp154 smallint NOT NULL,
		ttp155 smallint NOT NULL,
		ttp156 smallint NOT NULL,
		ttp157 smallint NOT NULL,
		ttp158 smallint NOT NULL,
		ttp159 smallint NOT NULL,
		ttp160 smallint NOT NULL,
		ttp161 smallint NOT NULL,
		ttp162 smallint NOT NULL,
		ttp163 smallint NOT NULL,
		ttp164 smallint NOT NULL,
		ttp165 smallint NOT NULL,
		ttp166 smallint NOT NULL,
		ttp167 smallint NOT NULL,
		ttp168 smallint NOT NULL,
		ttp169 smallint NOT NULL,
		ttp170 smallint NOT NULL,
		ttp171 smallint NOT NULL,
		ttp172 smallint NOT NULL,
		ttp173 smallint NOT NULL,
		ttp174 smallint NOT NULL,
		ttp175 smallint NOT NULL,
		ttp176 smallint NOT NULL,
		ttp177 smallint NOT NULL,
		ttp178 smallint NOT NULL,
		ttp179 smallint NOT NULL,
		ttp180 smallint NOT NULL,
		ttp181 smallint NOT NULL,
		ttp182 smallint NOT NULL,
		ttp183 smallint NOT NULL,
		ttp184 smallint NOT NULL,
		ttp185 smallint NOT NULL,
		ttp186 smallint NOT NULL,
		ttp187 smallint NOT NULL,
		ttp188 smallint NOT NULL,
		ttp189 smallint NOT NULL,
		ttp190 smallint NOT NULL,
		ttp191 smallint NOT NULL,
		ttp192 smallint NOT NULL,
		ttp193 smallint NOT NULL,
		ttp194 smallint NOT NULL,
		ttp195 smallint NOT NULL,
		ttp196 smallint NOT NULL,
		ttp197 smallint NOT NULL,
		ttp198 smallint NOT NULL,
		ttp199 smallint NOT NULL,
		ttp200 smallint NOT NULL,
		ttp201 smallint NOT NULL,
		ttp202 smallint NOT NULL,
		ttp203 smallint NOT NULL,
		ttp204 smallint NOT NULL,
		ttp205 smallint NOT NULL,
		ttp206 smallint NOT NULL,
		ttp207 smallint NOT NULL,
		ttp208 smallint NOT NULL,
		ttp209 smallint NOT NULL,
		ttp210 smallint NOT NULL,
		ttp211 smallint NOT NULL,
		ttp212 smallint NOT NULL,
		ttp213 smallint NOT NULL,
		ttp214 smallint NOT NULL,
		ttp215 smallint NOT NULL,
		ttp216 smallint NOT NULL,
		ttp217 smallint NOT NULL,
		ttp218 smallint NOT NULL,
		ttp219 smallint NOT NULL,
		ttp220 smallint NOT NULL,
		ttp221 smallint NOT NULL,
		ttp222 smallint NOT NULL,
		ttp223 smallint NOT NULL,
		ttp224 smallint NOT NULL,
		ttp225 smallint NOT NULL,
		ttp226 smallint NOT NULL,
		ttp227 smallint NOT NULL,
		ttp228 smallint NOT NULL,
		ttp229 smallint NOT NULL,
		ttp230 smallint NOT NULL,
		ttp231 smallint NOT NULL,
		ttp232 smallint NOT NULL,
		ttp233 smallint NOT NULL,
		ttp234 smallint NOT NULL,
		ttp235 smallint NOT NULL,
		ttp236 smallint NOT NULL,
		ttp237 smallint NOT NULL,
		ttp238 smallint NOT NULL,
		ttp239 smallint NOT NULL,
		ttp240 smallint NOT NULL,
		ttp241 smallint NOT NULL,
		constraint PK_ML_EXTD primary key (loc_n, id)
	);
end if;
go

grant select on DBA.ml_extd to public;
go

if not exists(select 0 from sys.systable where table_name='mrtesum_extd' and table_type='BASE' and creator=user_id('DBA')) then
	create table DBA.mrtesum_extd(
		loc_n       smallint          not null,
		mday        date              not null, -- LG-1454
		route       int               not null,
		ttp49 int NOT NULL,
		ttp50 int NOT NULL,
		ttp51 int NOT NULL,
		ttp52 int NOT NULL,
		ttp53 int NOT NULL,
		ttp54 int NOT NULL,
		ttp55 int NOT NULL,
		ttp56 int NOT NULL,
		ttp57 int NOT NULL,
		ttp58 int NOT NULL,
		ttp59 int NOT NULL,
		ttp60 int NOT NULL,
		ttp61 int NOT NULL,
		ttp62 int NOT NULL,
		ttp63 int NOT NULL,
		ttp64 int NOT NULL,
		ttp65 int NOT NULL,
		ttp66 int NOT NULL,
		ttp67 int NOT NULL,
		ttp68 int NOT NULL,
		ttp69 int NOT NULL,
		ttp70 int NOT NULL,
		ttp71 int NOT NULL,
		ttp72 int NOT NULL,
		ttp73 int NOT NULL,
		ttp74 int NOT NULL,
		ttp75 int NOT NULL,
		ttp76 int NOT NULL,
		ttp77 int NOT NULL,
		ttp78 int NOT NULL,
		ttp79 int NOT NULL,
		ttp80 int NOT NULL,
		ttp81 int NOT NULL,
		ttp82 int NOT NULL,
		ttp83 int NOT NULL,
		ttp84 int NOT NULL,
		ttp85 int NOT NULL,
		ttp86 int NOT NULL,
		ttp87 int NOT NULL,
		ttp88 int NOT NULL,
		ttp89 int NOT NULL,
		ttp90 int NOT NULL,
		ttp91 int NOT NULL,
		ttp92 int NOT NULL,
		ttp93 int NOT NULL,
		ttp94 int NOT NULL,
		ttp95 int NOT NULL,
		ttp96 int NOT NULL,
		ttp97 int NOT NULL,
		ttp98 int NOT NULL,
		ttp99 int NOT NULL,
		ttp100 int NOT NULL,
		ttp101 int NOT NULL,
		ttp102 int NOT NULL,
		ttp103 int NOT NULL,
		ttp104 int NOT NULL,
		ttp105 int NOT NULL,
		ttp106 int NOT NULL,
		ttp107 int NOT NULL,
		ttp108 int NOT NULL,
		ttp109 int NOT NULL,
		ttp110 int NOT NULL,
		ttp111 int NOT NULL,
		ttp112 int NOT NULL,
		ttp113 int NOT NULL,
		ttp114 int NOT NULL,
		ttp115 int NOT NULL,
		ttp116 int NOT NULL,
		ttp117 int NOT NULL,
		ttp118 int NOT NULL,
		ttp119 int NOT NULL,
		ttp120 int NOT NULL,
		ttp121 int NOT NULL,
		ttp122 int NOT NULL,
		ttp123 int NOT NULL,
		ttp124 int NOT NULL,
		ttp125 int NOT NULL,
		ttp126 int NOT NULL,
		ttp127 int NOT NULL,
		ttp128 int NOT NULL,
		ttp129 int NOT NULL,
		ttp130 int NOT NULL,
		ttp131 int NOT NULL,
		ttp132 int NOT NULL,
		ttp133 int NOT NULL,
		ttp134 int NOT NULL,
		ttp135 int NOT NULL,
		ttp136 int NOT NULL,
		ttp137 int NOT NULL,
		ttp138 int NOT NULL,
		ttp139 int NOT NULL,
		ttp140 int NOT NULL,
		ttp141 int NOT NULL,
		ttp142 int NOT NULL,
		ttp143 int NOT NULL,
		ttp144 int NOT NULL,
		ttp145 int NOT NULL,
		ttp146 int NOT NULL,
		ttp147 int NOT NULL,
		ttp148 int NOT NULL,
		ttp149 int NOT NULL,
		ttp150 int NOT NULL,
		ttp151 int NOT NULL,
		ttp152 int NOT NULL,
		ttp153 int NOT NULL,
		ttp154 int NOT NULL,
		ttp155 int NOT NULL,
		ttp156 int NOT NULL,
		ttp157 int NOT NULL,
		ttp158 int NOT NULL,
		ttp159 int NOT NULL,
		ttp160 int NOT NULL,
		ttp161 int NOT NULL,
		ttp162 int NOT NULL,
		ttp163 int NOT NULL,
		ttp164 int NOT NULL,
		ttp165 int NOT NULL,
		ttp166 int NOT NULL,
		ttp167 int NOT NULL,
		ttp168 int NOT NULL,
		ttp169 int NOT NULL,
		ttp170 int NOT NULL,
		ttp171 int NOT NULL,
		ttp172 int NOT NULL,
		ttp173 int NOT NULL,
		ttp174 int NOT NULL,
		ttp175 int NOT NULL,
		ttp176 int NOT NULL,
		ttp177 int NOT NULL,
		ttp178 int NOT NULL,
		ttp179 int NOT NULL,
		ttp180 int NOT NULL,
		ttp181 int NOT NULL,
		ttp182 int NOT NULL,
		ttp183 int NOT NULL,
		ttp184 int NOT NULL,
		ttp185 int NOT NULL,
		ttp186 int NOT NULL,
		ttp187 int NOT NULL,
		ttp188 int NOT NULL,
		ttp189 int NOT NULL,
		ttp190 int NOT NULL,
		ttp191 int NOT NULL,
		ttp192 int NOT NULL,
		ttp193 int NOT NULL,
		ttp194 int NOT NULL,
		ttp195 int NOT NULL,
		ttp196 int NOT NULL,
		ttp197 int NOT NULL,
		ttp198 int NOT NULL,
		ttp199 int NOT NULL,
		ttp200 int NOT NULL,
		ttp201 int NOT NULL,
		ttp202 int NOT NULL,
		ttp203 int NOT NULL,
		ttp204 int NOT NULL,
		ttp205 int NOT NULL,
		ttp206 int NOT NULL,
		ttp207 int NOT NULL,
		ttp208 int NOT NULL,
		ttp209 int NOT NULL,
		ttp210 int NOT NULL,
		ttp211 int NOT NULL,
		ttp212 int NOT NULL,
		ttp213 int NOT NULL,
		ttp214 int NOT NULL,
		ttp215 int NOT NULL,
		ttp216 int NOT NULL,
		ttp217 int NOT NULL,
		ttp218 int NOT NULL,
		ttp219 int NOT NULL,
		ttp220 int NOT NULL,
		ttp221 int NOT NULL,
		ttp222 int NOT NULL,
		ttp223 int NOT NULL,
		ttp224 int NOT NULL,
		ttp225 int NOT NULL,
		ttp226 int NOT NULL,
		ttp227 int NOT NULL,
		ttp228 int NOT NULL,
		ttp229 int NOT NULL,
		ttp230 int NOT NULL,
		ttp231 int NOT NULL,
		ttp232 int NOT NULL,
		ttp233 int NOT NULL,
		ttp234 int NOT NULL,
		ttp235 int NOT NULL,
		ttp236 int NOT NULL,
		ttp237 int NOT NULL,
		ttp238 int NOT NULL,
		ttp239 int NOT NULL,
		ttp240 int NOT NULL,
		ttp241 int NOT NULL,
		constraint PK_MRTESUM_EXTD primary key (loc_n, mday, route)
	);
elseif not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='mday' and coltype='date') then
		alter table mrtesum_extd drop constraint PK_MRTESUM_EXTD;
		alter table mrtesum_extd modify mday date not null;
		alter table mrtesum_extd add constraint PK_MRTESUM_EXTD primary key (loc_n, mday, route);
	end if;

	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp49' and coltype='integer') then
		alter table mrtesum_extd modify ttp49 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp50' and coltype='integer') then
		alter table mrtesum_extd modify ttp50 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp51' and coltype='integer') then
		alter table mrtesum_extd modify ttp51 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp52' and coltype='integer') then
		alter table mrtesum_extd modify ttp52 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp53' and coltype='integer') then
		alter table mrtesum_extd modify ttp53 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp54' and coltype='integer') then
		alter table mrtesum_extd modify ttp54 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp55' and coltype='integer') then
		alter table mrtesum_extd modify ttp55 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp56' and coltype='integer') then
		alter table mrtesum_extd modify ttp56 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp57' and coltype='integer') then
		alter table mrtesum_extd modify ttp57 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp58' and coltype='integer') then
		alter table mrtesum_extd modify ttp58 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp59' and coltype='integer') then
		alter table mrtesum_extd modify ttp59 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp60' and coltype='integer') then
		alter table mrtesum_extd modify ttp60 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp61' and coltype='integer') then
		alter table mrtesum_extd modify ttp61 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp62' and coltype='integer') then
		alter table mrtesum_extd modify ttp62 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp63' and coltype='integer') then
		alter table mrtesum_extd modify ttp63 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp64' and coltype='integer') then
		alter table mrtesum_extd modify ttp64 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp65' and coltype='integer') then
		alter table mrtesum_extd modify ttp65 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp66' and coltype='integer') then
		alter table mrtesum_extd modify ttp66 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp67' and coltype='integer') then
		alter table mrtesum_extd modify ttp67 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp68' and coltype='integer') then
		alter table mrtesum_extd modify ttp68 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp69' and coltype='integer') then
		alter table mrtesum_extd modify ttp69 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp70' and coltype='integer') then
		alter table mrtesum_extd modify ttp70 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp71' and coltype='integer') then
		alter table mrtesum_extd modify ttp71 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp72' and coltype='integer') then
		alter table mrtesum_extd modify ttp72 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp73' and coltype='integer') then
		alter table mrtesum_extd modify ttp73 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp74' and coltype='integer') then
		alter table mrtesum_extd modify ttp74 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp75' and coltype='integer') then
		alter table mrtesum_extd modify ttp75 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp76' and coltype='integer') then
		alter table mrtesum_extd modify ttp76 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp77' and coltype='integer') then
		alter table mrtesum_extd modify ttp77 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp78' and coltype='integer') then
		alter table mrtesum_extd modify ttp78 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp79' and coltype='integer') then
		alter table mrtesum_extd modify ttp79 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp80' and coltype='integer') then
		alter table mrtesum_extd modify ttp80 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp81' and coltype='integer') then
		alter table mrtesum_extd modify ttp81 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp82' and coltype='integer') then
		alter table mrtesum_extd modify ttp82 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp83' and coltype='integer') then
		alter table mrtesum_extd modify ttp83 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp84' and coltype='integer') then
		alter table mrtesum_extd modify ttp84 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp85' and coltype='integer') then
		alter table mrtesum_extd modify ttp85 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp86' and coltype='integer') then
		alter table mrtesum_extd modify ttp86 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp87' and coltype='integer') then
		alter table mrtesum_extd modify ttp87 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp88' and coltype='integer') then
		alter table mrtesum_extd modify ttp88 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp89' and coltype='integer') then
		alter table mrtesum_extd modify ttp89 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp90' and coltype='integer') then
		alter table mrtesum_extd modify ttp90 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp91' and coltype='integer') then
		alter table mrtesum_extd modify ttp91 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp92' and coltype='integer') then
		alter table mrtesum_extd modify ttp92 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp93' and coltype='integer') then
		alter table mrtesum_extd modify ttp93 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp94' and coltype='integer') then
		alter table mrtesum_extd modify ttp94 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp95' and coltype='integer') then
		alter table mrtesum_extd modify ttp95 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp96' and coltype='integer') then
		alter table mrtesum_extd modify ttp96 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp97' and coltype='integer') then
		alter table mrtesum_extd modify ttp97 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp98' and coltype='integer') then
		alter table mrtesum_extd modify ttp98 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp99' and coltype='integer') then
		alter table mrtesum_extd modify ttp99 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp100' and coltype='integer') then
		alter table mrtesum_extd modify ttp100 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp101' and coltype='integer') then
		alter table mrtesum_extd modify ttp101 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp102' and coltype='integer') then
		alter table mrtesum_extd modify ttp102 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp103' and coltype='integer') then
		alter table mrtesum_extd modify ttp103 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp104' and coltype='integer') then
		alter table mrtesum_extd modify ttp104 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp105' and coltype='integer') then
		alter table mrtesum_extd modify ttp105 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp106' and coltype='integer') then
		alter table mrtesum_extd modify ttp106 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp107' and coltype='integer') then
		alter table mrtesum_extd modify ttp107 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp108' and coltype='integer') then
		alter table mrtesum_extd modify ttp108 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp109' and coltype='integer') then
		alter table mrtesum_extd modify ttp109 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp110' and coltype='integer') then
		alter table mrtesum_extd modify ttp110 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp111' and coltype='integer') then
		alter table mrtesum_extd modify ttp111 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp112' and coltype='integer') then
		alter table mrtesum_extd modify ttp112 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp113' and coltype='integer') then
		alter table mrtesum_extd modify ttp113 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp114' and coltype='integer') then
		alter table mrtesum_extd modify ttp114 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp115' and coltype='integer') then
		alter table mrtesum_extd modify ttp115 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp116' and coltype='integer') then
		alter table mrtesum_extd modify ttp116 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp117' and coltype='integer') then
		alter table mrtesum_extd modify ttp117 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp118' and coltype='integer') then
		alter table mrtesum_extd modify ttp118 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp119' and coltype='integer') then
		alter table mrtesum_extd modify ttp119 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp120' and coltype='integer') then
		alter table mrtesum_extd modify ttp120 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp121' and coltype='integer') then
		alter table mrtesum_extd modify ttp121 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp122' and coltype='integer') then
		alter table mrtesum_extd modify ttp122 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp123' and coltype='integer') then
		alter table mrtesum_extd modify ttp123 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp124' and coltype='integer') then
		alter table mrtesum_extd modify ttp124 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp125' and coltype='integer') then
		alter table mrtesum_extd modify ttp125 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp126' and coltype='integer') then
		alter table mrtesum_extd modify ttp126 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp127' and coltype='integer') then
		alter table mrtesum_extd modify ttp127 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp128' and coltype='integer') then
		alter table mrtesum_extd modify ttp128 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp129' and coltype='integer') then
		alter table mrtesum_extd modify ttp129 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp130' and coltype='integer') then
		alter table mrtesum_extd modify ttp130 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp131' and coltype='integer') then
		alter table mrtesum_extd modify ttp131 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp132' and coltype='integer') then
		alter table mrtesum_extd modify ttp132 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp133' and coltype='integer') then
		alter table mrtesum_extd modify ttp133 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp134' and coltype='integer') then
		alter table mrtesum_extd modify ttp134 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp135' and coltype='integer') then
		alter table mrtesum_extd modify ttp135 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp136' and coltype='integer') then
		alter table mrtesum_extd modify ttp136 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp137' and coltype='integer') then
		alter table mrtesum_extd modify ttp137 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp138' and coltype='integer') then
		alter table mrtesum_extd modify ttp138 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp139' and coltype='integer') then
		alter table mrtesum_extd modify ttp139 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp140' and coltype='integer') then
		alter table mrtesum_extd modify ttp140 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp141' and coltype='integer') then
		alter table mrtesum_extd modify ttp141 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp142' and coltype='integer') then
		alter table mrtesum_extd modify ttp142 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp143' and coltype='integer') then
		alter table mrtesum_extd modify ttp143 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp144' and coltype='integer') then
		alter table mrtesum_extd modify ttp144 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp145' and coltype='integer') then
		alter table mrtesum_extd modify ttp145 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp146' and coltype='integer') then
		alter table mrtesum_extd modify ttp146 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp147' and coltype='integer') then
		alter table mrtesum_extd modify ttp147 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp148' and coltype='integer') then
		alter table mrtesum_extd modify ttp148 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp149' and coltype='integer') then
		alter table mrtesum_extd modify ttp149 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp150' and coltype='integer') then
		alter table mrtesum_extd modify ttp150 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp151' and coltype='integer') then
		alter table mrtesum_extd modify ttp151 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp152' and coltype='integer') then
		alter table mrtesum_extd modify ttp152 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp153' and coltype='integer') then
		alter table mrtesum_extd modify ttp153 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp154' and coltype='integer') then
		alter table mrtesum_extd modify ttp154 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp155' and coltype='integer') then
		alter table mrtesum_extd modify ttp155 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp156' and coltype='integer') then
		alter table mrtesum_extd modify ttp156 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp157' and coltype='integer') then
		alter table mrtesum_extd modify ttp157 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp158' and coltype='integer') then
		alter table mrtesum_extd modify ttp158 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp159' and coltype='integer') then
		alter table mrtesum_extd modify ttp159 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp160' and coltype='integer') then
		alter table mrtesum_extd modify ttp160 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp161' and coltype='integer') then
		alter table mrtesum_extd modify ttp161 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp162' and coltype='integer') then
		alter table mrtesum_extd modify ttp162 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp163' and coltype='integer') then
		alter table mrtesum_extd modify ttp163 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp164' and coltype='integer') then
		alter table mrtesum_extd modify ttp164 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp165' and coltype='integer') then
		alter table mrtesum_extd modify ttp165 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp166' and coltype='integer') then
		alter table mrtesum_extd modify ttp166 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp167' and coltype='integer') then
		alter table mrtesum_extd modify ttp167 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp168' and coltype='integer') then
		alter table mrtesum_extd modify ttp168 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp169' and coltype='integer') then
		alter table mrtesum_extd modify ttp169 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp170' and coltype='integer') then
		alter table mrtesum_extd modify ttp170 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp171' and coltype='integer') then
		alter table mrtesum_extd modify ttp171 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp172' and coltype='integer') then
		alter table mrtesum_extd modify ttp172 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp173' and coltype='integer') then
		alter table mrtesum_extd modify ttp173 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp174' and coltype='integer') then
		alter table mrtesum_extd modify ttp174 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp175' and coltype='integer') then
		alter table mrtesum_extd modify ttp175 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp176' and coltype='integer') then
		alter table mrtesum_extd modify ttp176 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp177' and coltype='integer') then
		alter table mrtesum_extd modify ttp177 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp178' and coltype='integer') then
		alter table mrtesum_extd modify ttp178 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp179' and coltype='integer') then
		alter table mrtesum_extd modify ttp179 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp180' and coltype='integer') then
		alter table mrtesum_extd modify ttp180 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp181' and coltype='integer') then
		alter table mrtesum_extd modify ttp181 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp182' and coltype='integer') then
		alter table mrtesum_extd modify ttp182 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp183' and coltype='integer') then
		alter table mrtesum_extd modify ttp183 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp184' and coltype='integer') then
		alter table mrtesum_extd modify ttp184 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp185' and coltype='integer') then
		alter table mrtesum_extd modify ttp185 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp186' and coltype='integer') then
		alter table mrtesum_extd modify ttp186 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp187' and coltype='integer') then
		alter table mrtesum_extd modify ttp187 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp188' and coltype='integer') then
		alter table mrtesum_extd modify ttp188 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp189' and coltype='integer') then
		alter table mrtesum_extd modify ttp189 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp190' and coltype='integer') then
		alter table mrtesum_extd modify ttp190 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp191' and coltype='integer') then
		alter table mrtesum_extd modify ttp191 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp192' and coltype='integer') then
		alter table mrtesum_extd modify ttp192 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp193' and coltype='integer') then
		alter table mrtesum_extd modify ttp193 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp194' and coltype='integer') then
		alter table mrtesum_extd modify ttp194 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp195' and coltype='integer') then
		alter table mrtesum_extd modify ttp195 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp196' and coltype='integer') then
		alter table mrtesum_extd modify ttp196 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp197' and coltype='integer') then
		alter table mrtesum_extd modify ttp197 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp198' and coltype='integer') then
		alter table mrtesum_extd modify ttp198 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp199' and coltype='integer') then
		alter table mrtesum_extd modify ttp199 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp200' and coltype='integer') then
		alter table mrtesum_extd modify ttp200 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp201' and coltype='integer') then
		alter table mrtesum_extd modify ttp201 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp202' and coltype='integer') then
		alter table mrtesum_extd modify ttp202 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp203' and coltype='integer') then
		alter table mrtesum_extd modify ttp203 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp204' and coltype='integer') then
		alter table mrtesum_extd modify ttp204 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp205' and coltype='integer') then
		alter table mrtesum_extd modify ttp205 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp206' and coltype='integer') then
		alter table mrtesum_extd modify ttp206 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp207' and coltype='integer') then
		alter table mrtesum_extd modify ttp207 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp208' and coltype='integer') then
		alter table mrtesum_extd modify ttp208 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp209' and coltype='integer') then
		alter table mrtesum_extd modify ttp209 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp210' and coltype='integer') then
		alter table mrtesum_extd modify ttp210 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp211' and coltype='integer') then
		alter table mrtesum_extd modify ttp211 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp212' and coltype='integer') then
		alter table mrtesum_extd modify ttp212 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp213' and coltype='integer') then
		alter table mrtesum_extd modify ttp213 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp214' and coltype='integer') then
		alter table mrtesum_extd modify ttp214 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp215' and coltype='integer') then
		alter table mrtesum_extd modify ttp215 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp216' and coltype='integer') then
		alter table mrtesum_extd modify ttp216 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp217' and coltype='integer') then
		alter table mrtesum_extd modify ttp217 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp218' and coltype='integer') then
		alter table mrtesum_extd modify ttp218 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp219' and coltype='integer') then
		alter table mrtesum_extd modify ttp219 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp220' and coltype='integer') then
		alter table mrtesum_extd modify ttp220 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp221' and coltype='integer') then
		alter table mrtesum_extd modify ttp221 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp222' and coltype='integer') then
		alter table mrtesum_extd modify ttp222 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp223' and coltype='integer') then
		alter table mrtesum_extd modify ttp223 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp224' and coltype='integer') then
		alter table mrtesum_extd modify ttp224 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp225' and coltype='integer') then
		alter table mrtesum_extd modify ttp225 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp226' and coltype='integer') then
		alter table mrtesum_extd modify ttp226 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp227' and coltype='integer') then
		alter table mrtesum_extd modify ttp227 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp228' and coltype='integer') then
		alter table mrtesum_extd modify ttp228 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp229' and coltype='integer') then
		alter table mrtesum_extd modify ttp229 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp230' and coltype='integer') then
		alter table mrtesum_extd modify ttp230 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp231' and coltype='integer') then
		alter table mrtesum_extd modify ttp231 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp232' and coltype='integer') then
		alter table mrtesum_extd modify ttp232 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp233' and coltype='integer') then
		alter table mrtesum_extd modify ttp233 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp234' and coltype='integer') then
		alter table mrtesum_extd modify ttp234 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp235' and coltype='integer') then
		alter table mrtesum_extd modify ttp235 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp236' and coltype='integer') then
		alter table mrtesum_extd modify ttp236 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp237' and coltype='integer') then
		alter table mrtesum_extd modify ttp237 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp238' and coltype='integer') then
		alter table mrtesum_extd modify ttp238 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp239' and coltype='integer') then
		alter table mrtesum_extd modify ttp239 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp240' and coltype='integer') then
		alter table mrtesum_extd modify ttp240 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='mrtesum_extd' and cname='ttp241' and coltype='integer') then
		alter table mrtesum_extd modify ttp241 int not null;
	end if;
go

grant select on DBA.mrtesum_extd to public;
go

if not exists(select 0 from sys.systable where table_name='rtesum_extd' and table_type='BASE' and creator=user_id('DBA')) then
	create table DBA.rtesum_extd(
		loc_n       smallint          not null,
		tday        date              not null, -- LG-1454
		route       int               not null,
		ttp49 int NOT NULL,
		ttp50 int NOT NULL,
		ttp51 int NOT NULL,
		ttp52 int NOT NULL,
		ttp53 int NOT NULL,
		ttp54 int NOT NULL,
		ttp55 int NOT NULL,
		ttp56 int NOT NULL,
		ttp57 int NOT NULL,
		ttp58 int NOT NULL,
		ttp59 int NOT NULL,
		ttp60 int NOT NULL,
		ttp61 int NOT NULL,
		ttp62 int NOT NULL,
		ttp63 int NOT NULL,
		ttp64 int NOT NULL,
		ttp65 int NOT NULL,
		ttp66 int NOT NULL,
		ttp67 int NOT NULL,
		ttp68 int NOT NULL,
		ttp69 int NOT NULL,
		ttp70 int NOT NULL,
		ttp71 int NOT NULL,
		ttp72 int NOT NULL,
		ttp73 int NOT NULL,
		ttp74 int NOT NULL,
		ttp75 int NOT NULL,
		ttp76 int NOT NULL,
		ttp77 int NOT NULL,
		ttp78 int NOT NULL,
		ttp79 int NOT NULL,
		ttp80 int NOT NULL,
		ttp81 int NOT NULL,
		ttp82 int NOT NULL,
		ttp83 int NOT NULL,
		ttp84 int NOT NULL,
		ttp85 int NOT NULL,
		ttp86 int NOT NULL,
		ttp87 int NOT NULL,
		ttp88 int NOT NULL,
		ttp89 int NOT NULL,
		ttp90 int NOT NULL,
		ttp91 int NOT NULL,
		ttp92 int NOT NULL,
		ttp93 int NOT NULL,
		ttp94 int NOT NULL,
		ttp95 int NOT NULL,
		ttp96 int NOT NULL,
		ttp97 int NOT NULL,
		ttp98 int NOT NULL,
		ttp99 int NOT NULL,
		ttp100 int NOT NULL,
		ttp101 int NOT NULL,
		ttp102 int NOT NULL,
		ttp103 int NOT NULL,
		ttp104 int NOT NULL,
		ttp105 int NOT NULL,
		ttp106 int NOT NULL,
		ttp107 int NOT NULL,
		ttp108 int NOT NULL,
		ttp109 int NOT NULL,
		ttp110 int NOT NULL,
		ttp111 int NOT NULL,
		ttp112 int NOT NULL,
		ttp113 int NOT NULL,
		ttp114 int NOT NULL,
		ttp115 int NOT NULL,
		ttp116 int NOT NULL,
		ttp117 int NOT NULL,
		ttp118 int NOT NULL,
		ttp119 int NOT NULL,
		ttp120 int NOT NULL,
		ttp121 int NOT NULL,
		ttp122 int NOT NULL,
		ttp123 int NOT NULL,
		ttp124 int NOT NULL,
		ttp125 int NOT NULL,
		ttp126 int NOT NULL,
		ttp127 int NOT NULL,
		ttp128 int NOT NULL,
		ttp129 int NOT NULL,
		ttp130 int NOT NULL,
		ttp131 int NOT NULL,
		ttp132 int NOT NULL,
		ttp133 int NOT NULL,
		ttp134 int NOT NULL,
		ttp135 int NOT NULL,
		ttp136 int NOT NULL,
		ttp137 int NOT NULL,
		ttp138 int NOT NULL,
		ttp139 int NOT NULL,
		ttp140 int NOT NULL,
		ttp141 int NOT NULL,
		ttp142 int NOT NULL,
		ttp143 int NOT NULL,
		ttp144 int NOT NULL,
		ttp145 int NOT NULL,
		ttp146 int NOT NULL,
		ttp147 int NOT NULL,
		ttp148 int NOT NULL,
		ttp149 int NOT NULL,
		ttp150 int NOT NULL,
		ttp151 int NOT NULL,
		ttp152 int NOT NULL,
		ttp153 int NOT NULL,
		ttp154 int NOT NULL,
		ttp155 int NOT NULL,
		ttp156 int NOT NULL,
		ttp157 int NOT NULL,
		ttp158 int NOT NULL,
		ttp159 int NOT NULL,
		ttp160 int NOT NULL,
		ttp161 int NOT NULL,
		ttp162 int NOT NULL,
		ttp163 int NOT NULL,
		ttp164 int NOT NULL,
		ttp165 int NOT NULL,
		ttp166 int NOT NULL,
		ttp167 int NOT NULL,
		ttp168 int NOT NULL,
		ttp169 int NOT NULL,
		ttp170 int NOT NULL,
		ttp171 int NOT NULL,
		ttp172 int NOT NULL,
		ttp173 int NOT NULL,
		ttp174 int NOT NULL,
		ttp175 int NOT NULL,
		ttp176 int NOT NULL,
		ttp177 int NOT NULL,
		ttp178 int NOT NULL,
		ttp179 int NOT NULL,
		ttp180 int NOT NULL,
		ttp181 int NOT NULL,
		ttp182 int NOT NULL,
		ttp183 int NOT NULL,
		ttp184 int NOT NULL,
		ttp185 int NOT NULL,
		ttp186 int NOT NULL,
		ttp187 int NOT NULL,
		ttp188 int NOT NULL,
		ttp189 int NOT NULL,
		ttp190 int NOT NULL,
		ttp191 int NOT NULL,
		ttp192 int NOT NULL,
		ttp193 int NOT NULL,
		ttp194 int NOT NULL,
		ttp195 int NOT NULL,
		ttp196 int NOT NULL,
		ttp197 int NOT NULL,
		ttp198 int NOT NULL,
		ttp199 int NOT NULL,
		ttp200 int NOT NULL,
		ttp201 int NOT NULL,
		ttp202 int NOT NULL,
		ttp203 int NOT NULL,
		ttp204 int NOT NULL,
		ttp205 int NOT NULL,
		ttp206 int NOT NULL,
		ttp207 int NOT NULL,
		ttp208 int NOT NULL,
		ttp209 int NOT NULL,
		ttp210 int NOT NULL,
		ttp211 int NOT NULL,
		ttp212 int NOT NULL,
		ttp213 int NOT NULL,
		ttp214 int NOT NULL,
		ttp215 int NOT NULL,
		ttp216 int NOT NULL,
		ttp217 int NOT NULL,
		ttp218 int NOT NULL,
		ttp219 int NOT NULL,
		ttp220 int NOT NULL,
		ttp221 int NOT NULL,
		ttp222 int NOT NULL,
		ttp223 int NOT NULL,
		ttp224 int NOT NULL,
		ttp225 int NOT NULL,
		ttp226 int NOT NULL,
		ttp227 int NOT NULL,
		ttp228 int NOT NULL,
		ttp229 int NOT NULL,
		ttp230 int NOT NULL,
		ttp231 int NOT NULL,
		ttp232 int NOT NULL,
		ttp233 int NOT NULL,
		ttp234 int NOT NULL,
		ttp235 int NOT NULL,
		ttp236 int NOT NULL,
		ttp237 int NOT NULL,
		ttp238 int NOT NULL,
		ttp239 int NOT NULL,
		ttp240 int NOT NULL,
		ttp241 int NOT NULL,
		constraint PK_RTESUM_EXTD primary key (loc_n, tday, route)
	);
else
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='tday' and coltype='date') then
		alter table rtesum_extd drop constraint PK_RTESUM_EXTD;
		alter table rtesum_extd modify tday date not null;
		alter table rtesum_extd add constraint PK_RTESUM_EXTD primary key (loc_n, tday, route);
	end if;

	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp49' and coltype='integer') then
		alter table rtesum_extd modify ttp49 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp50' and coltype='integer') then
		alter table rtesum_extd modify ttp50 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp51' and coltype='integer') then
		alter table rtesum_extd modify ttp51 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp52' and coltype='integer') then
		alter table rtesum_extd modify ttp52 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp53' and coltype='integer') then
		alter table rtesum_extd modify ttp53 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp54' and coltype='integer') then
		alter table rtesum_extd modify ttp54 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp55' and coltype='integer') then
		alter table rtesum_extd modify ttp55 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp56' and coltype='integer') then
		alter table rtesum_extd modify ttp56 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp57' and coltype='integer') then
		alter table rtesum_extd modify ttp57 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp58' and coltype='integer') then
		alter table rtesum_extd modify ttp58 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp59' and coltype='integer') then
		alter table rtesum_extd modify ttp59 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp60' and coltype='integer') then
		alter table rtesum_extd modify ttp60 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp61' and coltype='integer') then
		alter table rtesum_extd modify ttp61 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp62' and coltype='integer') then
		alter table rtesum_extd modify ttp62 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp63' and coltype='integer') then
		alter table rtesum_extd modify ttp63 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp64' and coltype='integer') then
		alter table rtesum_extd modify ttp64 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp65' and coltype='integer') then
		alter table rtesum_extd modify ttp65 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp66' and coltype='integer') then
		alter table rtesum_extd modify ttp66 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp67' and coltype='integer') then
		alter table rtesum_extd modify ttp67 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp68' and coltype='integer') then
		alter table rtesum_extd modify ttp68 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp69' and coltype='integer') then
		alter table rtesum_extd modify ttp69 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp70' and coltype='integer') then
		alter table rtesum_extd modify ttp70 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp71' and coltype='integer') then
		alter table rtesum_extd modify ttp71 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp72' and coltype='integer') then
		alter table rtesum_extd modify ttp72 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp73' and coltype='integer') then
		alter table rtesum_extd modify ttp73 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp74' and coltype='integer') then
		alter table rtesum_extd modify ttp74 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp75' and coltype='integer') then
		alter table rtesum_extd modify ttp75 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp76' and coltype='integer') then
		alter table rtesum_extd modify ttp76 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp77' and coltype='integer') then
		alter table rtesum_extd modify ttp77 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp78' and coltype='integer') then
		alter table rtesum_extd modify ttp78 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp79' and coltype='integer') then
		alter table rtesum_extd modify ttp79 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp80' and coltype='integer') then
		alter table rtesum_extd modify ttp80 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp81' and coltype='integer') then
		alter table rtesum_extd modify ttp81 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp82' and coltype='integer') then
		alter table rtesum_extd modify ttp82 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp83' and coltype='integer') then
		alter table rtesum_extd modify ttp83 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp84' and coltype='integer') then
		alter table rtesum_extd modify ttp84 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp85' and coltype='integer') then
		alter table rtesum_extd modify ttp85 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp86' and coltype='integer') then
		alter table rtesum_extd modify ttp86 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp87' and coltype='integer') then
		alter table rtesum_extd modify ttp87 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp88' and coltype='integer') then
		alter table rtesum_extd modify ttp88 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp89' and coltype='integer') then
		alter table rtesum_extd modify ttp89 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp90' and coltype='integer') then
		alter table rtesum_extd modify ttp90 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp91' and coltype='integer') then
		alter table rtesum_extd modify ttp91 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp92' and coltype='integer') then
		alter table rtesum_extd modify ttp92 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp93' and coltype='integer') then
		alter table rtesum_extd modify ttp93 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp94' and coltype='integer') then
		alter table rtesum_extd modify ttp94 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp95' and coltype='integer') then
		alter table rtesum_extd modify ttp95 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp96' and coltype='integer') then
		alter table rtesum_extd modify ttp96 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp97' and coltype='integer') then
		alter table rtesum_extd modify ttp97 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp98' and coltype='integer') then
		alter table rtesum_extd modify ttp98 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp99' and coltype='integer') then
		alter table rtesum_extd modify ttp99 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp100' and coltype='integer') then
		alter table rtesum_extd modify ttp100 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp101' and coltype='integer') then
		alter table rtesum_extd modify ttp101 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp102' and coltype='integer') then
		alter table rtesum_extd modify ttp102 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp103' and coltype='integer') then
		alter table rtesum_extd modify ttp103 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp104' and coltype='integer') then
		alter table rtesum_extd modify ttp104 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp105' and coltype='integer') then
		alter table rtesum_extd modify ttp105 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp106' and coltype='integer') then
		alter table rtesum_extd modify ttp106 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp107' and coltype='integer') then
		alter table rtesum_extd modify ttp107 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp108' and coltype='integer') then
		alter table rtesum_extd modify ttp108 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp109' and coltype='integer') then
		alter table rtesum_extd modify ttp109 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp110' and coltype='integer') then
		alter table rtesum_extd modify ttp110 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp111' and coltype='integer') then
		alter table rtesum_extd modify ttp111 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp112' and coltype='integer') then
		alter table rtesum_extd modify ttp112 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp113' and coltype='integer') then
		alter table rtesum_extd modify ttp113 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp114' and coltype='integer') then
		alter table rtesum_extd modify ttp114 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp115' and coltype='integer') then
		alter table rtesum_extd modify ttp115 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp116' and coltype='integer') then
		alter table rtesum_extd modify ttp116 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp117' and coltype='integer') then
		alter table rtesum_extd modify ttp117 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp118' and coltype='integer') then
		alter table rtesum_extd modify ttp118 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp119' and coltype='integer') then
		alter table rtesum_extd modify ttp119 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp120' and coltype='integer') then
		alter table rtesum_extd modify ttp120 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp121' and coltype='integer') then
		alter table rtesum_extd modify ttp121 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp122' and coltype='integer') then
		alter table rtesum_extd modify ttp122 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp123' and coltype='integer') then
		alter table rtesum_extd modify ttp123 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp124' and coltype='integer') then
		alter table rtesum_extd modify ttp124 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp125' and coltype='integer') then
		alter table rtesum_extd modify ttp125 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp126' and coltype='integer') then
		alter table rtesum_extd modify ttp126 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp127' and coltype='integer') then
		alter table rtesum_extd modify ttp127 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp128' and coltype='integer') then
		alter table rtesum_extd modify ttp128 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp129' and coltype='integer') then
		alter table rtesum_extd modify ttp129 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp130' and coltype='integer') then
		alter table rtesum_extd modify ttp130 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp131' and coltype='integer') then
		alter table rtesum_extd modify ttp131 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp132' and coltype='integer') then
		alter table rtesum_extd modify ttp132 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp133' and coltype='integer') then
		alter table rtesum_extd modify ttp133 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp134' and coltype='integer') then
		alter table rtesum_extd modify ttp134 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp135' and coltype='integer') then
		alter table rtesum_extd modify ttp135 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp136' and coltype='integer') then
		alter table rtesum_extd modify ttp136 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp137' and coltype='integer') then
		alter table rtesum_extd modify ttp137 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp138' and coltype='integer') then
		alter table rtesum_extd modify ttp138 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp139' and coltype='integer') then
		alter table rtesum_extd modify ttp139 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp140' and coltype='integer') then
		alter table rtesum_extd modify ttp140 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp141' and coltype='integer') then
		alter table rtesum_extd modify ttp141 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp142' and coltype='integer') then
		alter table rtesum_extd modify ttp142 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp143' and coltype='integer') then
		alter table rtesum_extd modify ttp143 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp144' and coltype='integer') then
		alter table rtesum_extd modify ttp144 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp145' and coltype='integer') then
		alter table rtesum_extd modify ttp145 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp146' and coltype='integer') then
		alter table rtesum_extd modify ttp146 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp147' and coltype='integer') then
		alter table rtesum_extd modify ttp147 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp148' and coltype='integer') then
		alter table rtesum_extd modify ttp148 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp149' and coltype='integer') then
		alter table rtesum_extd modify ttp149 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp150' and coltype='integer') then
		alter table rtesum_extd modify ttp150 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp151' and coltype='integer') then
		alter table rtesum_extd modify ttp151 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp152' and coltype='integer') then
		alter table rtesum_extd modify ttp152 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp153' and coltype='integer') then
		alter table rtesum_extd modify ttp153 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp154' and coltype='integer') then
		alter table rtesum_extd modify ttp154 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp155' and coltype='integer') then
		alter table rtesum_extd modify ttp155 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp156' and coltype='integer') then
		alter table rtesum_extd modify ttp156 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp157' and coltype='integer') then
		alter table rtesum_extd modify ttp157 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp158' and coltype='integer') then
		alter table rtesum_extd modify ttp158 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp159' and coltype='integer') then
		alter table rtesum_extd modify ttp159 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp160' and coltype='integer') then
		alter table rtesum_extd modify ttp160 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp161' and coltype='integer') then
		alter table rtesum_extd modify ttp161 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp162' and coltype='integer') then
		alter table rtesum_extd modify ttp162 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp163' and coltype='integer') then
		alter table rtesum_extd modify ttp163 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp164' and coltype='integer') then
		alter table rtesum_extd modify ttp164 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp165' and coltype='integer') then
		alter table rtesum_extd modify ttp165 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp166' and coltype='integer') then
		alter table rtesum_extd modify ttp166 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp167' and coltype='integer') then
		alter table rtesum_extd modify ttp167 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp168' and coltype='integer') then
		alter table rtesum_extd modify ttp168 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp169' and coltype='integer') then
		alter table rtesum_extd modify ttp169 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp170' and coltype='integer') then
		alter table rtesum_extd modify ttp170 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp171' and coltype='integer') then
		alter table rtesum_extd modify ttp171 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp172' and coltype='integer') then
		alter table rtesum_extd modify ttp172 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp173' and coltype='integer') then
		alter table rtesum_extd modify ttp173 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp174' and coltype='integer') then
		alter table rtesum_extd modify ttp174 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp175' and coltype='integer') then
		alter table rtesum_extd modify ttp175 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp176' and coltype='integer') then
		alter table rtesum_extd modify ttp176 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp177' and coltype='integer') then
		alter table rtesum_extd modify ttp177 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp178' and coltype='integer') then
		alter table rtesum_extd modify ttp178 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp179' and coltype='integer') then
		alter table rtesum_extd modify ttp179 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp180' and coltype='integer') then
		alter table rtesum_extd modify ttp180 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp181' and coltype='integer') then
		alter table rtesum_extd modify ttp181 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp182' and coltype='integer') then
		alter table rtesum_extd modify ttp182 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp183' and coltype='integer') then
		alter table rtesum_extd modify ttp183 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp184' and coltype='integer') then
		alter table rtesum_extd modify ttp184 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp185' and coltype='integer') then
		alter table rtesum_extd modify ttp185 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp186' and coltype='integer') then
		alter table rtesum_extd modify ttp186 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp187' and coltype='integer') then
		alter table rtesum_extd modify ttp187 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp188' and coltype='integer') then
		alter table rtesum_extd modify ttp188 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp189' and coltype='integer') then
		alter table rtesum_extd modify ttp189 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp190' and coltype='integer') then
		alter table rtesum_extd modify ttp190 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp191' and coltype='integer') then
		alter table rtesum_extd modify ttp191 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp192' and coltype='integer') then
		alter table rtesum_extd modify ttp192 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp193' and coltype='integer') then
		alter table rtesum_extd modify ttp193 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp194' and coltype='integer') then
		alter table rtesum_extd modify ttp194 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp195' and coltype='integer') then
		alter table rtesum_extd modify ttp195 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp196' and coltype='integer') then
		alter table rtesum_extd modify ttp196 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp197' and coltype='integer') then
		alter table rtesum_extd modify ttp197 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp198' and coltype='integer') then
		alter table rtesum_extd modify ttp198 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp199' and coltype='integer') then
		alter table rtesum_extd modify ttp199 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp200' and coltype='integer') then
		alter table rtesum_extd modify ttp200 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp201' and coltype='integer') then
		alter table rtesum_extd modify ttp201 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp202' and coltype='integer') then
		alter table rtesum_extd modify ttp202 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp203' and coltype='integer') then
		alter table rtesum_extd modify ttp203 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp204' and coltype='integer') then
		alter table rtesum_extd modify ttp204 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp205' and coltype='integer') then
		alter table rtesum_extd modify ttp205 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp206' and coltype='integer') then
		alter table rtesum_extd modify ttp206 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp207' and coltype='integer') then
		alter table rtesum_extd modify ttp207 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp208' and coltype='integer') then
		alter table rtesum_extd modify ttp208 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp209' and coltype='integer') then
		alter table rtesum_extd modify ttp209 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp210' and coltype='integer') then
		alter table rtesum_extd modify ttp210 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp211' and coltype='integer') then
		alter table rtesum_extd modify ttp211 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp212' and coltype='integer') then
		alter table rtesum_extd modify ttp212 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp213' and coltype='integer') then
		alter table rtesum_extd modify ttp213 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp214' and coltype='integer') then
		alter table rtesum_extd modify ttp214 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp215' and coltype='integer') then
		alter table rtesum_extd modify ttp215 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp216' and coltype='integer') then
		alter table rtesum_extd modify ttp216 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp217' and coltype='integer') then
		alter table rtesum_extd modify ttp217 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp218' and coltype='integer') then
		alter table rtesum_extd modify ttp218 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp219' and coltype='integer') then
		alter table rtesum_extd modify ttp219 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp220' and coltype='integer') then
		alter table rtesum_extd modify ttp220 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp221' and coltype='integer') then
		alter table rtesum_extd modify ttp221 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp222' and coltype='integer') then
		alter table rtesum_extd modify ttp222 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp223' and coltype='integer') then
		alter table rtesum_extd modify ttp223 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp224' and coltype='integer') then
		alter table rtesum_extd modify ttp224 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp225' and coltype='integer') then
		alter table rtesum_extd modify ttp225 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp226' and coltype='integer') then
		alter table rtesum_extd modify ttp226 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp227' and coltype='integer') then
		alter table rtesum_extd modify ttp227 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp228' and coltype='integer') then
		alter table rtesum_extd modify ttp228 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp229' and coltype='integer') then
		alter table rtesum_extd modify ttp229 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp230' and coltype='integer') then
		alter table rtesum_extd modify ttp230 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp231' and coltype='integer') then
		alter table rtesum_extd modify ttp231 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp232' and coltype='integer') then
		alter table rtesum_extd modify ttp232 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp233' and coltype='integer') then
		alter table rtesum_extd modify ttp233 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp234' and coltype='integer') then
		alter table rtesum_extd modify ttp234 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp235' and coltype='integer') then
		alter table rtesum_extd modify ttp235 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp236' and coltype='integer') then
		alter table rtesum_extd modify ttp236 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp237' and coltype='integer') then
		alter table rtesum_extd modify ttp237 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp238' and coltype='integer') then
		alter table rtesum_extd modify ttp238 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp239' and coltype='integer') then
		alter table rtesum_extd modify ttp239 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp240' and coltype='integer') then
		alter table rtesum_extd modify ttp240 int not null;
	end if;
	if not exists(select null from sys.syscolumns where tname='rtesum_extd' and cname='ttp241' and coltype='integer') then
		alter table rtesum_extd modify ttp241 int not null;
	end if;
end if;
go

grant select on DBA.rtesum_extd to public;
go

///////////////////////////////////////////////
// LG-1211
///////////////////////////////////////////////
if exists(select null from sys.syscolumns where tname='ppd' and cname='magnetic_seq') then
   alter table dba.ppd drop magnetic_seq;
end if;
go

if exists(select null from sys.syscolumns where tname='svd' and cname='magnetic_seq') then
   alter table dba.svd drop magnetic_seq;
end if;
go

if exists(select null from sys.syscolumns where tname='srd' and cname='magnetic_seq') then
   alter table dba.srd drop magnetic_seq;
end if;
go

if exists(select null from sys.syscolumns where tname='scd' and cname='magnetic_seq') then
   alter table dba.scd drop magnetic_seq;
end if;
go

if exists(select null from sys.syscolumns where tname='trd' and cname='magnetic_seq') then
   alter table dba.trd drop magnetic_seq;
end if;
go


////////////////////////////////////////////////
// LG-1384
////////////////////////////////////////////////
if not exists(select null from sys.systable where table_name='hcv_in' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.hcv_in(
		tr_id smallint,
		tr_type smallint,
		device_id varchar(16),
		drv integer,
		longtitude numeric(11, 8),
		latitude numeric(11, 8),
		bstop char(8),
		ts datetime,
		route integer,
		run integer,
		trip integer,
		direction char(1),
		eid bigint,
		valid char(1),
		reason smallint,
		mediatype integer,
		eqid integer,
		mfgid integer,
		eqtype integer,
		notes varchar(512)
	);
end if;
go

grant select on dba.hcv_in to public;
go

if not exists(select null from sys.systable where table_name='hcv' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.hcv(
		loc_n smallint not null,
		id integer not null,
		tr_id smallint,
		tr_type smallint,
		device_id varchar(16),
		drv integer,
		longtitude numeric(11, 8),
		latitude numeric(11, 8),
		bstop char(8),
		ts datetime,
		route integer,
		run integer,
		trip integer,
		direction char(1),
		eid bigint,
		valid char(1),
		reason smallint,
		mediatype integer,
		eqid integer,
		mfgid integer,
		eqtype integer,
		notes  varchar(512)
	);
end if;
go

grant select on dba.hcv to public;
go


/////////////////////////////////////////////////
// LG-1294
/////////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='firmware_info' and cname='boot_v') then
   alter table dba.firmware_info add boot_v smallint not null default 0;
   comment on column DBA.firmware_info.boot_v is 'Boot version';
end if;
go

///////////////////////////////////////////////
// LG-1279
///////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='trmisc' and cname='grp') then
   alter table dba.trmisc add grp tinyint null;
end if;
go

if not exists(select null from sys.syscolumns where tname='trmisc' and cname='des') then
   alter table dba.trmisc add des tinyint null;
end if;
go

if not exists(select null from sys.syscolumns where tname='trmisc' and cname='card_id') then
   alter table dba.trmisc add card_id integer null;
end if;
go


///////////////////////////////////////////////
// LG-1279
///////////////////////////////////////////////
if not exists(select null from sys.systable where table_name='media_category' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.media_category(
		[category] smallint not null,
		[text] varchar(255) not null,
		constraint PK_MEDIA_CATEGORY primary key ([category])
	);
end if;
go

grant select on dba.media_category to public;
go


///////////////////////////////////////////////
// LG-1279
///////////////////////////////////////////////
if not exists(select null from sys.systable where table_name='media_category_transaction_type' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.media_category_transaction_type(
		[category] smallint not null,
		[type] smallint not null,
		constraint PK_MEDIA_CATEGORY_TRANSACTION_TYPE primary key ([category], [type]),
		constraint FK__MEDIA_CATEGORY_TRANSACTION_TYPE__MEDIA_CATEGORY foreign key ([category])
			references DBA.media_category ([category]),
		constraint FK__MEDIA_CATEGORY_TRANSACTION_TYPE__ET foreign key ([type])
			references DBA.et ([type])
	);
end if;
go

grant select on dba.media_category_transaction_type to public;
go

////////////////////////////////////////////////
// LG-1587 - Modify SQL code to support 255 designators
////////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='transfers' and cname='des' and coltype='smallint') then
	alter table transfers modify des smallint not null;
end if;
go

/////////////////////////////////////////////////
// LG-1377
/////////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='rtelst' and cname='routecode') then
   alter table dba.rtelst add routecode varchar(8) null;
   comment on column DBA.rtelst.routecode is 'Route code';
end if;
go

////////////////////////////////////////////////
// LG-1666 - Mobile Ticketing Integration with Cloud
////////////////////////////////////////////////

if exists(select null from sys.systable where table_name='gfi_tr_mobile_ticket' and table_type='BASE' and creator=user_id('DBA')) then
	execute immediate 'drop table gfi_tr_mobile_ticket';
end if;
go

if exists(select null from sys.systable where table_name='gfi_epay_mobile_tickets' and table_type='BASE' and creator=user_id('DBA')) then
	if not exists(select null from sys.syscolumns where tname='gfi_epay_mobile_tickets' and cname='barcode' and length=50) then
		alter table gfi_epay_mobile_tickets modify barcode varchar(50) not null;	-- LG-1754
	end if;
else
	create table dba.gfi_epay_mobile_tickets (
		loc_n smallint not null default 1,
		id int not null,
		tr_seq smallint not null,
		barcode varchar(50) not null,	-- LG-1754
		create_ts datetime not null default current timestamp,
		create_by integer not null,
		modify_ts datetime null,
		modify_by int null,
		mid smallint null,
		fs_id smallint null,
		aid smallint null,
		fbx_n int null,
		grp tinyint null,
		des tinyint null,
		scan_ts datetime null,
		deduction numeric(10, 2) null,
		passenger_count smallint null,
		ttp tinyint null,
		validity bigint null,
		constraint PK_GFI_EPAY_MOBILE_TICKETS primary key (loc_n, id, tr_seq)
	);
		
	begin
		declare v_min_scan_ts datetime;
		declare v_min_ts datetime;
		declare v_mobile_ticket_id bigint;
		declare v_loc_n smallint;
		declare v_id int;
		declare v_tr_seq smallint;
		declare v_old_count int;
		declare v_new_count int;
		
		if exists(select null from sys.systable where table_name='gfi_epay_mobile_ticket' and table_type='BASE' and creator=user_id('DBA')) then
			select
				'1900-01-01', '1900-01-01' into v_min_scan_ts, v_min_ts
			;
			
			lbl_loop:
			loop
				select
					min(scan_ts) into v_min_scan_ts
				from gfi_epay_mobile_ticket
				where	scan_ts > v_min_scan_ts;
				
				if v_min_scan_ts is null then
					leave lbl_loop;
				end if;
				
				select
					mobile_ticket_id into v_mobile_ticket_id
				from gfi_epay_mobile_ticket
				where	scan_ts = v_min_scan_ts;
				
				if v_mobile_ticket_id is null then
					leave lbl_loop;
				end if;
			
				select
					min(ts) into v_min_ts
				from tr
				where	type = 212
					and	ts > v_min_ts;
				
				if v_min_ts is null then
					leave lbl_loop;
				end if;
			
				select
					loc_n, id, tr_seq into v_loc_n, v_id, v_tr_seq
				from tr
				where	type = 212
					and	ts = v_min_ts
				;
				
				if v_loc_n is null
				or v_id is null
				or v_tr_seq is null then
					leave lbl_loop;
				end if;
			
				insert into gfi_epay_mobile_tickets
				select
					tr.loc_n,
					tr.id,
					tr.tr_seq,
					t.barcode,
					t.create_ts,
					t.create_by,
					t.modify_ts,
					t.modify_by,
					t.mid,
					t.fs_id,
					t.aid,
					t.fbx_n,
					t.grp,
					t.des,
					t.scan_ts,
					t.deduction,
					t.passenger_count,
					t.ttp,
					t.validity
				from gfi_epay_mobile_ticket as t
				inner join tr
					on	tr.loc_n = v_loc_n
					and	tr.id = v_id
					and	tr.tr_seq = v_tr_seq
				where	t.mobile_ticket_id = v_mobile_ticket_id;
			end loop;
			
			commit;
			
			select
				count(*) into v_old_count
			from gfi_epay_mobile_ticket;
			
			select
				count(*) into v_new_count
			from gfi_epay_mobile_tickets;
			
			if v_old_count = v_new_count then
				execute immediate 'drop table gfi_epay_mobile_ticket';
			end if
		end if;
	end
end if;
go
   
grant SELECT on dba.gfi_epay_mobile_tickets to public;
go

///////////////////////////////////////////////
// LG-825
///////////////////////////////////////////////
alter table dba.gfi_epay_act_bus
	alter tr_d set compute(dateadd(day,datediff(day,('1900-01-01'),ts),('1900-01-01')));
go

alter table dba.gfi_epay_act_eq
	alter tr_d set compute(dateadd(day,datediff(day,('1900-01-01'),ts),('1900-01-01')));
go
	
alter table dba.gfi_epay_act_order
	alter tr_d set compute(coalesce(dateadd(day,datediff(day,('1900-01-01'),fulfill_ts),('1900-01-01')), dateadd(day,datediff(day,('1900-01-01'),submit_ts),('1900-01-01')), dateadd(day,datediff(day,('1900-01-01'),ts),('1900-01-01'))));
go

---------
-- GD-174
---------
if not exists	(	select null
					from syscolumn as c
					inner join systable as t
						on	t.table_id = c.table_id
					where	table_name = 'firmware_info'
						and	c.column_name = 'fbx_n'
						and c.pkey = 'Y'
				)
then
	alter table firmware_info drop constraint PK_FIRMWARE_INFO;
	alter table firmware_info add constraint PK_FIRMWARE_INFO primary key (loc_n, bus, fbx_n);
end if;
go

----------
-- GD-1629
----------
if not exists	(	select null
					from syscolumn as c
					inner join systable as t
						on	t.table_id = c.table_id
					where	table_name = 'firmware_info'
						and	c.column_name = 'fbx_v'
						and c.width = 4
				)
then
	alter table firmware_info modify fbx_v int not null;
end if;
go
if not exists	(	select null
					from syscolumn as c
					inner join systable as t
						on	t.table_id = c.table_id
					where	table_name = 'eqlst'
						and	c.column_name = 'fbx_ver'
						and c.width = 4
				)
then
	alter table eqlst modify fbx_ver int not null;
end if;
go
if not exists	(	select null
					from syscolumn as c
					inner join systable as t
						on	t.table_id = c.table_id
					where	table_name = 'ml'
						and	c.column_name = 'ver_n'
						and c.width = 4
				)
then
	alter table ml modify ver_n int not null;
end if;
go

/////////////////////////////////////////////////////////////////////////
// gfisp_get_ridership_data - GD-2101
/////////////////////////////////////////////////////////////////////////
if not exists(select null from sys.systable where table_name='ridership' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.ridership(
		loc_n            smallint          not null,
		id               int               not null,
		[route]          varchar(10)       not null,
		farecell_number  tinyint           not null,
		farecell_name    varchar(32)       not null,
		value            int               not null,
		constraint PK_RIDERSHIP primary key ([loc_n], [id], [route], [farecell_number])
	);
end if;
go

grant select on dba.ridership to public;
go

/////////////////////////////////////////////////////////////////////////
// xerox_tr - GD-572
/////////////////////////////////////////////////////////////////////////
if not exists(select null from sys.systable where table_name='xerox_tr' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.xerox_tr(
		id                          integer not null default null, 
		seq 						integer not null default null, 
		loc_n 						integer not null default null, 
		type1 						varchar(50) not null default null, 
		operation_datetime 			timestamp not null default null, 
		route_id 					integer not null default null, 
		run_id 						integer not null default null, 
		operation_type 				integer default null, 
		operation_request_id 		integer default null, 
		ticket_no_pos 				integer default null, 
		contract_id 				integer default null, 
		contract_no 				integer default null, 
		rider_cat_id 				integer default null,
		media_type 					varchar(4) default null, 
		media_cd 					integer default null, 
		media_serial_no 			integer default null, 
		unit_bal_before 			integer default null, 
		unit_bal_after 				integer default null, 
		start_date_after 			date default null, 
		end_date_before 			date default null, 
		end_date_after 				date default null, 
		number_riders 				integer default null, 
		operation_amount 			integer default null, 
		num_pending_reloads 		integer default null, 
		subscript_type 				integer default null, 
		granted_bonus_units 		integer default null, 
		granted_bonus_amount 		integer default null, 
		sub_act_threshold 			integer default null, 
		sub_reload_amount 			integer default null, 
		sub_owner_id 				integer default null, 
		sub_status 					integer default null, 
		act_auth 					integer default null, 
		act_recorded 				integer default null, 
		month_last_act 				integer default null, 
		year_last_act 				integer default null, 
		validity_cal 				integer default null, 
		card_seq_no 				integer default null, 
		pay_type 					integer default null, 
		validation_type 			varchar(1) default null, 
		deducted_units 				integer default null, 
		remaining_units 			integer default null, 
		contract_end_date			datetime default null, 
		reject_cd 					integer default null, 
		list_type 					varchar(3) default null, 
		xerox_tday 					date default null, 
		dir							char(1) default null
	);
end if;
go

grant select on dba.xerox_tr to public;
go

/////////////////////////////////////////////////////////////////////////
// probing_status - GD-2144
/////////////////////////////////////////////////////////////////////////
if exists(select null from sys.systable where table_name='wifi_probing_status' and table_type='BASE' and creator=user_id('DBA')) then
	execute immediate 'drop table wifi_probing_status';
end if;
go

if exists(select null from sys.syscolumns where tname='probing_status' and cname='busNumber') then
	execute immediate 'drop table probing_status';
end if;
go

if not exists(select null from sys.systable where table_name='probing_status' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.probing_status(
		loc_n smallint not null default 1,
		bus integer not null, 
		FbxNo integer not null,
		TimeStamp_Probe datetime not null,
		fbx_type smallint null,
		Rcvd_Trans bit not null default 0,
		Sent_Fbx_Cfg bit not null default 0,
		Rcvd_MBTK_Events bit not null default 0,
		Sent_Net_cfg bit not null default 0,
		Sent_FareStructure bit not null default 0,
		Sent_FutureFareStructure bit not null default 0,
		Sent_Autoload_list bit not null default 0,
		Sent_Bad_list bit not null default 0,
		Sent_Driver_list bit not null default 0,
		Sent_Route_list bit not null default 0,
		Sent_Run_list bit not null default 0,
		Sent_Trip_list bit not null default 0,
		Sent_Range_list bit not null default 0,
		Sent_SCD_list bit not null default 0,
		Banner bit not null default 0,
		Sent_MBTK_cfg bit not null default 0,
		Sent_MBTK_Baslist bit not null default 0,
		Sent_Alpha_route_list bit not null default 0,
		Sent_SSL_Cert bit not null default 0,
		Clear_Mem bit not null default 0,
		Sent_Logic_FW_version integer not null default 0,
		Sent_Lid_FW_Version integer not null default 0,
		Cbx_ID smallint not null default 0,
		Prb_n  tinyint not null default 10, -- wifi
		constraint PK_PROBING_STATUS primary key (loc_n, bus, FbxNo, TimeStamp_Probe)
	);
end if;
go

grant select on dba.probing_status to public;
go


////////////////////////////////////////////////
// GD-2206
////////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='trmisc' and cname='tr_ctype') then
	alter table dba.trmisc add tr_ctype tinyint null;
elseif exists(select null from sys.syscolumns where tname='trmisc' and cname='tr_ctype' and coltype = 'char') then
	alter table dba.trmisc modify tr_ctype char(3) null;
	
	update dba.trmisc set
		tr_ctype = cast(ascii(tr_ctype) as char(3))
	where	tr_ctype is not null;
	
	alter table dba.trmisc modify tr_ctype tinyint null;
end if;
go
if not exists(select null from sys.syscolumns where tname='trmisc' and cname='tr_etype') then
   alter table dba.trmisc add tr_etype tinyint null;
elseif exists(select null from sys.syscolumns where tname='trmisc' and cname='tr_etype' and coltype = 'char') then
	alter table dba.trmisc modify tr_etype char(3) null;
	
	update dba.trmisc set
		tr_etype = cast(ascii(tr_etype) as char(3))
	where	tr_etype is not null;

	alter table dba.trmisc modify tr_etype tinyint null;
end if;
go
if not exists(select null from sys.syscolumns where tname='trmisc' and cname='tr_events') then
   alter table dba.trmisc add tr_events bigint null;
end if;
go
if not exists(select 0 from sys.systable where table_name='event_subtype' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.event_subtype(
		etype              tinyint         not null,
		description        varchar(50)     not null,
		constraint PK_EVENT_SUBTYPE primary key (etype)
	);

	grant select on dba.event_subtype to public;
end if;
go
if not exists(select 0 from sys.systable where table_name='event_status' and table_type='BASE' and creator=user_id('DBA')) then
	create table dba.event_status(
		etype           tinyint         not null,
		id              bigint          not null,
		description     varchar(50)     not null,
		constraint PK_EVENT_STATUS primary key (etype, id)
	);

	grant select on dba.event_status to public;
end if;
go


////////////////////////////////////////////////
// GD-2287
////////////////////////////////////////////////
if not exists(select null from sys.syscolumns where tname='et' and cname='ridership') then
   alter table dba.et add ridership char(1) null;
end if;
go


////////////////////////////////////////////////
// DATARUN-83
////////////////////////////////////////////////
if not exists(select 0 from sys.syscolumns where tname='probing_status' and cname='prb_status') then
  alter table dba.probing_status add prb_status tinyint default 0;
  alter table dba.probing_status modify prb_status not null;
end if;

////////////////////////////////////////////////
// DATARUN-304
////////////////////////////////////////////////
if exists(select 1 from sysconstraint where constraint_name='PK_MEDIA_TRACK2') then
                alter table media_track2 drop constraint PK_MEDIA_TRACK2;
end if;

if exists(select 1 from sysconstraint where constraint_name='PK_FSC') then
                alter table fsc drop constraint PK_FSC;
end if;

if exists(select 1 from sysconstraint where constraint_name='PK_HOLIDAYS') then
                alter table holidays drop constraint PK_HOLIDAYS;
end if;

if exists(select 1 from sysconstraint where constraint_name='PK_FARESET') then
                alter table fareset drop constraint PK_FARESET;
end if;

if exists(select 1 from sysconstraint where constraint_name='PK_FARESET_FSC') then
                alter table fareset drop constraint PK_FARESET_FSC;
end if;

if exists(select 1 from sysconstraint where constraint_name='PK_MEDIA') then
                alter table media drop constraint PK_MEDIA;
end if;

if exists(select 1 from sysconstraint where constraint_name='FK_HOLIDAYS_FSC') then
                alter table holidays drop constraint FK_HOLIDAYS_FSC;
end if;

if exists(select 1 from sysconstraint where constraint_name='FK_FARESET_FSC') then
                alter table fareset drop constraint FK_FARESET_FSC;
end if;

if exists(select 1 from sysconstraint where constraint_name='FK_MEDIA_FSC') then
                alter table media drop constraint FK_MEDIA_FSC;
end if;

// Alter column //
if exists (select 1 from syscolumn where column_name = 'fs_id' and table_id = ( select table_id from systable where table_name = 'media_track2' )) then
                alter table media_track2 modify fs_id smallint not null default(1);
end if;

if exists (select 1 from syscolumn where column_name = 'fs_id' and table_id = ( select table_id from systable where table_name = 'fsc' )) then
                alter table fsc modify fs_id smallint not null;
end if;

if exists (select 1 from syscolumn where column_name = 'fs_id' and table_id = ( select table_id from systable where table_name = 'holidays' )) then
                alter table holidays modify fs_id smallint not null;
end if;

if exists (select 1 from syscolumn where column_name = 'fs_id' and table_id = ( select table_id from systable where table_name = 'fareset' )) then
                alter table fareset modify fs_id smallint not null;
end if;

if exists (select 1 from syscolumn where column_name = 'fs_id' and table_id = ( select table_id from systable where table_name = 'media' )) then
                alter table media modify fs_id smallint not null;
end if;

if exists (select 1 from syscolumn where column_name = 'fs_id' and table_id = ( select table_id from systable where table_name = 'gfi_epay_mobile_tickets' )) then
                alter table dba.gfi_epay_mobile_tickets modify fs_id smallint null;
end if;

// Recreate the constraints //

if not exists(select 1 from sysconstraint where constraint_name='PK_MEDIA_TRACK2') then
                alter table media_track2 add constraint PK_MEDIA_TRACK2 primary key (inst_id, loc_n, fs_id, trk2_mask);
end if;
if not exists(select 1 from sysconstraint where constraint_name='PK_FSC') then
                alter table fsc add constraint PK_FSC primary key (loc_n, fs_id);
end if;
if not exists(select 1 from sysconstraint where constraint_name='FK_HOLIDAYS_FSC') then
                alter table holidays add constraint FK_HOLIDAYS_FSC foreign key (loc_n, fs_id) references DBA.fsc (loc_n, fs_id);
end if;
if not exists(select 1 from sysconstraint where constraint_name='PK_FARESET') then
                alter table fareset add constraint PK_FARESET primary key (loc_n, fs_id, fareset_n);
end if;
if not exists(select 1 from sysconstraint where constraint_name='FK_FARESET_FSC') then
                alter table fareset add constraint FK_FARESET_FSC foreign key (loc_n, fs_id) references DBA.fsc (loc_n, fs_id);
end if;
if not exists(select 1 from sysconstraint where constraint_name='FK_MEDIA_FSC') then
                alter table media add constraint FK_MEDIA_FSC foreign key (loc_n, fs_id) references DBA.fsc (loc_n, fs_id);
end if;
if not exists(select 1 from sysconstraint where constraint_name='PK_MEDIA') then
                alter table media add constraint PK_MEDIA primary key (loc_n, fs_id, m_ndx);
end if;
if not exists(select 1 from sysconstraint where constraint_name='FK_MEDIA_FSC') then
                alter table media add constraint FK_MEDIA_FSC foreign key (loc_n, fs_id) references DBA.fsc (loc_n, fs_id);
end if;

////////////////////////////////////////////////
// DATARUN-273
////////////////////////////////////////////////
/* Remove the constraints on buslst.loc_n column before altering it */
if exists(select 1 from sysconstraint where constraint_name='FK_BUSLST_CNF') then
	alter table buslst drop constraint FK_BUSLST_CNF;
end if;

if exists(select 1 from sysconstraint where constraint_name='PK_BUSLST') then
	alter table buslst drop constraint PK_BUSLST ;
end if;

-- Alter column buslst.loc_n to add a default value
if exists (select 1 from syscolumn where column_name = 'loc_n' and table_id = ( select table_id from systable where table_name = 'buslst' )) then
	alter table buslst modify loc_n smallint not null default(1);
end if;

/* Recreate the constraints on buslst.loc_n column */
if not exists(select 1 from sysconstraint where constraint_name='FK_BUSLST_CNF') then
	alter table buslst add constraint FK_BUSLST_CNF foreign key (loc_n) references DBA.cnf (loc_n);
end if;

if not exists(select 1 from sysconstraint where constraint_name='PK_BUSLST') then
	alter table buslst add constraint PK_BUSLST primary key (loc_n, bus);
end if;

////////////////////////////////////////////////////////////////////////
if not exists(select 0 from sysindex where index_name='NDX_EV_LOC_ID') then
   create nonclustered index NDX_EV_LOC_ID on EV(loc_n,id);
end if;
go

////////////////////////////////////////////////////////////////////////
// ESBUILD-406
////////////////////////////////////////////////////////////////////////
if not exists(select 1 from sys.systable where table_name='fs_act' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_act (
		act tinyint NOT NULL,
		text varchar(255) NOT NULL,
		CONSTRAINT PK_FS_ACT PRIMARY KEY ( act )
	);
	COMMENT ON TABLE DBA.fs_act IS 'Action definition';
	COMMENT ON COLUMN DBA.fs_act.act IS 'Action ID';
	COMMENT ON COLUMN DBA.fs_act.text IS 'Description';
end if;
go

grant select on DBA.fs_act to public;
go

if not exists(select 1 from sys.systable where table_name='fs_actlst' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_actlst (
		id smallint NOT NULL,
		seq smallint NOT NULL,
		act tinyint NOT NULL,
		val varchar(255) NULL,
		CONSTRAINT PK_FS_ACTLST PRIMARY KEY ( id, seq )
	);
	COMMENT ON TABLE DBA.fs_actlst IS 'Chain of action definition';
	COMMENT ON COLUMN DBA.fs_actlst.id IS 'List ID';
	COMMENT ON COLUMN DBA.fs_actlst.seq IS 'List item sequence number';
	COMMENT ON COLUMN DBA.fs_actlst.act IS 'Action ID';
	COMMENT ON COLUMN DBA.fs_actlst.val IS 'Action value (date format: yyyy-mm-dd; time format: hh:mm:ss; datetime format: yyyy-mm-dd hh:mm:ss; money format: in cents; percentage format: 10 for 10% and 5.5 for 5.5%)';
end if;
go

grant select on DBA.fs_actlst to public;
go

if not exists(select 1 from sys.systable where table_name='fs_actlst_def' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_actlst_def (
		id smallint NOT NULL,
		text varchar(255) NOT NULL,
		CONSTRAINT PK_FS_ACTLST_DEF PRIMARY KEY ( id )
	);
	COMMENT ON TABLE DBA.fs_actlst_def IS 'Action list definition';
	COMMENT ON COLUMN DBA.fs_actlst_def.id IS 'Action list ID';
	COMMENT ON COLUMN DBA.fs_actlst_def.text IS 'Description';
end if;
go

grant select on DBA.fs_actlst_def to public;
go

if not exists(select 1 from sys.systable where table_name='fs_actlst_farecell' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_actlst_farecell (
		fareset_n tinyint NOT NULL,
		farecell_n tinyint NOT NULL,
		id smallint NOT NULL,
		CONSTRAINT PK_FS_ACTLST_FARECELL PRIMARY KEY ( fareset_n, farecell_n )
	);
	COMMENT ON TABLE DBA.fs_actlst_farecell IS 'Farecell and action list cross reference';
	COMMENT ON COLUMN DBA.fs_actlst_farecell.fareset_n IS 'Fareset number';
	COMMENT ON COLUMN DBA.fs_actlst_farecell.farecell_n IS 'Farecell number';
	COMMENT ON COLUMN DBA.fs_actlst_farecell.id IS 'Action list ID';
end if;
go

grant select on DBA.fs_actlst_farecell to public;
go

if not exists(select 1 from sys.systable where table_name='fs_cat' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_cat (
		cat tinyint NOT NULL,
		text varchar(255) NOT NULL,
		CONSTRAINT PK_FS_CAT PRIMARY KEY ( cat )
	);
	COMMENT ON TABLE DBA.fs_cat IS 'Fare structure rider category';
	COMMENT ON COLUMN DBA.fs_cat.cat IS 'Rider category';
	COMMENT ON COLUMN DBA.fs_cat.text IS 'Description';
end if;
go

grant select on DBA.fs_cat to public;
go

if not exists(select 1 from sys.systable where table_name='fs_fare' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_fare (
		cat tinyint NOT NULL,
		zone1 tinyint NOT NULL,
		zone2 tinyint NOT NULL,
		amt numeric(5,2) NOT NULL DEFAULT 0,
		CONSTRAINT PK_FS_FARE PRIMARY KEY ( cat, zone1, zone2 )
	);
	COMMENT ON TABLE DBA.fs_fare IS 'Fare structure fare definitions';
	COMMENT ON COLUMN DBA.fs_fare.cat IS 'Rider category';
	COMMENT ON COLUMN DBA.fs_fare.zone1 IS 'From zone';
	COMMENT ON COLUMN DBA.fs_fare.zone2 IS 'To zone';
	COMMENT ON COLUMN DBA.fs_fare.amt IS 'Fare';
end if;
go

grant select on DBA.fs_fare to public;
go

if not exists(select 1 from sys.systable where table_name='fs_ktfare' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_ktfare (
		fareset_n tinyint NOT NULL,
		farecell_n tinyint NOT NULL,
		cat tinyint NOT NULL,
		CONSTRAINT PK_FS_KTFARE PRIMARY KEY ( fareset_n, farecell_n )
	);
	COMMENT ON TABLE DBA.fs_ktfare IS 'Fare structure key/TTP and fare definition cross reference';
	COMMENT ON COLUMN DBA.fs_ktfare.fareset_n IS 'Fareset number';
	COMMENT ON COLUMN DBA.fs_ktfare.farecell_n IS 'Farecell number';
	COMMENT ON COLUMN DBA.fs_ktfare.cat IS 'Rider category';
end if;
go

grant select on DBA.fs_ktfare to public;
go

if not exists(select 1 from sys.systable where table_name='fs_xref' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fs_xref (
		aut_n smallint NOT NULL,
		t_ndx tinyint NOT NULL,
		product smallint NOT NULL,
		item smallint NOT NULL,
		CONSTRAINT PK_FS_XREF PRIMARY KEY ( aut_n, t_ndx )
	);
	COMMENT ON TABLE DBA.fs_xref IS 'GFI issued document and ACS product cross references';
	COMMENT ON COLUMN DBA.fs_xref.aut_n IS 'GFI transit authority number';
	COMMENT ON COLUMN DBA.fs_xref.t_ndx IS 'Issued document index';
	COMMENT ON COLUMN DBA.fs_xref.product IS 'ACS product number';
	COMMENT ON COLUMN DBA.fs_xref.item IS 'ACS product item number';
end if;
go

grant select on DBA.fs_xref to public;
go

if not exists(select 1 from sys.systable where table_name='fsc' and table_type='BASE' and creator=user_id('DBA')) then
	CREATE TABLE DBA.fsc (
		loc_n smallint NOT NULL,
		fs_id smallint NOT NULL,
		ver smallint NOT NULL DEFAULT 0,
		description varchar(32) NOT NULL DEFAULT 'Farestructure',
		created_ts timestamp NOT NULL DEFAULT current timestamp,
		lockcode varchar(5) NOT NULL,
		lockcode_f tinyint NOT NULL DEFAULT 2,
		store_f char(1) NOT NULL DEFAULT 'Y',
		fareset_c tinyint NOT NULL DEFAULT 10,
		alt_key char(1) NOT NULL DEFAULT '*',
		peak1on time NOT NULL DEFAULT '00:00:00',
		peak1off time NOT NULL DEFAULT '00:00:00',
		peak2on time NOT NULL DEFAULT '00:00:00',
		peak2off time NOT NULL DEFAULT '00:00:00',
		modified_ts timestamp NOT NULL DEFAULT current timestamp,
		modified_userid varchar(32) NOT NULL DEFAULT current user,
		effective_ts timestamp NOT NULL DEFAULT current timestamp,
		created_userid varchar(32) NOT NULL DEFAULT current user,
		text varchar(8) NOT NULL DEFAULT 'FS',
		CONSTRAINT PK_FSC PRIMARY KEY ( loc_n, fs_id )
	);
	COMMENT ON TABLE DBA.fsc IS 'Farestructure definition';
	COMMENT ON COLUMN DBA.fsc.loc_n IS 'Location (garage) number';
	COMMENT ON COLUMN DBA.fsc.fs_id IS 'Farestructure ID';
	COMMENT ON COLUMN DBA.fsc.ver IS 'Farestructure version number';
	COMMENT ON COLUMN DBA.fsc.description IS 'Farestructure description';
	COMMENT ON COLUMN DBA.fsc.created_ts IS 'Timestamp when created';
	COMMENT ON COLUMN DBA.fsc.lockcode IS 'Lock code number';
	COMMENT ON COLUMN DBA.fsc.lockcode_f IS 'Lock code flag (0 - ignore; 1 - test & open; 2 - store new code)';
	COMMENT ON COLUMN DBA.fsc.store_f IS 'Store farestructure flag (Y for store; N for ignore)';
	COMMENT ON COLUMN DBA.fsc.fareset_c IS 'Number of faresets implemented';
	COMMENT ON COLUMN DBA.fsc.alt_key IS 'Designated Alt key (1-9, *, A-D)';
	COMMENT ON COLUMN DBA.fsc.peak1on IS 'Peak period 1 on time';
	COMMENT ON COLUMN DBA.fsc.peak1off IS 'Peak period 1 off time';
	COMMENT ON COLUMN DBA.fsc.peak2on IS 'Peak period 2 on time';
	COMMENT ON COLUMN DBA.fsc.peak2off IS 'Peak period 2 off time';
	COMMENT ON COLUMN DBA.fsc.modified_ts IS 'Timestamp when modified';
	COMMENT ON COLUMN DBA.fsc.modified_userid IS 'User ID that modified the farestructure';
	COMMENT ON COLUMN DBA.fsc.effective_ts IS 'Effective date and time';
	COMMENT ON COLUMN DBA.fsc.created_userid IS 'User ID that created the farestructure';
	COMMENT ON COLUMN DBA.fsc.text IS 'Text displayed when processed';
end if;
go

grant select on DBA.fsc to public;
go

///////////////////////////////////////////////////////////////////////////
Update dba.configurationsData set datavalue = 'Complete' where dataname in('GFIDBASA1');
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// E N D
////////////////////////////////////////////////////////////////////////
