////////////////////////////////////////////////////////////////////////
//	Sybase SQL script updates GFI database objects to the current version
// DBMS: Sybase Adaptive Server Anywhere 9

// $Author: imikhalyev $
// $Revision: 1.12 $
// $Date: 2016-10-27 16:21:16 $

// Note:
// - some common utility functions are implemented in order for the SQL statements in GFI applications to be DBMS portable 

////////////////////////////////////////////////////////////////////////
// * * * S T O R E D   F U N C T I O N   D E F I N I T I O N S * * *
////////////////////////////////////////////////////////////////////////

if exists(select 1 from sys.sysprocedure where proc_name='fndbver') then
   drop function dba.fndbver;                    // obsolete; use new function gfisf_getver()
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='sf_chklist') then
   drop function dba.sf_chklist;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisf_parserange') then
   drop function dba.gfisf_parserange;
end if;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getver') then
   drop function gfisf_getver;
end if;
go

////////////////////////////////////////////////////////////////////////
// fnrdrstr - get ridership count formula for a location
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnrdrstr') then
   drop function dba.fnrdrstr;
end if;
go

create function dba.fnrdrstr(tbl varchar(32), loc smallint) returns varchar(1024)
begin
   // tbl - table name to prefix column
   // loc - location number 
   declare rdr varchar(1024);

   if tbl<>'' then
      set tbl=tbl||'.';
   else
      set tbl='';
   end if;

   select list(tbl||(case when farecell_n=0 then 'fare_c'
                          when farecell_n<10 then 'key'||farecell_n
                          when farecell_n=10 then 'keyast'
                          when farecell_n<15 then 'key'||substr('abcd',farecell_n - 10,1)
                          else 'ttp'||(farecell_n - 14) end) order by farecell_n)
   into rdr from farecell
   where loc_n=loc and fs_id=(select fs_id from cnf where loc_n=loc) and fareset_n=1 and included_f='Y';

   if rdr<>'' then
      set rdr=replace(rdr,',','+');
   else
      set rdr='0';
   end if;

   return rdr;
end;
go

grant execute on dba.fnrdrstr to public;
go

////////////////////////////////////////////////////////////////////////
// fnrdrstr2 - get ridership count formula for a location (version 2)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnrdrstr2') then
   drop function dba.fnrdrstr2;
end if;
go

create function dba.fnrdrstr2(tbl varchar(32), loc smallint) returns varchar(8000)
begin
   // to test :select fnrdrstr2('', 1);
   //
   // LG-1006
   // tbl - table name to prefix column
   // loc - location number 
   declare rdr varchar(8000);

   if tbl<>'' then
      set tbl=tbl||'.';
   else
      set tbl='';
   end if;

   select list('isnull('||tbl||(case when farecell_n=0 then 'fare_c'
                                     when farecell_n<10 then 'key'||farecell_n
                                     when farecell_n=10 then 'keyast'
                                     when farecell_n<15 then 'key'||substr('abcd',farecell_n - 10,1)
                                     else 'ttp'||(farecell_n - 14)
								end)||'<comma>0)' order by farecell_n)
   into rdr
   from farecell
   where loc_n=loc and fs_id=(select fs_id from cnf where loc_n=loc) and fareset_n=1 and included_f='Y';

   if rdr<>'' then
      set rdr=replace(rdr,',','+');
   else
      set rdr='0';
   end if;

   set rdr=replace(rdr,'<comma>',',');

   return rdr;
end;
go

grant execute on dba.fnrdrstr2 to public;
go

////////////////////////////////////////////////////////////////////////
// fnttpstr - get token, ticket, or pass count formula for a location
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnttpstr') then
   drop function dba.fnttpstr;
end if;
go

create function dba.fnttpstr(tbl varchar(32), typ smallint, loc smallint) returns varchar(1024)
begin
   // tbl - table name to prefix column
   // typ - 1: token; 2: ticket; 3: pass; anything else: return '0'
   // loc - location number 
   declare cmd varchar(1024);

   if tbl<>'' then
      set tbl=tbl||'.';
   else
      set tbl='';
   end if;

   select list(tbl||'ttp'||m_ndx order by m_ndx) into cmd from media
   where loc_n=loc and fs_id=(select fs_id from cnf where loc_n=loc) and m_ndx>0
     and ((typ=1 and grp=5 and des in (0,1,2,3,4,26)) or (typ=2 and grp=5 and des>=5 and des<>26) or
          (typ=3 and (grp in (1,2,3,4,7) or (grp=0 and (des>0 or trim(text)<>'' or trim(description)<>'')))));

   if cmd<>'' then
      set cmd=replace(cmd,',','+');
   else
      set cmd='0';
   end if;

   return cmd;
end;
go

grant execute on dba.fnttpstr to public;
go

////////////////////////////////////////////////////////////////////////
// fnttpstr2 - get token, ticket, or pass count formula for a location (version 2)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnttpstr2') then
   drop function dba.fnttpstr2;
end if;
go

create function dba.fnttpstr2(tbl varchar(32), typ smallint, loc smallint) returns varchar(8000)
begin
	// to test :select fnttpstr2('', 3, 1);
	//
	// LG-1006
	// tbl - table name to prefix column
	// typ - 1: token; 2: ticket; 3: pass; anything else: return '0'
	// loc - location number 
	declare cmd varchar(8000);

	if tbl <> '' then
		set tbl = tbl||'.';
	else
		set tbl = '';
	end if;

	select list(distinct 'isnull('||tbl||'ttp'||m.m_ndx||'<comma>0)' order by m.m_ndx)
	into cmd
	from media as m
	inner join farecell as fc
		on	m.loc_n = fc.loc_n
		and	m.fs_id = fc.fs_id
		and	m.m_ndx = fc.m_ndx
	inner join attr
		on	fc.attr = attr.attr
	where	m.loc_n = loc
		and	m.fs_id = (select fs_id from cnf where loc_n = loc)
		and	m.m_ndx > 0
		and	(	typ = 1 and grp = 5 and des in (0, 1, 2, 3, 4, 26)
			or	typ = 2 and grp = 5 and des >= 5 and des <> 26
			or	typ = 3 and	(	grp in (1, 2, 3, 4, 7)
							or	grp = 0 and des > 0 and attr.attr = 0
							)
			)
	;

	if cmd <> '' then
		set cmd = replace(cmd, ',', '+');
	else
		set cmd = '0';
	end if;

	set cmd = replace(cmd, '<comma>', ',');

	return cmd;
end;
go

grant execute on dba.fnttpstr2 to public;
go

////////////////////////////////////////////////////////////////////////
// fnintime - perform time range comparison
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnintime') then
   drop function dba.fnintime;
end if;
go

create function dba.fnintime(ts timestamp, tm1 time, tm2 time) returns smallint
begin
   // ts - timestamp to check whether it is within tm1 and tm2
   // tm1, tm2 - time range
   if cast(ts as time) between tm1 and tm2 then
      return 1;                                   // within time range
   else
      return -1;                                  // out of time range
   end if;
end;
go

grant execute on dba.fnintime to public;
go

////////////////////////////////////////////////////////////////////////
// fnchkrrt - perform Route-Run-Trip (RRT) validation
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnchkrrt') then
   drop function dba.fnchkrrt;
end if;
go

create function dba.fnchkrrt(loc smallint, rt integer, rn integer, tp integer, tm timestamp) returns tinyint
begin
   // loc - location number
   // rt, rn, tp - route, run, and trip to be validated
   // tm - time to be checked
   if exists(select 0 from rrtlst where loc_n=loc and route=rt and run=rn and trip=tp and fnintime(tm,tm1,tm2)=1 and active=1) then
      return 1;                                   // RRT is valid or defined
   else
      return 0;                                   // RRT is invalid or not defined
   end if;
end;
go

grant execute on dba.fnchkrrt to public;
go

////////////////////////////////////////////////////////////////////////
// fnbitwise - perform bitwise operation
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='fnbitwise') then
	drop function dba.fnbitwise;
end if;
go

create function dba.fnbitwise(typ char(1), x unsigned integer, y unsigned integer) returns unsigned integer
begin
	// typ - AND (&, A); OR (| O); XOR (^ X)
	// x, y - operands
	return (	case
					when typ in ('&','a','A') then (x & y)
					when typ in ('|','o','O') then (x | y)
					when typ in ('^','x','X') then (x ^ y)
					else 0
				end
			);
end;
go

grant execute on dba.fnbitwise to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getlistname - get gfi_lst.name
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getlistname') then
   drop function dba.gfisf_getlistname;
end if;
go

create function dba.gfisf_getlistname(list_type varchar(16), list_class smallint, list_code integer) returns varchar(256)
begin
   declare list_name varchar(256);

   set list_type=upper(trim(list_type));
   set list_name=null;

   if list_type<>'' and (list_code is not null) then
      if list_class is null then
         select min(class) into list_class from gfi_lst where type=list_type and code=list_code;
         if sqlcode<>0 then
            return list_name;
         end if;
      end if;
      select name into list_name from gfi_lst where type=list_type and class=list_class and code=list_code;
   end if;

   return list_name;
end;
go

grant execute on dba.gfisf_getlistname to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getlistvalue - get gfi_lst.value by name
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getlistvalue') then
   drop function dba.gfisf_getlistvalue;
end if;
go

create function dba.gfisf_getlistvalue(list_type varchar(16), list_class smallint, list_name varchar(256)) returns varchar(4000)
begin
   declare list_value varchar(4000);
   declare list_code  integer;

   set list_type=upper(trim(list_type));
   set list_value=null;

   if list_type<>'' and trim(list_name)<>'' then
      if list_class is null then
         select min(class) into list_class from gfi_lst where type=list_type and name=list_name;
         if sqlcode<>0 then
            return list_value;
         end if;
      end if;

      select min(code) into list_code from gfi_lst where type=list_type and class=list_class and name=list_name;
      if sqlcode=0 then
         select value into list_value from gfi_lst where type=list_type and class=list_class and code=list_code;
      end if;
   end if;

   return list_value;
end;
go

grant execute on dba.gfisf_getlistvalue to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getlistvalue2 - get gfi_lst.value by code
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getlistvalue2') then
   drop function dba.gfisf_getlistvalue2;
end if;
go

create function dba.gfisf_getlistvalue2(list_type varchar(16), list_class smallint, list_code integer) returns varchar(4000)
begin
   declare list_value varchar(4000);

   set list_type=upper(trim(list_type));
   set list_value=null;

   if list_type<>'' and (list_code is not null) then
      if list_class is null then
         select min(class) into list_class from gfi_lst where type=list_type and code=list_code;
         if sqlcode<>0 then
            return list_value;
         end if;
      end if;
      select value into list_value from gfi_lst where type=list_type and class=list_class and code=list_code;
   end if;

   return list_value;
end;
go

grant execute on dba.gfisf_getlistvalue2 to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getlastmlid - get last probing ml.id
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getlastmlid') then
   drop function dba.gfisf_getlastmlid;
end if;
go

create function dba.gfisf_getlastmlid(loc smallint, mlid integer) returns integer
begin
   declare lastid integer;

   set lastid=null;

   select first m1.id into lastid from ml m1, ml m2
   where m1.loc_n=loc and m2.loc_n=loc and m2.id=mlid and m1.bus=m2.bus and m1.ts<=m2.ts and m1.id<>mlid
   order by m1.ts desc;

   return lastid;
end;
go

grant execute on dba.gfisf_getlastmlid to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getlastmlts - get last probing ml.ts
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getlastmlts') then
   drop function dba.gfisf_getlastmlts;
end if;
go

create function dba.gfisf_getlastmlts(loc smallint, mlid integer) returns timestamp
begin
   declare lastts timestamp;

   select max(m1.ts) into lastts from ml m1, ml m2
   where m1.loc_n=loc and m2.loc_n=loc and m2.id=mlid and m1.bus=m2.bus and m1.ts<=m2.ts and m1.id<>mlid;

   return lastts;
end;
go

grant execute on dba.gfisf_getlastmlts to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_iseqvalid - check if an equipment is defined
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_iseqvalid') then
   drop function dba.gfisf_iseqvalid;
end if;
go

create function dba.gfisf_iseqvalid(eqt tinyint, eqn smallint) returns bit
begin
	if exists(select 0 from gfi_eq where eq_type=eqt and eq_n=eqn) then
		return 1;
	else
		return 0;
	end if;
end;
go

grant execute on dba.gfisf_iseqvalid to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_seconds - get difference in seconds between two timestamps
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_seconds') then
   drop function dba.gfisf_seconds;
end if;
go

create function dba.gfisf_seconds(ts1 timestamp, ts2 timestamp) returns int
begin
   return datediff(second,ts1,ts2);
end;
go

grant execute on dba.gfisf_seconds to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getxcnt - get invalid route/run/trip/driver count per probing
//                 loc: location number
//                 mlid: master list record ID
//                 xtype: exception type (1 - driver; 2 - route; 3 - run; 4 - trip; 0 - all)
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getxcnt') then
   drop function dba.gfisf_getxcnt;
end if;
go

create function dba.gfisf_getxcnt(loc smallint, mlid integer, xtype tinyint) returns integer
begin
   declare cnt integer;
   declare li  integer;

   set cnt=0;
   if xtype>=0 and xtype<=4 and exists(select 0 from ev where loc_n=loc and id=mlid and (curr_r>0 or rdr_c>0)) then
      if xtype in (0, 1) then                    // driver
         select count(*) into li from ev
         where loc_n=loc and id=mlid and (curr_r>0 or rdr_c>0) and drv not in (select drv from drvlst where loc_n=loc)
           and (not exists(select 0 from gfi_range where loc_n=loc and type=1 and drv between v1 and v2));
         if li>0 then
            set cnt=cnt+li;
         end if;
      end if;

      if xtype in (0, 2) then                    // route
         select count(*) into li from ev
         where loc_n=loc and id=mlid and (curr_r>0 or rdr_c>0) and route not in (select route from rtelst where loc_n=loc)
           and (not exists(select 0 from gfi_range where loc_n=loc and type=2 and route between v1 and v2));
         if li>0 then
            set cnt=cnt+li;
         end if;
      end if;

      if xtype in (0, 3) then                    // run
         select count(*) into li from ev
         where loc_n=loc and id=mlid and (curr_r>0 or rdr_c>0) and run not in (select run from runlst where loc_n=loc)
           and (not exists(select 0 from gfi_range where loc_n=loc and type=3 and run between v1 and v2));
         if li>0 then
            set cnt=cnt+li;
         end if;
      end if;

      if xtype in (0, 4) then                    // trip
         select count(*) into li from ev
         where loc_n=loc and id=mlid and (curr_r>0 or rdr_c>0) and trip not in (select trip from trplst where loc_n=loc)
           and (not exists(select 0 from gfi_range where loc_n=loc and type=4 and trip between v1 and v2));
         if li>0 then
            set cnt=cnt+li;
         end if;
      end if;
   end if;

   return cnt;
end;
go

grant execute on dba.gfisf_getxcnt to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_islistname - check if list name exists
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_islistname') then
	drop function dba.gfisf_islistname;
end if;
go

create function dba.gfisf_islistname(list_type varchar(16), list_class smallint, list_name varchar(256)) returns tinyint
begin
	if exists(select 0 from gfi_lst where type=upper(trim(list_type)) and class=isnull(list_class,0) and hash(name)=hash(list_name)) then
		return 1;
	elseif exists(select 0 from gfi_lst where type=upper(trim(list_type)) and class=isnull(list_class,0) and upper(trim(name))=upper(trim(list_name))) then
		return 2;
	else
		return 0;
	end if;
end;
go

grant execute on dba.gfisf_islistname to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_islistcode - check if list code exists
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_islistcode') then
	drop function dba.gfisf_islistcode;
end if;
go

create function dba.gfisf_islistcode(list_type varchar(16), list_class smallint, list_code integer) returns bit
begin
	if exists(select 0 from gfi_lst where type=upper(trim(list_type)) and class=isnull(list_class,0) and code=list_code) then
		return 1;
	else
		return 0;
	end if;
end;
go

grant execute on dba.gfisf_islistcode to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getlistcode - get gfi_lst.code by name
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getlistcode') then
	drop function dba.gfisf_getlistcode;
end if;
go

create function dba.gfisf_getlistcode(list_type varchar(16), list_class smallint, list_name varchar(256)) returns integer
begin
	declare li integer;

	select min(code) into li
	from gfi_lst
	where	type=upper(trim(list_type))
		and class=isnull(list_class,0)
		and upper(trim(name))=upper(trim(list_name))
	;
	
	return li;
end;
go

grant execute on dba.gfisf_getlistcode to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_calcsn - calculate serial number
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_calcsn') then
	drop function dba.gfisf_calcsn;
end if;
go

create function dba.gfisf_calcsn(grp tinyint, des tinyint, aid smallint, sc tinyint, mid tinyint, tpbc smallint, seq integer) returns bigint
begin
	return cast(((grp & 3)*32+(des & 31))*72057594037927936+(seq & 16777215)*4294967296+((sc & 15)*4096+(aid & 4095))*65536+(mid & 15)*4096+(tpbc & 4095) as bigint);
end;
go

grant execute on dba.gfisf_calcsn to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_isvalid - check validity of route/run/trip/driver
//                 loc: location number
//                 typ: item type (1 - driver; 2 - route; 3 - run; 4 - trip)
//                 item: item value
//
// Return: 1 - valid; 0 - invalid
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_isvalid') then
   drop function dba.gfisf_isvalid;
end if;
go

create function dba.gfisf_isvalid(loc smallint, typ tinyint, item integer) returns bit
begin
   if exists(select 0 from gfi_range where loc_n=loc and type=typ and item between v1 and v2) then
      return 1;
   end if;

   case typ
      when 1 then
         if exists(select 0 from drvlst where loc_n=loc and drv=item) then
            return 1;
         end if;
      when 2 then
         if exists(select 0 from rtelst where loc_n=loc and route=item) then
            return 1;
         end if;
      when 3 then
         if exists(select 0 from runlst where loc_n=loc and run=item) then
            return 1;
         end if;
      when 4 then
         if exists(select 0 from trplst where loc_n=loc and trip=item) then
            return 1;
         end if;
   end case;

   return 0;
end;
go

grant execute on dba.gfisf_isvalid to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_istrue - check if a list value is true
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_istrue') then
   drop function dba.gfisf_istrue;
end if;
go

create function dba.gfisf_istrue(list_type varchar(16), list_class smallint, list_name varchar(256)) returns bit
begin
   if upper(trim(gfisf_getlistvalue(list_type,list_class,list_name))) in ('TRUE','T','YES','Y','1') then
      return 1;
   else
      return 0;
   end if;
end;
go

grant execute on dba.gfisf_istrue to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_isfalse - check if a list value is false
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_isfalse') then
   drop function dba.gfisf_isfalse;
end if;
go

create function dba.gfisf_isfalse(list_type varchar(16), list_class smallint, list_name varchar(256)) returns bit
begin
   if upper(trim(gfisf_getlistvalue(list_type,list_class,list_name))) in ('FALSE','F','NO','N','0') then
      return 1;
   else
      return 0;
   end if;
end;
go

grant execute on dba.gfisf_isfalse to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_removenonnumchar - remove non-numeric characters from a string
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_removenonnumchar') then
   drop function dba.gfisf_removenonnumchar;
end if;
go

create function dba.gfisf_removenonnumchar(str varchar(4000)) returns varchar(4000)
begin
   declare li integer;

   set li=patindex('%[^0-9]%',str);
   while li>0 loop
      set str=stuff(str,li,1,'');
      set li=patindex('%[^0-9]%',str);
   end loop;

   return str;
end;
go

grant execute on dba.gfisf_removenonnumchar to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_getdistance - calculate distance between two coordinates in meters using haversine algorithm
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_getdistance') then
   drop function dba.gfisf_getdistance;
end if;
go

create function dba.gfisf_getdistance(lat1 double, long1 double, lat2 double, long2 double) returns double
begin
   declare dist double;

   set lat1=power(sin(radians(lat2 - lat1)/2.0),2)+
           (cos(radians(lat1))*cos(radians(lat2))*power(sin(radians(long2 - long1)/2.0),2));

   set dist=6371007.2*2.0*atn2(sqrt(lat1),sqrt(1.0 - lat1));

   return dist;
end;
go

grant execute on dba.gfisf_getdistance to public;
go

////////////////////////////////////////////////////////////////////////
// gfisf_gethostip - retrieve client hostname+' '+ip for the current connection
////////////////////////////////////////////////////////////////////////
if exists(select 1 from sys.sysprocedure where proc_name='gfisf_gethostip') then
   drop function dba.gfisf_gethostip;
end if;
go

create function dba.gfisf_gethostip() returns varchar(32)
begin
   declare hostip varchar(32);
   declare ls     varchar(1024);
   declare li     smallint;

   select trim(connection_property('appinfo',number))||';',
         (case when length(trim(nodeaddr))>0 then trim(nodeaddr) else '' end)
   into ls, hostip
   from sa_conn_info() where dbnumber=db_id() and number=@@spid;

   set li=locate(lower(ls),'host=');
   if li>0 then
      set ls=substr(ls,li+5);
      set ls=trim(left(ls,locate(ls,';') - 1));
      if length(ls)>0 then
         if length(hostip)>0 then
            set hostip=trim(left(ls,31 - length(hostip)))||' '||hostip;
         else
            set hostip=trim(left(ls,32));
         end if;
      end if;
   end if;

   return hostip;
end;
go

grant execute on dba.gfisf_gethostip to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisf_GetPowerBuilderVersion') then
   drop function dba.gfisf_GetPowerBuilderVersion;
end if;
go

////////////////////////////////////////////////////////////////////////
// Author:		Igor Mikhalyev
// Create date: 06/30/2015
// Description:	Get Power Builder Version
////////////////////////////////////////////////////////////////////////
create function dba.gfisf_GetPowerBuilderVersion()
returns varchar(100)
begin
/*
select gfisf_GetPowerBuilderVersion();
*/
	declare resultValue varchar(100);

	select dataValue into resultValue
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'Power Builder Version'
	  and isActive = 1;

	if resultValue is null then
		set resultValue = ''; -- default value
	end if;

	return resultValue;
end;
go

grant execute on dba.gfisf_GetPowerBuilderVersion to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisf_GetCloudSetting') then
   drop function dba.gfisf_GetCloudSetting;
end if;
go

////////////////////////////////////////////////////////////////////////
// Author:		Igor Mikhalyev
// Create date: 02/03/2016
// Description:	Get Cloud Setting	Yes/No
//				LG-1507 - Merge the SQL code for cloud and non cloud branches
////////////////////////////////////////////////////////////////////////
create function dba.gfisf_GetCloudSetting()
returns varchar(100)
begin
/*
select gfisf_GetCloudSetting();
*/
	declare resultValue varchar(100);

	select dataValue into resultValue
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'Cloud'
	  and isActive = 1;

	if isnull(resultValue, '') <> 'Yes' then
		set resultValue = 'No'; -- default value
	end if;

	return resultValue;
end;
go

grant execute on dba.gfisf_GetCloudSetting to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisf_GetFaresetNumber') then
   drop function dba.gfisf_GetFaresetNumber;
end if;
go

////////////////////////////////////////////////////////////////////////
// Author:		Igor Mikhalyev
// Create date: 01/28/2016
// Description:	Get Fareset Number
//				LG-1507 - Merge the SQL code for cloud and non cloud branches
////////////////////////////////////////////////////////////////////////
create function dba.gfisf_GetFaresetNumber()
returns int
begin
/*
select gfisf_GetFaresetNumber();
*/
	declare resultValue int;

	select cast(dataValue as int) into resultValue
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'Fareset Number'
	  and isActive = 1;

	if isnull(resultValue, 0) <= 0 then
		if gfisf_GetCloudSetting() = 'Yes' then
			set resultValue = 20; -- default value for Cloud
		else
			set resultValue = 10; -- default value for Non-Cloud
		end if;
	end if;

	return resultValue;
end;
go

grant execute on dba.gfisf_GetFaresetNumber to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='gfisf_TTP_Number') then
   drop function dba.gfisf_TTP_Number;
end if;
go

////////////////////////////////////////////////////////////////////////
// Author:		Igor Mikhalyev
// Create date: 01/28/2016
// Description:	Get TTP Number
//				LG-1507 - Merge the SQL code for cloud and non cloud branches
////////////////////////////////////////////////////////////////////////
create function dba.gfisf_TTP_Number()
returns int
begin
/*
select gfisf_TTP_Number();
*/
	declare resultValue int;

	select cast(dataValue as int) into resultValue
	from configurationsData
	where configurationID = 0 -- Default
	  and dataName = 'TTP Number'
	  and isActive = 1;

	if isnull(resultValue, 0) <= 0 then
		if gfisf_GetCloudSetting() = 'Yes' then
			set resultValue = 241; -- default value for Cloud
		else
			set resultValue = 48; -- default value for Non-Cloud
		end if;
	end if;

	return resultValue;
end;
go

grant execute on dba.gfisf_TTP_Number to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='fndunint') then
   drop function dba.fndunint;
end if;
go

create function dba.fndunint(loc smallint, ad date, choice smallint)
returns integer
////////////////////////////////////////////////////////////////////////
//
// Function integer fndunint(loc, ad, choice)
//
// Description: Get daily UNKNOWN (ml - ev) value for integer items
//
// Argument: loc - location number; NULL or 0 for all locations
//           ad  - transit date
//           choice - see case statement in codes
//
// Revision: 04/19/1999 Maryann Jordan - initial version
//           12/14/1999 Maryann Jordan - add support for mulitple locations and rewrite to simplify
//           02/09/2005 Mason Liu - rewrite for simplicity and performance (ASA9 and up)
//           LG-1260 - 11/02/2015 Igor Mikhalyev - use rtesum_extd table
//
////////////////////////////////////////////////////////////////////////
// Copyright(c)2008 GFI Genfare. All rights reserved.
////////////////////////////////////////////////////////////////////////
begin
/*
select dba.fndunint(1, '2015-07-02', 50) as result;
select dba.fndunint(1, '2015-07-02', 281) as result;
select dba.fndunint(0, '2015-07-02', 281) as result;
*/
   declare val integer;
   declare col varchar(16);

   set col=(case choice when 1 then 'dump_c'
                        when 2 then 'token_c'
                        when 3 then 'ticket_c'
                        when 4 then 'pass_c'
                        when 5 then 'bill_c'
                        when 6 then 'fare_c'
                        when 30 then 'keyast'
                        else (case when choice between 7 and 16 then 'fare'||(choice - 6)||'_c'
                              when choice between 21 and 29 then 'key'||(choice - 20)
                              when choice between 31 and 34 then 'key'||substr('abcd',choice - 30,1)
                              when choice between 41 and 281 then 'ttp'||(choice - 40)
                              else '0' end) end);
   set loc=isnull(loc,0);

   if loc=0 then
      execute immediate 'select sum('||col||')
      into val
      from rtesum as a
      left join rtesum_extd as b
         on  b.loc_n = a.loc_n
         and b.tday = a.tday
         and b.route = a.route
      where a.tday=ad and a.route=1000001';
   else
      execute immediate 'select sum(isnull('||col||',0))
      into val
      from rtesum as a
      left join rtesum_extd as b
         on  b.loc_n = a.loc_n
         and b.tday = a.tday
         and b.route = a.route
      where a.loc_n = loc and a.tday=ad and a.route=1000001';
   end if;

   return isnull(abs(val),0);
end;
go

grant execute on dba.fndunint to public;
go

if exists(select 1 from sys.sysprocedure where proc_name='fnmunint') then
   drop function dba.fnmunint;
end if;
go

create function dba.fnmunint(loc smallint, ad date, choice smallint)
returns integer
////////////////////////////////////////////////////////////////////////
//
// Function integer fnmunint(loc, ad, choice)
//
// Description: Get monthly UNKNOWN (ml - ev) value for integer items
//
// Argument: loc - location number; NULL or 0 for all locations
//           ad  - transit date (first day of the month)
//           choice - see case statement in codes
//
// Revision: 04/19/1999 Maryann Jordan - initial version
//           07/02/1999 Maryann Jordan - set day of mon_date (ML_mday) has value of 1
//           12/15/1999 Maryann Jordan - add support for mulitple locations and rewrite to simplify
//           02/09/2005 Mason Liu - rewrite for simplicity and performance (ASA9 and up)
//           LG-1260 - 11/02/2015 Igor Mikhalyev - use mrtesum_extd table
//
////////////////////////////////////////////////////////////////////////
// Copyright(c)2008 GFI Genfare. All rights reserved.
////////////////////////////////////////////////////////////////////////
begin
/*
select dba.fnmunint(1, '2015-07-02', 50) as result;
select dba.fnmunint(1, '2015-07-02', 281) as result;
select dba.fnmunint(0, '2015-07-02', 281) as result;
*/
   declare val integer;
   declare col varchar(16);

   set col=(case choice when 1 then 'dump_c'
                        when 2 then 'token_c'
                        when 3 then 'ticket_c'
                        when 4 then 'pass_c'
                        when 5 then 'bill_c'
                        when 6 then 'fare_c'
                        when 30 then 'keyast'
                        else (case when choice between 7 and 16 then 'fare'||(choice - 6)||'_c'
                              when choice between 21 and 29 then 'key'||(choice - 20)
                              when choice between 31 and 34 then 'key'||substr('abcd',choice - 30,1)
                              when choice between 41 and 281 then 'ttp'||(choice - 40)
                              else '0' end) end);
   set loc=isnull(loc,0);
   set ad=ymd(year(ad),month(ad),1);

   if loc = 0 then
      execute immediate 'select sum('||col||')
      into val
      from mrtesum as a
      left join mrtesum_extd as b
         on  b.loc_n = a.loc_n
         and b.mday = a.mday
         and b.route = a.route
      where a.mday=ad and a.route=1000001';
   else
      execute immediate 'select sum('||col||')
      into val
      from mrtesum as a
      left join mrtesum_extd as b
         on  b.loc_n = a.loc_n
         and b.mday = a.mday
         and b.route = a.route
      where a. loc_n = loc and a.mday=ad and a.route=1000001';
   end if;
   return isnull(abs(val),0);
end;
go

grant execute on dba.fnmunint to public;
go


------------------------------------------------------------------------
-- gfisf_tr_subtype - return tr status - GD-2206
------------------------------------------------------------------------
if exists(select null from sys.sysprocedure where proc_name='gfisf_tr_subtype') then
   drop function dba.gfisf_tr_subtype;
end if;
go

create function dba.gfisf_tr_subtype(
	in_tr_etype tinyint
) returns varchar(100)
begin
/*
select gfisf_tr_subtype(128);
select gfisf_tr_subtype(161);
*/
	declare v_result varchar(100);
	declare v_value varchar(100);

	set v_result = '';
	set v_value  = null;
	
	select
		description into v_value
	from event_subtype
	where	etype = in_tr_etype;
	
	if v_value is null then
		set v_value = 'Sub-Type #' + isnull(ltrim(cast(in_tr_etype as varchar(10))), 'null');
	end if;
	
	set v_result = v_value;

	return v_result;
end;
go

grant execute on dba.gfisf_tr_subtype to public
go


------------------------------------------------------------------------
-- gfisf_tr_events - return tr events - GD-2206
------------------------------------------------------------------------
if exists(select null from sys.sysprocedure where proc_name='gfisf_tr_events') then
   drop function dba.gfisf_tr_events;
end if;
go

create function dba.gfisf_tr_events(
	in_tr_etype tinyint,
	in_tr_events unsigned bigint
) returns varchar(8000)
begin
/*
select gfisf_tr_events(128, 515);
select gfisf_tr_events(161, 515);
*/
	declare v_result varchar(8000);
	declare v_value varchar(8000);
	
	set v_result = '';
	set v_value  = '';

	select
		list(description order by id) into v_value
	from event_status
	where	etype = in_tr_etype
		and	id & in_tr_events = id
	;
	
	if v_value = '' then
		set v_value = cast(in_tr_events as varchar(8000));
	end if;

	set v_result = v_value;

	return v_result;
end;
go

grant execute on dba.gfisf_tr_events to public
go

------------------------------------------------------------------------
-- tran detail report - DATARUN-569
------------------------------------------------------------------------
if exists(select null from sys.sysprocedure where proc_name='CharReversal') then
   drop function dba.CharReversal;
end if;
go

create function dba.CharReversal(@inputstring varchar(10))
returns varchar(10)
BEGIN
  DECLARE @i int; 
          Declare @Results varchar(10);
  SET @Results='';
  SET @i = 1;
  WHILE @i <= length(@inputstring) loop
    
      SET @Results = SUBSTRING(@inputstring,@i,1) + @Results;
      SET @i=@i + 1;
    END loop;
RETURN @Results;
END;
go

grant execute on dba.CharReversal to public
go

if exists(select null from sys.sysprocedure where proc_name='DecimalToBinary') then
   drop function dba.DecimalToBinary;
end if;
go

create function dba.DecimalToBinary(
@Input bigint
)
RETURNS varchar(255)
BEGIN
DECLARE @Output varchar(255) ;
declare @var bigint;
If @Input > 0 
then
select mod(@Input , 2)  into @var;
set @Input = convert(varchar(20),@Input );
SET @Output = @Output || @Input;--convert(varchar(20),@Input % 2 );
SET @Input = @Input / 2;
END if;
RETURN dba.CharReversal(@Output);
END;
go

grant execute on dba.DecimalToBinary to public
go


////////////////////////////////////////////////////////////////////////
Update dba.configurationsData set datavalue = 'Complete' where dataname in('GFIDBASA2');
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// END
////////////////////////////////////////////////////////////////////////
