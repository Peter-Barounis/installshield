-- =================
-- SCRIPT START
-- =================

print 'Script start: ' + convert(varchar(12),GETDATE(),114)

-- ===
-- END
-- ===-- ====
-- DATA
-- 
-- $Author: rbecker $
-- $Revision: 1.139 $
-- $Date: 2017-02-17 17:20:40 $
-- ====

set quoted_identifier on
go

set ansi_nulls on
go

set ansi_padding on
go

set nocount on;
go

------------------------------------------------------------------------
-- * * * L I S T   D E F I N I T I O N S * * *
------------------------------------------------------------------------
exec gfisp_deltype 'TPBC'                         -- obsolete lists
exec gfisp_deltype 'PROXY'
exec gfisp_deltype 'ALARM'
exec gfisp_deltype 'TVM IP'
exec gfisp_deltype 'SYS SUM'
exec gfisp_deltype 'VALID RANGE'
exec gfisp_deltype 'EPAY NOTIFY TYPE'
exec gfisp_deltype 'VALID USER TYPE'
exec gfisp_deltype 'FAREBOX LANGUAGE'
exec gfisp_deltype 'GENDER'
exec gfisp_deltype 'DB VER'
go

-- format: column=display label <value range> (range default is from 1 to 2^31 - 1, for code only)
--         column.width=value
-- flag: bit0-3 - hide class/code/name/value;
--       bit4-7 - readonly class/code/name/value (for existing rows only);
--       bit8   - do not allow add (256)
--       bit9   - do not allow delete (512) (delete new rows is always OK)
--       bit10  - readonly (no modify, no add, no delete) (1024)
--       bit11  - name in lower case
--       bit12  - name in upper case
-- flag default: 0x0400
exec gfisp_addtype 'ACCESSIBILITY',   'Handicaps for users at farebox (code=Code <1-255>; name=Description; value=Note; logo=Step!; flag=0x0221)'
exec gfisp_addtype 'AUT',             'Transit Authority (code=ID <1-99>; name=Name; value=Description; logo=gfitransit.ico; flag=0x01)'
exec gfisp_addtype 'AUTOLOAD TYPE',   'Autoload Type (code=Type <1-255>; name=Description; value=Note; logo=Where!; flag=0x0321)'
exec gfisp_addtype 'BIN ID',          'Bin ID and S/N Cross-Reference List (class=Transit Authority; code=ID <0-3999>; name=S/N; logo=Join!; flag=0x38)'
exec gfisp_addtype 'CBX ID',          'Cashbox ID and S/N Cross-Reference List (class=Transit Authority; code=ID <0-3999>; name=S/N; logo=Join!; flag=0x38)'
exec gfisp_addtype 'CARD TYPE',       'Card Category (code=Category <1-255>; name=Description; value=Note; logo=gfimedia.ico; flag=0x0321)'
exec gfisp_addtype 'CARD TYPE2',      'Card Brand Type (code=Type <0-255>; name=Description; value=Note; logo=gfimedia.ico; flag=0x0361)'
exec gfisp_addtype 'CC TYPE',         'Credit Card Type (code=Type <0-255>; name=Name; value=Description; logo=gfimedia.ico; flag=0x0321)'
exec gfisp_addtype 'CHANNEL',         'Sales and Service Channel (code=ID <1-255>; name=Description; value=Note; logo=OleGenReg!; flag=0x0321)'
exec gfisp_addtype 'COLOR',           'Color Code (code=ID; name=Code; value=Description; logo=ArrangeIcons!; flag=0x0401)'
exec gfisp_addtype 'CONTACT',         'Contact type (code=Code <1-255>; name=Name; value=Note; logo=mrmgr.ico; flag=0x0321)'
exec gfisp_addtype 'COUNTRY',         'Country Code (code=ID <1-255>; name=Code; value=Name; logo=Start!; flag=0x0401)'
exec gfisp_addtype 'CS STATUS',       'Socket Connection Status between Network Manager Server and Data System Server (code=Location; name="Status (1/0 - connected/disconnected)"; name.width=923; value=Description; logo=Join!; flag=0x0401)'
exec gfisp_addtype 'EMAIL',           'Mailing List (code=ID; name=Name; value=Email addresses (use semicolon to delimit); value.width=2299; logo=gfiemail.ico; flag=0x21)'
exec gfisp_addtype 'EPAY ACT TYPE',   'ePay Activity Type (code=Type <1-255>; name=Description; value=Note; logo=Where!; flag=0x0321)'
exec gfisp_addtype 'EPAY LABEL',      'ePay Custom Label and Message (code=ID; name=Standard Text; value=Custom Text; logo=EditMask5!; flag=0x0321)'
exec gfisp_addtype 'EPAY NOTIFY FLAG','ePay Notification Control Flag by Type (class=Switch; class.width=215; code=Type <1-255>; name=Description; name.width=1600; logo=Start!; flag=0x03E8)'
exec gfisp_addtype 'EPAY NOTIFY TYPE','ePay Notification Type (code=Type <1-255>; name=Description; name.width=869; value=Note; value.width=2299; logo=Uncomment!; flag=0x0401)'
exec gfisp_addtype 'EPAY PARM',       'ePay Parameter (name=Parameter; value=Value; logo=EditFuncDeclare!; flag=0x0343)'
exec gfisp_addtype 'EPAY PARM HELP',  'ePay Parameter Descriptor (name=Parameter; value=Descriptor (data type;description))'
exec gfisp_addtype 'EPAY RIGHT',      'ePay Access Right (code=ID; name=Right; value=Description; logo=gfisecurity.ico; flag=0x0361)'
exec gfisp_addtype 'EQ LOC',          'TVM/PEM Station (class=Transit Authority; code=ID <1-255>; name=Name; value="Description (name, address, etc.)"; logo=gfistation.ico)'
exec gfisp_addtype 'EQ_NAME',         'Equipment names used in reporting card history'
exec gfisp_addtype 'EQ STATUS',       'TVM/PEM Status Type (class=Color; class.width=302; code=Type <1-255>; code.width=151; name=Label; value=Description; value.width=2299; logo=gfilview.ico; flag=0x0320)'
exec gfisp_addtype 'EQ TYPE',         'Equipment Type (code=Type <1-255>; name=Name; value=Description; logo=gfieqpem16.ico; flag=0x0401)'
exec gfisp_addtype 'EV CLASS',        'Event Category (code=Category <1-255>; name=Label; value=Description; logo=Count!; flag=0x21)'
exec gfisp_addtype 'EV TYPE',         'Event Type (class=Event Category; class.width=654; code=Type <1-9999>; code.width=151; name=Description; name.width=864; value=Note; logo=EnglishEdit!; flag=0x0320)'
exec gfisp_addtype 'EXACT PAY TYPE',  'EXACT Payment Type (code=ID <1-255>; name=Type; value=Description; logo=gfimedia.ico; flag=0x1361)'
-- TFR CRUFT This type and the field it defines in the fare table are CRUFT
exec gfisp_addtype 'FARE TYPE',       'Fare Type (code=Type; name=Description; value=Note; logo=gfirider.ico; flag=0x0321)'
exec gfisp_addtype 'FIS PAY TYPE',    'FIS Payment Type (code=ID <1-255>; name=Type; value=Description; logo=gfimedia.ico; flag=0x1361)'
exec gfisp_addtype 'FS PEAK TIME',    'Fare Structure - Peak Time (class=Media; code=ID <1-255>; name=Peak On Time; value=Peak Off Time; logo=ShowWatch!; flag=0x22)'
exec gfisp_addtype 'FS HOLIDAY',      'Fare Structure - Holiday (class=Media; code=ID <1-255>; name=Name; value=Value; logo=ComputeToday5!; flag=0x22)'
exec gfisp_addtype 'INSP CODE',       'Inspection Code (code=Code; name=Label; value=Description; logo=EditDataTabular!; flag=0x0321)'
exec gfisp_addtype 'INST TYPE',       'Institution Type (code=Type <1-255>; name=Description; value=Note; logo=Structure!; flag=0x21)'
exec gfisp_addtype 'ISSU CODE',       'Infraction Inspection Issuance Code (code=Code; name=Label; value=Description; logo=DosEdit!; flag=0x21)'
exec gfisp_addtype 'L2G PARM',        'FIS Parameter (name=Parameter; value=Value; logo=l2g16.ico; flag=0x0343)'
exec gfisp_addtype 'L2G PARM HELP',   'FIS Parameter Descriptor (name=Parameter; value=Descriptor (data type;description))'
exec gfisp_addtype 'LANGUAGE',        'Language (code=Code <1-255>; name=Name; value=Note; logo=Globals!; flag=0x0221)'
exec gfisp_addtype 'GENDER',          'Gender (code=Code <1-255>; name=Name; value=Note; logo=Globals!; flag=0x0221)'
exec gfisp_addtype 'MNT CODE',        'Maintenance Code (code=Code <1-255>; name=Label; value=Description; logo=CreateLibrary!; flag=0x21)'
exec gfisp_addtype 'MOD STATUS',      'Module Status Code (code=Code <1-255>; name=Label; value=Description; logo=InsertReturn!; flag=0x21)'
exec gfisp_addtype 'MOD TYPE',        'Module Type (code=Type <1-255>; name=Label; value=Description; logo=InsertReturn!; flag=0x0361)'
exec gfisp_addtype 'NOTIFY TYPE',     'Notification Type (code=Type <1-255>; name=Label; value=Description; logo=gfiemail.ico; flag=0x0321)'
exec gfisp_addtype 'PASS CLASS',      'Passenger Type (code=Type <1-255>; name=Label; value=Description; logo=gfigroup.ico; flag=0x0321)'
exec gfisp_addtype 'PASS TYPE',       'Card Type (code=Type <1-255>; name=Label; value=Description; logo=gfimedia.ico; flag=0x21)'
exec gfisp_addtype 'PAY TYPE',        'Payment Type (code=Type <1-255>; name=Label; value=Description; logo=Custom048!; flag=0x0321)'
exec gfisp_addtype 'PDP PARM',        'Bluetooth Probe Parameter (code=ID; name=Parameter; value=Value; logo=bluetooth.ico; flag=0x0343)'
exec gfisp_addtype 'PDP PARM HELP',   'Bluetooth Probe Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;description))'
exec gfisp_addtype 'PROD TYPE',       'Product Type (code=Type <1-255>; name=Description; value=Note; logo=UserObject5!; flag=0x0321)'
exec gfisp_addtype 'PWD QUESTION',    'Password Security Question (code=ID <1-255>; code.width=151; name=Question; name.width=2094; value=Note; logo=Help!; flag=0x21)'
exec gfisp_addtype 'RAIL LINE',       'Rail Line (code=ID; code.width=151; name=Name; value=Description; value.width=2048; logo=gfirail.ico; flag=0x21)'
exec gfisp_addtype 'RAIL LN-ST-ZN',   'Rail Line-Station-Zone (class=Rail Line; code=GFI Station ID; name=Rail Fare Zone; value=Sort Order; logo=gfilnstz16.ico; flag=0)'
exec gfisp_addtype 'RAIL STATION',    'Rail Station (class=Transit Station ID; code=GFI Station ID; name=Name; value=Display Text; logo=gfistation.ico; flag=0)'
exec gfisp_addtype 'REASON CODE',     'Reason Code (code=Reason Code <0-255>; name=Description; value=Note; logo=CreateLibrary!; flag=0x21)'
exec gfisp_addtype 'RFC TYPE',        'Reduced Fare Contact Type (code=Code <1-255>; name=Name; value=Note; logo=Globals!; flag=0x0221)'
exec gfisp_addtype 'SHIPPER',         'Shipping Service (code=ID <1-255>; name=Name; value=Description; logo=CreateRuntime!; flag=0x21)'
exec gfisp_addtype 'SHROUD',          'Vault Shroud Interface (class=Location; code=Vault Number <1-8>; name=Enabled; logo=shroud.ico; flag=0x0328)'
exec gfisp_addtype 'STATE',           'State/Province Code (class=Country; class.width=434; code=ID <1-255>; code.width=151; name=Code; name.width=151; value=Name; value.width=850; logo=Start!)'
exec gfisp_addtype 'STOCK TYPE',      'Stock Type (code=Type <1-255>; name=Label; value=Description; logo=IncrementalBuild!; flag=0x0321)'
exec gfisp_addtype 'SUM LABEL',       'Summary Item Display Label (class=TVM/PEM; class.width=265; code=ID; name=Code; name.width=215; value=Display text; logo=ComputeSum!; flag=0x0360)'
exec gfisp_addtype 'SYS PARM',        'System Parameter (code=ID; name=Parameter; value=Value; logo=EditFuncDeclare!; flag=0x0343)'
exec gfisp_addtype 'SYS PARM HELP',   'System Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;description))'
exec gfisp_addtype 'TVM SUM',         'TVM System Summary for the Current Day (class=Transit Authority; code=ID; name=Label Code (SUM LABEL); value=Value; logo=gfieqtvm.ico)'
exec gfisp_addtype 'PEM SUM',         'PEM System Summary for the Current Day (class=Transit Authority; code=ID; name=Label Code (SUM LABEL); value=Value; logo=gfieqpem16.ico)'
exec gfisp_addtype 'TVM PARM',        'TVM Parameter (code=ID; name=Parameter; value=Value; logo=gfieqtvm.ico; flag=0x0343)'
exec gfisp_addtype 'TVM PARM HELP',   'TVM Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;TVM specific;description))'
exec gfisp_addtype 'PEM PARM',        'PEM Parameter (code=ID; name=Parameter; value=Value; logo=gfieqpem16.ico; flag=0x0343)'
exec gfisp_addtype 'PEM PARM HELP',   'PEM Parameter Descriptor (code=ID; name=Parameter; value=Descriptor (data type;PEM specific;description))'
exec gfisp_addtype 'TVM PARM SEQ',    'TVM Parameter File List (code=Sequence Number <1-10>; name=Name; value=Effective Date; logo=gfieqtvm.ico; flag=0x01)'
exec gfisp_addtype 'PEM PARM SEQ',    'PEM Parameter File List (code=Sequence Number <1-10>; name=Name; value=Effective Date; logo=gfieqpem16.ico; flag=0x01)'
exec gfisp_addtype 'USER ROLE',       'ePay User Role (code=Role <1-255>; name=Description; value=Note; logo=mrmgr.ico; flag=0x0321)'
exec gfisp_addtype 'USER TYPE',       'ePay User Type (code=Type <0-255>; name=Description; value=Note; logo=mrmgr.ico; flag=0x0321)'
exec gfisp_addtype 'WP REBATE',       'Rebate program for WTA (name=Parameter; value=Value; logo=EditFuncDeclare!; flag=0x0343)'
exec gfisp_addtype 'EXEMPT FARE_ID',  'Fare_ID to use by default on exempt orders'	-- LG-1226
-- GD-1936
exec gfisp_addtype 'ALN OLD PENDING',   '|NM Autoload Notifications - Old Pending Autoloads';
exec gfisp_addtype 'ALN UNEXECUTED',    '|NM Autoload Notifications - Unexecuted Autoloads';
exec gfisp_addtype 'ALN FAILED',        '|NM Autoload Notifications - Failed Autoloads';
exec gfisp_addtype 'ALN SMARTCARDUSE',  '|NM Autoload Notifications - Smart Card Ridership';
exec gfisp_addtype 'ALN SUCCESS',       '|NM Autoload Notifications - Number of Completed Autoloads';
exec gfisp_addtype 'ALN FARE BOXES',    '|NM Autoload Notifications - Number of Probed Fareboxes';
exec gfisp_addtype 'TOKEN VALUE',       'Cash value of tokens'		-- DATARUN-11

go

------------------------------------------------------------------------
-- List: ACCESSIBILITY
------------------------------------------------------------------------
exec gfisp_additem 'ACCESSIBILITY', 'None',              0, 0, 'Active'
exec gfisp_additem 'ACCESSIBILITY', 'Visually Impaired', 0, 1, 'Active'
exec gfisp_additem 'ACCESSIBILITY', 'Hearing Impaired',  0, 2, 'Active'
go

------------------------------------------------------------------------
-- Setup default authority and garage
------------------------------------------------------------------------
if not exists(select null from aut)
	insert into aut (aut_n, aut_name) values(1,'Transit authority 1');

if not exists(select null from cnf)
	insert into cnf (loc_n, loc_name, authority)
	select 1,'Location 1',aut_name from aut where aut_n=(select min(aut_n) from aut);

if not exists(select null from aut_cnf)
	insert into aut_cnf (aut_n, loc_n)
	select aut_n, loc_n from aut, cnf where aut_n=(select min(aut_n) from aut);
go

declare @aut   smallint

------------------------------------------------------------------------
-- List: AUT
------------------------------------------------------------------------
exec gfisp_delitem 'AUT',null,null,0;              -- remove "Undefined" authority
                                                   -- if no authority has been defined, migrate from aut table
if not exists(select null from gfi_lst where type='AUT' and code>0)
   insert into gfi_lst (type,class,code,name,value)
   select distinct 'AUT',0,aut.aut_n,ltrim(rtrim(aut_name)),ltrim(rtrim(aut_name)) from aut, aut_cnf where aut.aut_n=aut_cnf.aut_n;

-- use the first authority as the default one
select @aut=isnull(min(code),1) from gfi_lst where type='AUT' and class=0 and code>0;

------------------------------------------------------------------------
-- List: EQ LOC
------------------------------------------------------------------------
begin transaction
                                                  -- delete stations without transit authorities
delete from gfi_lst
where type = 'EQ LOC'
  and class not in (select code
                    from gfi_lst
					where type = 'AUT');
                                                  -- add "Undefined" stations, one for each transit authority
insert into gfi_lst (type,class,code,name,value)
select 'EQ LOC',code,0,'Undefined','Undefined TVM/PEM station'
from gfi_lst as aut
where type = 'AUT'
  and class = 0
  and not exists(select null from gfi_lst
                 where type = 'EQ LOC'
				   and class = aut.code
				   and code = 0);

commit transaction

if not exists(select null from gfi_lst where type='EQ LOC' and code>0)
   exec gfisp_additem 'EQ LOC','Station 1',@aut,1,'Station 1'
go

------------------------------------------------------------------------
-- List: AUTOLOAD TYPE
------------------------------------------------------------------------
exec gfisp_additem 'AUTOLOAD TYPE','Dummy type',            0, 0,'No action'
exec gfisp_additem 'AUTOLOAD TYPE','Add value',             0, 1,'Recharage'
exec gfisp_additem 'AUTOLOAD TYPE','Add product',           0, 2
exec gfisp_additem 'AUTOLOAD TYPE','Card registration',     0, 3,'Mark card registered'
exec gfisp_additem 'AUTOLOAD TYPE','Card unregistration',   0, 4,'Remove card registration mark'
exec gfisp_additem 'AUTOLOAD TYPE','Card badlist',          0, 5,'Mark card bad'
exec gfisp_additem 'AUTOLOAD TYPE','Card goodlist',         0, 6,'Remove bad card mark'
exec gfisp_additem 'AUTOLOAD TYPE','Product badlist',       0, 7,'Mark product bad'
exec gfisp_additem 'AUTOLOAD TYPE','Product goodlist',      0, 8,'Remove bad product mark'
exec gfisp_additem 'AUTOLOAD TYPE','Set load sequence',     0, 9,'Set individual load sequence number on card'
exec gfisp_additem 'AUTOLOAD TYPE','Set product start date',0,10,'For period product only'
exec gfisp_additem 'AUTOLOAD TYPE','Set language',          0,11,'Set language code on card'
exec gfisp_additem 'AUTOLOAD TYPE','Set product designator',0,12
exec gfisp_additem 'AUTOLOAD TYPE','Set product exp type',  0,13,'For period product only'
exec gfisp_additem 'AUTOLOAD TYPE','Set product priority',  0,14,'Set product priority and condition byte (see DESFire format document appendix E)'
exec gfisp_additem 'AUTOLOAD TYPE','Set User exp date',     0,15,'Set User profile expiration date'
exec gfisp_additem 'AUTOLOAD TYPE','Set product flag',      0,16,'Set product tied to user profile flag'
exec gfisp_additem 'AUTOLOAD TYPE','Clear product flag',    0,17,'Clear product tied to user profile flag'
exec gfisp_additem 'AUTOLOAD TYPE','Set accessibility',     0,18,'Set accessibility on card'
go

------------------------------------------------------------------------
-- List: CARD TYPE
------------------------------------------------------------------------
exec gfisp_additem 'CARD TYPE','Period card',0,1
exec gfisp_additem 'CARD TYPE','Ride card',  0,2
exec gfisp_additem 'CARD TYPE','Value card', 0,3
go

------------------------------------------------------------------------
-- List: CARD TYPE2
------------------------------------------------------------------------
exec gfisp_additem 'CARD TYPE2','|Magnetic',         0, 0
exec gfisp_additem 'CARD TYPE2','|MiFare DESFire',   0, 1
exec gfisp_additem 'CARD TYPE2','|MiFare Ultralight',0, 2
exec gfisp_additem 'CARD TYPE2','|MiFare Classic',   0, 3

delete from gfi_lst
where type = 'CARD TYPE2'
  and class = 0
  and code not in (0,1,2,3);
go

------------------------------------------------------------------------
-- List: CC TYPE
------------------------------------------------------------------------
exec gfisp_additem 'CC TYPE','Undefined',0,0,'Undefined credit card type'
exec gfisp_additem 'CC TYPE','VISA',     0,1,'VISA'
exec gfisp_additem 'CC TYPE','Master',   0,2,'Master Card'
exec gfisp_additem 'CC TYPE','Discover', 0,3,'Discover Card'
exec gfisp_additem 'CC TYPE','AMEX',     0,4,'American Express'
go

------------------------------------------------------------------------
-- List: CHANNEL
------------------------------------------------------------------------
exec gfisp_additem 'CHANNEL','Ticket Vending Machine',            0, 1,'|5-1'
exec gfisp_additem 'CHANNEL','Retail Outlets',                    0, 2,'|9-1'
exec gfisp_additem 'CHANNEL','Customer Service',                  0, 3,'|9'
exec gfisp_additem 'CHANNEL','Pass-by-Mail',                      0, 4,'|9'
exec gfisp_additem 'CHANNEL','Web - Individual',                  0, 5,'|5-3'
exec gfisp_additem 'CHANNEL','Web - Bulk',                        0, 6,'|5-3'
exec gfisp_additem 'CHANNEL','Farebox or SCRV',                   0, 7,'|13-1,13-5'
exec gfisp_additem 'CHANNEL','Administration (Walk in Sales)',    0, 8,'|9'
exec gfisp_additem 'CHANNEL','Special Services',                  0, 9,'|9-3'
exec gfisp_additem 'CHANNEL','Registered Organization via RPOS',  0,10,'|9-1'
exec gfisp_additem 'CHANNEL','Administration (System Management)',0,11,'|9-2'
go

------------------------------------------------------------------------
-- List: COLOR
------------------------------------------------------------------------
exec gfisp_additem 'COLOR','|g',  0,0,'Green'
exec gfisp_additem 'COLOR','|b',  0,1,'Blue'
exec gfisp_additem 'COLOR','|y',  0,2,'Yellow'
exec gfisp_additem 'COLOR','|r',  0,3,'Red'
exec gfisp_additem 'COLOR','|w',  0,4,'White'
exec gfisp_additem 'COLOR','|amb',0,5,'Amber'
exec gfisp_additem 'COLOR','|uv', 0,6,'Ultra-violet'
go

------------------------------------------------------------------------
-- List: CONTACT
------------------------------------------------------------------------
exec gfisp_additem 'CONTACT','|Home',    0,  1,'Home address, phone, etc.'
exec gfisp_additem 'CONTACT','|Work',    0,  2,'Work address, phone, etc.'
exec gfisp_additem 'CONTACT','|School',  0,  3,'School address, phone, etc.'
exec gfisp_additem 'CONTACT','|Shipping',0,200,'Shipping address, phone, etc.'
exec gfisp_additem 'CONTACT','|Billing', 0,201,'Billing address, phone, etc.'
go

------------------------------------------------------------------------
-- List: COUNTRY
------------------------------------------------------------------------
exec gfisp_additem 'COUNTRY','|US',0,1,'United States'
exec gfisp_additem 'COUNTRY','|CA',0,2,'Canada'
exec gfisp_additem 'COUNTRY','|MX',0,3,'Mexico'
go

------------------------------------------------------------------------
-- List: EPAY ACT TYPE
------------------------------------------------------------------------
exec gfisp_additem 'EPAY ACT TYPE','New card',           0, 0
exec gfisp_additem 'EPAY ACT TYPE','Add value',          0, 1
exec gfisp_additem 'EPAY ACT TYPE','Add product',        0, 2
exec gfisp_additem 'EPAY ACT TYPE','Card registration',  0, 3
exec gfisp_additem 'EPAY ACT TYPE','Card unregistration',0, 4
exec gfisp_additem 'EPAY ACT TYPE','Card badlist',       0, 5
exec gfisp_additem 'EPAY ACT TYPE','Card goodlist',      0, 6
exec gfisp_additem 'EPAY ACT TYPE','Product badlist',    0, 7
exec gfisp_additem 'EPAY ACT TYPE','Product goodlist',   0, 8
exec gfisp_additem 'EPAY ACT TYPE','Product usage',      0, 9
exec gfisp_additem 'EPAY ACT TYPE','Payment',            0,10
exec gfisp_additem 'EPAY ACT TYPE','Card fee',           0,11
go

------------------------------------------------------------------------
-- List: EPAY NOTIFY TYPE
------------------------------------------------------------------------
exec gfisp_additem 'EPAY NOTIFY TYPE','|User account creation by e-GO',     0, 1,'|When a user account is created in e-GO'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User account deletion',             0, 2,'|When a user account deleted (logical deletion)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User contact modification',         0, 3,'|When a user contact information are modified'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Failed user login attempt',         0, 4,'|When a user fails to login (incorrect password, expired account, etc.)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User account modification',         0, 5,'|When a user account information are modified (exclude contact information)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User password change',              0, 6,'|When a user''s password is modified'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Forgotten login ID',                0, 7,'|When a user forgets his/her login ID'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Forgotten password',                0, 8,'|When a user forgets his/her password (a system generated temporary password is sent)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User billing modification',         0, 9,'|When a user''s billing information are modified'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Credit card expiration',            0,10,'|When a user credit card on file is about to expire'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User account creation by 3rd party',0,11,'|When a user account is created by a third party system'

exec gfisp_additem 'EPAY NOTIFY TYPE','|Card registration - request',       0,21,'|When a user requests that a card to be registered to his/her account'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card lost or stolen',               0,22,'|When a card is reported lost or stolen'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card unregistration - request',     0,23,'|When a user requests that a card to be unregistered from his/her account'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card badlist - submit',             0,24,'|When a card badlist request is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card goodlist - submit',            0,25,'|When a card goodlist request is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card found',                        0,26,'|When a lost card is found'
exec gfisp_additem 'EPAY NOTIFY TYPE','|New card order - create',           0,27,'|When a user orders new card(s) with or without product(s)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Recharge order - create',           0,28,'|When a user orders recharge(s) for existing product(s)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|New product order - create',        0,29,'|When a user orders new product(s) for existing card(s)'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Negative product balance',          0,30,'|When a product balance becomes negative from 0 or a positive value'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Product badlist - submit',          0,31,'|When a product badlist request is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Product goodlist - submit',         0,32,'|When a product goodlist request is submitted via autoload'

exec gfisp_additem 'EPAY NOTIFY TYPE','|Card registration - submit',        0,33,'|When a card registration request is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card registration - fulfill',       0,34,'|When a card registration request is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card unregistration - submit',      0,35,'|When a card unregistration request is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card unregistration - fulfill',     0,36,'|When a card unregistration request is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Card badlist - fulfill',            0,37,'|When a card badlist request is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|card goodlist - fulfill',           0,38,'|When a card goodlist request is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Product badlist - fulfill',         0,39,'|When a card badlist request is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Product goodlist - fulfill',        0,40,'|When a card goodlist request is fulfilled by an end device'

exec gfisp_additem 'EPAY NOTIFY TYPE','|New card order - submit',           0,41,'|When a new card order item is submitted'
exec gfisp_additem 'EPAY NOTIFY TYPE','|New card order - fulfill',          0,42,'|When a new card order item is fulfilled'
exec gfisp_additem 'EPAY NOTIFY TYPE','|New card order - ship',             0,43,'|When a fulfilled new card order item is shipped'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Auto-replenishment - pre-notice',   0,44,'|When auto-replenishment threshold is reached, notify the card holder 24 hours (configurable) before replenishment'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Auto-replenishment - submit',       0,45,'|When an auto-replenishment order item is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Order cancelled',                   0,46,'|When an order or an order item is cancelled'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Auto-replenishment - fulfill',      0,47,'|When auto-replenishment is completed successfully'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Auto-replenishment failure',        0,48,'|When auto-replenishment failed'

exec gfisp_additem 'EPAY NOTIFY TYPE','|Recharge order - submit',           0,49,'|When a recharge order item is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Recharge order - fulfill',          0,50,'|When a recharge order item is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|New product order - submit',        0,51,'|When a new product order item is submitted via autoload'
exec gfisp_additem 'EPAY NOTIFY TYPE','|New product order - fulfill',       0,52,'|When a new product order item is fulfilled by an end device'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Auto-replenishment - fulfill',      0,53,'|When an auto-replenishment order item is fulfilled by an end device'

exec gfisp_additem 'EPAY NOTIFY TYPE','|Institution registration',          0,60,'|When an institution is registered'
exec gfisp_additem 'EPAY NOTIFY TYPE','|User password set by Admin',        0,61,'|When a users password is directly set by Admin'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Registration Rebate - Fulfill',     0,62,'|When a registration rebate is fulfilled via the system'

exec gfisp_additem 'EPAY NOTIFY TYPE','|Feedback submitted',                0,63,'|When a user submits the Feedback Form'
exec gfisp_additem 'EPAY NOTIFY TYPE','|Auto-replenishment invoice failure',0,64,'|When auto-replenishment invoice failed for an Org'
go

------------------------------------------------------------------------
-- List: EPAY NOTIFY TYPE and EPAY NOTIFY FLAG
------------------------------------------------------------------------
begin transaction

delete from gfi_lst
where type = 'EPAY NOTIFY FLAG'
  and code not in (select distinct code
                   from gfi_lst
				   where type = 'EPAY NOTIFY TYPE');

insert into gfi_lst (type,class,code,name)
select distinct 'EPAY NOTIFY FLAG',1,code,name
from gfi_lst as t
where type = 'EPAY NOTIFY TYPE'
  and not exists(select null from gfi_lst
                 where type = 'EPAY NOTIFY FLAG'
				   and code = t.code);

commit transaction
go

------------------------------------------------------------------------
-- List: EPAY PARM
------------------------------------------------------------------------
exec gfisp_additem 'EPAY PARM','|AR AMT MIN',       0,null,'500'
exec gfisp_additem 'EPAY PARM','|AR AMT MAX',       0,null,'65535'
exec gfisp_additem 'EPAY PARM','|LOCKOUT THRESHOLD',0,null,'0'
exec gfisp_additem 'EPAY PARM','|LOCKOUT WAIT TIME',0,null,'30'
exec gfisp_additem 'EPAY PARM','|PWD ALPHANUMERIC', 0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|PWD DICTIONARY',   0,null,'No'
exec gfisp_additem 'EPAY PARM','|PWD EXPIRATION',   0,null,'0'
exec gfisp_additem 'EPAY PARM','|PWD HISTORY',      0,null,'5'
exec gfisp_additem 'EPAY PARM','|PWD MIN LENGTH',   0,null,'8'
exec gfisp_additem 'EPAY PARM','|PWD MIX',          0,null,'No'
exec gfisp_additem 'EPAY PARM','|AGENCY ID',        0,null,'0'
exec gfisp_additem 'EPAY PARM','|MANUFACTURER ID',  0,null,'5'
exec gfisp_additem 'EPAY PARM','|SEC CODE SMART',   0,null,'5'
exec gfisp_additem 'EPAY PARM','|SEC CODE MAG',     0,null,'1'
exec gfisp_additem 'EPAY PARM','|AUTO REP',         0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|GUEST AUTO REP',   0,null,'No'
exec gfisp_additem 'EPAY PARM','|GUEST RECHARGE',   0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|FEE PICC',         0,null,'0'
exec gfisp_additem 'EPAY PARM','|FEE PICC LU',      0,null,'0'
exec gfisp_additem 'EPAY PARM','|FEE PICC DESC',    0,null,'New Card Fee'
exec gfisp_additem 'EPAY PARM','|FEE PICC LU DESC', 0,null,'New Ticket Fee'
exec gfisp_additem 'EPAY PARM','|CARD EXP DATE',    0,null,'4y'
exec gfisp_additem 'EPAY PARM','|CARD EXP DATE 2',  0,null,'4m'
exec gfisp_additem 'EPAY PARM','|DEFAULT COUNTRY',  0,null,'US'
exec gfisp_additem 'EPAY PARM','|DEFAULT STATE',    0,null,'AZ'
exec gfisp_additem 'EPAY PARM','|RECHARGE MAX EXP', 0
exec gfisp_additem 'EPAY PARM','|PROD LIMIT 1',     0,null,'2'
exec gfisp_additem 'EPAY PARM','|PROD LIMIT 2',     0,null,'2'
exec gfisp_additem 'EPAY PARM','|PROD LIMIT 3',     0,null,'2'
exec gfisp_additem 'EPAY PARM','|PROD LIMIT 4',     0,null,'2'
exec gfisp_additem 'EPAY PARM','|PENDING LIMIT 1',  0,null,'3'
exec gfisp_additem 'EPAY PARM','|PENDING LIMIT 2',  0,null,'3'
exec gfisp_additem 'EPAY PARM','|PENDING LIMIT 3',  0,null,'3'
exec gfisp_additem 'EPAY PARM','|PENDING LIMIT 4',  0,null,'3'
exec gfisp_additem 'EPAY PARM','|RECHARGE ALLOW 1', 0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|RECHARGE ALLOW 2', 0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|RECHARGE ALLOW 3', 0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|RECHARGE ALLOW 4', 0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|ROOT PATH', 0, null,'D:\Software\ePay'

exec gfisp_additem 'EPAY PARM','|AUTO REP USER GROUPING',0,null,'No'
exec gfisp_additem 'EPAY PARM','|CAPTCHA IMAGE',    0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|NUM OF PRODUCTS ON CARD',0,null,'3'
exec gfisp_additem 'EPAY PARM','|PURCHASE WITHOUT LOGIN', 0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|DATE OF BIRTH',    0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|SESSION TIMEOUT',  0,null,'10'
exec gfisp_additem 'EPAY PARM','|MAX TXN AMT',      0,null,'20000'
exec gfisp_additem 'EPAY PARM','|MIN TXN AMT',      0,null,'1000'
exec gfisp_additem 'EPAY PARM','|MAX ORG TXN AMT',  0,null,'40000'
exec gfisp_additem 'EPAY PARM','|MIN ORG TXN AMT',  0,null,'1000'
exec gfisp_additem 'EPAY PARM','|L2G PAYMENT METHOD',0,null,'100'
exec gfisp_additem 'EPAY PARM','|L2G RETURN URL'
exec gfisp_additem 'EPAY PARM','|L2G POST URL'
exec gfisp_additem 'EPAY PARM','|L2G CANCEL URL'

exec gfisp_additem 'EPAY PARM','|L2G URL MODE',            0,null,'TEST'
exec gfisp_additem 'EPAY PARM','|L2G MERCHANT CODE'
exec gfisp_additem 'EPAY PARM','|L2G SETTLE MERCHANT CODE'
exec gfisp_additem 'EPAY PARM','|L2G PWD'

-- set e-xact / paymentech values
exec gfisp_additem 'EPAY PARM','|E-xact URL Mode',             0,null,'TEST'
exec gfisp_additem 'EPAY PARM','|E-xact Payment Page ID TEST', 0,null,'HCO-GENFA-799'
exec gfisp_additem 'EPAY PARM','|E-xact Transaction Key TEST', 0,null,'5aGXi83bbqkIHYaghv4t'
exec gfisp_additem 'EPAY PARM','|E-xact Response Key TEST',    0,null,'jKV0JTx_c_IVilkqT_wJ'
exec gfisp_additem 'EPAY PARM','|E-xact WEB URL TEST',         0,null,'https://rpm.demo.e-xact.com/payment'
exec gfisp_additem 'EPAY PARM','|E-xact WEB Return URL TEST',  0,null,'http://10.1.4.25:8080/e-Fare/shoppingCart/success.html'
exec gfisp_additem 'EPAY PARM','|E-xact API URL TEST',         0,null,'https://api-demo.e-xact.com/'
exec gfisp_additem 'EPAY PARM','|E-xact API USER TEST',        0,null,'AD5118-01'
exec gfisp_additem 'EPAY PARM','|E-xact API PWD TEST',         0,null,'terminal1'

exec gfisp_additem 'EPAY PARM','|Credit Service',   0,null,'|FIS'

exec gfisp_additem 'EPAY PARM','|PROXY SERVER'
exec gfisp_additem 'EPAY PARM','|PROXY PORT'
exec gfisp_additem 'EPAY PARM','|PROXY UID'
exec gfisp_additem 'EPAY PARM','|PROXY PWD'
exec gfisp_additem 'EPAY PARM','|PROXY USE',        0,null,'No'

exec gfisp_additem 'EPAY PARM','|SMTP SERVER',  0,null,'smtp.gmail.com'
exec gfisp_additem 'EPAY PARM','|SMTP PORT',    0,null,'465'
exec gfisp_additem 'EPAY PARM','|SMTP UID',     0,null,'gfiepaytest@gmail.com'
exec gfisp_additem 'EPAY PARM','|SMTP PWD',     0,null,'gfi314159'
exec gfisp_additem 'EPAY PARM','|SMTP PROTOCOL',0,null,'smtps'

exec gfisp_additem 'EPAY PARM','|MAX GUEST TXN AMT',0,null,'30000'
exec gfisp_additem 'EPAY PARM','|MIN GUEST TXN AMT',0,null,'2000'

exec gfisp_additem 'EPAY PARM','|AUTO REP FOR PAPER PRODUCT',  0,null,'No'
exec gfisp_additem 'EPAY PARM','|AUTO REP FOR PLASTIC PRODUCT',0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|SHIPPING OPTION',             0,null,'No'
exec gfisp_additem 'EPAY PARM','|CANCEL OPTION',               0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|BUFFER PERIOD',0,null,'2'
exec gfisp_additem 'EPAY PARM','|CONTACT_ADDR', 0,null,''
exec gfisp_additem 'EPAY PARM','|CONTACT_CITY', 0,null,''
exec gfisp_additem 'EPAY PARM','|CONTACT_STATE',0,null,''
exec gfisp_additem 'EPAY PARM','|CONTACT_ZIP',  0,null,''
exec gfisp_additem 'EPAY PARM','|CONTACT_PHONE',0,null,' '
exec gfisp_additem 'EPAY PARM','|CONTACT_OTHER',0,null,''

exec gfisp_additem 'EPAY PARM','|SUBSCRIPTION FLAG',0,null,'001100'
exec gfisp_additem 'EPAY PARM','|STORED VALUE MAX', 0,null,'20000'
exec gfisp_additem 'EPAY PARM','|ORG CREDIT LIMIT OPTION',0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|RFID LIST',        0,null,'130,155,156,157'
exec gfisp_additem 'EPAY PARM','|ASSIGNED USR MAX', 0,null,'20'
exec gfisp_additem 'EPAY PARM','|EMAIL RATE',       0,null,'60'
exec gfisp_additem 'EPAY PARM','|AUTOREP RATE',     0,null,'60'

exec gfisp_additem 'EPAY PARM','|DEFAULT CITY',0,null,''
exec gfisp_additem 'EPAY PARM','|PAGE SIZE',   0,null,'25'
exec gfisp_additem 'EPAY PARM','|HOME PAGE',   0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|FAQ PAGE',    0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|ABOUT PAGE',  0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|FOUND CARD',  0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|MAX REGISTER CARD CNT', 0,null,'5'
exec gfisp_additem 'EPAY PARM','|MAX ACTIVE SESSION CNT',0,null,'0'

exec gfisp_additem 'EPAY PARM','|REMOTE USER SERVICE ACTIVE',0,null,'No'
exec gfisp_additem 'EPAY PARM','|REMOTE USER SERVICE URL',0,null,'http://10.1.4.24:3200'
exec gfisp_additem 'EPAY PARM','|REMOTE USER API TOKEN',0,null,'abcde'
exec gfisp_additem 'EPAY PARM','|REMOTE USER SERVICE ONLY CA ADDRESS',0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|AUTOREP ACTIVATION',0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|EMAIL ACTIVATION',  0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|NOTIFICATION BLOCK SIZE',  0,null,'20'
exec gfisp_additem 'EPAY PARM','|NEW CARD NOTIFY USERS',  0,null,''

exec gfisp_additem 'EPAY PARM','|SITE URL'
exec gfisp_additem 'EPAY PARM','|ORG_ROLES',0,null,'OrgPurchaser,OrgValueAdder,OrgUnlinker,OrgAutoBuyer,OrgViewer,OrgReporter'

exec gfisp_additem 'EPAY PARM','|NEGATIVE RECOVERY',  0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|EMAIL REPLY TO',     0,null,'gfiepaytest@gmail.com'
exec gfisp_additem 'EPAY PARM','|GA ACCOUNT ID',  0,null,'1'

exec gfisp_additem 'EPAY PARM','|ORG RESTRICT LINKED PROD',0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|ACCESSBILITY ACTIVE',0,null,'Yes'
exec gfisp_additem 'EPAY PARM','|FAREBOX LANGUAGES ACTIVE',0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|SITE URL ORG'

exec gfisp_additem 'EPAY PARM','|REDUCED FARE REQS ACTIVE',0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|FARE CARD CVV REQUIRED',0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|FEEDBACK NOTIFY USERS',  0,null,''
exec gfisp_additem 'EPAY PARM','|FEEDBACK PAGE',  0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|ALLOW TICKET REGISTRATION',  0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|GETORD PROD',0,null,'No'

exec gfisp_additem 'EPAY PARM','|PURCHASE TICKETS',0,null,'Yes'

exec gfisp_additem 'EPAY PARM','|FORCE ONE PRODUCT NEW CARD',0,null,'No'
exec gfisp_additem 'EPAY PARM','|ALLOW ORG CARD PLUS PRODUCT',0,null,'No'

exec gfisp_additem 'EPAY PARM','|LOCK DOB',0,null,'No'
exec gfisp_additem 'EPAY PARM','|LOCK REDUCED',0,null,'No'

exec gfisp_additem 'EPAY PARM','|ALLOW ORG CREDIT CARD',0,null,'No'
exec gfisp_additem 'EPAY PARM','|RESTRICT PRODUCT TYPE FARE PROFILE',0,null,'No'

exec gfisp_additem 'EPAY PARM','|DISABLE UNREGISTER UNLESS PROFILE EXP',0,null,'No'

-- LG-830
if exists (select null from aut where aut_name like '%SORTA%') -- this is a fix/hook
begin
	exec gfisp_additem 'EPAY PARM','AUTOLOAD WITH REGISTRATION',0,null,'No'		-- for SORTA No
end
else
begin
	exec gfisp_additem 'EPAY PARM','AUTOLOAD WITH REGISTRATION',0,null,'Yes'	-- for Winnipeg Yes
end
go

------------------------------------------------------------------------
-- List: EPAY PARM HELP
------------------------------------------------------------------------
exec gfisp_additem 'EPAY PARM HELP','|AR AMT MIN',       0,null,'|N1-65535;Minimum auto-replenishment amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|AR AMT MAX',       0,null,'|N1-65535;Maximum auto-replenishment amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|LOCKOUT THRESHOLD',0,null,'|N0;User account lockout threshold (number of failed login attempts allowed using the same user ID; use 0 to disable lockout function)'
exec gfisp_additem 'EPAY PARM HELP','|LOCKOUT WAIT TIME',0,null,'|N0;Wait time in minutes before a temporarily locked out account is automatically reactivated; use 0 to disable automatic reactivation of a locked out account'
exec gfisp_additem 'EPAY PARM HELP','|PWD ALPHANUMERIC', 0,null,'|BY;Password policy: Yes - need both digit (0-9) and alphabet (A-Z); No - disable this check'
exec gfisp_additem 'EPAY PARM HELP','|PWD DICTIONARY',   0,null,'|BY;Password policy: Yes - cannot be in the dictionary; No - disable this check'
exec gfisp_additem 'EPAY PARM HELP','|PWD EXPIRATION',   0,null,'|N0-32767;Password policy: expiration in days; use 0 to disable this check'
exec gfisp_additem 'EPAY PARM HELP','|PWD HISTORY',      0,null,'|N0-100;Password policy: number of the past passwords to be stored that cannot be used as new passwords until they are pushed out; use 0 to disable this check'
exec gfisp_additem 'EPAY PARM HELP','|PWD MIN LENGTH',   0,null,'|N0-32;Password policy: minimum length; use 0 to disable this check'
exec gfisp_additem 'EPAY PARM HELP','|PWD MIX',          0,null,'|BY;Password policy: Yes - need at least one character from three of the following four categories: upper case letter (A-Z), lower case letter (a-z), digit (0-9), and special character (%&#$!@*^?); No - disable this check'
exec gfisp_additem 'EPAY PARM HELP','|AGENCY ID',        0,null,'|N0-4095;GFI assigned transit agency ID (part of S/N encoding)'
exec gfisp_additem 'EPAY PARM HELP','|MANUFACTURER ID',  0,null,'|N0-255;GFI assigned card manufacturer ID (part of S/N encoding)'
exec gfisp_additem 'EPAY PARM HELP','|SEC CODE SMART',   0,null,'|N0-15;GFI assigned smart card security code (part of S/N encoding; 5 - smart card)'
exec gfisp_additem 'EPAY PARM HELP','|SEC CODE MAG',     0,null,'|N0-15;GFI assigned magnetic card security code (part of S/N encoding; 1 - magnetic card)'
exec gfisp_additem 'EPAY PARM HELP','|AUTO REP',         0,null,'|BY;Yes - auto-replenishment function is on; No - auto-replenishment function is off'
exec gfisp_additem 'EPAY PARM HELP','|GUEST AUTO REP',   0,null,'|BY;Yes - guest auto-replenishment function is on; No - guest auto-replenishment function is off'
exec gfisp_additem 'EPAY PARM HELP','|GUEST RECHARGE',   0,null,'|BY;Yes - guest recharge function is on; No - guest recharge function is off'
exec gfisp_additem 'EPAY PARM HELP','|FEE PICC',         0,null,'|N0;Plastic or durable card unit fee in cent'
exec gfisp_additem 'EPAY PARM HELP','|FEE PICC LU',      0,null,'|N0;Limited usage card unit fee in cent'
exec gfisp_additem 'EPAY PARM HELP','|FEE PICC DESC',    0,null,'|;Plastic or durable card unit fee description for display to user'
exec gfisp_additem 'EPAY PARM HELP','|FEE PICC LU DESC', 0,null,'|;Limited usage card unit fee description for display to user'
exec gfisp_additem 'EPAY PARM HELP','|CARD EXP DATE',    0,null,'|;Absolute card expiration date for DESFire (default is no expiration): yyyy-mm-dd - expiration date; nY/M/D - n year/month/day from card creation date'
exec gfisp_additem 'EPAY PARM HELP','|CARD EXP DATE 2',  0,null,'|;Absolute card expiration date for Ultralight (default is no expiration): yyyy-mm-dd - expiration date; nY/M/D - n year/month/day from card creation date'
exec gfisp_additem 'EPAY PARM HELP','|DEFAULT COUNTRY',  0,null,'|;Default country code'
exec gfisp_additem 'EPAY PARM HELP','|DEFAULT STATE',    0,null,'|;Default state/province code'
exec gfisp_additem 'EPAY PARM HELP','|RECHARGE MAX EXP', 0,null,'|N0;The maximum number of days that a product has expired to still allow a recharge on the product (0 - do not allow recharge on an expired product; null/empty - always allow recharge regardless of product expiration)'
exec gfisp_additem 'EPAY PARM HELP','|PROD LIMIT 1',     0,null,'|N0;Maximum number of period product on card'
exec gfisp_additem 'EPAY PARM HELP','|PROD LIMIT 2',     0,null,'|N0;Maximum number of ride product on card'
exec gfisp_additem 'EPAY PARM HELP','|PROD LIMIT 3',     0,null,'|N0;Maximum number of value product on card'
exec gfisp_additem 'EPAY PARM HELP','|PROD LIMIT 4',     0,null,'|N0;Maximum number of purses on card'
exec gfisp_additem 'EPAY PARM HELP','|PENDING LIMIT 1',  0,null,'|N0;Number of pending period product recharge allowed'
exec gfisp_additem 'EPAY PARM HELP','|PENDING LIMIT 2',  0,null,'|N0;Number of pending ride product recharge allowed'
exec gfisp_additem 'EPAY PARM HELP','|PENDING LIMIT 3',  0,null,'|N0;Number of pending value product recharge allowed'
exec gfisp_additem 'EPAY PARM HELP','|PENDING LIMIT 4',  0,null,'|N0;Number of pending purse recharge allowed'
exec gfisp_additem 'EPAY PARM HELP','|RECHARGE ALLOW 1', 0,null,'|BY;Yes - allow recharge on period product; No - disallow'
exec gfisp_additem 'EPAY PARM HELP','|RECHARGE ALLOW 2', 0,null,'|BY;Yes - allow recharge on ride product; No - disallow'
exec gfisp_additem 'EPAY PARM HELP','|RECHARGE ALLOW 3', 0,null,'|BY;Yes - allow recharge on value product; No - disallow'
exec gfisp_additem 'EPAY PARM HELP','|RECHARGE ALLOW 4', 0,null,'|BY;Yes - allow recharge on purse; No - disallow'

exec gfisp_additem 'EPAY PARM HELP','|ROOT PATH',        0,null,'|;Absolute path for external files directory tree'

exec gfisp_additem 'EPAY PARM HELP','|AUTO REP USER GROUPING',0,null,'|BY;Combining multiple auto-replenishment requests into a single credit card transaction'
exec gfisp_additem 'EPAY PARM HELP','|CAPTCHA IMAGE',    0,null,'|BY;Capture image'
exec gfisp_additem 'EPAY PARM HELP','|NUM OF PRODUCTS ON CARD',0,null,'|N0;Maximum number of products allowed on a smart card'
exec gfisp_additem 'EPAY PARM HELP','|PURCHASE WITHOUT LOGIN', 0,null,'|BY;Yes - allow guest purchase; No - disallow'
exec gfisp_additem 'EPAY PARM HELP','|DATE OF BIRTH',    0,null,'|BY;Yes - Collect date of birth information; No - do not collect date of birth information'
exec gfisp_additem 'EPAY PARM HELP','|SESSION TIMEOUT',  0,null,'|N0;Login session time out in minutes'
exec gfisp_additem 'EPAY PARM HELP','|MAX TXN AMT',      0,null,'|N0;Maximum transaction amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|MIN TXN AMT',      0,null,'|N0;Minimum transaction amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|MAX ORG TXN AMT',  0,null,'|N0;Maximum organization transaction amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|MIN ORG TXN AMT',  0,null,'|N0;Minimum organization transaction amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|L2G PAYMENT METHOD',0,null,'|N0;FIS payment method'
exec gfisp_additem 'EPAY PARM HELP','|L2G RETURN URL',   0,null,'|;Return to e-GO URL for payment interface'
exec gfisp_additem 'EPAY PARM HELP','|L2G POST URL',     0,null,'|;FIS payment interface URL'
exec gfisp_additem 'EPAY PARM HELP','|L2G CANCEL URL',   0,null,'|;Cancel and return to e-GO URL for payment interface'

exec gfisp_additem 'EPAY PARM HELP','|L2G URL MODE',     0,null,'|;FIS PayDirect (Link2Gov) connection type: TEST - test server; PROD - production server'
exec gfisp_additem 'EPAY PARM HELP','|L2G MERCHANT CODE',0,null,'|;FIS PayDirect (Link2Gov) merchant code'
exec gfisp_additem 'EPAY PARM HELP','|L2G SETTLE MERCHANT CODE',0,null,'|;FIS PayDirect (Link2Gov) settlement merchant code'
exec gfisp_additem 'EPAY PARM HELP','|L2G PWD',          0,null,'|;FIS PayDirect (Link2Gov) password'

exec gfisp_additem 'EPAY PARM HELP','|E-xact URL Mode',             0,null,'|;URL mode'
exec gfisp_additem 'EPAY PARM HELP','|E-xact Payment Page ID TEST', 0,null,'|;Test payment page ID'
exec gfisp_additem 'EPAY PARM HELP','|E-xact Transaction Key TEST', 0,null,'|;Test transaction key'
exec gfisp_additem 'EPAY PARM HELP','|E-xact Response Key TEST',    0,null,'|;Test response key'
exec gfisp_additem 'EPAY PARM HELP','|E-xact WEB URL TEST',         0,null,'|;Test web URL'
exec gfisp_additem 'EPAY PARM HELP','|E-xact WEB Return URL TEST',  0,null,'|;Test web return URL'
exec gfisp_additem 'EPAY PARM HELP','|E-xact API URL TEST',         0,null,'|;Test API URL'
exec gfisp_additem 'EPAY PARM HELP','|E-xact API USER TEST',        0,null,'|;Test API user ID'
exec gfisp_additem 'EPAY PARM HELP','|E-xact API PWD TEST',         0,null,'|;Test API password'

exec gfisp_additem 'EPAY PARM HELP','|Credit Service',   0,null,'|;Payment gateway'

exec gfisp_additem 'EPAY PARM HELP','|PROXY SERVER',     0,null,'|;Proxy server address'
exec gfisp_additem 'EPAY PARM HELP','|PROXY PORT',       0,null,'|;Proxy server port'
exec gfisp_additem 'EPAY PARM HELP','|PROXY UID',        0,null,'|;Proxy server user ID'
exec gfisp_additem 'EPAY PARM HELP','|PROXY PWD',        0,null,'|;Proxy server password'
exec gfisp_additem 'EPAY PARM HELP','|PROXY USE',        0,null,'|BY;Proxy server usage flag: Yes - use proxy server; No - do not use proxy server'

exec gfisp_additem 'EPAY PARM HELP','|SMTP SERVER',      0,null,'|;SMTP server address'
exec gfisp_additem 'EPAY PARM HELP','|SMTP PORT',        0,null,'|;SMTP server port'
exec gfisp_additem 'EPAY PARM HELP','|SMTP UID',         0,null,'|;SMTP server user ID'
exec gfisp_additem 'EPAY PARM HELP','|SMTP PWD',         0,null,'|;SMTP server password'
exec gfisp_additem 'EPAY PARM HELP','|SMTP PROTOCOL',    0,null,'|;SMTP protocol (smtp, smtps)'

exec gfisp_additem 'EPAY PARM HELP','|MAX GUEST TXN AMT',0,null,'|N0;Maximum transaction amount for guest user(�)'
exec gfisp_additem 'EPAY PARM HELP','|MIN GUEST TXN AMT',0,null,'|N0;Minimum transaction amount for guest user(�)'

exec gfisp_additem 'EPAY PARM HELP','|AUTO REP FOR PAPER PRODUCT',  0,null,'|BY;Yes - auto-replenishment function is on for paper product; No - auto-replenishment function is off for paper product'
exec gfisp_additem 'EPAY PARM HELP','|AUTO REP FOR PLASTIC PRODUCT',0,null,'|BY;Yes - auto-replenishment function is on for plastic product; No - auto-replenishment function is off for plastic product'
exec gfisp_additem 'EPAY PARM HELP','|SHIPPING OPTION',             0,null,'|BY;Yes - Shipping Option for pending order screen is on; No - Shipping Option for pending order screen is off'
exec gfisp_additem 'EPAY PARM HELP','|CANCEL OPTION',               0,null,'|BY;Yes - Cancel Option for add product or recharge order is on; No - Cancel Option for add product or recharge order is off'

exec gfisp_additem 'EPAY PARM HELP','|BUFFER PERIOD',0,null,'|;Buffer period for autoload'
exec gfisp_additem 'EPAY PARM HELP','|CONTACT_ADDR', 0,null,'|;Contact address of agency'
exec gfisp_additem 'EPAY PARM HELP','|CONTACT_CITY', 0,null,'|;Contact city of agency'
exec gfisp_additem 'EPAY PARM HELP','|CONTACT_STATE',0,null,'|;Contact state of agency'
exec gfisp_additem 'EPAY PARM HELP','|CONTACT_ZIP',  0,null,'|;Contact zip of agency'
exec gfisp_additem 'EPAY PARM HELP','|CONTACT_PHONE',0,null,'|;Contact phone of agency'
exec gfisp_additem 'EPAY PARM HELP','|CONTACT_OTHER',0,null,'|;Other contact details of agency'

exec gfisp_additem 'EPAY PARM HELP','|SUBSCRIPTION FLAG',0,null,'|N0;Flag type value to configure the subscriptions'
exec gfisp_additem 'EPAY PARM HELP','|STORED VALUE MAX', 0,null,'|N0;Maximum Stored Value amount (�)'
exec gfisp_additem 'EPAY PARM HELP','|ORG CREDIT LIMIT OPTION',0,null,'|BY;Yes - Org Credit Limit Option for manage accounts screen is on; No - Org Credit Limit Option for manage accounts screen is off'
exec gfisp_additem 'EPAY PARM HELP','|RFID LIST',        0,null,'|;List of USER TYPEs for reduced fare ID (comma delimited)'
exec gfisp_additem 'EPAY PARM HELP','|ASSIGNED USR MAX', 0,null,'|N0;Maximum number of users that can be assigned to an org'
exec gfisp_additem 'EPAY PARM HELP','|EMAIL RATE',       0,null,'|N0;Seconds between runs of email process'
exec gfisp_additem 'EPAY PARM HELP','|AUTOREP RATE',     0,null,'|N0;Seconds between runs of autoreplenishment process'

exec gfisp_additem 'EPAY PARM HELP','|DEFAULT CITY',     0,null,'|;Default city name'
exec gfisp_additem 'EPAY PARM HELP','|PAGE SIZE',        0,null,'|N0;Page size for the paginated screens'

exec gfisp_additem 'EPAY PARM HELP','|HOME PAGE',        0,null,'|BY;Yes Show the Home Page menu item'
exec gfisp_additem 'EPAY PARM HELP','|FAQ PAGE',         0,null,'|BY;Yes Show the FAQ Page menu item'
exec gfisp_additem 'EPAY PARM HELP','|ABOUT PAGE',       0,null,'|BY;Yes Show the About Page menu item'
exec gfisp_additem 'EPAY PARM HELP','|FOUND CARD',       0,null,'|BY;Yes Show the option to mark a lost card found'
exec gfisp_additem 'EPAY PARM HELP','|MAX REGISTER CARD CNT', 0,null,'|N0;Max number of registered cards allowed per user account; use 0 to disable this check'
exec gfisp_additem 'EPAY PARM HELP','|MAX ACTIVE SESSION CNT',0,null,'|N0;Max number of active session count; use 0 to disable this check'

exec gfisp_additem 'EPAY PARM HELP','|REMOTE USER SERVICE ACTIVE',0,null,'|BY;Yes sends data to a remote service for updating'
exec gfisp_additem 'EPAY PARM HELP','|REMOTE USER SERVICE URL',0,null,'|URL used when updating users on a remote service'
exec gfisp_additem 'EPAY PARM HELP','|REMOTE USER API TOKEN',0,null,'|Security token used for authentication'
exec gfisp_additem 'EPAY PARM HELP','|REMOTE USER SERVICE ONLY CA ADDRESS',0,null,'|BY;Yes - Only pass CA addresses to Productive API (validate on Zip Code); No - Does not validate before sending to Productive API '

exec gfisp_additem 'EPAY PARM HELP','|AUTOREP ACTIVATION',0,null,'|BY;Yes Activate the Autocharge module'
exec gfisp_additem 'EPAY PARM HELP','|EMAIL ACTIVATION',  0,null,'|BY;Yes Activate the Notification module'
exec gfisp_additem 'EPAY PARM HELP','|NOTIFICATION BLOCK SIZE',  0,null,'|N0;Set the block size for Notification module'
exec gfisp_additem 'EPAY PARM HELP','|NEW CARD NOTIFY USERS',  0,null,'|;Comma delimited list of usernames to notify when a new card has been purchased'

exec gfisp_additem 'EPAY PARM HELP','|SITE URL',         0,null,'|;URL used to reach the site from the Internet'
exec gfisp_additem 'EPAY PARM HELP','|ORG_ROLES',        0,null,'|;Defined permissions for an organization user'

exec gfisp_additem 'EPAY PARM HELP','|NEGATIVE RECOVERY',0,null,'|BY;Yes enables negative recovery order placement'
exec gfisp_additem 'EPAY PARM HELP','|EMAIL REPLY TO',        0,null,'|;The Email Reply-To field for outgoing emails.'
exec gfisp_additem 'EPAY PARM HELP','|GA ACCOUNT ID',        0,null,'|;The Google Analytics Account ID.'

exec gfisp_additem 'EPAY PARM HELP','|ORG RESTRICT LINKED PROD',0,null,'|BY;Yes Restrict Individuals from managing Org linked products on their card'

exec gfisp_additem 'EPAY PARM HELP','|SITE URL ORG',     0,null,'|;URL used to reach the org site from the Internet'

exec gfisp_additem 'EPAY PARM HELP','|REDUCED FARE REQS ACTIVE',0,null,'|BY;Yes - prompt for reduced fare data; No - do not prompt for reduced fare data'
exec gfisp_additem 'EPAY PARM HELP','|FARE CARD CVV REQUIRED',0,null,'|BY;Yes - prompt for farecard cvv when registing new card; No - do not prompt for farecard cvv when registing new card'
exec gfisp_additem 'EPAY PARM HELP','|FAREBOX LANGUAGES ACTIVE',0,null,'|BY;Yes - prompt for farebox language choice; No - do not prompt for farebox language choice'
exec gfisp_additem 'EPAY PARM HELP','|ACCESSBILITY ACTIVE',0,null,'|BY;Yes - prompt for accessibility details; No - do not prompt for accessibility details'

exec gfisp_additem 'EPAY PARM HELP','|FEEDBACK NOTIFY USERS',  0,null,'|;Comma delimited list of usernames to email when customer feedback form is submitted'

exec gfisp_additem 'EPAY PARM HELP','|FEEDBACK PAGE',  0,null,'|BY;Yes Activate the Customer Feedback form'

exec gfisp_additem 'EPAY PARM HELP','|ALLOW TICKET REGISTRATION',0,null,'|BY;Yes - allow tickets to be registered via manage cards; No - do not allow tickets to be registered via manage cards'

exec gfisp_additem 'EPAY PARM HELP','|GETORD PROD',0,null,'|BY;Yes - gfisp_epay_getord_newcard should return product info for DESFire card; No - do not return product info'

exec gfisp_additem 'EPAY PARM HELP','|PURCHASE TICKETS',0,null,'|BY;Yes - Purchase of LUCC Tickets enabled; No - No Ticket purchases available'
exec gfisp_additem 'EPAY PARM HELP','|FORCE ONE PRODUCT NEW CARD',0,null,'|BY;Yes - Limit users to buy only 1 product (qty1) for new card purchases; No - Users may purchase multiple products of any qty for new card purchases'
exec gfisp_additem 'EPAY PARM HELP','|ALLOW ORG CARD PLUS PRODUCT',0,null,'|BY;Yes - Allow orgs to purchase cards plus product; No - Do not allow orgs to purchase cards plus product'
exec gfisp_additem 'EPAY PARM HELP','|LOCK DOB',0,null,'|BY;Yes - Locks DOB after account creation. Only a proxied admin may change; No - DOB is unlocked and changeable at any time'
exec gfisp_additem 'EPAY PARM HELP','|LOCK REDUCED',0,null,'|BY;Yes - Locks Reduced User Profile Info after account creation. Only a proxied admin may change; No - Reduced Profile info is unlocked and changeable at any time'
exec gfisp_additem 'EPAY PARM HELP','|ALLOW ORG CREDIT CARD',0,null,'|BY;Yes - Allows Orgs to buy with credit card; No - Does not allow orgs to buy with credit card'
exec gfisp_additem 'EPAY PARM HELP','|RESTRICT PRODUCT TYPE FARE PROFILE',0,null,'|BY;Yes - Restricts products that can be purchased so they are only allowed for profiles as listed in the user type fare table; No - Allows fares for user profile types 0 or 1 for all users'
exec gfisp_additem 'EPAY PARM HELP','|DISABLE UNREGISTER UNLESS PROFILE EXP',0,null,'|BY;Yes - Disables unregister a card link unless user profile exp set; No - Does not disable unless user profile exp set'
exec gfisp_additem 'EPAY PARM HELP','|AUTOLOAD WITH REGISTRATION',0,null,'|Yes - Create product autoload at registration; No - Do not create product autoload at registration'		-- LG-830
go

------------------------------------------------------------------------
-- Remove obsolete ePay parameters
------------------------------------------------------------------------
exec gfisp_delitem 'EPAY PARM','AUTO REP PRICE 1'
exec gfisp_delitem 'EPAY PARM','AUTO REP PRICE 2'
exec gfisp_delitem 'EPAY PARM','AUTO REP PRICE 3'
exec gfisp_delitem 'EPAY PARM','AUTO REP PRICE 4'
exec gfisp_delitem 'EPAY PARM','AUTO REP VALUE 1'
exec gfisp_delitem 'EPAY PARM','AUTO REP VALUE 2'
exec gfisp_delitem 'EPAY PARM','AUTO REP VALUE 3'
exec gfisp_delitem 'EPAY PARM','AUTO REP VALUE 4'
exec gfisp_delitem 'EPAY PARM','AUTO REP THRESHOLD 1'
exec gfisp_delitem 'EPAY PARM','AUTO REP THRESHOLD 2'
exec gfisp_delitem 'EPAY PARM','AUTO REP THRESHOLD 3'
exec gfisp_delitem 'EPAY PARM','AUTO REP THRESHOLD 4'

exec gfisp_delitem 'EPAY PARM HELP','AUTO REP PRICE 1'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP PRICE 2'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP PRICE 3'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP PRICE 4'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP VALUE 1'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP VALUE 2'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP VALUE 3'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP VALUE 4'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP THRESHOLD 1'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP THRESHOLD 2'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP THRESHOLD 3'
exec gfisp_delitem 'EPAY PARM HELP','AUTO REP THRESHOLD 4'
exec gfisp_delitem 'EPAY PARM HELP','SN NEXT SEQ1'
exec gfisp_delitem 'EPAY PARM HELP','SN NEXT SEQ2'
exec gfisp_delitem 'EPAY PARM HELP','SN NEXT SEQ3'
go

------------------------------------------------------------------------
-- List: EPAY RIGHT
------------------------------------------------------------------------
exec gfisp_additem 'EPAY RIGHT','|LOGON',           0,null,'Logon'
exec gfisp_additem 'EPAY RIGHT','|DENY LOGON',      0,null,'Deny logon'
exec gfisp_additem 'EPAY RIGHT','|NEW',             0,null,'Buy a new card'
exec gfisp_additem 'EPAY RIGHT','|RECHARGE',        0,null,'Recharge a card'
exec gfisp_additem 'EPAY RIGHT','|AUTO REPLENISH',  0,null,'|Auto-replenishment'
exec gfisp_additem 'EPAY RIGHT','|ORG REGISTRATION',0,null,'Register organization'
exec gfisp_additem 'EPAY RIGHT','|ORG ACTIVITY RPT',0,null,'Run organization activity report'
exec gfisp_additem 'EPAY RIGHT','|PENDING ORDERS',  0,null,'Fulfillment of orders'
exec gfisp_additem 'EPAY RIGHT','|MODIFY FARES',    0,null,'Modify fare structure'
exec gfisp_additem 'EPAY RIGHT','|RESET PWD',       0,null,'Reset password'
exec gfisp_additem 'EPAY RIGHT','|LOG RPT',         0,null,'Log Report'
exec gfisp_additem 'EPAY RIGHT','|ADMIN TRANX RPT', 0,null,'Admin transaction report'
exec gfisp_additem 'EPAY RIGHT','|ADM ORG ACT RPT', 0,null,'Organization admin activity report'
exec gfisp_additem 'EPAY RIGHT','|MANAGE USR ACC',  0,null,'Manage user access'
exec gfisp_additem 'EPAY RIGHT','|CUST SERVICE',    0,null,'Customer service'
exec gfisp_additem 'EPAY RIGHT','|GEN CONFIG',      0,null,'General Configuration'
exec gfisp_additem 'EPAY RIGHT','|ORG DELEGATE',    0,null,'Delegate Organization Administration'
exec gfisp_additem 'EPAY RIGHT','|ORG ALLOCATE',    0,null,'Allocate Organization Products'
exec gfisp_additem 'EPAY RIGHT','|ORG MANAGE PROD', 0,null,'Manage Organization Products'
exec gfisp_additem 'EPAY RIGHT','|ORG FARES',       0,null,'Administer Organization Fares'
exec gfisp_additem 'EPAY RIGHT','|REGISTER CARD',   0,null,'Register card'
exec gfisp_additem 'EPAY RIGHT','|REPORT LOST CARD',0,null,'Report a card as lost'
exec gfisp_additem 'EPAY RIGHT','|MANAGE CARD',     0,null,'Manage existing card'
exec gfisp_additem 'EPAY RIGHT','|TRANX RPT',       0,null,'Transaction report'
exec gfisp_additem 'EPAY RIGHT','|ACTIVITY RPT',    0,null,'Activity report'
exec gfisp_additem 'EPAY RIGHT','|PROCESS MGMT',    0,null,'Process Management'
exec gfisp_additem 'EPAY RIGHT','|ORDER STATUS',    0,null,'Order Status'
exec gfisp_additem 'EPAY RIGHT','|ADM ACC GRP',     0,null,'Admin Manage Access Groups'
exec gfisp_additem 'EPAY RIGHT','|ADM ACC GRP ASSGN',0,null,'Admin Assign Users to Groups'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ADMIN_USER',     0,null,'Admin role for rest app'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_ADMIN_USER', 0,null,'Org Admin role for rest app'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_CLERK_USER', 0,null,'Org Clerk, aka sub-user, role for rest app'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ANONYMOUS_USER', 0,null,'Anonymous role for rest app'
exec gfisp_additem 'EPAY RIGHT','|ROLE_INDIVIDUAL_USER',0,null,'The default role for rest app'
exec gfisp_additem 'EPAY RIGHT','|ADM PROXY USER',  0,null,'Admin can proxy as an individual user'
exec gfisp_additem 'EPAY RIGHT','|ADM UPDATE CARD', 0,null,'Admin can add products to cards'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_PURCHASER',  0,null,'Purchase New Cards & Tickets'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_REPORTER',   0,null,'View Reports'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_AUTO_BUYER', 0,null,'Manage Auto Buy'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_UNLINKER',   0,null,'Unlink a Card'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_VALUE_ADDER',0,null,'Add Value/Pass to Existing Cards'
exec gfisp_additem 'EPAY RIGHT','|ROLE_ORG_VIEWER',     0,null,'View Card Balance'
exec gfisp_additem 'EPAY RIGHT','|CONFIGURATOR',0,null,'Access Admin Configurator'
go

------------------------------------------------------------------------
-- List: EQ STATUS
------------------------------------------------------------------------
exec gfisp_additem 'EQ STATUS','Normal',          0,0,'The equipment is functioning normally; events that are not associated with other status types automatically fall into this category'
exec gfisp_additem 'EQ STATUS','Priority 3 alarm',1,1,'Maintenance access in progress or attention needed'
exec gfisp_additem 'EQ STATUS','Priority 2 alarm',2,2,'The equipment is malfunctioning, out of service, or off-line'
exec gfisp_additem 'EQ STATUS','Priority 1 alarm',3,3,'Security alert such as intrusion, impact, and silent alarm'
exec gfisp_additem 'EQ STATUS','Disconnected',    4,4,'No communication'
exec gfisp_additem 'EQ STATUS','Instrusion',      6,6,'Instrusion, Vibration, or Tilt'
go

------------------------------------------------------------------------
-- List: EQ TYPE
------------------------------------------------------------------------
exec gfisp_additem 'EQ TYPE','|TVM',0,1,'|Ticket Vending Machine (TVM)'
exec gfisp_additem 'EQ TYPE','|PEM',0,2,'|Printer/Encoder Machine (PEM)'
go

------------------------------------------------------------------------
-- List: EV CLASS
------------------------------------------------------------------------
exec gfisp_additem 'EV CLASS','Undefined',  0,0,'Undefined event/transaction type class/category'
exec gfisp_additem 'EV CLASS','Transaction',0,1,'Transactions'
exec gfisp_additem 'EV CLASS','Service',    0,2,'Service events'
exec gfisp_additem 'EV CLASS','Security',   0,3,'Security events'
exec gfisp_additem 'EV CLASS','Revenue',    0,4,'Revenue events'
exec gfisp_additem 'EV CLASS','Hot',        0,5,'Hot events'
exec gfisp_additem 'EV CLASS','Information',0,6,'Informational events'
go

------------------------------------------------------------------------
-- List: EV TYPE
------------------------------------------------------------------------
exec gfisp_addevt 0,  0,'Undefined','Undefined event/transaction type'
exec gfisp_addevt 6,  1,'Log event'
-- Transactions
exec gfisp_addevt 1,301,'Sign-in',           'PEM only'
exec gfisp_addevt 1,302,'Issue card'
exec gfisp_addevt 1,303,'Add value',         'PEM only'
exec gfisp_addevt 1,304,'Cancel card'
exec gfisp_addevt 1,305,'Replace card'
exec gfisp_addevt 1,306,'Sign-out',          'PEM only'
exec gfisp_addevt 1,307,'Issue badge',       'PEM only'
exec gfisp_addevt 1,308,'Delete badge',      'PEM only'
exec gfisp_addevt 1,309,'Event',             'PEM only'
exec gfisp_addevt 1,310,'Clear bad-list',    'PEM only'
exec gfisp_addevt 1,311,'Read card',         'PEM only'
exec gfisp_addevt 1,312,'Recharge'
exec gfisp_addevt 1,313,'Card cancelled',    'PEM only'
exec gfisp_addevt 1,314,'Batch issue card',  'PEM only'
exec gfisp_addevt 1,315,'Cancel transaction','PEM only (set the cancel bit on card and print CANCEL on card)'
exec gfisp_addevt 1,316,'Validate'
exec gfisp_addevt 1,317,'Deduct',            'e.g. Paying with a value card'
exec gfisp_addevt 1,318,'Proof of purchase'
if exists(select null from gfi_lst where type='EV TYPE' and code=319 and name='Refund')
begin
   delete from gfi_lst where type='EV TYPE' and code=319 and name='Refund'
end
exec gfisp_addevt 1,319,'Cancel/Refund',     'TVM only'
exec gfisp_addevt 1,320,'Master record',     'Multi-purchase transaction'
exec gfisp_addevt 1,321,'Overwrite card',    'Overwrite card with the current format and settings'
-- TRiM
exec gfisp_addevt 6,401,'TRiM disabled'
exec gfisp_addevt 6,402,'TRiM enabled'
exec gfisp_addevt 2,403,'TRiM offline'
exec gfisp_addevt 2,404,'TRiM online'
exec gfisp_addevt 2,405,'TRiM swapped',                      'Replace the current module with a different one'
exec gfisp_addevt 2,406,'TRiM card obstruction',             'Card jam in TRiM/cassette; n: 0 - condition on; 1 - condition off'
exec gfisp_addevt 2,407,'TRiM incorrectly encoded cards in cassette'
exec gfisp_addevt 4,408,'TRiM cassette empty'
exec gfisp_addevt 4,409,'TRiM cassette low'
exec gfisp_addevt 4,410,'TRiM cassette filled'
exec gfisp_addevt 2,411,'TRiM write timeout'
exec gfisp_addevt 2,412,'TRiM restart'
exec gfisp_addevt 6,413,'TRiM diagnostics'
exec gfisp_addevt 2,414,'TRiM stripe missing',               'Cards not loaded properly'
exec gfisp_addevt 2,415,'TRiM too many verify errors'
exec gfisp_addevt 6,416,'TRiM test'
exec gfisp_addevt 2,417,'TRiM issuance failure',             'Give an indication of a TRiM having a failure to issue on request, a final catch-all event beyond a specific condition (jam, message queue full, etc.)'
exec gfisp_addevt 2,418,'TRiM smart card reader offline',    'n: 0 - condition on; 1 - condition off'
exec gfisp_addevt 2,419,'TRiM type wrong',                   'n: 0 - condition on; 1 - condition off'
-- BTP
exec gfisp_addevt 2,420,'Bill tekpak disabled'
exec gfisp_addevt 2,421,'Bill tekpak enabled'
exec gfisp_addevt 2,422,'Bill tekpak offline'
exec gfisp_addevt 2,423,'Bill tekpak online'
exec gfisp_addevt 4,424,'Bill stacker removed'
exec gfisp_addevt 4,425,'Bill stacker inserted'
exec gfisp_addevt 2,426,'Bill tekpak obstruction',           'n: 0 - condition on; 1 - condition off'
exec gfisp_addevt 4,427,'Bill stacker near full'
exec gfisp_addevt 4,428,'Bill stacker full'
exec gfisp_addevt 2,429,'Bill tekpak too many invalid bills'
exec gfisp_addevt 2,430,'Bill tekpak swapped',               'Replace the current module with a different one'
exec gfisp_addevt 4,431,'Bill stacker swapped',              'Replace the current module with a different one (removed->inserted->swapped)'
exec gfisp_addevt 4,432,'Bill stacker leave out'
exec gfisp_addevt 2,433,'Bill stacker missing',              'Removed during power off'
exec gfisp_addevt 4,434,'Bill stacker cash limit reached'
exec gfisp_addevt 2,435,'Bill stacker non-transaction bills','Maintenance mode'
exec gfisp_addevt 4,436,'Bill stacker bill count',           'Total bills in bill stacker'
exec gfisp_addevt 2,437,'Bill tekpak alarm'
exec gfisp_addevt 2,438,'Bill tekpak sensor alarm'
exec gfisp_addevt 2,439,'Bill stacker invalid remove',       'Non-revenue mode remove'
exec gfisp_addevt 2,670,'Bill stacker invalid insert',       'Non-revenue mode insert'
exec gfisp_addevt 4,671,'Bill stacker locked out',           'Same module inserted (removed->inserted->locked out)'
exec gfisp_addevt 2,672,'Bill tekpak power loss'
exec gfisp_addevt 2,673,'Bill tekpak escrow return'
exec gfisp_addevt 2,674,'Bill tekpak communication error',   'Bill tekpak has entered a state where communication is limited/non-existent; it responds to only certain commands'
exec gfisp_addevt 2,675,'Bill tekpak cash recovery audit'
-- CTP
exec gfisp_addevt 2,440,'Coin tekpak disabled'
exec gfisp_addevt 2,441,'Coin tekpak enabled'
exec gfisp_addevt 2,442,'Coin tekpak offline'
exec gfisp_addevt 2,443,'Coin tekpak online'
exec gfisp_addevt 4,444,'Coin tekpak removed'
exec gfisp_addevt 4,445,'Coin tekpak inserted'
exec gfisp_addevt 2,446,'Coin tekpak tube obstruction'
exec gfisp_addevt 4,447,'Coin tekpak near full'
exec gfisp_addevt 2,448,'Coin tekpak validator obstruction'
exec gfisp_addevt 2,449,'Coin tekpak too many invalid coins'
exec gfisp_addevt 2,450,'Coin tekpak swapped',                        'Replace the current module with a different one (removed->inserted->swapped)'
exec gfisp_addevt 2,451,'Coin tekpak routing alarm',                  'Validated coins are routed wrong'
exec gfisp_addevt 2,452,'Coin tekpak checksum alarm'
exec gfisp_addevt 2,453,'Coin tekpak no credit alarm',                'Validated, but no credit'
exec gfisp_addevt 2,454,'Coin tekpak coin stuck'
exec gfisp_addevt 2,455,'Coin tekpak leave out'
exec gfisp_addevt 4,456,'Coin tekpak tube low'
exec gfisp_addevt 2,457,'Coin tekpak missing',                        'Removed during power off'
exec gfisp_addevt 4,458,'Coin tekpak cash limit reached'
exec gfisp_addevt 4,459,'Coin tekpak coin count',                     'Total coins in coin tekpak'
exec gfisp_addevt 2,460,'Coin tekpak non-transaction coins inserted', 'Maintenance mode'
exec gfisp_addevt 2,461,'Coin tekpak non-transaction coins dispensed','Maintenance mode'
exec gfisp_addevt 2,462,'Coin tekpak coins not dispensed'
exec gfisp_addevt 2,463,'Coin tekpak defective tube sensor alarm'
exec gfisp_addevt 2,464,'Coin tekpak invalid remove',                 'Non-revenue mode remove'
exec gfisp_addevt 2,465,'Coin tekpak invalid insert',                 'Non-revenue mode insert'
exec gfisp_addevt 4,466,'Coin tekpak locked out',                     'Same module inserted (removed->inserted->locked out)'
exec gfisp_addevt 2,467,'Coin tekpak throat obstruction',             'Coin stuck at mouth forcing solenoid to activate'
exec gfisp_addevt 2,468,'Coin tekpak dispensing error',               'n: 0 - condition on; 1 - condition off'
exec gfisp_addevt 2,469,'Coin tekpak cassette tubes missing'
exec gfisp_addevt 4,699,'Coin tekpak tube empty'
exec gfisp_addevt 2,710,'Coin tekpak escrow stuck open'
exec gfisp_addevt 2,711,'Coin tekpak accept gate open'
exec gfisp_addevt 2,712,'Coin tekpak accept gate error'
exec gfisp_addevt 2,713,'Coin tekpak separator error'
exec gfisp_addevt 2,714,'Coin tekpak discriminator error'
exec gfisp_addevt 2,715,'Coin tekpak disconnected',                   'n: 0 - condition on; 1 - condition off'
exec gfisp_addevt 2,716,'Coin tekpak count mismatch'
exec gfisp_addevt 2,717,'Coin tekpak cash recovery audit'
exec gfisp_addevt 2,718,'Coin tekpak cash replenish audit'
-- LG-285
exec gfisp_addevt 2,731,'Issue of tickets was successful'
exec gfisp_addevt 2,732,'Issue of tickets was failed'
-- Hopper
exec gfisp_addevt 2,470,'Hopper disabled'
exec gfisp_addevt 2,471,'Hopper enabled'
exec gfisp_addevt 2,472,'Hopper offline'
exec gfisp_addevt 2,473,'Hopper online'
exec gfisp_addevt 4,474,'Hopper removed'
exec gfisp_addevt 4,475,'Hopper inserted'
exec gfisp_addevt 4,476,'Hopper swapped',                         'Replace the current module with a different one (removed->inserted->swapped)'
exec gfisp_addevt 4,477,'Hopper low'
exec gfisp_addevt 4,478,'Hopper empty'
exec gfisp_addevt 2,479,'Hopper obstruction'
exec gfisp_addevt 2,480,'Hopper leave out'
exec gfisp_addevt 2,481,'Hopper missing',                        'Removed during power off'
exec gfisp_addevt 4,482,'Hopper near empty'
exec gfisp_addevt 4,483,'Hopper coin count',                     'Total coins in hopper'
exec gfisp_addevt 2,484,'Hopper non-transaction coins dispensed','Maintenance mode'
exec gfisp_addevt 2,485,'Hopper coin blocked in payout'
exec gfisp_addevt 4,486,'Hopper invalid remove',                 'Non-revenue mode remove'
exec gfisp_addevt 2,487,'Hopper invalid insert',                 'Non-revenue mode insert'
exec gfisp_addevt 2,488,'Hopper unauthorized coins dispensed'   -- jackpot
exec gfisp_addevt 4,489,'Hopper locked out',                     'Same module inserted (removed->inserted->locked out)'
exec gfisp_addevt 2,690,'Hopper disconnected'
exec gfisp_addevt 2,720,'Hopper dispensing error'
exec gfisp_addevt 2,721,'Hopper coin type mismatch',             'n: 0 - condition on; 1 - condition off'
-- Printer
exec gfisp_addevt 2,490,'Printer offline'
exec gfisp_addevt 2,491,'Printer online'
exec gfisp_addevt 2,492,'Printer paper out'
exec gfisp_addevt 2,493,'Printer paper low'
exec gfisp_addevt 2,494,'Printer paper replaced'
-- Cashbox
exec gfisp_addevt 2,500,'Cashbox disabled'
exec gfisp_addevt 2,501,'Cashbox enabled'
exec gfisp_addevt 2,502,'Cashbox offline'
exec gfisp_addevt 2,503,'Cashbox online'
exec gfisp_addevt 4,504,'Cashbox removed'
exec gfisp_addevt 4,505,'Cashbox inserted'
exec gfisp_addevt 4,506,'Cashbox near full'
exec gfisp_addevt 4,507,'Cashbox full'
exec gfisp_addevt 4,508,'Cashbox swapped',                       'Replace the current module with a different one (removed->inserted->swapped)'
exec gfisp_addevt 2,509,'Cashbox leave out'
exec gfisp_addevt 2,510,'Cashbox missing',                       'Removed during power off'
exec gfisp_addevt 4,511,'Cashbox cash limit reached'
exec gfisp_addevt 4,512,'Cashbox coin count',                    'Total coins in cashbox'
exec gfisp_addevt 2,513,'Cashbox non-transaction coins inserted','Maintenance mode'
exec gfisp_addevt 2,514,'Cashbox invalid remove',                'Non-revenue mode remove'
exec gfisp_addevt 2,515,'Cashbox invalid insert',                'Non-revenue mode insert'
exec gfisp_addevt 4,516,'Cashbox locked out',                    'Same module inserted (removed->inserted->locked out)'
exec gfisp_addevt 2,517,'Cashbox cash recovery audit'
exec gfisp_addevt 2,518,'Cashbox cash replenish audit'
-- Magnetic card reader
exec gfisp_addevt 2,520,'Card reader disabled'
exec gfisp_addevt 2,521,'Card reader enabled'
exec gfisp_addevt 2,524,'Card reader online'
exec gfisp_addevt 2,525,'Card reader offline'
-- Pinpad
exec gfisp_addevt 2,522,'Pinpad disabled'
exec gfisp_addevt 2,523,'Pinpad enabled'
exec gfisp_addevt 2,526,'Pinpad online'
exec gfisp_addevt 2,527,'Pinpad offline'
exec gfisp_addevt 2,528,'Wrong teminal ID','Smartcard processing'
-- Alarm module
exec gfisp_addevt 3,540,'Alarm module offline'
exec gfisp_addevt 3,541,'Alarm module online'
exec gfisp_addevt 3,542,'Silent alarm'
exec gfisp_addevt 3,543,'Vibration alarm on'
exec gfisp_addevt 3,544,'Vibration alarm off'
exec gfisp_addevt 3,545,'Tilt alarm on'
exec gfisp_addevt 3,546,'Tilt alarm off'
exec gfisp_addevt 3,547,'Alarm calibrated'
exec gfisp_addevt 3,548,'Alarm board door sensor status','n: 0 - sensor missing; 1 - bad sensor; 2 - closed; 3 - open; 4 - misaligned'
exec gfisp_addevt 3,549,'Alarm shunted'	-- GD-233
exec gfisp_addevt 3,585,'Door opened'
exec gfisp_addevt 3,586,'Door closed'
-- UPS
exec gfisp_addevt 2,560,'UPS offline'
exec gfisp_addevt 2,561,'UPS online'
exec gfisp_addevt 2,562,'UPS on battery power'
exec gfisp_addevt 2,563,'UPS battery low'
exec gfisp_addevt 2,564,'UPS on AC power'
exec gfisp_addevt 2,565,'UPS shutdown requested'
exec gfisp_addevt 6,566,'UPS blower on'
exec gfisp_addevt 6,567,'UPS blower off'
exec gfisp_addevt 6,568,'UPS heater on'
exec gfisp_addevt 6,569,'UPS heater off'
exec gfisp_addevt 2,570,'UPS high temperature warning','n=temperature in �C'
exec gfisp_addevt 2,571,'UPS low temperature warning', 'n=temperature in �C'
exec gfisp_addevt 2,572,'UPS temperature stabilized',  'n=temperature in �C'
exec gfisp_addevt 2,573,'UPS temperature sensor communication error'
-- Smart card reader
exec gfisp_addevt 6,630,'Smartcard reader disabled'
exec gfisp_addevt 6,631,'Smartcard reader enabled'
exec gfisp_addevt 2,632,'Smartcard reader offline'
exec gfisp_addevt 2,633,'Smartcard reader online'
-- System
exec gfisp_addevt 4,580,'Into revenue service'
exec gfisp_addevt 2,581,'Into maintenance service'
exec gfisp_addevt 2,582,'Back to service'
exec gfisp_addevt 2,583,'Local out of service (OOS) by maintenance'
exec gfisp_addevt 2,584,'Reset local out of service (OOS)'
exec gfisp_addevt 3,587,'Siren on'
exec gfisp_addevt 3,588,'Siren off'
exec gfisp_addevt 2,589,'Out of service (OOS)'
exec gfisp_addevt 3,590,'Configuration file missing'
exec gfisp_addevt 2,591,'Temperature alarm',       'n=temperature in �C'
exec gfisp_addevt 3,592,'Sign-in debounce'
exec gfisp_addevt 3,593,'Unauthorized entry',      'Invalid sign-in'
exec gfisp_addevt 3,594,'Authorized entry',        'Valid pin number'
exec gfisp_addevt 2,595,'Out of service (OOS) condition cleared'
exec gfisp_addevt 2,596,'Terminal ID changed',     'Smartcard processing'
exec gfisp_addevt 2,597,'Equipment number changed'
if exists(select null from gfi_lst where type='EV TYPE' and code=598 and name='Equipment shutdown requested')
begin
   delete from gfi_lst where type='EV TYPE' and code=598 and name='Equipment shutdown requested'
end
exec gfisp_addevt 3,598,'Equipment shutdown',      'n: 0 - no reason; 1 - local; 2 - remote; 3 - power loss; 4 - temperature'
exec gfisp_addevt 2,599,'Equipment too many restarts'
exec gfisp_addevt 2,600,'Equipment abrupt restart','Unclean shutdown'
exec gfisp_addevt 2,601,'Equipment recovery from abrupt restart'
exec gfisp_addevt 2,602,'Temperature',             'n=temperature in �C'
exec gfisp_addevt 3,603,'Factory authorized entry','Into factory mode'
exec gfisp_addevt 6,604,'Cancel event',            'Push Cancel button on equipment'
exec gfisp_addevt 3,605,'IP address changed'
exec gfisp_addevt 3,606,'Fare structure changed'
exec gfisp_addevt 3,607,'Configuration changed'
exec gfisp_addevt 3,608,'Sound file changed'

exec gfisp_addevt 2,610,'Button stuck'
exec gfisp_addevt 2,611,'Button freed'

exec gfisp_addevt 2,650,'BIOS updated'
exec gfisp_addevt 2,651,'BIOS update failed or too many BIOS updates'

exec gfisp_addevt 2,691,'TVM offline'
exec gfisp_addevt 2,692,'TVM online'
exec gfisp_addevt 2,693,'Door board offline'
exec gfisp_addevt 2,694,'Door board online'
exec gfisp_addevt 2,695,'Media board offline'
exec gfisp_addevt 2,696,'Media board online'
exec gfisp_addevt 2,697,'Power distribution board offline'
exec gfisp_addevt 2,698,'Power distribution board online'
exec gfisp_addevt 4,700,'No change given mode'
exec gfisp_addevt 4,701,'No bill accepted mode',        'n: 0 - no bill accepted; 1 - bill accepted'
exec gfisp_addevt 4,702,'No coin accepted mode',        'n: 0 - no coin accepted; 1 - coin accepted'
exec gfisp_addevt 2,703,'Coin shutter stuck',           'n: 0 - coin shutter stuck; 1 - condition cleared'
exec gfisp_addevt 2,704,'Door shutter status',          'n: 0 - sensor missing; 1 - closed; 2 - open; 3 - partially open'
exec gfisp_addevt 2,730,'Ticket detection problem at ticket tray','n>0 - corresponding transaction ID'
exec gfisp_addevt 2,735,'Ticket tray sensor malfunction','n: 0 - condition on; 1 - condition off'
if exists(select null from gfi_lst where type='EV TYPE' and code=740 and name='Door sensor status')
begin
   delete from gfi_lst where type='EV TYPE' and code=740 and name='Door sensor status'
end
exec gfisp_addevt 2,740,'Door board door sensor status','n: 0 - sensor missing; 1 - bad sensor; 2 - closed; 3 - open; 4 - misaligned'
-- Server generated events
exec gfisp_addevt 6,900,'Poll equipment'
exec gfisp_addevt 6,901,'Put equipment in service'
exec gfisp_addevt 6,902,'Put equipment out of service (OOS)'
exec gfisp_addevt 6,903,'Shutdown/reboot equipment'

exec gfisp_addevt 5,951,'TVM not communicating',   'Server has not heard from TVM for a predefined period of time'
exec gfisp_addevt 5,952,'TVM disk space',          'n: 0 - normal; 1 - near full; 2 - full'

exec gfisp_addevt 3,993,'Acknowledge security alert'
exec gfisp_addevt 6,994,'Fare structure download'
exec gfisp_addevt 6,995,'Configuration download'
exec gfisp_addevt 6,996,'Sound file download'
exec gfisp_addevt 6,997,'All configuration download'
go

------------------------------------------------------------------------
-- List: EXACT PAY TYPE
------------------------------------------------------------------------
exec gfisp_additem 'EXACT PAY TYPE','|VISA',              0, 1,'Visa'
exec gfisp_additem 'EXACT PAY TYPE','|MASTERCARD',        0, 2,'MasterCard'
exec gfisp_additem 'EXACT PAY TYPE','|AMERICAN EXPRESS',  0, 3,'American Express'
exec gfisp_additem 'EXACT PAY TYPE','|DINERS CLUB',       0, 4,'Diners Club'
exec gfisp_additem 'EXACT PAY TYPE','|DISCOVER',          0, 5,'Discover'
exec gfisp_additem 'EXACT PAY TYPE','|ENROUTE',           0, 6,'Enroute'
exec gfisp_additem 'EXACT PAY TYPE','|JCB',               0, 7,'JCB'
exec gfisp_additem 'EXACT PAY TYPE','|HAMILTON FINANCIAL',0, 8,'Hamilton Financial'
exec gfisp_additem 'EXACT PAY TYPE','|SEARS',             0, 9,'Sears'
exec gfisp_additem 'EXACT PAY TYPE','|MAESTRO',           0,10,'Maestro'
exec gfisp_additem 'EXACT PAY TYPE','|PAYPAL',            0,11,'PayPal'
exec gfisp_additem 'EXACT PAY TYPE','|Unknown',           0,12,'Unknown'
exec gfisp_additem 'EXACT PAY TYPE','|Interac Online Debit', 0,13,'Interac Online Debit'
go

-- changed by Marine for JTA-34

exec gfisp_delitem 'EV TYPE',NULL,1,321

exec gfisp_additem 'EV TYPE','Add product',1,321, 'Add new product on card'
go

------------------------------------------------------------------------
-- List: FARE TYPE - TFR - CRUFT This type and the field it defines in the fare table are CRUFT
------------------------------------------------------------------------
exec gfisp_additem 'FARE TYPE','Regular', 0,1,'Full fare'
exec gfisp_additem 'FARE TYPE','Senior',  0,2,'Discount fare group 1'
exec gfisp_additem 'FARE TYPE','Youth',   0,3,'Discount fare group 2'
exec gfisp_additem 'FARE TYPE','Disabled',0,4,'Discount fare group 3'
go

------------------------------------------------------------------------
-- List: FIS PAY TYPE
------------------------------------------------------------------------
exec gfisp_additem 'FIS PAY TYPE','|VI',0,1,'Visa'
exec gfisp_additem 'FIS PAY TYPE','|MC',0,2,'Master Card'
exec gfisp_additem 'FIS PAY TYPE','|AM',0,3,'American Express'
exec gfisp_additem 'FIS PAY TYPE','|DI',0,4,'Discover'
exec gfisp_additem 'FIS PAY TYPE','|DB',0,5,'Debit Card'
exec gfisp_additem 'FIS PAY TYPE','|PL',0,6,'Pinless Debit Card'
exec gfisp_additem 'FIS PAY TYPE','|EC',0,7,'eCheck'
go

------------------------------------------------------------------------
-- List: INSP CODE
------------------------------------------------------------------------
exec gfisp_additem 'INSP CODE','Undefined',     0,0,'Undefined inspection code'
exec gfisp_additem 'INSP CODE','No ticket',     0,1,'Did not have ticket or card in possession at time of inspection'
exec gfisp_additem 'INSP CODE','Expired ticket',0,2,'Had expired ticket or card'
exec gfisp_additem 'INSP CODE','Wrong ticket',  0,3,'Had wrong category of ticket or card'
exec gfisp_additem 'INSP CODE','Altered ticket',0,4,'Had altered ticket or card'
go

------------------------------------------------------------------------
-- List: INST TYPE
------------------------------------------------------------------------
exec gfisp_additem 'INST TYPE','Undefined',   0,0,'Undefined institution type'
exec gfisp_additem 'INST TYPE','Government',  0,1
exec gfisp_additem 'INST TYPE','Education',   0,2
exec gfisp_additem 'INST TYPE','Non-profit',  0,3
exec gfisp_additem 'INST TYPE','Corporation', 0,4,'Get-On-Board'

exec gfisp_delitem 'INST TYPE','Sales Outlet',0,5
exec gfisp_additem 'INST TYPE','Sales Outlet',1,5
go

------------------------------------------------------------------------
-- List: ISSU CODE
------------------------------------------------------------------------
exec gfisp_additem 'ISSU CODE','Undefined',0,0,'Undefined infraction inspection issuance code'
exec gfisp_additem 'ISSU CODE','|A',       0,1,'First time offender - written warning'
exec gfisp_additem 'ISSU CODE','|B',       0,2,'Second time offender - pay $10 fine'
exec gfisp_additem 'ISSU CODE','|C',       0,3,'Repeat offender - pay $50 fine'
go

------------------------------------------------------------------------
-- List: L2G PARM
------------------------------------------------------------------------
exec gfisp_additem 'L2G PARM','|L2G URL OFFLINE',      0,null,'http://localhost:8080/e-GO_Payment/start'
exec gfisp_additem 'L2G PARM','|L2G URL TEST',         0,null,'https://ca.link2gov.com'
exec gfisp_additem 'L2G PARM','|L2G URL PROD',         0,null,'https://gate.link2gov.com'
exec gfisp_additem 'L2G PARM','|L2G API CREDIT',       0,null,'/apivelocity/ExecuteCharge.aspx'
exec gfisp_additem 'L2G PARM','|L2G API CREDIT RECUR', 0,null,'/apivelocity/ExecuteRecurringCharge.aspx'
exec gfisp_additem 'L2G PARM','|L2G API CREDIT AUTH',  0,null,'/apivelocity/ExecuteAuth.aspx'
exec gfisp_additem 'L2G PARM','|L2G API CREDIT SETTLE',0,null,'/apivelocity/ExecuteSettle.aspx'
exec gfisp_additem 'L2G PARM','|L2G API DEBIT',        0,null,'/apivelocity/ExecuteDebit.aspx'
exec gfisp_additem 'L2G PARM','|L2G API DEBIT PINLESS',0,null,'/apivelocity/ExecuteDebitPINless.aspx'
exec gfisp_additem 'L2G PARM','|L2G API ECHECK',       0,null,'/apivelocity/ExecuteCheck.aspx'
exec gfisp_additem 'L2G PARM','|L2G API REFUND',       0,null,'/apivelocity/ExecuteRefund.aspx'
exec gfisp_additem 'L2G PARM','|L2G API STATUS',       0,null,'/apivelocity/GetTransactionStatus.aspx'
exec gfisp_additem 'L2G PARM','|L2G API STATUS2',      0,null,'/apivelocity/GetTransactionStatusExpanded.aspx'
exec gfisp_additem 'L2G PARM','|L2G API HEARTBEAT',    0,null,'/apivelocity/HeartBeat.aspx'
exec gfisp_additem 'L2G PARM','|L2G API CREDIT RECUR', 0,null,'/api/ExecuteRecurringCharge.aspx'
go

------------------------------------------------------------------------
-- List: L2G PARM HELP
------------------------------------------------------------------------
exec gfisp_additem 'L2G PARM HELP','|L2G URL OFFLINE',      0,null,'|;FIS PayDirect (Link2Gov) offline simulator for sales demos'
exec gfisp_additem 'L2G PARM HELP','|L2G URL TEST',         0,null,'|;FIS PayDirect (Link2Gov) test server'
exec gfisp_additem 'L2G PARM HELP','|L2G URL PROD',         0,null,'|;FIS PayDirect (Link2Gov) production server'
exec gfisp_additem 'L2G PARM HELP','|L2G API CREDIT',       0,null,'|;FIS PayDirect (Link2Gov) credit card charge API'
exec gfisp_additem 'L2G PARM HELP','|L2G API CREDIT RECUR', 0,null,'|;FIS PayDirect (Link2Gov) credit card recurring charge API'
exec gfisp_additem 'L2G PARM HELP','|L2G API CREDIT AUTH',  0,null,'|;FIS PayDirect (Link2Gov) credit card authorization API'
exec gfisp_additem 'L2G PARM HELP','|L2G API CREDIT SETTLE',0,null,'|;FIS PayDirect (Link2Gov) credit card settlement API'
exec gfisp_additem 'L2G PARM HELP','|L2G API DEBIT',        0,null,'|;FIS PayDirect (Link2Gov) debit card charge API'
exec gfisp_additem 'L2G PARM HELP','|L2G API DEBIT PINLESS',0,null,'|;FIS PayDirect (Link2Gov) debit card pinless charge API'
exec gfisp_additem 'L2G PARM HELP','|L2G API ECHECK',       0,null,'|;FIS PayDirect (Link2Gov) electronic check charge API'
exec gfisp_additem 'L2G PARM HELP','|L2G API REFUND',       0,null,'|;FIS PayDirect (Link2Gov) refund API'
exec gfisp_additem 'L2G PARM HELP','|L2G API STATUS',       0,null,'|;FIS PayDirect (Link2Gov) transaction status API'
exec gfisp_additem 'L2G PARM HELP','|L2G API STATUS2',      0,null,'|;FIS PayDirect (Link2Gov) expanded transaction status API'
exec gfisp_additem 'L2G PARM HELP','|L2G API HEARTBEAT',    0,null,'|;FIS PayDirect (Link2Gov) server heartbeat API'
exec gfisp_additem 'L2G PARM HELP','|L2G API CREDIT RECUR', 0,null,'|;FIS PayDirect (Link2Gov) recurring charge API'
go

------------------------------------------------------------------------
-- List: LANGUAGE
------------------------------------------------------------------------
exec gfisp_additem 'LANGUAGE', 'English',    0,  0, 'Active'
exec gfisp_additem 'LANGUAGE', 'Spanish',    0,  1, 'Active'
exec gfisp_additem 'LANGUAGE', 'French',     0,  2, 'Active'
exec gfisp_additem 'LANGUAGE', 'Cantonese',  0,  3, 'Active'
exec gfisp_additem 'LANGUAGE', 'Ukrainian',  0,  4, 'Active'
exec gfisp_additem 'LANGUAGE', 'German',     0,  5, 'Active'
exec gfisp_additem 'LANGUAGE', 'Tagalog',    0,  6, 'Active'
exec gfisp_additem 'LANGUAGE', 'Mandarin',   0,  7, 'Active'
exec gfisp_additem 'LANGUAGE', 'Polish',     0,  8, 'Active'
exec gfisp_additem 'LANGUAGE', 'Portuguese', 0,  9, 'Active'
exec gfisp_additem 'LANGUAGE', 'Italian',    0, 10, 'Active'
exec gfisp_additem 'LANGUAGE', 'Punjabi',    0, 11, 'Active'
exec gfisp_additem 'LANGUAGE', 'Vietnamese', 0, 12, 'Active'
exec gfisp_additem 'LANGUAGE', 'Ojibwe',     0, 13, 'Active'
exec gfisp_additem 'LANGUAGE', 'Hindi',      0, 14, 'Active'
exec gfisp_additem 'LANGUAGE', 'Russian',    0, 15, 'Active'
exec gfisp_additem 'LANGUAGE', 'Cree',       0, 16, 'Active'
exec gfisp_additem 'LANGUAGE', 'Dutch',      0, 17, 'Active'
go

------------------------------------------------------------------------
-- List: Gender
------------------------------------------------------------------------
exec gfisp_additem 'GENDER', 'Male',    	0, 0, 'Active'
exec gfisp_additem 'GENDER', 'Female',   	0, 1, 'Active'
exec gfisp_additem 'GENDER', 'Other',   	0, 2, 'Active'
go

------------------------------------------------------------------------
-- List: MNT CODE
------------------------------------------------------------------------
exec gfisp_additem 'MNT CODE','Undefined',0,0,'Undefined maintenance code'
exec gfisp_additem 'MNT CODE','Unjam',    0,1,'Unjam'
exec gfisp_additem 'MNT CODE','Adjust',   0,2,'Adjust'
exec gfisp_additem 'MNT CODE','Replace',  0,3,'Replace'
exec gfisp_additem 'MNT CODE','Clean',    0,4,'Clean'
exec gfisp_additem 'MNT CODE','Inspect',  0,5,'Inspect'
exec gfisp_additem 'MNT CODE','Other',    0,6,'Other'
go

------------------------------------------------------------------------
-- List: MOD STATUS
------------------------------------------------------------------------
exec gfisp_additem 'MOD STATUS','In service',        0,0,'In service'
exec gfisp_additem 'MOD STATUS','Counting room',     0,1,'Counting room'
exec gfisp_additem 'MOD STATUS','Inventory',         0,2,'Inventory'
exec gfisp_additem 'MOD STATUS','Repair',            0,3,'Repair'
exec gfisp_additem 'MOD STATUS','Out of circulation',0,4,'Out of circulation'
go

------------------------------------------------------------------------
-- List: MOD TYPE
------------------------------------------------------------------------
exec gfisp_additem 'MOD TYPE','Undefined',        0,0,'Undefined module type'
exec gfisp_additem 'MOD TYPE','Hopper',           0,1,'Hopper'
exec gfisp_additem 'MOD TYPE','TRiM',             0,2,'TRiM'
exec gfisp_additem 'MOD TYPE','Cashbox',          0,3,'Cashbox'
exec gfisp_additem 'MOD TYPE','Bill Stacker',     0,4,'Bill Stacker'
exec gfisp_additem 'MOD TYPE','Coin Tekpak',      0,5,'Coin Tekpak (CTP)'
exec gfisp_additem 'MOD TYPE','Bill Tekpak',      0,6,'Bill Tekpak (BTP)'

exec gfisp_additem 'MOD TYPE','BTP Board',        0,7,'Bill Tekpak Interface Board'
exec gfisp_additem 'MOD TYPE','CTP Board',        0,8,'Coin Tekpak Interface Board'
exec gfisp_additem 'MOD TYPE','TRiM Board',       0,9,'TRiM Interface Board'
exec gfisp_additem 'MOD TYPE','Main Controller',  0,10,'Main Controller'
exec gfisp_additem 'MOD TYPE','Alarm',            0,11,'Alarm'
exec gfisp_additem 'MOD TYPE','UPS',              0,12,'UPS'
exec gfisp_additem 'MOD TYPE','AC Controller',    0,13,'AC Controller'
exec gfisp_additem 'MOD TYPE','Door Board',       0,14,'Door Board'
exec gfisp_additem 'MOD TYPE','Media Board',      0,15,'Media Board'
exec gfisp_additem 'MOD TYPE','Power Dist. Board',0,16,'Power Distribution Board'
exec gfisp_additem 'MOD TYPE','Printer',          0,17,'Printer'
exec gfisp_additem 'MOD TYPE','Smartcard',        0,18,'Smartcard'
exec gfisp_additem 'MOD TYPE','Pinpad',           0,19,'Pinpad'
exec gfisp_additem 'MOD TYPE','Temperature Board',0,20,'Temperature/Light Sensor Board'
go

------------------------------------------------------------------------
-- List: NOTIFY TYPE
------------------------------------------------------------------------
exec gfisp_additem 'NOTIFY TYPE','Undefined',0,0,'Undefined notification type'
exec gfisp_additem 'NOTIFY TYPE','|Email',    0,1,'Email'
exec gfisp_additem 'NOTIFY TYPE','|Print',    0,2,'Print'
exec gfisp_additem 'NOTIFY TYPE','|Popup',    0,3,'Popup'
exec gfisp_additem 'NOTIFY TYPE','|Log',      0,4,'Log'
go

------------------------------------------------------------------------
-- List: PASS CLASS
------------------------------------------------------------------------
exec gfisp_additem 'PASS CLASS','Undefined',0,0,'Undefined passenger type'
exec gfisp_additem 'PASS CLASS','Adult',    0,1,'Adult'
exec gfisp_additem 'PASS CLASS','Senior',   0,2,'Senior'
exec gfisp_additem 'PASS CLASS','Youth',    0,3,'Youth'
exec gfisp_additem 'PASS CLASS','Disabled', 0,4,'Disabled'
go

------------------------------------------------------------------------
-- List: PASS TYPE
------------------------------------------------------------------------
exec gfisp_additem 'PASS TYPE','Undefined',0,0,'Undefined pass type'
exec gfisp_additem 'PASS TYPE','1R',       0,1,'Single Ride Pass'
exec gfisp_additem 'PASS TYPE','1D',       0,2,'1 Day Pass'
exec gfisp_additem 'PASS TYPE','3D',       0,3,'3 Day Pass'
exec gfisp_additem 'PASS TYPE','5D',       0,4,'5 Day Pass'
exec gfisp_additem 'PASS TYPE','7D',       0,5,'7 Day Pass'
exec gfisp_additem 'PASS TYPE','30D',      0,6,'30 Day Pass'
exec gfisp_additem 'PASS TYPE','10R',      0,7,'10 Ride Pass'
go

------------------------------------------------------------------------
-- List: PAY TYPE
------------------------------------------------------------------------
exec gfisp_additem 'PAY TYPE','Undefined',0,0,'Undefined payment type'
exec gfisp_additem 'PAY TYPE','Cash',     0,1,'Cash'
exec gfisp_additem 'PAY TYPE','Credit',   0,2,'Credit Card'
exec gfisp_additem 'PAY TYPE','Debit',    0,3,'Debit Card'
exec gfisp_additem 'PAY TYPE','Check',    0,4,'Check'
exec gfisp_additem 'PAY TYPE','Account',  0,5,'Account'
exec gfisp_additem 'PAY TYPE','GFI card', 0,6,'GFI issued card'
exec gfisp_additem 'PAY TYPE','Voucher',  0,7,'Voucher';		-- LG-270
go

declare @ls    varchar(256)
declare @li    smallint
declare @loc   smallint

------------------------------------------------------------------------
-- List: PDP PARM and PDP PARM HELP
------------------------------------------------------------------------
select @ls=dbo.list(loc_n) from cnf
while len(@ls)>0                                  -- loop through all locations
begin
   set @li=charindex(',',@ls)
   if @li>0
   begin
      set @loc=cast(left(@ls,@li - 1) as smallint)
      set @ls=substring(@ls,@li+1,256)
   end
   else
   begin
      set @loc=cast(@ls as smallint)
      set @ls=''
   end

   exec gfisp_additem 'PDP PARM','|Probe Limit',   @loc,null,'0'
   exec gfisp_additem 'PDP PARM','|Probe Exp Date',@loc,null,'12312100'
   exec gfisp_additem 'PDP PARM','|Probe Exp Time',@loc,null,'2359'
end
go

exec gfisp_additem 'PDP PARM HELP','|Probe Limit',   0,null,'|N0;Maximum number of probings allowed per session; use 0 to disable probing'
exec gfisp_additem 'PDP PARM HELP','|Probe Exp Date',0,null,'|N1012010-12312100;Probe session expiration date (mmddyyyy)'
exec gfisp_additem 'PDP PARM HELP','|Probe Exp Time',0,null,'|N0-2359;Probe session expiration time (hhmm)'

delete from gfi_lst where type='PDP PARM' and class not in (select loc_n from cnf)
go

------------------------------------------------------------------------
-- List: PROD TYPE
------------------------------------------------------------------------
exec gfisp_additem 'PROD TYPE','Transfer', 0, 0; -- LG-1295
exec gfisp_additem 'PROD TYPE','Period',  0,1
exec gfisp_additem 'PROD TYPE','Ride',    0,2
exec gfisp_additem 'PROD TYPE','Value',   0,3
exec gfisp_additem 'PROD TYPE','Purse',   0,4
exec gfisp_additem 'PROD TYPE','Employee',0,7
go

------------------------------------------------------------------------
-- List: PWD QUESTION
------------------------------------------------------------------------
exec gfisp_additem 'PWD QUESTION','What street did you live on in third grade?',                                   0, 1
exec gfisp_additem 'PWD QUESTION','What is your oldest sibling''s birthday month and year? (e.g., January 1900)',  0, 2
exec gfisp_additem 'PWD QUESTION','What is the middle name of your youngest child?',                               0, 3
exec gfisp_additem 'PWD QUESTION','What was your childhood nickname?',                                             0, 4
exec gfisp_additem 'PWD QUESTION','In what city did you meet your spouse/significant other?',                      0, 5
exec gfisp_additem 'PWD QUESTION','What is the name of your favorite childhood friend?',                           0, 6
exec gfisp_additem 'PWD QUESTION','What is your oldest sibling''s middle name?',                                   0, 7
exec gfisp_additem 'PWD QUESTION','What school did you attend for sixth grade?',                                   0, 8
exec gfisp_additem 'PWD QUESTION','What was your childhood phone number including area code? (e.g., 000-000-0000)',0, 9
--exec gfisp_additem 'PWD QUESTION','What is your oldest cousin''s first and last name?',                            0,10
exec gfisp_additem 'PWD QUESTION','What was the name of your first stuffed animal?',                               0,11
exec gfisp_additem 'PWD QUESTION','In what city or town did your mother and father meet?',                         0,12
exec gfisp_additem 'PWD QUESTION','Where were you when you had your first kiss?',                                  0,13
exec gfisp_additem 'PWD QUESTION','What is the first name of the boy or girl that you first kissed?',              0,14
exec gfisp_additem 'PWD QUESTION','What was the last name of your third grade teacher?',                           0,15
exec gfisp_additem 'PWD QUESTION','In what city does your nearest sibling live?',                                  0,16
exec gfisp_additem 'PWD QUESTION','What is your youngest brother''s birthday month and year? (e.g., January 1900)',0,17
exec gfisp_additem 'PWD QUESTION','What is your maternal grandmother''s maiden name?',                             0,18
exec gfisp_additem 'PWD QUESTION','In what city or town was your first job?',                                      0,19
exec gfisp_additem 'PWD QUESTION','What is the name of the place your wedding reception was held?',                0,20
exec gfisp_additem 'PWD QUESTION','What is the name of a college you applied to but didn''t attend?',              0,21
exec gfisp_additem 'PWD QUESTION','Where were you when you first heard about 9/11?',                               0,22
exec gfisp_additem 'PWD QUESTION','What is the name of the company of your first job?',                            0,23

exec gfisp_delitem 'PWD QUESTION','What is your oldest cousin''s first and last name?'
go

------------------------------------------------------------------------
-- List: REASON CODE
------------------------------------------------------------------------
exec gfisp_additem 'REASON CODE','Undefined',          0,0,'Undefined'
exec gfisp_additem 'REASON CODE','Vend Error',         0,1,'Value not vended properly'
exec gfisp_additem 'REASON CODE','Dispute Resolution', 0,2,'Placate customer'
go

------------------------------------------------------------------------
-- List: Reduced Fare Contact Type (RFC Type)
------------------------------------------------------------------------
exec gfisp_additem 'RFC TYPE','Work',		0,2,'ACTIVE'
exec gfisp_additem 'RFC TYPE','School',		0,3,'ACTIVE'
go

------------------------------------------------------------------------
-- List: SHIPPER
------------------------------------------------------------------------
exec gfisp_additem 'SHIPPER','N/A',  0,0,'Undefined shipper'
exec gfisp_additem 'SHIPPER','USPS', 0,1,'United States Postal Service'
exec gfisp_additem 'SHIPPER','UPS',  0,2,'United Parcel Service'
exec gfisp_additem 'SHIPPER','FedEx',0,3,'Federal Express'
exec gfisp_additem 'SHIPPER','DHL',  0,4,'DHL Express'
go

------------------------------------------------------------------------
-- List: STATE
------------------------------------------------------------------------
exec gfisp_additem 'STATE','AL',1,  1,'Alabama'
exec gfisp_additem 'STATE','AK',1,  2,'Alaska'
exec gfisp_additem 'STATE','AZ',1,  3,'Arizona'
exec gfisp_additem 'STATE','AR',1,  4,'Arkansas'
exec gfisp_additem 'STATE','CA',1,  5,'California'
exec gfisp_additem 'STATE','CO',1,  6,'Colorado'
exec gfisp_additem 'STATE','CT',1,  7,'Connecticut'
exec gfisp_additem 'STATE','DE',1,  8,'Delaware'
exec gfisp_additem 'STATE','FL',1,  9,'Florida'
exec gfisp_additem 'STATE','GA',1, 10,'Georgia'
exec gfisp_additem 'STATE','HI',1, 11,'Hawaii'
exec gfisp_additem 'STATE','ID',1, 12,'Idaho'
exec gfisp_additem 'STATE','IL',1, 13,'Illinois'
exec gfisp_additem 'STATE','IN',1, 14,'Indiana'
exec gfisp_additem 'STATE','IA',1, 15,'Iowa'
exec gfisp_additem 'STATE','KS',1, 16,'Kansas'
exec gfisp_additem 'STATE','KY',1, 17,'Kentucky'
exec gfisp_additem 'STATE','LA',1, 18,'Louisiana'
exec gfisp_additem 'STATE','ME',1, 19,'Maine'
exec gfisp_additem 'STATE','MD',1, 20,'Maryland'
exec gfisp_additem 'STATE','MA',1, 21,'Massachusetts'
exec gfisp_additem 'STATE','MI',1, 22,'Michigan'
exec gfisp_additem 'STATE','MN',1, 23,'Minnesota'
exec gfisp_additem 'STATE','MS',1, 24,'Mississippi'
exec gfisp_additem 'STATE','MO',1, 25,'Missouri'
exec gfisp_additem 'STATE','MT',1, 26,'Montana'
exec gfisp_additem 'STATE','NE',1, 27,'Nebraska'
exec gfisp_additem 'STATE','NV',1, 28,'Nevada'
exec gfisp_additem 'STATE','NH',1, 29,'New Hampshire'
exec gfisp_additem 'STATE','NJ',1, 30,'New Jersey'
exec gfisp_additem 'STATE','NM',1, 31,'New Mexico'
exec gfisp_additem 'STATE','NY',1, 32,'New York'
exec gfisp_additem 'STATE','NC',1, 33,'North Carolina'
exec gfisp_additem 'STATE','ND',1, 34,'North Dakota'
exec gfisp_additem 'STATE','OH',1, 35,'Ohio'
exec gfisp_additem 'STATE','OK',1, 36,'Oklahoma'
exec gfisp_additem 'STATE','OR',1, 37,'Oregon'
exec gfisp_additem 'STATE','PA',1, 38,'Pennsylvania'
exec gfisp_additem 'STATE','RI',1, 39,'Rhode Island'
exec gfisp_additem 'STATE','SC',1, 40,'South Carolina'
exec gfisp_additem 'STATE','SD',1, 41,'South Dakota'
exec gfisp_additem 'STATE','TN',1, 42,'Tennessee'
exec gfisp_additem 'STATE','TX',1, 43,'Texas'
exec gfisp_additem 'STATE','UT',1, 44,'Utah'
exec gfisp_additem 'STATE','VT',1, 45,'Vermont'
exec gfisp_additem 'STATE','VA',1, 46,'Virginia'
exec gfisp_additem 'STATE','WA',1, 47,'Washington'
exec gfisp_additem 'STATE','WV',1, 48,'West Virginia'
exec gfisp_additem 'STATE','WI',1, 49,'Wisconsin'
exec gfisp_additem 'STATE','WY',1, 50,'Wyoming'

exec gfisp_additem 'STATE','AS',1, 51,'American Samoa'
exec gfisp_additem 'STATE','DC',1, 52,'District of Columbia'
exec gfisp_additem 'STATE','GU',1, 53,'Guam'
exec gfisp_additem 'STATE','MH',1, 54,'Marshall Islands'
exec gfisp_additem 'STATE','FM',1, 55,'Micronesia'
exec gfisp_additem 'STATE','MP',1, 56,'Northern Marianas'
exec gfisp_additem 'STATE','PW',1, 57,'Palau'
exec gfisp_additem 'STATE','PR',1, 58,'Puerto Rico'
exec gfisp_additem 'STATE','VI',1, 59,'Virgin Islands'

exec gfisp_additem 'STATE','AB',2, 61,'Alberta'
exec gfisp_additem 'STATE','BC',2, 62,'British Columbia'
exec gfisp_additem 'STATE','MB',2, 63,'Manitoba'
exec gfisp_additem 'STATE','NB',2, 64,'New Brunswick'
exec gfisp_additem 'STATE','NL',2, 65,'Newfoundland and Labrador'
exec gfisp_additem 'STATE','NT',2, 66,'Northwest Territories'
exec gfisp_additem 'STATE','NS',2, 67,'Nova Scotia'
exec gfisp_additem 'STATE','NU',2, 68,'Nunavut'
exec gfisp_additem 'STATE','ON',2, 69,'Ontario'
exec gfisp_additem 'STATE','PE',2, 70,'Prince Edward Island'
exec gfisp_additem 'STATE','QC',2, 71,'Quebec'
exec gfisp_additem 'STATE','SK',2, 72,'Saskatchewan'
exec gfisp_additem 'STATE','YT',2, 73,'Yukon'

exec gfisp_additem 'STATE','AG',3, 81,'Aguascalientes'
exec gfisp_additem 'STATE','BN',3, 82,'Baja California'
exec gfisp_additem 'STATE','BS',3, 83,'Baja California Sur'
exec gfisp_additem 'STATE','CM',3, 84,'Campeche'
exec gfisp_additem 'STATE','CP',3, 85,'Chiapas'
exec gfisp_additem 'STATE','CH',3, 86,'Chihuahua'
exec gfisp_additem 'STATE','CA',3, 87,'Coahuila'
exec gfisp_additem 'STATE','CL',3, 88,'Colima'
exec gfisp_additem 'STATE','DF',3, 89,'Distrito Federal'
exec gfisp_additem 'STATE','DU',3, 90,'Durango'
exec gfisp_additem 'STATE','GJ',3, 91,'Guanajuato'
exec gfisp_additem 'STATE','GR',3, 92,'Guerrero'
exec gfisp_additem 'STATE','HI',3, 93,'Hidalgo'
exec gfisp_additem 'STATE','JA',3, 94,'Jalisco'
exec gfisp_additem 'STATE','MX',3, 95,'M�xico'
exec gfisp_additem 'STATE','MC',3, 96,'Michoac�n'
exec gfisp_additem 'STATE','MR',3, 97,'Morelos'
exec gfisp_additem 'STATE','NA',3, 98,'Nayarit'
exec gfisp_additem 'STATE','NL',3, 99,'Nuevo Le�n'
exec gfisp_additem 'STATE','OA',3,100,'Oaxaca'
exec gfisp_additem 'STATE','PU',3,101,'Puebla'
exec gfisp_additem 'STATE','QE',3,102,'Quer�taro'
exec gfisp_additem 'STATE','QR',3,103,'Quintana Roo'
exec gfisp_additem 'STATE','SL',3,104,'San Luis Potos�'
exec gfisp_additem 'STATE','SI',3,105,'Sinaloa'
exec gfisp_additem 'STATE','SO',3,106,'Sonora'
exec gfisp_additem 'STATE','TB',3,107,'Tabasco'
exec gfisp_additem 'STATE','TM',3,108,'Tamaulipas'
exec gfisp_additem 'STATE','TL',3,109,'Tlaxcala'
exec gfisp_additem 'STATE','VE',3,110,'Veracruz'
exec gfisp_additem 'STATE','YU',3,111,'Yucat�n'
exec gfisp_additem 'STATE','ZA',3,112,'Zacatecas'
go

------------------------------------------------------------------------
-- List: STOCK TYPE
------------------------------------------------------------------------
exec gfisp_additem 'STOCK TYPE','Unknown type',0, 0,'Unknown stock type'
exec gfisp_additem 'STOCK TYPE','Nickel',      0, 1
exec gfisp_additem 'STOCK TYPE','Dime',        0, 2
exec gfisp_additem 'STOCK TYPE','Quarter',     0, 3
exec gfisp_additem 'STOCK TYPE','SBA',         0, 4
exec gfisp_additem 'STOCK TYPE','Token 1',     0, 5
exec gfisp_additem 'STOCK TYPE','Token 2',     0, 6
exec gfisp_additem 'STOCK TYPE','Spare coin',  0, 7
exec gfisp_additem 'STOCK TYPE','Ride Pass',   0, 8,'TRiM ride pass stock'
exec gfisp_additem 'STOCK TYPE','Day Pass',    0, 9,'TRiM day pass stock'
exec gfisp_additem 'STOCK TYPE','30-Day Pass', 0,10,'TRiM 30-day pass stock'
go

------------------------------------------------------------------------
-- List: SUM LABEL
------------------------------------------------------------------------
exec gfisp_additem 'SUM LABEL','TVM',  1, 1,'Number of TVMs'
exec gfisp_additem 'SUM LABEL','REV',  1, 2,'Revenue'
exec gfisp_additem 'SUM LABEL','ISS',  1, 3,'Cards issued'            -- transaction type 302
exec gfisp_additem 'SUM LABEL','REC',  1, 4,'Cards recharged'         -- transaction types 303 & 312
exec gfisp_additem 'SUM LABEL','VAL',  1, 5,'Cards validated'         -- transaction type 316
exec gfisp_additem 'SUM LABEL','CUS',  1, 6,'Customers served'        -- transaction types 302-305, 307, 308, 311, 312, 314, 316, 317, 319, 320, excluding child transactions (tr_seq>0)

exec gfisp_additem 'SUM LABEL','RVS',  1,21,'Need revenue service'    -- srv_flags.S_SRV_Rev
exec gfisp_additem 'SUM LABEL','INRVS',1,22,'In revenue service'      -- srv_flags.S_SRV_In_Rev
exec gfisp_additem 'SUM LABEL','MNT',  1,23,'Need maintenance'        -- srv_flags.S_SRV_Mnt
exec gfisp_additem 'SUM LABEL','INMNT',1,24,'In maintenance'          -- srv_flags.S_SRV_In_Mnt
exec gfisp_additem 'SUM LABEL','INSVC',1,25,'In service'              -- srv_flags.S_SRV_In_Svc
exec gfisp_additem 'SUM LABEL','OOS',  1,26,'Out of service'          -- flags.S_LOCALOOS or flags.S_REMOTEOOS
exec gfisp_additem 'SUM LABEL','SEC',  1,27,'Security alert'          -- srv_flags.S_SRV_Security
exec gfisp_additem 'SUM LABEL','DIS',  1,28,'Disconnected'            -- srv_flags.S_SRV_In_Rev/S_SRV_In_Mnt/S_SRV_In_Svc are all off

exec gfisp_additem 'SUM LABEL','SEQ',  1,41,'System'                  -- gfi_eq.status
exec gfisp_additem 'SUM LABEL','STRM', 1,42,'     TRiM'               -- gfi_eq.status_trim
exec gfisp_additem 'SUM LABEL','SHPR', 1,43,'     Hopper'             -- gfi_eq.status_hpr
exec gfisp_additem 'SUM LABEL','SBTP', 1,44,'     Bill tekpak'        -- gfi_eq.status_btp
exec gfisp_additem 'SUM LABEL','SBST', 1,45,'     Bill stacker'       -- gfi_eq.status_bst
exec gfisp_additem 'SUM LABEL','SCTP', 1,46,'     Coin tekpak'        -- gfi_eq.status_ctp
exec gfisp_additem 'SUM LABEL','SCBX', 1,47,'     Cashbox'            -- gfi_eq.status_cbx
exec gfisp_additem 'SUM LABEL','SCRD', 1,48,'     Card reader'        -- gfi_eq.status_crd
exec gfisp_additem 'SUM LABEL','SPIN', 1,49,'     Pinpad'             -- gfi_eq.status_pin
exec gfisp_additem 'SUM LABEL','SSMC', 1,50,'     Smartcard'          -- gfi_eq.status_smc
exec gfisp_additem 'SUM LABEL','SPRN', 1,51,'     Printer'            -- gfi_eq.status_prn
exec gfisp_additem 'SUM LABEL','SUPS', 1,52,'     UPS'                -- gfi_eq.status_ups
exec gfisp_additem 'SUM LABEL','SCPU', 1,53,'     Computer'           -- gfi_eq.status_cpu

exec gfisp_additem 'SUM LABEL','LUPT', 1,61,'Last updated'            -- gfi_eq.last_update_ts
exec gfisp_additem 'SUM LABEL','LPOT', 1,62,'Last polled'             -- gfi_eq.last_poll_ts
exec gfisp_additem 'SUM LABEL','LPOU', 1,63,'Last polled by'          -- gfi_eq.last_poll_uid

exec gfisp_additem 'SUM LABEL','PEM',  2, 1,'Number of PEMs'
exec gfisp_additem 'SUM LABEL','REV',  2, 2,'Revenue'
exec gfisp_additem 'SUM LABEL','ISS',  2, 3,'Cards issued'            -- transaction type 302
exec gfisp_additem 'SUM LABEL','REC',  2, 4,'Cards recharged'         -- transaction types 303 & 312
exec gfisp_additem 'SUM LABEL','VAL',  2, 5,'Cards validated'         -- transaction type 316
exec gfisp_additem 'SUM LABEL','CUS',  2, 6,'Customers served'        -- transaction types 302-305, 307, 308, 311, 312, 314, 316, 317, 319, 320, excluding child transactions (tr_seq>0)

exec gfisp_additem 'SUM LABEL','RVS',  2,21,'Need revenue service'    -- srv_flags.S_SRV_Rev
exec gfisp_additem 'SUM LABEL','INRVS',2,22,'In revenue service'      -- srv_flags.S_SRV_In_Rev
exec gfisp_additem 'SUM LABEL','MNT',  2,23,'Need maintenance'        -- srv_flags.S_SRV_Mnt
exec gfisp_additem 'SUM LABEL','INMNT',2,24,'In maintenance'          -- srv_flags.S_SRV_In_Mnt
exec gfisp_additem 'SUM LABEL','INSVC',2,25,'In service'              -- srv_flags.S_SRV_In_Svc
exec gfisp_additem 'SUM LABEL','OOS',  2,26,'Out of service'          -- flags.S_LOCALOOS or flags.S_REMOTEOOS
exec gfisp_additem 'SUM LABEL','SEC',  2,27,'Security alert'          -- srv_flags.S_SRV_Security
exec gfisp_additem 'SUM LABEL','DIS',  2,28,'Disconnected'            -- srv_flags.S_SRV_In_Rev/S_SRV_In_Mnt/S_SRV_In_Svc are all off

exec gfisp_delitem 'SUM LABEL',null,0                                 -- remove old SUM LABEL items with class=0
go

------------------------------------------------------------------------
-- List: SYS PARM
------------------------------------------------------------------------
exec gfisp_additem 'SYS PARM','|EQ DISK SPACE WARN 1',0,null,'90%'
exec gfisp_additem 'SYS PARM','|EQ DISK SPACE WARN 2',0,null,'95%'
exec gfisp_additem 'SYS PARM','|MAGNETIC FORMAT',     0,null,'0'
exec gfisp_additem 'SYS PARM','|RBBL',                0,null,'No'
exec gfisp_additem 'SYS PARM','|FBX FIRMWARE NEW',    0,null,'Yes'
exec gfisp_additem 'SYS PARM','|SOTD',                0,null,'00:00:00'
exec gfisp_additem 'SYS PARM','|EQ STATUS PRIORITY',  0,null,'4'
exec gfisp_additem 'SYS PARM','|DEBUG SP',            0,null,'No'
exec gfisp_additem 'SYS PARM','|DEBUG TRG',           0,null,'No'
go

------------------------------------------------------------------------
-- List: SYS PARM HELP
------------------------------------------------------------------------
exec gfisp_additem 'SYS PARM HELP','|EQ DISK SPACE WARN 1',0,null,'|;TVM/PEM disk space usage percentage warning threshold 1 (n% full)'
exec gfisp_additem 'SYS PARM HELP','|EQ DISK SPACE WARN 2',0,null,'|;TVM/PEM disk space usage percentage warning threshold 2 (n% full)'
exec gfisp_additem 'SYS PARM HELP','|MAGNETIC FORMAT',     0,null,'|N0-1;Magnetic format version to use in generating bad list binary file (0 - original format; 1 - new format)'
exec gfisp_additem 'SYS PARM HELP','|RBBL',                0,null,'|BY;Real Big Bad List (maximum bad list records allowed): Yes - 10,000 records; No - 2,500 records; 100k (100k badlist);'
exec gfisp_additem 'SYS PARM HELP','|FBX FIRMWARE NEW',    0,null,'|BY;New farebox firmware version: Yes - >=350 for CaB or >400 for Odyssey; No - legacy version (this setting has an impact on bad list processing)'
exec gfisp_additem 'SYS PARM HELP','|SOTD',                0,null,'|T;Start time of a transit date'
exec gfisp_additem 'SYS PARM HELP','|EQ STATUS PRIORITY',  0,null,'|N3-4;Top priority TVM/PEM status (3 - security alert; 4 - disconnected)'
exec gfisp_additem 'SYS PARM HELP','|DEBUG SP',            0,null,'|BY;Database stored procedure debug flag: Yes - debug mode on; No - debug mode off'
exec gfisp_additem 'SYS PARM HELP','|DEBUG TRG',           0,null,'|BY;Database trigger debug flag: Yes - debug mode on; No - debug mode off'
go

------------------------------------------------------------------------
-- Populate TVM/PEM list table gfi_eq
------------------------------------------------------------------------
if not exists(select null from gfi_eq)               -- if no TVM/PEN was found, add a default one
begin
   begin transaction

   insert into gfi_eq (loc_n,eq_type,eq_n,eq_label,eq_desc) values(0,1,1,'TVM 1','TVM 1, not stationed')
   insert into gfi_eq (loc_n,eq_type,eq_n,eq_label,eq_desc) values(0,2,1,'PEM 1','PEM 1, not stationed')

   commit transaction
end
go

declare @ls    varchar(256)
declare @ls_eq varchar(4000)
declare @li    smallint
declare @loc   smallint

------------------------------------------------------------------------
-- List: TVM PARM
------------------------------------------------------------------------
begin transaction

select @ls_eq=dbo.list(eq_n)+',' from gfi_eq where eq_type=1 and eq_n>0
while (@ls_eq<>'' and @ls_eq<>',')
begin
   set @li=charindex(',',@ls_eq)
   set @loc=cast(left(@ls_eq,@li - 1) as smallint)
   set @ls_eq=rtrim(substring(@ls_eq,@li+1,4000))

   exec gfisp_additem 'TVM PARM','|IP@',                     @loc
   exec gfisp_additem 'TVM PARM','|MAC@',                    @loc

   select @ls=min(name) from gfi_lst loc, gfi_eq eq where type='EQ LOC' and eq_type=1 and eq_n=@loc and loc_n=code
   exec gfisp_additem 'TVM PARM','|STATION NAME',            @loc,null,ls

   exec gfisp_additem 'TVM PARM','|STATION NAME SHORT',      @loc
   exec gfisp_additem 'TVM PARM','|ADDR1',                   @loc
   exec gfisp_additem 'TVM PARM','|ADDR2',                   @loc

   exec gfisp_additem 'TVM PARM','|L2G URL MODE',            @loc,null,'TEST'
   exec gfisp_additem 'TVM PARM','|L2G MERCHANT CODE',       @loc
   exec gfisp_additem 'TVM PARM','|L2G SETTLE MERCHANT CODE',@loc
   exec gfisp_additem 'TVM PARM','|L2G PWD',                 @loc
   exec gfisp_additem 'TVM PARM','|ISP PHONE',               @loc
   exec gfisp_additem 'TVM PARM','|ISP UID',                 @loc
   exec gfisp_additem 'TVM PARM','|ISP PWD',                 @loc
   exec gfisp_additem 'TVM PARM','|DIALUP PHONE',            @loc
   exec gfisp_additem 'TVM PARM','|DIALUP UID',              @loc,null,'VIP'
   exec gfisp_additem 'TVM PARM','|DIALUP PWD',              @loc
end

delete from gfi_lst where type='TVM PARM' and class not in (select eq_n from gfi_eq where eq_type=1 and eq_n>0)

commit transaction
go

------------------------------------------------------------------------
-- List: TVM PARM HELP (1 - TVM unique parameter)
------------------------------------------------------------------------
exec gfisp_additem 'TVM PARM HELP','|IP@',                     0,null,'|;1;TVM IP address'
exec gfisp_additem 'TVM PARM HELP','|MAC@',                    0,null,'|;1;TVM MAC address'
exec gfisp_additem 'TVM PARM HELP','|STATION NAME',            0,null,'|;1;TVM station name'
exec gfisp_additem 'TVM PARM HELP','|STATION NAME SHORT',      0,null,'|;1;TVM station abbreviated name'
exec gfisp_additem 'TVM PARM HELP','|ADDR1',                   0,null,'|;1;TVM address 1'
exec gfisp_additem 'TVM PARM HELP','|ADDR2',                   0,null,'|;1;TVM address 2'

exec gfisp_additem 'TVM PARM HELP','|L2G URL MODE',            0,null,'|;;FIS PayDirect (Link2Gov) connection type: TEST - test server; PROD - production server'
exec gfisp_additem 'TVM PARM HELP','|L2G MERCHANT CODE',       0,null,'|;;FIS PayDirect (Link2Gov) merchant code'
exec gfisp_additem 'TVM PARM HELP','|L2G SETTLE MERCHANT CODE',0,null,'|;;FIS PayDirect (Link2Gov) settlement merchant code'
exec gfisp_additem 'TVM PARM HELP','|L2G PWD',                 0,null,'|;;FIS PayDirect (Link2Gov) password'

exec gfisp_additem 'TVM PARM HELP','|ISP PHONE',               0,null,'|;;TVM ISP dial-up phone number'
exec gfisp_additem 'TVM PARM HELP','|ISP UID',                 0,null,'|;1;TVM ISP dial-up user ID'
exec gfisp_additem 'TVM PARM HELP','|ISP PWD',                 0,null,'|;1;TVM ISP dial-up password'
exec gfisp_additem 'TVM PARM HELP','|DIALUP PHONE',            0,null,'|;1;TVM dial-up connection phone number'
exec gfisp_additem 'TVM PARM HELP','|DIALUP UID',              0,null,'|;;TVM dial-up connection user ID'
exec gfisp_additem 'TVM PARM HELP','|DIALUP PWD',              0,null,'|;;TVM dial-up connection password'
go

declare @ls    varchar(256)
declare @ls_eq varchar(4000)
declare @li    smallint
declare @loc   smallint

------------------------------------------------------------------------
-- List: PEM PARM
------------------------------------------------------------------------
begin transaction

select @ls_eq=dbo.list(eq_n)+',' from gfi_eq where eq_type=2 and eq_n>0
while (@ls_eq<>'' and @ls_eq<>',')
begin
   set @li=charindex(',',@ls_eq)
   set @loc=cast(left(@ls_eq,@li - 1) as smallint)
   set @ls_eq=rtrim(substring(@ls_eq,@li+1,4000))

   exec gfisp_additem 'PEM PARM','|IP@',@loc
end

delete from gfi_lst where type='PEM PARM' and class not in (select eq_n from gfi_eq where eq_type=2 and eq_n>0)

commit transaction
go

------------------------------------------------------------------------
-- List: PEM PARM HELP (1 - PEM unique parameter)
------------------------------------------------------------------------
exec gfisp_additem 'PEM PARM HELP','|IP@',0,null,'|;1;PEM IP address'
go

------------------------------------------------------------------------
-- List: TVM PARM SEQ
------------------------------------------------------------------------
exec gfisp_additem 'TVM PARM SEQ','Default',    0,1,'2011-06-01 00:00:00'
exec gfisp_additem 'TVM PARM SEQ','Alternative',0,2,'2011-06-01 00:00:00'
go

------------------------------------------------------------------------
-- List: PEM PARM SEQ
------------------------------------------------------------------------
exec gfisp_additem 'PEM PARM SEQ','Default',    0,1,'2011-06-01 00:00:00'
exec gfisp_additem 'PEM PARM SEQ','Alternative',0,2,'2011-06-01 00:00:00'
go

------------------------------------------------------------------------
-- List: USER ROLE
------------------------------------------------------------------------
exec gfisp_additem 'USER ROLE','Owner',        0,1,'Owner of the card'
exec gfisp_additem 'USER ROLE','Administrator',0,2,'Authorized user with administrative access'
exec gfisp_additem 'USER ROLE','Viewer',       0,3,'Authorized user with view only access'
go

------------------------------------------------------------------------
-- List: USER TYPE
------------------------------------------------------------------------
exec gfisp_additem 'USER TYPE','Not defined',0,0
go

------------------------------------------------------------------------
-- List: WTA Rebate program parameter
------------------------------------------------------------------------
exec gfisp_additem 'WP REBATE','|Fare ID ',0,null,'999'
exec gfisp_additem 'WP REBATE','|Eligible User Profiles ',0,null,'999'
go

------------------------------------------------------------------------
-- List: WTA exempt fare_id parameter (LG-1226)
------------------------------------------------------------------------
exec gfisp_additem 'EXEMPT FARE_ID','EXEMPT FARE_ID', 0,0,'1050'
go

------------------------------------------------------------------------
-- List: TOKEN VALUE
------------------------------------------------------------------------
exec gfisp_additem 'TOKEN VALUE','Token 1 Cash Value',0,1,'0'
exec gfisp_additem 'TOKEN VALUE','Token 2 Cash Value',0,2,'0'
go

------------------------------------------------------------------------
-- populate attr table
------------------------------------------------------------------------
-- attr = 0
if exists (select null from attr where attr = 0)
	update attr set description='Undefined',text='Undef',ext_description='Undefined' where attr = 0;
else
	insert into attr (attr,description,text,ext_description) values(0,'Undefined','Undef','Undefined');
-- attr = 1
if exists (select null from attr where attr = 1)
	update attr set description='TallyClear',text='T & C',ext_description='Fare value for tally and clear' where attr = 1;
else
	insert into attr (attr,description,text,ext_description) values(1,'TallyClear','T & C','Fare value for tally and clear');
-- attr = 2
if exists (select null from attr where attr = 2)
	update attr set description='TallyDumpClear',text='T-D & C',ext_description='Fare value for tally, dump and clear' where attr = 2;
else
	insert into attr (attr,description,text,ext_description) values(2,'TallyDumpClear','T-D & C','Fare value for tally, dump and clear');
-- attr = 3
if exists (select null from attr where attr = 3)
	update attr set description='UseCurrentValue',text='CurrFare',ext_description='Use current value to compute the fare' where attr = 3;
else
	insert into attr (attr,description,text,ext_description) values(3,'UseCurrentValue','CurrFare','Use current value to compute the fare');
-- attr = 4
if exists (select null from attr where attr = 4)
	update attr set description='OverrideTransfer',text='O/T',ext_description='Fare value that flags the farebox to use' where attr = 4;
else
	insert into attr (attr,description,text,ext_description) values(4,'OverrideTransfer','O/T','Fare value that flags the farebox to use');
-- attr = 5
if exists (select null from attr where attr = 5)
	update attr set description='_05DollarBill',text='$5 Bill',ext_description='$5 dollar bill' where attr = 5;
else
	insert into attr (attr,description,text,ext_description) values(5,'_05DollarBill','$5 Bill','$5 dollar bill');
-- attr = 6
if exists (select null from attr where attr = 6)
	update attr set description='_10DollarBill',text='$10 Bill',ext_description='$10 dollar bill' where attr = 6;
else
	insert into attr (attr,description,text,ext_description) values(6,'_10DollarBill','$10 Bill','$10 dollar bill');
-- attr = 7
if exists (select null from attr where attr = 7)
	update attr set description='_20DollarBill',text='$20 Bill',ext_description='$20 dollar bill' where attr = 7;
else
	insert into attr (attr,description,text,ext_description) values(7,'_20DollarBill','$20 Bill','$20 dollar bill');
-- attr = 8
if exists (select null from attr where attr = 8)
	update attr set description='RejectTRiMCard',text='RejCard',ext_description='Return TRiM card without processing' where attr = 8;
else
	insert into attr (attr,description,text,ext_description) values(8,'RejectTRiMCard','RejCard','Return TRiM card without processing');
-- attr = 9
if exists (select null from attr where attr = 9)
	update attr set description='ReadTRiMCard',text='ReadCard',ext_description='Read TRiM card without processing' where attr = 9;
else
	insert into attr (attr,description,text,ext_description) values(9,'ReadTRiMCard','ReadCard','Read TRiM card without processing');
-- attr = 10
if exists (select null from attr where attr = 10)
	update attr set description='RestoreTRiMCard',text='RestCard',ext_description='Restore TRiM card without processing' where attr = 10;
else
	insert into attr (attr,description,text,ext_description) values(10,'RestoreTRiMCard','RestCard','Restore TRiM card without processing');
-- attr = 11
if exists (select null from attr where attr = 11)
	update attr set description='IssueTransferKey',text='XferPrev',ext_description='Issue transfer based on previous fare' where attr = 11;
else
	insert into attr (attr,description,text,ext_description) values(11,'IssueTransferKey','XferPrev','Issue transfer based on previous fare');
-- attr = 12
if exists (select null from attr where attr = 12)
	update attr set description='ChangeCard',text='Change',ext_description='Issue change card' where attr = 12;
else
	insert into attr (attr,description,text,ext_description) values(12,'ChangeCard','Change','Issue change card');
-- attr = 13
if exists (select null from attr where attr = 13)
	update attr set description='HoldCard',text='HoldCard',ext_description='Hold card in escrow' where attr = 13;
else
	insert into attr (attr,description,text,ext_description) values(13,'HoldCard','HoldCard','Hold card in escrow');
-- attr = 14
if exists (select null from attr where attr = 14)
	update attr set description='ReleaseCard',text='RelCard',ext_description='Release card from escrow' where attr = 14;
else
	insert into attr (attr,description,text,ext_description) values(14,'ReleaseCard','RelCard','Release card from escrow');
-- attr = 15
if exists (select null from attr where attr = 15)
	update attr set description='HoldFare',text='HoldFare',ext_description='Ignore current fare value' where attr = 15;
else
	insert into attr (attr,description,text,ext_description) values(15,'HoldFare','HoldFare','Ignore current fare value');
-- attr = 16
if exists (select null from attr where attr = 16)
	update attr set description='ProcessFare',text='ProcFare',ext_description='Process current fare value' where attr = 16;
else
	insert into attr (attr,description,text,ext_description) values(16,'ProcessFare','ProcFare','Process current fare value');
-- attr = 17
if exists (select null from attr where attr = 17)
	update attr set description='BillEnable',text='BillEnab',ext_description='Enable/disable bill transport' where attr = 17;
else
	insert into attr (attr,description,text,ext_description) values(17,'BillEnable','BillEnab','Enable/disable bill transport');
-- attr = 18
if exists (select null from attr where attr = 18)
	update attr set description='BillOverride',text='BillOver',ext_description='Bill override' where attr = 18;
else
	insert into attr (attr,description,text,ext_description) values(18,'BillOverride','BillOver','Bill override');
-- attr = 19
if exists (select null from attr where attr = 19)
	update attr set description='TicketOverride',text='TickOver',ext_description='Ticket override' where attr = 19;
else
	insert into attr (attr,description,text,ext_description) values(19,'TicketOverride','TickOver','Ticket override');
-- attr = 20
if exists (select null from attr where attr = 20)
	update attr set description='Multi-Fare',text='MultFare',ext_description='Multi-Fare' where attr = 20;
else
	insert into attr (attr,description,text,ext_description) values(20,'Multi-Fare','MultFare','Multi-Fare');
-- attr = 21
if exists (select null from attr where attr = 21)
	update attr set description='FullFare+Transfer',text='FF+Xfer',ext_description='Full Fare + Transfer' where attr = 21;
else
	insert into attr (attr,description,text,ext_description) values(21,'FullFare+Transfer','FF+Xfer','Full Fare + Transfer');
-- attr = 22
if exists (select null from attr where attr = 22)
	update attr set description='TransferFareOverride',text='XferFOvr',ext_description='Transfer Full Fare Override' where attr = 22;
else
	insert into attr (attr,description,text,ext_description) values(22,'TransferFareOverride','XferFOvr','Transfer Full Fare Override');
-- attr = 23
if exists (select null from attr where attr = 23)
	update attr set description='Recharge',text='Recharge',ext_description='Add to stored value' where attr = 23;
else
	insert into attr (attr,description,text,ext_description) values(23,'Recharge','Recharge','Add to stored value');
-- attr = 24
if exists (select null from attr where attr = 24)
	update attr set description='Swap Zone',text='SwapZone',ext_description='Swap Zone' where attr = 24;
else
	insert into attr (attr,description,text,ext_description) values(24,'Swap Zone','SwapZone','Swap Zone');
-- attr = 25
if exists (select null from attr where attr = 25)
	update attr set description='BillOverrideScreen',text='BlOvrScr',ext_description='Display bill Override choice Screen (Odyssey with menued OCU only)' where attr = 25;
else
	insert into attr (attr,description,text,ext_description) values(25,'BillOverrideScreen','BlOvrScr','Display bill Override choice Screen (Odyssey with menued OCU only)');
-- attr = 26
if exists (select null from attr where attr = 26)
	update attr set description='IssueScreen',text='IssueScr',ext_description='Issue screen' where attr = 26;
else
	insert into attr (attr,description,text,ext_description) values(26,'IssueScreen','IssueScr','Issue screen');
-- attr = 27
if exists (select null from attr where attr = 27)
	update attr set description='CPUReset',text='CPUReset',ext_description='Farebox warm or cold starts' where attr = 27;
else
	insert into attr (attr,description,text,ext_description) values(27,'CPUReset','CPUReset','Farebox warm or cold starts');
-- attr = 28
if exists (select null from attr where attr = 28)
	update attr set description='IssueDollarValueCard',text='XValCard',ext_description='When key is pressed, puts the farebox in hold and accepts money. When same key pressed, issues a value card for the dollar amount on the Drivers Display.' where attr = 28;
else
	insert into attr (attr,description,text,ext_description) values(28,'IssueDollarValueCard','XValCard','When key is pressed, puts the farebox in hold and accepts money. When same key pressed, issues a value card for the dollar amount on the Drivers Display.');
-- attr = 29
if exists (select null from attr where attr = 29)
	update attr set description='AutoCouponOverride',text='CoupOvr',ext_description='Automatically override coupon' where attr = 29;
else
	insert into attr (attr,description,text,ext_description) values(29,'AutoCouponOverride','CoupOvr','Automatically override coupon');
-- attr = 30
if exists (select null from attr where attr = 30)
	update attr set description='TwoBillReclassify',text='$2 Bill',ext_description='$2 bill reclassify' where attr = 30;
else
	insert into attr (attr,description,text,ext_description) values(30,'TwoBillReclassify','$2 Bill','$2 bill reclassify');
-- attr = 31
if exists (select null from attr where attr = 31)
	update attr set description='DisplayFreeTally',text='DspFree',ext_description='Display free tally' where attr = 31;
else
	insert into attr (attr,description,text,ext_description) values(31,'DisplayFreeTally','DspFree','Display free tally');
-- attr = 32
if exists (select null from attr where attr = 32)
	update attr set description='DisplayTally',text='DspTally',ext_description='Display tally' where attr = 32;
else
	insert into attr (attr,description,text,ext_description) values(32,'DisplayTally','DspTally','Display tally');
-- attr = 33
if exists (select null from attr where attr = 33)
	update attr set description='DisplayNoTextTally',text='DspNoTxt',ext_description='Display no text for tally' where attr = 33;
else
	insert into attr (attr,description,text,ext_description) values(33,'DisplayNoTextTally','DspNoTxt','Display no text for tally');
-- attr = 34
if exists (select null from attr where attr = 34)
	update attr set description='DisableCoinMech',text='DisablCM',ext_description='Disable coin mech' where attr = 34;
else
	insert into attr (attr,description,text,ext_description) values(34,'DisableCoinMech','DisablCM','Disable coin mech');
-- attr = 35
if exists (select null from attr where attr = 35)
	update attr set description='DisableBillValidator',text='DisablBV',ext_description='Disable bill validator' where attr = 35;
else
	insert into attr (attr,description,text,ext_description) values(35,'DisableBillValidator','DisablBV','Disable bill validator');
-- attr = 36
if exists (select null from attr where attr = 36)
	update attr set description='DisableCoinMech&BillValidator',text='DisCMBV',ext_description='Disable coin mech and bill validator' where attr = 36;
else
	insert into attr (attr,description,text,ext_description) values(36,'DisableCoinMech&BillValidator','DisCMBV','Disable coin mech and bill validator');
-- attr = 37
if exists (select null from attr where attr = 37)
	update attr set description='TallyClearBlank',text='T&CBlank',ext_description='Tally & Clear, but don''t show "T & C"' where attr = 37;
else
	insert into attr (attr,description,text,ext_description) values(37,'TallyClearBlank','T&CBlank','Tally & Clear, but don''t show "T & C"');
-- attr = 38
if exists (select null from attr where attr = 38)
	update attr set description='Swap Fareset 1',text='SwapFS1',ext_description='Swap Fareset 1' where attr = 38;
else
	insert into attr (attr,description,text,ext_description) values(38,'Swap Fareset 1','SwapFS1','Swap Fareset 1');
-- attr = 39
if exists (select null from attr where attr = 39)
	update attr set description='Swap Fareset 2',text='SwapFS2',ext_description='Swap Fareset 2' where attr = 39;
else
	insert into attr (attr,description,text,ext_description) values(39,'Swap Fareset 2','SwapFS2','Swap Fareset 2');
-- attr = 40
if exists (select null from attr where attr = 40)
	update attr set description='Swap Fareset 3',text='SwapFS3',ext_description='Swap Fareset 3' where attr = 40;
else
	insert into attr (attr,description,text,ext_description) values(40,'Swap Fareset 3','SwapFS3','Swap Fareset 3');
-- attr = 41
if exists (select null from attr where attr = 41)
	update attr set description='Swap Fareset 4',text='SwapFS4',ext_description='Swap Fareset 4' where attr = 41;
else
	insert into attr (attr,description,text,ext_description) values(41,'Swap Fareset 4','SwapFS4','Swap Fareset 4');
-- attr = 42
if exists (select null from attr where attr = 42)
	update attr set description='Swap Fareset 5',text='SwapFS5',ext_description='Swap Fareset 5' where attr = 42;
else
	insert into attr (attr,description,text,ext_description) values(42,'Swap Fareset 5','SwapFS5','Swap Fareset 5');
-- attr = 43
if exists (select null from attr where attr = 43)
	update attr set description='Swap Fareset 6',text='SwapFS6',ext_description='Swap Fareset 6' where attr = 43;
else
	insert into attr (attr,description,text,ext_description) values(43,'Swap Fareset 6','SwapFS6','Swap Fareset 6');
-- attr = 44
if exists (select null from attr where attr = 44)
	update attr set description='Swap Fareset 7',text='SwapFS7',ext_description='Swap Fareset 7' where attr = 44;
else
	insert into attr (attr,description,text,ext_description) values(44,'Swap Fareset 7','SwapFS7','Swap Fareset 7');
-- attr = 45
if exists (select null from attr where attr = 45)
	update attr set description='Swap Fareset 8',text='SwapFS8',ext_description='Swap Fareset 8' where attr = 45;
else
	insert into attr (attr,description,text,ext_description) values(45,'Swap Fareset 8','SwapFS8','Swap Fareset 8');
-- attr = 46
if exists (select null from attr where attr = 46)
	update attr set description='Swap Fareset 9',text='SwapFS9',ext_description='Swap Fareset 9' where attr = 46;
else
	insert into attr (attr,description,text,ext_description) values(46,'Swap Fareset 9','SwapFS9','Swap Fareset 9');
-- attr = 47
if exists (select null from attr where attr = 47)
	update attr set description='Swap Fareset 10',text='SwapFS10',ext_description='Swap Fareset 10' where attr = 47;
else
	insert into attr (attr,description,text,ext_description) values(47,'Swap Fareset 10','SwapFS10','Swap Fareset 10');
-- attr = 48
if exists (select null from attr where attr = 48)
	update attr set description='TallyKeyNoEffectOnDisplay',text='NoOCUEff',ext_description='Tally key has no effect on OCU or patron display; count key only' where attr = 48;
else
	insert into attr (attr,description,text,ext_description) values(48,'TallyKeyNoEffectOnDisplay','NoOCUEff','Tally key has no effect on OCU or patron display; count key only');
-- attr = 49
if exists (select null from attr where attr = 49)
	update attr set description='IgnorePassbackOnNextCard',text='IgnorePB',ext_description='Ignore passback on next card' where attr = 49;
else
	insert into attr (attr,description,text,ext_description) values(49,'IgnorePassbackOnNextCard','IgnorePB','Ignore passback on next card');
-- attr = 50
if exists (select null from attr where attr = 50)
	update attr set description='TariffControl',text='TarifCtl',ext_description='Toggle tariff control on/off' where attr = 50;
else
	insert into attr (attr,description,text,ext_description) values(50,'TariffControl','TarifCtl','Toggle tariff control on/off');
-- attr = 51
if exists (select null from attr where attr = 51)
	update attr set description='NextZone',text='NextZone',ext_description='Next zone' where attr = 51;
else
	insert into attr (attr,description,text,ext_description) values(51,'NextZone','NextZone','Next zone');
-- attr = 52
if exists (select null from attr where attr = 52)
	update attr set description='PreviousZone',text='PrevZone',ext_description='Previous zone' where attr = 52;
else
	insert into attr (attr,description,text,ext_description) values(52,'PreviousZone','PrevZone','Previous zone');
-- attr = 53
if exists (select null from attr where attr = 53)
	update attr set description='FaresetToggle',text='FSToggle',ext_description='Toggle fareset' where attr = 53;
else
	insert into attr (attr,description,text,ext_description) values(53,'FaresetToggle','FSToggle','Toggle fareset');
-- attr = 54
if exists (select null from attr where attr = 54)
	update attr set description='GroupAuthorizationKey',text='GroupKey',ext_description='Group boarding authorization key' where attr = 54;
else
	insert into attr (attr,description,text,ext_description) values(54,'GroupAuthorizationKey','GroupKey','Group boarding authorization key');
-- attr = 55
if exists (select null from attr where attr = 55)
	update attr set description='TariffControlOn/Off',text='TarifOff',ext_description='Toggle tariff control on/off permanently' where attr = 55;
else
	insert into attr (attr,description,text,ext_description) values(55,'TariffControlOn/Off','TarifOff','Toggle tariff control on/off permanently');
-- attr = 56
if exists (select null from attr where attr = 56)
	update attr set description='IssueProofofPay',text='PayProof',ext_description='Issue a proof of payment document which carries designator 250 so that it cannot be accepted back into the farebox' where attr = 56;
else
	insert into attr (attr,description,text,ext_description) values(56,'IssueProofofPay','PayProof','Issue a proof of payment document which carries designator 250 so that it cannot be accepted back into the farebox');
-- attr = 57
if exists (select null from attr where attr = 57)
	update attr set description='PassCounter',text='PassCntr',ext_description='Count valid passes' where attr = 57;
else
	insert into attr (attr,description,text,ext_description) values(57,'PassCounter','PassCntr','Count valid passes');
-- attr = 58
if exists (select null from attr where attr = 58)
	update attr set description='MemoryStatus',text='MemStat',ext_description='Memory status' where attr = 58;
else
	insert into attr (attr,description,text,ext_description) values(58,'MemoryStatus','MemStat','Memory status');
-- attr = 59
if exists (select null from attr where attr = 59)
	update attr set description='TallyWithFare',text='T & Fare',ext_description='Modified tally that needed money' where attr = 59;
else
	insert into attr (attr,description,text,ext_description) values(59,'TallyWithFare','T & Fare','Modified tally that needed money');
-- attr = 60
if exists (select null from attr where attr = 60)
	update attr set description='ActionKey',text='ActionK',ext_description='Action key pressed' where attr = 60;
else
	insert into attr (attr,description,text,ext_description) values(60,'ActionKey','ActionK','Action key pressed');
-- attr = 61
if exists (select null from attr where attr = 61)
	update attr set description='TempTRiMBypass',text='TmpTRMBP',ext_description='Place TRiM in temporary bypass to issue current doc' where attr = 61;
else
	insert into attr (attr,description,text,ext_description) values(61,'TempTRiMBypass','TmpTRMBP','Place TRiM in temporary bypass to issue current doc');
-- attr = 62
if exists (select null from attr where attr = 62)
	update attr set description='LastCardBad',text='LastCBad',ext_description='Driver reported bad card' where attr = 62;
else
	insert into attr (attr,description,text,ext_description) values(62,'LastCardBad','LastCBad','Driver reported bad card');
-- attr = 63
if exists (select null from attr where attr = 63)
	update attr set description='ChangeUITheme',text='UITheme',ext_description='SCRV - change display UI theme (light/dark)' where attr = 63;
else
	insert into attr (attr,description,text,ext_description) values(63,'ChangeUITheme','UITheme','SCRV - change display UI theme (light/dark)');
-- attr = 64
if exists (select null from attr where attr = 64)
	update attr set description='CheckID',text='CheckID',ext_description='Check ID to verify user profile' where attr = 64;
else
	insert into attr (attr,description,text,ext_description) values(64,'CheckID','CheckID','Check ID to verify user profile');
-- attr = 65
if exists (select null from attr where attr = 65)
	update attr set description='Display the last transaction',text='DispLast',ext_description='Display the last transaction' where attr = 65;
else
	insert into attr (attr,description,text,ext_description) values(65,'Display the last transaction','DispLast','Display the last transaction');
-- attr = 66
if exists (select null from attr where attr = 66)
	update attr set description='TrmCrd_wPendFare',text='TrmCrd_w',ext_description='Release card from escrow requiring upcharge' where attr = 66;
else
	insert into attr (attr,description,text,ext_description) values(66,'TrmCrd_wPendFare','TrmCrd_w','Release card from escrow requiring upcharge');
-- attr = 67
if exists (select null from attr where attr = 67)
	update attr set description='Attribute 67',text='Attr 67',ext_description='Attribute 67' where attr = 67;
else
	insert into attr (attr,description,text,ext_description) values(67,'Attribute 67','Attr 67','Attribute 67');
-- attr = 68
if exists (select null from attr where attr = 68)
	update attr set description='ProofPayment',text='PrfPymnt',ext_description='Time Based Proof of Payment' where attr = 68;
else
	insert into attr (attr,description,text,ext_description) values(68,'ProofPayment','PrfPymnt','Time Based Proof of Payment');
-- attr = 69
if exists (select null from attr where attr = 69)
	update attr set description='Attribute 69',text='Attr 69',ext_description='Attribute 69' where attr = 69;
else
	insert into attr (attr,description,text,ext_description) values(69,'Attribute 69','Attr 69','Attribute 69');
-- attr = 70
if exists (select null from attr where attr = 70)
	update attr set description='Attribute 70',text='Attr 70',ext_description='Attribute 70' where attr = 70;
else
	insert into attr (attr,description,text,ext_description) values(70,'Attribute 70','Attr 70','Attribute 70');
-- attr = 71
if exists (select null from attr where attr = 71)
	update attr set description='Attribute 71',text='Attr 71',ext_description='Attribute 71' where attr = 71;
else
	insert into attr (attr,description,text,ext_description) values(71,'Attribute 71','Attr 71','Attribute 71');
-- attr = 72
if exists (select null from attr where attr = 72)
	update attr set description='Attribute 72',text='Attr 72',ext_description='Attribute 72' where attr = 72;
else
	insert into attr (attr,description,text,ext_description) values(72,'Attribute 72','Attr 72','Attribute 72');
-- attr = 73
if exists (select null from attr where attr = 73)
	update attr set description='Attribute 73',text='Attr 73',ext_description='Attribute 73' where attr = 73;
else
	insert into attr (attr,description,text,ext_description) values(73,'Attribute 73','Attr 73','Attribute 73');
-- attr = 74
if exists (select null from attr where attr = 74)
	update attr set description='Attribute 74',text='Attr 74',ext_description='Attribute 74' where attr = 74;
else
	insert into attr (attr,description,text,ext_description) values(74,'Attribute 74','Attr 74','Attribute 74');
-- attr = 75
if exists (select null from attr where attr = 75)
	update attr set description='Attribute 75',text='Attr 75',ext_description='Attribute 75' where attr = 75;
else
	insert into attr (attr,description,text,ext_description) values(75,'Attribute 75','Attr 75','Attribute 75');
-- attr = 76
if exists (select null from attr where attr = 76)
	update attr set description='Attribute 76',text='Attr 76',ext_description='Attribute 76' where attr = 76;
else
	insert into attr (attr,description,text,ext_description) values(76,'Attribute 76','Attr 76','Attribute 76');
-- attr = 77
if exists (select null from attr where attr = 77)
	update attr set description='Attribute 77',text='Attr 77',ext_description='Attribute 77' where attr = 77;
else
	insert into attr (attr,description,text,ext_description) values(77,'Attribute 77','Attr 77','Attribute 77');
-- attr = 78
if exists (select null from attr where attr = 78)
	update attr set description='Attribute 78',text='Attr 78',ext_description='Attribute 78' where attr = 78;
else
	insert into attr (attr,description,text,ext_description) values(78,'Attribute 78','Attr 78','Attribute 78');
-- attr = 79
if exists (select null from attr where attr = 79)
	update attr set description='Attribute 79',text='Attr 79',ext_description='Attribute 79' where attr = 79;
else
	insert into attr (attr,description,text,ext_description) values(79,'Attribute 79','Attr 79','Attribute 79');
-- attr = 80
if exists (select null from attr where attr = 80)
	update attr set description='Attribute 80',text='Attr 80',ext_description='Attribute 80' where attr = 80;
else
	insert into attr (attr,description,text,ext_description) values(80,'Attribute 80','Attr 80','Attribute 80');
-- attr = 81
if exists (select null from attr where attr = 81)
	update attr set description='Attribute 81',text='Attr 81',ext_description='Attribute 81' where attr = 81;
else
	insert into attr (attr,description,text,ext_description) values(81,'Attribute 81','Attr 81','Attribute 81');
-- attr = 82
if exists (select null from attr where attr = 82)
	update attr set description='Attribute 82',text='Attr 82',ext_description='Attribute 82' where attr = 82;
else
	insert into attr (attr,description,text,ext_description) values(82,'Attribute 82','Attr 82','Attribute 82');
-- attr = 83
if exists (select null from attr where attr = 83)
	update attr set description='Attribute 83',text='Attr 83',ext_description='Attribute 83' where attr = 83;
else
	insert into attr (attr,description,text,ext_description) values(83,'Attribute 83','Attr 83','Attribute 83');
-- attr = 84
if exists (select null from attr where attr = 84)
	update attr set description='Attribute 84',text='Attr 84',ext_description='Attribute 84' where attr = 84;
else
	insert into attr (attr,description,text,ext_description) values(84,'Attribute 84','Attr 84','Attribute 84');
-- attr = 85
if exists (select null from attr where attr = 85)
	update attr set description='Attribute 85',text='Attr 85',ext_description='Attribute 85' where attr = 85;
else
	insert into attr (attr,description,text,ext_description) values(85,'Attribute 85','Attr 85','Attribute 85');
-- attr = 86
if exists (select null from attr where attr = 86)
	update attr set description='Attribute 86',text='Attr 86',ext_description='Attribute 86' where attr = 86;
else
	insert into attr (attr,description,text,ext_description) values(86,'Attribute 86','Attr 86','Attribute 86');
-- attr = 87
if exists (select null from attr where attr = 87)
	update attr set description='Attribute 87',text='Attr 87',ext_description='Attribute 87' where attr = 87;
else
	insert into attr (attr,description,text,ext_description) values(87,'Attribute 87','Attr 87','Attribute 87');
-- attr = 88
if exists (select null from attr where attr = 88)
	update attr set description='Attribute 88',text='Attr 88',ext_description='Attribute 88' where attr = 88;
else
	insert into attr (attr,description,text,ext_description) values(88,'Attribute 88','Attr 88','Attribute 88');
-- attr = 89
if exists (select null from attr where attr = 89)
	update attr set description='Attribute 89',text='Attr 89',ext_description='Attribute 89' where attr = 89;
else
	insert into attr (attr,description,text,ext_description) values(89,'Attribute 89','Attr 89','Attribute 89');
-- attr = 90
if exists (select null from attr where attr = 90)
	update attr set description='Attribute 90',text='Attr 90',ext_description='Attribute 90' where attr = 90;
else
	insert into attr (attr,description,text,ext_description) values(90,'Attribute 90','Attr 90','Attribute 90');
-- attr = 91
if exists (select null from attr where attr = 91)
	update attr set description='Attribute 91',text='Attr 91',ext_description='Attribute 91' where attr = 91;
else
	insert into attr (attr,description,text,ext_description) values(91,'Attribute 91','Attr 91','Attribute 91');
-- attr = 92
if exists (select null from attr where attr = 92)
	update attr set description='Attribute 92',text='Attr 92',ext_description='Attribute 92' where attr = 92;
else
	insert into attr (attr,description,text,ext_description) values(92,'Attribute 92','Attr 92','Attribute 92');
-- attr = 93
if exists (select null from attr where attr = 93)
	update attr set description='Attribute 93',text='Attr 93',ext_description='Attribute 93' where attr = 93;
else
	insert into attr (attr,description,text,ext_description) values(93,'Attribute 93','Attr 93','Attribute 93');
-- attr = 94
if exists (select null from attr where attr = 94)
	update attr set description='Attribute 94',text='Attr 94',ext_description='Attribute 94' where attr = 94;
else
	insert into attr (attr,description,text,ext_description) values(94,'Attribute 94','Attr 94','Attribute 94');
-- attr = 95
if exists (select null from attr where attr = 95)
	update attr set description='Attribute 95',text='Attr 95',ext_description='Attribute 95' where attr = 95;
else
	insert into attr (attr,description,text,ext_description) values(95,'Attribute 95','Attr 95','Attribute 95');
-- attr = 255
if exists (select null from attr where attr = 255)
	update attr set description='FareDisabled',text='FareDis',ext_description='Fare is disabled' where attr = 255;
else
	insert into attr (attr,description,text,ext_description) values(255,'FareDisabled','FareDis','Fare is disabled');
go

------------------------------------------------------------------------
-- populate et table
------------------------------------------------------------------------
-- type 0
if exists (select null from et where type = 0)
	update et set text = 'Idle event', ridership = 'N' where type = 0;
else
	insert into et values(0, 'Idle event', 'N');
-- type 1
if exists (select null from et where type = 1)
	update et set text = 'Cashbox out', ridership = 'N' where type = 1;
else
	insert into et values(1, 'Cashbox out', 'N');
-- type 2
if exists (select null from et where type = 2)
	update et set text = 'Cashbox in', ridership = 'N' where type = 2;
else
	insert into et values(2, 'Cashbox in', 'N');
-- type 3
if exists (select null from et where type = 3)
	update et set text = 'Logon', ridership = 'N' where type = 3;
else
	insert into et values(3, 'Logon', 'N');
-- type 4
if exists (select null from et where type = 4)
	update et set text = 'Logoff', ridership = 'N' where type = 4;
else
	insert into et values(4, 'Logoff', 'N');
-- type 5
if exists (select null from et where type = 5)
	update et set text = 'Midnight', ridership = 'N' where type = 5;
else
	insert into et values(5, 'Midnight', 'N');
-- type 6
if exists (select null from et where type = 6)
	update et set text = 'Event overflow', ridership = 'N' where type = 6;
else
	insert into et values(6, 'Event overflow', 'N');
-- type 7
if exists (select null from et where type = 7)
	update et set text = 'Bypass on', ridership = 'N' where type = 7;
else
	insert into et values(7, 'Bypass on', 'N');
-- type 8
if exists (select null from et where type = 8)
	update et set text = 'Bypass off', ridership = 'N' where type = 8;
else
	insert into et values(8, 'Bypass off', 'N');
-- type 9
if exists (select null from et where type = 9)
	update et set text = 'Door opened unauthorized', ridership = 'N' where type = 9;
else
	insert into et values(9, 'Door opened unauthorized', 'N');
-- type 10
if exists (select null from et where type = 10)
	update et set text = 'Door closed unauthorized', ridership = 'N' where type = 10;
else
	insert into et values(10, 'Door closed unauthorized', 'N');
-- type 11
if exists (select null from et where type = 11)
	update et set text = 'Cashbox out unauthorized', ridership = 'N' where type = 11;
else
	insert into et values(11, 'Cashbox out unauthorized', 'N');
-- type 12
if exists (select null from et where type = 12)
	update et set text = 'Cashbox door duration', ridership = 'N' where type = 12;
else
	insert into et values(12, 'Cashbox door duration', 'N');
-- type 13
if exists (select null from et where type = 13)
	update et set text = 'Cold start', ridership = 'N' where type = 13;
else
	insert into et values(13, 'Cold start', 'N');
-- type 14
if exists (select null from et where type = 14)
	update et set text = 'Hardware clock failure', ridership = 'N' where type = 14;
else
	insert into et values(14, 'Hardware clock failure', 'N');
-- type 15
if exists (select null from et where type = 15)
	update et set text = 'Portable probe used', ridership = 'N' where type = 15;
else
	insert into et values(15, 'Portable probe used', 'N');
-- type 16
if exists (select null from et where type = 16)
	update et set text = 'Time discrepancy', ridership = 'N' where type = 16;
else
	insert into et values(16, 'Time discrepancy', 'N');
-- type 17
if exists (select null from et where type = 17)
	update et set text = 'Power restored', ridership = 'N' where type = 17;
else
	insert into et values(17, 'Power restored', 'N');
-- type 18
if exists (select null from et where type = 18)
	update et set text = 'Probed, cashbox not removed', ridership = 'N' where type = 18;
else
	insert into et values(18, 'Probed, cashbox not removed', 'N');
-- type 19
if exists (select null from et where type = 19)
	update et set text = 'Probed, no rev, CBX not removed', ridership = 'N' where type = 19;
else
	insert into et values(19, 'Probed, no rev, CBX not removed', 'N');
-- type 20
if exists (select null from et where type = 20)
	update et set text = 'TRiM offline', ridership = 'N' where type = 20;
else
	insert into et values(20, 'TRiM offline', 'N');
-- type 21
if exists (select null from et where type = 21)
	update et set text = 'Cashbox after memory clear', ridership = 'N' where type = 21;
else
	insert into et values(21, 'Cashbox after memory clear', 'N');
-- type 22
if exists (select null from et where type = 22)
	update et set text = 'No cashbox after memory clear', ridership = 'N' where type = 22;
else
	insert into et values(22, 'No cashbox after memory clear', 'N');
-- type 23
if exists (select null from et where type = 23)
	update et set text = 'Cashbox full', ridership = 'N' where type = 23;
else
	insert into et values(23, 'Cashbox full', 'N');
-- type 24
if exists (select null from et where type = 24)
	update et set text = 'TRiM online', ridership = 'N' where type = 24;
else
	insert into et values(24, 'TRiM online', 'N');
-- type 25
if exists (select null from et where type = 25)
	update et set text = 'Maintenance number entered', ridership = 'N' where type = 25;
else
	insert into et values(25, 'Maintenance number entered', 'N');
-- type 26
if exists (select null from et where type = 26)
	update et set text = 'Fareset change', ridership = 'N' where type = 26;
else
	insert into et values(26, 'Fareset change', 'N');
-- type 27
if exists (select null from et where type = 27)
	update et set text = 'Bill mechanism error', ridership = 'N' where type = 27;
else
	insert into et values(27, 'Bill mechanism error', 'N');
-- type 28
if exists (select null from et where type = 28)
	update et set text = 'Coin mechanism error', ridership = 'N' where type = 28;
else
	insert into et values(28, 'Coin mechanism error', 'N');
-- type 29
if exists (select null from et where type = 29)
	update et set text = 'Partial memory clear', ridership = 'N' where type = 29;
else
	insert into et values(29, 'Partial memory clear', 'N');
-- type 30
if exists (select null from et where type = 30)
	update et set text = 'Door closed', ridership = 'N' where type = 30;
else
	insert into et values(30, 'Door closed', 'N');
-- type 31
if exists (select null from et where type = 31)
	update et set text = 'Probed completed', ridership = 'N' where type = 31;
else
	insert into et values(31, 'Probed completed', 'N');
-- type 32
if exists (select null from et where type = 32)
	update et set text = 'Bad list pass used', ridership = 'N' where type = 32;
else
	insert into et values(32, 'Bad list pass used', 'N');
-- type 33
if exists (select null from et where type = 33)
	update et set text = 'Probe, door not opened', ridership = 'N' where type = 33;
else
	insert into et values(33, 'Probe, door not opened', 'N');
-- type 34
if exists (select null from et where type = 34)
	update et set text = 'Automatic memory clear', ridership = 'N' where type = 34;
else
	insert into et values(34, 'Automatic memory clear', 'N');
-- type 35
if exists (select null from et where type = 35)
	update et set text = 'Bypass after probe', ridership = 'N' where type = 35;
else
	insert into et values(35, 'Bypass after probe', 'N');
-- type 36
if exists (select null from et where type = 36)
	update et set text = 'Auto logoff', ridership = 'N' where type = 36;
else
	insert into et values(36, 'Auto logoff', 'N');
-- type 37
if exists (select null from et where type = 37)
	update et set text = 'Hardware clock error', ridership = 'N' where type = 37;
else
	insert into et values(37, 'Hardware clock error', 'N');
-- type 38
if exists (select null from et where type = 38)
	update et set text = 'Maintenance pass entered', ridership = 'N' where type = 38;
else
	insert into et values(38, 'Maintenance pass entered', 'N');
-- type 39
if exists (select null from et where type = 39)
	update et set text = 'Maint pass partial memory clear', ridership = 'N' where type = 39;
else
	insert into et values(39, 'Maint pass partial memory clear', 'N');
-- type 40
if exists (select null from et where type = 40)
	update et set text = 'Card logon', ridership = 'N' where type = 40;
else
	insert into et values(40, 'Card logon', 'N');
-- type 41
if exists (select null from et where type = 41)
	update et set text = 'Maintenance pass memory clear', ridership = 'N' where type = 41;
else
	insert into et values(41, 'Maintenance pass memory clear', 'N');
-- type 42
if exists (select null from et where type = 42)
	update et set text = 'Keypad memory clear', ridership = 'N' where type = 42;
else
	insert into et values(42, 'Keypad memory clear', 'N');
-- type 43
if exists (select null from et where type = 43)
	update et set text = 'Cashbox ID lost normal', ridership = 'N' where type = 43;
else
	insert into et values(43, 'Cashbox ID lost normal', 'N');
-- type 44
if exists (select null from et where type = 44)
	update et set text = 'Cashbox ID lost on power-up', ridership = 'N' where type = 44;
else
	insert into et values(44, 'Cashbox ID lost on power-up', 'N');
-- type 45
if exists (select null from et where type = 45)
	update et set text = 'Puller pass entered', ridership = 'N' where type = 45;
else
	insert into et values(45, 'Puller pass entered', 'N');
-- type 46
if exists (select null from et where type = 46)
	update et set text = 'TRiM stock low', ridership = 'N' where type = 46;
else
	insert into et values(46, 'TRiM stock low', 'N');
-- type 47
if exists (select null from et where type = 47)
	update et set text = 'TRiM bypass on', ridership = 'N' where type = 47;
else
	insert into et values(47, 'TRiM bypass on', 'N');
-- type 48
if exists (select null from et where type = 48)
	update et set text = 'TRiM bypass off', ridership = 'N' where type = 48;
else
	insert into et values(48, 'TRiM bypass off', 'N');
-- type 49
if exists (select null from et where type = 49)
	update et set text = 'Manual event', ridership = 'N' where type = 49;
else
	insert into et values(49, 'Manual event', 'N');
-- type 50
if exists (select null from et where type = 50)
	update et set text = 'Hourly event', ridership = 'N' where type = 50;
else
	insert into et values(50, 'Hourly event', 'N');
-- type 51
if exists (select null from et where type = 51)
	update et set text = 'Menu entry event', ridership = 'N' where type = 51;
else
	insert into et values(51, 'Menu entry event', 'N');
-- type 52
if exists (select null from et where type = 52)
	update et set text = 'Probe event', ridership = 'N' where type = 52;
else
	insert into et values(52, 'Probe event', 'N');
-- type 53
if exists (select null from et where type = 53)
	update et set text = 'TRiM stock added', ridership = 'N' where type = 53;
else
	insert into et values(53, 'TRiM stock added', 'N');
-- type 54
if exists (select null from et where type = 54)
	update et set text = 'Lid open', ridership = 'N' where type = 54;
else
	insert into et values(54, 'Lid open', 'N');
-- type 55
if exists (select null from et where type = 55)
	update et set text = 'Lid closed', ridership = 'N' where type = 55;
else
	insert into et values(55, 'Lid closed', 'N');
-- type 56
if exists (select null from et where type = 56)
	update et set text = 'Periodic event', ridership = 'N' where type = 56;
else
	insert into et values(56, 'Periodic event', 'N');
-- type 57
if exists (select null from et where type = 57)
	update et set text = 'Door open after memory clear', ridership = 'N' where type = 57;
else
	insert into et values(57, 'Door open after memory clear', 'N');
-- type 58
if exists (select null from et where type = 58)
	update et set text = 'Lid open after memory clear', ridership = 'N' where type = 58;
else
	insert into et values(58, 'Lid open after memory clear', 'N');
-- type 59
if exists (select null from et where type = 59)
	update et set text = 'Bypass on after memory clear', ridership = 'N' where type = 59;
else
	insert into et values(59, 'Bypass on after memory clear', 'N');
-- type 60
if exists (select null from et where type = 60)
	update et set text = 'Future fare structure', ridership = 'N' where type = 60;
else
	insert into et values(60, 'Future fare structure', 'N');
-- type 61
if exists (select null from et where type = 61)
	update et set text = 'Bad driver', ridership = 'N' where type = 61;
else
	insert into et values(61, 'Bad driver', 'N');
-- type 62
if exists (select null from et where type = 62)
	update et set text = 'Bad route', ridership = 'N' where type = 62;
else
	insert into et values(62, 'Bad route', 'N');
-- type 63
if exists (select null from et where type = 63)
	update et set text = 'TRiM card lost', ridership = 'N' where type = 63;
else
	insert into et values(63, 'TRiM card lost', 'N');
-- type 64
if exists (select null from et where type = 64)
	update et set text = 'Barcode reader offline', ridership = 'N' where type = 64;
else
	insert into et values(64, 'Barcode reader offline', 'N');
-- type 65
if exists (select null from et where type = 65)
	update et set text = 'Barcode reader online', ridership = 'N' where type = 65;
else
	insert into et values(65, 'Barcode reader online', 'N');
-- type 66
if exists (select null from et where type = 66)
	update et set text = 'Barcode printer offline', ridership = 'N' where type = 66;
else
	insert into et values(66, 'Barcode printer offline', 'N');
-- type 67
if exists (select null from et where type = 67)
	update et set text = 'Barcode printer online', ridership = 'N' where type = 67;
else
	insert into et values(67, 'Barcode printer online', 'N');
-- type 68
if exists (select null from et where type = 68)
	update et set text = 'TRiM no stock', ridership = 'N' where type = 68;
else
	insert into et values(68, 'TRiM no stock', 'N');
-- type 69
if exists (select null from et where type = 69)
	update et set text = 'Receive future FS via wi-fi probing', ridership = 'N' where type = 69;
else
	insert into et values(69, 'Receive future FS via wi-fi probing', 'N');
-- type 70
if exists (select null from et where type = 70)
	update et set text = 'Event 70', ridership = 'N' where type = 70;
else
	insert into et values(70, 'Event 70', 'N');
-- type 71
if exists (select null from et where type = 71)
	update et set text = 'Event 71', ridership = 'N' where type = 71;
else
	insert into et values(71, 'Event 71', 'N');
-- type 72
if exists (select null from et where type = 72)
	update et set text = 'Event 72', ridership = 'N' where type = 72;
else
	insert into et values(72, 'Event 72', 'N');
-- type 73
if exists (select null from et where type = 73)
	update et set text = 'Event 73', ridership = 'N' where type = 73;
else
	insert into et values(73, 'Event 73', 'N');
-- type 74
if exists (select null from et where type = 74)
	update et set text = 'Event 74', ridership = 'N' where type = 74;
else
	insert into et values(74, 'Event 74', 'N');
-- type 75
if exists (select null from et where type = 75)
	update et set text = 'Event 75', ridership = 'N' where type = 75;
else
	insert into et values(75, 'Event 75', 'N');
-- type 76
if exists (select null from et where type = 76)
	update et set text = 'Event 76', ridership = 'N' where type = 76;
else
	insert into et values(76, 'Event 76', 'N');
-- type 77
if exists (select null from et where type = 77)
	update et set text = 'Event 77', ridership = 'N' where type = 77;
else
	insert into et values(77, 'Event 77', 'N');
-- type 78
if exists (select null from et where type = 78)
	update et set text = 'Event 78', ridership = 'N' where type = 78;
else
	insert into et values(78, 'Event 78', 'N');
-- type 79
if exists (select null from et where type = 79)
	update et set text = 'Event 79', ridership = 'N' where type = 79;
else
	insert into et values(79, 'Event 79', 'N');
-- type 80
if exists (select null from et where type = 80)
	update et set text = 'Event 80', ridership = 'N' where type = 80;
else
	insert into et values(80, 'Event 80', 'N');
-- type 81
if exists (select null from et where type = 81)
	update et set text = 'Event 81', ridership = 'N' where type = 81;
else
	insert into et values(81, 'Event 81', 'N');
-- type 82
if exists (select null from et where type = 82)
	update et set text = 'Event 82', ridership = 'N' where type = 82;
else
	insert into et values(82, 'Event 82', 'N');
-- type 83
if exists (select null from et where type = 83)
	update et set text = 'Event 83', ridership = 'N' where type = 83;
else
	insert into et values(83, 'Event 83', 'N');
-- type 84
if exists (select null from et where type = 84)
	update et set text = 'Event 84', ridership = 'N' where type = 84;
else
	insert into et values(84, 'Event 84', 'N');
-- type 85
if exists (select null from et where type = 85)
	update et set text = 'Event 85', ridership = 'N' where type = 85;
else
	insert into et values(85, 'Event 85', 'N');
-- type 86
if exists (select null from et where type = 86)
	update et set text = 'Event 86', ridership = 'N' where type = 86;
else
	insert into et values(86, 'Event 86', 'N');
-- type 87
if exists (select null from et where type = 87)
	update et set text = 'Event 87', ridership = 'N' where type = 87;
else
	insert into et values(87, 'Event 87', 'N');
-- type 88
if exists (select null from et where type = 88)
	update et set text = 'Event 88', ridership = 'N' where type = 88;
else
	insert into et values(88, 'Event 88', 'N');
-- type 89
if exists (select null from et where type = 89)
	update et set text = 'Event 89', ridership = 'N' where type = 89;
else
	insert into et values(89, 'Event 89', 'N');
-- type 90
if exists (select null from et where type = 90)
	update et set text = 'Event 90', ridership = 'N' where type = 90;
else
	insert into et values(90, 'Event 90', 'N');
-- type 91
if exists (select null from et where type = 91)
	update et set text = 'Event 91', ridership = 'N' where type = 91;
else
	insert into et values(91, 'Event 91', 'N');
-- type 92
if exists (select null from et where type = 92)
	update et set text = 'Event 92', ridership = 'N' where type = 92;
else
	insert into et values(92, 'Event 92', 'N');
-- type 93
if exists (select null from et where type = 93)
	update et set text = 'Event 93', ridership = 'N' where type = 93;
else
	insert into et values(93, 'Event 93', 'N');
-- type 94
if exists (select null from et where type = 94)
	update et set text = 'Event 94', ridership = 'N' where type = 94;
else
	insert into et values(94, 'Event 94', 'N');
-- type 95
if exists (select null from et where type = 95)
	update et set text = 'Event 95', ridership = 'N' where type = 95;
else
	insert into et values(95, 'Event 95', 'N');
-- type 96
if exists (select null from et where type = 96)
	update et set text = 'Event 96', ridership = 'N' where type = 96;
else
	insert into et values(96, 'Event 96', 'N');
-- type 97
if exists (select null from et where type = 97)
	update et set text = 'Event 97', ridership = 'N' where type = 97;
else
	insert into et values(97, 'Event 97', 'N');
-- type 98
if exists (select null from et where type = 98)
	update et set text = 'Event 98', ridership = 'N' where type = 98;
else
	insert into et values(98, 'Event 98', 'N');
-- type 99
if exists (select null from et where type = 99)
	update et set text = 'Event 99', ridership = 'N' where type = 99;
else
	insert into et values(99, 'Event 99', 'N');
-- type 100
if exists (select null from et where type = 100)
	update et set text = 'Event 100', ridership = 'N' where type = 100;
else
	insert into et values(100, 'Event 100', 'N');
-- type 101
if exists (select null from et where type = 101)
	update et set text = 'Power-up', ridership = 'N' where type = 101;
else
	insert into et values(101, 'Power-up', 'N');
-- type 102
if exists (select null from et where type = 102)
	update et set text = 'Midnight', ridership = 'N' where type = 102;
else
	insert into et values(102, 'Midnight', 'N');
-- type 103
if exists (select null from et where type = 103)
	update et set text = 'Hourly timestamp', ridership = 'N' where type = 103;
else
	insert into et values(103, 'Hourly timestamp', 'N');
-- type 104
if exists (select null from et where type = 104)
	update et set text = 'Driver change', ridership = 'N' where type = 104;
else
	insert into et values(104, 'Driver change', 'N');
-- type 105
if exists (select null from et where type = 105)
	update et set text = 'Fareset change', ridership = 'N' where type = 105;
else
	insert into et values(105, 'Fareset change', 'N');
-- type 106
if exists (select null from et where type = 106)
	update et set text = 'Route change', ridership = 'N' where type = 106;
else
	insert into et values(106, 'Route change', 'N');
-- type 107
if exists (select null from et where type = 107)
	update et set text = 'Run change', ridership = 'N' where type = 107;
else
	insert into et values(107, 'Run change', 'N');
-- type 108
if exists (select null from et where type = 108)
	update et set text = 'Trip change', ridership = 'N' where type = 108;
else
	insert into et values(108, 'Trip change', 'N');
-- type 109
if exists (select null from et where type = 109)
	update et set text = 'Direction change', ridership = 'N' where type = 109;
else
	insert into et values(109, 'Direction change', 'N');
-- type 110
if exists (select null from et where type = 110)
	update et set text = 'Bus stop change', ridership = 'N' where type = 110;
else
	insert into et values(110, 'Bus stop change', 'N');
-- type 111
if exists (select null from et where type = 111)
	update et set text = 'Cut time change', ridership = 'N' where type = 111;
else
	insert into et values(111, 'Cut time change', 'N');
-- type 112
if exists (select null from et where type = 112)
	update et set text = 'Ready for revenue', ridership = 'N' where type = 112;
else
	insert into et values(112, 'Ready for revenue', 'N');
-- type 113
if exists (select null from et where type = 113)
	update et set text = 'Maintenance', ridership = 'N' where type = 113;
else
	insert into et values(113, 'Maintenance', 'N');
-- type 114
if exists (select null from et where type = 114)
	update et set text = 'Stored ride card', ridership = 'Y' where type = 114;
else
	insert into et values(114, 'Stored ride card', 'Y');
-- type 115
if exists (select null from et where type = 115)
	update et set text = 'Period pass', ridership = 'Y' where type = 115;
else
	insert into et values(115, 'Period pass', 'Y');
-- type 116
if exists (select null from et where type = 116)
	update et set text = 'Stored value card', ridership = 'Y' where type = 116;
else
	insert into et values(116, 'Stored value card', 'Y');
-- type 117
if exists (select null from et where type = 117)
	update et set text = 'Credit card', ridership = 'Y' where type = 117;
else
	insert into et values(117, 'Credit card', 'Y');
-- type 118
if exists (select null from et where type = 118)
	update et set text = 'Transfer received', ridership = 'Y' where type = 118;
else
	insert into et values(118, 'Transfer received', 'Y');
-- type 119
if exists (select null from et where type = 119)
	update et set text = 'Got fare', ridership = 'Y' where type = 119;
else
	insert into et values(119, 'Got fare', 'Y');
-- type 120
if exists (select null from et where type = 120)
	update et set text = 'Bypass dump', ridership = 'N' where type = 120;
else
	insert into et values(120, 'Bypass dump', 'N');
-- type 121
if exists (select null from et where type = 121)
	update et set text = 'Escrow dump', ridership = 'N' where type = 121;
else
	insert into et values(121, 'Escrow dump', 'N');
-- type 122
if exists (select null from et where type = 122)
	update et set text = 'Escrow timeout', ridership = 'N' where type = 122;
else
	insert into et values(122, 'Escrow timeout', 'N');
-- type 123
if exists (select null from et where type = 123)
	update et set text = 'General purpose', ridership = 'N' where type = 123;
else
	insert into et values(123, 'General purpose', 'N');
-- type 124
if exists (select null from et where type = 124)
	update et set text = 'Badlisted card', ridership = 'N' where type = 124;
else
	insert into et values(124, 'Badlisted card', 'N');
-- type 125
if exists (select null from et where type = 125)
	update et set text = 'Login', ridership = 'N' where type = 125;
else
	insert into et values(125, 'Login', 'N');
-- type 126
if exists (select null from et where type = 126)
	update et set text = 'Restore card', ridership = 'N' where type = 126;
else
	insert into et values(126, 'Restore card', 'N');
-- type 127
if exists (select null from et where type = 127)
	update et set text = 'Read card', ridership = 'N' where type = 127;
else
	insert into et values(127, 'Read card', 'N');
-- type 128
if exists (select null from et where type = 128)
	update et set text = 'Transfer issued', ridership = 'N' where type = 128;
else
	insert into et values(128, 'Transfer issued', 'N');
-- type 129
if exists (select null from et where type = 129)
	update et set text = 'Special card', ridership = 'Y' where type = 129;
else
	insert into et values(129, 'Special card', 'Y');
-- type 130
if exists (select null from et where type = 130)
	update et set text = 'Cashbox code', ridership = 'N' where type = 130;
else
	insert into et values(130, 'Cashbox code', 'N');
-- type 131
if exists (select null from et where type = 131)
	update et set text = 'Electronic key code', ridership = 'N' where type = 131;
else
	insert into et values(131, 'Electronic key code', 'N');
-- type 132
if exists (select null from et where type = 132)
	update et set text = 'POST', ridership = 'N' where type = 132;
else
	insert into et values(132, 'POST', 'N');
-- type 133
if exists (select null from et where type = 133)
	update et set text = 'TTP', ridership = 'N' where type = 133;
else
	insert into et values(133, 'TTP', 'N');
-- type 134
if exists (select null from et where type = 134)
	update et set text = 'Revenue status', ridership = 'N' where type = 134;
else
	insert into et values(134, 'Revenue status', 'N');
-- type 135
if exists (select null from et where type = 135)
	update et set text = 'Recharge', ridership = 'N' where type = 135;
else
	insert into et values(135, 'Recharge', 'N');
-- type 136
if exists (select null from et where type = 136)
	update et set text = 'TRiM diagnostic', ridership = 'N' where type = 136;
else
	insert into et values(136, 'TRiM diagnostic', 'N');
-- type 137
if exists (select null from et where type = 137)
	update et set text = 'TRiM bad verify', ridership = 'N' where type = 137;
else
	insert into et values(137, 'TRiM bad verify', 'N');
-- type 138
if exists (select null from et where type = 138)
	update et set text = 'GPS latitude/longitude', ridership = 'N' where type = 138;
else
	insert into et values(138, 'GPS latitude/longitude', 'N');
-- type 139
if exists (select null from et where type = 139)
	update et set text = 'MetroCard', ridership = 'Y' where type = 139;
else
	insert into et values(139, 'MetroCard', 'Y');
-- type 140
if exists (select null from et where type = 140)
	update et set text = 'Coin reject threshold', ridership = 'N' where type = 140;
else
	insert into et values(140, 'Coin reject threshold', 'N');
-- type 141
if exists (select null from et where type = 141)
	update et set text = 'TRiM misfeed', ridership = 'N' where type = 141;
else
	insert into et values(141, 'TRiM misfeed', 'N');
-- type 142
if exists (select null from et where type = 142)
	update et set text = 'Issue card', ridership = 'N' where type = 142;
else
	insert into et values(142, 'Issue card', 'N');
-- type 143
if exists (select null from et where type = 143)
	update et set text = 'Token or ticket', ridership = 'Y' where type = 143;
else
	insert into et values(143, 'Token or ticket', 'Y');
-- type 144
if exists (select null from et where type = 144)
	update et set text = 'Bill validator OOS', ridership = 'N' where type = 144;
else
	insert into et values(144, 'Bill validator OOS', 'N');
-- type 145
if exists (select null from et where type = 145)
	update et set text = 'Multifare metrocard', ridership = 'Y' where type = 145;
else
	insert into et values(145, 'Multifare metrocard', 'Y');
-- type 146
if exists (select null from et where type = 146)
	update et set text = 'GPS diagnostics', ridership = 'N' where type = 146;
else
	insert into et values(146, 'GPS diagnostics', 'N');
-- type 147
if exists (select null from et where type = 147)
	update et set text = 'Component firmware version/SN', ridership = 'N' where type = 147;
else
	insert into et values(147, 'Component firmware version/SN', 'N');
-- type 148
if exists (select null from et where type = 148)
	update et set text = 'GPS driver status transaction', ridership = 'N' where type = 148;
else
	insert into et values(148, 'GPS driver status transaction', 'N');
-- type 149
if exists (select null from et where type = 149)
	update et set text = 'Lid status', ridership = 'N' where type = 149;
else
	insert into et values(149, 'Lid status', 'N');
-- type 150
if exists (select null from et where type = 150)
	update et set text = 'GPS time discrepancy', ridership = 'N' where type = 150;
else
	insert into et values(150, 'GPS time discrepancy', 'N');
-- type 151
if exists (select null from et where type = 151)
	update et set text = 'Driver login via swipe card', ridership = 'N' where type = 151;
else
	insert into et values(151, 'Driver login via swipe card', 'N');
-- type 152
if exists (select null from et where type = 152)
	update et set text = 'Driver login via smart card', ridership = 'N' where type = 152;
else
	insert into et values(152, 'Driver login via smart card', 'N');
-- type 153
if exists (select null from et where type = 153)
	update et set text = 'Driver login via card in TRiM', ridership = 'N' where type = 153;
else
	insert into et values(153, 'Driver login via card in TRiM', 'N');
-- type 154
if exists (select null from et where type = 154)
	update et set text = 'Driver login via GPS', ridership = 'N' where type = 154;
else
	insert into et values(154, 'Driver login via GPS', 'N');
-- type 155
if exists (select null from et where type = 155)
	update et set text = 'Maintenance pass (enable probe)', ridership = 'N' where type = 155;
else
	insert into et values(155, 'Maintenance pass (enable probe)', 'N');
-- type 156
if exists (select null from et where type = 156)
	update et set text = 'Smart card diagnostics', ridership = 'N' where type = 156;
else
	insert into et values(156, 'Smart card diagnostics', 'N');
-- type 157
if exists (select null from et where type = 157)
	update et set text = 'CTS diagnostics', ridership = 'N' where type = 157;
else
	insert into et values(157, 'CTS diagnostics', 'N');
-- type 158
if exists (select null from et where type = 158)
	update et set text = 'Bad driver via keypad', ridership = 'N' where type = 158;
else
	insert into et values(158, 'Bad driver via keypad', 'N');
-- type 159
if exists (select null from et where type = 159)
	update et set text = 'Bad driver not via keypad', ridership = 'N' where type = 159;
else
	insert into et values(159, 'Bad driver not via keypad', 'N');
-- type 160
if exists (select null from et where type = 160)
	update et set text = 'Bad route', ridership = 'N' where type = 160;
else
	insert into et values(160, 'Bad route', 'N');
-- type 161
if exists (select null from et where type = 161)
	update et set text = 'ACS parameter error', ridership = 'N' where type = 161;
else
	insert into et values(161, 'ACS parameter error', 'N');
-- type 162
if exists (select null from et where type = 162)
	update et set text = 'ACS card transaction', ridership = 'Y' where type = 162;
else
	insert into et values(162, 'ACS card transaction', 'Y');
-- type 163
if exists (select null from et where type = 163)
	update et set text = 'ACS CMJ sale', ridership = 'N' where type = 163;
else
	insert into et values(163, 'ACS CMJ sale', 'N');
-- type 164
if exists (select null from et where type = 164)
	update et set text = 'Probe without ACS parameter', ridership = 'N' where type = 164;
else
	insert into et values(164, 'Probe without ACS parameter', 'N');
-- type 165
if exists (select null from et where type = 165)
	update et set text = 'Bus number not initialized', ridership = 'N' where type = 165;
else
	insert into et values(165, 'Bus number not initialized', 'N');
-- type 166
if exists (select null from et where type = 166)
	update et set text = 'Smart card reader write error', ridership = 'N' where type = 166;
else
	insert into et values(166, 'Smart card reader write error', 'N');
-- type 167
if exists (select null from et where type = 167)
	update et set text = 'Smart card reader offline', ridership = 'N' where type = 167;
else
	insert into et values(167, 'Smart card reader offline', 'N');
-- type 168
if exists (select null from et where type = 168)
	update et set text = 'TRiM offline/online', ridership = 'N' where type = 168;
else
	insert into et values(168, 'TRiM offline/online', 'N');
-- type 169
if exists (select null from et where type = 169)
	update et set text = 'Component state - TRiM', ridership = 'N' where type = 169;
else
	insert into et values(169, 'Component state - TRiM', 'N');
-- type 170
if exists (select null from et where type = 170)
	update et set text = 'Component state - Smart Card Rdr', ridership = 'N' where type = 170;
else
	insert into et values(170, 'Component state - Smart Card Rdr', 'N');
-- type 171
if exists (select null from et where type = 171)
	update et set text = 'Component state - Cashbox', ridership = 'N' where type = 171;
else
	insert into et values(171, 'Component state - Cashbox', 'N');
-- type 172
if exists (select null from et where type = 172)
	update et set text = 'Transaction 172', ridership = 'N' where type = 172;
else
	insert into et values(172, 'Transaction 172', 'N');
-- type 173
if exists (select null from et where type = 173)
	update et set text = 'New parameter received by FBX', ridership = 'N' where type = 173;
else
	insert into et values(173, 'New parameter received by FBX', 'N');
-- type 174
if exists (select null from et where type = 174)
	update et set text = 'SAM inventory', ridership = 'N' where type = 174;
else
	insert into et values(174, 'SAM inventory', 'N');
-- type 175
if exists (select null from et where type = 175)
	update et set text = 'Transaction 175', ridership = 'N' where type = 175;
else
	insert into et values(175, 'Transaction 175', 'N');
-- type 176
if exists (select null from et where type = 176)
	update et set text = 'Farebox configuration mode', ridership = 'N' where type = 176;
else
	insert into et values(176, 'Farebox configuration mode', 'N');
-- type 177
if exists (select null from et where type = 177)
	update et set text = 'New bus number', ridership = 'N' where type = 177;
else
	insert into et values(177, 'New bus number', 'N');
-- type 178
if exists (select null from et where type = 178)
	update et set text = 'Component state - All', ridership = 'N' where type = 178;
else
	insert into et values(178, 'Component state - All', 'N');
-- type 179
if exists (select null from et where type = 179)
	update et set text = 'Auto logoff (no activity)', ridership = 'N' where type = 179;
else
	insert into et values(179, 'Auto logoff (no activity)', 'N');
-- type 180
if exists (select null from et where type = 180)
	update et set text = 'Bill transport diagnostics', ridership = 'N' where type = 180;
else
	insert into et values(180, 'Bill transport diagnostics', 'N');
-- type 181
if exists (select null from et where type = 181)
	update et set text = 'Zone change', ridership = 'N' where type = 181;
else
	insert into et values(181, 'Zone change', 'N');
-- type 182
if exists (select null from et where type = 182)
	update et set text = 'CTS DCU status', ridership = 'N' where type = 182;
else
	insert into et values(182, 'CTS DCU status', 'N');
-- type 183
if exists (select null from et where type = 183)
	update et set text = 'Gate status', ridership = 'N' where type = 183;
else
	insert into et values(183, 'Gate status', 'N');
-- type 184
if exists (select null from et where type = 184)
	update et set text = 'Wheelchair lift status', ridership = 'N' where type = 184;
else
	insert into et values(184, 'Wheelchair lift status', 'N');
-- type 185
if exists (select null from et where type = 185)
	update et set text = 'Bad run', ridership = 'N' where type = 185;
else
	insert into et values(185, 'Bad run', 'N');
-- type 186
if exists (select null from et where type = 186)
	update et set text = 'Bad trip', ridership = 'N' where type = 186;
else
	insert into et values(186, 'Bad trip', 'N');
-- type 187
if exists (select null from et where type = 187)
	update et set text = 'Trip-ID/schedule number change', ridership = 'N' where type = 187;
else
	insert into et values(187, 'Trip-ID/schedule number change', 'N');
-- type 188
if exists (select null from et where type = 188)
	update et set text = 'Memory status', ridership = 'N' where type = 188;
else
	insert into et values(188, 'Memory status', 'N');
-- type 189
if exists (select null from et where type = 189)
	update et set text = 'ePay autoload add value', ridership = 'N' where type = 189;
else
	insert into et values(189, 'ePay autoload add value', 'N');
-- type 190
if exists (select null from et where type = 190)
	update et set text = 'ePay autoload add product', ridership = 'N' where type = 190;
else
	insert into et values(190, 'ePay autoload add product', 'N');
-- type 191
if exists (select null from et where type = 191)
	update et set text = 'ePay autoload card property', ridership = 'N' where type = 191;
else
	insert into et values(191, 'ePay autoload card property', 'N');
-- type 192
if exists (select null from et where type = 192)
	update et set text = 'Smart card usage - ride', ridership = 'Y' where type = 192;
else
	insert into et values(192, 'Smart card usage - ride', 'Y');
-- type 193
if exists (select null from et where type = 193)
	update et set text = 'Smart card usage - value', ridership = 'Y' where type = 193;
else
	insert into et values(193, 'Smart card usage - value', 'Y');
-- type 194
if exists (select null from et where type = 194)
	update et set text = 'Smart card usage - period', ridership = 'Y' where type = 194;
else
	insert into et values(194, 'Smart card usage - period', 'Y');
-- type 195
if exists (select null from et where type = 195)
	update et set text = 'Smart card usage - transfer', ridership = 'Y' where type = 195;
else
	insert into et values(195, 'Smart card usage - transfer', 'Y');
-- type 196
if exists (select null from et where type = 196)
	update et set text = 'Door open via card', ridership = 'N' where type = 196;
else
	insert into et values(196, 'Door open via card', 'N');
-- type 197
if exists (select null from et where type = 197)
	update et set text = 'Smart card deduct', ridership = 'N' where type = 197;
else
	insert into et values(197, 'Smart card deduct', 'N');
-- type 198
if exists (select null from et where type = 198)
	update et set text = 'Smart card add product', ridership = 'N' where type = 198;
else
	insert into et values(198, 'Smart card add product', 'N');
-- type 199
if exists (select null from et where type = 199)
	update et set text = 'Smart card add value', ridership = 'N' where type = 199;
else
	insert into et values(199, 'Smart card add value', 'N');
-- type 200
if exists (select null from et where type = 200)
	update et set text = 'ID card', ridership = 'N' where type = 200;
else
	insert into et values(200, 'ID card', 'N');
-- type 201
if exists (select null from et where type = 201)
	update et set text = 'Event 201', ridership = 'N' where type = 201;
else
	insert into et values(201, 'Event 201', 'N');
-- type 202
if exists (select null from et where type = 202)
	update et set text = 'Event 202', ridership = 'N' where type = 202;
else
	insert into et values(202, 'Event 202', 'N');
-- type 203
if exists (select null from et where type = 203)
	update et set text = 'Event 203', ridership = 'N' where type = 203;
else
	insert into et values(203, 'Event 203', 'N');
-- type 204
if exists (select null from et where type = 204)
	update et set text = 'Bad card not a bad listed card', ridership = 'N' where type = 204;
else
	insert into et values(204, 'Bad card not a bad listed card', 'N');
-- type 206
if exists (select null from et where type = 206)
	update et set text = 'PaygoRide', ridership = 'Y' where type = 206;
else
	insert into et values(206, 'PaygoRide', 'Y');
-- type 207
if exists (select null from et where type = 207)
	update et set text = 'Bill Transport', ridership = 'N' where type = 207;
else
	insert into et values(207, 'Bill Transport', 'N');
-- type 208
if exists (select null from et where type = 208)
	update et set text = 'Zigbee Info', ridership = 'N' where type = 208;
else
	insert into et values(208, 'Zigbee Info', 'N');
-- type 209
if exists (select null from et where type = 209)
	update et set text = 'Validation Transaction', ridership = 'N' where type = 209;
else
	insert into et values(209, 'Validation Transaction', 'N');
-- type 210
if exists (select null from et where type = 210)
	update et set text = 'Reload Transaction', ridership = 'N' where type = 210;
else
	insert into et values(210, 'Reload Transaction', 'N');
-- type 212
if exists (select null from et where type = 212)
	update et set text = 'Mobile Ticket Transaction', ridership = 'Y' where type = 212;
else
	insert into et values(212, 'Mobile Ticket Transaction', 'Y');
-- type 221
if exists (select null from et where type = 221)
	update et set text = 'MasterList', ridership = 'N' where type = 221;
else
	insert into et values(221, 'MasterList', 'N');
-- type 228
if exists (select null from et where type = 228)
	update et set text = 'Event', ridership = 'N' where type = 228;
else
	insert into et values(228, 'Event', 'N');
-- type 300		LG-1384 - Error inserting HCV_IN table in GDS 
if exists (select null from et where type = 300)
	update et set text = 'HCV validation', ridership = 'N' where type = 300;
else
	insert into et values(300, 'HCV validation', 'N');
go


declare @ls    varchar(256)
declare @ls_eq varchar(4000)
declare @li    smallint
declare @loc   smallint

------------------------------------------------------------------------
-- Set default farebox fare structure
------------------------------------------------------------------------
if not exists(select null from fsc)
begin
   select @ls=dbo.list(loc_n) from cnf
   while len(@ls)>0                               -- loop through all locations
   begin
      set @li=charindex(',',@ls)
      if @li>0
      begin
         set @loc=cast(left(@ls,@li - 1) as smallint)
         set @ls=substring(@ls,@li+1,256)
      end
      else
      begin
         set @loc=cast(@ls as smallint)
         set @ls=''
      end

      exec gfisp_addfs @loc, 1
   end
end
go

------------------------------------------------------------------------
-- Set default e-GO data
------------------------------------------------------------------------
if not exists(select null from gfi_epay_fare where fare_id=0)
   insert into gfi_epay_fare (fare_id,active,type,grp,des,description,flags)
   values(0,0,1,1,0,'Dummy fare record (required)',0)

if not exists(select null from gfi_epay_card where card_id=0)
   insert into gfi_epay_card (card_id,card_type,card_eid,aid,mid,eq_type,eq_n,flags)
   values(0,0,0,0,0,0,0,2)

if not exists(select null from gfi_epay_contact where contact_id=0)
   insert into gfi_epay_contact (contact_id,fname,lname,addr1,city,state,zip,country,phone1,pref,email)
   values(0,'System','Manager','751 Pratt Blvd.','Elk Grove Village','IL','60007','US','847-593-8855',0,'test@example.com')

if not exists(select null from gfi_epay_usr where user_id=0)
begin
   if exists(select 1 from gfi_epay_usr where acct_n=0)
      insert into gfi_epay_usr (user_id,acct_n,login_id,active)
      select 0,min(acct_n) - 1,'system',0 from gfi_epay_usr
   else
      insert into gfi_epay_usr (user_id,acct_n,login_id,active) values(0,0,'system',0)
end
go

declare @ls    varchar(256)
declare @ls_eq varchar(4000)
declare @li    smallint
declare @loc   smallint

set @ls_eq='system,genfare,super,sysop,administrator,dba,'
while (@ls_eq<>'' and @ls_eq<>',')
begin
   set @li=charindex(',',@ls_eq)
   set @ls=left(@ls_eq,@li - 1)
   set @ls_eq=rtrim(substring(@ls_eq,@li+1,4000))

   if not exists(select null from gfi_epay_usr where login_id=@ls)
      insert into gfi_epay_usr (user_id,acct_n,login_id,active)
      select min(user_id) - 1,min(acct_n) - 1,@ls,0 from gfi_epay_usr
end

insert into gfi_epay_usr_billing (user_id,contact_id,default_billing)
select user_id,contact_id,1 from gfi_epay_usr u
where acct_n<1 and not exists(select null from gfi_epay_usr_billing where user_id=u.user_id and contact_id=u.contact_id)
go

declare @loc   smallint

------------------------------------------------------------------------
-- Create "typical" user accounts
------------------------------------------------------------------------
if not exists (select null from gfi_epay_usr where login_id = 'user_admin')
begin
	exec @loc=gfisp_epay_adduseracct 0,'','Devin','','Hester','','NO:1 Halas Way','','','Lake Forest','IL','60001','US','devin.hester@example.com','3457892345','',159,
									 null,null,'user_admin',null,'VCXEvxilNbo=','bfWPygXcc1MQOplJfHLBnqJhpno=',0
	update gfi_epay_usr set pwdexp_ts=null, pwd_qid1=19, pwd_answer1='chicago' where user_id=@loc
end

if not exists (select null from gfi_epay_usr where login_id = 'user_public')
begin
	exec @loc=gfisp_epay_adduseracct 0,'','John','Q','Public','','Park St.','','','Pleasantville','IA','50001','US','john.public@example.com','3457892345','',159,
									 null,null,'user_public',null,'VCXEvxilNbo=','bfWPygXcc1MQOplJfHLBnqJhpno=',0
	update gfi_epay_usr set pwdexp_ts=null, pwd_qid1=19, pwd_answer1='pleasantville' where user_id=@loc
end
go

------------------------------------------------------------------------
-- Create the groups and assign the users 
------------------------------------------------------------------------
-- Create Groups 

if not exists (select null from gfi_epay_grp where grp = 'Admin')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('Admin','Top Level Administrator, Full Privileges',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'Default')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('Default','Default Privilege Group',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'OrgAdmin')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgAdmin','Organization Administrator, Manage Membership',1,current_timestamp,0)

if not exists (select null from gfi_epay_grp where grp = 'OrgAutoBuyer')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgAutoBuyer','Manage Auto Buy',1,current_timestamp,0);
           
if not exists (select null from gfi_epay_grp where grp = 'OrgClerk')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgClerk','Organization Clerk, Service Organization Clients',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'OrgPurchaser') 
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgPurchaser','Purchase New Cards & Tickets',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'OrgReporter')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgReporter','View Reports',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'OrgUnlinker')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgUnlinker','Unlink a Card',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'OrgValueAdder')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgValueAdder','Add Value/Pass to Existing Cards',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'OrgViewer')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('OrgViewer','View Card Balance',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'TransitAgent')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('TransitAgent','Agency Administrator, Most Privileges',1,current_timestamp,0);

if not exists (select null from gfi_epay_grp where grp = 'TransitCustSvc')
	insert into gfi_epay_grp (grp,description,active,create_ts,create_by)
	values ('TransitCustSvc','Agency Customer Service',1,current_timestamp,0);

/* Assigning the tasks to the groups */ 

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ADM ACC GRP')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ADM ACC GRP',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ADM ACC GRP ASSGN')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ADM ACC GRP ASSGN',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ADM ORG ACT RPT')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ADM ORG ACT RPT',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ADM PROXY USER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ADM PROXY USER',current_timestamp,0);
     
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ADM UPDATE CARD')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ADM UPDATE CARD',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ADMIN TRANX RPT')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values('Admin','ADMIN TRANX RPT',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'CONFIGURATOR')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','CONFIGURATOR',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'CUST SERVICE')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','CUST SERVICE',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'GEN CONFIG')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','GEN CONFIG',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'LOG RPT')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','LOG RPT',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'MANAGE USR ACC')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','MANAGE USR ACC',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'MODIFY FARES')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','MODIFY FARES',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ORG ACTIVITY RPT')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ORG ACTIVITY RPT',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ORG REGISTRATION')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ORG REGISTRATION',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'PROCESS MGMT')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','PROCESS MGMT',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'RESET PWD')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','RESET PWD',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Admin' and item = 'ROLE_ADMIN_USER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Admin','ROLE_ADMIN_USER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'AUTO REPLENISH')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','AUTO REPLENISH',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'NEW')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','NEW',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'MANAGE CARD')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','MANAGE CARD',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'ORDER STATUS')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','ORDER STATUS',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'RECHARGE')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','RECHARGE',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'REGISTER CARD')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','REGISTER CARD',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'ROLE_INDIVIDUAL_USER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','ROLE_INDIVIDUAL_USER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'Default' and item = 'REPORT LOST CARD')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('Default','REPORT LOST CARD',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ORG ACTIVITY RPT')
	insert into gfi_epay_grp_right (grp,item)
	values ('OrgAdmin','ORG ACTIVITY RPT')

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_ADMIN_USER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_ADMIN_USER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_AUTO_BUYER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_AUTO_BUYER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_PURCHASER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_PURCHASER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_REPORTER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_REPORTER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_UNLINKER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_UNLINKER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_VALUE_ADDER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_VALUE_ADDER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'OrgAdmin' and item = 'ROLE_ORG_VIEWER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('OrgAdmin','ROLE_ORG_VIEWER',current_timestamp,0);

if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgClerk' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgClerk', 'ROLE_ORG_CLERK_USER')

if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgPurchaser' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgPurchaser','ROLE_ORG_PURCHASER')

if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgValueAdder' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgValueAdder','ROLE_ORG_VALUE_ADDER')
      
if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgUnlinker' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgUnlinker','ROLE_ORG_UNLINKER')
      
if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgAutoBuyer' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgAutoBuyer','ROLE_ORG_AUTO_BUYER')
      
if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgViewer' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgViewer','ROLE_ORG_VIEWER')
      
if not exists( select null from gfi_epay_grp_right WHERE grp = 'OrgReporter' )
	INSERT INTO gfi_epay_grp_right (grp,item)
	VALUES ('OrgReporter','ROLE_ORG_REPORTER')

if not exists (select null from gfi_epay_grp_right where grp = 'TransitAgent' and item = 'MODIFY FARES')
	insert into gfi_epay_grp_right (grp,item)
	values ('TransitAgent','MODIFY FARES')

if not exists (select null from gfi_epay_grp_right where grp = 'TransitAgent' and item = 'ORG REGISTRATION')
	insert into gfi_epay_grp_right (grp,item)
	values ('TransitAgent','ORG REGISTRATION')

if not exists (select null from gfi_epay_grp_right where grp = 'TransitAgent' and item = 'ROLE_ORG_CLERK_USER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('TransitAgent','ROLE_ORG_CLERK_USER',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'TransitCustSvc' and item = 'CUST SERVICE')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('TransitCustSvc','CUST SERVICE',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'TransitCustSvc' and item = 'ORDER STATUS')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('TransitCustSvc','ORDER STATUS',current_timestamp,0);
           
if not exists (select null from gfi_epay_grp_right where grp = 'TransitCustSvc' and item = 'REGISTER CARD')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('TransitCustSvc','REGISTER CARD',current_timestamp,0);

if not exists (select null from gfi_epay_grp_right where grp = 'TransitCustSvc' and item = 'ROLE_ADMIN_USER')
	insert into gfi_epay_grp_right (grp,item,create_ts,create_by)
	values ('TransitCustSvc','ROLE_ADMIN_USER',current_timestamp,0);
go
      
-- Assign users to their respective groups 
   
if not exists( select null from gfi_epay_grp_usr WHERE grp = 'Admin' and user_id = 1 )
	insert into gfi_epay_grp_usr (grp,user_id)
	values ('Admin',1)

if not exists( select null from gfi_epay_grp_usr WHERE grp = 'Default' and user_id = 0 )
	insert into gfi_epay_grp_usr (grp,user_id)
	values ('Default',0)
     
if not exists( select null from gfi_epay_grp_usr WHERE grp = 'Default' and user_id = 2 )
	insert into gfi_epay_grp_usr (grp,user_id)
	values ('Default',2)
go

-- convention change: special route numbers (1000000 and over) are replaced by negative numbers
update mrtesum set route= -2 where route=1000000 and (not exists(select null from mrtesum where route= -2));
update mrtesum set route= -3 where route=1000001 and (not exists(select null from mrtesum where route= -3));
update rtesum  set route= -2 where route=1000000 and (not exists(select null from rtesum  where route= -2));
update rtesum  set route= -3 where route=1000001 and (not exists(select null from rtesum  where route= -3));
go

--------------------------------------------------------------------------
-- Add default row into configurations
--------------------------------------------------------------------------	
if not exists(select null from configurations where id = 0)
begin
	insert into configurations values(0, 'Default', 1, 'Default configuration should be available all the time');
end
go

--------------------------------------------------------------------------
-- Add Cloud Setting			LG-1507 - Merge the SQL code for cloud and non cloud branches
--------------------------------------------------------------------------	
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'Cloud')
begin
	declare @value varchar(100) =	case isnull(dbo.gfisf_getlistvalue('EPAY PARM',0,'TYPE'),'CLASSIC')
										when 'CLOUD' then 'Yes'
										else 'No'
									end
	;
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'Cloud', @value, 1, 'Cloud Setting');
end
go

--------------------------------------------------------------------------
-- Add Fareset Number				LG-1507 - Merge the SQL code for cloud and non cloud branches
--------------------------------------------------------------------------	
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'Fareset Number')
begin
	if dbo.gfisf_GetCloudSetting() = 'Yes'
	begin
		insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
		values(0, 'Fareset Number', '20', 1, 'Number of fare sets');
	end
	else
	begin
		insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
		values(0, 'Fareset Number', '10', 1, 'Number of fare sets');
	end
end
go

--------------------------------------------------------------------------
-- Add TTP Number				LG-1507 - Merge the SQL code for cloud and non cloud branches
--------------------------------------------------------------------------	
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'TTP Number')
begin
	if dbo.gfisf_GetCloudSetting() = 'Yes'
	begin
		insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
		values(0, 'TTP Number', '241', 1, 'Number of TTPs');
	end
	else
	begin
		insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
		values(0, 'TTP Number', '48', 1, 'Number of TTPs');
	end
end
go

--------------------------------------------------------------------------
-- Add admin daily maintenance data
--------------------------------------------------------------------------	
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'admin daily maintenance 001')
begin
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'admin daily maintenance 001', 'exec gfisp_epay_autoload_clean;', 1, 'Command run by gfisp_admin_daily_maintenance');
end
go

--------------------------------------------------------------------------
-- Add Power Builder Version
--------------------------------------------------------------------------	
declare @pbVersion varchar(100) = '3.1.2'; -- format #.##.##.##

if not exists(select null from configurationsData where configurationID = 0 and dataName = 'Power Builder Version')
begin
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'Power Builder Version', @pbVersion, 1, 'Power Builder Version Number (format #.##.##.##)');
end
else
begin
	update configurationsData set
		dataValue = @pbVersion
	where configurationID = 0
	  and dataName = 'Power Builder Version';
end
go

--------------------------------------------------------------------------
-- Add autoload row into configurations
--------------------------------------------------------------------------	
if not exists(select null from configurations where id = 1)
begin
	insert into configurations values(1, 'Autoload', 1, 'Autoload configuration');
end
go

--------------------------------------------------------------------------
-- Add autoload row into autoload configuration data
--------------------------------------------------------------------------	
if not exists(select null from configurationsData where configurationID = 1 and dataName = 'InactiveDays')
begin
	-- deactivated by default
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(1, 'InactiveDays', '180', 0, 'Number of days - must be a positive integer');
end
go

if not exists(select null from configurationsData where configurationID = 1 and dataName = 'IndividualAutoLoadSize')
begin
	-- deactivated by default
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(1, 'IndividualAutoLoadSize', '950000', 0, 'Default size of individual autoload files');
end
go

--------------------------------------------------------------------------
-- Add Network Manager row into configurations
--------------------------------------------------------------------------	
if not exists(select null from configurations where id = 2)
begin
	insert into configurations values(2, 'Network Mgr', 1, 'Network Mgr/SCC/GDS');
end
go

--------------------------------------------------------------------------
-- Add QA Release value row into Network Manager configuration data
--------------------------------------------------------------------------	
declare @qaVersion varchar(100) = '1';

if not exists(select null from configurationsData where configurationID = 2 and dataName = 'QA Release')
begin
	-- deactivated by default
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(2, 'QA Release', @qaVersion, 0, 'GFI INTERNAL ONLY - QA Release Number for specific Release');
end
else 
begin
	update configurationsData set
		dataValue = @qaVersion
	where configurationID = 2
	  and dataName = 'QA Release';
end
go

if not exists(select null from gfi_lst where type='EQ_NAME')
begin
	exec gfisp_additem 'EQ_NAME',	'|MagnaData',					1,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|EDM',							2,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|TVM',							5,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|PEM',							5,	2,	NULL
	exec gfisp_additem 'EQ_NAME',	'|eFare',						5,	3,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Indra',						7,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|ACS',							8,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|RPASS',						9,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|CST',							9,	2,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Special Service Workstation',	9,	3,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Badging Station',				9,	4,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Pass Production',				9,	5,	NULL
	exec gfisp_additem 'EQ_NAME',	'|HID',							10,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|CENTSaBILL',					13,	1,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Odyssey',						13,	2,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Vault',						13,	3,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Handheld Probe',				13,	4,	NULL
	exec gfisp_additem 'EQ_NAME',	'|SCRV',						13,	5,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Fast Fare',	                13,	7,	NULL
	exec gfisp_additem 'EQ_NAME',	'|Fast Fare -e',	            13,	8,	NULL
end
go

-----------------------
-- set database version
-----------------------
declare @v1   tinyint;
declare @v2   tinyint;
declare @v3   tinyint;
declare @note varchar(32);

select @v1=2, @v2=6, @v3=0;

set @note=ltrim(@v1)+'.'+ltrim(@v2)+'.'+ltrim(@v3);

if exists(select null from gfi_db_ver where id=1)
   update gfi_db_ver set v1=@v1, v2=@v2, v3=@v3, note=@note where id=1;
else
   insert into gfi_db_ver(id,v1,v2,v3,note) values(1,@v1,@v2,@v3,@note);
go


-- LG-722
set identity_insert etl_process on;

if exists (select null from etl_process where ProcessID = 1)
	update etl_process set
		[Name] = 'CLOUD_NetworkManager_Orders',
		[Description] = 'Package used to transfer Orders from cloud to network manager'
	where ProcessID = 1
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (1, 'CLOUD_NetworkManager_Orders', 'Package used to transfer Orders from cloud to network manager', N'EtlAdmin', getdate());


if exists (select null from etl_process where ProcessID = 2)
	update etl_process set
		[Name] = 'NetworkManager_Cloud_Transactions_AutoLoad',
		[Description] = 'Package used to transfer AutoLoad transactions network manager to cloud'
	where ProcessID = 2
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (2, 'NetworkManager_Cloud_Transactions_AutoLoad', 'Package used to transfer AutoLoad transactions network manager to cloud', N'EtlAdmin', getdate());

if exists (select null from etl_process where ProcessID = 3)
	update etl_process set
		[Name] = 'NetworkManager_Cloud_Transactions_Ridership',
		[Description] = 'Package used to transfer Ridership transactions network manager to cloud'
	where ProcessID = 3
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (3, 'NetworkManager_Cloud_Transactions_Ridership', 'Package used to transfer Ridership transactions network manager to cloud', N'EtlAdmin', getdate());

if exists (select null from etl_process where ProcessID = 4)
	update etl_process set
		[Name] = 'CLOUD_NetworkManager_FareStructure',
		[Description] = 'Package used to transfer Fare Structure from cloud to network manager'
	where ProcessID = 4
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (4, 'CLOUD_NetworkManager_FareStructure', 'Package used to transfer Fare Structure from cloud to network manager', N'EtlAdmin', getdate());

-- LG-665
if exists (select null from etl_process where ProcessID = 5)
	update etl_process set
		[Name] = 'CLOUD_NetworkManager_Drivers',
		[Description] = 'Package used to transfer Drivers from cloud to network manager'
	where ProcessID = 5
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (5, 'CLOUD_NetworkManager_Drivers', 'Package used to transfer Drivers from cloud to network manager', N'EtlAdmin', getdate());

-- GD-1936
if exists (select null from etl_process where ProcessID = 6)
	update etl_process set
		[Name] = 'ETL_PROCESS_CHECK',
		[Description] = 'Package to check that ETL file process is working OK'
	where ProcessID = 6
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (6, 'ETL_PROCESS_CHECK', 'Package to check that ETL file process is working OK', N'EtlAdmin', getdate());

if exists (select null from etl_process where ProcessID = 7)
	update etl_process set
		[Name] = 'AUTOLOAD_NOTIFICATIONS',
		[Description] = 'Package to send autoload notifications'
	where ProcessID = 7
else
	insert into etl_process ([ProcessID], [Name], [Description], [CreatedBy], [UpdatedDate])
	values (7, 'AUTOLOAD_NOTIFICATIONS', 'Package to send autoload notifications', N'EtlAdmin', getdate());

set identity_insert etl_process off;
go

-- LG-906
if exists (select null from security_app where application = 'data system')
begin
	if exists (select null from security_pref where application = 'data system' and code = 'Show Login')
		update security_pref set
			value = 'Yes'
		where application = 'data system'
		  and code = 'Show Login';
	else
	   insert into security_pref (application,code,value,type,description)
	   values('data system','Show Login','Yes','BY','Yes - show login window; No - do not show login window')
	;
end
go

------------------------------------------------------------------------
-- Set default farebox extended  fare structure		LG-841
------------------------------------------------------------------------
exec gfisp_addfs_extd;
go

-- LG-1053
delete from security_sync where item = 'dup';
go
	
-- LG-1279
if not exists (select null from [dbo].[media_category] where [category] = 0)
insert [dbo].[media_category] ([category], [text]) values (0, 'Transfer');
go
if not exists (select null from [dbo].[media_category] where [category] = 1)
insert [dbo].[media_category] ([category], [text]) values (1, 'Period Pass');
go
if not exists (select null from [dbo].[media_category] where [category] = 2)
insert [dbo].[media_category] ([category], [text]) values (2, 'Stored Ride');
go
if not exists (select null from [dbo].[media_category] where [category] = 3)
insert [dbo].[media_category] ([category], [text]) values (3, 'Stored Value');
go
if exists (select null from [dbo].[media_category] where [category] = 4)
	update [dbo].[media_category] set [text] = 'Credit Card/Special Card' where [category] = 4;
else
	insert [dbo].[media_category] ([category], [text]) values (4, 'Credit Card/Special Card');
go
if not exists (select null from [dbo].[media_category] where [category] = 5)
insert [dbo].[media_category] ([category], [text]) values (5, 'Token / Ticket');
go
if not exists (select null from [dbo].[media_category] where [category] = 7)
insert [dbo].[media_category] ([category], [text]) values (7, 'Maintenance / Employee Card');
go

if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 0 and [type] = 118)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (0, 118);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 0 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (0, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 0 and [type] = 128)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (0, 128);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 0 and [type] = 142)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (0, 142);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 0 and [type] = 195)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (0, 195);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 1 and [type] = 115)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (1, 115);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 1 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (1, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 1 and [type] = 142)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (1, 142);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 1 and [type] = 194)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (1, 194);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 2 and [type] = 114)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (2, 114);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 2 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (2, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 2 and [type] = 142)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (2, 142);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 2 and [type] = 192)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (2, 192);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 116)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 116);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 135)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 135);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 139)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 139);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 142)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 142);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 145)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 145);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 3 and [type] = 193)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (3, 193);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 4 and [type] = 117)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (4, 117);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 4 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (4, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 4 and [type] = 129)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (4, 129);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 5 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (5, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 5 and [type] = 129)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (5, 129);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 7 and [type] = 119)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (7, 119);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 7 and [type] = 194)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (7, 194);
go
if not exists (select null from [dbo].[media_category_transaction_type] where [category] = 7 and [type] = 196)
insert [dbo].[media_category_transaction_type] ([category], [type]) values (7, 196);
go

-- GD-572 - Implement DB/PB related changes for Cleveland Xerox Integration
exec gfisp_addtype 'CLEVELAND XEROX',   'Cleveland Xerox: Input Parms Path, Output Parms Path, Output Transactions Path';
go
if not exists (select null from gfi_lst where [type] = 'Cleveland Xerox' and [class] = 0 and code = 1)
	insert into gfi_lst
	values ('CLEVELAND XEROX', 0,1,'Input Parms Path','C:\Users\NMPN6R\Documents\Cleveland Input Parameters');
go
if not exists (select null from gfi_lst where [type] = 'Cleveland Xerox' and [class] = 0 and code = 2)
	insert into gfi_lst
	values ('CLEVELAND XEROX', 0,2,'Output Parms Path' ,'C:\Users\NMPN6R\Documents\Cleveland Parameter Files');
go
if not exists (select null from gfi_lst where [type] = 'Cleveland Xerox' and [class] = 0 and code = 3)
	insert into gfi_lst
	values ('CLEVELAND XEROX', 0,3,'Output Transactions Path','C:\Users\NMPN6R\Documents\Cleveland Transaction Files');
go

-- add default vnd_def if it does NOT exist
if not exists (select null from vnd_def where pass_type = 0 and pass_category = 0)
	insert into vnd_def (pass_type, pass_category, t_ndx, enabled_f, pass_desc, grp, des)
	values (0, 0, 0, 'Y', 'Undefined card', 0, 0);
go

-- GD-443
-- e.g. 'D:\GFIDB\logFile.txt'
declare @WritingToLogFilePath varchar(100) = '';	-- by default it should be empty

if not exists(select null from configurationsData where configurationID = 0 and dataName = 'LOG_FILE_FULL_NAME')
begin
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'LOG_FILE_FULL_NAME', @WritingToLogFilePath, 1, 'Writing to log file inside transaction');
end
else
begin
	update configurationsData set
		dataValue = @WritingToLogFilePath
	where configurationID = 0
	  and dataName = 'LOG_FILE_FULL_NAME';
end
go

-- GD-2370 - Add option settings to run updtgs and updtrsf stored procedures
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'RUN_UPDTGS')
begin
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'RUN_UPDTGS', 'Yes', 1, 'Setting to run updtgs stored procedure');
end
go
if not exists(select null from configurationsData where configurationID = 0 and dataName = 'RUN_UPDTRSF')
begin
	insert into configurationsData (configurationID, dataName, dataValue, isActive, comments)
	values(0, 'RUN_UPDTRSF', 'Yes', 1, 'Setting to run updtrsf stored procedure');
end
go

-- GD-1936
exec gfisp_additem 'ALN OLD PENDING','|NM Autoload Notifications - Old Pending Autoloads', 0, 0, 'Email Subject';
exec gfisp_additem 'ALN OLD PENDING','', 0, 1, 'Email Recipients (semi-colon separated list)';
exec gfisp_additem 'ALN OLD PENDING','48', 0, 2, 'Positive integer. Number of hours the autoloads had been pending.';
exec gfisp_additem 'ALN OLD PENDING','Number of Autoloads that had been pending for more than <x_hours> hours: <count>.', 0, 3,
					'Email Text.  <x_hours> is place holder for number of hours.  <count> is place holder for number of autoloads';
exec gfisp_additem 'ALN OLD PENDING','8', 0, 4, 'Positive integer. Number of hours.  How frequently to run the process.';

exec gfisp_additem 'ALN UNEXECUTED','|NM Autoload Notifications - Unexecuted Autoloads', 0, 0, 'Email Subject';
exec gfisp_additem 'ALN UNEXECUTED','', 0, 1, 'Email Recipients (semi-colon separated list)';
exec gfisp_additem 'ALN UNEXECUTED','24', 0, 2, 'Positive integer. Number of hours the autoloads had been pending.';
exec gfisp_additem 'ALN UNEXECUTED','Number of Autoloads that had been pending for more than <x_hours> hours although the card has been tapped: <count>.', 0, 3,
					'Email Text.  <x_hours> is place holder for number of hours.  <count> is place holder for number of autoloads';
exec gfisp_additem 'ALN UNEXECUTED','8', 0, 4, 'Positive integer. Number of hours.  How frequently to run the process.';

exec gfisp_additem 'ALN FAILED','|NM Autoload Notifications - Last Failed Autoloads', 0, 0, 'Email Subject';
exec gfisp_additem 'ALN FAILED','', 0, 1, 'Email Recipients (semi-colon separated list)';
exec gfisp_additem 'ALN FAILED','0', 0, 2, 'Last failed gfi_epay_autoload_tr.tr_id';
exec gfisp_additem 'ALN FAILED','Last Failed Autoloads:', 0, 3, 'Header of the Email Text.';
exec gfisp_additem 'ALN FAILED','8', 0, 4, 'Positive integer. Number of hours.  How frequently to run the process.';

exec gfisp_additem 'ALN SMARTCARDUSE','|NM Autoload Notifications - Smart Card Ridership', 0, 0, 'Email Subject';
exec gfisp_additem 'ALN SMARTCARDUSE','', 0, 1, 'Email Recipients (semi-colon separated list)';
exec gfisp_additem 'ALN SMARTCARDUSE','24', 0, 2, 'Positive integer. Number of ridership hours.';
exec gfisp_additem 'ALN SMARTCARDUSE','Usage of Smart Cards within the last <x_hours> hours: <count>.', 0, 3,
					'Email Text.  <x_hours> is place holder for number of hours.  <count> is place holder for number of autoloads';
exec gfisp_additem 'ALN SMARTCARDUSE','8', 0, 4, 'Positive integer. Number of hours.  How frequently to run the process.';

exec gfisp_additem 'ALN SUCCESS','|NM Autoload Notifications - Number of Completed Autoloads', 0, 0, 'Email Subject';
exec gfisp_additem 'ALN SUCCESS','', 0, 1, 'Email Recipients (semi-colon separated list)';
exec gfisp_additem 'ALN SUCCESS','24', 0, 2, 'Positive integer. Number of autoload hours.';
exec gfisp_additem 'ALN SUCCESS','Number of Successfully Completed Autoloads within the last <x_hours> hours: <count>.', 0, 3,
					'Email Text.  <x_hours> is place holder for number of hours.  <count> is place holder for number of autoloads';
exec gfisp_additem 'ALN SUCCESS','8', 0, 4, 'Positive integer. Number of hours.  How frequently to run the process.';

exec gfisp_additem 'ALN FARE BOXES','|NM Autoload Notifications - Number of Probed Fareboxes', 0, 0, 'Email Subject';
exec gfisp_additem 'ALN FARE BOXES','', 0, 1, 'Email Recipients (semi-colon separated list)';
exec gfisp_additem 'ALN FARE BOXES','24', 0, 2, 'Positive integer. Number of hours.';
exec gfisp_additem 'ALN FARE BOXES','Number of Fareboxes that probed within the last <x_hours> hours: <count>.', 0, 3,
					'Email Text.  <x_hours> is place holder for number of hours.  <count> is place holder for number of autoloads';
exec gfisp_additem 'ALN FARE BOXES','8', 0, 4, 'Positive integer. Number of hours.  How frequently to run the process.';
go

-- GD-2206
----------
-- event_subtype
if not exists (select null from event_subtype where etype = 0x80)
	insert into event_subtype values (0x80, 'Clock discrepancy');
go
if not exists (select null from event_subtype where etype = 0x81)
	insert into event_subtype values (0x81, 'Clock failure');
go
if not exists (select null from event_subtype where etype = 0x82)
	insert into event_subtype values (0x82, 'Clock was running on back-up battery');
go
if not exists (select null from event_subtype where etype = 0x83)
	insert into event_subtype values (0x83, 'Cold start');
go
if not exists (select null from event_subtype where etype = 0x84)
	insert into event_subtype values (0x84, 'Memory cleared');
go
if not exists (select null from event_subtype where etype = 0x85)
	insert into event_subtype values (0x85, 'Memory filling past high water mark');
go
if not exists (select null from event_subtype where etype = 0x86)
	insert into event_subtype values (0x86, 'Memory full');
go
if not exists (select null from event_subtype where etype = 0x87)
	insert into event_subtype values (0x87, 'Misread ( non-recoverable )');
go
if not exists (select null from event_subtype where etype = 0x88)
	insert into event_subtype values (0x88, 'Clock Power lost');
go
if not exists (select null from event_subtype where etype = 0x89)
	insert into event_subtype values (0x89, 'IP address was obtained from DHCP server');
go
if not exists (select null from event_subtype where etype = 0x8A)
	insert into event_subtype values (0x8A, 'Bypass on');
go
if not exists (select null from event_subtype where etype = 0x8B)
	insert into event_subtype values (0x8B, 'Bypass on after probe');
go
if not exists (select null from event_subtype where etype = 0x8C)
	insert into event_subtype values (0x8C, 'Bypass off');
go
if not exists (select null from event_subtype where etype = 0x8D)
	insert into event_subtype values (0x8D, 'Cashbox full');
go
if not exists (select null from event_subtype where etype = 0x8E)
	insert into event_subtype values (0x8E, 'Probed, cashbox not removed, has no money');
go
if not exists (select null from event_subtype where etype = 0x8F)
	insert into event_subtype values (0x8F, 'Probed, cashbox not removed, has money');
go
if not exists (select null from event_subtype where etype = 0x90)
	insert into event_subtype values (0x90, 'Door opened authorized');
go
if not exists (select null from event_subtype where etype = 0x91)
	insert into event_subtype values (0x91, 'Door opened unauthorized');
go
if not exists (select null from event_subtype where etype = 0x92)
	insert into event_subtype values (0x92, 'Door/cashbox elapsed time too long');
go
if not exists (select null from event_subtype where etype = 0x93)
	insert into event_subtype values (0x93, 'Door closed');
go
if not exists (select null from event_subtype where etype = 0x94)
	insert into event_subtype values (0x94, 'Portable probe used');
go
if not exists (select null from event_subtype where etype = 0x95)
	insert into event_subtype values (0x95, 'Farebox/TRIM communications lost');
go
if not exists (select null from event_subtype where etype = 0x96)
	insert into event_subtype values (0x96, 'Farebox/TRIM communications restored');
go
if not exists (select null from event_subtype where etype = 0x97)
	insert into event_subtype values (0x97, 'Farebox coin mech failure');
go
if not exists (select null from event_subtype where etype = 0x98)
	insert into event_subtype values (0x98, 'Farebox bill mech failure');
go
-- where is 0x99 ?
if not exists (select null from event_subtype where etype = 0x9A)
	insert into event_subtype values (0x9A, 'Proper driver log off event');
go
if not exists (select null from event_subtype where etype = 0x9B)
	insert into event_subtype values (0x9B, 'Force log off of previous driver, supv etc');
go
if not exists (select null from event_subtype where etype = 0x9C)
	insert into event_subtype values (0x9C, 'Door not open after farebox probed');
go
if not exists (select null from event_subtype where etype = 0x9D)
	insert into event_subtype values (0x9D, 'cashbox removed, authorized');
go
if not exists (select null from event_subtype where etype = 0x9E)
	insert into event_subtype values (0x9E, 'cashbox detected out, unauthorized');
go
if not exists (select null from event_subtype where etype = 0x9F)
	insert into event_subtype values (0x9F, 'cashbox installed');
go
if not exists (select null from event_subtype where etype = 0xA0)
	insert into event_subtype values (0xA0, 'IP address was obtained from DNS condition');
go
if not exists (select null from event_subtype where etype = 0xA1)
	insert into event_subtype values (0xA1, 'status flags to indicate farebox condition');
go
-- event_status
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000001)
	insert into event_status values (0xA1, 0x00000001, 'dump key active');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000002)
	insert into event_status values (0xA1, 0x00000002, 'door switch active');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000004)
	insert into event_status values (0xA1, 0x00000004, 'door latch up');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000008)
	insert into event_status values (0xA1, 0x00000008, 'door latch down');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000010)
	insert into event_status values (0xA1, 0x00000010, 'coin in coin return');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000020)
	insert into event_status values (0xA1, 0x00000020, 'bypass switch active');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000040)
	insert into event_status values (0xA1, 0x00000040, 'burnin test jumper');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000080)
	insert into event_status values (0xA1, 0x00000080, 'memory clear jumper');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000100)
	insert into event_status values (0xA1, 0x00000100, 'spare 1 jumper');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000200)
	insert into event_status values (0xA1, 0x00000200, 'spare 2 jumper');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000400)
	insert into event_status values (0xA1, 0x00000400, 'spare 3 jumper');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00000800)
	insert into event_status values (0xA1, 0x00000800, 'cashbox present timer');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00001000)
	insert into event_status values (0xA1, 0x00001000, 'currently probing');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00002000)
	insert into event_status values (0xA1, 0x00002000, 'probe just completed');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00004000)
	insert into event_status values (0xA1, 0x00004000, 'full cashbox');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00008000)
	insert into event_status values (0xA1, 0x00008000, 'bypass switch was active');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00010000)
	insert into event_status values (0xA1, 0x00010000, 'invalid configuration');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00020000)
	insert into event_status values (0xA1, 0x00020000, 'farebox in self test');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00040000)
	insert into event_status values (0xA1, 0x00040000, 'TRIM comm. offline');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00080000)
	insert into event_status values (0xA1, 0x00080000, 'invalid fare structure');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00100000)
	insert into event_status values (0xA1, 0x00100000, 'test set flag');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00200000)
	insert into event_status values (0xA1, 0x00200000, 'top lid switch active');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00400000)
	insert into event_status values (0xA1, 0x00400000, 'bill override enabled via driver');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x00800000)
	insert into event_status values (0xA1, 0x00800000, 'stock empty');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x01000000)
	insert into event_status values (0xA1, 0x01000000, 'low stock');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x02000000)
	insert into event_status values (0xA1, 0x02000000, 'trim in bypass');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x04000000)
	insert into event_status values (0xA1, 0x04000000, 'bill validator oos');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x08000000)
	insert into event_status values (0xA1, 0x08000000, 'probed with alarm delay');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x10000000)
	insert into event_status values (0xA1, 0x10000000, 'bill jam detected');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x20000000)
	insert into event_status values (0xA1, 0x20000000, 'trim card jam detected');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x40000000)
	insert into event_status values (0xA1, 0x40000000, 'smart card off line');
go
if not exists (select null from event_status where etype = 0xA1 and id = 0x80000000)
	insert into event_status values (0xA1, 0x80000000, 'OCU off line');
go

-- GD-2555
update scd set
	grp = 0,
	des = 0,
	ttp = 0
where grp > 9;
go

-- GD-2293
update top (1000) prb_log set
	user_id = 'n/a'
where user_id = '';

while @@rowcount = 1000
begin
	update top (1000) prb_log set
		user_id = 'n/a'
	where user_id = '';
end
go


---------------------------
-- Enabled Jobs
---------------------------
declare @jobName varchar(100);

declare curData cursor local
for	select
		dataName = substring(dataName, 5, 100)
	from configurationsData
	where	configurationID = 0 -- Default
		and	dataName like 'Job %'
		and isActive = 1
		and dataValue = 'enabled'
	order by dataName
	for read only
;

open curData;

fetch next from curData into @jobName;

while (@@fetch_status = 0)
begin
	print 'Enabling job ' + @jobName + '...';
	exec msdb.dbo.sp_update_job @job_name=@jobName, @enabled = 1;
	print 'Done';

	fetch next from curData into @jobName;
end

close curData;
deallocate curData;

update configurationsData set
	dataValue = ''
where	configurationID = 0
	and	dataName like 'Job %'
	and	dataValue <> ''
;
go

-- ===
-- END
-- ===
-- =================
-- SCRIPT END
-- =================

print 'Script end: ' + convert(varchar(12),GETDATE(),114)

-- ===
-- END
-- ===