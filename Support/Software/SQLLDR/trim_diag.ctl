OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/trimdiag.TXT"
INTO TABLE trim_diag APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
partnum,
read,
misread,
write,
badverify,
print,
issue,
jam,
read_t,
misread_t,
write_t,
badverify_t,
print_t,
issue_t,
jam_t,
cycle_t,
cold,
warm,
pwrdown,
reset
)
