OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/SCD.TXT"
INTO TABLE scd APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
tr_seq,
fs,
flags,
trk2,
grp,
des
)
