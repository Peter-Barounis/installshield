OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/SRD.TXT"
INTO TABLE srd APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
tr_seq,
seq,
fs,
ttp,
grp,
des,
aid,
flags,
remval,
impval,
f_use_f,
restored_f
)
