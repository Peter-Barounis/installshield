OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/TRD.TXT"
INTO TABLE trd APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
tr_seq,
seq,
fs,
ttp,
grp,
des,
aid,
mid,
flags,
restored_f "(CASE WHEN LENGTH(:restored_f)>0 THEN :restored_f ELSE ' ' END)",
org_rte,
org_dir "(CASE WHEN LENGTH(:org_dir)>0 THEN :org_dir ELSE ' ' END)",
trim_n,
impval
)
