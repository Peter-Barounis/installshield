OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/GFI_EPAY_AUTOLOAD_TR.TXT"
INTO TABLE gfi_epay_autoload_tr APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
tr_seq,
load_type,
ts "TO_DATE(SUBSTR(:ts, 1, 19), 'YYYY-MM-DD HH24:MI:SS')",
mid,
eq_type,
eq_n,
eq_tr_id,
card_id,
card_type,
card_eid,
prod_id,
prod_id_card,
pkg_id,
load_seq,
load_seq_card,
range,
rc,
value,
pending,
flags
)
