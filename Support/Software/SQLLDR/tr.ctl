OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/TR.TXT"
INTO TABLE tr APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
tr_seq,
ts "TO_DATE(SUBSTR(:ts, 1, 19), 'YYYY-MM-DD HH24:MI:SS')",
type,
bus,
route,
run,
trip,
dir "(CASE WHEN LENGTH(:dir)>0 THEN :dir ELSE ' ' END)",
n,
longitude,
latitude,
stop_name,
fs,
drv
)
