OPTIONS(ROWS=1000,ERRORS=1000000)
LOAD INFILE "%GFI%/tmp/SVD.TXT"
INTO TABLE svd APPEND
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'"
TRAILING NULLCOLS
(
loc_n,
id,
tr_seq,
seq,
fs,
ttp,
grp,
des,
aid,
mid,
flags,
remval,
deduction,
impval,
ktfare,
f_use_f,
restored_f
)
