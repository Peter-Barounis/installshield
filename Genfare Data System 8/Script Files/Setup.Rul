//===========================================================================
//
//  File Name:    Setup.rul
//
//  Description:  Blank setup main script file
//
//  Comments:     Blank setup is an empty setup project. If you want to
//				  create a new project via. step-by step instructions use the
//				  Project Assistant.
//
//===========================================================================

// Included header files ----------------------------------------------------
#include "ifx.h"
#include "c:\git\data-system\common\include\DataSystem8Version.h"
prototype InstallVS2012Redist();
prototype StopGFI();
prototype KillProc();
prototype string GetGFIDir();
prototype number RunMaximized(string, string);
prototype number Run(string, string);
prototype PathIn(string);
prototype PathOut(string);
prototype NotifyChange();
prototype RegSetValue(string,string,int,string);
prototype RegDelValue(string,string);
prototype InstallReg();
prototype CreateGFIDir();
prototype SetPermissionsOnDir(string);
prototype number CreateDirectory(string);

prototype BOOL USER.SendNotifyMessageA(HWND,long,long,byref string); 
prototype BOOL Shlwapi.StrTrimA(byref string, byval string);
prototype LONG USER.GetSystemMetrics(long);
prototype BOOL KERNEL.GetProductInfo(long,long,long,long,byref long);

number bInitService;
string sNotifyType;
number nSysType;

//---------------------------------------------------------------------------                                                                        
// OnFirstUIBefore
//
// First Install UI Sequence - Before Move Data
//
// The OnFirstUIBefore event is called by OnShowUI when the setup is
// running in first install mode. By default this event displays UI allowing
// the end user to specify installation parameters.
//
// Note: This event will not be called automatically in a
// program...endprogram style setup.
//---------------------------------------------------------------------------
function OnFirstUIBefore()
    number  nResult, nLevel, nSize, nSetupType, nType;
    string  szTitle, szMsg, szOpt1, szOpt2, szLicenseFile;
    string  szName, szCompany, szTargetPath, szDir, szFeatures;
    BOOL    bLicenseAccepted;
    string DotNetVersion;    
    
begin	
    IFX_PRODUCT_VERSION=DATASYSTEM8VERSION;
    IFX_PRODUCT_DISPLAY_NAME="GFI Data System "+IFX_PRODUCT_VERSION+" Setup";
	// Set default font for dialogs
    //DialogSetFont("Arial",10,0);      
    
    // Set the alternate bitmap dialogs
    //DialogSetInfo(DLG_INFO_ALTIMAGE, SUPPORTDIR^"FastFare.bmp", TRUE); 

    nSysType=6;	// Set system type to Data System 8
    
    // Added in InstallShield 15 - Show an appropriate error message if
    // -removeonly is specified and the product is not installed.
    if( REMOVEONLY ) then
        Disable( DIALOGCACHE );
		szMsg = SdLoadString( IDS_IFX_ERROR_PRODUCT_NOT_INSTALLED_UNINST );
   		SdSubstituteProductInfo( szMsg );
		MessageBox( szMsg, SEVERE );
		abort;
    endif;
    
    RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);
    RegDBGetKeyValueEx("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\V3.5","Version",nType,DotNetVersion,nSize);    
    if (StrCompare(DotNetVersion,"3.5") < 0) then
	    if AskYesNo("The Genfare Data System requires .Net 3.5 or higher\nCurrent version is "+DotNetVersion + ". If you continue with the installation, you will need to install .Net 3.5 before running the applicaiton.\n\nDo you want to Continue?",NO)=NO then
			exit;
		endif;
    endif;   

    StopGFI();                                    // stop GFI processes
    
    nSetupType = COMPLETE;	
    szDir = TARGETDIR;
    szName = "";
    szCompany = "";
    bLicenseAccepted = FALSE;

// Beginning of UI Sequence
Dlg_Start:
    nResult = 0;
	InstallVS2012Redist();
	TARGETDIR="C:\\GFI";
Dlg_SdWelcome:
    szTitle = "";
    szMsg = "";
    //{{IS_SCRIPT_TAG(Dlg_SdWelcome)
    nResult = SdWelcome( szTitle, szMsg );
    //}}IS_SCRIPT_TAG(Dlg_SdWelcome)
    if (nResult = BACK) goto Dlg_Start;

Dlg_SdLicense2:
    szTitle = "";
    szOpt1 = "";
    szOpt2 = "";
    //{{IS_SCRIPT_TAG(License_File_Path)
    szLicenseFile = SUPPORTDIR ^ "lic.rtf";
    //}}IS_SCRIPT_TAG(License_File_Path)
    //{{IS_SCRIPT_TAG(Dlg_SdLicense2)
    nResult = SdLicense2Ex( szTitle, szOpt1, szOpt2, szLicenseFile, bLicenseAccepted, TRUE );
    //}}IS_SCRIPT_TAG(Dlg_SdLicense2)
    if (nResult = BACK) then
        goto Dlg_SdWelcome;
    else
        bLicenseAccepted = TRUE;
    endif;

Dlg_SdRegisterUser:
    szMsg = "";
    szTitle = "";
    //{{IS_SCRIPT_TAG(Dlg_SdRegisterUser)	
    nResult = SdRegisterUser( szTitle, szMsg, szName, szCompany );
    //}}IS_SCRIPT_TAG(Dlg_SdRegisterUser)
    if (nResult = BACK) goto Dlg_SdLicense2;

Dlg_SetupType2:   
    szTitle = "";
    szMsg = "";
    nResult = CUSTOM;
    if (nResult = BACK) then
        goto Dlg_SdRegisterUser;
    else
        nSetupType = nResult;
        if (nSetupType != CUSTOM) then
            szTargetPath = TARGETDIR;
            nSize = 0;
            FeatureCompareSizeRequired( MEDIA, szTargetPath, nSize );
            if (nSize != 0) then      
                MessageBox( szSdStr_NotEnoughSpace, WARNING );
                goto Dlg_SetupType2;
            endif;
        endif;   
    endif;

Dlg_SdAskDestPath2:
    if ((nResult = BACK) && (nSetupType != CUSTOM)) goto Dlg_SetupType2;
	szTitle = "";
    szMsg = "";
    szDir=TARGETDIR;
    if (nSetupType = CUSTOM) then
		nResult = SdAskDestPath2( szTitle, szMsg, szDir );
        TARGETDIR = szDir;
    endif;
    if (nResult = BACK) goto Dlg_SetupType2;

Dlg_SdFeatureTree: 
    if ((nResult = BACK) && (nSetupType != CUSTOM)) goto Dlg_SdAskDestPath2;
    szTitle = "";
    szMsg = "";
    szFeatures = "";
    nLevel = 2;
    if (nSetupType = CUSTOM) then
        //{{IS_SCRIPT_TAG(Dlg_SdFeatureTree)	
        //nResult = SdFeatureTree( szTitle, szMsg, TARGETDIR, szFeatures, nLevel );
        //}}IS_SCRIPT_TAG(Dlg_SdFeatureTree)
        if (nResult = BACK) goto Dlg_SdAskDestPath2;  
    endif;

Dlg_SQLServer:
    nResult = OnSQLServerInitialize( nResult );
    if( nResult = BACK ) goto Dlg_SdFeatureTree;

Dlg_ObjDialogs:
    nResult = ShowObjWizardPages( nResult );
    if (nResult = BACK) goto Dlg_SQLServer;
  
Dlg_SdStartCopy2:
    szTitle = "";
    szMsg = "";
    nResult = SdStartCopy2( szTitle, szMsg );	
    if (nResult = BACK) goto Dlg_ObjDialogs;
    // Added in 11.0 - Set appropriate StatusEx static text.
    SetStatusExStaticText( SdLoadString( IDS_IFX_STATUSEX_STATICTEXT_FIRSTUI ) );

    return 0;
end;


function InstallVS2012Redist()
    string svVal;
	number	nvType, nvSize, n;
begin

	if (!Is(FILE_EXISTS,WINSYSDIR^"msvcr110.dll")) then 
		RunMaximized(SUPPORTDIR^"vcredist_x86.exe","");			
	endif;		
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function StopGFI()
//
// Description: stop GFI programs
//
/////////////////////////////////////////////////////////////////////////////
function StopGFI()
   number n;
begin
   bInitService=FALSE;
   if ServiceExistsService("InitService") then
      if ServiceGetServiceState("InitService",n)>=ISERR_SUCCESS then
         if n!=SERVICE_STOPPED then
            SdShowMsg("Stopping GFI INIT service ...",TRUE);
            //Log("Stopping GFI INIT service ...");
            ServiceStopService("InitService");
            bInitService=TRUE;
            SdShowMsg("",FALSE);
         endif;
      endif;
   endif;

   KillProc();
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function KillProc()
//
// Description: terminate all running GFI processes
//
/////////////////////////////////////////////////////////////////////////////
function KillProc()
   string s, sDir, sMsg;
   number n;
begin
   if !Is(FILE_EXISTS,SUPPORTDIR^"kill.exe") then
      return 0;
   endif;

   //Log("Terminating GFI applications and background processes ...");

   sDir=GetGFIDir();                             // GFI processes are in TARGETDIR\bin directory

   if sDir != "" then
	   sMsg="Stopping process ";
	   sDir=sDir^"bin";
	   n=FindAllFiles(sDir,"*.exe",s,RESET);
	   
	   while n=0
		  if Is(FILE_LOCKED,s) then                  // if an executable file is locked, it is running
			 if ParsePath(s,s,FILENAME_ONLY)=0 then
				SdShowMsg(sMsg+s+"...",TRUE);
				Run(SUPPORTDIR^"kill.exe",s);
				//Log("GFI process "+s+" was terminated");
				SdShowMsg("",FALSE);
			 endif;
		  endif;
		  n=FindAllFiles(sDir,"*.exe",s,CONTINUE);
	   endwhile;
	   
	   FindAllFiles(sDir,"*.exe",s,CANCEL);
   endif;
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function string GetGFIDir()
//
// Description: obtain GFI software base directory
//
// Return: GFI software base directory or an empty string if not found
//
/////////////////////////////////////////////////////////////////////////////
function string GetGFIDir()
   string s;
   number n, nRootKey;
begin
   nRootKey=RegDBGetDefaultRoot();               // remember current root key

   RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);      // GFI software base directory setting is stored either in registry and/or in GFI environment variable
   if RegDBGetKeyValueEx("SOFTWARE\\GFI Genfare\\Global","Base Directory",n,s,n)=0 && StrLength(s)>3 then
   elseif GetEnvVar("GFI",s)=0 && StrLength(s)>3 then
   else
      s="";
   endif;

   if StrLength(s)>3 then
      StrRemoveLastSlash(s);
      if !Is(PATH_EXISTS,s) then                 // make sure the directory is valid
         s="";
      endif;
   endif;

   RegDBSetDefaultRoot(nRootKey);                // restore original root key

   return s;
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function number Run(sCmd, sParm)
//
// Description: execute a command and wait until it finishes 
//
// Arguments: sCmd - executable file
//            sParm - command line arguments
//
// Return: 0 (ISERR_SUCCESS) - success; <0 - failed
//
/////////////////////////////////////////////////////////////////////////////
function number RunMaximized(sCmd,sParm)
   BOOL   bReboot, bLogging;
   number n;
begin
   bReboot=BATCH_INSTALL;                        // remember current reboot status
   bLogging=Is(LOGGING,"");
   Disable(LOGGING);                             // disable InstallShield logging (uninstallation logging)

   LongPathToQuote(sCmd,TRUE);
   //n=LaunchAppAndWait(sCmd,sParm,LAAW_OPTION_WAIT | LAAW_OPTION_HIDDEN | LAAW_OPTION_MINIMIZED);
   n=LaunchAppAndWait(sCmd,sParm,LAAW_OPTION_WAIT);

   if bLogging then
      Enable(LOGGING);                           // restore InstallShield logging setting
   endif;
   BATCH_INSTALL=bReboot;                        // restore InstallShield reboot flag

   return n;
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function number Run(sCmd, sParm)
//
// Description: execute a command and wait until it finishes 
//
// Arguments: sCmd - executable file
//            sParm - command line arguments
//
// Return: 0 (ISERR_SUCCESS) - success; <0 - failed
//
/////////////////////////////////////////////////////////////////////////////
function number Run(sCmd,sParm)
   BOOL   bReboot, bLogging;
   number n;
begin
   bReboot=BATCH_INSTALL;                        // remember current reboot status
   bLogging=Is(LOGGING,"");
   Disable(LOGGING);                             // disable InstallShield logging (uninstallation logging)

   LongPathToQuote(sCmd,TRUE);
   n=LaunchAppAndWait(sCmd,sParm,LAAW_OPTION_WAIT | LAAW_OPTION_HIDDEN | LAAW_OPTION_MINIMIZED);

   if bLogging then
      Enable(LOGGING);                           // restore InstallShield logging setting
   endif;
   BATCH_INSTALL=bReboot;                        // restore InstallShield reboot flag

   return n;
end;

////////////////////////////////////////////////////////////////////////////
//
// Function PathIn(sDir)
//
// Description: appends sDir to system search path
//
// Argument: sDir - directory to append
//
/////////////////////////////////////////////////////////////////////////////
function PathIn(sDir)
   string sKey, sVar;
   number nType, nSize, nRootKey;
   BOOL   bLogging;
begin
   StrRemoveLastSlash(sDir);
   if StrLength(sDir)>0 && Is(VALID_PATH,sDir) then
      nRootKey=RegDBGetDefaultRoot();            // remember current root key
      bLogging=Is(LOGGING,"");

      PathOut(sDir);                             // if it is already in the path, take it out

      Disable(LOGGING);                          // do not log PATH change

      RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);
      sKey="SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
         
      if RegDBGetKeyValueEx(sKey,"Path",nType,sVar,nSize)<0 then
         sVar="%SystemRoot%\\system32;%SystemRoot%;%SystemRoot%\\System32\\Wbem";
         nType=REGDB_STRING_EXPAND;              // if PATH is not defined, use default
      else
         StrTrimA(sVar,";");                     // remove trailing semicolons
         if StrLength(sVar)=0 then
            sVar="%SystemRoot%\\system32;%SystemRoot%;%SystemRoot%\\System32\\Wbem";
         endif;
      endif;                                     // append directory to path
      RegSetValue(sKey,"Path",nType,sVar+";"+sDir);

      NotifyChange();                            // broadcast the PATH change

      if bLogging then
         Enable(LOGGING);
      endif;

      RegDBSetDefaultRoot(nRootKey);             // restore original root key

      //Log("Directory "+sDir+" was appended to the system search path");
      //Log("System search path: "+sVar+";"+sDir);
   endif;
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function PathOut(sDir)
//
// Description: remove sDir from system search path
//
// Argument: sDir - directory to remove from PATH
//
/////////////////////////////////////////////////////////////////////////////
function PathOut(sDir)
   string sKey, sVar, sV1, sV2, sPath;
   number nType, nSize, nRootKey;
   BOOL   bLogging, bRemoved;
begin
   StrRemoveLastSlash(sDir);
   if StrLength(sDir)>0 && Is(VALID_PATH,sDir) then
      nRootKey=RegDBGetDefaultRoot();            // remember current root key
      bLogging=Is(LOGGING,"");

      Disable(LOGGING);                          // do not log PATH change

      RegDBSetDefaultRoot(HKEY_CURRENT_USER);    // examine user PATH first
      sKey="Environment";

lbl_start:
      if RegDBGetKeyValueEx(sKey,"Path",nType,sVar,nSize)=0 && nSize>0 then
         sPath=sVar;
         while ((sVar+";") % (sDir+";"))         // case insensitive search, remove all occurrences of sDir
            nSize=StrFind(sVar+";",sDir+";");
            StrSub(sV1,sVar,0,nSize);
            StrSub(sV2,sVar,nSize+StrLength(sDir+";"),32767);
            sVar=sV1+sV2;
            bRemoved=TRUE;
         endwhile;
   
         while ((sVar+";") % (sDir+"\\;"))       // case insensitive search, remove all occurrences of sDir\
            nSize=StrFind(sVar+";",sDir+"\\;");
            StrSub(sV1,sVar,0,nSize);
            StrSub(sV2,sVar,nSize+StrLength(sDir+"\\;"),32767);
            sVar=sV1+sV2;
            bRemoved=TRUE;
         endwhile;
   
         StrTrimA(sVar,";");                     // remove trailing semicolons
   
         if sPath=sVar then
         elseif StrLength(sVar)>0 then
            RegSetValue(sKey,"Path",nType,sVar);
         elseif RegDBGetDefaultRoot()=HKEY_LOCAL_MACHINE then
                                                 // system search path is not defined, set to default
            RegSetValue(sKey,"Path",nType,"%SystemRoot%\\system32;%SystemRoot%;%SystemRoot%\\System32\\Wbem");
         else
            RegDelValue(sKey,"Path");            // user search path is not defined, make sure it's removed
         endif;

      elseif RegDBGetDefaultRoot()=HKEY_LOCAL_MACHINE then
                                                 // system search path is not defined, set to default
         RegSetValue(sKey,"Path",REGDB_STRING_EXPAND,"%SystemRoot%\\system32;%SystemRoot%;%SystemRoot%\\System32\\Wbem");
      else
         RegDelValue(sKey,"Path");               // user search path is not defined, make sure it's removed
      endif;
         
      if RegDBGetDefaultRoot()=HKEY_CURRENT_USER then
         RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);// examine HKCU first and HKLM second
         sKey="SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
         goto lbl_start;
      endif;
      
      NotifyChange();

      if bLogging then
         Enable(LOGGING);
      endif;

      RegDBSetDefaultRoot(nRootKey);             // restore original root key

      //if bRemoved then
      //   Log("Directory "+sDir+" was removed from the user/system search path");
      //endif;
   endif;
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function RegSetValue(sKey,sName,nType,sVal)
//
// Description: set a registry value
//
// Arguments: sKey - registry key (use the current root key)
//            sName - registry value name
//            nType - registry value type (support REGDB_NUMBER, REGDB_STRING and REGDB_STRING_EXPAND only)
//            sVal - value
//
/////////////////////////////////////////////////////////////////////////////
function RegSetValue(sKey,sName,nType,sVal)
   string s;
   number n;
begin
   if RegDBGetKeyValueEx(sKey,sName,n,s,n)=0 && s=sVal then
   else
      if (REGDB_OPTIONS & REGDB_OPTION_WOW64_64KEY) then
         s=" (64-bit)";
      else
         s="";
      endif;

      RegDBSetKeyValueEx(sKey,sName,nType,sVal, -1);
   endif;
end;
/////////////////////////////////////////////////////////////////////////////
//
// Function NotifyChange()
//
// Description: broadcast Windows environment variable change, e.g. PATH
//
/////////////////////////////////////////////////////////////////////////////
function NotifyChange()
begin
   if sNotifyType="Environment" then             // use global variable for its duration; this may be necessary since the API returns immediately
   else
      sNotifyType="Environment";
   endif;
                                                 // SendNotifyMessage() API returns control back immediately
   USER.SendNotifyMessageA(0xffff,0x001A,0,sNotifyType);   // HWND_BROADCAST=0xffff; WM_SETTINGCHANGE=0x001A 
end;


/////////////////////////////////////////////////////////////////////////////
//
// Function RegDelValue(sKey,sName)
//
// Description: delete a registry entry
//
// Arguments: sKey - registry key (use the current root key)
//            sName - registry value name
//
/////////////////////////////////////////////////////////////////////////////
function RegDelValue(sKey,sName)
   number n;
   string s;
begin
   if StrLength(sName)>0 && RegDBGetKeyValueEx(sKey,sName,n,s,n)=0 then
      if (REGDB_OPTIONS & REGDB_OPTION_WOW64_64KEY) then
         s=" (64-bit)";
      else
         s="";
      endif;

      RegDBDeleteValue(sKey,sName);
      //   Log("Registry entry "+GetRootKey()+"\\"+sKey+"->"+sName+" was deleted"+s);
      //else
      //   Log("Deleting registry entry "+GetRootKey()+"\\"+sKey+"->"+sName+" failed"+s);
      //endif;
   endif;
end;
//---------------------------------------------------------------------------
// OnUpdateUIAfter
//
// Update Mode UI Sequence - After Move Data
//
// The OnUpdateUIAfter event called by OnShowUI after the file transfer
// of the setup when the setup is running in update mode. By default
// this event displays UI that informs the end user that the maintenance setup
// has been completed successfully.
//
// Note: This event will not be called automatically in a
// program...endprogram style setup.
//---------------------------------------------------------------------------
function OnUpdateUIAfter()
    string	szTitle, szMsg1, szMsg2, szOpt1, szOpt2;
    BOOL	bOpt1, bOpt2;
begin

    ShowObjWizardPages(NEXT);
    
	szTitle = SdLoadString(IDS_IFX_SDFINISH_UPDATE_TITLE);    
    szMsg1  = SdLoadString(IDS_IFX_SDFINISH_UPDATE_MSG1);
	szMsg2 = "";    
    szOpt1 = "";
    szOpt2 = "";
	bOpt1   = FALSE;
    bOpt2   = FALSE;    
	PathIn(TARGETDIR^"bin");    
    if ( BATCH_INSTALL ) then
    	SdFinishReboot ( szTitle , szMsg1 , SYS_BOOTMACHINE , szMsg2 , 0 );
    else    
       	SdFinish ( szTitle , szMsg1 , szMsg2 , szOpt1 , szOpt2 , bOpt1 , bOpt2 );
    endif;
end;

//---------------------------------------------------------------------------
// OnEnd
//
// The OnEnd event is called at the end of the setup. This event is not
// called if the setup is aborted.
//---------------------------------------------------------------------------
function OnEnd()
	string sDir;
begin
	if REMOVEONLY || REMOVEALLMODE then
		sDir=GetGFIDir()^"bin";  
		PathOut(sDir);
	else
		InstallReg();
		CreateGFIDir();
	endif;
end;


// this function sets registry settings
function InstallReg()
	string	svVal, s, sysType;
	number	n, nvType, nvSize;
	number nvLine,nvError;
	
begin
	NumToStr(sysType,nSysType);

	//Log("Updating registry settings");
	SdShowMsg("Creating file associations...",TRUE);	
	Delay(1);
	// associate .stl file with GFI log file viewer
	RegDBSetDefaultRoot(HKEY_CLASSES_ROOT);
	svVal="\""+TARGETDIR^"bin\\gfifmon.exe\" %1";
	RegDBSetKeyValueEx("Applications\\gfifmon.exe\\Shell\\Open\\Command","",REGDB_STRING_EXPAND,svVal, -1);
	RegDBSetKeyValueEx("GFI.LogViewer\\Shell\\Open\\Command","",REGDB_STRING_EXPAND,svVal, -1);
	RegDBSetKeyValueEx("GFI.LogViewer","",REGDB_STRING,"GFI Log Viewer Associated File", -1);
	RegDBSetKeyValueEx("GFI.LogViewer\\DefaultIcon","",REGDB_STRING,TARGETDIR^"bin\\gfifmon.exe", -1);
	if RegDBGetKeyValueEx(".stl","",n,svVal,n)=0 && StrLength(svVal)>0 && svVal!="GFI.LogViewer" then
		RegDBSetKeyValueEx(".stl\\OpenWithProgids\\"+svVal,"",REGDB_STRING,"", -1);
	endif;
	RegDBSetKeyValueEx(".stl","",REGDB_STRING,"GFI.LogViewer", -1);


	//SetStatusWindow(0,"Creating registry settings...");
	SdShowMsg("Creating registry settings...",TRUE);	
	Delay(1);
	RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);

	if !MAINTENANCE then
		PathIn(TARGETDIR^"bin");

		RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);
		RegDBDeleteKey("SOFTWARE\\GFI Genfare");
		RegDBCreateKeyEx("SOFTWARE\\GFI Genfare","");
		RegDBCreateKeyEx("SOFTWARE\\GFI Genfare\\Global","");
		RegDBSetKeyValueEx("SOFTWARE\\GFI Genfare\\Global","Base Directory",REGDB_STRING,TARGETDIR, -1);
		RegDBSetKeyValueEx("SOFTWARE\\GFI Genfare\\Global","System Type",REGDB_NUMBER,sysType, -1);

		//Log("Installing Init Service: "+ TARGETDIR^"bin\\init.exe");
		n=ServiceAddService("InitService","GFI INIT Service","GFI background process dispatcher",TARGETDIR^"bin\\gfiinit.exe",FALSE,"");
		NumToStr(svVal,n);
		if n<ISERR_SUCCESS then
			GetExtendedErrInfo(svVal,nvLine,nvError); 			
			MessageBox ( "Can't install Service - " + svVal, WARNING); 
		endif;
	
		RegDBSetItem(REGDB_UNINSTALL_COMMENTS,IFX_PRODUCT_DISPLAY_NAME);
	endif;
	// Set Data System Version in registry
    RegDBSetDefaultRoot(HKEY_LOCAL_MACHINE);
    RegDBSetKeyValueEx("SOFTWARE\\GFI Genfare","Version",REGDB_STRING,IFX_PRODUCT_VERSION, -1);		
	
	SdShowMsg("",FALSE);	
end;


function CreateGFIDir()
begin
	SdShowMsg("Creating GFI software directory structure...",TRUE);
	CreateDirectory(TARGETDIR);
	SetPermissionsOnDir(TARGETDIR);
	CreateDirectory(TARGETDIR^"bin");
	CreateDirectory(TARGETDIR^"cnf");
	CreateDirectory(TARGETDIR^"hlp");
	CreateDirectory(TARGETDIR^"log");
	CreateDirectory(TARGETDIR^"aa");
	CreateDirectory(TARGETDIR^"aa.cnf");
	CreateDirectory(TARGETDIR^"aa.err");
	CreateDirectory(TARGETDIR^"aa.lst");
	CreateDirectory(TARGETDIR^"aa.prc");
	CreateDirectory(TARGETDIR^"aa.snd");
	CreateDirectory(TARGETDIR^"aa.tmp");
	CreateDirectory(TARGETDIR^"firmware");
	CreateDirectory(TARGETDIR^"mtk");		
	CreateDirectory(TARGETDIR^"mbtk.err");
	CreateDirectory(TARGETDIR^"mbtk.snd");
	CreateDirectory(TARGETDIR^"new");
	CreateDirectory(TARGETDIR^"old");
	CreateDirectory(TARGETDIR^"rcv");
	CreateDirectory(TARGETDIR^"rcv.err");
	CreateDirectory(TARGETDIR^"rcv.prc");
	CreateDirectory(TARGETDIR^"rcv.tmp");
	CreateDirectory(TARGETDIR^"tmp");
	SdShowMsg("",FALSE);	
end;

//---------------------------------------------------------------------------
// OnMoveData
//
// The OnMoveData event is called by OnShowUI to initiate the file
// transfer of the setup.
//
// Note: This event will not be called automatically in a
// program...endprogram style setup.
//---------------------------------------------------------------------------
function OnMoveData()
number	nResult, nMediaFlags;
begin
	// Don't install the DISK1COMPONENT if MAINT_OPTION_NONE was specified.
	if( MAINT_OPTION = MAINT_OPTION_NONE ) then
		FeatureSelectItem( MEDIA, DISK1COMPONENT, FALSE );
	endif;

    // Updated in 11.5, disable the cancel button during file transfer unless
	// this is non-maintenance mode or repair mode.
    if( MAINTENANCE && ( !REINSTALLMODE || UPDATEMODE ) ) then
        Disable( CANCELBUTTON );
    endif;

    // Show Status
	// Note: Start status window at 1 in case CreateInstallationInfo call
	// is lengthy.
	SetStatusWindow( 1, "" );
	Enable( STATUSEX );
	StatusUpdate( ON, 100 );

	// Create the uninstall infomation (after displaying the progress dialog)
	// Don't create uninstall information if MAINT_OPTION_NONE was specified.
	if( MAINT_OPTION != MAINT_OPTION_NONE ) then
		CreateInstallationInfo();
	endif;

	// Move Data
	nResult = FeatureTransferData( MEDIA );
	
    // Moved in 11.0, Check for failure before creating uninstall key.
    // Handle move data error and abort if error occured.
	if( nResult < ISERR_SUCCESS ) then
		OnComponentError();
		abort;
	endif;	    

	// Create uninstall key, if DISK1COMPONENT was installed.
	if( IFX_DISK1INSTALLED ) then

		// Store text-subs for maintenance mode later, only do this when
		// disk 1 is installed. Note that any text-subs that are updated after
        // this call will not be remembered during maintenance mode.
		FeatureSaveTarget("");

		// Write uninstall information.
		MaintenanceStart();

		// Customize Uninstall Information
		OnCustomizeUninstInfo();

	endif;

    // Disable Status
	Disable( STATUSEX );

end;


function SetPermissionsOnDir(szPath)
STRING szCmd;
STRING szCmdArgs;
begin
	StrRemoveLastSlash(szPath);
	szCmd = WINSYSDIR ^ "cmd.exe";
	szCmdArgs = "/c icacls \"" + szPath + "\" /grant Users:(OI)(CI)M";
	LaunchAppAndWait(szCmd, szCmdArgs, LAAW_OPTION_HIDDEN | LAAW_OPTION_WAIT);
end;

/////////////////////////////////////////////////////////////////////////////
//
// Function number CreateDirectory(sDir)
//
// Description: create a directory
//
// Argument: sDir - directory to create
//
// Return: 0 - already exists; 1 - created successfully; -1 - error
//
/////////////////////////////////////////////////////////////////////////////
function number CreateDirectory(sDir)
   number n;
begin
   StrRemoveLastSlash(sDir);
   if Is(PATH_EXISTS,sDir) then
      return 0;                                  // did nothing (already exists)
   elseif StrLength(sDir)>3 then
      n=CreateDir(sDir);
      if n>=0 then
         //Log("Directory "+sDir+" was created");
         return 1;                               // directory created
      else
         //Log("Creating directory "+sDir+" failed ("+FormatMessage(n)+")");
         return -1;                              // creating directory failed
      endif;
   elseif StrLength(sDir)>0 then
      //Log("Creating directory "+sDir+" failed because the input directory was invalid");
      return -1;
   endif;
end;