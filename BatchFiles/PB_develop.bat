cd \
REM ===========================================================
REM ***** NOTE *****
REM BEFORE CONTINUNG WITH THIS PROCESS:
REM 1. MAKE SURE ALL POWERBUILDER OBJECTS/PBLS ARE CHECKED IN TO CVS
REM 2. CLOSE ALL OCCURENCES OF THIS APPLICATION THAT ARE RUNNING IN POWERBUILDER
REM
REM ===========================================================
pause
REM ===========================================================
REM  Change the variables accordingly
REM  Always use the develop branch to create PB executables
REM ===========================================================
set branch_name=develop
set bld_folder=GIT_POWERBUILDER\PB_compile
set target_folder=PB_develop
REM ===========================================================
REM Rename the existing directory that is tied to CVS
REM Create a new directory for export from CVS
REM ===========================================================
C:
cd \
REM mkdir c:\%bld_folder%
rmdir c:\%bld_folder%\powerbuilder /s/q
REM pause
REM Get source code
REM
REM NOTE: 
REM ==============================================================
REM *** We will use the builder user for getting the source code
REM ==============================================================
REM cvs -d :pserver:mmcgraw@10.1.1.5:/usr/local/cvs/gfi export -r %target_folder% pb
cd \%bld_folder%
rem cvs -d :pserver;username=mmcgraw;password=mike4729:10.1.1.5:/usr/local/cvs/gfi export -r %branch_name% pb
git clone -b develop https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/powerbuilder.git
cd powerbuilder
git fetch && git checkout %branch_name%
pause

REM Create the Powerbuilder executables
REM
REM NOTE: 
REM ==============================================================
REM *** We will use the Powerbuilder batch deployment tool
REM ==============================================================
set bld_folder=GIT_POWERBUILDER\PB_compile\powerbuilder
cd \
cd C:\Program Files (x86)\Sybase\PowerBuilder 12.5\
PB125 /w C:\%bld_folder%\PB_develop.pbw /d /out C:\GFI\log\PB_develop.out
REM pause

REM ==============================================================
REM Copy the Powerbuilder executables the appropriate \bin file for Installshield pickup
REM ==============================================================
copy /v /y C:\%bld_folder%\dupkill\dupkill.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfibase\gfibase.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfivm\gfivm.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfivm\gfivm.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfiacs\gfiacs*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfiacs\gfiacs*.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiacsfs\gfiacsfs.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiacsgc\gfiacsgc.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfiarc\gfiarc.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiascii\gfiascii.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gficonf\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gficmd\gficmd.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gficfs\gficfs.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfidatav\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfidba\gfidba.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfiedit\gfiedit.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfiedit\gfiedit.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfieqcfg\gfieqcfg3.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfievmon\gfievmon.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfifmon\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfifsgg\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfifswiz\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiinfo\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfildr\gfildr.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfimon\gfimon.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfimon\gfimon.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfipprb\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiprbcleanup\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfirebld\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfirecon\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfirpt\gfirpt.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfirpt\gfirpt*.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfirptmf\gfirptmf.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfirptmf\gfirptsc.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfirun\gfirun.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfirun\gfirun.pbd "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfisetup\gfisetup.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfisetup\gfisetup_individual.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiscc\g*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfisql\gfisql.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfisum\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfisync\gfisync.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfitrcleanup\gfitr*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\gfiwsa\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /y C:\%bld_folder%\mkbin\*.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\vipedit\vipedit.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfirecon\gfirecon.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfidba\gfidba.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\logview\logview.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\launcher\launcher.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\binview\binview.exe "C:\InstallShield Projects\Support\Software\Common"
copy /v /Y C:\%bld_folder%\gfiprbcleanup\gfiprbcleanup.exe "C:\InstallShield Projects\Support\Software\Common"
REM pause
REM INSERT CODE TO SWITCH PB WORKSPACE BACK TO non-BLD WORKSPACE
REM PB125 /w C:\%branch_name%\pb125_non_cld_pb125_merge.pbw /d /out C:\GFI\log\pb125_non_cld_pb125_merge.out
REM ===========================================================
REM Remove the build folder directory 
REM Solve the problem of connecting to the wrong PB workspace
REM ===========================================================
C:
cd \
REM Powerbuilder Build Completed
pause