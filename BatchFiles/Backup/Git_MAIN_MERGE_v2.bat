Echo off

REM ===========================================================
REM  Copy PB9/Database files for CLOUD
REM ===========================================================
REM
REM ===========================================================
REM Get the Branch to build
REM ===========================================================
goto :BranchContinue
:ReEnterBranch
REM 
REM BitBucket Branch for this release
REM
echo Enter BitBucketBranch without prefix(PB,DB,DS 
echo Example: 03_00_01
set /p BRANCH=Branch:
REM 
REM Get confirmation
REM
set /p CONFIRMBRANCH=Is this correct [%BRANCH%] (Y/N)?
echo You entered '%CONFIRMBRANCH%'
REM
REM Continue after confirmation
REM
if %CONFIRMBRANCH%==Y goto BranchContinue
if %CONFIRMBRANCH%==y goto BranchContinue
goto ReEnterBranch
:BranchContinue

REM @del /F /Q "C:\InstallShield 2011 Projects\Share\File\GFI\SQLLDR\*.*" > NUL
REM copy /Y "\\10.1.1.42\public\gfisoft\DBTools\SQL\SQLLDR\*.ctl" "C:\InstallShield 2011 Projects\Share\File\GFI\SQLLDR"

REM - V2 - Included in pb_develop.bat - Begin
REM Copy POWERBUILDER Code to Installshield folder
REM @del /F /Q "C:\InstallShield Projects\Support\Software\Common\*.*" > NUL
REM copy /Y "\\10.1.1.42\public\gfisoft\PB_develop\Bin\*.*" "C:\InstallShield Projects\Support\Software\Common"

REM copy /Y "\\10.1.1.42\public\gfisoft\DBTools\SSIS\InstallShield_CLOUD\*.dtsx" "C:\InstallShield 2011 Projects\Share\File\GFI\SSIS"
REM @pause
REM - V2 - Included in pb_develop.bat - End

REM ===========================================================
REM Build solution
REM
REM NOTE: You can build the solution by simply calling devenv but you can also
REM       Build with MSBuild (which is preferable) as it give you a betting 
REM       logging facility as well as color coded display for warnings and errors
REM       and a lot of other stuff.
REM
REM	03/01/16 - MGM - Change to pull from DSBR_V2061119_20160422 branch instead of V7_Albany
REM ===========================================================

REM ===========================================================
REM Set up the Microsoft Development environment
REM ===========================================================
REM call "C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat" 
call "C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\Tools\vsvars32.bat" 

REM ===========================================================
REM Remove any existing files in preparation for checking out projects from GIT
REM ===========================================================
rmdir c:\git\data-system /S /Q
rmdir c:\git\databases /S /Q
REM rmdir c:\git\powerbuilder /S /Q 	- V2 - Included in pb_develop.bat

REM pause

REM Get source code
REM ==============================================================
REM *** Use the builder user for getting the source code
REM ==============================================================
REM

echo Will now get the C Source Code
cd \git
REM Get Data System Source From Git Repository
git clone -b develop https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/data-system.git
cd data-system
REM git fetch && git checkout develop
REM git fetch && git checkout DS_%BRANCH%
git fetch && git checkout develop

echo Will now get the Database Source Code
cd \git
REM Get Data System Source From Git Repository
git clone -b develop https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/databases.git
cd databases
git fetch && git checkout develop
REM git fetch && git checkout DB_%BRANCH%

REM - V2 - Included in pb_develop.bat - Begin
REM echo Will now get the PowerBuilder Source Code
REM cd \git
REM Get Data System Source From Git Repository
REM git clone -b develop https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/powerbuilder.git
REM cd powerbuilder
REM git fetch && git checkout develop
REM git fetch && git checkout PB_%BRANCH%
REM - V2 - Included in pb_develop.bat - End

REM ==============================================================
REM Copy SQL to Installshield folder
REM ==============================================================
@del /F /Q "C:\InstallShield Projects\Support\Software\SQL\*.SQL" > NUL
copy /Y "C:\Git\databases\MSSQL\ScriptFiles\InstallShield\*.sql" "C:\InstallShield Projects\Support\Software\SQL"
copy /Y "C:\Git\databases\Sybase\InstallShield\*.SQL" "C:\InstallShield Projects\Support\Software\SQL"
copy /Y "C:\Git\data-system\CloudSQL\*.sql" "C:\InstallShield Projects\Support\Software\SQL"
REM ========================================

echo Make sure that branch was successfully retrieved.

REM ==============================================================
REM ***	Update Release version verion in ProductVersion.h     ****
REM ============================================================== 
:ReEnterVersion
REM 
REM Get the version for this release
REM
set /p VERSION=Please enter version (example: 3.0.1): 
REM 
REM Get confirmation
REM
set /p CONFIRM=Is this correct [%VERSION%] (Y/N)?
echo You entered '%CONFIRM%'
REM
REM Continue after confirmation
REM
if %CONFIRM%==Y goto Continue
if %CONFIRM%==y goto Continue
goto ReEnterVersion
:Continue
echo #define STRPRODUCTVER  "%VERSION%\0" > C:\git\data-system\Common\Include\ProductVersion.h

C:\Replace_text\FART -i -r "C:\InstallShield Projects\Support\Software\SQL\GFIDB*.sql" 0.0.0 %VERSION%
REM pause

REM ==============================================================
REM Now add the comma delimited version 
REM ==============================================================
set str=%VERSION%
set str=%str:.=,%
echo New String=%str%
echo #define NUMPRODUCTVERSION  %str% >> C:\git\data-system\Common\Include\ProductVersion.h

REM ==============================================================
REM Compile all the Powerbuilder targets and copy to C:\InstallShield Projects\Support\Software\Common
REM ==============================================================
call "C:\GIT_POWERBUILDER\PB_develop\Batch\PB_develop.bat" 

REM ==============================================================
REM Make the release folder in case it does not exist
REM ==============================================================
cd \git\data-system\MasterBuild
md Release

REM ===========================================================
REM Remove log file if using MSBuild
REM ===========================================================
del \git\data-system\MasterBuild\msbuild.log

REM ===========================================================
REM Build the Data System Release  
REM ===========================================================
del  /F /Q "C:\InstallShield Projects\Support\Software\VSS\*.*"
msbuild MasterBuild.sln /t:Build /p:Configuration=Release /FileLogger /verbosity:normal

REM ===========================================================
REM Build the Configuration Project  
REM ===========================================================
cd \git\data-system\ConfigurationWinForm\ConfigurationWinForm
md bin
cd bin
md Release
cd ..\..
msbuild ConfigurationWinForm.sln /t:Build /p:Configuration=Release /FileLogger /verbosity:normal


REM FINAL STEP Will be to Copy the files to the staging area 
REM ==============================================================================================================================
copy \git\data-system\MasterBuild\Release\*.exe  	               "C:\InstallShield Projects\Support\Software\VSS" /Y
copy \git\data-system\MasterBuild\Release\*.dll  	               "C:\InstallShield Projects\Support\Software\VSS" /Y
copy \git\data-system\MasterBuild\Release\kill.exe                     "C:\InstallShield Projects\Support\Software\Support" /Y
copy \git\data-system\MasterBuild\Release\ps.exe  	               "C:\InstallShield Projects\Support\Software\Support" /Y
copy \git\data-system\MasterBuild\Release\gfipb32.dll                  "C:\InstallShield Projects\Support\Software\Support" /Y
copy \InstallShield 2011 Projects\Support\Software\Common\gfisetup.exe "C:\InstallShield Projects\Support\Software\Support" /Y
REM ==============================================================================================================================

echo off
REM echo the log file to the screen
REM type \git\data-system\MasterBuild\msbuild.log
REM
echo =============================================================================
echo Please check the log file for errors (/git/data-system/MasterBuild/msbuild.log)
echo =============================================================================

:CreateInstallation
set /p CONFIRMTAG=Create Installation (Y/N)?
if %CONFIRMTAG%==Y goto :Installshield
if %CONFIRMTAG%==y goto :Installshield
set /p CONFIRMTAG=Confirm Abort Installation (Y/N)?
if %CONFIRMTAG%==Y goto :End
if %CONFIRMTAG%==y goto :End
goto CreateInstallation
pause Will now start installshield

:Installshield
REM Create Data System Installation
"C:\Program Files (x86)\InstallShield\2011\System\IscmdBld.exe" -p "C:\InstallShield Projects\GFI Data System\GFI Data System.ism"
REM
REM Create Configuration Installation
"C:\Program Files (x86)\InstallShield\2011\System\IscmdBld.exe" -p "C:\InstallShield Projects\Configuration\Configuration.ism"
:End

echo Installation Complete!
copy /Y "C:\InstallShield Projects\GFI Data System\GFI Data System\Media\SINGLE_EXE_IMAGE\Package\Setup.exe" "\\10.1.2.204\Software\GenfareSoftware\Builds\EXE"
copy /Y "C:\InstallShield Projects\Configuration\Configuration\Media\SINGLE_EXE_IMAGE\Package\setup.exe" "\\10.1.2.204\Software\GenfareSoftware\ConfigurationTools\EXE"
pause DONE








