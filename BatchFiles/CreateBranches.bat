echo off
REM ========================== Create Branches ==========================================
:StartBranch
cd \git
REM 
REM Get the version for this release
REM

REM 
REM Get confirmation
REM
set /p CONFIRM=Branch Souce Code (Y/N)?
echo You entered '%CONFIRM%'
REM
REM Continue after confirmation
REM
if %CONFIRM%==Y goto CreateBranch
if %CONFIRM%==y goto CreateBranch
if %CONFIRM%==n goto Finished
if %CONFIRM%==N goto NoBranch
goto Finished
pause

:CreateBranch
REM 
REM Set Git Branch for this release
REM
set /p TAG=Please enter Git BRANCH NAME (i.e: 030001_20140728 (version(VVVVVV)/Date(YYYYMMDD)): 
REM 
REM Get confirmation
REM
REM
echo DS Branch: DSBR_%TAG% 
echo PB Branch: PBBR_%TAG%
echo DB Branch: DBBR_%TAG%
set /p CONFIRMTAG=Is this correct (Y/N)?
echo You entered '%CONFIRMTAG%'
REM
REM Continue after confirmation

if %CONFIRMTAG%==Y goto Continue
if %CONFIRMTAG%==y goto Continue
goto Finished
pause

:Continue
echo =======================BRANCHING===================
REM git branch <BRANCH NAME>
REM git checkout <BRANCH NAME>
REM git push https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/data-system.git <BRANCH NAME>
pause
cd \git\data-system
git branch DSBR_%TAG%
git checkout DSBR_%TAG%
git push https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/data-system.git DSBR_%TAG%

cd \git\databases
git branch DBBR_%TAG%
git checkout DBBR_%TAG%
git push https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/databases.git DBBR_%TAG%

cd \git_powerbuilder\PB_compile\powerbuilder
git branch PBBR_%TAG%
git checkout PBBR_%TAG%
git push https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/powerbuilder.git PBBR_%TAG%


:Finished